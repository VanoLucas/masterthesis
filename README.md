# Solving the Cover Printing Problem

## Running RKHC

The executable jar is located here:
`program/cppsolver/out/artifacts/CppSolverExperiment_jar/CppSolverExperiment.jar`

Usage:
`java -jar program/cppsolver/out/artifacts/CppSolverExperiment_jar/CppSolverExperiment.jar "path/to/input/file.json"`

Example:
`java -jar program/cppsolver/out/artifacts/CppSolverExperiment_jar/CppSolverExperiment.jar "/lucas/Lucas/Travail/UMons/Master_2/Memoire/git/program/cppsolver/CppSolver/input/input.json"`

Before running, put the desired parameters in a JSON file that you pass as param to the executable.
The JSON contains the path to the desired CPP demand instance.

## Main Class to Run RKHC

The main class for the ready-to-run RKHC CPP solver is located here:
`program/cppsolver/CppSolver/src/com/vanolucas/cppsolver/experiment/Experiment.java`

## Reference CPP Instances

The files that contain the demand for the reference CPP instances are available in:
`instances/`

## Sample Solutions

Some obtained solutions are stored in:
`solutions/`
