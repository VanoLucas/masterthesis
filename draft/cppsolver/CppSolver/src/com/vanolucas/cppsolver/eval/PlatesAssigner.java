package com.vanolucas.cppsolver.eval;

import com.vanolucas.cppsolver.representation.Demand;
import com.vanolucas.cppsolver.representation.Slots;
import com.vanolucas.cppsolver.solution.CppSolution;

/**
 * Allocates the input slots to some offset plates.
 */
public abstract class PlatesAssigner {
    protected final Demand demand;

    public PlatesAssigner(Demand demand) {
        this.demand = demand;
    }

    public abstract CppSolution assign(Slots slots, int slotsPerPlateCount);
}
