package com.vanolucas.cppsolver.eval;

import com.vanolucas.cppsolver.representation.Demand;
import com.vanolucas.cppsolver.representation.Slots;

/**
 * Allocates the demand for book cover copies over a number of slots.
 */
public interface SlotsAssigner {
    Slots assign(Demand demand, int slotsCount);
}
