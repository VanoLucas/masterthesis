package com.vanolucas.cppsolver.eval;

import com.vanolucas.cppsolver.instance.CppInstance;
import com.vanolucas.cppsolver.representation.Demand;
import com.vanolucas.cppsolver.representation.Slots;
import com.vanolucas.cppsolver.solution.CppSolution;

public class SlotsDecoder {
    private final PlatesAssigner platesAssigner;
    private final int slotsPerPlateCount;

    public SlotsDecoder(CppInstance cppInstance) {
        this(new SortByCopiesPlatesAssigner(cppInstance.getDemand()), cppInstance);
    }

    public SlotsDecoder(Demand demand, int slotsPerPlateCount) {
        this(new SortByCopiesPlatesAssigner(demand), slotsPerPlateCount);
    }

    public SlotsDecoder(PlatesAssigner platesAssigner, CppInstance cppInstance) {
        this(platesAssigner, cppInstance.getSlotsPerPlateCount());
    }

    public SlotsDecoder(PlatesAssigner platesAssigner, int slotsPerPlateCount) {
        this.platesAssigner = platesAssigner;
        this.slotsPerPlateCount = slotsPerPlateCount;
    }

    public CppSolution decode(Slots slots) {
        return platesAssigner.assign(slots, slotsPerPlateCount);
    }
}
