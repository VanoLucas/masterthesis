package com.vanolucas.cppsolver.experiment;

import com.vanolucas.cppsolver.genome.GenomeType;
import com.vanolucas.cppsolver.instance.StandardCppInstance;
import com.vanolucas.cppsolver.representation.Demand;
import com.vanolucas.cppsolver.solver.Rkhc;
import com.vanolucas.cppsolver.ux.BestCostHistoryChart;
import com.vanolucas.opti.quality.DoubleCost;
import com.vanolucas.opti.solution.keeper.EarliestSingleBestKeeper;

import java.io.IOException;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

public class Experiment {
    private final AtomicBoolean exit = new AtomicBoolean(false);

    public static void main(String[] args) throws IOException {
        new Experiment(args).run();
    }

    private final String[] args;

    public Experiment(String[] args) {
        this.args = args;
    }

    public void run() throws IOException {
        // print usage
        System.out.println("Usage: the first cmd line arg must be the path to the input params JSON file.");
        System.out.println();

        // load input params from JSON
        if (args.length < 1) {
            throw new IllegalArgumentException("Missing path to the JSON input params file.");
        }
        InputParams inputParams = InputParams.fromJsonFile(args[0].trim());

        // load demand file
        Demand demand = Demand.loadFromFile(Paths.get(inputParams.getDemandPath()));

        // create cpp instance
        StandardCppInstance cppInstance = new StandardCppInstance(
                demand,
                inputParams.getPlatesCount(),
                inputParams.getSlotsPerPlateCount(),
                inputParams.getPlateCost(),
                inputParams.getPrintingCost()
        );

        // init random numbers generator
        Random random = new Random();

        // get available processors on this machine
        final int availableProcessors = Runtime.getRuntime().availableProcessors();

        // create overall best solution keeper
        EarliestSingleBestKeeper experimentBestKeeper = new EarliestSingleBestKeeper(true);

        // to store measures
        List<List<Double>> costMeasures = new ArrayList<>();

        // display best cost chart
        BestCostHistoryChart chart = new BestCostHistoryChart(() -> exit.set(true));
        if (inputParams.isShowChart()) {
            chart.display();
        }

        // do runs forever
        for (int run = 1; !exit.get(); run++) {
            // get a new RKHC algo instance
            Rkhc rkhc = new Rkhc(
                    cppInstance,
                    inputParams.getRestartAfterDurationWithoutImprovement(),
                    GenomeType.valueOf(inputParams.getGenomeType()),
                    availableProcessors,
                    run == 1, // print all better solutions in the first run
                    random
            );

            // store run start time
            Instant runStart = Instant.now();

            // run the first iteration
            rkhc.runForIterations(1L);

            // store current best cost
            costMeasures.add(new ArrayList<>());
            DoubleCost currentBestCost = rkhc.getBestQuality();
            costMeasures.get(costMeasures.size() - 1).add(currentBestCost.doubleValue());

            // update overall best
            experimentBestKeeper.submit(rkhc.getBestSolution());

            // plot best on chart
            if (inputParams.isShowChart() && inputParams.isPlotFirstIteration()) {
                chart.addValue(currentBestCost.doubleValue());
            }

            // run for as long as asked
            while (!exit.get()
                    && runStart
                    .plus(inputParams.getRunDuration())
                    .minus(inputParams.getMeasurePeriod())
                    .isAfter(Instant.now())) {
                // run until the next measure point
                rkhc.runForAtLeast(inputParams.getMeasurePeriod());

                // store current best cost
                currentBestCost = rkhc.getBestQuality();
                costMeasures.get(costMeasures.size() - 1).add(currentBestCost.doubleValue());

                // plot current best cost on chart
                if (inputParams.isShowChart()) {
                    chart.addValue(currentBestCost.doubleValue());
                }

                // update overall best
                experimentBestKeeper.submit(rkhc.getBestSolution());
            }

            // shutdown thread pool
            rkhc.shutdownThreadPool();

            // print cpp instance
            System.out.println();
            System.out.println("CPP instance:");
            System.out.println(cppInstance);

            // print overall best solution
            System.out.println();
            System.out.println("Overall best:");
            experimentBestKeeper.printBest();

            // print best costs detailed history
            System.out.println();
            System.out.println("Detailed history of best costs:");
            costMeasures.stream()
                    .map(Objects::toString)
                    .forEach(System.out::println);

            // todo print experiment stats (run count, avg cost, median cost, detailed costs)
        }
    }
}
