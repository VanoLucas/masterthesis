package com.vanolucas.cppsolver.experiment;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.time.Duration;

public class InputParams {
    private final String demandPath;
    private final int platesCount;
    private final int slotsPerPlateCount;
    private final double plateCost;
    private final double printingCost;
    private final String genomeType;
    private final Duration restartAfterDurationWithoutImprovement;
    private final Duration runDuration;
    private final Duration measurePeriod;
    private final boolean showChart;
    private final boolean plotFirstIteration;

    public InputParams(String demandPath,
                       int platesCount,
                       int slotsPerPlateCount,
                       double plateCost,
                       double printingCost,
                       String genomeType,
                       Duration restartAfterDurationWithoutImprovement,
                       Duration runDuration,
                       Duration measurePeriod,
                       boolean showChart,
                       boolean plotFirstIteration) {
        this.demandPath = demandPath;
        this.platesCount = platesCount;
        this.slotsPerPlateCount = slotsPerPlateCount;
        this.plateCost = plateCost;
        this.printingCost = printingCost;
        this.genomeType = genomeType;
        this.restartAfterDurationWithoutImprovement = restartAfterDurationWithoutImprovement;
        this.runDuration = runDuration;
        this.measurePeriod = measurePeriod;
        this.showChart = showChart;
        this.plotFirstIteration = plotFirstIteration;
    }

    public String getDemandPath() {
        return demandPath;
    }

    public int getPlatesCount() {
        return platesCount;
    }

    public int getSlotsPerPlateCount() {
        return slotsPerPlateCount;
    }

    public double getPlateCost() {
        return plateCost;
    }

    public double getPrintingCost() {
        return printingCost;
    }

    public String getGenomeType() {
        return genomeType;
    }

    public Duration getRestartAfterDurationWithoutImprovement() {
        return restartAfterDurationWithoutImprovement;
    }

    public Duration getRunDuration() {
        return runDuration;
    }

    public Duration getMeasurePeriod() {
        return measurePeriod;
    }

    public boolean isShowChart() {
        return showChart;
    }

    public boolean isPlotFirstIteration() {
        return plotFirstIteration;
    }

    public static InputParams fromJsonFile(String jsonPath) throws FileNotFoundException {
        BufferedReader br = new BufferedReader(new FileReader(jsonPath));
        return new Gson().fromJson(br, InputParams.class);
    }

    public String toJson() {
        return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }

    @Override
    public String toString() {
        return toJson();
    }
}
