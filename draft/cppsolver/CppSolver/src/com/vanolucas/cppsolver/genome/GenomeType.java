package com.vanolucas.cppsolver.genome;

import com.vanolucas.cppsolver.genome.decode.GenomeCfDecoder;
import com.vanolucas.cppsolver.genome.decode.GenomePpcDecoder;
import com.vanolucas.cppsolver.genome.decode.GenomeSpDecoder;
import com.vanolucas.cppsolver.instance.StandardCppInstance;
import com.vanolucas.cppsolver.representation.Slots;
import com.vanolucas.opti.genome.RandomKeyVector;

import java.util.function.Function;

public enum GenomeType {
    CF(GenomeCf::new,
            cppInstance -> genome -> new GenomeCfDecoder(cppInstance).decode((GenomeCf) genome)),
    P_PC(GenomePpc::new,
            cppInstance -> genome -> new GenomePpcDecoder(cppInstance).decode((GenomePpc) genome)),
    S_P(GenomeSp::new,
            cppInstance -> genome -> new GenomeSpDecoder(cppInstance).decode((GenomeSp) genome));

    private final Function<StandardCppInstance, RandomKeyVector> genomeFactory;
    private final Function<StandardCppInstance, Function<RandomKeyVector, Slots>> genomeDecoderFactory;

    GenomeType(Function<StandardCppInstance, RandomKeyVector> genomeFactory,
               Function<StandardCppInstance, Function<RandomKeyVector, Slots>> genomeDecoderFactory) {
        this.genomeFactory = genomeFactory;
        this.genomeDecoderFactory = genomeDecoderFactory;
    }

    public <TGenome extends RandomKeyVector> TGenome newGenome(StandardCppInstance cppInstance) {
        return (TGenome) genomeFactory.apply(cppInstance);
    }

    public <TGenome extends RandomKeyVector> Function<TGenome, Slots> newGenomeDecoder(StandardCppInstance cppInstance) {
        return (Function<TGenome, Slots>) genomeDecoderFactory.apply(cppInstance);
    }
}

