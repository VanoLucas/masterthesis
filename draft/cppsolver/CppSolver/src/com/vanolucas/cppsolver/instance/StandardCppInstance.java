package com.vanolucas.cppsolver.instance;

import com.vanolucas.cppsolver.representation.Demand;

/**
 * An instance of the standard cover printing problem.
 */
public class StandardCppInstance extends CppInstance {
    /**
     * n = number of offset plates to use.
     */
    private final int platesCount;

    public StandardCppInstance(Demand demand, int platesCount, int slotsPerPlateCount) {
        super(demand, slotsPerPlateCount);
        this.platesCount = platesCount;
    }

    public StandardCppInstance(Demand demand, int platesCount, int slotsPerPlateCount, double costPlate, double costPrinting) {
        super(demand, slotsPerPlateCount, costPlate, costPrinting);
        this.platesCount = platesCount;
    }

    public int getSlotsCount() {
        return platesCount * getSlotsPerPlateCount();
    }

    public String strPlatesCount() {
        return String.format("n = %d offset plates%n" +
                        "n*S = total %d slots",
                platesCount, getSlotsCount()
        );
    }

    @Override
    public String toString() {
        return String.format("%s%n" +
                        "%s%n" +
                        "%s%n" +
                        "%s%n" +
                        "%s%n" +
                        "Demand:%n" +
                        "%s%n",
                strBookCoversCount(),
                strPlatesCount(),
                strSlotsPerPlateCount(),
                strCostPlate(),
                strCostPrinting(),
                strDemand()
        );
    }
}
