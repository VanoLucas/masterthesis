package com.vanolucas.cppsolver.representation;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Slots {
    private final List<Copies> copies;

    public Slots(int expectedSize) {
        this(new ArrayList<>(expectedSize));
    }

    public Slots(List<Copies> copies) {
        this.copies = new ArrayList<>(copies);
    }

    public Stream<BookCover> streamBookCovers() {
        return copies.stream()
                .map(Copies::getBookCover);
    }

    public int getMaxCopiesCount() {
        return copies.stream()
                .mapToInt(Copies::getCopiesCount)
                .max()
                .orElse(0);
    }

    public int getSlotsCount() {
        return copies.size();
    }

    public void add(Copies c) {
        copies.add(c);
    }

    public void addAll(Collection<? extends Copies> copiesCollection) {
        copies.addAll(copiesCollection);
    }

    public Stream<Slots> splitInGroupsOf(int groupSize) {
        return Lists.partition(copies, groupSize)
                .stream()
                .map(Slots::new);
    }

    public void sortByCopiesCountDescending() {
        copies.sort(Comparator.reverseOrder());
    }

    public void splitInTwo(int slotIndex, double fractionToExtract) {
        final Copies newCopiesItem = copies.get(slotIndex)
                .splitInTwo(fractionToExtract);
        add(newCopiesItem);
    }

    @Override
    public String toString() {
        return copies.stream()
                .map(Object::toString)
                .collect(Collectors.joining(System.lineSeparator()));
    }
}

