package com.vanolucas.cppsolver.ux;

import com.vanolucas.cppsolver.representation.Demand;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Paths;

public abstract class DemandFileChooser {
    private static final String LUCAS_HOSTNAME = "lucas-pc";
    private static final String LUCAS_CPP_INSTANCES_PATH = "/lucas/Lucas/Travail/UMons/Master_2/Memoire/git/instances/";

    public static Demand loadInstanceOrChooseFile(String instanceName) throws IOException {
        if (isLucasHost()) {
            return Demand.loadFromFile(
                    Paths.get(LUCAS_CPP_INSTANCES_PATH + instanceName + ".dat")
            );
        } else {
            return chooseFileAndGetDemand();
        }
    }

    public static Demand chooseFileAndGetDemand() throws IOException {
        File file = chooseFile();
        if (file == null) {
            throw new IllegalStateException("No file was chosen to import the book covers demand.");
        }

        return Demand.loadFromFile(
                file.toPath()
        );
    }

    public static File chooseFile() {
        System.out.println("\nPlease select a CPP demand file where each line contains:\n" +
                "(1) the book cover number (2) ONE space (3) the demand.");

        final JFileChooser jfc = new JFileChooser(getInitialDir());

        final int returnValue = jfc.showOpenDialog(null);

        if (returnValue == JFileChooser.APPROVE_OPTION) {
            return jfc.getSelectedFile();
        } else {
            return null;
        }
    }

    private static File getInitialDir() {
        // if lucas host, the initial dir is the known path to cpp instances folder
        if (isLucasHost()) {
            return Paths.get(LUCAS_CPP_INSTANCES_PATH).toFile();
        } else {
            return FileSystemView.getFileSystemView().getHomeDirectory();
        }
    }

    private static boolean isLucasHost() {
        // detect hostname
        String hostname;
        try {
            hostname = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            hostname = "";
        }

        return hostname.equals(LUCAS_HOSTNAME);
    }
}

