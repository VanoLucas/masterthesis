package com.vanolucas.cppsolver.ux;

import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class BestCostHistoryChartTest {

    @Test
    void display() throws InterruptedException {
        BestCostHistoryChart chart = new BestCostHistoryChart(() -> System.out.println("window closed"));
        chart.display();
        chart.addValue(0d);
        chart.addValue(1d);
        TimeUnit.SECONDS.sleep(5L);
    }
}