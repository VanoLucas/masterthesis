package com.vanolucas.opti.algo;

import com.vanolucas.opti.eval.Evaluator;
import com.vanolucas.opti.mutation.neighborhood.NeighborsProducer;
import com.vanolucas.opti.solution.EvaluatedSolution;
import com.vanolucas.opti.solution.Solution;

import java.util.concurrent.Executor;

public class HillClimbing {
    public enum Variant {
        SIMPLE, STEEPEST_ASCENT
    }

    public static EvaluatedSolution steepestAscentStep(Solution currentSolution, NeighborsProducer neighborsProducer, Evaluator evaluator) {
        return neighborsProducer.getBestNeighbor(currentSolution, evaluator);
    }

    public static EvaluatedSolution steepestAscentStepParallel(Solution currentSolution, NeighborsProducer neighborsProducer, Evaluator evaluator, Executor executor) {
        return neighborsProducer.getBestNeighborParallel(currentSolution, evaluator, executor);
    }
}
