package com.vanolucas.opti.algo;

import java.time.Duration;
import java.time.Instant;

public class OptiRunner {
    private long iteration = 0L;
    private final Opti opti;

    public OptiRunner(Opti opti) {
        this.opti = opti;
    }

    public long runForIterations(long iterationsCount) {
        for (long i = 0L; i < iterationsCount; i++) {
            runIteration();
        }
        return iteration;
    }

    public long runForAtLeast(Duration runDuration) {
        Instant instantStarted = Instant.now();
        while (instantStarted.plus(runDuration).isAfter(Instant.now())) {
            runIteration();
        }
        return iteration;
    }

    public void runIteration() {
        iteration++;
        opti.runStep();
    }
}
