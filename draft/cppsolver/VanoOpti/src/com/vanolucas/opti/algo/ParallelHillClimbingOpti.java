package com.vanolucas.opti.algo;

import com.vanolucas.opti.eval.Evaluator;
import com.vanolucas.opti.mutation.neighborhood.NeighborsProducer;
import com.vanolucas.opti.solution.EvaluatedSolution;
import com.vanolucas.opti.solution.Solution;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.function.Consumer;

public class ParallelHillClimbingOpti extends HillClimbingOpti {
    private final Executor executor;

    public ParallelHillClimbingOpti(Solution initialSolution,
                                    NeighborsProducer neighborsProducer,
                                    Evaluator evaluator,
                                    HillClimbing.Variant variant,
                                    Executor executor) {
        super(initialSolution, neighborsProducer, evaluator, variant);
        this.executor = executor;
    }

    public ParallelHillClimbingOpti(Solution initialSolution,
                                    NeighborsProducer neighborsProducer,
                                    Evaluator evaluator,
                                    HillClimbing.Variant variant,
                                    Executor executor,
                                    Consumer<EvaluatedSolution> onNewCurrentSolutionListener) {
        super(initialSolution, neighborsProducer, evaluator, variant, onNewCurrentSolutionListener);
        this.executor = executor;
    }

    public ParallelHillClimbingOpti(Solution initialSolution,
                                    NeighborsProducer neighborsProducer,
                                    Evaluator evaluator,
                                    HillClimbing.Variant variant,
                                    Executor executor,
                                    List<Consumer<EvaluatedSolution>> onNewCurrentSolutionListeners) {
        super(initialSolution, neighborsProducer, evaluator, variant, onNewCurrentSolutionListeners);
        this.executor = executor;
    }

    @Override
    protected void runSteepestAscentStep() {
        updateCurrentSolution(HillClimbing.steepestAscentStepParallel(
                currentSolution, neighborsProducer, evaluator, executor
        ));
    }
}
