package com.vanolucas.opti.algo;

import com.vanolucas.opti.solution.Solution;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.BooleanSupplier;
import java.util.function.Supplier;

public class RandomRestartHillClimbingOpti implements Opti {
    private final HillClimbingOpti hillClimbingOpti;
    private final Supplier<Solution> randomSolutionSupplier;
    private final BooleanSupplier randomRestartDecider;
    private final List<Runnable> onRandomRestartListeners;

    public RandomRestartHillClimbingOpti(HillClimbingOpti hillClimbingOpti,
                                         Supplier<Solution> randomSolutionSupplier,
                                         BooleanSupplier randomRestartDecider) {
        this(hillClimbingOpti, randomSolutionSupplier, randomRestartDecider, (List<Runnable>) null);
    }

    public RandomRestartHillClimbingOpti(HillClimbingOpti hillClimbingOpti,
                                         Supplier<Solution> randomSolutionSupplier,
                                         BooleanSupplier randomRestartDecider,
                                         Runnable onRandomRestartListener) {
        this(hillClimbingOpti, randomSolutionSupplier, randomRestartDecider, Collections.singletonList(onRandomRestartListener));
    }

    public RandomRestartHillClimbingOpti(HillClimbingOpti hillClimbingOpti,
                                         Supplier<Solution> randomSolutionSupplier,
                                         BooleanSupplier randomRestartDecider,
                                         List<Runnable> onRandomRestartListeners) {
        this.hillClimbingOpti = hillClimbingOpti;
        this.randomSolutionSupplier = randomSolutionSupplier;
        this.randomRestartDecider = randomRestartDecider;
        if (onRandomRestartListeners != null && onRandomRestartListeners.size() > 0) {
            this.onRandomRestartListeners = new ArrayList<>(onRandomRestartListeners);
        } else {
            this.onRandomRestartListeners = new ArrayList<>(2);
        }
    }

    @Override
    public void runStep() {
        if (randomRestartDecider.getAsBoolean()) {
            randomRestart();
        }
        hillClimbingOpti.runStep();
    }

    protected void randomRestart() {
//        System.out.println("Random restart");
        hillClimbingOpti.setCurrentSolution(randomSolutionSupplier.get());
        onRandomRestartListeners.forEach(Runnable::run);
    }
}
