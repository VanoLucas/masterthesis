package com.vanolucas.opti.clone;

import com.vanolucas.opti.solution.Solution;

public interface Cloner {
    Solution clone(Solution solution);
}
