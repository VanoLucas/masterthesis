package com.vanolucas.opti.eval;

import com.vanolucas.opti.solution.EvaluatedSolution;
import com.vanolucas.opti.solution.Solution;

public interface Evaluator {
    EvaluatedSolution evaluate(Solution solution);
}
