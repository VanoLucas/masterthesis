package com.vanolucas.opti.eval;

import com.vanolucas.opti.solution.EvaluatedSolution;
import com.vanolucas.opti.solution.Solution;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

public class ListenableEvaluator implements Evaluator {
    private final Evaluator evaluator;
    private final List<Consumer<EvaluatedSolution>> onSolutionEvaluatedListeners;

    public ListenableEvaluator(Evaluator evaluator) {
        this(evaluator, (List<Consumer<EvaluatedSolution>>) null);
    }

    public ListenableEvaluator(Evaluator evaluator, Consumer<EvaluatedSolution> onSolutionEvaluatedListener) {
        this(evaluator, Collections.singletonList(onSolutionEvaluatedListener));
    }

    public ListenableEvaluator(Evaluator evaluator, List<Consumer<EvaluatedSolution>> onSolutionEvaluatedListeners) {
        this.evaluator = evaluator;
        if (onSolutionEvaluatedListeners != null && onSolutionEvaluatedListeners.size() > 0) {
            this.onSolutionEvaluatedListeners = new ArrayList<>(onSolutionEvaluatedListeners);
        } else {
            this.onSolutionEvaluatedListeners = new ArrayList<>(2);
        }
    }

    @Override
    public EvaluatedSolution evaluate(Solution solution) {
        EvaluatedSolution evaluatedSolution = evaluator.evaluate(solution);
        onSolutionEvaluatedListeners.forEach(listener -> listener.accept(evaluatedSolution));
        return evaluatedSolution;
    }
}
