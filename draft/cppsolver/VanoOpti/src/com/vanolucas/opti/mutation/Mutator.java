package com.vanolucas.opti.mutation;

import com.vanolucas.opti.solution.Solution;

public interface Mutator {
    Solution mutate(Solution solution);
}
