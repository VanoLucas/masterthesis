package com.vanolucas.opti.mutation.neighborhood;

import com.vanolucas.opti.eval.Evaluator;
import com.vanolucas.opti.solution.EvaluatedSolution;
import com.vanolucas.opti.solution.Solution;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.stream.Collectors;

public class NeighborsProducer {
    private final List<NeighborProducer> neighborProducers;

    public NeighborsProducer(List<NeighborProducer> neighborProducers) {
        if (neighborProducers.size() < 1) {
            throw new IllegalArgumentException("There must be at least 1 neighbor producer.");
        }
        this.neighborProducers = new ArrayList<>(neighborProducers);
    }

    public EvaluatedSolution getBestNeighbor(Solution srcSolution, Evaluator evaluator) {
        EvaluatedSolution bestNeighbor = null;
        for (NeighborProducer neighborProducer : neighborProducers) {
            EvaluatedSolution neighbor = neighborProducer.produceAndEvaluateNeighbor(srcSolution, evaluator);
            bestNeighbor = EvaluatedSolution.getBestOrElseFirst(bestNeighbor, neighbor);
        }
        return bestNeighbor;
    }

    public EvaluatedSolution getBestNeighborParallel(Solution srcSolution, Evaluator evaluator, Executor executor) {
        return neighborProducers.stream()
                .map(neighborProducer ->
                        CompletableFuture.supplyAsync(() ->
                                        neighborProducer.produceAndEvaluateNeighbor(srcSolution, evaluator),
                                executor)
                )
                .collect(Collectors.toList())
                .stream()
                .map(CompletableFuture::join)
                .max(Comparator.naturalOrder())
                .orElseThrow(() -> new IllegalStateException("Could not find the best neighbor."));
    }
}
