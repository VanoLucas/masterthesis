package com.vanolucas.opti.problem.targetnb;

import com.vanolucas.opti.clone.Cloneable;

public class FourInt implements Cloneable<FourInt> {
    private int left1;
    private int left2;
    private int right1;
    private int right2;

    public FourInt(int left1, int left2, int right1, int right2) {
        this.left1 = left1;
        this.left2 = left2;
        this.right1 = right1;
        this.right2 = right2;
    }

    public int getSumLeft() {
        return left1 + left2;
    }

    public int getSumRight() {
        return right1 + right2;
    }

    public FourInt incrementLeft1() {
        left1++;
        return this;
    }

    public FourInt decrementLeft1() {
        left1--;
        return this;
    }

    public FourInt incrementLeft2() {
        left2++;
        return this;
    }

    public FourInt decrementLeft2() {
        left2--;
        return this;
    }

    public FourInt incrementRight1() {
        right1++;
        return this;
    }

    public FourInt decrementRight1() {
        right1--;
        return this;
    }

    public FourInt incrementRight2() {
        right2++;
        return this;
    }

    public FourInt decrementRight2() {
        right2--;
        return this;
    }

    @Override
    public FourInt clone() throws CloneNotSupportedException {
        return (FourInt) super.clone();
    }

    @Override
    public String toString() {
        return String.format("%3d;%3d;%3d;%3d", left1, left2, right1, right2);
    }
}
