package com.vanolucas.opti.problem.targetnb;

import com.vanolucas.opti.quality.IntCost;

public class IntEvaluator {
    private final int target;

    public IntEvaluator(int target) {
        this.target = target;
    }

    public IntCost evaluate(Int phenotype) {
        return new IntCost(
                phenotype.getDistanceTo(target)
        );
    }
}
