package com.vanolucas.opti.problem.targetnb;

public interface TwoIntDecoder {
    static Int decode(TwoInt twoInt) {
        return new Int(twoInt.getSum());
    }
}
