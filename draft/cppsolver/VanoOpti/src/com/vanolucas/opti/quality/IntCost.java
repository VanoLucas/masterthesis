package com.vanolucas.opti.quality;

public class IntCost extends Objective<Integer> {
    public static final IntCost ZERO = new IntCost(0);

    public IntCost(Integer value) {
        super(value);
    }

    @Override
    public boolean isBetterThan(Integer otherValue) {
        return value < otherValue;
    }

    @Override
    public String toString() {
        return String.format("Cost: %s", super.toString());
    }
}
