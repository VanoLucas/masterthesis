package com.vanolucas.opti.quality;

public abstract class Objective<T extends Number> implements Quality {
    protected final T value;

    public Objective(T value) {
        if (value == null) {
            throw new IllegalArgumentException("The objective value cannot be null.");
        }
        this.value = value;
    }

    public double doubleValue() {
        return value.doubleValue();
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean isBetterThan(Quality other) {
        return isBetterThan((Objective<T>) other);
    }

    public boolean isBetterThan(Objective<T> other) {
        return isBetterThan(other.value);
    }

    public abstract boolean isBetterThan(T otherValue);

    @SuppressWarnings("unchecked")
    @Override
    public boolean isEquivalentTo(Quality other) {
        return isEquivalentTo((Objective<T>) other);
    }

    public boolean isEquivalentTo(Objective<T> other) {
        return isEquivalentTo(other.value);
    }

    public boolean isEquivalentTo(T otherValue) {
        return value.equals(otherValue);
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Objective) {
            return isEquivalentTo((Objective<T>) obj);
        } else if (obj instanceof Number) {
            return isEquivalentTo((T) obj);
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return value.toString();
    }
}