package com.vanolucas.opti.solution;

import com.vanolucas.opti.quality.Quality;

import java.util.function.UnaryOperator;

public class EvaluatedSolution implements Solution, Comparable<EvaluatedSolution> {
    private final Solution solution;
    private final Quality quality;

    public EvaluatedSolution(Solution solution, Quality quality) {
        this.solution = solution;
        this.quality = quality;
    }

    public Quality getQuality() {
        return quality;
    }

    public boolean isBetterThan(EvaluatedSolution other) {
        return other == null || quality.isBetterThan(other.quality);
    }

    public boolean isBetterOrEquivalentTo(EvaluatedSolution other) {
        return other == null || quality.isBetterOrEquivalentTo(other.quality);
    }

    @Override
    public <TMostEncodedRepresentation> Solution cloneMostEncodedRepresentationUsing(UnaryOperator<TMostEncodedRepresentation> mostEncodedRepresentationCloner) {
        return solution.cloneMostEncodedRepresentationUsing(mostEncodedRepresentationCloner);
    }

    @Override
    public <TMostEncodedRepresentation> Solution mutateMostEncodedRepresentationUsing(UnaryOperator<TMostEncodedRepresentation> mostEncodedRepresentationMutator) {
        return solution.mutateMostEncodedRepresentationUsing(mostEncodedRepresentationMutator);
    }

    public static EvaluatedSolution getBestOrElseFirst(EvaluatedSolution solution1,
                                                       EvaluatedSolution solution2) {
        if (solution1 == null) {
            return solution2;
        }
        return solution1.isBetterOrEquivalentTo(solution2) ? solution1 : solution2;
    }

    @Override
    public int compareTo(EvaluatedSolution other) {
        return quality.compareTo(other.quality);
    }

    @Override
    public String toString() {
        return String.format("%s%n%s", solution, quality);
    }
}
