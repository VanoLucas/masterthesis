package com.vanolucas.opti.solution;

import java.util.function.UnaryOperator;

public interface Solution {
    <TMostEncodedRepresentation> Solution cloneMostEncodedRepresentationUsing(UnaryOperator<TMostEncodedRepresentation> mostEncodedRepresentationCloner);

    <TMostEncodedRepresentation> Solution mutateMostEncodedRepresentationUsing(UnaryOperator<TMostEncodedRepresentation> mostEncodedRepresentationMutator);
}
