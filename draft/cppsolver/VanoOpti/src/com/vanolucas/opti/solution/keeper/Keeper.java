package com.vanolucas.opti.solution.keeper;

import com.vanolucas.opti.solution.EvaluatedSolution;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public abstract class Keeper {
    public enum Decision {
        KEPT, REJECTED
    }

    private final boolean printKeptSolutions;
    private Instant instantLastKept;
    private final List<Consumer<EvaluatedSolution>> onCandidateKeptListeners = new ArrayList<>(2);

    public Keeper() {
        this(false);
    }

    public Keeper(boolean printKeptSolutions) {
        this.printKeptSolutions = printKeptSolutions;
    }

    public Decision submit(EvaluatedSolution candidate) {
        Decision decision = makeDecision(candidate);
        switch (decision) {
            case KEPT:
                keep(candidate);
                instantLastKept = Instant.now();
                if (printKeptSolutions) System.out.println(String.format("Solution kept:%n%s", candidate));
                onCandidateKeptListeners.forEach(l -> l.accept(candidate));
                break;
            case REJECTED:
            default:
                reject(candidate);
                break;
        }
        return decision;
    }

    protected abstract Decision makeDecision(EvaluatedSolution candidate);

    protected void keep(EvaluatedSolution keptSolution) {
    }

    protected void reject(EvaluatedSolution rejectedSolution) {
    }

    public void reset() {
        instantLastKept = null;
    }

    public boolean isDurationSinceLastKeptMoreThan(Duration duration) {
        if (instantLastKept == null) {
            return false;
        }
        return instantLastKept.plus(duration).isBefore(Instant.now());
    }

    public void addOnCandidateKeptListener(Consumer<EvaluatedSolution> listener) {
        onCandidateKeptListeners.add(listener);
    }
}
