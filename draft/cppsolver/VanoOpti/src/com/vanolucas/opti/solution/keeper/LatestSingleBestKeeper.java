package com.vanolucas.opti.solution.keeper;

import com.vanolucas.opti.solution.EvaluatedSolution;

public class LatestSingleBestKeeper extends SingleBestKeeper {
    public LatestSingleBestKeeper() {
        super();
    }

    public LatestSingleBestKeeper(boolean printKeptSolutions) {
        super(printKeptSolutions);
    }

    @Override
    protected boolean shouldReplaceBestWith(EvaluatedSolution candidate) {
        return candidate.isBetterOrEquivalentTo(getBest());
    }
}
