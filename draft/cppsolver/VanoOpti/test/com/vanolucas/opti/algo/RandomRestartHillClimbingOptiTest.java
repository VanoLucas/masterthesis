package com.vanolucas.opti.algo;

import com.vanolucas.opti.clone.Cloneable;
import com.vanolucas.opti.clone.MostEncodedRepresentationCloner;
import com.vanolucas.opti.eval.Evaluator;
import com.vanolucas.opti.eval.OneIntermediateGeneticEvaluator;
import com.vanolucas.opti.mutation.MostEncodedRepresentationMutator;
import com.vanolucas.opti.mutation.neighborhood.CloneThenMutateNeighborProducer;
import com.vanolucas.opti.mutation.neighborhood.NeighborProducer;
import com.vanolucas.opti.mutation.neighborhood.NeighborsProducer;
import com.vanolucas.opti.problem.targetnb.FourInt;
import com.vanolucas.opti.problem.targetnb.FourIntDecoder;
import com.vanolucas.opti.problem.targetnb.IntEvaluator;
import com.vanolucas.opti.problem.targetnb.TwoIntDecoder;
import com.vanolucas.opti.solution.GeneticSolution;
import com.vanolucas.opti.solution.Solution;
import com.vanolucas.opti.solution.keeper.EarliestSingleBestKeeper;
import com.vanolucas.opti.solution.keeper.SingleBestKeeper;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.function.BooleanSupplier;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

class RandomRestartHillClimbingOptiTest {

    @Test
    void runStep() {
        // target number instance
        final int target = 100;

        // initial solution
        FourInt initialGenome = new FourInt(0, 0, 0, 0);
        Solution initialSolution = new GeneticSolution(initialGenome);

        // neighboring operators
        List<UnaryOperator<FourInt>> genomeMutators = Arrays.asList(
                FourInt::incrementLeft1,
                FourInt::incrementLeft2,
                FourInt::decrementLeft1,
                FourInt::decrementLeft2,
                FourInt::incrementRight1,
                FourInt::incrementRight2,
                FourInt::decrementRight1,
                FourInt::decrementRight2
        );
        List<NeighborProducer> neighborProducers = genomeMutators.stream()
                .map(genomeMutator -> new CloneThenMutateNeighborProducer(
                        new MostEncodedRepresentationCloner(Cloneable.cloner()),
                        new MostEncodedRepresentationMutator(genomeMutator)
                ))
                .collect(Collectors.toList());
        NeighborsProducer neighborsProducer = new NeighborsProducer(neighborProducers);

        // evaluator
        IntEvaluator intEvaluator = new IntEvaluator(target);
        Evaluator evaluator = new OneIntermediateGeneticEvaluator<>(
                FourIntDecoder::decode,
                TwoIntDecoder::decode,
                intEvaluator::evaluate
        );

        // best solution keeper
        SingleBestKeeper bestKeeper = new EarliestSingleBestKeeper();
        SingleBestKeeper innerHillClimbingBestKeeper = new EarliestSingleBestKeeper();

        // hill climbing opti algo
        HillClimbingOpti hillClimbing = new HillClimbingOpti(
                initialSolution, neighborsProducer, evaluator,
                HillClimbing.Variant.STEEPEST_ASCENT,
                Arrays.asList(bestKeeper::submit, innerHillClimbingBestKeeper::submit)
        );

        // random restart
        final Duration maxDurationWithoutImprovement = Duration.ofSeconds(5L);

        Supplier<FourInt> randomGenomeSupplier = () -> new FourInt(123, 123, 123, 123);
        Supplier<Solution> randomSolutionSupplier = () -> new GeneticSolution(randomGenomeSupplier.get());

        BooleanSupplier randomRestartDecider = () -> innerHillClimbingBestKeeper.isDurationSinceLastKeptMoreThan(maxDurationWithoutImprovement);

        RandomRestartHillClimbingOpti randomRestartHillClimbingOpti = new RandomRestartHillClimbingOpti(
                hillClimbing, randomSolutionSupplier, randomRestartDecider, innerHillClimbingBestKeeper::reset
        );

        // opti runner
        OptiRunner optiRunner = new OptiRunner(randomRestartHillClimbingOpti);

        // run
        optiRunner.runForAtLeast(Duration.ofSeconds(15L));

        // print stats
        bestKeeper.printBest();
    }
}