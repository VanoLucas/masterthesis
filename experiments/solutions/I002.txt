New better solution at iteration 1619021:
Genome:
(8 genes) 0.13488446467285164 0.3333544384411384 | 0.336302591509816 0.5000191668305981 | 0.14785365572282597 0.5000285391075395 | 0.7450599345871087 0.5950457454859365
Intermediate:
(8 slots) 1:9000 | 1:9000 | 2:8925 | 3:8500 | 0:6667 | 0:6667 | 0:6666 | 2:6075
Phenotype:
Offset plates:
|  2|  2|  3|  4|	  9000 printings
|  1|  1|  1|  3|	  6667 printings
Printings per book cover:
  1: 20001 printings
  2: 18000 printings
  3: 15667 printings
  4: 9000 printings
Total printings: 15667
Quality: 247916.47999999998


Optimal is: 15667