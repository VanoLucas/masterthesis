
Genome SP:

New better solution at iteration 584847:
Genome:
(70 genes) 0.6381264009670333;0.8244179119357569;0.551982035674258;0.4581378291699618;0.7863053761150294;0.4267196796132458;0.8201625659914846;0.8261663028814624;0.06256169985729935;0.08710725800531335;0.4072463838779521;0.5341558708964802;0.6674206991575287;0.12913304974364798;0.16971735668385748;0.7157212669085304;0.5770313227607603;0.5743776682500898;0.3390908786786093;0.3658539857145884;0.03835804065865379;0.8377058786397216;0.4558518945929638;0.44291210846293905;0.2762597872430468;0.245044437234775;0.39199605778214813;0.7450291549231771;0.3144917709376559;0.035644254409248455;0.40650517358640775;0.4066050898709538;0.9067147886861591;0.03664729473337156;0.4887140993718355;0.02072876908592415;0.1301033384203184;0.24920500722618713;0.7032584472946564;0.9546969989600889;0.608173319661541;0.694341280949454;0.07230531109979058;0.05615497608005883;0.9999999999999999;0.44439803039711856;0.06115160925201049;0.9999999999999999;0.7357161198109498;0.30924083429038185;0.7275972140094766;0.5852742350519774;0.19421217636346022;0.9043492328300361;0.38067000188954603;0.07512047268707665;0.3165964615102633;0.6568670801795421;0.7529420447135724;0.9282402388029689;0.9280820348872652;0.6539406294793451;0.5259134556198232;0.1595349991637477;0.9104332555450001;0.14555089803780408;0.2844537848500449;0.3668034404971521;0.34948461886027943;0.7533444127110754
Intermediate:
(40 slots) 2:27000 | 1:26912 | 3:26000 | 4:26000 | 8:20000 | 9:20000 | 7:19925 | 10:19000 | 12:17000 | 5:16982 | 11:16963 | 13:16000 | 0:15002 | 14:15000 | 15:15000 | 0:14998 | 16:14000 | 17:13500 | 6:13439 | 18:13000 | 22:9000 | 23:9000 | 19:8891 | 6:8561 | 24:6382 | 5:6018 | 25:6000 | 20:5608 | 21:5000 | 21:5000 | 26:5000 | 20:4892 | 27:2500 | 19:2109 | 7:2075 | 28:1500 | 24:1118 | 1:1088 | 11:1037 | 29:1000
Phenotype:
Offset plates:
|  2|  3|  4|  5|	 27000 printings
|  8|  9| 10| 11|	 20000 printings
|  6| 12| 13| 14|	 17000 printings
|  1|  1| 15| 16|	 15002 printings
|  7| 17| 18| 19|	 14000 printings
|  7| 20| 23| 24|	  9000 printings
|  6| 21| 25| 26|	  6382 printings
| 21| 22| 22| 27|	  5000 printings
|  8| 20| 28| 29|	  2500 printings
|  2| 12| 25| 30|	  1118 printings
Printings per book cover:
  1: 30004 printings
  2: 28118 printings
  3: 27000 printings
  4: 27000 printings
  5: 27000 printings
  6: 23382 printings
  7: 23000 printings
  8: 22500 printings
  9: 20000 printings
 10: 20000 printings
 11: 20000 printings
 12: 18118 printings
 13: 17000 printings
 14: 17000 printings
 15: 15002 printings
 16: 15002 printings
 17: 14000 printings
 18: 14000 printings
 19: 14000 printings
 20: 11500 printings
 21: 11382 printings
 22: 10000 printings
 23: 9000 printings
 24: 9000 printings
 25: 7500 printings
 26: 6382 printings
 27: 5000 printings
 28: 2500 printings
 29: 2500 printings
 30: 1118 printings
Total printings: 117002
Quality: 1759266.88

Genome CF:

New better solution at iteration 273878:
Genome:
(20 genes) 0.060335038744806814 0.0896043119378815 | 0.41581577360967065 0.8791278681183987 | 0.8539054639840422 0.26267297788326727 | 0.574172152871611 0.6756767242078392 | 0.1853515893225395 0.9316485132551405 | 0.09812327689911175 0.18521830397546454 | 0.43413152588722875 0.6822908108202868 | 0.8032867838578986 0.3385528730442364 | 0.6090910112937168 0.30280981623792563 | 0.0 0.8651512286106919
Intermediate:
(40 slots) 3:26000 | 4:26000 | 0:25954 | 1:25492 | 2:22000 | 6:22000 | 7:22000 | 5:21427 | 8:20000 | 9:20000 | 10:19000 | 11:18000 | 14:15000 | 15:15000 | 12:14945 | 16:14000 | 19:11000 | 13:10916 | 20:10500 | 21:10000 | 17:9121 | 18:9064 | 22:9000 | 23:9000 | 13:5084 | 26:5000 | 2:5000 | 24:4961 | 25:4424 | 17:4379 | 0:4046 | 18:3936 | 24:2539 | 1:2508 | 27:2500 | 12:2055 | 25:1576 | 5:1573 | 28:1500 | 29:1000
Phenotype:
Offset plates:
|  1|  2|  4|  5|	 26000 printings
|  3|  6|  7|  8|	 22000 printings
|  9| 10| 11| 12|	 20000 printings
| 13| 15| 16| 17|	 15000 printings
| 14| 20| 21| 22|	 11000 printings
| 18| 19| 23| 24|	  9121 printings
|  3| 14| 25| 27|	  5084 printings
|  1| 18| 19| 26|	  4424 printings
|  2| 13| 25| 28|	  2539 printings
|  6| 26| 29| 30|	  1576 printings
Printings per book cover:
  1: 30424 printings
  2: 28539 printings
  3: 27084 printings
  4: 26000 printings
  5: 26000 printings
  6: 23576 printings
  7: 22000 printings
  8: 22000 printings
  9: 20000 printings
 10: 20000 printings
 11: 20000 printings
 12: 20000 printings
 13: 17539 printings
 14: 16084 printings
 15: 15000 printings
 16: 15000 printings
 17: 15000 printings
 18: 13545 printings
 19: 13545 printings
 20: 11000 printings
 21: 11000 printings
 22: 11000 printings
 23: 9121 printings
 24: 9121 printings
 25: 7623 printings
 26: 6000 printings
 27: 5084 printings
 28: 2539 printings
 29: 1576 printings
 30: 1576 printings
Total printings: 116744
Quality: 1755799.3599999999


New best:
|   1 |   3 |   4 |   5 | :  26000 printings
|   2 |   6 |   7 |   8 | :  22005 printings
|   9 |  10 |  11 |  12 | :  20000 printings
|  14 |  15 |  16 |  17 | :  15000 printings
|  13 |  20 |  21 |  22 | :  11000 printings
|  18 |  19 |  23 |  24 | :   9000 printings
|   2 |  13 |  25 |  26 | :   6044 printings
|   1 |  18 |  19 |  27 | :   5000 printings
|   3 |  25 |  28 |  29 | :   1500 printings
|   6 |  14 |  28 |  30 | :   1007 printings
116556.0
At iteration: 32935
Total iterations: 132368
Run duration: PT14.969S
Iteration: 132368
Total run duration: PT14.883S

New best:
|   2 |   6 |   7 |   8 | :  22030 printings
|   1 |   9 |  10 |  11 | :  20000 printings
|   4 |   5 |  12 |  13 | :  17021 printings
|  14 |  15 |  16 |  17 | :  15000 printings
|   3 |   3 |  18 |  19 | :  13517 printings
|   1 |  20 |  21 |  22 | :  11000 printings
|   4 |   5 |  23 |  24 | :   9000 printings
|   2 |  25 |  26 |  27 | :   6072 printings
|   6 |  25 |  28 |  29 | :   1500 printings
|  12 |  14 |  28 |  30 | :   1131 printings
116271.0



Experiment: Test experiment
4 runs of duration 15s
Overall best:
Encoded representation 1 (GenomeCf):
(20 genes) 0.05901693161821986 ; 0.21277078945976746 ; 0.19322478712510804 ; 0.9528343985219131 ; 0.0985077452070795 ; 0.5926088619230226 ; 0.9207976256190418 ; 0.21177726592560675 ; 0.12253176061722382 ; 0.4217162783724875 ; 0.7307296057966365 ; 0.09883349854824808 ; 0.14662754388985488 ; 0.23148398790787184 ; 0.3897714766974 ; 0.889080176574843 ; 0.014754087424994955 ; 0.46504365919234825 ; 0.4316612214086677 ; 0.8840079533259272
Encoded representation 2 (Slots):
 22043 copies /  28000 of book cover   2
 22000 copies /  22000 of book cover   7
 22000 copies /  22000 of book cover   8
 21915 copies /  23000 of book cover   6
 20000 copies /  20000 of book cover   9
 20000 copies /  20000 of book cover  10
 19982 copies /  26000 of book cover   5
 19000 copies /  19000 of book cover  11
 16049 copies /  30000 of book cover   1
 16003 copies /  18000 of book cover  12
 16000 copies /  16000 of book cover  14
 16000 copies /  27000 of book cover   3
 15036 copies /  26000 of book cover   4
 15028 copies /  17000 of book cover  13
 15000 copies /  15000 of book cover  15
 15000 copies /  15000 of book cover  16
 14000 copies /  14000 of book cover  17
 13951 copies /  30000 of book cover   1
 13500 copies /  13500 of book cover  18
 13000 copies /  13000 of book cover  19
 11000 copies /  27000 of book cover   3
 11000 copies /  11000 of book cover  20
 10964 copies /  26000 of book cover   4
 10500 copies /  10500 of book cover  21
  9012 copies /  10000 of book cover  22
  9000 copies /   9000 of book cover  23
  9000 copies /   9000 of book cover  24
  7500 copies /   7500 of book cover  25
  6018 copies /  26000 of book cover   5
  6000 copies /   6000 of book cover  26
  5957 copies /  28000 of book cover   2
  5000 copies /   5000 of book cover  27
  1997 copies /  18000 of book cover  12
  1972 copies /  17000 of book cover  13
  1971 copies /   2500 of book cover  28
  1500 copies /   1500 of book cover  29
  1085 copies /  23000 of book cover   6
  1000 copies /   1000 of book cover  30
   988 copies /  10000 of book cover  22
   529 copies /   2500 of book cover  28
Phenotype (CppSolution):
Demand fulfillment:
  1:  30049 /  30000 (waste: 0.163 %)
  2:  28061 /  28000 (waste: 0.217 %)
  3:  27049 /  27000 (waste: 0.181 %)
  4:  26036 /  26000 (waste: 0.138 %)
  5:  26018 /  26000 (waste: 0.069 %)
  6:  23128 /  23000 (waste: 0.553 %)
  7:  22043 /  22000 (waste: 0.195 %)
  8:  22043 /  22000 (waste: 0.195 %)
  9:  20000 /  20000 (waste: 0.000 %)
 10:  20000 /  20000 (waste: 0.000 %)
 11:  20000 /  19000 (waste: 5.000 %)
 12:  18046 /  18000 (waste: 0.255 %)
 13:  17033 /  17000 (waste: 0.194 %)
 14:  16049 /  16000 (waste: 0.305 %)
 15:  15036 /  15000 (waste: 0.239 %)
 16:  15036 /  15000 (waste: 0.239 %)
 17:  14000 /  14000 (waste: 0.000 %)
 18:  14000 /  13500 (waste: 3.571 %)
 19:  14000 /  13000 (waste: 7.143 %)
 20:  11000 /  11000 (waste: 0.000 %)
 21:  11000 /  10500 (waste: 4.545 %)
 22:  10097 /  10000 (waste: 0.961 %)
 23:   9012 /   9000 (waste: 0.133 %)
 24:   9012 /   9000 (waste: 0.133 %)
 25:   9012 /   7500 (waste: 16.778 %)
 26:   6018 /   6000 (waste: 0.299 %)
 27:   6018 /   5000 (waste: 16.916 %)
 28:   3082 /   2500 (waste: 18.884 %)
 29:   1997 /   1500 (waste: 24.887 %)
 30:   1085 /   1000 (waste: 7.834 %)
Plates configuration:
|   2 |   6 |   7 |   8 | :  22043 printings
|   5 |   9 |  10 |  11 | :  20000 printings
|   1 |   3 |  12 |  14 | :  16049 printings
|   4 |  13 |  15 |  16 | :  15036 printings
|   1 |  17 |  18 |  19 | :  14000 printings
|   3 |   4 |  20 |  21 | :  11000 printings
|  22 |  23 |  24 |  25 | :   9012 printings
|   2 |   5 |  26 |  27 | :   6018 printings
|  12 |  13 |  28 |  29 | :   1997 printings
|   6 |  22 |  28 |  30 | :   1085 printings
Total printings: 116240
Total waste: 1.6306 %
Quality: 1749025.5999999999



Experiment: CF genome
10 runs of duration 15s
Overall best:
Encoded representation 1 (GenomeCf):
(20 genes) 0.16454826404220946 ; 0.6536073394318371 ; 0.1317433090498068 ; 0.23105637073702145 ; 0.015495373845669569 ; 0.27657335978524433 ; 0.17020414748347568 ; 0.9559065537166282 ; 0.07341584090173625 ; 0.5000783721633059 ; 0.4494194112017401 ; 0.914089429200852 ; 0.05779344257276009 ; 0.3927910131958089 ; 0.921961232513194 ; 0.6598566619167192 ; 0.3768216998470225 ; 0.05577338905724116 ; 0.8179064168955904 ; 0.7790076785381989
Encoded representation 2 (Slots):
 22000 copies /  22000 of book cover   7
 22000 copies /  22000 of book cover   8
 21985 copies /  23000 of book cover   6
 21703 copies /  30000 of book cover   1
 20000 copies /  20000 of book cover   9
 20000 copies /  20000 of book cover  10
 19993 copies /  26000 of book cover   4
 19000 copies /  19000 of book cover  11
 17002 copies /  28000 of book cover   2
 17000 copies /  17000 of book cover  13
 16997 copies /  18000 of book cover  12
 16993 copies /  26000 of book cover   5
 15000 copies /  15000 of book cover  15
 15000 copies /  15000 of book cover  16
 14625 copies /  16000 of book cover  14
 14000 copies /  14000 of book cover  17
 13502 copies /  27000 of book cover   3
 13500 copies /  13500 of book cover  18
 13498 copies /  27000 of book cover   3
 13000 copies /  13000 of book cover  19
 11000 copies /  11000 of book cover  20
 10998 copies /  28000 of book cover   2
 10500 copies /  10500 of book cover  21
 10000 copies /  10000 of book cover  22
  9007 copies /  26000 of book cover   5
  9000 copies /   9000 of book cover  23
  9000 copies /   9000 of book cover  24
  8297 copies /  30000 of book cover   1
  6007 copies /  26000 of book cover   4
  6000 copies /   6000 of book cover  26
  5842 copies /   7500 of book cover  25
  5000 copies /   5000 of book cover  27
  1658 copies /   7500 of book cover  25
  1649 copies /   2500 of book cover  28
  1500 copies /   1500 of book cover  29
  1375 copies /  16000 of book cover  14
  1015 copies /  23000 of book cover   6
  1003 copies /  18000 of book cover  12
  1000 copies /   1000 of book cover  30
   851 copies /   2500 of book cover  28
Phenotype (CppSolution):
|   1 |   6 |   7 |   8 | :  22000 printings
|   4 |   9 |  10 |  11 | :  20000 printings
|   2 |   5 |  12 |  13 | :  17002 printings
|  14 |  15 |  16 |  17 | :  15000 printings
|   3 |   3 |  18 |  19 | :  13502 printings
|   2 |  20 |  21 |  22 | :  11000 printings
|   1 |   5 |  23 |  24 | :   9007 printings
|   4 |  25 |  26 |  27 | :   6007 printings
|  14 |  25 |  28 |  29 | :   1658 printings
|   6 |  12 |  28 |  30 | :   1015 printings
116191.0





4 runs of duration 15s
Overall best:
Encoded representation 1 (GenomeCf):
(20 genes) 0.3931833435237898 ; 0.10815060323542702 ; 0.0981388177285657 ; 0.996918359740729 ; 0.06194101316889655 ; 0.7903180728480315 ; 0.19246592489388392 ; 0.0882039721847322 ; 0.1261791436853802 ; 0.6895271574506183 ; 0.9211954709906293 ; 0.7986973653284817 ; 0.15447199125921462 ; 0.4630818091606069 ; 0.013384209767527078 ; 0.5976120838200184 ; 0.46080703054715144 ; 0.16163556130489298 ; 0.8226596255351423 ; 0.39832238148804366
Encoded representation 2 (Slots):
 22000 copies /  22000 of book cover   7
 22000 copies /  22000 of book cover   8
 21986 copies /  23000 of book cover   6
 21036 copies /  30000 of book cover   1
 20000 copies /  20000 of book cover   9
 20000 copies /  20000 of book cover  10
 19980 copies /  26000 of book cover   5
 19000 copies /  19000 of book cover  11
 17037 copies /  26000 of book cover   4
 17027 copies /  18000 of book cover  12
 17000 copies /  17000 of book cover  13
 16936 copies /  28000 of book cover   2
 15000 copies /  15000 of book cover  15
 15000 copies /  15000 of book cover  16
 14707 copies /  16000 of book cover  14
 14000 copies /  14000 of book cover  17
 13542 copies /  27000 of book cover   3
 13500 copies /  13500 of book cover  18
 13458 copies /  27000 of book cover   3
 13000 copies /  13000 of book cover  19
 11064 copies /  28000 of book cover   2
 11000 copies /  11000 of book cover  20
 10500 copies /  10500 of book cover  21
 10000 copies /  10000 of book cover  22
  9000 copies /   9000 of book cover  23
  9000 copies /   9000 of book cover  24
  8964 copies /  30000 of book cover   1
  8963 copies /  26000 of book cover   4
  6020 copies /  26000 of book cover   5
  6007 copies /   7500 of book cover  25
  6000 copies /   6000 of book cover  26
  5000 copies /   5000 of book cover  27
  1502 copies /   2500 of book cover  28
  1500 copies /   1500 of book cover  29
  1493 copies /   7500 of book cover  25
  1293 copies /  16000 of book cover  14
  1014 copies /  23000 of book cover   6
  1000 copies /   1000 of book cover  30
   998 copies /   2500 of book cover  28
   973 copies /  18000 of book cover  12
Phenotype (CppSolution):
Demand fulfillment:
  1:  31000 /  30000 (waste: 3.226 %)
  2:  28101 /  28000 (waste: 0.359 %)
  3:  27084 /  27000 (waste: 0.310 %)
  4:  26037 /  26000 (waste: 0.142 %)
  5:  26020 /  26000 (waste: 0.077 %)
  6:  23014 /  23000 (waste: 0.061 %)
  7:  22000 /  22000 (waste: 0.000 %)
  8:  22000 /  22000 (waste: 0.000 %)
  9:  20000 /  20000 (waste: 0.000 %)
 10:  20000 /  20000 (waste: 0.000 %)
 11:  20000 /  19000 (waste: 5.000 %)
 12:  18051 /  18000 (waste: 0.283 %)
 13:  17037 /  17000 (waste: 0.217 %)
 14:  16502 /  16000 (waste: 3.042 %)
 15:  15000 /  15000 (waste: 0.000 %)
 16:  15000 /  15000 (waste: 0.000 %)
 17:  15000 /  14000 (waste: 6.667 %)
 18:  13542 /  13500 (waste: 0.310 %)
 19:  13542 /  13000 (waste: 4.002 %)
 20:  11064 /  11000 (waste: 0.578 %)
 21:  11064 /  10500 (waste: 5.098 %)
 22:  11064 /  10000 (waste: 9.617 %)
 23:   9000 /   9000 (waste: 0.000 %)
 24:   9000 /   9000 (waste: 0.000 %)
 25:   7522 /   7500 (waste: 0.292 %)
 26:   6020 /   6000 (waste: 0.332 %)
 27:   6020 /   5000 (waste: 16.944 %)
 28:   2516 /   2500 (waste: 0.636 %)
 29:   1502 /   1500 (waste: 0.133 %)
 30:   1014 /   1000 (waste: 1.381 %)
Plates configuration:
|   1 |   6 |   7 |   8 | :  22000 printings
|   5 |   9 |  10 |  11 | :  20000 printings
|   2 |   4 |  12 |  13 | :  17037 printings
|  14 |  15 |  16 |  17 | :  15000 printings
|   3 |   3 |  18 |  19 | :  13542 printings
|   2 |  20 |  21 |  22 | :  11064 printings
|   1 |   4 |  23 |  24 | :   9000 printings
|   5 |  25 |  26 |  27 | :   6020 printings
|  14 |  25 |  28 |  29 | :   1502 printings
|   6 |  12 |  28 |  30 | :   1014 printings
Feasible solution: true
Total printings: 116179
Total waste: 1.5773 %
Quality: 116179.0
Best qualities:
116179.0 116631.0 116688.0 116831.0
Average quality: 116582.250000
Median quality: 116659.500000




Experiment 'Test experiment', run   1 [start]
New best:
Encoded representation 1 (GenomeCf):
(20 genes) 0.40419167174796955 ; 0.23827936515837322 ; 0.07307467171995352 ; 0.8173914842925858 ; 0.36811396691448584 ; 0.22149605073692635 ; 0.028709594911551006 ; 0.932702366552352 ; 0.06485543071419542 ; 0.4326921371184663 ; 0.1995391149949971 ; 0.08586067932670594 ; 0.14825300483952353 ; 0.8474039412160842 ; 0.7131932826858577 ; 0.20158202254767466 ; 0.9157270600597411 ; 0.7947502854190197 ; 0.11622095682435085 ; 0.4591663412154873
Encoded representation 2 (Slots):
 22013 copies /  23000 of book cover   6
 22000 copies /  22000 of book cover   7
 22000 copies /  22000 of book cover   8
 21943 copies /  28000 of book cover   2
 20031 copies /  26000 of book cover   4
 20000 copies /  20000 of book cover   9
 20000 copies /  20000 of book cover  10
 19000 copies /  19000 of book cover  11
 16010 copies /  30000 of book cover   1
 16007 copies /  18000 of book cover  12
 16000 copies /  16000 of book cover  14
 15966 copies /  27000 of book cover   3
 15000 copies /  15000 of book cover  15
 15000 copies /  15000 of book cover  16
 14984 copies /  26000 of book cover   5
 14975 copies /  17000 of book cover  13
 14000 copies /  14000 of book cover  17
 13990 copies /  30000 of book cover   1
 13500 copies /  13500 of book cover  18
 13000 copies /  13000 of book cover  19
 11034 copies /  27000 of book cover   3
 11016 copies /  26000 of book cover   5
 11000 copies /  11000 of book cover  20
 10500 copies /  10500 of book cover  21
  9000 copies /   9000 of book cover  23
  9000 copies /   9000 of book cover  24
  8993 copies /  10000 of book cover  22
  7500 copies /   7500 of book cover  25
  6057 copies /  28000 of book cover   2
  6000 copies /   6000 of book cover  26
  5969 copies /  26000 of book cover   4
  5000 copies /   5000 of book cover  27
  2025 copies /  17000 of book cover  13
  1993 copies /  18000 of book cover  12
  1507 copies /   2500 of book cover  28
  1500 copies /   1500 of book cover  29
  1007 copies /  10000 of book cover  22
  1000 copies /   1000 of book cover  30
   993 copies /   2500 of book cover  28
   987 copies /  23000 of book cover   6
Phenotype (CppSolution):
Demand fulfillment:
  1:  30010 /  30000 (waste: 0.033 %)
  2:  28070 /  28000 (waste: 0.249 %)
  3:  27044 /  27000 (waste: 0.163 %)
  4:  26088 /  26000 (waste: 0.337 %)
  5:  26034 /  26000 (waste: 0.131 %)
  6:  23020 /  23000 (waste: 0.087 %)
  7:  22013 /  22000 (waste: 0.059 %)
  8:  22013 /  22000 (waste: 0.059 %)
  9:  20031 /  20000 (waste: 0.155 %)
 10:  20031 /  20000 (waste: 0.155 %)
 11:  20031 /  19000 (waste: 5.147 %)
 12:  18035 /  18000 (waste: 0.194 %)
 13:  17025 /  17000 (waste: 0.147 %)
 14:  16010 /  16000 (waste: 0.062 %)
 15:  15000 /  15000 (waste: 0.000 %)
 16:  15000 /  15000 (waste: 0.000 %)
 17:  14000 /  14000 (waste: 0.000 %)
 18:  14000 /  13500 (waste: 3.571 %)
 19:  14000 /  13000 (waste: 7.143 %)
 20:  11034 /  11000 (waste: 0.308 %)
 21:  11034 /  10500 (waste: 4.840 %)
 22:  10007 /  10000 (waste: 0.070 %)
 23:   9000 /   9000 (waste: 0.000 %)
 24:   9000 /   9000 (waste: 0.000 %)
 25:   9000 /   7500 (waste: 16.667 %)
 26:   6057 /   6000 (waste: 0.941 %)
 27:   6057 /   5000 (waste: 17.451 %)
 28:   3032 /   2500 (waste: 17.546 %)
 29:   2025 /   1500 (waste: 25.926 %)
 30:   1007 /   1000 (waste: 0.695 %)
Plates configuration:
|   2 |   6 |   7 |   8 | :  22013 printings
|   4 |   9 |  10 |  11 | :  20031 printings
|   1 |   3 |  12 |  14 | :  16010 printings
|   5 |  13 |  15 |  16 | :  15000 printings
|   1 |  17 |  18 |  19 | :  14000 printings
|   3 |   5 |  20 |  21 | :  11034 printings
|  22 |  23 |  24 |  25 | :   9000 printings
|   2 |   4 |  26 |  27 | :   6057 printings
|  12 |  13 |  28 |  29 | :   2025 printings
|   6 |  22 |  28 |  30 | :   1007 printings
Feasible solution: true
Total printings: 116177
Total waste: 1.5755 %
Quality: 1748178.88


Encoded representation 1 (GenomeCf):
(20 keys) 0.7120933836300883 ; 0.21110480352751482 ; 0.1506269656832887 ; 0.46259289844247065 ; 0.17132210888418076 ; 0.09184084281032834 ; 0.11711755334356955 ; 0.8479080876910291 ; 0.06802033177106581 ; 0.8184561812413382 ; 0.03021425177136905 ; 0.9323450590538491 ; 0.4166900059458222 ; 0.23381259480001004 ; 0.9326578917028212 ; 0.7431387037627003 ; 0.3855701092111019 ; 0.21941961656722697 ; 0.04765121630303504 ; 0.4268337245633025
Encoded representation 2 (Slots):
 22025 copies /  28000 of book cover   2
 22000 copies /  22000 of book cover   7
 22000 copies /  22000 of book cover   8
 21944 copies /  23000 of book cover   6
 20000 copies /  20000 of book cover   9
 20000 copies /  20000 of book cover  10
 19987 copies /  26000 of book cover   5
 19000 copies /  19000 of book cover  11
 16026 copies /  18000 of book cover  12
 16015 copies /  30000 of book cover   1
 16000 copies /  16000 of book cover  14
 15951 copies /  27000 of book cover   3
 15013 copies /  17000 of book cover  13
 15000 copies /  15000 of book cover  15
 15000 copies /  15000 of book cover  16
 14978 copies /  26000 of book cover   4
 14000 copies /  14000 of book cover  17
 13985 copies /  30000 of book cover   1
 13500 copies /  13500 of book cover  18
 13000 copies /  13000 of book cover  19
 11049 copies /  27000 of book cover   3
 11022 copies /  26000 of book cover   4
 11000 copies /  11000 of book cover  20
 10500 copies /  10500 of book cover  21
  9000 copies /   9000 of book cover  23
  9000 copies /   9000 of book cover  24
  8945 copies /  10000 of book cover  22
  7500 copies /   7500 of book cover  25
  6013 copies /  26000 of book cover   5
  6000 copies /   6000 of book cover  26
  5975 copies /  28000 of book cover   2
  5000 copies /   5000 of book cover  27
  1987 copies /  17000 of book cover  13
  1974 copies /  18000 of book cover  12
  1572 copies /   2500 of book cover  28
  1500 copies /   1500 of book cover  29
  1056 copies /  23000 of book cover   6
  1055 copies /  10000 of book cover  22
  1000 copies /   1000 of book cover  30
   928 copies /   2500 of book cover  28
Phenotype (CppSolution):
Demand fulfillment:
  1:  30026 /  30000 (   26 extra)
  2:  28038 /  28000 (   38 extra)
  3:  27075 /  27000 (   75 extra)
  4:  26062 /  26000 (   62 extra)
  5:  26013 /  26000 (   13 extra)
  6:  23081 /  23000 (   81 extra)
  7:  22025 /  22000 (   25 extra)
  8:  22025 /  22000 (   25 extra)
  9:  20000 /  20000 (    0 extra)
 10:  20000 /  20000 (    0 extra)
 11:  20000 /  19000 ( 1000 extra)
 12:  18013 /  18000 (   13 extra)
 13:  17000 /  17000 (    0 extra)
 14:  16026 /  16000 (   26 extra)
 15:  15013 /  15000 (   13 extra)
 16:  15013 /  15000 (   13 extra)
 17:  14000 /  14000 (    0 extra)
 18:  14000 /  13500 (  500 extra)
 19:  14000 /  13000 ( 1000 extra)
 20:  11049 /  11000 (   49 extra)
 21:  11049 /  10500 (  549 extra)
 22:  10056 /  10000 (   56 extra)
 23:   9000 /   9000 (    0 extra)
 24:   9000 /   9000 (    0 extra)
 25:   9000 /   7500 ( 1500 extra)
 26:   6013 /   6000 (   13 extra)
 27:   6013 /   5000 ( 1013 extra)
 28:   3043 /   2500 (  543 extra)
 29:   1987 /   1500 (  487 extra)
 30:   1056 /   1000 (   56 extra)
Plates configuration:
|   2 |   6 |   7 |   8 | :  22025 printings
|   5 |   9 |  10 |  11 | :  20000 printings
|   1 |   3 |  12 |  14 | :  16026 printings
|   4 |  13 |  15 |  16 | :  15013 printings
|   1 |  17 |  18 |  19 | :  14000 printings
|   3 |   4 |  20 |  21 | :  11049 printings
|  22 |  23 |  24 |  25 | :   9000 printings
|   2 |   5 |  26 |  27 | :   6013 printings
|  12 |  13 |  28 |  29 | :   1987 printings
|   6 |  22 |  28 |  30 | :   1056 printings
Feasible solution: true
Total printings: 116169
Total copies: 464676 (457500 requested)
Total waste: 1.5685 %
Quality: 1748071.3599999999

