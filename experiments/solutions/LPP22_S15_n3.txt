Best = CF



m = 22 book covers
n = 3 offset plates
S = 15 slots per offset plate
C1 = cost of one offset plate = 18676.000000
C2 = cost of one printing = 13.440000
Demand:
  1: 600 copies
  2: 700 copies
  3: 2350 copies
  4: 850 copies
  5: 625 copies
  6: 800 copies
  7: 4100 copies
  8: 850 copies
  9: 800 copies
 10: 1025 copies
 11: 4050 copies
 12: 3300 copies
 13: 950 copies
 14: 1050 copies
 15: 5300 copies
 16: 3750 copies
 17: 6300 copies
 18: 6275 copies
 19: 2275 copies
 20: 3650 copies
 21: 2650 copies
 22: 4850 copies


Starting experiment with genome S_P
Experiment 'Genome S_P', run   1 [start]
Experiment: Genome S_P
1 runs of duration 2m
Overall best:
Encoded representation 1 (GenomeSp):
(67 keys) 0.20992415200737813 ; 0.9451186860266192 ; 0.016995442279933215 ; 0.056405978364772236 ; 0.8874809673276347 ; 0.13422694866116125 ; 0.04887615044376481 ; 0.10062827186775869 ; 0.08779778429026519 ; 0.22758825797554671 ; 0.169367157959476 ; 0.09842650272107889 ; 0.1508757393000617 ; 0.19026750061236164 ; 0.19166847032337564 ; 0.3091562265475597 ; 0.19788664810442347 ; 0.24149709611138548 ; 0.4823627058786122 ; 0.180707741605046 ; 0.07222353873972032 ; 0.1600720561275233 ; 0.9494057282500217 ; 0.20488624454743987 ; 0.7604369592949795 ; 0.7730484529810764 ; 0.7194840657223253 ; 0.7931606263994304 ; 0.7564286238995691 ; 0.9529870181928133 ; 0.7743360967941715 ; 0.8049024596260851 ; 0.8473389085909073 ; 0.8370144129929716 ; 0.8378972246695408 ; 0.9520983324250804 ; 0.8348327657100137 ; 0.289355763800347 ; 0.9650955570745414 ; 0.530007524012517 ; 0.026325339285563576 ; 0.09594854611026826 ; 0.6327355501961746 ; 0.23061687338567738 ; 0.7576967142586812 ; 0.49284448864682096 ; 0.022736336558313197 ; 0.06519949575171369 ; 0.3980077320360699 ; 0.3200423689254499 ; 0.04763599037814048 ; 0.6989482701487209 ; 0.9126574796704465 ; 0.15846089186311996 ; 0.6637729822523167 ; 0.4315615845027876 ; 0.4316861498692526 ; 0.2586742324630128 ; 0.25748047296244525 ; 0.6830618352907234 ; 0.6836030917648767 ; 0.7847170220297089 ; 0.27174352718889927 ; 0.9385767480600492 ; 0.4186195875673856 ; 0.71617066804829 ; 0.894612721554646
Encoded representation 2 (Slots):
  3154 copies /   4100 of book cover   7
  3154 copies /   3300 of book cover  12
  3150 copies /   6300 of book cover  17
  3150 copies /   6300 of book cover  17
  3145 copies /   6275 of book cover  18
  3130 copies /   6275 of book cover  18
  3105 copies /   4050 of book cover  11
  3027 copies /   3750 of book cover  16
  3001 copies /   5300 of book cover  15
  2830 copies /   3650 of book cover  20
  2694 copies /   4850 of book cover  22
  2650 copies /   2650 of book cover  21
  2350 copies /   2350 of book cover   3
  2299 copies /   5300 of book cover  15
  2156 copies /   4850 of book cover  22
   946 copies /   4100 of book cover   7
   945 copies /   4050 of book cover  11
   914 copies /   1050 of book cover  14
   890 copies /   1025 of book cover  10
   850 copies /    850 of book cover   4
   850 copies /    850 of book cover   8
   830 copies /   2275 of book cover  19
   820 copies /   3650 of book cover  20
   816 copies /    950 of book cover  13
   800 copies /    800 of book cover   6
   800 copies /    800 of book cover   9
   723 copies /   3750 of book cover  16
   723 copies /   2275 of book cover  19
   722 copies /   2275 of book cover  19
   494 copies /    600 of book cover   1
   146 copies /    700 of book cover   2
   146 copies /   3300 of book cover  12
   142 copies /    700 of book cover   2
   140 copies /    700 of book cover   2
   139 copies /    700 of book cover   2
   139 copies /    625 of book cover   5
   136 copies /   1050 of book cover  14
   135 copies /   1025 of book cover  10
   134 copies /    950 of book cover  13
   133 copies /    700 of book cover   2
   124 copies /    625 of book cover   5
   122 copies /    625 of book cover   5
   122 copies /    625 of book cover   5
   118 copies /    625 of book cover   5
   106 copies /    600 of book cover   1
Phenotype (CppSolution):
Demand fulfillment:
  1:   1092 /    600 (  492 extra)
  2:    730 /    700 (   30 extra)
  3:   3154 /   2350 (  804 extra)
  4:    946 /    850 (   96 extra)
  5:    730 /    625 (  105 extra)
  6:    946 /    800 (  146 extra)
  7:   4100 /   4100 (    0 extra)
  8:    946 /    850 (   96 extra)
  9:    946 /    800 (  146 extra)
 10:   1092 /   1025 (   67 extra)
 11:   4100 /   4050 (   50 extra)
 12:   3300 /   3300 (    0 extra)
 13:   1092 /    950 (  142 extra)
 14:   1092 /   1050 (   42 extra)
 15:   6308 /   5300 ( 1008 extra)
 16:   4100 /   3750 (  350 extra)
 17:   6308 /   6300 (    8 extra)
 18:   6308 /   6275 (   33 extra)
 19:   2838 /   2275 (  563 extra)
 20:   4100 /   3650 (  450 extra)
 21:   3154 /   2650 (  504 extra)
 22:   6308 /   4850 ( 1458 extra)
Plates configuration:
|   3 |   7 |  11 |  12 |  15 |  15 |  16 |  17 |  17 |  18 |  18 |  20 |  21 |  22 |  22 | :   3154 printings
|   1 |   4 |   6 |   7 |   8 |   9 |  10 |  11 |  13 |  14 |  16 |  19 |  19 |  19 |  20 | :    946 printings
|   1 |   2 |   2 |   2 |   2 |   2 |   5 |   5 |   5 |   5 |   5 |  10 |  12 |  13 |  14 | :    146 printings
Feasible solution: true
Total printings: 4246
Total copies: 63690 (57100 requested)
Total waste: 10.3470 %
Cost: 113094.23999999999
Best qualities:
Cost: 113094.23999999999
Average quality: 113094.240000
Median quality: 113094.240000

Starting experiment with genome P_PC
Experiment 'Genome P_PC', run   1 [start]
Experiment: Genome P_PC
1 runs of duration 2m
Overall best:
Encoded representation 1 (GenomePpc):
(68 keys) 0.17809060393980047 ; 0.5484676128083793 ; 0.6112159069079414 ; 0.5239687205742563 ; 0.746115656151867 ; 0.8673897021432683 ; 0.9660551749770633 ; 0.41006523449326593 ; 0.8833555078197297 ; 0.34158026102245875 ; 0.7713531156147172 ; 0.015579189072444377 ; 0.9153617057324756 ; 0.6324323540626975 ; 0.5806571532692999 ; 0.6289425110050058 ; 0.5270685386342492 ; 0.48085100736343 ; 0.13300054936782124 ; 0.2712521718995774 ; 0.9907988476721826 ; 0.49549036281525216 ; 0.762653981167612 ; 0.9568136075660888 ; 0.7961555111950399 ; 0.5993401957379174 ; 0.0531021547418673 ; 0.4656961523951636 ; 0.1495695392726134 ; 0.7248278267985023 ; 0.9935828702848956 ; 0.8820244239924709 ; 0.18132765505524429 ; 0.5266736015097 ; 0.34129862019698254 ; 0.41247263086289787 ; 0.5851595941314077 ; 0.6601326820228359 ; 0.14271273279280416 ; 0.015257735316983712 ; 0.34087569381835514 ; 0.43403043938061947 ; 0.4808597806272128 ; 0.8131763954940445 ; 0.36424307939259504 ; 0.563316158885808 ; 0.1286537768897853 ; 0.7561554658945078 ; 0.8921205802904044 ; 0.2862294070146969 ; 0.5696370985059079 ; 0.636113143838679 ; 0.9765716521755294 ; 0.28178635707237776 ; 0.2117001548062789 ; 0.48185029434748194 ; 0.412628116028696 ; 0.7630589462259877 ; 0.9582559470852329 ; 0.27987123267959757 ; 0.7128214589464187 ; 0.6046868703163814 ; 0.9797650371987143 ; 0.3046658198285519 ; 0.5721444606835075 ; 0.17499687872653513 ; 0.5663430693154519 ; 0.15092649936195335
Encoded representation 2 (Slots):
  3138 copies /   6275 of book cover  18
  3137 copies /   6275 of book cover  18
  3108 copies /   6300 of book cover  17
  3039 copies /   3300 of book cover  12
  3030 copies /   3750 of book cover  16
  3015 copies /   4050 of book cover  11
  2940 copies /   4850 of book cover  22
  2867 copies /   3650 of book cover  20
  2660 copies /   5300 of book cover  15
  2650 copies /   2650 of book cover  21
  2640 copies /   5300 of book cover  15
  2433 copies /   6300 of book cover  17
  2350 copies /   2350 of book cover   3
  2275 copies /   2275 of book cover  19
  1910 copies /   4850 of book cover  22
   850 copies /    850 of book cover   8
   842 copies /   4100 of book cover   7
   839 copies /   4100 of book cover   7
   830 copies /   4100 of book cover   7
   827 copies /   4050 of book cover  11
   823 copies /   4100 of book cover   7
   800 copies /    800 of book cover   6
   800 copies /    800 of book cover   9
   783 copies /   3650 of book cover  20
   766 copies /   4100 of book cover   7
   759 copies /   6300 of book cover  17
   720 copies /   3750 of book cover  16
   700 copies /    700 of book cover   2
   680 copies /    950 of book cover  13
   625 copies /    625 of book cover   5
   342 copies /   1025 of book cover  10
   342 copies /   1025 of book cover  10
   341 copies /   1025 of book cover  10
   333 copies /    600 of book cover   1
   308 copies /   1050 of book cover  14
   293 copies /    850 of book cover   4
   289 copies /    850 of book cover   4
   276 copies /   1050 of book cover  14
   270 copies /    950 of book cover  13
   268 copies /    850 of book cover   4
   267 copies /    600 of book cover   1
   261 copies /   3300 of book cover  12
   245 copies /   1050 of book cover  14
   221 copies /   1050 of book cover  14
   208 copies /   4050 of book cover  11
Phenotype (CppSolution):
Demand fulfillment:
  1:    684 /    600 (   84 extra)
  2:    850 /    700 (  150 extra)
  3:   3138 /   2350 (  788 extra)
  4:   1026 /    850 (  176 extra)
  5:    850 /    625 (  225 extra)
  6:    850 /    800 (   50 extra)
  7:   4250 /   4100 (  150 extra)
  8:    850 /    850 (    0 extra)
  9:    850 /    800 (   50 extra)
 10:   1026 /   1025 (    1 extra)
 11:   4330 /   4050 (  280 extra)
 12:   3480 /   3300 (  180 extra)
 13:   1192 /    950 (  242 extra)
 14:   1368 /   1050 (  318 extra)
 15:   6276 /   5300 (  976 extra)
 16:   3988 /   3750 (  238 extra)
 17:   7126 /   6300 (  826 extra)
 18:   6276 /   6275 (    1 extra)
 19:   3138 /   2275 (  863 extra)
 20:   3988 /   3650 (  338 extra)
 21:   3138 /   2650 (  488 extra)
 22:   6276 /   4850 ( 1426 extra)
Plates configuration:
|   3 |  11 |  12 |  15 |  15 |  16 |  17 |  17 |  18 |  18 |  19 |  20 |  21 |  22 |  22 | :   3138 printings
|   2 |   5 |   6 |   7 |   7 |   7 |   7 |   7 |   8 |   9 |  11 |  13 |  16 |  17 |  20 | :    850 printings
|   1 |   1 |   4 |   4 |   4 |  10 |  10 |  10 |  11 |  12 |  13 |  14 |  14 |  14 |  14 | :    342 printings
Feasible solution: true
Total printings: 4330
Total copies: 64950 (57100 requested)
Total waste: 12.0862 %
Cost: 114223.2
Best qualities:
Cost: 114223.2
Average quality: 114223.200000
Median quality: 114223.200000

Starting experiment with genome CF
Experiment 'Genome CF', run   1 [start]
Experiment: Genome CF
1 runs of duration 2m
Overall best:
Encoded representation 1 (GenomeCf):
(46 keys) 0.9991530297250355 ; 0.34807672663896927 ; 0.6471015675574933 ; 0.9555793724307121 ; 0.6211934752760881 ; 0.385660572864043 ; 0.7651652043325363 ; 0.0947073820673856 ; 0.7751588475727967 ; 0.09929472838264031 ; 0.7196738600999848 ; 0.47062793641501 ; 0.9872076502958073 ; 0.4384705531274491 ; 0.7337777391251329 ; 0.9519804849017719 ; 0.7914247451058728 ; 0.9582053272292579 ; 0.30375404239710047 ; 0.13055231176670057 ; 0.9650029596518206 ; 0.17871249068523876 ; 0.30339850207083885 ; 0.4129490875750551 ; 0.019634815899986058 ; 0.968731757584806 ; 0.5161954265337894 ; 0.4764534625083542 ; 0.7589925645654971 ; 0.19527215791673813 ; 0.569071531513178 ; 0.5599788930145885 ; 0.4889839902418057 ; 0.4337012647850653 ; 0.4873447412484827 ; 0.19230172249057742 ; 0.8850088200997516 ; 0.4588531839643518 ; 0.2953769952769021 ; 0.1258056964032881 ; 0.8145641715110743 ; 0.15837322918183194 ; 0.41424398778658156 ; 0.39440392470921504 ; 0.1940847408082912 ; 0.9989104243931105
Encoded representation 2 (Slots):
  2868 copies /   4050 of book cover  11
  2868 copies /   3750 of book cover  16
  2861 copies /   6275 of book cover  18
  2857 copies /   6275 of book cover  18
  2856 copies /   6300 of book cover  17
  2851 copies /   4100 of book cover   7
  2849 copies /   4850 of book cover  22
  2839 copies /   6300 of book cover  17
  2813 copies /   3650 of book cover  20
  2768 copies /   5300 of book cover  15
  2650 copies /   2650 of book cover  21
  2532 copies /   5300 of book cover  15
  2514 copies /   3300 of book cover  12
  2350 copies /   2350 of book cover   3
  2275 copies /   2275 of book cover  19
   882 copies /   3750 of book cover  16
   878 copies /   4850 of book cover  22
   878 copies /   4050 of book cover  11
   850 copies /    850 of book cover   4
   850 copies /    850 of book cover   8
   848 copies /   1050 of book cover  14
   844 copies /   4850 of book cover  22
   837 copies /   3650 of book cover  20
   823 copies /   1025 of book cover  10
   800 copies /    800 of book cover   6
   800 copies /    800 of book cover   9
   791 copies /   4100 of book cover   7
   786 copies /   3300 of book cover  12
   700 copies /    700 of book cover   2
   685 copies /    950 of book cover  13
   313 copies /    625 of book cover   5
   312 copies /    625 of book cover   5
   311 copies /   6275 of book cover  18
   310 copies /    600 of book cover   1
   307 copies /   6300 of book cover  17
   304 copies /   4050 of book cover  11
   298 copies /   6300 of book cover  17
   290 copies /    600 of book cover   1
   279 copies /   4850 of book cover  22
   267 copies /   4100 of book cover   7
   265 copies /    950 of book cover  13
   246 copies /   6275 of book cover  18
   202 copies /   1050 of book cover  14
   202 copies /   1025 of book cover  10
   191 copies /   4100 of book cover   7
Phenotype (CppSolution):
Demand fulfillment:
  1:    626 /    600 (   26 extra)
  2:    882 /    700 (  182 extra)
  3:   2868 /   2350 (  518 extra)
  4:    882 /    850 (   32 extra)
  5:    626 /    625 (    1 extra)
  6:    882 /    800 (   82 extra)
  7:   4376 /   4100 (  276 extra)
  8:    882 /    850 (   32 extra)
  9:    882 /    800 (   82 extra)
 10:   1195 /   1025 (  170 extra)
 11:   4063 /   4050 (   13 extra)
 12:   3750 /   3300 (  450 extra)
 13:   1195 /    950 (  245 extra)
 14:   1195 /   1050 (  145 extra)
 15:   5736 /   5300 (  436 extra)
 16:   3750 /   3750 (    0 extra)
 17:   6362 /   6300 (   62 extra)
 18:   6362 /   6275 (   87 extra)
 19:   2868 /   2275 (  593 extra)
 20:   3750 /   3650 (  100 extra)
 21:   2868 /   2650 (  218 extra)
 22:   4945 /   4850 (   95 extra)
Plates configuration:
|   3 |   7 |  11 |  12 |  15 |  15 |  16 |  17 |  17 |  18 |  18 |  19 |  20 |  21 |  22 | :   2868 printings
|   2 |   4 |   6 |   7 |   8 |   9 |  10 |  11 |  12 |  13 |  14 |  16 |  20 |  22 |  22 | :    882 printings
|   1 |   1 |   5 |   5 |   7 |   7 |  10 |  11 |  13 |  14 |  17 |  17 |  18 |  18 |  22 | :    313 printings
Feasible solution: true
Total printings: 4063
Total copies: 60945 (57100 requested)
Total waste: 6.3090 %
Cost: 110634.72
Best qualities:
Cost: 110634.72
Average quality: 110634.720000
Median quality: 110634.720000


Experiment: Genome CF
1 runs of duration 2m
Overall best:
Encoded representation 1 (GenomeCf):
(46 keys) 0.2778177894648287 ; 0.14428084613301584 ; 0.31322010244521536 ; 0.5233212707444665 ; 0.20103912546784108 ; 0.9817612524311811 ; 0.9758071358229645 ; 0.9153372871459384 ; 0.7450891566675544 ; 0.8304412001261035 ; 0.4664862335651194 ; 0.5236945016094465 ; 0.7862934527842141 ; 0.8403773864156607 ; 0.828109800265359 ; 0.9007872239509715 ; 0.13940631295298445 ; 0.5196680303182298 ; 0.07695618700106688 ; 0.7212565651096031 ; 0.7874795626869586 ; 0.5525497468668127 ; 0.0880829996215613 ; 0.6094307389059619 ; 0.4811138041242863 ; 0.22189336301353102 ; 0.6403418818698221 ; 0.9979520357706188 ; 0.04127110532166567 ; 0.9489729090493857 ; 0.14072903505230738 ; 0.9992423906689658 ; 0.7604335215279511 ; 0.567432469360414 ; 0.8478845817643519 ; 0.3373805969325585 ; 0.6854694812538495 ; 0.5450132128061055 ; 0.5104540484047665 ; 0.6263711858365745 ; 0.7217984590293175 ; 0.15579810301573593 ; 0.3123740663080461 ; 0.11292728692266829 ; 0.8910317955958225 ; 0.5548905081830982
Encoded representation 2 (Slots):
  2659 copies /   4050 of book cover  11
  2656 copies /   5300 of book cover  15
  2652 copies /   4100 of book cover   7
  2650 copies /   2650 of book cover  21
  2644 copies /   5300 of book cover  15
  2640 copies /   6300 of book cover  17
  2638 copies /   3650 of book cover  20
  2636 copies /   6275 of book cover  18
  2634 copies /   6275 of book cover  18
  2631 copies /   4850 of book cover  22
  2615 copies /   6300 of book cover  17
  2517 copies /   3750 of book cover  16
  2350 copies /   2350 of book cover   3
  2267 copies /   3300 of book cover  12
  2219 copies /   4850 of book cover  22
  1060 copies /   4050 of book cover  11
  1050 copies /   1050 of book cover  14
  1045 copies /   6300 of book cover  17
  1040 copies /   2275 of book cover  19
  1033 copies /   3300 of book cover  12
  1025 copies /   1025 of book cover  10
  1024 copies /   2275 of book cover  19
  1021 copies /   3750 of book cover  16
  1012 copies /   3650 of book cover  20
  1005 copies /   6275 of book cover  18
   995 copies /   4100 of book cover   7
   950 copies /    950 of book cover  13
   850 copies /    850 of book cover   8
   800 copies /    800 of book cover   6
   800 copies /    800 of book cover   9
   331 copies /   4050 of book cover  11
   319 copies /    625 of book cover   5
   316 copies /    600 of book cover   1
   316 copies /    850 of book cover   4
   314 copies /    850 of book cover   4
   312 copies /    700 of book cover   2
   306 copies /    625 of book cover   5
   295 copies /   4100 of book cover   7
   284 copies /    600 of book cover   1
   252 copies /    700 of book cover   2
   220 copies /    850 of book cover   4
   212 copies /   3750 of book cover  16
   211 copies /   2275 of book cover  19
   158 copies /   4100 of book cover   7
   136 copies /    700 of book cover   2
Phenotype (CppSolution):
Demand fulfillment:
  1:    662 /    600 (   62 extra)
  2:    993 /    700 (  293 extra)
  3:   2659 /   2350 (  309 extra)
  4:    993 /    850 (  143 extra)
  5:    662 /    625 (   37 extra)
  6:   1060 /    800 (  260 extra)
  7:   4381 /   4100 (  281 extra)
  8:   1060 /    850 (  210 extra)
  9:   1060 /    800 (  260 extra)
 10:   1060 /   1025 (   35 extra)
 11:   4050 /   4050 (    0 extra)
 12:   3719 /   3300 (  419 extra)
 13:   1060 /    950 (  110 extra)
 14:   1060 /   1050 (   10 extra)
 15:   5318 /   5300 (   18 extra)
 16:   4050 /   3750 (  300 extra)
 17:   6378 /   6300 (   78 extra)
 18:   6378 /   6275 (  103 extra)
 19:   2451 /   2275 (  176 extra)
 20:   3719 /   3650 (   69 extra)
 21:   2659 /   2650 (    9 extra)
 22:   5318 /   4850 (  468 extra)
Plates configuration:
|   3 |   7 |  11 |  12 |  15 |  15 |  16 |  17 |  17 |  18 |  18 |  20 |  21 |  22 |  22 | :   2659 printings
|   6 |   7 |   8 |   9 |  10 |  11 |  12 |  13 |  14 |  16 |  17 |  18 |  19 |  19 |  20 | :   1060 printings
|   1 |   1 |   2 |   2 |   2 |   4 |   4 |   4 |   5 |   5 |   7 |   7 |  11 |  16 |  19 | :    331 printings
Feasible solution: true
Total printings: 4050
Total copies: 60750 (57100 requested)
Total waste: 6.0082 %
Cost: 110460.0
Best qualities:
Cost: 110460.0
Average quality: 110460.000000
Median quality: 110460.000000