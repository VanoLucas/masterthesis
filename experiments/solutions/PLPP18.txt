Experiment 'PLPP18 with 3 plates', run   1 [start]
New best:
Encoded representation 1 (GenomeSp):
(60 keys) 0.7982503929089936 ; 0.7189354745538528 ; 0.032819729103555795 ; 0.5423578698537924 ; 0.5592771313439084 ; 0.09034019033899754 ; 0.4517753980535212 ; 0.007016757552199215 ; 0.27725102148398395 ; 0.6632562596324134 ; 0.20612199157402666 ; 0.7281917808082647 ; 0.7134627849664247 ; 0.0075879871173307745 ; 0.32053044149430854 ; 0.14345702009369 ; 0.6359053188983333 ; 0.8616325548822447 ; 0.029787269832192376 ; 0.49680651091684835 ; 0.991055359469639 ; 0.8625712777034447 ; 0.9505982163130786 ; 0.9550469135771444 ; 0.9346944564379677 ; 0.924882293244568 ; 0.04417317768838769 ; 0.5462946004743253 ; 0.4682610414709165 ; 0.7234445558392565 ; 0.5613643759305725 ; 0.38430268848475846 ; 0.17603868072444306 ; 0.9883392235803693 ; 0.36182739873585235 ; 0.45463814149096304 ; 0.44847562963176757 ; 0.05877714668837963 ; 0.4184323138970094 ; 0.7971634684288801 ; 0.8889997497802087 ; 0.2814247036916413 ; 0.017488120923003336 ; 0.5164570828735918 ; 0.20977365599802789 ; 0.22383383625535702 ; 0.42383485340422455 ; 0.42388677594505275 ; 0.58711994385229 ; 0.8104492972214417 ; 0.7115161786421299 ; 0.0913279514926143 ; 0.07419579202833837 ; 0.9938830209091832 ; 0.5492605950286986 ; 0.5522649979872668 ; 0.8817513530590285 ; 0.8783567630277327 ; 0.8597264016018491 ; 0.5956004006208129
Encoded representation 2 (Slots):
  1533 copies /   2450 of book cover  10
  1523 copies /   3850 of book cover  13
  1523 copies /   3850 of book cover  13
  1522 copies /   5550 of book cover  18
  1516 copies /   5550 of book cover  18
  1484 copies /   5550 of book cover  18
  1447 copies /   3050 of book cover  17
  1438 copies /   2700 of book cover  15
  1437 copies /   2200 of book cover   1
  1400 copies /   1400 of book cover  14
  1262 copies /   2700 of book cover  15
  1259 copies /   2500 of book cover   9
  1241 copies /   2500 of book cover   9
  1028 copies /   5550 of book cover  18
   804 copies /   2450 of book cover  10
   804 copies /   3850 of book cover  13
   804 copies /   3050 of book cover  17
   799 copies /   1150 of book cover  12
   799 copies /   3050 of book cover  17
   772 copies /   1400 of book cover  16
   720 copies /   2200 of book cover   1
   628 copies /   1400 of book cover  16
   550 copies /    550 of book cover   6
   550 copies /    550 of book cover   8
   500 copies /    500 of book cover   3
   467 copies /    550 of book cover   7
   324 copies /   1150 of book cover  12
   266 copies /    350 of book cover  11
   113 copies /   2450 of book cover  10
   103 copies /    250 of book cover   5
    84 copies /    350 of book cover  11
    83 copies /    550 of book cover   7
    80 copies /    250 of book cover   5
    69 copies /    200 of book cover   2
    69 copies /    200 of book cover   2
    67 copies /    250 of book cover   5
    62 copies /    200 of book cover   2
    61 copies /    100 of book cover   4
    43 copies /   2200 of book cover   1
    36 copies /    100 of book cover   4
    27 copies /   1150 of book cover  12
     3 copies /    100 of book cover   4
Phenotype (CppSolution):
Demand fulfillment:
  1:   2450 /   2200 (  250 extra)
  2:    339 /    200 (  139 extra)
  3:    804 /    500 (  304 extra)
  4:    339 /    100 (  239 extra)
  5:    339 /    250 (   89 extra)
  6:    804 /    550 (  254 extra)
  7:    917 /    550 (  367 extra)
  8:    804 /    550 (  254 extra)
  9:   3066 /   2500 (  566 extra)
 10:   2450 /   2450 (    0 extra)
 11:    917 /    350 (  567 extra)
 12:   1721 /   1150 (  571 extra)
 13:   3870 /   3850 (   20 extra)
 14:   1533 /   1400 (  133 extra)
 15:   3066 /   2700 (  366 extra)
 16:   1608 /   1400 (  208 extra)
 17:   3141 /   3050 (   91 extra)
 18:   6132 /   5550 (  582 extra)
Plates configuration:
|   1 |   9 |   9 |  10 |  13 |  13 |  14 |  15 |  15 |  17 |  18 |  18 |  18 |  18 | :   1533 printings
|   1 |   3 |   6 |   7 |   8 |  10 |  11 |  12 |  12 |  13 |  16 |  16 |  17 |  17 | :    804 printings
|   1 |   2 |   2 |   2 |   4 |   4 |   4 |   5 |   5 |   5 |   7 |  10 |  11 |  12 | :    113 printings
Feasible solution: true
Total printings: 2450
Total waste: 14.5773 %
Quality: 88956.0



Experiment 'PLPP18 with 4 plates', run   1 [start]
New best:
Encoded representation 1 (GenomeSp):
(74 keys) 0.7987331979835932 ; 0.6720648972950678 ; 0.7731760923508698 ; 0.30710982060788083 ; 0.3261711562345152 ; 0.7248436116415974 ; 0.707421783951809 ; 0.20537108051741548 ; 0.906004707956933 ; 0.5375289164519798 ; 0.5894335105400753 ; 0.004716628273268242 ; 0.4547747289593652 ; 0.032456679225725704 ; 0.9422073422785251 ; 0.04443117332724167 ; 0.6653195215320863 ; 0.6862181627937779 ; 0.5703264634293841 ; 0.5638034705224961 ; 0.557428936875335 ; 0.41763719519741194 ; 0.8183238243686456 ; 0.8064729789393523 ; 0.7273462753361185 ; 0.6827533503030405 ; 0.8147799053606479 ; 0.8522640370996033 ; 0.8131131991324543 ; 0.27251239350403944 ; 0.5986823025290936 ; 0.5533310911947185 ; 0.9876525757751903 ; 0.7694143109406211 ; 0.8379731884309702 ; 0.2791123880873695 ; 0.8635852944519642 ; 0.8585037165136862 ; 0.31893959966584307 ; 0.9850829338865585 ; 0.9631509705811697 ; 0.9706571574143603 ; 0.45815594601509135 ; 0.028657245939817333 ; 0.042136042443557065 ; 0.5880282709123926 ; 0.042588751628108934 ; 0.5978886814359472 ; 0.983333345878464 ; 0.5613763512557323 ; 0.5746462376136944 ; 0.9512739032405413 ; 0.13194244501083974 ; 0.4529447511980452 ; 0.4360337772706735 ; 0.42260740517532147 ; 0.8538829727602554 ; 0.7791544298177825 ; 0.845770052931153 ; 0.1765773295456765 ; 0.9562304047924185 ; 0.9558793791932915 ; 0.9527229709728492 ; 0.9557699895670159 ; 0.007298055037268325 ; 0.580056245486253 ; 0.0941636250462351 ; 0.11179117797490823 ; 0.8367700104967762 ; 0.8758646131228243 ; 0.45405136405011914 ; 0.45106367429746597 ; 0.45259014862895475 ; 0.4442017891354718
Encoded representation 2 (Slots):
  1400 copies /   1400 of book cover  14
  1400 copies /   1400 of book cover  16
  1399 copies /   5550 of book cover  18
  1394 copies /   5550 of book cover  18
  1392 copies /   3050 of book cover  17
  1389 copies /   5550 of book cover  18
  1368 copies /   5550 of book cover  18
  1331 copies /   3050 of book cover  17
  1326 copies /   3850 of book cover  13
  1314 copies /   3850 of book cover  13
  1210 copies /   3850 of book cover  13
  1150 copies /   1150 of book cover  12
  1117 copies /   2450 of book cover  10
  1091 copies /   2500 of book cover   9
   674 copies /   2450 of book cover  10
   674 copies /   2700 of book cover  15
   674 copies /   2700 of book cover  15
   674 copies /   2700 of book cover  15
   673 copies /   2700 of book cover  15
   663 copies /   2500 of book cover   9
   659 copies /   2450 of book cover  10
   652 copies /   2500 of book cover   9
   595 copies /   2200 of book cover   1
   588 copies /   2200 of book cover   1
   581 copies /   2200 of book cover   1
   518 copies /    550 of book cover   8
   436 copies /   2200 of book cover   1
   177 copies /   3050 of book cover  17
   167 copies /    550 of book cover   6
   167 copies /    550 of book cover   7
   166 copies /    550 of book cover   6
   165 copies /    550 of book cover   7
   164 copies /    550 of book cover   7
   163 copies /    550 of book cover   6
   155 copies /    500 of book cover   3
   155 copies /    350 of book cover  11
   150 copies /    350 of book cover  11
   150 copies /   3050 of book cover  17
   148 copies /    500 of book cover   3
   148 copies /    500 of book cover   3
   141 copies /    250 of book cover   5
   109 copies /    250 of book cover   5
    54 copies /    200 of book cover   2
    54 copies /    550 of book cover   6
    54 copies /    550 of book cover   7
    53 copies /    200 of book cover   2
    52 copies /    100 of book cover   4
    49 copies /    500 of book cover   3
    48 copies /    200 of book cover   2
    48 copies /    100 of book cover   4
    47 copies /   2500 of book cover   9
    47 copies /   2500 of book cover   9
    45 copies /    200 of book cover   2
    45 copies /    350 of book cover  11
    32 copies /    550 of book cover   8
     5 copies /   2700 of book cover  15
Phenotype (CppSolution):
Demand fulfillment:
  1:   2696 /   2200 (  496 extra)
  2:    216 /    200 (   16 extra)
  3:    555 /    500 (   55 extra)
  4:    108 /    100 (    8 extra)
  5:    334 /    250 (   84 extra)
  6:    555 /    550 (    5 extra)
  7:    555 /    550 (    5 extra)
  8:    728 /    550 (  178 extra)
  9:   2856 /   2500 (  356 extra)
 10:   2748 /   2450 (  298 extra)
 11:    388 /    350 (   38 extra)
 12:   1400 /   1150 (  250 extra)
 13:   4200 /   3850 (  350 extra)
 14:   1400 /   1400 (    0 extra)
 15:   2750 /   2700 (   50 extra)
 16:   1400 /   1400 (    0 extra)
 17:   3641 /   3050 (  591 extra)
 18:   5600 /   5550 (   50 extra)
Plates configuration:
|   9 |  10 |  12 |  13 |  13 |  13 |  14 |  16 |  17 |  17 |  18 |  18 |  18 |  18 | :   1400 printings
|   1 |   1 |   1 |   1 |   8 |   9 |   9 |  10 |  10 |  15 |  15 |  15 |  15 |  17 | :    674 printings
|   3 |   3 |   3 |   5 |   5 |   6 |   6 |   6 |   7 |   7 |   7 |  11 |  11 |  17 | :    167 printings
|   2 |   2 |   2 |   2 |   3 |   4 |   4 |   6 |   7 |   8 |   9 |   9 |  11 |  15 | :     54 printings
Feasible solution: true
Total printings: 2295
Total waste: 8.8080 %
Quality: 105548.8



Experiment: PLPP18 with 3 plates
10 runs of duration 1m 40s
Overall best:
Encoded representation 1 (GenomeSp):
(60 keys) 0.11990926511711242 ; 0.3958311802990033 ; 0.19209098992682738 ; 0.08260206157892125 ; 0.29739581681567295 ; 0.022019417000421093 ; 0.13671029532364198 ; 0.885982032474914 ; 0.8538759239780459 ; 0.2799506050637286 ; 0.33999195333565757 ; 0.6286565339929415 ; 0.7673378298917467 ; 0.00825644329885633 ; 0.7711653966985212 ; 0.20754525768390952 ; 0.19900874260308632 ; 0.8883365338030162 ; 0.8792168941998197 ; 0.31738595401627145 ; 0.6538734100218866 ; 0.8742778705151288 ; 0.5135207319486126 ; 0.27186596491053827 ; 0.5671799254489972 ; 0.5667403946531842 ; 0.11951090385344298 ; 0.5981584451177989 ; 0.4733794315804313 ; 0.4738895367002679 ; 0.4706131107488991 ; 0.4730793090146247 ; 0.7404280039311499 ; 0.7448214427201381 ; 0.9661040195411875 ; 0.6754520226026915 ; 0.8540039219910145 ; 0.46311286106903304 ; 0.22763522070639775 ; 0.9590022434435251 ; 0.9576474299913867 ; 0.2145903783830717 ; 0.8936007675002584 ; 0.9862000313418579 ; 0.0615386883950374 ; 0.986141077401634 ; 0.3425075353850744 ; 0.09207668114531598 ; 0.26733977742463344 ; 0.24284170711509112 ; 0.6937911910820177 ; 0.004014346327761098 ; 0.9219451294888326 ; 0.009684342828976011 ; 0.6248186414390163 ; 0.5501016407625733 ; 0.44531311437728016 ; 0.44540151915450066 ; 0.16113030750273782 ; 0.4455248364388441
Encoded representation 2 (Slots):
  1651 copies /   5550 of book cover  18
  1651 copies /   5550 of book cover  18
  1651 copies /   5550 of book cover  18
  1622 copies /   3050 of book cover  17
  1616 copies /   2200 of book cover   1
  1598 copies /   3850 of book cover  13
  1597 copies /   3850 of book cover  13
  1589 copies /   2450 of book cover  10
  1551 copies /   2700 of book cover  15
  1428 copies /   3050 of book cover  17
  1400 copies /   1400 of book cover  14
  1385 copies /   1400 of book cover  16
   861 copies /   2450 of book cover  10
   772 copies /   2500 of book cover   9
   597 copies /   2700 of book cover  15
   597 copies /   5550 of book cover  18
   596 copies /   2500 of book cover   9
   592 copies /   2500 of book cover   9
   584 copies /   2200 of book cover   1
   555 copies /   3850 of book cover  13
   550 copies /    550 of book cover   6
   550 copies /    550 of book cover   7
   543 copies /   2700 of book cover  15
   540 copies /   2500 of book cover   9
   533 copies /   1150 of book cover  12
   500 copies /    500 of book cover   3
   498 copies /   1150 of book cover  12
   283 copies /    350 of book cover  11
   138 copies /    550 of book cover   8
   138 copies /    550 of book cover   8
   137 copies /    550 of book cover   8
   137 copies /    550 of book cover   8
   125 copies /    250 of book cover   5
   125 copies /    250 of book cover   5
   119 copies /   1150 of book cover  12
   114 copies /    200 of book cover   2
   100 copies /    100 of book cover   4
   100 copies /   3850 of book cover  13
    86 copies /    200 of book cover   2
    67 copies /    350 of book cover  11
    15 copies /   1400 of book cover  16
     9 copies /   2700 of book cover  15
Phenotype (CppSolution):
Demand fulfillment:
  1:   2248 /   2200 (   48 extra)
  2:    276 /    200 (   76 extra)
  3:    597 /    500 (   97 extra)
  4:    138 /    100 (   38 extra)
  5:    276 /    250 (   26 extra)
  6:    597 /    550 (   47 extra)
  7:    597 /    550 (   47 extra)
  8:    552 /    550 (    2 extra)
  9:   3442 /   2500 (  942 extra)
 10:   3302 /   2450 (  852 extra)
 11:    735 /    350 (  385 extra)
 12:   1332 /   1150 (  182 extra)
 13:   4037 /   3850 (  187 extra)
 14:   1651 /   1400 (  251 extra)
 15:   2983 /   2700 (  283 extra)
 16:   1789 /   1400 (  389 extra)
 17:   3302 /   3050 (  252 extra)
 18:   5550 /   5550 (    0 extra)
Plates configuration:
|   1 |   9 |  10 |  10 |  13 |  13 |  14 |  15 |  16 |  17 |  17 |  18 |  18 |  18 | :   1651 printings
|   1 |   3 |   6 |   7 |   9 |   9 |   9 |  11 |  12 |  12 |  13 |  15 |  15 |  18 | :    597 printings
|   2 |   2 |   4 |   5 |   5 |   8 |   8 |   8 |   8 |  11 |  12 |  13 |  15 |  16 | :    138 printings
Feasible solution: true
Total printings: 2386
Total waste: 0.1401 %
Quality: 88095.84
Best qualities:
88095.84 88284.0 89063.51999999999 89399.51999999999 89775.84 89829.6 90017.76000000001 90622.56 91482.72 92195.04000000001
Average quality: 89876.640000
Median quality: 89802.720000


Experiment: PLPP18 with 3 plates
10 runs of duration 15s
Overall best:
Encoded representation 1 (GenomeCf):
(48 keys) 0.9279125089645197 ; 0.9929246961710764 ; 0.988227407822527 ; 0.01806706935674718 ; 0.2393349290431941 ; 0.47114324429257837 ; 0.8062263634455216 ; 0.9500869222265044 ; 0.5274023451303135 ; 0.3190046302243589 ; 0.6891878502754725 ; 0.2992297664854089 ; 0.7017957388733942 ; 0.04251174605107089 ; 0.7058261494554269 ; 0.9998353897884094 ; 0.10805384635000914 ; 0.6843086682170317 ; 0.9996861780685014 ; 0.5821497692804245 ; 0.08752369397463855 ; 0.9667116483799404 ; 0.01449789713617955 ; 0.5153850715314098 ; 0.4724211528861164 ; 0.3354431864725059 ; 0.4760807195663136 ; 0.48621934306708925 ; 0.9695024665758759 ; 0.03503611661142347 ; 0.9525754278942938 ; 0.0342370881501034 ; 0.2330903530502758 ; 0.6799653613739229 ; 0.5129978256749271 ; 0.5095811523664289 ; 0.9901867081302227 ; 0.3013103682096143 ; 0.2715813868838619 ; 0.9628317199689788 ; 0.9929518202766151 ; 0.9981655560424011 ; 0.015000056013386942 ; 0.0714361608996279 ; 0.19407518834099535 ; 0.7983007397313829 ; 0.654350575737044 ; 0.9997364620097308
Encoded representation 2 (Slots):
  1603 copies /   3850 of book cover  13
  1603 copies /   5550 of book cover  18
  1602 copies /   3850 of book cover  13
  1600 copies /   5550 of book cover  18
  1597 copies /   5550 of book cover  18
  1576 copies /   2200 of book cover   1
  1576 copies /   2500 of book cover   9
  1536 copies /   2450 of book cover  10
  1536 copies /   3050 of book cover  17
  1514 copies /   3050 of book cover  17
  1418 copies /   2700 of book cover  15
  1400 copies /   1400 of book cover  14
  1400 copies /   1400 of book cover  16
  1282 copies /   2700 of book cover  15
   576 copies /   1150 of book cover  12
   576 copies /   3850 of book cover  13
   574 copies /   1150 of book cover  12
   567 copies /   5550 of book cover  18
   566 copies /   2200 of book cover   1
   550 copies /    550 of book cover   6
   550 copies /    550 of book cover   7
   550 copies /    550 of book cover   8
   524 copies /   2450 of book cover  10
   505 copies /   2500 of book cover   9
   500 copies /    500 of book cover   3
   419 copies /   2500 of book cover   9
   390 copies /   2450 of book cover  10
   350 copies /    350 of book cover  11
    69 copies /    200 of book cover   2
    69 copies /   3850 of book cover  13
    68 copies /    200 of book cover   2
    68 copies /   5550 of book cover  18
    66 copies /    250 of book cover   5
    65 copies /   5550 of book cover  18
    65 copies /    250 of book cover   5
    63 copies /    200 of book cover   2
    61 copies /    100 of book cover   4
    61 copies /    250 of book cover   5
    58 copies /    250 of book cover   5
    58 copies /   2200 of book cover   1
    50 copies /   5550 of book cover  18
    39 copies /    100 of book cover   4
Phenotype (CppSolution):
Demand fulfillment:
  1:   2248 /   2200 (   48 extra)
  2:    207 /    200 (    7 extra)
  3:    576 /    500 (   76 extra)
  4:    138 /    100 (   38 extra)
  5:    276 /    250 (   26 extra)
  6:    576 /    550 (   26 extra)
  7:    576 /    550 (   26 extra)
  8:    576 /    550 (   26 extra)
  9:   2755 /   2500 (  255 extra)
 10:   2755 /   2450 (  305 extra)
 11:    576 /    350 (  226 extra)
 12:   1152 /   1150 (    2 extra)
 13:   3851 /   3850 (    1 extra)
 14:   1603 /   1400 (  203 extra)
 15:   3206 /   2700 (  506 extra)
 16:   1603 /   1400 (  203 extra)
 17:   3206 /   3050 (  156 extra)
 18:   5592 /   5550 (   42 extra)
Plates configuration:
|   1 |   9 |  10 |  13 |  13 |  14 |  15 |  15 |  16 |  17 |  17 |  18 |  18 |  18 | :   1603 printings
|   1 |   3 |   6 |   7 |   8 |   9 |   9 |  10 |  10 |  11 |  12 |  12 |  13 |  18 | :    576 printings
|   1 |   2 |   2 |   2 |   4 |   4 |   5 |   5 |   5 |   5 |  13 |  18 |  18 |  18 | :     69 printings
Feasible solution: true
Total printings: 2248
Total waste: 7.4130 %
Quality: 86241.12
Best qualities:
86241.12 86388.95999999999 86778.72 87168.48 87195.36 87652.32 87988.32 88351.2 89628.0 91751.51999999999
Average quality: 87914.400000
Median quality: 87423.840000


Encoded representation 1 (GenomeCf):
(104 keys) 0.6633685311340397 ; 0.2975268255678818 ; 0.3533096195252464 ; 0.28663964769679207 ; 0.9815225140905173 ; 0.498754537949311 ; 0.2991246946018943 ; 0.6124257923854286 ; 0.5641244448137535 ; 0.9991078456510801 ; 0.7112931968159628 ; 0.6540230420416475 ; 0.9339641583904729 ; 0.9084596255178468 ; 0.7000558469906598 ; 0.9542164369325682 ; 0.7347449496177051 ; 0.10651815515426455 ; 0.5547216445587869 ; 0.4006319059429361 ; 0.7382204570387689 ; 0.2663955230035313 ; 0.01839480319911868 ; 0.9780146695288052 ; 0.6130305307525469 ; 0.9997133947105965 ; 0.9948175447456276 ; 0.6635070957601304 ; 0.941003878102257 ; 0.025137965405104712 ; 0.9464815726032948 ; 0.9934829627210148 ; 0.1950566557040173 ; 0.432292997946516 ; 0.10289775099582277 ; 0.8294621759425623 ; 0.174293760605286 ; 0.5346922211926267 ; 0.4924215192044338 ; 0.3867241993094658 ; 0.043352125971718 ; 0.8722148568616219 ; 0.9282729035101861 ; 0.2140432237806894 ; 0.7338859368545813 ; 0.8522979742528303 ; 0.28814208650635154 ; 0.4271467542021492 ; 0.350289455017882 ; 0.7423328801212099 ; 0.8468631451263243 ; 0.24161972002344811 ; 0.30686488681295243 ; 0.13547998677904893 ; 0.4172009000915139 ; 0.25808280053568056 ; 0.05236931127084865 ; 0.4911016490116411 ; 0.8359170943830333 ; 0.1360326459748047 ; 0.46641952795376107 ; 0.16257867228529221 ; 0.8556791250512585 ; 0.8495768488872688 ; 0.36298797360371826 ; 0.5764980323694257 ; 0.8953428928157582 ; 0.11573748850940657 ; 0.05591205888915951 ; 0.3508891911126918 ; 0.19101219138280345 ; 0.7450542663498507 ; 0.8390360005473039 ; 0.5324997894132861 ; 0.24525413462394208 ; 0.6703849572229883 ; 0.17541462863285073 ; 0.8904356295397694 ; 0.3124590484928612 ; 0.15197109818774146 ; 0.4486991114195774 ; 0.5179914365619441 ; 0.38148236646982925 ; 0.8049417579880543 ; 0.37283192302080714 ; 0.3366232596960269 ; 0.511016217526177 ; 0.5004917120289203 ; 0.38732151325843267 ; 0.40310132829714307 ; 0.1333600260832969 ; 0.08558302026021392 ; 0.7722201528401661 ; 0.5253732644946303 ; 0.965770656345804 ; 0.025418786778895197 ; 0.8152274898760525 ; 0.9754365701798378 ; 0.5440244575009032 ; 0.11637776819269086 ; 0.10451728480135247 ; 0.27566193027995267 ; 0.32498363017128784 ; 0.6600485492131298
Encoded representation 2 (Slots):
  1385 copies /   2450 of book cover  10
  1385 copies /   5550 of book cover  18
  1385 copies /   3050 of book cover  17
  1384 copies /   2700 of book cover  15
  1384 copies /   3050 of book cover  17
  1384 copies /   5550 of book cover  18
  1382 copies /   5550 of book cover  18
  1382 copies /   5550 of book cover  18
  1374 copies /   2500 of book cover   9
  1356 copies /   3850 of book cover  13
  1316 copies /   2700 of book cover  15
  1258 copies /   3850 of book cover  13
  1236 copies /   3850 of book cover  13
  1075 copies /   2200 of book cover   1
   490 copies /   1150 of book cover  12
   490 copies /   2450 of book cover  10
   490 copies /   2200 of book cover   1
   490 copies /   1400 of book cover  14
   490 copies /   2450 of book cover  10
   489 copies /   1150 of book cover  12
   487 copies /   1400 of book cover  14
   487 copies /   1400 of book cover  16
   486 copies /   1400 of book cover  16
   483 copies /   2500 of book cover   9
   480 copies /   2200 of book cover   1
   480 copies /    550 of book cover   8
   480 copies /   2500 of book cover   9
   479 copies /    500 of book cover   3
   176 copies /    350 of book cover  11
   176 copies /   1400 of book cover  14
   176 copies /   3050 of book cover  17
   175 copies /    550 of book cover   6
   175 copies /    550 of book cover   7
   175 copies /   1400 of book cover  16
   174 copies /    350 of book cover  11
   173 copies /   1400 of book cover  14
   171 copies /   1150 of book cover  12
   169 copies /   1400 of book cover  16
   168 copies /    550 of book cover   6
   167 copies /    250 of book cover   5
   163 copies /   2500 of book cover   9
   155 copies /   2200 of book cover   1
    85 copies /    200 of book cover   2
    85 copies /    550 of book cover   7
    85 copies /    550 of book cover   7
    85 copies /   3050 of book cover  17
    85 copies /    550 of book cover   7
    85 copies /   2450 of book cover  10
    85 copies /    550 of book cover   6
    83 copies /   1400 of book cover  16
    83 copies /    250 of book cover   5
    82 copies /    200 of book cover   2
    81 copies /    550 of book cover   6
    78 copies /    550 of book cover   7
    74 copies /   1400 of book cover  14
    70 copies /    550 of book cover   8
    21 copies /    100 of book cover   4
    21 copies /    100 of book cover   4
    21 copies /    100 of book cover   4
    21 copies /    100 of book cover   4
    21 copies /    550 of book cover   6
    21 copies /    550 of book cover   7
    21 copies /    550 of book cover   7
    21 copies /    500 of book cover   3
    20 copies /   3050 of book cover  17
    20 copies /    550 of book cover   6
    20 copies /    200 of book cover   2
    17 copies /   5550 of book cover  18
    16 copies /    100 of book cover   4
    13 copies /    200 of book cover   2
Phenotype (CppSolution):
Demand fulfillment:
  1:   2541 /   2200 (  341 extra)
  2:    212 /    200 (   12 extra)
  3:    511 /    500 (   11 extra)
  4:    105 /    100 (    5 extra)
  5:    261 /    250 (   11 extra)
  6:    564 /    550 (   14 extra)
  7:    558 /    550 (    8 extra)
  8:    575 /    550 (   25 extra)
  9:   2541 /   2500 (   41 extra)
 10:   2450 /   2450 (    0 extra)
 11:    352 /    350 (    2 extra)
 12:   1156 /   1150 (    6 extra)
 13:   4155 /   3850 (  305 extra)
 14:   1417 /   1400 (   17 extra)
 15:   2770 /   2700 (   70 extra)
 16:   1417 /   1400 (   17 extra)
 17:   3052 /   3050 (    2 extra)
 18:   5561 /   5550 (   11 extra)
Plates configuration:
|   1 |   9 |  10 |  13 |  13 |  13 |  15 |  15 |  17 |  17 |  18 |  18 |  18 |  18 | :   1385 printings
|   1 |   1 |   3 |   8 |   9 |   9 |  10 |  10 |  12 |  12 |  14 |  14 |  16 |  16 | :    490 printings
|   1 |   5 |   6 |   6 |   7 |   9 |  11 |  11 |  12 |  14 |  14 |  16 |  16 |  17 | :    176 printings
|   2 |   2 |   5 |   6 |   6 |   7 |   7 |   7 |   7 |   8 |  10 |  14 |  16 |  17 | :     85 printings
|   2 |   2 |   3 |   4 |   4 |   4 |   4 |   4 |   6 |   6 |   7 |   7 |  17 |  18 | :     21 printings
Feasible solution: true
Total printings: 2157
Total copies: 30198 (29300 requested)
Total waste: 2.9737 %
Cost: 122370.08
