New best: [2019-08-12T12:50:35.624Z]
Encoded representation 1 (GenomeCf):
(50 keys) 0.09736518941169858 ; 0.6711907086778429 ; 0.19343251392637018 ; 0.5146604657332454 ; 0.5442162491021189 ; 0.2858655629358847 ; 0.20171034482856398 ; 0.5994437733721251 ; 0.01103803598588493 ; 0.6342016576806343 ; 0.5086530911299232 ; 0.974043296891136 ; 0.15169860659931456 ; 0.6848374928494639 ; 0.7506197796519717 ; 0.20754726613032337 ; 0.05397478907912767 ; 0.06853935884745144 ; 0.6051180027732046 ; 0.32723488828087455 ; 0.5360274151388826 ; 0.33481738736678446 ; 0.8898463440664999 ; 0.9796250702050671 ; 0.18690147092804488 ; 0.12342270580789938 ; 0.13009805625778392 ; 0.7570042635439874 ; 0.011720901708181408 ; 0.17387884400974596 ; 0.05021195828552438 ; 0.13073790918999828 ; 0.5605714097091227 ; 0.2691835477797496 ; 0.8479515018378786 ; 0.5643907525562991 ; 0.14650574861053933 ; 0.3250727012111415 ; 0.06578328890536 ; 0.5084277356631675 ; 0.061444026848539335 ; 0.12032305744104921 ; 0.5828885957116275 ; 0.21901736002792505 ; 0.8742128062863237 ; 0.8792251393512137 ; 0.4695601337767271 ; 0.9998875881217325 ; 0.9495257673892316 ; 0.989533367235196
Encoded representation 2 (Slots):
  6170 copies /   6170 of book cover   7
  6168 copies /   8850 of book cover   5
  6164 copies /   9740 of book cover   2
  6162 copies /   9880 of book cover   1
  6160 copies /   9270 of book cover   3
  6110 copies /   6110 of book cover   8
  6093 copies /   8700 of book cover   6
  5980 copies /   5980 of book cover   9
  5900 copies /   5900 of book cover  10
  5330 copies /   5330 of book cover  11
  3133 copies /   9150 of book cover   4
  3133 copies /   9150 of book cover   4
  3132 copies /   9880 of book cover   1
  3130 copies /   4060 of book cover  15
  3110 copies /   9270 of book cover   3
  3069 copies /   4300 of book cover  14
  3020 copies /   3610 of book cover  16
  2990 copies /   2990 of book cover  17
  2740 copies /   2740 of book cover  18
  2607 copies /   8700 of book cover   6
  2280 copies /   2280 of book cover  20
  2277 copies /   8850 of book cover   5
  2277 copies /   9150 of book cover   4
  2276 copies /   4550 of book cover  12
  2274 copies /   4550 of book cover  12
  2250 copies /   2510 of book cover  19
  2235 copies /   9740 of book cover   2
  2206 copies /   4300 of book cover  13
  2094 copies /   4300 of book cover  13
  1970 copies /   1970 of book cover  21
   618 copies /   1210 of book cover  23
   617 copies /   4300 of book cover  14
   614 copies /   4300 of book cover  14
   614 copies /   9740 of book cover   2
   607 copies /   9150 of book cover   4
   592 copies /   1210 of book cover  23
   590 copies /   3610 of book cover  16
   586 copies /   9880 of book cover   1
   546 copies /   4060 of book cover  15
   515 copies /   1280 of book cover  22
   405 copies /    800 of book cover  24
   405 copies /   8850 of book cover   5
   404 copies /   1280 of book cover  22
   395 copies /    800 of book cover  24
   394 copies /   9740 of book cover   2
   384 copies /   4060 of book cover  15
   361 copies /   1280 of book cover  22
   333 copies /   9740 of book cover   2
   330 copies /    330 of book cover  25
   260 copies /   2510 of book cover  19
Phenotype (CppSolution):
Demand fulfillment:
  1:   9921 /   9880 (   41 extra)
  2:   9878 /   9740 (  138 extra)
  3:   9303 /   9270 (   33 extra)
  4:   9164 /   9150 (   14 extra)
  5:   8855 /   8850 (    5 extra)
  6:   9303 /   8700 (  603 extra)
  7:   6170 /   6170 (    0 extra)
  8:   6170 /   6110 (   60 extra)
  9:   6170 /   5980 (  190 extra)
 10:   6170 /   5900 (  270 extra)
 11:   6170 /   5330 (  840 extra)
 12:   4560 /   4550 (   10 extra)
 13:   4560 /   4300 (  260 extra)
 14:   4369 /   4300 (   69 extra)
 15:   4156 /   4060 (   96 extra)
 16:   3751 /   3610 (  141 extra)
 17:   3133 /   2990 (  143 extra)
 18:   3133 /   2740 (  393 extra)
 19:   2685 /   2510 (  175 extra)
 20:   2280 /   2280 (    0 extra)
 21:   2280 /   1970 (  310 extra)
 22:   1428 /   1280 (  148 extra)
 23:   1236 /   1210 (   26 extra)
 24:    810 /    800 (   10 extra)
 25:    405 /    330 (   75 extra)
Plates configuration:
|   1 |   2 |   3 |   5 |   6 |   7 |   8 |   9 |  10 |  11 | :   6170 printings
|   1 |   3 |   4 |   4 |   6 |  14 |  15 |  16 |  17 |  18 | :   3133 printings
|   2 |   4 |   5 |  12 |  12 |  13 |  13 |  19 |  20 |  21 | :   2280 printings
|   1 |   2 |   4 |  14 |  14 |  15 |  16 |  22 |  23 |  23 | :    618 printings
|   2 |   2 |   5 |  15 |  19 |  22 |  22 |  24 |  24 |  25 | :    405 printings
Feasible solution: true
Total printings: 12606
Total copies: 126060 (122010 requested)
Total waste: 3.2128 %
Cost: 262804.64