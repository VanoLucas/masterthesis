package com.vanolucas.cppsolver.eval;

import com.vanolucas.cppsolver.instance.CppInstance;
import com.vanolucas.cppsolver.solution.CppSolution;
import com.vanolucas.opti.quality.DoubleCost;

public class CppSolutionEvaluator {
    private final double costPlate;
    private final double costPrinting;

    public CppSolutionEvaluator(CppInstance cppInstance) {
        this(cppInstance.getCostPlate(), cppInstance.getCostPrinting());
    }

    public CppSolutionEvaluator(double costPlate, double costPrinting) {
        this.costPlate = costPlate;
        this.costPrinting = costPrinting;
    }

    public DoubleCost evaluate(CppSolution cppSolution) {
        return new DoubleCost(
                cppSolution.getCost(costPlate, costPrinting)
        );
    }
}
