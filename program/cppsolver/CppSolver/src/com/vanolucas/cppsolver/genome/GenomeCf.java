package com.vanolucas.cppsolver.genome;

import com.vanolucas.cppsolver.instance.StandardCppInstance;
import com.vanolucas.opti.genome.RandomKeyVector;

/**
 * CF Genome for the standard cover printing problem.
 * Cover-Fraction random key vector genome representation of a standard cover printing problem solution.
 * <p>
 * Sequence of pairs of keys. Each pair defines which book cover and how many printings to allocate to each slot.
 * The first key of each pair points to one of the book covers.
 * The second key of each pair gives the fraction of the demand to allocate to the slot.
 * Each book cover needs to be allocated to at least 1 slot. We therefore only need to describe all other (nbSlots - nbCovers) slots.
 */
public class GenomeCf extends RandomKeyVector {
    public GenomeCf(StandardCppInstance cppInstance) {
        this(calculateLengthForCppInstance(cppInstance));
    }

    public GenomeCf(int length) {
        super(length);
    }

    public static int calculateLengthForCppInstance(StandardCppInstance cppInstance) {
        return calculateLength(
                cppInstance.getBookCoversCount(),
                cppInstance.getSlotsCount()
        );
    }

    public static int calculateLength(int bookCoversCount, int slotsCount) {
        return (slotsCount - bookCoversCount) * 2;
    }
}
