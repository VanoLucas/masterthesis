package com.vanolucas.cppsolver.genome;

import com.vanolucas.cppsolver.instance.StandardCppInstance;
import com.vanolucas.opti.genome.RandomKeyVector;

/**
 * P-PC Genome for the standard cover printing problem.
 * Printings, Printings-Cover random key vector genome representation of a standard cover printing problem solution.
 * <p>
 * Part 1: keys to determine the nb of printings of each cover's first slot.
 * Part 2: pairs of keys to determine the nb of printings of each extra slot and its allocated book cover.
 * <p>
 * The first part of the P-PC genome (Printings) are keys that specify the number of copies to allocate to the first
 * slot of each book cover.
 * The second part is composed of pairs of keys. Each pair of keys describes how to allocate each extra slot.
 * The first key of each pair determines the number of copies to allocate to the slot.
 * The second key determines which book cover to allocate to the slot.
 */
public class GenomePpc extends RandomKeyVector {
    public GenomePpc(StandardCppInstance cppInstance) {
        this(calculateLengthForCppInstance(cppInstance));
    }

    public GenomePpc(int length) {
        super(length);
    }

    public static int calculateLengthForCppInstance(StandardCppInstance cppInstance) {
        return calculateLength(
                cppInstance.getBookCoversCount(),
                cppInstance.getSlotsCount()
        );
    }

    public static int calculateLength(int bookCoversCount, int slotsCount) {
        return bookCoversCount + 2 * (slotsCount - bookCoversCount);
    }
}

