package com.vanolucas.cppsolver.genome.decode;

import com.vanolucas.cppsolver.genome.GenomeCf;
import com.vanolucas.cppsolver.instance.StandardCppInstance;
import com.vanolucas.cppsolver.representation.Copies;
import com.vanolucas.cppsolver.representation.Demand;
import com.vanolucas.cppsolver.representation.Slots;

public class GenomeCfDecoder {
    private final Demand demand;
    private final int bookCoversCount;
    private final int slotsCount;

    public GenomeCfDecoder(StandardCppInstance cppInstance) {
        this(cppInstance.getDemand(), cppInstance.getBookCoversCount(), cppInstance.getSlotsCount());
    }

    public GenomeCfDecoder(Demand demand, int bookCoversCount, int slotsCount) {
        this.demand = demand;
        this.bookCoversCount = bookCoversCount;
        this.slotsCount = slotsCount;
    }

    public Slots decode(GenomeCf genome) {
        Slots slots = new Slots(slotsCount);

        // initially allot each book cover to a single slot that fills 100% of its copies demand
        demand.stream()
                .map(bookCover -> new Copies(bookCover, bookCover.getDemand()))
                .forEach(slots::add);

        // then we need to divide these copies over multiple slots to use all the available slots
        {
            // for each extra slot to fill
            for (int k = 0; k < genome.getLength(); k += 2) {
                // get the keys that characterize this slot
                final double keyCoverToSplit = genome.getKey(k);
                final double fractionToSplitOut = genome.getKey(k + 1) / 2d; // *0.5 because splitting more than 50% is symmetrical

                // calculate the index of the book cover/slot to split over an extra slot
                final int indexCoverToSplit = (int) (keyCoverToSplit * bookCoversCount);

                // split a fraction of the printings from the chosen book cover over an extra slot
                slots.splitInTwo(indexCoverToSplit, fractionToSplitOut);
            }
        }

        return slots;
    }
}

