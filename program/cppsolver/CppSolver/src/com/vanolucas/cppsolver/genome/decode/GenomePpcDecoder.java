package com.vanolucas.cppsolver.genome.decode;

import com.vanolucas.cppsolver.genome.GenomePpc;
import com.vanolucas.cppsolver.instance.StandardCppInstance;
import com.vanolucas.cppsolver.representation.Copies;
import com.vanolucas.cppsolver.representation.Demand;
import com.vanolucas.cppsolver.representation.Slots;
import com.vanolucas.opti.util.math.CutCake;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class GenomePpcDecoder {
    private final Demand demand;
    private final int bookCoversCount;
    private final int slotsCount;

    public GenomePpcDecoder(StandardCppInstance cppInstance) {
        this(cppInstance.getDemand(), cppInstance.getBookCoversCount(), cppInstance.getSlotsCount());
    }

    public GenomePpcDecoder(Demand demand, int bookCoversCount, int slotsCount) {
        this.demand = demand;
        this.bookCoversCount = bookCoversCount;
        this.slotsCount = slotsCount;
    }

    public Slots decode(GenomePpc genome) {
        final Slots slots = new Slots(slotsCount);

        // get the keys that determine the number of copies required for the slots of each book cover
        List<List<Double>> keysSlotsPrintingsByBookCover = getKeysSlotCopiesByBookCover(genome);

        // determine the allocated slots based on the keys
        for (int bookCoverIndex = 0; bookCoverIndex < bookCoversCount; bookCoverIndex++) {
            // get keys that define slot printings for this book cover
            double[] keys = keysSlotsPrintingsByBookCover.get(bookCoverIndex)
                    .stream()
                    .mapToDouble(__ -> __)
                    .toArray();
            // calculate number of copies to allocate to each slot based on the keys
            int[] slotsCopiesCount = CutCake.getShares(demand.getDemand(bookCoverIndex), keys);
            // construct the allocated slots
            for (int copiesCount : slotsCopiesCount) {
                slots.add(new Copies(demand.getBookCover(bookCoverIndex), copiesCount));
            }
        }

        return slots;
    }

    private List<List<Double>> getKeysSlotCopiesByBookCover(GenomePpc genome) {
        // to store, for each book cover, the keys that determine how to spread its printings over its slots
        List<List<Double>> keysByCover = IntStream.range(0, bookCoversCount)
                .mapToObj(__ -> new ArrayList<Double>(slotsCount - bookCoversCount))
                .collect(Collectors.toList());

        // get the initial key of each book cover
        for (int cover = 0; cover < bookCoversCount; cover++) {
            keysByCover.get(cover).add(
                    genome.getKey(cover)
            );
        }

        // read the extra slot keys
        final int genomeSize = genome.getLength();
        for (int i = bookCoversCount; i + 1 < genomeSize; i += 2) {
            // read keys for this extra slot
            double keyNbPrintings = genome.getKey(i);
            double keyBookCover = genome.getKey(i + 1);
            // determine the book cover index based on the key
            int cover = (int) (keyBookCover * (double) bookCoversCount);
            // store the printings key in this book cover's list of keys
            keysByCover.get(cover).add(keyNbPrintings);
        }

        return keysByCover;
    }
}

