package com.vanolucas.cppsolver.instance;

import com.vanolucas.cppsolver.representation.Demand;

/**
 * Instance of the cover printing problem.
 */
public class CppInstance {
    public static final double DEFAULT_COST_PLATE = 18_676.0d;
    public static final double DEFAULT_COST_PRINTING = 13.44d;

    /**
     * Demand for each book cover (m book covers).
     */
    private final Demand demand;
    /**
     * S = number of slots per offset plate.
     */
    private final int slotsPerPlateCount;
    /**
     * C1 = cost of one offset plate.
     */
    private final double costPlate;
    /**
     * C2 = cost of producing one printing using an offset plate.
     */
    private final double costPrinting;

    public CppInstance(Demand demand, int slotsPerPlateCount) {
        this(demand, slotsPerPlateCount, DEFAULT_COST_PLATE, DEFAULT_COST_PRINTING);
    }

    public CppInstance(Demand demand, int slotsPerPlateCount, double costPlate, double costPrinting) {
        this.demand = demand;
        if (slotsPerPlateCount < 1) {
            throw new IllegalArgumentException("The number of slots per offset plate must be at least 1.");
        }
        this.slotsPerPlateCount = slotsPerPlateCount;
        this.costPlate = costPlate;
        this.costPrinting = costPrinting;
    }

    public Demand getDemand() {
        return demand;
    }

    public int getSlotsPerPlateCount() {
        return slotsPerPlateCount;
    }

    public double getCostPlate() {
        return costPlate;
    }

    public double getCostPrinting() {
        return costPrinting;
    }

    public int getBookCoversCount() {
        return demand.getBookCoversCount();
    }

    public String strDemand() {
        return demand.toString();
    }

    public String strBookCoversCount() {
        return String.format("m = %d book covers", getBookCoversCount());
    }

    public String strSlotsPerPlateCount() {
        return String.format("S = %d slots per offset plate", slotsPerPlateCount);
    }

    public String strCostPlate() {
        return String.format("C1 = cost of one offset plate = %f", costPlate);
    }

    public String strCostPrinting() {
        return String.format("C2 = cost of one printing = %f", costPrinting);
    }

    @Override
    public String toString() {
        return String.format("%s%n" +
                        "%s%n" +
                        "%s%n" +
                        "%s%n" +
                        "Demand:%n" +
                        "%s%n",
                strBookCoversCount(),
                strSlotsPerPlateCount(),
                strCostPlate(),
                strCostPrinting(),
                strDemand()
        );
    }
}
