package com.vanolucas.cppsolver.rand;

import com.vanolucas.cppsolver.eval.PlatesAssigner;
import com.vanolucas.cppsolver.eval.SlotsAssigner;
import com.vanolucas.cppsolver.eval.SortByCopiesPlatesAssigner;
import com.vanolucas.cppsolver.instance.StandardCppInstance;
import com.vanolucas.cppsolver.representation.Demand;
import com.vanolucas.cppsolver.representation.Slots;
import com.vanolucas.cppsolver.solution.CppSolution;

import java.util.Random;

public class RandomCppSolutionFactory {
    private final StandardCppInstance cppInstance;
    private final SlotsAssigner slotsAssigner;
    private final PlatesAssigner platesAssigner;

    public RandomCppSolutionFactory(StandardCppInstance cppInstance) {
        this(cppInstance, new Random());
    }

    public RandomCppSolutionFactory(StandardCppInstance cppInstance, Random random) {
        this.cppInstance = cppInstance;
        this.slotsAssigner = new RandomSlotsAssigner(random);
        this.platesAssigner = new SortByCopiesPlatesAssigner(cppInstance.getDemand());
    }

    public CppSolution newCppSolution() {
        final Demand demand = cppInstance.getDemand();
        final int slotsCount = cppInstance.getSlotsCount();

        Slots slots = slotsAssigner.assign(
                demand, slotsCount
        );
        @SuppressWarnings("UnnecessaryLocalVariable") CppSolution cppSolution = platesAssigner.assign(
                slots, cppInstance.getSlotsPerPlateCount()
        );

        return cppSolution;
    }
}
