package com.vanolucas.cppsolver.rand;

import com.vanolucas.cppsolver.eval.SlotsAssigner;
import com.vanolucas.cppsolver.representation.Copies;
import com.vanolucas.cppsolver.representation.Demand;
import com.vanolucas.cppsolver.representation.Slots;
import com.vanolucas.opti.util.rand.Rand;

import java.util.Random;

public class RandomSlotsAssigner implements SlotsAssigner {
    private final Rand rand;

    public RandomSlotsAssigner() {
        this(new Random());
    }

    public RandomSlotsAssigner(Random random) {
        this.rand = new Rand(random);
    }

    @Override
    public Slots assign(Demand demand, int slotsCount) {
        final Slots slots = new Slots(slotsCount);

        // allot each book cover to at least one slot
        demand.stream()
                .map(bookCover -> new Copies(bookCover, bookCover.getDemand()))
                .forEach(slots::add);

        // allot each extra slot randomly to one of the book covers
        while (slots.getSlotsCount() < slotsCount) {
            final int slotToSplitIndex = rand.int0To(slots.getSlotsCount());
            final double fractionToExtract = rand.double01();
            slots.splitInTwo(
                    slotToSplitIndex,
                    fractionToExtract
            );
        }

        return slots;
    }
}
