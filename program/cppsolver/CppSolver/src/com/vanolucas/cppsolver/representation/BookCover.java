package com.vanolucas.cppsolver.representation;

/**
 * Represents a book cover from the cover printing problem.
 * It contains the id and the requested number of copies for this book cover.
 */
public class BookCover implements Comparable<BookCover> {
    private final int id;
    private final int demand;

    public BookCover(int id, int demand) {
        this.id = id;
        this.demand = demand;
    }

    public int getId() {
        return id;
    }

    public int getDemand() {
        return demand;
    }

    @Override
    public int compareTo(BookCover other) {
        return Integer.compare(id, other.id);
    }

    @Override
    public String toString() {
        return String.format("%3d: %d copies",
                id, demand
        );
    }
}
