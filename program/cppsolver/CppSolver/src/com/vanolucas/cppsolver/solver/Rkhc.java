package com.vanolucas.cppsolver.solver;

import com.vanolucas.cppsolver.eval.CppSolutionEvaluator;
import com.vanolucas.cppsolver.eval.SlotsDecoder;
import com.vanolucas.cppsolver.genome.GenomeType;
import com.vanolucas.cppsolver.instance.StandardCppInstance;
import com.vanolucas.cppsolver.representation.Slots;
import com.vanolucas.cppsolver.solution.CppSolution;
import com.vanolucas.opti.algo.*;
import com.vanolucas.opti.clone.Cloneable;
import com.vanolucas.opti.clone.MostEncodedRepresentationCloner;
import com.vanolucas.opti.eval.Evaluator;
import com.vanolucas.opti.eval.OneIntermediateGeneticEvaluator;
import com.vanolucas.opti.genome.RandomKeyVector;
import com.vanolucas.opti.mutation.MostEncodedRepresentationMutator;
import com.vanolucas.opti.mutation.neighborhood.CloneThenMutateNeighborProducer;
import com.vanolucas.opti.mutation.neighborhood.NeighborProducer;
import com.vanolucas.opti.mutation.neighborhood.NeighborsProducer;
import com.vanolucas.opti.quality.DoubleCost;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.solution.EvaluatedSolution;
import com.vanolucas.opti.solution.GeneticSolution;
import com.vanolucas.opti.solution.Solution;
import com.vanolucas.opti.solution.keeper.EarliestSingleBestKeeper;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.BooleanSupplier;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Rkhc {
    private final OptiRunner rkhcRunner;
    private final EarliestSingleBestKeeper bestKeeper;
    private final ExecutorService threadPool;

    public Rkhc(StandardCppInstance cppInstance,
                Duration restartAfterDurationWithoutImprovement,
                GenomeType genomeType,
                int threadsCount,
                boolean printNewBetterSolutions) {
        this(cppInstance, restartAfterDurationWithoutImprovement, genomeType, threadsCount, printNewBetterSolutions, new Random());
    }

    public Rkhc(StandardCppInstance cppInstance,
                Duration restartAfterDurationWithoutImprovement,
                GenomeType genomeType,
                int threadsCount,
                boolean printNewBetterSolutions,
                Random random) {
        // create random genome
        Supplier<RandomKeyVector> randomGenomeSupplier = () -> genomeType.newGenome(cppInstance).randomize(random);

        // create random solution
        Supplier<Solution> randomSolutionSupplier = () -> new GeneticSolution(randomGenomeSupplier.get());

        // get genome length
        final int genomeLength = randomGenomeSupplier.get().getLength();

        // factory of 'randomize 1 key' genome mutator
        Function<Integer, UnaryOperator<RandomKeyVector>> randomizeOneKeyRkvMutatorFactory = keyIndex ->
                rkv -> rkv.randomizeKey(keyIndex, random);

        // create 'randomize 1 key' genome mutators
        List<NeighborProducer> randomizeOneKeyNeighborProducers = IntStream.range(0, genomeLength)
                .mapToObj(keyIndex -> new CloneThenMutateNeighborProducer(
                        new MostEncodedRepresentationCloner(Cloneable.cloner()),
                        new MostEncodedRepresentationMutator(
                                randomizeOneKeyRkvMutatorFactory.apply(keyIndex)
                        )
                ))
                .collect(Collectors.toList());

        // while we have more available threads than neighboring operators, add neighboring operators
        while (threadsCount > randomizeOneKeyNeighborProducers.size()) {
            randomizeOneKeyNeighborProducers = new ArrayList<>(randomizeOneKeyNeighborProducers);
            randomizeOneKeyNeighborProducers.addAll(randomizeOneKeyNeighborProducers.subList(0, genomeLength));
        }

        // random-search
//        randomizeOneKeyNeighborProducers = Collections.singletonList(
//                new CloneThenMutateNeighborProducer(
//                        new MostEncodedRepresentationCloner(Cloneable.cloner()),
//                        new MostEncodedRepresentationMutator(
//                                genome -> ((RandomKeyVector) genome).randomize(random)
//                        )
//                )
//        );

        // create neighbor producer
        NeighborsProducer neighborsProducer = new NeighborsProducer(randomizeOneKeyNeighborProducers);

        // create evaluator
        Function<RandomKeyVector, Slots> genomeDecoder = genomeType.newGenomeDecoder(cppInstance);
        Function<Slots, CppSolution> intermediateDecoder = new SlotsDecoder(cppInstance)::decode;
        Function<CppSolution, DoubleCost> phenotypeEvaluator = new CppSolutionEvaluator(cppInstance)::evaluate;
        Evaluator evaluator = new OneIntermediateGeneticEvaluator<>(
                genomeDecoder, intermediateDecoder, phenotypeEvaluator
        );

        // create thread pool
        this.threadPool = Executors.newFixedThreadPool(threadsCount);

        // create best solution keepers
        EarliestSingleBestKeeper localBestKeeper = new EarliestSingleBestKeeper(false);
        this.bestKeeper = new EarliestSingleBestKeeper(printNewBetterSolutions);

        // create parallel hill climbing
        HillClimbingOpti parallelHillClimbing = new ParallelHillClimbingOpti(
                randomSolutionSupplier.get(),
                neighborsProducer,
                evaluator,
                HillClimbing.Variant.STEEPEST_ASCENT,
                threadPool,
                Arrays.asList(localBestKeeper::submit, bestKeeper::submit)
        );

        // random restart condition
        BooleanSupplier randomRestartDecider = () -> localBestKeeper.isDurationSinceLastKeptMoreThan(restartAfterDurationWithoutImprovement);

        // create random-restart hill climbing
        Opti randomRestartHillClimbing = new RandomRestartHillClimbingOpti(
                parallelHillClimbing, randomSolutionSupplier, randomRestartDecider, localBestKeeper::reset
        );

        // create RKHC opti runner
        this.rkhcRunner = new OptiRunner(randomRestartHillClimbing);
    }

    public long runForIterations(long iterationsCount) {
        try {
            return rkhcRunner.runForIterations(iterationsCount);
        } catch (Exception ex) {
            // shutdown thread pool kindly
            shutdownThreadPool();
            ex.printStackTrace();
            throw new IllegalStateException("Stopped RKHC or error.");
        }
    }

    public long runForAtLeast(Duration runDuration) {
        try {
            return rkhcRunner.runForAtLeast(runDuration);
        } catch (Exception ex) {
            shutdownThreadPool();
            ex.printStackTrace();
            throw new IllegalStateException("Stopped RKHC or error.");
        }
    }

    public EvaluatedSolution getBestSolution() {
        return bestKeeper.getBestSolution();
    }

    public <TQuality extends Quality> TQuality getBestQuality() {
        return bestKeeper.getBestQuality();
    }

    public void shutdownThreadPool() {
        // shutdown thread pool kindly
        threadPool.shutdown();
        // wait for thread pool to shutdown
        try {
            threadPool.awaitTermination(1L, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
        }
        // force shutdown if needed
        if (!threadPool.isShutdown()) {
            threadPool.shutdownNow();
        }
    }
}
