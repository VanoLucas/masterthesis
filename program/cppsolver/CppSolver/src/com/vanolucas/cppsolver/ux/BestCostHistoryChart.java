package com.vanolucas.cppsolver.ux;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.time.Duration;
import java.time.Instant;

public class BestCostHistoryChart {
    private static final int WIDTH = 800;
    private static final int HEIGHT = 600;

    private final Runnable onWindowClosed;

    private final XYSeries series = new XYSeries("Best solution cost");

    private Instant firstPoint;

    public BestCostHistoryChart(Runnable onWindowClosed) {
        this.onWindowClosed = onWindowClosed;
    }

    public void display() {
        JFrame f = new JFrame("RKHC CPP Solver");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent windowEvent) {
                onWindowClosed.run();
                windowEvent.getWindow().dispose();
            }
        });
        f.add(createChartPane(), BorderLayout.CENTER);
        f.setSize(WIDTH, HEIGHT);
        f.pack();
        f.setLocationRelativeTo(null);
        f.setVisible(true);
    }

    public void addValue(double newCostValue) {
        if (this.firstPoint == null) {
            this.firstPoint = Instant.now();
        }
        Duration sinceFirstPoint = Duration.between(this.firstPoint, Instant.now());
        series.add(sinceFirstPoint.getSeconds(), newCostValue);
    }

    private ChartPanel createChartPane() {
        XYSeriesCollection dataset = new XYSeriesCollection(series);
        JFreeChart chart = ChartFactory.createXYLineChart(
                "Best solution cost", "Time", "Cost",
                dataset, PlotOrientation.VERTICAL, false, false, false
        );
        ((NumberAxis) chart.getXYPlot().getRangeAxis()).setAutoRangeIncludesZero(false);
        return new ChartPanel(chart) {{
            setSize(new Dimension(WIDTH, HEIGHT));
        }};
    }
}
