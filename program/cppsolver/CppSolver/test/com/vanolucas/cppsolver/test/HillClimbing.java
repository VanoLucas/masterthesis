package com.vanolucas.cppsolver.test;

import com.vanolucas.cppsolver.eval.CppSolutionEvaluator;
import com.vanolucas.cppsolver.eval.SlotsDecoder;
import com.vanolucas.cppsolver.genome.GenomeCf;
import com.vanolucas.cppsolver.genome.decode.GenomeCfDecoder;
import com.vanolucas.cppsolver.instance.StandardCppInstance;
import com.vanolucas.cppsolver.representation.Demand;
import com.vanolucas.cppsolver.representation.Slots;
import com.vanolucas.cppsolver.solution.CppSolution;
import com.vanolucas.cppsolver.ux.DemandFileChooser;
import com.vanolucas.opti.algo.HillClimbingOpti;
import com.vanolucas.opti.algo.Opti;
import com.vanolucas.opti.algo.OptiRunner;
import com.vanolucas.opti.eval.Evaluator;
import com.vanolucas.opti.eval.OneIntermediateGeneticEvaluator;
import com.vanolucas.opti.genome.RandomKeyVector;
import com.vanolucas.opti.mutation.MostEncodedRepresentationMutator;
import com.vanolucas.opti.mutation.neighborhood.NeighborProducer;
import com.vanolucas.opti.mutation.neighborhood.NeighborsProducer;
import com.vanolucas.opti.quality.DoubleCost;
import com.vanolucas.opti.solution.GeneticSolution;
import com.vanolucas.opti.solution.Solution;
import com.vanolucas.opti.solution.keeper.EarliestSingleBestKeeper;
import com.vanolucas.opti.solution.keeper.SingleBestKeeper;

import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.vanolucas.opti.algo.HillClimbing.Variant;

public class HillClimbing {
    public static void main(String[] args) throws IOException {
        // cpp instance
        Demand demand = DemandFileChooser.loadInstanceOrChooseFile("I013");
        int platesCount = 10;
        int slotsPerPlateCount = 4;
        StandardCppInstance cppInstance = new StandardCppInstance(
                demand, platesCount, slotsPerPlateCount
        );

        // initial solution
        RandomKeyVector initialGenome = new GenomeCf(cppInstance);
        initialGenome.randomize();
        Solution initialSolution = new GeneticSolution(initialGenome);

        // randomize 1 key genome mutators
        Random random = new Random();
        Function<Integer, UnaryOperator<RandomKeyVector>> randomizeOneKeyRkvMutatorFactory = keyIndex ->
                rkv -> rkv.randomizeKey(keyIndex, random);
        List<NeighborProducer> neighborProducers = IntStream.range(0, initialGenome.getLength())
                .mapToObj(keyIndex -> new NeighborProducer(
                        new MostEncodedRepresentationMutator(
                                randomizeOneKeyRkvMutatorFactory.apply(keyIndex)
                        )
                ))
                .collect(Collectors.toList());
        NeighborsProducer neighborsProducer = new NeighborsProducer(neighborProducers);

        // evaluator
        Function<GenomeCf, Slots> genomeDecoder = new GenomeCfDecoder(cppInstance)::decode;
        Function<Slots, CppSolution> intermediateDecoder = new SlotsDecoder(cppInstance)::decode;
        Function<CppSolution, DoubleCost> phenotypeEvaluator = new CppSolutionEvaluator(cppInstance)::evaluate;
        Evaluator evaluator = new OneIntermediateGeneticEvaluator<>(
                genomeDecoder, intermediateDecoder, phenotypeEvaluator
        );

        // best solution keeper
        SingleBestKeeper bestKeeper = new EarliestSingleBestKeeper();

        // hill climbing opti algo
        Variant variant = Variant.STEEPEST_ASCENT;
        Opti hillClimbingOpti = new HillClimbingOpti(
                initialSolution, neighborsProducer, evaluator, variant, bestKeeper::submit
        );

        // opti runner
        OptiRunner optiRunner = new OptiRunner(hillClimbingOpti);

        // run
        optiRunner.runForIterations(100L);

        // print best
        bestKeeper.printBest();
    }
}
