package com.vanolucas.cppsolver.test;

import com.vanolucas.cppsolver.eval.CppSolutionEvaluator;
import com.vanolucas.cppsolver.eval.SlotsDecoder;
import com.vanolucas.cppsolver.genome.GenomeCf;
import com.vanolucas.cppsolver.genome.decode.GenomeCfDecoder;
import com.vanolucas.cppsolver.instance.StandardCppInstance;
import com.vanolucas.cppsolver.representation.Demand;
import com.vanolucas.cppsolver.representation.Slots;
import com.vanolucas.cppsolver.solution.CppSolution;
import com.vanolucas.cppsolver.ux.DemandFileChooser;
import com.vanolucas.opti.algo.*;
import com.vanolucas.opti.algo.HillClimbing;
import com.vanolucas.opti.clone.Cloneable;
import com.vanolucas.opti.clone.MostEncodedRepresentationCloner;
import com.vanolucas.opti.eval.Evaluator;
import com.vanolucas.opti.eval.OneIntermediateGeneticEvaluator;
import com.vanolucas.opti.genome.RandomKeyVector;
import com.vanolucas.opti.mutation.MostEncodedRepresentationMutator;
import com.vanolucas.opti.mutation.neighborhood.CloneThenMutateNeighborProducer;
import com.vanolucas.opti.mutation.neighborhood.NeighborProducer;
import com.vanolucas.opti.mutation.neighborhood.NeighborsProducer;
import com.vanolucas.opti.quality.DoubleCost;
import com.vanolucas.opti.solution.GeneticSolution;
import com.vanolucas.opti.solution.Solution;
import com.vanolucas.opti.solution.keeper.EarliestSingleBestKeeper;
import com.vanolucas.opti.solution.keeper.SingleBestKeeper;

import java.io.IOException;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.BooleanSupplier;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Rkhc {
    public static void main(String[] args) throws IOException {
        // cpp instance params
        Demand demand = DemandFileChooser.loadInstanceOrChooseFile("I013");
        int platesCount = 10;
        int slotsPerPlateCount = 4;

        // cpp instance
        StandardCppInstance cppInstance = new StandardCppInstance(
                demand, platesCount, slotsPerPlateCount
        );
        System.out.println(cppInstance);

        // random solution supplier
        Random random = new Random();
        Supplier<RandomKeyVector> randomGenomeSupplier = () -> new GenomeCf(cppInstance).randomize(random);
        Supplier<Solution> randomSolutionSupplier = () -> new GeneticSolution(randomGenomeSupplier.get());

        // initial solution
        Solution initialSolution = randomSolutionSupplier.get();
        System.out.println(String.format("Initial solution:%n%s", initialSolution));

        // randomize 1 key genome mutators
        Function<Integer, UnaryOperator<RandomKeyVector>> randomizeOneKeyRkvMutatorFactory = keyIndex ->
                rkv -> rkv.randomizeKey(keyIndex, random);
        List<NeighborProducer> randomizeOneKeyNeighborProducers = IntStream.range(0, randomGenomeSupplier.get().getLength())
                .mapToObj(keyIndex -> new CloneThenMutateNeighborProducer(
                        new MostEncodedRepresentationCloner(Cloneable.cloner()),
                        new MostEncodedRepresentationMutator(
                                randomizeOneKeyRkvMutatorFactory.apply(keyIndex)
                        )
                ))
                .collect(Collectors.toList());

        // neighbors producer: neighboring operators
        NeighborsProducer neighborsProducer = new NeighborsProducer(randomizeOneKeyNeighborProducers);

        // evaluator
        Function<GenomeCf, Slots> genomeDecoder = new GenomeCfDecoder(cppInstance)::decode;
        Function<Slots, CppSolution> intermediateDecoder = new SlotsDecoder(cppInstance)::decode;
        Function<CppSolution, DoubleCost> phenotypeEvaluator = new CppSolutionEvaluator(cppInstance)::evaluate;
        Evaluator evaluator = new OneIntermediateGeneticEvaluator<>(
                genomeDecoder, intermediateDecoder, phenotypeEvaluator
        );

        // best solution keeper
        SingleBestKeeper overallBestKeeper = new EarliestSingleBestKeeper();
        SingleBestKeeper localBestKeeper = new EarliestSingleBestKeeper(true);

        // thread pool
        ExecutorService executor = Executors.newFixedThreadPool(4);

        // hill climbing opti algo
        final boolean parallel = true;
        HillClimbingOpti hillClimbingOpti;
        if (parallel) {
            hillClimbingOpti = new ParallelHillClimbingOpti(
                    initialSolution, neighborsProducer, evaluator,
                    HillClimbing.Variant.STEEPEST_ASCENT,
                    executor,
                    Arrays.asList(overallBestKeeper::submit, localBestKeeper::submit)
            );
        } else {
            hillClimbingOpti = new HillClimbingOpti(
                    initialSolution, neighborsProducer, evaluator,
                    HillClimbing.Variant.STEEPEST_ASCENT,
                    Arrays.asList(overallBestKeeper::submit, localBestKeeper::submit)
            );
        }

        // random restart after duration without improvement
        final Duration maxDurationWithoutImprovement = Duration.ofSeconds(5L);

        // random restart condition
        BooleanSupplier randomRestartDecider = () -> localBestKeeper.isDurationSinceLastKeptMoreThan(maxDurationWithoutImprovement);

        // random restart hill climbing opti algo
        RandomRestartHillClimbingOpti randomRestartHillClimbingOpti = new RandomRestartHillClimbingOpti(
                hillClimbingOpti, randomSolutionSupplier, randomRestartDecider, localBestKeeper::reset
        );

        // opti runner
        OptiRunner optiRunner = new OptiRunner(randomRestartHillClimbingOpti);

        // run
        long lastIteration = optiRunner.runForAtLeast(Duration.ofSeconds(15L));

        // shutdown thread pool
        executor.shutdown();

        // print stats
        System.out.println("Overall best:");
        overallBestKeeper.printBest();
        System.out.println(String.format("Last iteration: %d", lastIteration));
    }
}
