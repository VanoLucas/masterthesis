package com.vanolucas.opti.algo;

import com.vanolucas.opti.eval.Evaluator;
import com.vanolucas.opti.mutation.neighborhood.NeighborsProducer;
import com.vanolucas.opti.solution.EvaluatedSolution;
import com.vanolucas.opti.solution.Solution;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

public class HillClimbingOpti implements Opti {
    private final HillClimbing.Variant variant;
    protected Solution currentSolution;
    protected final NeighborsProducer neighborsProducer;
    protected final Evaluator evaluator;
    private final List<Consumer<EvaluatedSolution>> onNewCurrentSolutionListeners;

    public HillClimbingOpti(Solution initialSolution, NeighborsProducer neighborsProducer, Evaluator evaluator, HillClimbing.Variant variant) {
        this(initialSolution, neighborsProducer, evaluator, variant, (List<Consumer<EvaluatedSolution>>) null);
    }

    public HillClimbingOpti(Solution initialSolution,
                            NeighborsProducer neighborsProducer,
                            Evaluator evaluator,
                            HillClimbing.Variant variant,
                            Consumer<EvaluatedSolution> onNewCurrentSolutionListener) {
        this(initialSolution, neighborsProducer, evaluator, variant, Collections.singletonList(onNewCurrentSolutionListener));
    }

    public HillClimbingOpti(Solution initialSolution,
                            NeighborsProducer neighborsProducer,
                            Evaluator evaluator,
                            HillClimbing.Variant variant,
                            List<Consumer<EvaluatedSolution>> onNewCurrentSolutionListeners) {
        this.variant = variant;
        this.currentSolution = initialSolution;
        this.neighborsProducer = neighborsProducer;
        this.evaluator = evaluator;
        if (onNewCurrentSolutionListeners != null && onNewCurrentSolutionListeners.size() > 0) {
            this.onNewCurrentSolutionListeners = new ArrayList<>(onNewCurrentSolutionListeners);
        } else {
            this.onNewCurrentSolutionListeners = new ArrayList<>(2);
        }
    }

    public void setCurrentSolution(Solution newCurrentSolution) {
        currentSolution = newCurrentSolution;
    }

    @Override
    public void runStep() {
        switch (variant) {
            case SIMPLE:
            case STEEPEST_ASCENT:
            default:
                runSteepestAscentStep();
                break;
        }
    }

    protected void runSteepestAscentStep() {
        updateCurrentSolution(HillClimbing.steepestAscentStep(
                currentSolution, neighborsProducer, evaluator
        ));
    }

    protected void updateCurrentSolution(EvaluatedSolution newCurrentSolution) {
        currentSolution = newCurrentSolution;
        onNewCurrentSolutionListeners.forEach(l -> l.accept(newCurrentSolution));
    }
}
