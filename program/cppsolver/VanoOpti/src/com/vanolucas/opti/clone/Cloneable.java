package com.vanolucas.opti.clone;

import java.util.function.UnaryOperator;

public interface Cloneable<T extends Cloneable> extends java.lang.Cloneable {
    T clone() throws CloneNotSupportedException;

    static <T extends Cloneable<T>> UnaryOperator<T> cloner() {
        return cloneable -> {
            try {
                return cloneable.clone();
            } catch (CloneNotSupportedException e) {
                throw new UnsupportedOperationException(String.format("Clone not supported in class %s.",
                        cloneable.getClass().getSimpleName()
                ));
            }
        };
    }
}
