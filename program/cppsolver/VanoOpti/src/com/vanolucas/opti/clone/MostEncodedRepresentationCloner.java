package com.vanolucas.opti.clone;

import com.vanolucas.opti.solution.Solution;

import java.util.function.UnaryOperator;

public class MostEncodedRepresentationCloner implements Cloner {
    private final UnaryOperator<?> mostEncodedRepresentationCloner;

    public MostEncodedRepresentationCloner(UnaryOperator<?> mostEncodedRepresentationCloner) {
        this.mostEncodedRepresentationCloner = mostEncodedRepresentationCloner;
    }

    @Override
    public Solution clone(Solution solution) {
        return solution.cloneMostEncodedRepresentationUsing(mostEncodedRepresentationCloner);
    }
}
