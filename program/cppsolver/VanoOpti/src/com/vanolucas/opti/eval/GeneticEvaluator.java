package com.vanolucas.opti.eval;

import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.solution.DecodedGeneticSolution;
import com.vanolucas.opti.solution.EvaluatedSolution;
import com.vanolucas.opti.solution.GeneticSolution;
import com.vanolucas.opti.solution.Solution;

import java.util.function.Function;

public class GeneticEvaluator implements Evaluator {
    private final Function<?, ?> genomeDecoder;
    private final Function<?, ? extends Quality> phenotypeEvaluator;

    public <TGenome, TPhenotype> GeneticEvaluator(Function<TGenome, TPhenotype> genomeDecoder, Function<TPhenotype, ? extends Quality> phenotypeEvaluator) {
        this.genomeDecoder = genomeDecoder;
        this.phenotypeEvaluator = phenotypeEvaluator;
    }

    @Override
    public EvaluatedSolution evaluate(Solution solution) {
        return evaluate((GeneticSolution) solution);
    }

    public EvaluatedSolution evaluate(GeneticSolution geneticSolution) {
        return evaluate(
                decode(geneticSolution)
        );
    }

    public EvaluatedSolution evaluate(DecodedGeneticSolution decodedGeneticSolution) {
        return new EvaluatedSolution(
                decodedGeneticSolution,
                decodedGeneticSolution.evaluatePhenotypeUsing(phenotypeEvaluator)
        );
    }

    private DecodedGeneticSolution decode(GeneticSolution geneticSolution) {
        return geneticSolution.decodeUsing(genomeDecoder);
    }
}
