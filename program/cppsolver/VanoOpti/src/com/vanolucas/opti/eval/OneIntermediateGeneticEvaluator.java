package com.vanolucas.opti.eval;

import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.solution.EvaluatedSolution;
import com.vanolucas.opti.solution.GeneticSolution;
import com.vanolucas.opti.solution.OneIntermediateDecodedGeneticSolution;
import com.vanolucas.opti.solution.Solution;

import java.util.function.Function;

public class OneIntermediateGeneticEvaluator<TGenome, TIntermediate, TPhenotype> implements Evaluator {
    private final Function<TGenome, TIntermediate> genomeDecoder;
    private final Function<TIntermediate, TPhenotype> intermediateDecoder;
    private final Function<TPhenotype, ? extends Quality> phenotypeEvaluator;

    public OneIntermediateGeneticEvaluator(Function<TGenome, TIntermediate> genomeDecoder,
                                           Function<TIntermediate, TPhenotype> intermediateDecoder,
                                           Function<TPhenotype, ? extends Quality> phenotypeEvaluator) {
        this.genomeDecoder = genomeDecoder;
        this.intermediateDecoder = intermediateDecoder;
        this.phenotypeEvaluator = phenotypeEvaluator;
    }

    @Override
    public EvaluatedSolution evaluate(Solution solution) {
        return evaluate((GeneticSolution) solution);
    }

    public EvaluatedSolution evaluate(GeneticSolution geneticSolution) {
        return evaluate(
                decode(geneticSolution)
        );
    }

    public EvaluatedSolution evaluate(OneIntermediateDecodedGeneticSolution decodedGeneticSolution) {
        return new EvaluatedSolution(
                decodedGeneticSolution,
                decodedGeneticSolution.evaluatePhenotypeUsing(phenotypeEvaluator)
        );
    }

    private OneIntermediateDecodedGeneticSolution decode(GeneticSolution geneticSolution) {
        return geneticSolution.decodeUsing(genomeDecoder, intermediateDecoder);
    }
}
