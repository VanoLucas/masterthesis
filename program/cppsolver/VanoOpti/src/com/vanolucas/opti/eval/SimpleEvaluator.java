package com.vanolucas.opti.eval;

import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.solution.EvaluatedSolution;
import com.vanolucas.opti.solution.SimpleSolution;
import com.vanolucas.opti.solution.Solution;

import java.util.function.Function;

public class SimpleEvaluator implements Evaluator {
    private final Function<?, ? extends Quality> actualSolutionEvaluator;

    public SimpleEvaluator(Function<?, ? extends Quality> actualSolutionEvaluator) {
        this.actualSolutionEvaluator = actualSolutionEvaluator;
    }

    @Override
    public EvaluatedSolution evaluate(Solution solution) {
        return evaluate((SimpleSolution) solution);
    }

    public EvaluatedSolution evaluate(SimpleSolution simpleSolution) {
        return new EvaluatedSolution(
                simpleSolution,
                simpleSolution.evaluateActualSolutionUsing(actualSolutionEvaluator)
        );
    }
}
