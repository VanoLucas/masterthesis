package com.vanolucas.opti.genome;

import com.vanolucas.opti.clone.Cloneable;
import com.vanolucas.opti.util.rand.Rand;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

public class RandomKeyVector implements Cloneable<RandomKeyVector> {
    private List<Double> keys;

    public RandomKeyVector(int length) {
        this(length, 0d);
    }

    public RandomKeyVector(int length, Double initialValue) {
        this(Collections.nCopies(length, initialValue));
    }

    public RandomKeyVector(List<Double> keys) {
        this.keys = new ArrayList<>(keys);
    }

    public int getLength() {
        return keys.size();
    }

    public Double getKey(int index) {
        return keys.get(index);
    }

    public DoubleStream doubleStream() {
        return keys.stream().mapToDouble(__ -> __);
    }

    public RandomKeyVector randomize() {
        return randomize(new Random());
    }

    public RandomKeyVector randomize(Random random) {
        if (random == null) {
            throw new IllegalArgumentException("A random numbers generator must be provided.");
        }
        final int length = getLength();
        keys.clear();
        keys.addAll(Rand.doubles01(length, random));
        return this;
    }

    public RandomKeyVector randomizeKey(int keyIndex, Random random) {
        if (random == null) {
            throw new IllegalArgumentException("A random numbers generator must be provided.");
        }
        keys.set(keyIndex, Rand.double01(random));
        return this;
    }

    @Override
    public RandomKeyVector clone() throws CloneNotSupportedException {
        RandomKeyVector cloned = (RandomKeyVector) super.clone();
        cloned.keys = new ArrayList<>(keys);
        return cloned;
    }

    @Override
    public String toString() {
        return keys.stream()
                .map(Object::toString)
                .collect(Collectors.joining(" ; "));
    }
}
