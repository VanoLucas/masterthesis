package com.vanolucas.opti.mutation;

import com.vanolucas.opti.solution.Solution;

import java.util.function.UnaryOperator;

public class MostEncodedRepresentationMutator implements Mutator {
    private final UnaryOperator<?> mostEncodedRepresentationMutator;

    public <TGenome> MostEncodedRepresentationMutator(UnaryOperator<TGenome> mostEncodedRepresentationMutator) {
        this.mostEncodedRepresentationMutator = mostEncodedRepresentationMutator;
    }

    @Override
    public Solution mutate(Solution solution) {
        return solution.mutateMostEncodedRepresentationUsing(mostEncodedRepresentationMutator);
    }
}
