package com.vanolucas.opti.mutation.neighborhood;

import com.vanolucas.opti.clone.Cloner;
import com.vanolucas.opti.mutation.Mutator;
import com.vanolucas.opti.solution.Solution;

public class CloneThenMutateNeighborProducer extends NeighborProducer {
    private final Cloner cloner;

    public CloneThenMutateNeighborProducer(Cloner cloner, Mutator neighboringOperator) {
        super(neighboringOperator);
        this.cloner = cloner;
    }

    @Override
    public Solution produceNeighbor(Solution srcSolution) {
        return super.produceNeighbor(
                cloner.clone(srcSolution)
        );
    }
}
