package com.vanolucas.opti.mutation.neighborhood;

import com.vanolucas.opti.eval.Evaluator;
import com.vanolucas.opti.mutation.Mutator;
import com.vanolucas.opti.solution.EvaluatedSolution;
import com.vanolucas.opti.solution.Solution;

public class NeighborProducer {
    private final Mutator neighboringOperator;

    public NeighborProducer(Mutator neighboringOperator) {
        this.neighboringOperator = neighboringOperator;
    }

    public Solution produceNeighbor(Solution srcSolution) {
        return neighboringOperator.mutate(srcSolution);
    }

    public EvaluatedSolution produceAndEvaluateNeighbor(Solution srcSolution, Evaluator evaluator) {
        return evaluator.evaluate(
                produceNeighbor(srcSolution)
        );
    }
}
