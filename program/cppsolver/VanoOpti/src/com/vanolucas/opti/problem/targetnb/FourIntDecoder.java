package com.vanolucas.opti.problem.targetnb;

public interface FourIntDecoder {
    static TwoInt decode(FourInt fourInt) {
        return new TwoInt(fourInt.getSumLeft(), fourInt.getSumRight());
    }
}
