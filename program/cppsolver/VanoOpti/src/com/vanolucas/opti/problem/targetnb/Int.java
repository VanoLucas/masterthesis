package com.vanolucas.opti.problem.targetnb;

public class Int {
    private final int value;

    public Int(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public int getDistanceTo(int target) {
        return Math.abs(value - target);
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}