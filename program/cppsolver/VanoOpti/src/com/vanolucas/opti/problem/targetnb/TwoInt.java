package com.vanolucas.opti.problem.targetnb;

public class TwoInt {
    private int value1;
    private int value2;

    public TwoInt(int value1, int value2) {
        this.value1 = value1;
        this.value2 = value2;
    }

    public int getSum() {
        return value1 + value2;
    }

    public void incrementValue1() {
        value1++;
    }

    public void incrementValue2() {
        value2++;
    }

    public void decrementValue1() {
        value1--;
    }

    public void decrementValue2() {
        value2--;
    }

    @Override
    public String toString() {
        return String.format("%3d;%3d", value1, value2);
    }
}
