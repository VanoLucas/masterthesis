package com.vanolucas.opti.quality;

public class DoubleCost extends Objective<Double> {
    public static final DoubleCost ZERO = new DoubleCost(0d);

    public DoubleCost(Double value) {
        super(value);
    }

    @Override
    public boolean isBetterThan(Double otherValue) {
        return value < otherValue;
    }

    @Override
    public String toString() {
        return String.format("Cost: %s", super.toString());
    }
}
