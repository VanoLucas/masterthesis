package com.vanolucas.opti.quality;

public interface Quality extends Comparable<Quality> {
    boolean isBetterThan(Quality other);

    boolean isEquivalentTo(Quality other);

    default boolean isBetterOrEquivalentTo(Quality other) {
        return isBetterThan(other) || isEquivalentTo(other);
    }

    @Override
    default int compareTo(Quality other) {
        if (isBetterThan(other)) {
            return 1;
        } else if (isEquivalentTo(other)) {
            return 0;
        } else {
            return -1;
        }
    }

    @Override
    String toString();
}
