package com.vanolucas.opti.solution;

import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.util.any.Any;
import com.vanolucas.opti.util.any.Some;

import java.util.function.Function;

public class DecodedGeneticSolution extends GeneticSolution {
    private final Any phenotype;

    public <TGenome, TPhenotype> DecodedGeneticSolution(TGenome genome, TPhenotype phenotype) {
        this(new Some<>(genome), new Some<>(phenotype));
    }

    public DecodedGeneticSolution(Any genome, Any phenotype) {
        super(genome);
        this.phenotype = phenotype;
    }

    public <TPhenotype, TQuality extends Quality> TQuality evaluatePhenotypeUsing(Function<TPhenotype, TQuality> phenotypeEvaluator) {
        return phenotypeEvaluator.apply(phenotype.get());
    }

    public String getPhenotypeStr() {
        return String.format("Phenotype (%s):%n%s", phenotype.getClassSimpleName(), phenotype);
    }

    @Override
    public String toString() {
        return String.format("%s%n%s", getGenomeStr(), getPhenotypeStr());
    }
}
