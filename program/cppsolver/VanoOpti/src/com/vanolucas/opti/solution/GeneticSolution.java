package com.vanolucas.opti.solution;

import com.vanolucas.opti.util.any.Any;
import com.vanolucas.opti.util.any.Some;

import java.util.function.Function;
import java.util.function.UnaryOperator;

public class GeneticSolution implements Solution {
    private final Any genome;

    public <T> GeneticSolution(T genome) {
        this(new Some<>(genome));
    }

    public GeneticSolution(Any genome) {
        this.genome = genome;
    }

    @Override
    public <TGenome> GeneticSolution cloneMostEncodedRepresentationUsing(UnaryOperator<TGenome> genomeCloner) {
        return new GeneticSolution(genomeCloner.apply(genome.get()));
    }

    @Override
    public <TGenome> GeneticSolution mutateMostEncodedRepresentationUsing(UnaryOperator<TGenome> genomeMutator) {
        return new GeneticSolution(genomeMutator.apply(genome.get()));
    }

    public <TGenome, TPhenotype> DecodedGeneticSolution decodeUsing(Function<TGenome, TPhenotype> genomeDecoder) {
        return new DecodedGeneticSolution(genome.get(), genomeDecoder.apply(genome.get()));
    }

    public <TGenome, TIntermediate, TPhenotype> OneIntermediateDecodedGeneticSolution decodeUsing(Function<TGenome, TIntermediate> genomeDecoder,
                                                                                                  Function<TIntermediate, TPhenotype> intermediateDecoder) {
        TIntermediate intermediate = genomeDecoder.apply(genome.get());
        TPhenotype phenotype = intermediateDecoder.apply(intermediate);
        return new OneIntermediateDecodedGeneticSolution(genome.get(), intermediate, phenotype);
    }

    public String getGenomeStr() {
        return String.format("Genome (%s):%n%s", genome.getClassSimpleName(), genome);
    }

    @Override
    public String toString() {
        return getGenomeStr();
    }
}
