package com.vanolucas.opti.solution;

import com.vanolucas.opti.util.any.Any;
import com.vanolucas.opti.util.any.Some;

public class OneIntermediateDecodedGeneticSolution extends DecodedGeneticSolution {
    private final Any intermediate;

    public <TGenome, TIntermediate, TPhenotype> OneIntermediateDecodedGeneticSolution(TGenome genome, TIntermediate intermediate, TPhenotype tPhenotype) {
        this(new Some<>(genome), new Some<>(intermediate), new Some<>(tPhenotype));
    }

    public OneIntermediateDecodedGeneticSolution(Any genome, Any intermediate, Any phenotype) {
        super(genome, phenotype);
        this.intermediate = intermediate;
    }

    public String getIntermediateStr() {
        return String.format("Intermediate representation (%s):%n%s", intermediate.getClassSimpleName(), intermediate);
    }

    @Override
    public String toString() {
        return String.format("%s%n%s%n%s", getGenomeStr(), getIntermediateStr(), getPhenotypeStr());
    }
}
