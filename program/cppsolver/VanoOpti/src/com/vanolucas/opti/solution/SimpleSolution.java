package com.vanolucas.opti.solution;

import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.util.any.Any;
import com.vanolucas.opti.util.any.Some;

import java.util.function.Function;
import java.util.function.UnaryOperator;

public class SimpleSolution implements Solution {
    private final Any actualSolution;

    public <T> SimpleSolution(T actualSolution) {
        this(new Some<>(actualSolution));
    }

    public SimpleSolution(Any actualSolution) {
        this.actualSolution = actualSolution;
    }

    @Override
    public <TActualSolution> SimpleSolution cloneMostEncodedRepresentationUsing(UnaryOperator<TActualSolution> actualSolutionCloner) {
        return new SimpleSolution(actualSolutionCloner.apply(actualSolution.get()));
    }

    @Override
    public <TActualSolution> SimpleSolution mutateMostEncodedRepresentationUsing(UnaryOperator<TActualSolution> actualSolutionMutator) {
        return new SimpleSolution(actualSolutionMutator.apply(actualSolution.get()));
    }

    public <TActualSolution, TQuality extends Quality> TQuality evaluateActualSolutionUsing(Function<TActualSolution, TQuality> actualSolutionEvaluator) {
        return actualSolutionEvaluator.apply(actualSolution.get());
    }
}
