package com.vanolucas.opti.solution.keeper;

import com.vanolucas.opti.solution.EvaluatedSolution;

public class EarliestSingleBestKeeper extends SingleBestKeeper {
    public EarliestSingleBestKeeper() {
        super();
    }

    public EarliestSingleBestKeeper(boolean printKeptSolutions) {
        super(printKeptSolutions);
    }

    @Override
    protected boolean shouldReplaceBestWith(EvaluatedSolution candidate) {
        return candidate.isBetterThan(getBest());
    }
}
