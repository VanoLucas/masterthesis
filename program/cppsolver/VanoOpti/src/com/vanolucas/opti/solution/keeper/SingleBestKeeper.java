package com.vanolucas.opti.solution.keeper;

import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.solution.EvaluatedSolution;

public abstract class SingleBestKeeper extends Keeper {
    private EvaluatedSolution best;

    public SingleBestKeeper() {
        super();
    }

    public SingleBestKeeper(boolean printKeptSolutions) {
        super(printKeptSolutions);
    }

    @Override
    protected Decision makeDecision(EvaluatedSolution candidate) {
        if (shouldReplaceBestWith(candidate)) {
            return Decision.KEPT;
        } else {
            return Decision.REJECTED;
        }
    }

    protected abstract boolean shouldReplaceBestWith(EvaluatedSolution candidate);

    @Override
    protected void keep(EvaluatedSolution keptSolution) {
        best = keptSolution;
    }

    protected EvaluatedSolution getBest() {
        return best;
    }

    public <TQuality extends Quality> TQuality getBestQuality() {
        if (best == null) {
            return null;
        }
        return (TQuality) best.getQuality();
    }

    public String getBestStr() {
        return String.format("Best solution:%n%s", best);
    }

    @Override
    public void reset() {
        super.reset();
        best = null;
    }

    public void printBest() {
        System.out.println(getBestStr());
    }

    @Override
    public String toString() {
        return getBestStr();
    }

    public EvaluatedSolution getBestSolution() {
        return best;
    }
}
