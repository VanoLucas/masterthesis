package com.vanolucas.opti.solution.keeper;

import com.vanolucas.opti.solution.EvaluatedSolution;

import java.time.Duration;

public class ThreadSafeKeeper extends Keeper {
    private final Keeper keeper;

    public ThreadSafeKeeper(Keeper keeper) {
        this.keeper = keeper;
    }

    @Override
    protected Decision makeDecision(EvaluatedSolution candidate) {
        synchronized (keeper) {
            return keeper.makeDecision(candidate);
        }
    }

    @Override
    public Decision submit(EvaluatedSolution candidate) {
        synchronized (keeper) {
            return keeper.submit(candidate);
        }
    }

    @Override
    public void reset() {
        synchronized (keeper) {
            keeper.reset();
        }
    }

    @Override
    public boolean isDurationSinceLastKeptMoreThan(Duration duration) {
        synchronized (keeper) {
            return keeper.isDurationSinceLastKeptMoreThan(duration);
        }
    }
}
