package com.vanolucas.opti.util.any;

public interface Any {
    <T> T get();

    String getClassSimpleName();
}
