package com.vanolucas.opti.util.any;

public class Some<T> implements Any {
    private final T item;

    public Some(T item) {
        this.item = item;
    }

    @SuppressWarnings("unchecked")
    @Override
    public T get() {
        return item;
    }

    @Override
    public String getClassSimpleName() {
        return item.getClass().getSimpleName();
    }

    @Override
    public String toString() {
        return item.toString();
    }
}
