package com.vanolucas.opti.util.math;

import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.IntStream;

public class CutCake {
    /**
     * Divide a given integer (cakeSize) in multiple integers (shares) according to relative weights and
     * such that the sum of shares equals cakeSize.
     *
     * @param cakeSize     Integer to split.
     * @param shareWeights Relative weights that define the size of each share to produce.
     * @return The shares (pieces of the initial cake).
     */
    public static int[] getShares(int cakeSize, double[] shareWeights) {
        final int nbShares = shareWeights.length;
        int[] shares = new int[nbShares];

        // get indexes of weights sorted by weight descending
        int[] sortedWeightIndexes = IntStream.range(0, nbShares)
                .boxed()
                .sorted(Comparator.comparingDouble(i -> shareWeights[(int) i]).reversed())
                .mapToInt(__ -> __)
                .toArray();

        // remaining qty of cake to cut
        int remainingCake = cakeSize;
        // remaining weight to process
        double remainingWeight = Arrays.stream(shareWeights).sum();

        // for each piece of cake to cut (from biggest to smallest) except the last
        for (int i = 0; i < nbShares - 1; i++) {
            // get current share index
            final int shareIndex = sortedWeightIndexes[i];
            // get current share weight
            final double weight = shareWeights[shareIndex];

            // cut the slice
            final int share = (int) Math.round(weight / remainingWeight * remainingCake);
            shares[shareIndex] = share;

            // update remaining cake
            remainingCake -= share;
            // update remaining weight
            remainingWeight -= weight;
        }

        // the last (and smallest) piece is the remaining cake
        final int lastShareIndex = sortedWeightIndexes[nbShares - 1];
        shares[lastShareIndex] = remainingCake;

        // return sliced cake
        return shares;
    }
}
