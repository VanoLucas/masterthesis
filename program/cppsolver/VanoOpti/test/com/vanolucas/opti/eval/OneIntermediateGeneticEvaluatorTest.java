package com.vanolucas.opti.eval;

import org.junit.jupiter.api.Test;
import com.vanolucas.opti.problem.targetnb.FourInt;
import com.vanolucas.opti.problem.targetnb.FourIntDecoder;
import com.vanolucas.opti.problem.targetnb.IntEvaluator;
import com.vanolucas.opti.problem.targetnb.TwoIntDecoder;
import com.vanolucas.opti.solution.EvaluatedSolution;
import com.vanolucas.opti.solution.GeneticSolution;

class OneIntermediateGeneticEvaluatorTest {

    @Test
    void evaluate() {
        GeneticSolution geneticSolution = new GeneticSolution(
                new FourInt(0, 0, 0, 0)
        );

        IntEvaluator intEvaluator = new IntEvaluator(100);
        OneIntermediateGeneticEvaluator evaluator = new OneIntermediateGeneticEvaluator<>(
                FourIntDecoder::decode,
                TwoIntDecoder::decode,
                intEvaluator::evaluate
        );

        EvaluatedSolution evaluatedSolution = evaluator.evaluate(geneticSolution);

        System.out.println(evaluatedSolution);
    }
}