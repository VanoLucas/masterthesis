package com.vanolucas.opti.solution;

import com.vanolucas.opti.problem.targetnb.FourInt;
import com.vanolucas.opti.problem.targetnb.TwoInt;
import org.junit.jupiter.api.Test;
import com.vanolucas.opti.problem.targetnb.Int;

class OneIntermediateDecodedGeneticSolutionTest {

    @Test
    void toString1() {
        OneIntermediateDecodedGeneticSolution solution = new OneIntermediateDecodedGeneticSolution(
                new FourInt(0, 0, 0, 0),
                new TwoInt(0, 0),
                new Int(0)
        );
        System.out.println(solution);
    }
}