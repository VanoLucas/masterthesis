package com.vanolucas.cppsolver.genome;

import com.vanolucas.cppsolver.problem.GeneticCoverPrintingProblem;
import com.vanolucas.cppsolver.solution.CppSolution;
import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.genome.decoder.GenomeDecoder;
import com.vanolucas.opti.solution.CloneablePhenotype;

public abstract class CppGenomeDecoder extends GenomeDecoder {

    protected GeneticCoverPrintingProblem problem;

    public CppGenomeDecoder(GeneticCoverPrintingProblem problem) {
        this.problem = problem;
    }

    @Override
    public CloneablePhenotype decodeGenome(Genome genome) {
        return new CloneablePhenotype(decodeCppGenome(genome));
    }

    public abstract CppSolution decodeCppGenome(Genome genome);
}
