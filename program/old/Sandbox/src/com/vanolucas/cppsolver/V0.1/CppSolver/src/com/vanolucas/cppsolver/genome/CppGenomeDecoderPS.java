package com.vanolucas.cppsolver.genome;

import com.vanolucas.cppsolver.problem.GeneticCoverPrintingProblem;
import com.vanolucas.cppsolver.solution.BookCoverDemandPart;
import com.vanolucas.cppsolver.solution.CppSolution;
import com.vanolucas.opti.genome.DoubleGenome;
import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.genome.IntGenome;
import com.vanolucas.opti.math.Scale;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class CppGenomeDecoderPS extends CppGenomeDecoder {

    public CppGenomeDecoderPS(GeneticCoverPrintingProblem problem) {
        super(problem);
    }

    @Override
    public CppSolution decodeCppGenome(Genome genome) {

        List<Double> keys;

        // from Double genome
        if (genome instanceof DoubleGenome) {
            DoubleGenome g = (DoubleGenome) genome;
            keys = g.getGenes();
        }

        // else from Integer genome
        else {
            IntGenome g = (IntGenome) genome;
            keys = g.getGenes().stream()
                    .mapToDouble(Integer::doubleValue)
                    .boxed()
                    .collect(Collectors.toList());
        }

        // list that will contain the nb of printings for each book allocated to each slot
        // it starts with 1 single slot for each book cover
        List<BookCoverDemandPart> bookCoverDemandParts = problem.getBookCoverDemands().stream()
                .map(bookCover -> new BookCoverDemandPart(bookCover, bookCover.getDemand()))
                .collect(Collectors.toList());

        final int NB_BOOK_COVERS = bookCoverDemandParts.size();

        // for each extra slot to fill
        for (int k = 0; k < keys.size(); k += 2) {
            // read params from genome
            final double keyPartToSplit = keys.get(k);
            final double cutFractionSize = keys.get(k + 1);

            // calculate index of the book cover demand to split over two slots
//            final int indexToSplit = ((int) Scale.scale(keyPartToSplit, 1.0d, bookCoverDemandParts.size())); // performs worse
            final int indexToSplit = (int) Scale.scale(keyPartToSplit, 1.0d, NB_BOOK_COVERS);

            // split chosen book cover demand over two slots, with provided cutSize
            bookCoverDemandParts.add(
                    bookCoverDemandParts.get(indexToSplit)
                            .splitInTwo(cutFractionSize)
            );
        }

        /*
        Sort demand splits by number of printings descending
         */

        // sort by number of printings so that we group slots with similar
        // required number of printings on the same offset plates
        Collections.sort(bookCoverDemandParts, Collections.reverseOrder());

        /*
        Create the CppSolution object to store this decoded solution
         */

        return new CppSolution(problem, bookCoverDemandParts);
    }
}
