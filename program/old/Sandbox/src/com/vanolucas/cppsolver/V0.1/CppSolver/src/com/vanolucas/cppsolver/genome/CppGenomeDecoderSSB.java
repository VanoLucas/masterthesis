package com.vanolucas.cppsolver.genome;

import com.vanolucas.cppsolver.problem.GeneticCoverPrintingProblem;
import com.vanolucas.cppsolver.solution.BookCoverDemand;
import com.vanolucas.cppsolver.solution.BookCoverDemandPart;
import com.vanolucas.cppsolver.solution.CppSolution;
import com.vanolucas.opti.genome.DoubleGenome;
import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.genome.IntGenome;
import com.vanolucas.opti.math.CutInteger;
import com.vanolucas.opti.math.Scale;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class CppGenomeDecoderSSB extends CppGenomeDecoder {

    public CppGenomeDecoderSSB(GeneticCoverPrintingProblem problem) {
        super(problem);
    }

    @Override
    public CppSolution decodeCppGenome(Genome genome) {

        /*
        Get book cover demands from the problem instance
         */

        final List<BookCoverDemand> bookCovers = problem.getBookCoverDemands();

        /*
        Get useful params
         */

        final int NB_BOOK_COVERS = problem.getNbBookCovers();
        final int TOTAL_NB_SLOTS = problem.getTotalNbSlots();

        /*
        Extract keys (weights) from the two parts of the S-SB genome
         */

        // for each book cover, the key that determines the size of the first demand slice
        List<Double> keysFirstSlice;
        // keys that determine the size of other slices and to which book cover it is attributed
        List<Double> keysOtherSlices;

        // from double genome
        if (genome instanceof DoubleGenome) {
            DoubleGenome g = (DoubleGenome) genome;

            // extract gene values
            keysFirstSlice = g.getGenes(0, NB_BOOK_COVERS);
            keysOtherSlices = g.getGenes(NB_BOOK_COVERS, g.size());
        }

        // else from Integer genome
        else {
            IntGenome g = (IntGenome) genome;

            // extract gene values
            keysFirstSlice = g.getGenes(0, NB_BOOK_COVERS).stream()
                    .mapToDouble(Integer::doubleValue)
                    .boxed()
                    .collect(Collectors.toList());
            keysOtherSlices = g.getGenes(NB_BOOK_COVERS, g.size()).stream()
                    .mapToDouble(Integer::doubleValue)
                    .boxed()
                    .collect(Collectors.toList());
        }

        /*
        Construct list of keys that determine the size of demand slices for each book cover
         */

        // for each book cover, the keys that determine how to split its demand
        List<List<Double>> keysSplitSize = new ArrayList<>(NB_BOOK_COVERS);

        // add the first split to each book cover
        for (int bookCoverIndex = 0; bookCoverIndex < NB_BOOK_COVERS; bookCoverIndex++) {
            keysSplitSize.add(new ArrayList<>());
            keysSplitSize.get(bookCoverIndex).add(keysFirstSlice.get(bookCoverIndex));
        }

        // read all remaining genome keys to determine demand slices
        for (int cursor = 0; cursor < keysOtherSlices.size(); cursor += 2) {
            final double KEY_SLICE_SIZE = keysOtherSlices.get(cursor);
            final double KEY_BOOK_COVER = keysOtherSlices.get(cursor + 1);

            final int BOOK_COVER_INDEX = (int) Scale.scale(KEY_BOOK_COVER, 1.0d, NB_BOOK_COVERS);

            keysSplitSize.get(BOOK_COVER_INDEX).add(KEY_SLICE_SIZE);
        }

        /*
        Construct the list of nb of printings for each book cover-slot
         */

        // list that will contain the nb of printings for each book allocated to each slot
        List<BookCoverDemandPart> bookCoverDemandParts = new ArrayList<>(TOTAL_NB_SLOTS);

        // cut integer algorithm to split the demand of each book cover
        CutInteger demandSplitter = new CutInteger();

        // for each book cover, split its demand according to keys
        for (int bookCoverIndex = 0; bookCoverIndex < NB_BOOK_COVERS; bookCoverIndex++) {
            // get demand for this book cover
            final int DEMAND = bookCovers.get(bookCoverIndex).getDemand();
            // get keys that determine the size of each demand slice for this book cover
            final List<Double> keys = keysSplitSize.get(bookCoverIndex);

            // calculate each demand slice size based on the keys
            final List<Integer> nbPrintings = demandSplitter.cutCake(DEMAND, keys);

            // store the demand slices for this book cover
            BookCoverDemand bookCover = bookCovers.get(bookCoverIndex);
            bookCoverDemandParts.addAll(
                    nbPrintings.stream()
                            .map(printings -> new BookCoverDemandPart(bookCover, printings))
                            .collect(Collectors.toList())
            );
        }

        /*
        Sort demand slices by number of printings descending
         */

        // sort by number of printings so that we group slots with similar
        // required number of printings on the same offset plates
        Collections.sort(bookCoverDemandParts, Collections.reverseOrder());

        /*
        Create the CppSolution object to store this decoded solution
         */

        return new CppSolution(problem, bookCoverDemandParts);
    }
}
