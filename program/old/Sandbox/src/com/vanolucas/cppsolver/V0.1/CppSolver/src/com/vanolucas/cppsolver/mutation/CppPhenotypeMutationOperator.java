package com.vanolucas.cppsolver.mutation;

import com.vanolucas.cppsolver.solution.CppSolution;
import com.vanolucas.opti.mutation.CustomPhenotypeMutationOperator;
import com.vanolucas.opti.mutation.PhenotypeMutationOperator;
import com.vanolucas.opti.solution.Phenotype;

public abstract class CppPhenotypeMutationOperator extends CustomPhenotypeMutationOperator<CppSolution> {

    public abstract void mutateCppSolution(CppSolution cppSolution);
}
