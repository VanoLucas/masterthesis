package com.vanolucas.cppsolver.problem;

import com.vanolucas.cppsolver.solution.BookCoverDemand;
import com.vanolucas.cppsolver.solution.CppSolution;
import com.vanolucas.opti.problem.ConvenientProblem;
import com.vanolucas.opti.quality.DoubleCost;

import java.util.List;

public class CoverPrintingProblem extends ConvenientProblem<CppSolution, DoubleCost> implements ICoverPrintingProblem {

    /*
    Defaults
     */

    private static final int DEFAULT_NB_SLOTS_PER_OFFSET_PLATE = 4;
    private static final double DEFAULT_COST_OFFSET_PLATE = 0d;
    private static final double DEFAULT_COST_ONE_PRINTING = 1d;

    /*
    Attributes
     */

    private final List<BookCoverDemand> bookCoverDemands;
    private final int NB_OFFSET_PLATES;
    private final int NB_SLOTS_PER_OFFSET_PLATE;
    private final double COST_OFFSET_PLATE;
    private final double COST_ONE_PRINTING;

    /*
    Constructors
     */

    public CoverPrintingProblem(List<BookCoverDemand> bookCoverDemands, int nbOffsetPlates) {
        this(bookCoverDemands, nbOffsetPlates, DEFAULT_NB_SLOTS_PER_OFFSET_PLATE);
    }

    public CoverPrintingProblem(List<BookCoverDemand> bookCoverDemands, int nbOffsetPlates, int nbSlotsPerOffsetPlate) {
        this(bookCoverDemands, nbOffsetPlates, nbSlotsPerOffsetPlate,
                DEFAULT_COST_OFFSET_PLATE, DEFAULT_COST_ONE_PRINTING);
    }

    public CoverPrintingProblem(List<BookCoverDemand> bookCoverDemands, int nbOffsetPlates, int nbSlotsPerOffsetPlate,
                                double costOffsetPlate, double costOnePrinting) {
        this.bookCoverDemands = bookCoverDemands;
        this.NB_OFFSET_PLATES = nbOffsetPlates;
        this.NB_SLOTS_PER_OFFSET_PLATE = nbSlotsPerOffsetPlate;
        this.COST_OFFSET_PLATE = costOffsetPlate;
        this.COST_ONE_PRINTING = costOnePrinting;
    }

    /*
    Cover Printing Problem Accessors (ICoverPrintingProblem)
     */

    @Override
    public List<BookCoverDemand> getBookCoverDemands() {
        return bookCoverDemands;
    }

    @Override
    public int getNbBookCovers() {
        return bookCoverDemands.size();
    }

    @Override
    public int getNbOffsetPlates() {
        return NB_OFFSET_PLATES;
    }

    @Override
    public int getNbSlotsPerOffsetPlate() {
        return NB_SLOTS_PER_OFFSET_PLATE;
    }

    @Override
    public int getTotalNbSlots() {
        return NB_OFFSET_PLATES * NB_SLOTS_PER_OFFSET_PLATE;
    }

    @Override
    public int getTotalDemands() {
        return bookCoverDemands.stream()
                .mapToInt(BookCoverDemand::getDemand)
                .sum();
    }

    @Override
    public double getCostOffsetPlate() {
        return COST_OFFSET_PLATE;
    }

    @Override
    public double getCostOnePrinting() {
        return COST_ONE_PRINTING;
    }

    /*
    Optimization Algorithm Components
     */

    @Override
    public CppSolution defaultPhenotype() {
        return new CppSolution(this);
    }

    @Override
    public DoubleCost defaultQuality() {
        return new DoubleCost();
    }

    @Override
    public void randomize(CppSolution cppSolution) {
        cppSolution.randomize(bookCoverDemands, true);
    }

    @Override
    public DoubleCost evaluate(CppSolution cppSolution) {
        return new DoubleCost(cppSolution.getTotalCost());
    }
}
