package com.vanolucas.cppsolver.problem;

import com.vanolucas.cppsolver.genome.CppGenomeDecoderNS;
import com.vanolucas.cppsolver.genome.CppGenomeDecoderPS;
import com.vanolucas.cppsolver.genome.CppGenomeDecoderSSB;
import com.vanolucas.cppsolver.solution.BookCoverDemand;
import com.vanolucas.cppsolver.solution.CppSolution;
import com.vanolucas.opti.eval.EvaluationFunction;
import com.vanolucas.opti.genome.DoubleGenome;
import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.genome.IntGenome;
import com.vanolucas.opti.genome.decoder.GenomeDecoder;
import com.vanolucas.opti.problem.GeneticProblem;
import com.vanolucas.opti.quality.DoubleCost;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.randomizer.DoubleGenomeRandomizer;
import com.vanolucas.opti.randomizer.GenomeRandomizer;
import com.vanolucas.opti.randomizer.IntGenomeRandomizer;
import com.vanolucas.opti.solution.CloneablePhenotype;
import com.vanolucas.opti.solution.Phenotype;

import java.util.List;

public class GeneticCoverPrintingProblem extends GeneticProblem implements ICoverPrintingProblem {

    /*
    Defaults
     */

    private static final int DEFAULT_NB_SLOTS_PER_OFFSET_PLATE = 4;
    private static final double DEFAULT_COST_OFFSET_PLATE = 0d;
    private static final double DEFAULT_COST_ONE_PRINTING = 1d;

    /*
    Attributes
     */

    private final List<BookCoverDemand> bookCoverDemands;

    private final int NB_OFFSET_PLATES;
    private final int NB_SLOTS_PER_OFFSET_PLATE;

    private final double COST_OFFSET_PLATE;
    private final double COST_ONE_PRINTING;

    private final GenomeType genomeType;

    /*
    Enum
     */

    public enum GenomeType {
        N_S_INT, N_S_DOUBLE, S_SB, PS
    }

    /*
    Constructors
     */

    public GeneticCoverPrintingProblem(List<BookCoverDemand> bookCoverDemands, int nbOffsetPlates, GenomeType genomeType) {
        this(bookCoverDemands, nbOffsetPlates, genomeType, DEFAULT_NB_SLOTS_PER_OFFSET_PLATE);
    }

    public GeneticCoverPrintingProblem(List<BookCoverDemand> bookCoverDemands, int nbOffsetPlates, GenomeType genomeType, int nbSlotsPerOffsetPlate) {
        this(bookCoverDemands, nbOffsetPlates, genomeType, nbSlotsPerOffsetPlate,
                DEFAULT_COST_OFFSET_PLATE, DEFAULT_COST_ONE_PRINTING);
    }

    public GeneticCoverPrintingProblem(List<BookCoverDemand> bookCoverDemands, int nbOffsetPlates, GenomeType genomeType, int nbSlotsPerOffsetPlate,
                                       double costOffsetPlate, double costOnePrinting) {
        this.bookCoverDemands = bookCoverDemands;
        this.NB_OFFSET_PLATES = nbOffsetPlates;
        this.NB_SLOTS_PER_OFFSET_PLATE = nbSlotsPerOffsetPlate;
        this.genomeType = genomeType;
        this.COST_OFFSET_PLATE = costOffsetPlate;
        this.COST_ONE_PRINTING = costOnePrinting;
    }

    /*
    Cover Printing Problem Accessors (ICoverPrintingProblem)
     */

    @Override
    public List<BookCoverDemand> getBookCoverDemands() {
        return bookCoverDemands;
    }

    @Override
    public int getNbBookCovers() {
        return bookCoverDemands.size();
    }

    @Override
    public int getNbOffsetPlates() {
        return NB_OFFSET_PLATES;
    }

    @Override
    public int getNbSlotsPerOffsetPlate() {
        return NB_SLOTS_PER_OFFSET_PLATE;
    }

    @Override
    public int getTotalNbSlots() {
        return NB_OFFSET_PLATES * NB_SLOTS_PER_OFFSET_PLATE;
    }

    @Override
    public int getTotalDemands() {
        return bookCoverDemands.stream()
                .mapToInt(BookCoverDemand::getDemand)
                .sum();
    }

    @Override
    public double getCostOffsetPlate() {
        return COST_OFFSET_PLATE;
    }

    @Override
    public double getCostOnePrinting() {
        return COST_ONE_PRINTING;
    }

    /*
    Genetic Optimization Algorithm Components
     */

    @Override
    public Phenotype newPhenotype() {
        return new CloneablePhenotype(defaultPhenotype());
    }

    public CppSolution defaultPhenotype() {
        return new CppSolution(this);
    }

    @Override
    public Quality newQuality() {
        return new DoubleCost();
    }

    @Override
    public GenomeRandomizer newGenomeRandomizer() {
        switch (genomeType) {
            case N_S_DOUBLE:
            case S_SB:
            case PS:
                return new DoubleGenomeRandomizer();
            case N_S_INT:
                return new IntGenomeRandomizer();
            default:
                return null;
        }
    }

    @Override
    public EvaluationFunction newEvaluationFunction() {
        return new EvaluationFunction() {
            /**
             * Evaluation function of a Cover Printing Problem solution.
             * @param phenotype Phenotype to evaluate: it encapsulates the CPP solution.
             * @return The Quality (cost) of the evaluated CPP solution.
             */
            @Override
            public Quality evaluatePhenotype(Phenotype phenotype) {
                CppSolution cppSolution = (CppSolution) phenotype.get();
                return new DoubleCost(cppSolution.getTotalCost());
            }
        };
    }

    public int getGenomeSize() {
        switch (genomeType) {
            case N_S_DOUBLE:
            case N_S_INT:
                return getNbBookCovers() + getTotalNbSlots();
            case S_SB:
                return getNbBookCovers() + 2 * (getTotalNbSlots() - getNbBookCovers());
            case PS:
                return (getTotalNbSlots() - getNbBookCovers()) * 2;
            default:
                return 0;
        }
    }

    @Override
    public Genome newGenome() {
        switch (genomeType) {
            case N_S_DOUBLE:
            case S_SB:
            case PS:
                return new DoubleGenome(getGenomeSize());
            case N_S_INT:
                return new IntGenome(getGenomeSize());
            default:
                return null;
        }
    }

    @Override
    public GenomeDecoder newGenomeDecoder() {
        switch (genomeType) {
            case N_S_DOUBLE:
            case N_S_INT:
                return new CppGenomeDecoderNS(this);
            case S_SB:
                return new CppGenomeDecoderSSB(this);
            case PS:
                return new CppGenomeDecoderPS(this);
            default:
                return null;
        }
    }
}
