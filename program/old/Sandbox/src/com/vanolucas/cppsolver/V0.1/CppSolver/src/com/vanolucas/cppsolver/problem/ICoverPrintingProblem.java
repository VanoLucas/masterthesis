package com.vanolucas.cppsolver.problem;

import com.vanolucas.cppsolver.solution.BookCoverDemand;

import java.util.List;

public interface ICoverPrintingProblem {

    List<BookCoverDemand> getBookCoverDemands();

    int getNbBookCovers();
    int getNbOffsetPlates();
    int getNbSlotsPerOffsetPlate();

    double getCostOffsetPlate();
    double getCostOnePrinting();

    int getTotalNbSlots();
    int getTotalDemands();
}
