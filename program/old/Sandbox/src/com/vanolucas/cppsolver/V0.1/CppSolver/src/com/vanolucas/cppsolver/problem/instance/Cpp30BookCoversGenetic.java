package com.vanolucas.cppsolver.problem.instance;

import com.vanolucas.cppsolver.problem.GeneticCoverPrintingProblem;
import com.vanolucas.cppsolver.solution.BookCoverDemand;

import java.util.ArrayList;
import java.util.List;

public class Cpp30BookCoversGenetic extends GeneticCoverPrintingProblem {

    /*
    Offset plates params
     */

    private static final int NB_OFFSET_PLATES = 10;
    private static final int NB_SLOTS_PER_OFFSET_PLATE = 4;

    /*
    Costs params
     */

    private static final double COST_OFFSET_PLATE = 0d;
    private static final double COST_ONE_PRINTING = 1d;

    /*
    Book cover demands
     */

    private static final List<BookCoverDemand> BOOK_COVER_DEMANDS = new ArrayList<BookCoverDemand>() {{
        add(new BookCoverDemand("1", 30000));
        add(new BookCoverDemand("2", 28000));
        add(new BookCoverDemand("3", 27000));
        add(new BookCoverDemand("4", 26000));
        add(new BookCoverDemand("5", 26000));
        add(new BookCoverDemand("6", 23000));
        add(new BookCoverDemand("7", 22000));
        add(new BookCoverDemand("8", 22000));
        add(new BookCoverDemand("9", 20000));
        add(new BookCoverDemand("10", 20000));
        add(new BookCoverDemand("11", 19000));
        add(new BookCoverDemand("12", 18000));
        add(new BookCoverDemand("13", 17000));
        add(new BookCoverDemand("14", 16000));
        add(new BookCoverDemand("15", 15000));
        add(new BookCoverDemand("16", 15000));
        add(new BookCoverDemand("17", 14000));
        add(new BookCoverDemand("18", 13500));
        add(new BookCoverDemand("19", 13000));
        add(new BookCoverDemand("20", 11000));
        add(new BookCoverDemand("21", 10500));
        add(new BookCoverDemand("22", 10000));
        add(new BookCoverDemand("23", 9000));
        add(new BookCoverDemand("24", 9000));
        add(new BookCoverDemand("25", 7500));
        add(new BookCoverDemand("26", 6000));
        add(new BookCoverDemand("27", 5000));
        add(new BookCoverDemand("28", 2500));
        add(new BookCoverDemand("29", 1500));
        add(new BookCoverDemand("30", 1000));
    }};

    /*
    Constructor
     */

    public Cpp30BookCoversGenetic(GenomeType genomeType) {
        super(BOOK_COVER_DEMANDS, NB_OFFSET_PLATES, genomeType, NB_SLOTS_PER_OFFSET_PLATE, COST_OFFSET_PLATE, COST_ONE_PRINTING);
    }

}
