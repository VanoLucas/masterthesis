package com.vanolucas.cppsolver.problem.instance;

import com.vanolucas.cppsolver.problem.GeneticCoverPrintingProblem;
import com.vanolucas.cppsolver.solution.BookCoverDemand;

import java.util.ArrayList;
import java.util.List;

public class Cpp50BookCoversGenetic extends GeneticCoverPrintingProblem {

    /*
    Offset plates params
     */

    private static final int NB_OFFSET_PLATES = 21;
    private static final int NB_SLOTS_PER_OFFSET_PLATE = 4;

    /*
    Costs params
     */

    private static final double COST_OFFSET_PLATE = 0d;
    private static final double COST_ONE_PRINTING = 1d;

    /*
    Book cover demands
     */

    private static final List<BookCoverDemand> BOOK_COVER_DEMANDS = new ArrayList<BookCoverDemand>() {{
        add(new BookCoverDemand("1", 95000));
        add(new BookCoverDemand("2", 90000));
        add(new BookCoverDemand("3", 85500));
        add(new BookCoverDemand("4", 80000));
        add(new BookCoverDemand("5", 77000));
        add(new BookCoverDemand("6", 72300));
        add(new BookCoverDemand("7", 70500));
        add(new BookCoverDemand("8", 69000));
        add(new BookCoverDemand("9", 67000));
        add(new BookCoverDemand("10", 67000));
        add(new BookCoverDemand("11", 61700));
        add(new BookCoverDemand("12", 60000));
        add(new BookCoverDemand("13", 57650));
        add(new BookCoverDemand("14", 55500));
        add(new BookCoverDemand("15", 52100));
        add(new BookCoverDemand("16", 51000));
        add(new BookCoverDemand("17", 50000));
        add(new BookCoverDemand("18", 43500));
        add(new BookCoverDemand("19", 43000));
        add(new BookCoverDemand("20", 39000));
        add(new BookCoverDemand("21", 38900));
        add(new BookCoverDemand("22", 37000));
        add(new BookCoverDemand("23", 36000));
        add(new BookCoverDemand("24", 34300));
        add(new BookCoverDemand("25", 32700));
        add(new BookCoverDemand("26", 30000));
        add(new BookCoverDemand("27", 30000));
        add(new BookCoverDemand("28", 28300));
        add(new BookCoverDemand("29", 28000));
        add(new BookCoverDemand("30", 26350));
        add(new BookCoverDemand("31", 25500));
        add(new BookCoverDemand("32", 22400));
        add(new BookCoverDemand("33", 21000));
        add(new BookCoverDemand("34", 21000));
        add(new BookCoverDemand("35", 19000));
        add(new BookCoverDemand("36", 16050));
        add(new BookCoverDemand("37", 14000));
        add(new BookCoverDemand("38", 11900));
        add(new BookCoverDemand("39", 11000));
        add(new BookCoverDemand("40", 10000));
        add(new BookCoverDemand("41", 10000));
        add(new BookCoverDemand("42", 7800));
        add(new BookCoverDemand("43", 6000));
        add(new BookCoverDemand("44", 4500));
        add(new BookCoverDemand("45", 4000));
        add(new BookCoverDemand("46", 3000));
        add(new BookCoverDemand("47", 2900));
        add(new BookCoverDemand("48", 1450));
        add(new BookCoverDemand("49", 1000));
        add(new BookCoverDemand("50", 750));
    }};

    /*
    Constructor
     */

    public Cpp50BookCoversGenetic(GenomeType genomeType) {
        super(BOOK_COVER_DEMANDS, NB_OFFSET_PLATES, genomeType, NB_SLOTS_PER_OFFSET_PLATE, COST_OFFSET_PLATE, COST_ONE_PRINTING);
    }
}
