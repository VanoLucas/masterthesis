package com.vanolucas.cppsolver.solution;

public class BookCover {

    private String name;

    /*
    Constructors
     */

    public BookCover(String name) {
        setName(name);
    }

    /*
    Accessors
     */

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
