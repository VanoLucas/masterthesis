package com.vanolucas.cppsolver.solution;

public class BookCoverActualPrintings {

    private BookCoverDemand bookCoverDemand;
    private int actualPrintings;

    /*
    Constructors
     */

    public BookCoverActualPrintings(BookCoverDemand bookCoverDemand, int actualPrintings) {
        this.bookCoverDemand = bookCoverDemand;
        this.actualPrintings = actualPrintings;
    }

    /*
    Commands
     */

    public boolean isDemandFulfilled() {
        return actualPrintings >= bookCoverDemand.getDemand();
    }

    public int getWaste() {
        return actualPrintings - bookCoverDemand.getDemand();
    }

    /*
    toString
     */

    @Override
    public String toString() {
        String str = bookCoverDemand.getName() + ":" + actualPrintings + "/" + bookCoverDemand.getDemand();

        final int waste = getWaste();
        if (waste >= 0) {
            str += "(waste:" + waste + ")";
        } else {
            str += "(missing:" + Math.abs(waste) + ")";
        }

        str += isDemandFulfilled() ? "[ok]" : "[  ]";

        return str;
    }
}
