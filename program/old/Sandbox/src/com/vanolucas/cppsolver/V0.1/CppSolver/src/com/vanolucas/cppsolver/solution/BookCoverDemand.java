package com.vanolucas.cppsolver.solution;

public class BookCoverDemand extends BookCover implements Comparable<BookCoverDemand> {

    private int demand;

    /*
    Constructors
     */

    public BookCoverDemand(String name) {
        this(name, 0);
    }
    public BookCoverDemand(String name, int demand) {
        super(name);
        setDemand(demand);
    }

    /*
    Accessors
     */

    public int getDemand() {
        return demand;
    }

    public void setDemand(int demand) {
        this.demand = demand;
    }

    /*
    Comparable
     */

    @Override
    public int compareTo(BookCoverDemand o) {
        return Integer.valueOf(demand).compareTo(o.demand);
    }
}
