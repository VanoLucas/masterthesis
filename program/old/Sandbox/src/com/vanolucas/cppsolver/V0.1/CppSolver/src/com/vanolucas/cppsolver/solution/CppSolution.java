package com.vanolucas.cppsolver.solution;

import com.vanolucas.cppsolver.problem.ICoverPrintingProblem;
import com.vanolucas.opti.util.Cloneable;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * This stores a solution of the Cover Printing Problem.
 */
public class CppSolution implements Cloneable {

    private List<OffsetPlate> offsetPlates;
    private final int NB_OFFSET_PLATES;
    private final int NB_SLOTS_PER_OFFSET_PLATE;
    private final int TOTAL_NB_SLOTS;
    private final int TOTAL_DEMANDS;
    private final ICoverPrintingProblem problem;

    /*
    Constructors
     */

    public CppSolution(ICoverPrintingProblem problem) {
        this(problem, null);
    }
    public CppSolution(ICoverPrintingProblem problem, List<BookCoverDemandPart> bookCoverDemandParts) {
        this.NB_OFFSET_PLATES = problem.getNbOffsetPlates();
        this.NB_SLOTS_PER_OFFSET_PLATE = problem.getNbSlotsPerOffsetPlate();
        this.TOTAL_NB_SLOTS = this.NB_OFFSET_PLATES * this.NB_SLOTS_PER_OFFSET_PLATE;
        this.TOTAL_DEMANDS = problem.getTotalDemands();
        this.offsetPlates = new ArrayList<>(NB_OFFSET_PLATES);
        this.problem = problem;

        if (bookCoverDemandParts != null) {
            assignBookCovers(bookCoverDemandParts);
        }
    }

    /*
    Accessors
     */

    /**
     * Get the total number of printings required to realize this solution.
     * It is the sum of the number of printings required for each offset plate.
     *
     * @return Total number of printings required to realize this solution.
     */
    public int getTotalNbPrintings() {
        return offsetPlates.stream()
                .mapToInt(OffsetPlate::getNbPrintings)
                .sum();
    }

    /*
    Commands
     */

    /**
     * Assign book covers and their number of printings to slots of the offset plates in the order we received them.
     *
     * @param bookCoverDemandParts Split book cover demands with the number of printings required for each slot.
     */
    public void assignBookCovers(List<BookCoverDemandPart> bookCoverDemandParts) {
        // erase any old offset plate
        offsetPlates.clear();

        // for each offset plate to fill
        IntStream.range(0, NB_OFFSET_PLATES)
                // split the list of book cover assignments for each offset plate (each list has n items for n slots of the offset plate)
                .mapToObj(offsetPlate ->
                        bookCoverDemandParts.subList(offsetPlate * NB_SLOTS_PER_OFFSET_PLATE,
                                offsetPlate * NB_SLOTS_PER_OFFSET_PLATE + NB_SLOTS_PER_OFFSET_PLATE)
                )
                // put the book cover assignments in the offset plates
                .forEach(bookCoversForThisOffsetPlate ->
                        offsetPlates.add(new OffsetPlate(bookCoversForThisOffsetPlate))
                );

        // sort offset plates by number of printings (descending)
        Collections.sort(offsetPlates, Collections.reverseOrder());
    }

    /**
     * Evaluation function to calculate this solution's total cost.
     *
     * @return Total cost of the solution based on the params of the Cover Printing Problem instance.
     */
    public double getTotalCost() {
        double totalCost = problem.getCostOffsetPlate() * NB_OFFSET_PLATES;
        totalCost += problem.getCostOnePrinting() * getTotalNbPrintings();
        return totalCost;
    }

    /**
     * For each book cover, get how many times it gets printed when realizing this solution.
     * @return The actual number of printings for each book cover when realizing this solution.
     */
    public List<BookCoverActualPrintings> getActualPrintings() {
        // for each book cover, calculate the actual number of printings produced
        return problem.getBookCoverDemands().stream()
                .map(bookCover ->
                        new BookCoverActualPrintings(bookCover,
                                // for each offset plate, get the number of printings of this book cover it produces
                                offsetPlates.stream()
                                        // get the actual nb of printings of this book cover from this offset plate
                                        .mapToInt(offsetPlate -> offsetPlate.getNbPrintingsOfBookCover(bookCover))
                                        // sum for all offset plates that print this book cover
                                        .sum()
                        )
                )
                .collect(Collectors.toList());
    }

    /**
     * Randomize this solution.
     *
     * @param bookCoverDemands               The demand for each book cover of the CPP instance we are solving.
     * @param sortSlotsByAssignedNbPrintings When true, we first sort the slot assignments by nb of printings
     *                                       before assigning them to the offset plates.
     */
    public void randomize(final List<BookCoverDemand> bookCoverDemands, final boolean sortSlotsByAssignedNbPrintings) {
        // list that will contain the book cover assignments
        List<BookCoverDemandPart> bookCoverDemandParts = bookCoverDemands.stream()
                .map(b -> new BookCoverDemandPart(b, b.getDemand()))
                .collect(Collectors.toList());

        // random generator
        Random rand = new Random();

        // spread the demand over all available slots
        while (bookCoverDemandParts.size() < TOTAL_NB_SLOTS) {
            // pick a random book cover demand assignment
            int indexToSplit = rand.nextInt(bookCoverDemandParts.size());

            // split it over two slots
            bookCoverDemandParts.add(
                    bookCoverDemandParts.get(indexToSplit)
                            .splitInTwoOfRandomSize()
            );
        }

        // sort by number of printings assigned to each slot (descending)
        if (sortSlotsByAssignedNbPrintings) {
            Collections.sort(bookCoverDemandParts, Collections.reverseOrder());
        }

        // fill this solution's offset plates with proposed assignments
        assignBookCovers(bookCoverDemandParts);
    }

    /*
    Cloneable
     */

    @Override
    public CppSolution clone() {
        CppSolution copy = new CppSolution(this.problem);

        copy.offsetPlates = this.offsetPlates.stream()
                .map(offsetPlate -> offsetPlate.clone())
                .collect(Collectors.toList());

        return copy;
    }

    /*
    toString
     */

    @Override
    public String toString() {
        String str = "";

        /*
        Offset plates
         */

        str += NB_OFFSET_PLATES + " offset plates:";
        str += offsetPlates.stream()
                .map(offsetPlate -> offsetPlate.toString())
                .collect(Collectors.joining("\n\t", "\n\t", ""));
        str += "\n\tTotal printings: " + getTotalNbPrintings();

        /*
        Actual printings
         */

        str += "\nActually printed:";
        str += getActualPrintings().stream()
                .map(BookCoverActualPrintings::toString)
                .collect(Collectors.joining("\n\t", "\n\t", ""));

        /*
        Total printings
         */

        str += "\nBook covers printed: " + offsetPlates.stream()
                .mapToInt(o -> o.getNbPrintings() * NB_SLOTS_PER_OFFSET_PLATE)
                .sum();
        str += " (demand was " + TOTAL_DEMANDS + ")";

        /*
        Waste
         */

        str += "\nTotal waste: ";

        final double totalWaste = getActualPrintings().stream()
                .mapToInt(BookCoverActualPrintings::getWaste)
                .sum();
        final double totalDemands = TOTAL_DEMANDS;

        str += (int)totalWaste + "/" + TOTAL_DEMANDS;
        str += " (" + totalWaste/totalDemands*100d + "%)";

        /*
        (Non) realizable solution
         */

        if (getActualPrintings().stream()
                .map(BookCoverActualPrintings::isDemandFulfilled)
                .anyMatch(p -> p == false)
                ) {
            str += "\n/!\\ Non realizable solution.";
        }

        /*
        Total cost
         */

        str += "\nTotal cost: " + getTotalCost();

        return str;
    }
}
