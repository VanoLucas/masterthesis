package com.vanolucas.cppsolver.test;

import com.vanolucas.cppsolver.problem.instance.Cpp30BookCovers;
import com.vanolucas.opti.algo.OptiAlgo;
import com.vanolucas.opti.algo.random.RandomSearch;
import com.vanolucas.opti.math.ParetoFront;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.solution.Solution;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CoverPrintingProblemTest {

    @Test
    void randomSearch() {

        final double TARGET_COST = 121000d;

        Cpp30BookCovers cpp = new Cpp30BookCovers();

        RandomSearch randomSearch = cpp.newRandomSearch();

        randomSearch.addListener(new OptiAlgo.Listener() {
            @Override
            public void onIterationStart(long iteration, ParetoFront<Quality> bestQualities) {

            }

            @Override
            public void onIterationEnd(long iteration, ParetoFront<Quality> bestQualities) {

            }

            @Override
            public void onNewBetterSolutionFound(Solution newBetterSolution, long iteration) {
                if (newBetterSolution.isEquivalentOrBetterThan(cpp.defaultQuality().set(TARGET_COST))) {
//                    System.out.println("Iteration: " + iteration);
//                    System.out.println(newBetterSolution);
//                    System.out.println();
                }
            }

            @Override
            public void onNewEquivalentSolutionFound(Solution equivalentSolution, long iteration) {

            }

            @Override
            public void onPause(long iteration, ParetoFront<Quality> bestQualities) {

            }
        });

        randomSearch.runUntilQuality(cpp.defaultQuality().set(TARGET_COST));
//        randomSearch.runUntilIteration(900000l);

        assertTrue(randomSearch.getLastBetterSolutionFoundQuality()
                .isEquivalentOrBetterThan(cpp.defaultQuality().set(TARGET_COST))
        );
    }

}