package com.vanolucas.cppsolver.test;

import com.vanolucas.cppsolver.problem.GeneticCoverPrintingProblem;
import com.vanolucas.cppsolver.problem.instance.Cpp30BookCoversGenetic;
import com.vanolucas.cppsolver.problem.instance.Cpp50BookCoversGenetic;
import com.vanolucas.opti.algo.local.GeneticLocalSearch;
import com.vanolucas.opti.algo.local.LocalSearch;
import com.vanolucas.opti.math.ParetoFront;
import com.vanolucas.opti.mutation.MutationOperator;
import com.vanolucas.opti.mutation.Random01DoubleGenesMutationOperator;
import com.vanolucas.opti.mutation.RandomPositiveIntGenesMutationOperator;
import com.vanolucas.opti.quality.DoubleCost;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.solution.Solution;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class GeneticCoverPrintingProblemTest {

    private final double TARGET_30_BOOK_COVERS_COST = 118400d;

    @Test
    void cppGeneticLocalSearchNSDouble() {

        final boolean ENABLE_PRINT = true;
        final double TARGET_COST = TARGET_30_BOOK_COVERS_COST;

        List<Integer> qualityHistory = new ArrayList<>();

        Cpp30BookCoversGenetic problem = new Cpp30BookCoversGenetic(GeneticCoverPrintingProblem.GenomeType.N_S_DOUBLE);

        GeneticLocalSearch localSearch = problem.newGeneticLocalSearch();

        List<MutationOperator> mutationOperators = Stream.of(
                0.003d,
                0.005d,
                0.007d,
                0.010d,
                0.020d,
                0.030d,
                0.050d,
                0.070d,
                0.100d,
                0.200d,
                0.300d,
                0.500d,
                0.700d,
                0.800d
        )
                .map(mutationProba -> new Random01DoubleGenesMutationOperator(mutationProba))
                .collect(Collectors.toList());

        localSearch.setMutationOperators(mutationOperators);

        localSearch.addListener(new LocalSearch.Listener() {
            @Override
            public void onLocalOptimumReached(long iteration, Solution localOptimum) {
//                System.out.println(localOptimum.getQuality());
            }

            @Override
            public void onIterationStart(long iteration, ParetoFront<Quality> bestQualities) {
//                System.out.println(iteration);
            }

            @Override
            public void onIterationEnd(long iteration, ParetoFront<Quality> bestQualities) {

            }

            @Override
            public void onNewBetterSolutionFound(Solution newBetterSolution, long iteration) {
                if (!ENABLE_PRINT) {
                    return;
                }

                System.out.println("Iteration: " + iteration);
                System.out.println(newBetterSolution);

                qualityHistory.add(((int) ((DoubleCost) newBetterSolution.getQuality()).doubleValue()));
                Collections.sort(qualityHistory);
                System.out.println("Quality history: " + qualityHistory);

                System.out.println();
            }

            @Override
            public void onNewEquivalentSolutionFound(Solution equivalentSolution, long iteration) {

            }

            @Override
            public void onPause(long iteration, ParetoFront<Quality> bestQualities) {

            }
        });

        localSearch.runUntilQuality(((DoubleCost) problem.newQuality()).set(TARGET_COST));
//        localSearch.runUntilIteration(100000l);

        assertTrue(localSearch.getLastBetterSolutionFoundQuality()
                .isEquivalentOrBetterThan(((DoubleCost) problem.newQuality()).set(TARGET_COST))
        );
    }

    @Test
    void cppGeneticLocalSearchNSInt() {

        final boolean ENABLE_PRINT = true;
        final double TARGET_COST = TARGET_30_BOOK_COVERS_COST;

        List<Integer> qualityHistory = new ArrayList<>();

        Cpp30BookCoversGenetic problem = new Cpp30BookCoversGenetic(GeneticCoverPrintingProblem.GenomeType.N_S_INT);

        GeneticLocalSearch localSearch = problem.newGeneticLocalSearch();

        List<MutationOperator> mutationOperators = Stream.of(
                0.003d,
                0.005d,
                0.007d,
                0.010d,
                0.020d,
                0.030d,
                0.050d,
                0.070d,
                0.100d,
                0.200d,
                0.300d,
                0.500d,
                0.700d,
                0.800d
        )
                .map(mutationProba -> new RandomPositiveIntGenesMutationOperator(mutationProba))
                .collect(Collectors.toList());

        localSearch.setMutationOperators(mutationOperators);

        localSearch.addListener(new LocalSearch.Listener() {
            @Override
            public void onLocalOptimumReached(long iteration, Solution localOptimum) {
//                System.out.println(localOptimum.getQuality());
            }

            @Override
            public void onIterationStart(long iteration, ParetoFront<Quality> bestQualities) {
//                System.out.println(iteration);
            }

            @Override
            public void onIterationEnd(long iteration, ParetoFront<Quality> bestQualities) {

            }

            @Override
            public void onNewBetterSolutionFound(Solution newBetterSolution, long iteration) {
                if (!ENABLE_PRINT) {
                    return;
                }

                System.out.println("Iteration: " + iteration);
                System.out.println(newBetterSolution);

                qualityHistory.add(((int) ((DoubleCost) newBetterSolution.getQuality()).doubleValue()));
                Collections.sort(qualityHistory);
                System.out.println("Quality history: " + qualityHistory);

                System.out.println();
            }

            @Override
            public void onNewEquivalentSolutionFound(Solution equivalentSolution, long iteration) {

            }

            @Override
            public void onPause(long iteration, ParetoFront<Quality> bestQualities) {

            }
        });

        localSearch.runUntilQuality(((DoubleCost) problem.newQuality()).set(TARGET_COST));
//        localSearch.runUntilIteration(100000l);

        assertTrue(localSearch.getLastBetterSolutionFoundQuality()
                .isEquivalentOrBetterThan(((DoubleCost) problem.newQuality()).set(TARGET_COST))
        );
    }

    @Test
    void cppGeneticLocalSearchSSBDouble() {

        final boolean ENABLE_PRINT = true;
        final double TARGET_COST = TARGET_30_BOOK_COVERS_COST;

        List<Integer> qualityHistory = new ArrayList<>();

        Cpp30BookCoversGenetic problem = new Cpp30BookCoversGenetic(GeneticCoverPrintingProblem.GenomeType.S_SB);

        GeneticLocalSearch localSearch = problem.newGeneticLocalSearch();

        List<MutationOperator> mutationOperators = Stream.of(
                0.003d,
                0.005d,
                0.007d,
                0.010d,
                0.020d,
                0.030d,
                0.050d,
                0.070d,
                0.100d,
                0.200d,
                0.300d,
                0.500d,
                0.700d,
                0.800d
        )
                .map(mutationProba -> new Random01DoubleGenesMutationOperator(mutationProba))
                .collect(Collectors.toList());

        localSearch.setMutationOperators(mutationOperators);

        localSearch.addListener(new LocalSearch.Listener() {
            @Override
            public void onLocalOptimumReached(long iteration, Solution localOptimum) {
//                System.out.println(localOptimum.getQuality());
            }

            @Override
            public void onIterationStart(long iteration, ParetoFront<Quality> bestQualities) {
//                System.out.println(iteration);
            }

            @Override
            public void onIterationEnd(long iteration, ParetoFront<Quality> bestQualities) {

            }

            @Override
            public void onNewBetterSolutionFound(Solution newBetterSolution, long iteration) {
                if (!ENABLE_PRINT) {
                    return;
                }

                System.out.println("Iteration: " + iteration);
                System.out.println(newBetterSolution);

                qualityHistory.add(((int) ((DoubleCost) newBetterSolution.getQuality()).doubleValue()));
                Collections.sort(qualityHistory);
                System.out.println("Quality history: " + qualityHistory);

                System.out.println();
            }

            @Override
            public void onNewEquivalentSolutionFound(Solution equivalentSolution, long iteration) {

            }

            @Override
            public void onPause(long iteration, ParetoFront<Quality> bestQualities) {

            }
        });

        localSearch.runUntilQuality(((DoubleCost) problem.newQuality()).set(TARGET_COST));
//        localSearch.runUntilIteration(100000l);

        assertTrue(localSearch.getLastBetterSolutionFoundQuality()
                .isEquivalentOrBetterThan(((DoubleCost) problem.newQuality()).set(TARGET_COST))
        );
    }

    @Test
    void cppGeneticLocalSearchPSDouble() {

        final boolean ENABLE_PRINT = true;
        final double TARGET_COST = TARGET_30_BOOK_COVERS_COST;

        List<Integer> qualityHistory = new ArrayList<>();

        Cpp30BookCoversGenetic problem = new Cpp30BookCoversGenetic(GeneticCoverPrintingProblem.GenomeType.PS);

        GeneticLocalSearch localSearch = problem.newGeneticLocalSearch();

        List<MutationOperator> mutationOperators = Stream.of(
                0.003d,
                0.005d,
                0.007d,
                0.010d,
                0.020d,
                0.030d,
                0.050d,
                0.070d,
                0.100d,
                0.200d,
                0.300d,
                0.500d,
                0.700d,
                0.800d
        )
                .map(mutationProba -> new Random01DoubleGenesMutationOperator(mutationProba))
                .collect(Collectors.toList());

        localSearch.setMutationOperators(mutationOperators);

        localSearch.addListener(new LocalSearch.Listener() {
            @Override
            public void onLocalOptimumReached(long iteration, Solution localOptimum) {
//                System.out.println(localOptimum.getQuality());
            }

            @Override
            public void onIterationStart(long iteration, ParetoFront<Quality> bestQualities) {
//                System.out.println(iteration);
            }

            @Override
            public void onIterationEnd(long iteration, ParetoFront<Quality> bestQualities) {

            }

            @Override
            public void onNewBetterSolutionFound(Solution newBetterSolution, long iteration) {
                if (!ENABLE_PRINT) {
                    return;
                }

                System.out.println("Iteration: " + iteration);
                System.out.println(newBetterSolution);

                qualityHistory.add(((int) ((DoubleCost) newBetterSolution.getQuality()).doubleValue()));
                Collections.sort(qualityHistory);
                System.out.println("Quality history: " + qualityHistory);

                System.out.println();
            }

            @Override
            public void onNewEquivalentSolutionFound(Solution equivalentSolution, long iteration) {

            }

            @Override
            public void onPause(long iteration, ParetoFront<Quality> bestQualities) {

            }
        });

        localSearch.runUntilQuality(((DoubleCost) problem.newQuality()).set(TARGET_COST));
//        localSearch.runUntilIteration(100000l);

        assertTrue(localSearch.getLastBetterSolutionFoundQuality()
                .isEquivalentOrBetterThan(((DoubleCost) problem.newQuality()).set(TARGET_COST))
        );
    }

    @Test
    void cppParallelGeneticLocalSearchPSDouble() {

        final boolean ENABLE_PRINT = false;
        final double TARGET_COST = 468000d;

        List<Integer> qualityHistory = new ArrayList<>();

        GeneticCoverPrintingProblem problem = new Cpp50BookCoversGenetic(GeneticCoverPrintingProblem.GenomeType.PS);

        GeneticLocalSearch localSearch = problem.newParallelGeneticLocalSearch();

        List<MutationOperator> mutationOperators = Stream.of(
                0.003d,
                0.005d,
                0.007d,
                0.010d,
                0.020d,
                0.030d,
                0.050d,
                0.070d,
                0.100d,
                0.200d,
                0.300d,
                0.500d,
                0.700d,
                0.800d
        )
                .map(mutationProba -> new Random01DoubleGenesMutationOperator(mutationProba))
                .collect(Collectors.toList());

        localSearch.setMutationOperators(mutationOperators);

        localSearch.addListener(new LocalSearch.Listener() {
            @Override
            public void onLocalOptimumReached(long iteration, Solution localOptimum) {
//                System.out.println(localOptimum.getQuality());
            }

            @Override
            public void onIterationStart(long iteration, ParetoFront<Quality> bestQualities) {
//                System.out.println(iteration);
            }

            @Override
            public void onIterationEnd(long iteration, ParetoFront<Quality> bestQualities) {

            }

            @Override
            public void onNewBetterSolutionFound(Solution newBetterSolution, long iteration) {
                if (!ENABLE_PRINT) {
                    return;
                }

                System.out.println("Iteration: " + iteration);
                System.out.println(newBetterSolution);

                qualityHistory.add(((int) ((DoubleCost) newBetterSolution.getQuality()).doubleValue()));
                Collections.sort(qualityHistory);
                System.out.println("Quality history: " + qualityHistory);

                System.out.println();
            }

            @Override
            public void onNewEquivalentSolutionFound(Solution equivalentSolution, long iteration) {

            }

            @Override
            public void onPause(long iteration, ParetoFront<Quality> bestQualities) {

            }
        });

        localSearch.runUntilQuality(((DoubleCost) problem.newQuality()).set(TARGET_COST));
//        localSearch.runUntilIteration(100000l);

        assertTrue(localSearch.getLastBetterSolutionFoundQuality()
                .isEquivalentOrBetterThan(((DoubleCost) problem.newQuality()).set(TARGET_COST))
        );
    }
}