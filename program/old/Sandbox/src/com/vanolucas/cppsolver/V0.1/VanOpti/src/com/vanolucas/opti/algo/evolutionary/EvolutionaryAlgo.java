package com.vanolucas.opti.algo.evolutionary;

import com.vanolucas.opti.algo.OptiAlgo;
import com.vanolucas.opti.math.ParetoFront;
import com.vanolucas.opti.quality.Quality;

/**
 * Evolutionary Optimization Algorithm.
 */
public class EvolutionaryAlgo extends OptiAlgo {

    /*
    Listener
     */

    public interface Listener extends OptiAlgo.Listener {

        void onNewGeneration(long generation, ParetoFront<Quality> bestQualities);
    }

    /*
    Attributes
     */

    /*
    Constructors
     */

    /*
    Helpers
     */

    /**
     * Execute an iteration step (= generation) of the Evolutionary Algorithm
     */
    @Override
    protected void runIteration() {
        initIfNeeded();
        // todo implement
    }

    protected void initIfNeeded() {

    }
}
