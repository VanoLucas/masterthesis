package com.vanolucas.opti.algo.local;

import com.vanolucas.opti.genome.decoder.GenomeDecoder;
import com.vanolucas.opti.genome.decoder.GenomeDecoderFactory;
import com.vanolucas.opti.mutation.MutationOperator;
import com.vanolucas.opti.solution.Individual;
import com.vanolucas.opti.solution.Solution;

import java.util.List;

public class GeneticLocalSearch extends LocalSearch {

    /*
    Attributes
     */

    private GenomeDecoder genomeDecoder;
    protected GenomeDecoderFactory genomeDecoderFactory;

    /*
    Constructors
     */

    public GeneticLocalSearch(GeneticLocalSearchFactory factory) {
        this(factory, null);
    }

    public GeneticLocalSearch(GeneticLocalSearchFactory factory, List<MutationOperator> mutationOperators) {
        this(factory, mutationOperators, null);
    }

    public GeneticLocalSearch(GeneticLocalSearchFactory factory, List<MutationOperator> mutationOperators, Solution startSolution) {
        super(factory, mutationOperators, startSolution);

        this.genomeDecoderFactory = factory;
    }

    /*
    Helpers
     */

    /**
     * Init all necessary objects before running.
     */
    @Override
    protected void initIfNeeded() {
        super.initIfNeeded();

        if (genomeDecoder == null) {
            genomeDecoder = genomeDecoderFactory.newGenomeDecoder();
        }
    }

    @Override
    protected void evaluateNeighbors() {
        // decode each neighbor's genome then evaluate it
        neighbors.forEach(neighbor -> {
            // decode this neighbor's genome
            ((Individual) neighbor).decodeGenomeUsing(genomeDecoder);
            // we can then evaluate this neighbor
            neighbor.evaluateUsing(evaluationFunction);
        });
    }

}
