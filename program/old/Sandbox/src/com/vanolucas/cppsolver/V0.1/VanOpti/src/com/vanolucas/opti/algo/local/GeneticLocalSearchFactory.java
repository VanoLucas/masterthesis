package com.vanolucas.opti.algo.local;

import com.vanolucas.opti.algo.random.GeneticRandomSearchFactory;

public interface GeneticLocalSearchFactory extends LocalSearchFactory, GeneticRandomSearchFactory {

    GeneticLocalSearch newGeneticLocalSearch();
    ParallelGeneticLocalSeach newParallelGeneticLocalSearch();
}
