package com.vanolucas.opti.algo.local;

import com.vanolucas.opti.algo.OptiAlgo;
import com.vanolucas.opti.eval.EvaluationFunction;
import com.vanolucas.opti.eval.EvaluationFunctionFactory;
import com.vanolucas.opti.mutation.MutationOperator;
import com.vanolucas.opti.randomizer.SolutionRandomizer;
import com.vanolucas.opti.randomizer.SolutionRandomizerFactory;
import com.vanolucas.opti.solution.Solution;
import com.vanolucas.opti.solution.SolutionFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Local Search Algorithm.
 */
public class LocalSearch extends OptiAlgo {

    /*
    Listener
     */

    public interface Listener extends OptiAlgo.Listener {

        void onLocalOptimumReached(long iteration, Solution localOptimum);
    }

    /*
    Attributes
     */

    protected Solution currentSolution;
    protected List<Solution> neighbors;
    protected SolutionRandomizer randomizer;
    protected List<MutationOperator> mutationOperators;
    protected EvaluationFunction evaluationFunction;
    protected boolean noMoreGoodNeighbor = false;

    protected SolutionFactory solutionFactory;
    protected SolutionRandomizerFactory randomizerFactory;
    protected EvaluationFunctionFactory evaluationFunctionFactory;

    /*
    Constructors
     */

    public LocalSearch(LocalSearchFactory factory) {
        this(factory, null);
    }

    public LocalSearch(LocalSearchFactory factory, List<MutationOperator> mutationOperators) {
        this(factory, mutationOperators, null);
    }

    public LocalSearch(LocalSearchFactory factory, List<MutationOperator> mutationOperators, Solution startSolution) {
        this.currentSolution = startSolution;

        this.solutionFactory = factory;
        this.randomizerFactory = factory;
        this.evaluationFunctionFactory = factory;

        setMutationOperators(mutationOperators);
    }

    /*
    Accessors
     */

    public void setMutationOperators(List<MutationOperator> mutationOperators) {
        this.mutationOperators = mutationOperators;
    }

    /*
    Commands
     */

    public void addMutationOperator(MutationOperator mutationOperator) {
        if (mutationOperators == null) {
            mutationOperators = new ArrayList<>(2);
        }

        mutationOperators.add(mutationOperator);
    }

    /*
    Helpers
     */

    /**
     * Execute an iteration step of the Local Search Algorithm
     */
    @Override
    protected void runIteration() {
        initIfNeeded();

        if (noMoreGoodNeighbor) {
            randomizeSolution();
            noMoreGoodNeighbor = false;
        }

        copyCurrentSolutionToNeighbors();
        mutateNeighbors();
        evaluateNeighbors();

        noMoreGoodNeighbor = !moveToBestNeighbor();

        if (!noMoreGoodNeighbor) {
            proposeNewSolution(currentSolution);
        }
    }

    protected void initIfNeeded() {
        if (randomizer == null) {
            randomizer = randomizerFactory.newSolutionRandomizer();
        }

        if (evaluationFunction == null) {
            evaluationFunction = evaluationFunctionFactory.newEvaluationFunction();
        }

        if (currentSolution == null) {
            randomizeSolution();
        }

        if (neighbors == null) {
            neighbors = new ArrayList<>(mutationOperators.size());
        }
        if (neighbors.size() != mutationOperators.size()) {
            neighbors.clear();
            IntStream.range(0, mutationOperators.size())
                    .forEach(i -> neighbors.add(solutionFactory.newSolution()));
        }
    }

    protected void randomizeSolution() {
        currentSolution = solutionFactory.newSolution();
        currentSolution.randomizeUsing(randomizer);
    }

    protected void copyCurrentSolutionToNeighbors() {
        neighbors.clear();
        IntStream.range(0, mutationOperators.size())
                .forEach(n -> neighbors.add(currentSolution.clone()));
    }

    protected void mutateNeighbors() {
        IntStream.range(0, mutationOperators.size())
                .forEach(i -> neighbors.get(i).mutateUsing(mutationOperators.get(i)));
    }

    protected void evaluateNeighbors() {
        neighbors.forEach(neighbor -> neighbor.evaluateUsing(evaluationFunction));
    }

    /**
     * Make the best neighbor become our current solution.
     * @return True when there was a neighbor that is at least as good as the current solution.
     */
    protected boolean moveToBestNeighbor() {
        // find the best neighbor
        Solution bestNeighbor = Collections.max(neighbors);

        // if the best neighbor is at least equivalent to the current solution, move to it
        if (bestNeighbor.isEquivalentOrBetterThan(currentSolution)) {
            // swap the best neighbor with our current solution
            Solution tmp = currentSolution;
            currentSolution = bestNeighbor;
            neighbors.remove(bestNeighbor);
            neighbors.add(tmp);

            return true;
        }

        // else, no more good neighbor: the local search is over
        else {
            // notify listeners that we reached a local optimum
            notify(listener -> {
                if (listener instanceof Listener) {
                    ((Listener) listener).onLocalOptimumReached(iteration, currentSolution);
                }
            });

            return false;
        }
    }
}
