package com.vanolucas.opti.algo.local;

import com.vanolucas.opti.algo.local.LocalSearch;
import com.vanolucas.opti.algo.random.RandomSearchFactory;

public interface LocalSearchFactory extends RandomSearchFactory {

    LocalSearch newLocalSearch();
}
