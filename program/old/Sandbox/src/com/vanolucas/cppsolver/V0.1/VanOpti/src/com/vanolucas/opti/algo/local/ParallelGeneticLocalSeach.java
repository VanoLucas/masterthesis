package com.vanolucas.opti.algo.local;

import com.vanolucas.opti.mutation.MutationOperator;
import com.vanolucas.opti.solution.Individual;
import com.vanolucas.opti.solution.Solution;

import java.util.List;

public class ParallelGeneticLocalSeach extends GeneticLocalSearch {

    /*
    Constructors
     */

    public ParallelGeneticLocalSeach(GeneticLocalSearchFactory factory) {
        super(factory);
    }

    public ParallelGeneticLocalSeach(GeneticLocalSearchFactory factory, List<MutationOperator> mutationOperators) {
        super(factory, mutationOperators);
    }

    public ParallelGeneticLocalSeach(GeneticLocalSearchFactory factory, List<MutationOperator> mutationOperators, Solution startSolution) {
        super(factory, mutationOperators, startSolution);
    }

    /*
    Helpers
     */

    @Override
    protected void evaluateNeighbors() {
        // decode each neighbor's genome then evaluate it
        neighbors.parallelStream()
                .forEach(neighbor -> {
                    // decode this neighbor's genome
                    ((Individual) neighbor).decodeGenomeUsing(genomeDecoderFactory.newGenomeDecoder());
                    // we can then evaluate this neighbor
                    neighbor.evaluateUsing(evaluationFunction);
                });
    }
}
