package com.vanolucas.opti.algo.random;

import com.vanolucas.opti.eval.EvaluationFunction;
import com.vanolucas.opti.genome.decoder.GenomeDecoder;
import com.vanolucas.opti.genome.decoder.GenomeDecoderFactory;
import com.vanolucas.opti.randomizer.SolutionRandomizer;
import com.vanolucas.opti.solution.Individual;
import com.vanolucas.opti.solution.Solution;

/**
 * Genetic random search algorithm.
 * It evaluates a random solution at each iteration.
 * To run, the Random Search algorithm needs at least:
 * - The same thing as the Random Search algorithm on which it is based
 * - A Genome Decoder Factory or a Genome Decoder object
 */
public class GeneticRandomSearch extends RandomSearch {

    /*
    Attributes
     */

    private GenomeDecoder genomeDecoder;
    private GenomeDecoderFactory genomeDecoderFactory;

    /*
    Constructors
     */

    public GeneticRandomSearch(GeneticRandomSearchFactory factory) {
        this(factory.newIndividual(), factory.newGenomeRandomizer(), factory.newGenomeDecoder(), factory.newEvaluationFunction());
    }

    public GeneticRandomSearch(Solution solution, SolutionRandomizer randomizer, GenomeDecoder genomeDecoder, EvaluationFunction evaluationFunction) {
        super(solution, randomizer, evaluationFunction);
        this.genomeDecoder = genomeDecoder;
    }

    /*
    Helpers
     */

    /**
     * Init all necessary objects before running.
     */
    @Override
    protected void initIfNeeded() {
        super.initIfNeeded();

        if (genomeDecoder == null) {
            genomeDecoder = genomeDecoderFactory.newGenomeDecoder();
        }
    }

    /**
     * First decode the solution's Genome as it is a genetic individual,
     * then evaluate it.
     */
    @Override
    protected void evaluateSolution() {
        // decode the current individual's genome
        Individual currentIndiv = (Individual) currentSolution;
        currentIndiv.decodeGenomeUsing(genomeDecoder);

        // evaluate the current individual
        super.evaluateSolution();
    }
}
