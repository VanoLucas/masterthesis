package com.vanolucas.opti.algo.random;

import com.vanolucas.opti.algo.random.GeneticRandomSearch;
import com.vanolucas.opti.algo.random.RandomSearchFactory;
import com.vanolucas.opti.genome.decoder.GenomeDecoderFactory;
import com.vanolucas.opti.randomizer.GenomeRandomizerFactory;
import com.vanolucas.opti.solution.IndividualFactory;

public interface GeneticRandomSearchFactory extends RandomSearchFactory, GenomeRandomizerFactory, GenomeDecoderFactory, IndividualFactory {

    GeneticRandomSearch newGeneticRandomSearch();
}
