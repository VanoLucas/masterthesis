package com.vanolucas.opti.algo.random;

import com.vanolucas.opti.algo.OptiAlgo;
import com.vanolucas.opti.eval.EvaluationFunction;
import com.vanolucas.opti.eval.EvaluationFunctionFactory;
import com.vanolucas.opti.randomizer.SolutionRandomizer;
import com.vanolucas.opti.randomizer.SolutionRandomizerFactory;
import com.vanolucas.opti.solution.Solution;
import com.vanolucas.opti.solution.SolutionFactory;

/**
 * Random search algorithm.
 * It evaluates a random solution at each iteration.
 * To run, the Random Search algorithm needs at least:
 * - A Solution Factory or a Solution
 * - A Solution Randomizer Factory or a Solution Randomizer
 * - An Evaluation Function Factory or the Evaluation Function
 */
public class RandomSearch extends OptiAlgo {

    /*
    Attributes
     */

    protected Solution currentSolution;
    private SolutionRandomizer randomizer;
    private EvaluationFunction evaluationFunction;

    private SolutionFactory solutionFactory;
    private SolutionRandomizerFactory randomizerFactory;
    private EvaluationFunctionFactory evaluationFunctionFactory;

    /*
    Constructors
     */

    public RandomSearch(RandomSearchFactory factory) {
        this.solutionFactory = factory;
        this.randomizerFactory = factory;
        this.evaluationFunctionFactory = factory;
    }
    public RandomSearch(Solution solution, SolutionRandomizer randomizer, EvaluationFunction evaluationFunction) {
        this.currentSolution = solution;
        this.randomizer = randomizer;
        this.evaluationFunction = evaluationFunction;
    }

    /*
    Helpers
     */

    /**
     * Execute an iteration step of the Random Search algorithm
     */
    @Override
    protected void runIteration() {
        initIfNeeded();
        randomizeSolution();
        evaluateSolution();
        proposeNewSolution(currentSolution);
    }

    /**
     * Init all necessary objects before running.
     */
    protected void initIfNeeded() {
        if (currentSolution == null) {
            currentSolution = solutionFactory.newSolution();
        }

        if (randomizer == null) {
            randomizer = randomizerFactory.newSolutionRandomizer();
        }

        if (evaluationFunction == null) {
            evaluationFunction = evaluationFunctionFactory.newEvaluationFunction();
        }
    }

    /**
     * Fully randomize the current solution.
     */
    private void randomizeSolution() {
        currentSolution.randomizeUsing(randomizer);
    }

    /**
     * Evaluate the current solution.
     */
    protected void evaluateSolution() {
        currentSolution.evaluateUsing(evaluationFunction);
    }
}
