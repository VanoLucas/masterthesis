package com.vanolucas.opti.algo.random;

import com.vanolucas.opti.algo.random.RandomSearch;
import com.vanolucas.opti.eval.EvaluationFunctionFactory;
import com.vanolucas.opti.randomizer.SolutionRandomizerFactory;
import com.vanolucas.opti.solution.SolutionFactory;

public interface RandomSearchFactory extends SolutionFactory, SolutionRandomizerFactory, EvaluationFunctionFactory {

    RandomSearch newRandomSearch();
}
