package com.vanolucas.opti.eval;

import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.solution.Solution;
import com.vanolucas.opti.solution.Phenotype;

/**
 * Evaluates a Phenotype solution to calculate its Quality.
 */
public abstract class EvaluationFunction {
    /**
     * Conveniently evaluate the given Solution to update its Quality.
     * @param solution Solution to evaluate and update Quality.
     */
    public void evaluate(Solution solution) {
        solution.evaluateUsing(this);
    }

    /**
     * Override this to implement an Evaluation Function that calculates the Quality of the given Phenotype.
     * @param phenotype Phenotype to evaluate.
     * @return Quality of the evaluated Phenotype.
     */
    public abstract Quality evaluatePhenotype(Phenotype phenotype);
}
