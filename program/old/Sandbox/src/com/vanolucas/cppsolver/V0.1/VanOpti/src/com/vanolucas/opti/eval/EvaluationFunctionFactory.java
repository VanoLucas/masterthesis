package com.vanolucas.opti.eval;

public interface EvaluationFunctionFactory {

    EvaluationFunction newEvaluationFunction();
}
