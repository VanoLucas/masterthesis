package com.vanolucas.opti.genome;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Genome encoded as a vector of double values.
 *
 * todo If the genome has a GeneValueGuard, check and fix out of bounds values.
 * Without a GeneValueGuard, it is up to the operators to keep valid values in the Genome.
 */
public class DoubleGenome extends ListGenome<Double> {

    /*
    Constructors
     */

    public DoubleGenome() {
        super();
    }

    /**
     * Initialize this genome of double with 0s with the provided size.
     * @param size The size (number of genes) in this genome.
     */
    public DoubleGenome(final int size) {
        super(size);
        IntStream.range(0, size)
                .forEach(i -> genes.add(new Double(0d)));
    }

    public DoubleGenome(List<Double> genes) {
        super(genes);
    }

    /*
    Cloneable
     */

    @Override
    public DoubleGenome clone() {
        return new DoubleGenome(new ArrayList<>(genes));
    }
}
