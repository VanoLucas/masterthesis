package com.vanolucas.opti.genome;

/**
 * Fixes gene values to keep them within the allowed range.
 */
public abstract class GeneValueGuard<TGene> {

    public abstract TGene fixValue(TGene gene);
}
