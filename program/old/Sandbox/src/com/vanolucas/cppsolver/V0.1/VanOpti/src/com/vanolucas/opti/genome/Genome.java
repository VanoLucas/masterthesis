package com.vanolucas.opti.genome;

import com.vanolucas.opti.util.Cloneable;

/**
 * Represents a Genome.
 * This is an encoded representation that defines a solution and can be decoded.
 */
public abstract class Genome implements Cloneable {

    /*
    Cloneable
     */

    @Override
    public abstract Genome clone();

    /*
    toString
     */

    @Override
    public abstract String toString();
}
