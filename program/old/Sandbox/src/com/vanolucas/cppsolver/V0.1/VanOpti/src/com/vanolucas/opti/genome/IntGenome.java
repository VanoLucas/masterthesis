package com.vanolucas.opti.genome;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Genome encoded as a vector of integer values.
 */
public class IntGenome extends ListGenome<Integer> {

    /*
    Constructors
     */

    public IntGenome() {
        super();
    }

    /**
     * Initialize this genome of double with 0s with the provided size.
     * @param size The size (number of genes) in this genome.
     */
    public IntGenome(final int size) {
        super(size);
        IntStream.range(0, size)
                .forEach(i -> genes.add(new Integer(0)));
    }

    public IntGenome(List<Integer> genes) {
        super(genes);
    }

    /*
    Cloneable
     */

    @Override
    public IntGenome clone() {
        return new IntGenome(new ArrayList<>(genes));
    }
}
