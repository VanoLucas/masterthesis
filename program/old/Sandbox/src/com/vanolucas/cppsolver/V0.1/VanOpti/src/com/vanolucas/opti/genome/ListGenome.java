package com.vanolucas.opti.genome;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Represents a Genome that is a vector of values.
 * @param <T> Type of the genes (values) in this Genome.
 */
public abstract class ListGenome<T> extends Genome implements Iterable<T> {

    /*
    Attributes
     */

    protected List<T> genes;

    /*
    Constructors
     */

    public ListGenome() {
        this.genes = new ArrayList<>();
    }
    public ListGenome(int expectedSize) {
        this.genes = new ArrayList<>(expectedSize);
    }
    public ListGenome(List<T> genes) {
        this.genes = genes;
    }

    /*
    Accessors
     */

    public int size() {
        return genes.size();
    }

    public List<T> getGenes() {
        return genes;
    }

    public List<T> getGenes(int fromIndexInclusive, int toIndexExclusive) {
        return genes.subList(fromIndexInclusive, toIndexExclusive);
    }

    public void setGene(int index, T value) {
        genes.set(index, value);
    }

    /*
    Iterable
     */

    @Override
    public Iterator<T> iterator() {
        return genes.iterator();
    }

    @Override
    public Spliterator<T> spliterator() {
        return genes.spliterator();
    }

    @Override
    public void forEach(Consumer<? super T> action) {
        genes.forEach(action);
    }

    /*
    Stream
     */

    public Stream<T> stream() {
        return genes.stream();
    }

    /*
    toString
     */

    @Override
    public String toString() {
        return "(" + size() + " genes) " + genes.stream()
                .map(g -> g.toString())
                .collect(Collectors.joining(","));
    }
}
