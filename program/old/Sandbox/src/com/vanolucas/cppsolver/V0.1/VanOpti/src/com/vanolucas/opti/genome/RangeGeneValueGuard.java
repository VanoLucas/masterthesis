package com.vanolucas.opti.genome;

public abstract class RangeGeneValueGuard<TGene> extends GeneValueGuard<TGene> {

    /**
     * Lowest value allowed in this range (inclusive).
     */
    protected TGene minInclusive;
    /**
     * Highest value allowed in this range (exclusive).
     */
    protected TGene maxExclusive;

    /*
    Constructors
     */

    public RangeGeneValueGuard(TGene minInclusive, TGene maxExclusive) {
        this.minInclusive = minInclusive;
        this.maxExclusive = maxExclusive;
    }
}
