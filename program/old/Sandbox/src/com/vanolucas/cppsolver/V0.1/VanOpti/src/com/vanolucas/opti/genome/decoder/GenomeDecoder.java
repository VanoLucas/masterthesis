package com.vanolucas.opti.genome.decoder;

import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.solution.Individual;
import com.vanolucas.opti.solution.Phenotype;

/**
 * Decoder able to decode a Genome to its corresponding Phenotype.
 */
public abstract class GenomeDecoder {
    /**
     * Conveniently decode an Individual's Genome to calculate its Phenotype.
     * @param individual Individual for which to decode the Genome and update Phenotype.
     */
    public void decode(Individual individual) {
        individual.decodeGenomeUsing(this);
    }

    /**
     * Override this to implement the Genome decoding algorithm.
     * @param genome Genome to decode to the Phenotype it describes.
     * @return Phenotype described by the input Genome.
     */
    public abstract Phenotype decodeGenome(Genome genome);
}
