package com.vanolucas.opti.genome.decoder;

public interface GenomeDecoderFactory {

    GenomeDecoder newGenomeDecoder();
}
