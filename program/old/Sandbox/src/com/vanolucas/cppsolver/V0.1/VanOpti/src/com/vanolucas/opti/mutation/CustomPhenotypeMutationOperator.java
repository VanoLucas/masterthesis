package com.vanolucas.opti.mutation;

import com.vanolucas.opti.solution.Phenotype;

public abstract class CustomPhenotypeMutationOperator<TPhenotype> extends PhenotypeMutationOperator {

    @Override
    public void mutatePhenotype(Phenotype phenotype) {
        mutate(((TPhenotype) phenotype.get()));
    }

    /**
     * Override this method to implement the mutation operator on the desired phenotype object.
     * @param phenotype Phenotype solution to mutate with this operator.
     */
    public abstract void mutate(TPhenotype phenotype);
}
