package com.vanolucas.opti.mutation;

import com.vanolucas.opti.genome.DoubleGenome;
import com.vanolucas.opti.genome.Genome;

public abstract class DoubleGenomeMutationOperator extends GeneticMutationOperator {
    @Override
    public void mutateGenome(Genome genome) {
        mutateDoubleGenome((DoubleGenome) genome);
    }

    public abstract void mutateDoubleGenome(DoubleGenome genome);
}
