package com.vanolucas.opti.mutation;

import com.vanolucas.opti.solution.Individual;
import com.vanolucas.opti.solution.Solution;
import com.vanolucas.opti.genome.Genome;

/**
 * Performs a Genome mutation.
 */
public abstract class GeneticMutationOperator extends MutationOperator {

    @Override
    public void mutate(Solution solution) {
        Individual indiv = (Individual) solution;
        indiv.mutateGenomeUsing(this);
    }

    /**
     * Override to implement an operator that mutates the given Genome.
     * @param genome Genome to mutate.
     */
    public abstract void mutateGenome(Genome genome);
}
