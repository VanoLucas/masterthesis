package com.vanolucas.opti.mutation;

import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.genome.IntGenome;

public abstract class IntGenomeMutationOperator extends GeneticMutationOperator {

    @Override
    public void mutateGenome(Genome genome) {
        mutateIntGenome((IntGenome) genome);
    }

    public abstract void mutateIntGenome(IntGenome genome);
}
