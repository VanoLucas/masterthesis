package com.vanolucas.opti.mutation;

import com.vanolucas.opti.solution.Solution;

/**
 * Performs a mutation operation on a Solution.
 */
public abstract class MutationOperator {

    public abstract void mutate(Solution solution);
}
