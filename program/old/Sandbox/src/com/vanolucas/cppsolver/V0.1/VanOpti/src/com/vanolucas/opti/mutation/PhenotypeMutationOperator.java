package com.vanolucas.opti.mutation;

import com.vanolucas.opti.solution.Phenotype;
import com.vanolucas.opti.solution.Solution;

/**
 * Performs a Solution (Phenotype) mutation.
 */
public abstract class PhenotypeMutationOperator extends MutationOperator {

    @Override
    public void mutate(Solution solution) {
        solution.mutatePhenotypeUsing(this);
    }

    /**
     * Override this to implement an operator that mutates the given Phenotype.
     * @param phenotype Phenotype to mutate.
     */
    public abstract void mutatePhenotype(Phenotype phenotype);
}
