package com.vanolucas.opti.mutation;

import com.vanolucas.opti.genome.DoubleGenome;

import java.util.Random;

public class Random01DoubleGenesMutationOperator extends DoubleGenomeMutationOperator {

    /*
    Defaults
     */

    private static final double DEFAULT_PROBA_GENE_MUTATION = 0.05;

    /*
    Attributes
     */

    private double probaGeneMutation;
    private Random rand;

    /*
    Constructors
     */

    public Random01DoubleGenesMutationOperator() {
        this(DEFAULT_PROBA_GENE_MUTATION);
    }
    public Random01DoubleGenesMutationOperator(final double probaGeneMutation) {
        setProbaGeneMutation(probaGeneMutation);
        this.rand = new Random();
    }

    /*
    Accessors
     */

    public void setProbaGeneMutation(double probaGeneMutation) {
        this.probaGeneMutation = probaGeneMutation;
    }

    /*
    Mutation Operator
     */

    @Override
    public void mutateDoubleGenome(DoubleGenome genome) {
        // for each gene
        for (int g=0 ; g<genome.size() ; g++) {
            // decide whether or not to mutate it based on the provided probability
            if (rand.nextDouble() < probaGeneMutation) {
                // randomize this gene
                genome.setGene(g, rand.nextDouble());
            }
        }
    }
}
