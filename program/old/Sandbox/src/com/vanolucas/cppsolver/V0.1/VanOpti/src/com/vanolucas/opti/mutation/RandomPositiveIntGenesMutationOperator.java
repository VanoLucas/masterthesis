package com.vanolucas.opti.mutation;

import com.vanolucas.opti.genome.IntGenome;

import java.util.Random;

public class RandomPositiveIntGenesMutationOperator extends IntGenomeMutationOperator {


    /*
    Defaults
     */

    private static final double DEFAULT_PROBA_GENE_MUTATION = 0.05;

    /*
    Attributes
     */

    private double probaGeneMutation;
    private Random rand;

    /*
    Constructors
     */

    public RandomPositiveIntGenesMutationOperator() {
        this(DEFAULT_PROBA_GENE_MUTATION);
    }
    public RandomPositiveIntGenesMutationOperator(final double probaGeneMutation) {
        setProbaGeneMutation(probaGeneMutation);
        this.rand = new Random();
    }

    /*
    Accessors
     */

    public void setProbaGeneMutation(double probaGeneMutation) {
        this.probaGeneMutation = probaGeneMutation;
    }

    /*
    Mutation Operator
     */

    @Override
    public void mutateIntGenome(IntGenome genome) {

        // for each gene
        for (int g=0 ; g<genome.size() ; g++) {
            // decide whether or not to mutate it based on the provided probability
            if (rand.nextDouble() < probaGeneMutation) {
                // randomize this gene
                genome.setGene(g, Math.abs(rand.nextInt()));
            }
        }
    }
}
