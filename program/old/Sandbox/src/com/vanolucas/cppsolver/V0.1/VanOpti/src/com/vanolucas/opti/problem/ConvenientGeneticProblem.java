package com.vanolucas.opti.problem;

import com.vanolucas.opti.eval.EvaluationFunction;
import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.genome.decoder.GenomeDecoder;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.randomizer.GenomeRandomizer;
import com.vanolucas.opti.solution.Phenotype;

public abstract class ConvenientGeneticProblem<TPhenotype, TGenome extends Genome, TQuality extends Quality> extends GeneticProblem {

// TODO inherit constructors

    /*
    Factories
     */

    @Override
    public GenomeRandomizer newGenomeRandomizer() {
        return new GenomeRandomizer() {
            @Override
            public void randomizeGenome(Genome genome) {
                ConvenientGeneticProblem.this.randomize(((TGenome) genome));
            }
        };
    }

    @Override
    public EvaluationFunction newEvaluationFunction() {
        return new EvaluationFunction() {
            @Override
            public Quality evaluatePhenotype(Phenotype phenotype) {
                return ConvenientGeneticProblem.this.evaluate((TPhenotype) phenotype.get());
            }
        };
    }

    @Override
    public Genome newGenome() {
        return defaultGenome();
    }

    @Override
    public GenomeDecoder newGenomeDecoder() {
        return new GenomeDecoder() {
            @Override
            public Phenotype decodeGenome(Genome genome) {
                return new Phenotype(ConvenientGeneticProblem.this.decode((TGenome) genome));
            }
        };
    }

    @Override
    public Quality newQuality() {
        return defaultQuality();
    }

    @Override
    public Phenotype newPhenotype() {
        return new Phenotype(defaultPhenotype());
    }

    /*
    Optimization Components to Implement
     */

    public abstract TPhenotype defaultPhenotype();
    public abstract TGenome defaultGenome();
    public abstract TQuality defaultQuality();
    public abstract void randomize(TGenome genome);
    public abstract TPhenotype decode(TGenome genome);
    public abstract TQuality evaluate(TPhenotype phenotype);
}
