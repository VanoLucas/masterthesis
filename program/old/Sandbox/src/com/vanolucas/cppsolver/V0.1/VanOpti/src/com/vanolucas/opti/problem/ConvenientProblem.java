package com.vanolucas.opti.problem;

import com.vanolucas.opti.eval.EvaluationFunction;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.randomizer.PhenotypeRandomizer;
import com.vanolucas.opti.solution.Phenotype;

public abstract class ConvenientProblem<TPhenotype, TQuality extends Quality> extends Problem {

// TODO inherit constructors

    /*
    Factories
     */

    @Override
    public PhenotypeRandomizer newPhenotypeRandomizer() {
        return new PhenotypeRandomizer() {
            @Override
            public void randomizePhenotype(Phenotype phenotype) {
                ConvenientProblem.this.randomize((TPhenotype) phenotype.get());
            }
        };
    }

    @Override
    public EvaluationFunction newEvaluationFunction() {
        return new EvaluationFunction() {
            @Override
            public Quality evaluatePhenotype(Phenotype phenotype) {
                return ConvenientProblem.this.evaluate((TPhenotype) phenotype.get());
            }
        };
    }

    @Override
    public Phenotype newPhenotype() {
        return new Phenotype(defaultPhenotype());
    }

    @Override
    public TQuality newQuality() {
        return defaultQuality();
    }

    /*
    Optimization Components to Implement
     */

    public abstract TPhenotype defaultPhenotype();
    public abstract TQuality defaultQuality();
    public abstract void randomize(TPhenotype phenotype);
    public abstract TQuality evaluate(TPhenotype phenotype);
}
