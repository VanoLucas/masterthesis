package com.vanolucas.opti.problem;

import com.vanolucas.opti.algo.local.GeneticLocalSearch;
import com.vanolucas.opti.algo.local.GeneticLocalSearchFactory;
import com.vanolucas.opti.algo.local.ParallelGeneticLocalSeach;
import com.vanolucas.opti.algo.random.GeneticRandomSearch;
import com.vanolucas.opti.algo.random.GeneticRandomSearchFactory;
import com.vanolucas.opti.algo.random.RandomSearch;
import com.vanolucas.opti.genome.decoder.GenomeDecoderFactory;
import com.vanolucas.opti.randomizer.GenomeRandomizer;
import com.vanolucas.opti.randomizer.GenomeRandomizerFactory;
import com.vanolucas.opti.randomizer.PhenotypeRandomizer;
import com.vanolucas.opti.randomizer.SolutionRandomizer;
import com.vanolucas.opti.solution.Individual;
import com.vanolucas.opti.solution.IndividualFactory;

public abstract class GeneticProblem extends Problem
        implements GenomeRandomizerFactory, GenomeDecoderFactory, IndividualFactory,
        GeneticRandomSearchFactory, GeneticLocalSearchFactory {

// TODO inherit constructors

    /*
    Component Factories
     */

    @Override
    public SolutionRandomizer newSolutionRandomizer() {
        return newGenomeRandomizer();
    }

    @Override
    public abstract GenomeRandomizer newGenomeRandomizer();

    /**
     * Phenotype Randomizer is useless for genetic (Genome-encoded) solutions.
     *
     * @return Always null.
     */
    @Override
    public PhenotypeRandomizer newPhenotypeRandomizer() {
        return null;
    }


    @Override
    public Individual newSolution() {
        return newIndividual();
    }

    @Override
    public Individual newIndividual() {
        return new Individual(this);
    }

    @Override
    public RandomSearch newRandomSearch() {
        return newGeneticRandomSearch();
    }

    /*
    Optimization Algorithm Factories
     */

    @Override
    public GeneticRandomSearch newGeneticRandomSearch() {
        return new GeneticRandomSearch(this);
    }

    @Override
    public GeneticLocalSearch newGeneticLocalSearch() {
        return new GeneticLocalSearch(this);
    }

    @Override
    public ParallelGeneticLocalSeach newParallelGeneticLocalSearch() {
        return new ParallelGeneticLocalSeach(this);
    }
}
