package com.vanolucas.opti.problem;

import com.vanolucas.opti.algo.local.LocalSearch;
import com.vanolucas.opti.algo.local.LocalSearchFactory;
import com.vanolucas.opti.algo.random.RandomSearch;
import com.vanolucas.opti.algo.random.RandomSearchFactory;
import com.vanolucas.opti.eval.EvaluationFunctionFactory;
import com.vanolucas.opti.randomizer.PhenotypeRandomizer;
import com.vanolucas.opti.randomizer.SolutionRandomizer;
import com.vanolucas.opti.randomizer.SolutionRandomizerFactory;
import com.vanolucas.opti.solution.Solution;
import com.vanolucas.opti.solution.SolutionFactory;

import java.util.UUID;

public abstract class Problem
        implements SolutionFactory, SolutionRandomizerFactory, EvaluationFunctionFactory,
        RandomSearchFactory, LocalSearchFactory {

    private String name;

    /*
    Constructors
     */

    public Problem() {
        this(UUID.randomUUID().toString());
    }

    public Problem(String name) {
        this.name = name;
    }

    /*
    Accessors
     */

    public String getName() {
        return name;
    }

    /*
    Factories
     */

    @Override
    public SolutionRandomizer newSolutionRandomizer() {
        return newPhenotypeRandomizer();
    }

    public abstract PhenotypeRandomizer newPhenotypeRandomizer();

    @Override
    public Solution newSolution() {
        return new Solution(this);
    }

    @Override
    public RandomSearch newRandomSearch() {
        return new RandomSearch(this);
    }

    @Override
    public LocalSearch newLocalSearch() {
        return new LocalSearch(this);
    }
}
