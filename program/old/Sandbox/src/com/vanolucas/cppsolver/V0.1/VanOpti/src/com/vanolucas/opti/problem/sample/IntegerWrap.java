package com.vanolucas.opti.problem.sample;

public class IntegerWrap {

    private Integer value;

    /*
    Constructors
     */

    public IntegerWrap() {
        this(0);
    }
    public IntegerWrap(int value) {
        set(value);
    }

    /*
    Accessors
     */

    public Integer get() {
        return value;
    }

    public void set(int value) {
        this.value = value;
    }

    /*
    toString
     */

    @Override
    public String toString() {
        return value.toString();
    }
}
