package com.vanolucas.opti.problem.sample;

import com.vanolucas.opti.problem.ConvenientProblem;
import com.vanolucas.opti.problem.Problem;
import com.vanolucas.opti.quality.LongCost;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.randomizer.PhenotypeRandomizer;
import com.vanolucas.opti.solution.Phenotype;

import java.util.Random;

public class TargetNumberProblem extends ConvenientProblem<IntegerWrap, LongCost> {

    private int targetNumber;

    /*
    Constructors
     */

    public TargetNumberProblem(int targetNumber) {
        setTargetNumber(targetNumber);
    }

    /*
    Accessors
     */

    public double getTargetNumber() {
        return targetNumber;
    }

    public void setTargetNumber(int targetNumber) {
        this.targetNumber = targetNumber;
    }

    /*
    Definitions
     */

    @Override
    public IntegerWrap defaultPhenotype() {
        return new IntegerWrap();
    }

    @Override
    public LongCost defaultQuality() {
        return new LongCost();
    }

    @Override
    public void randomize(IntegerWrap phenotype) {
        phenotype.set(new Random().nextInt(1000000000 + 1));
    }

    @Override
    public LongCost evaluate(IntegerWrap phenotype) {
        int value = phenotype.get();
        long distance = Math.abs(value - targetNumber);
        return new LongCost(distance);
    }
}
