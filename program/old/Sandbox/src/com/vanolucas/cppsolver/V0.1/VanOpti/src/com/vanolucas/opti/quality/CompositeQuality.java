package com.vanolucas.opti.quality;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class CompositeQuality extends Quality implements Iterable<Quality> {

    /*
    Attributes
     */

    protected List<Quality> qualities;

    /*
    Constructors
     */

    public CompositeQuality(int size) {
        this.qualities = new ArrayList<>(size);
    }
    public CompositeQuality(List<Quality> compositeQualities) {
        this.qualities = compositeQualities;
    }

    /*
    Comparison
     */

    @Override
    public boolean isBetterThan(Quality o) {
        CompositeQuality other = (CompositeQuality) o;

        int nbQualities = qualities.size();
        boolean atLeastOneBetter = false;
        // for each composite quality
        for (int i=0 ; i<nbQualities ; i++) {
            // are all components at least equivalent?
            // if any quality component is worse than the other, we're not better
            if (this.qualities.get(i).isWorseThan(other.qualities.get(i))) {
                return false;
            }
            // we need at least one component to be better than the other to be better overall
            else if (!atLeastOneBetter && this.qualities.get(i).isBetterThan(other.qualities.get(i))) {
                atLeastOneBetter = true;
            }
        }
        return atLeastOneBetter;
    }

    @Override
    public boolean isWorseThan(Quality o) {
        CompositeQuality other = (CompositeQuality) o;

        int nbQualities = qualities.size();
        boolean atLeastOneWorse = false;
        // for each composite quality
        for (int i=0 ; i<nbQualities ; i++) {
            // if any quality component is better than the other, we're not worse
            if (this.qualities.get(i).isBetterThan(other.qualities.get(i))) {
                return false;
            }
            // we need at least one component to be worse than the other to be worse overall
            else if (!atLeastOneWorse && this.qualities.get(i).isWorseThan(other.qualities.get(i))) {
                atLeastOneWorse = true;
            }
        }
        return atLeastOneWorse;
    }

    @Override
    public double differenceWith(Quality o) {
        CompositeQuality other = (CompositeQuality) o;
        int nbComponents = Math.min(this.qualities.size(), other.qualities.size());
        double sum = 0.0d;
        for (int i=0 ; i<nbComponents ; i++) {
            sum += this.qualities.get(i).differenceWith(other.qualities.get(i));
        }
        return sum;
    }

    @Override
    public boolean isEquivalentTo(Quality o) {
        return !this.dominates(o) && !this.isDominatedBy(o);
    }

    /*
    Cloneable
     */

    @Override
    public Quality clone() {
        return new CompositeQuality(this.qualities.stream().map(Quality::clone).collect(Collectors.toList()));
    }

    /*
    Iterable
     */

    @Override
    public Iterator<Quality> iterator() {
        return qualities.iterator();
    }

    @Override
    public void forEach(Consumer<? super Quality> action) {
        qualities.forEach(action);
    }

    @Override
    public Spliterator<Quality> spliterator() {
        return qualities.spliterator();
    }

    /*
    toString
     */

    @Override
    public String toString() {
        return qualities.stream()
                .map(Quality::toString)
                .collect(Collectors.joining(","));
    }
}
