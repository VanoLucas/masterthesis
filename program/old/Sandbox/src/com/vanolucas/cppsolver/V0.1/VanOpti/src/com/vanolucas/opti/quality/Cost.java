package com.vanolucas.opti.quality;

public abstract class Cost<T extends Number & Comparable<T>> extends SingleValueQuality<T> {

    /*
    Constructors
     */

    public Cost(Cost<T> o) {
        this(o.value);
    }
    public Cost(T value) {
        super(value);
    }

    /**
     * Compare to another Cost.
     * A lower cost is better.
     * @param o The other Cost to compare this one to.
     * @return True if this cost is better than the other one, false if worse or equal.
     */
    @Override
    public boolean isBetterThan(Quality o) {
        Cost<T> other = (Cost<T>) o;
        return this.value.compareTo(other.value) < 0;
    }

}
