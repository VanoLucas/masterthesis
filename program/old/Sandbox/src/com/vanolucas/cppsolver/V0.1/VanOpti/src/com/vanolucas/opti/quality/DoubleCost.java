package com.vanolucas.opti.quality;

public class DoubleCost extends Cost<Double> {

    /*
    Constructors
     */

    public DoubleCost() {
        super(Double.MAX_VALUE);
    }
    public DoubleCost(Cost<Double> o) {
        super(o);
    }
    public DoubleCost(double cost) {
        super(cost);
    }

    /*
    Accessors
     */

    public double doubleValue() {
        return this.value.doubleValue();
    }

    /*
    Comparison
     */

    @Override
    public double differenceWith(Quality o) {
        DoubleCost other = (DoubleCost) o;
        return (this.value - other.value) * (this.value - other.value);
    }

    /*
    Cloneable
     */

    @Override
    public DoubleCost clone() {
        return new DoubleCost(this);
    }

}
