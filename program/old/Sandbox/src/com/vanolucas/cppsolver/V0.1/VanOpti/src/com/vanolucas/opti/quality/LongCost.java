package com.vanolucas.opti.quality;

public class LongCost extends Cost<Long> {

    /*
    Constructors
     */

    public LongCost() {
        super(Long.MAX_VALUE);
    }
    public LongCost(Cost<Long> o) {
        super(o);
    }
    public LongCost(long cost) {
        super(cost);
    }

    /*
    Accessors
     */

    public double doubleValue() {
        return this.value.doubleValue();
    }

    /*
    Comparison
     */

    @Override
    public double differenceWith(Quality o) {
        LongCost other = (LongCost) o;
        return (this.value - other.value) * (this.value - other.value);
    }

    /*
    Cloneable
     */

    @Override
    public LongCost clone() {
        return new LongCost(this);
    }

}
