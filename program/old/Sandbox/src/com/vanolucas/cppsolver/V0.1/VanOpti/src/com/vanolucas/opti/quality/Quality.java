package com.vanolucas.opti.quality;

/**
 * Represents the Quality of a solution.
 */
public abstract class Quality implements Comparable<Quality>, Cloneable {

    /*
    Comparison
     */

    /**
     * Override this to define how a Quality is better than another.
     * @param other Other Quality to compare this one to.
     * @return True if this Quality is better than the other one.
     */
    public abstract boolean isBetterThan(Quality other);

    /**
     * Override this to define how a Quality is equivalent to another.
     * @param other Other Quality to compare this one to.
     * @return True if this Quality is equivalent to the other one.
     */
    public abstract boolean isEquivalentTo(Quality other);

    public boolean isEquivalentOrBetterThan(Quality other) {
        return isBetterThan(other) || isEquivalentTo(other);
    }

    public boolean isWorseOrEquivalentTo(Quality other) {
        return !isBetterThan(other);
    }

    public boolean isWorseThan(Quality other) {
        return !isEquivalentOrBetterThan(other);
    }

    public boolean dominates(Quality other) {
        return isBetterThan(other);
    }

    public boolean isDominatedBy(Quality other) {
        return isWorseThan(other);
    }

    /**
     * Returns the Squared Euclidean distance between this Quality and the one provided as arg.
     * @param other The other Quality we want to get the distance with.
     * @return The Squared Euclidean distance between this Quality and the one provided as arg.
     */
    public abstract double differenceWith(Quality other);

    @Override
    public boolean equals(Object o) {
        Quality other = (Quality) o;
        return this.isEquivalentTo(other);
    }

    /*
    Comparable
     */

    /**
     * Compare two qualities.
     * @param other The other Quality to compare this one to.
     * @return Positive if this Quality is better than the other one.
     */
    @Override
    public int compareTo(Quality other) {
        if (this.isBetterThan(other)) {
            return 1;
        } else if (this.isEquivalentTo(other)) {
            return 0;
        } else {
            return -1;
        }
    }

    /*
    Cloneable
     */

    @Override
    public abstract Quality clone();

    /*
    toString
     */

    @Override
    public abstract String toString();
}
