package com.vanolucas.opti.randomizer;

import com.vanolucas.opti.genome.DoubleGenome;
import com.vanolucas.opti.genome.Genome;

import java.util.Random;
import java.util.stream.IntStream;

public class DoubleGenomeRandomizer extends GenomeRandomizer {

    private Random rand;

    public DoubleGenomeRandomizer() {
        this.rand = new Random();
    }

    @Override
    public void randomizeGenome(Genome genome) {
        DoubleGenome g = (DoubleGenome) genome;

        IntStream.range(0, g.size())
                .forEach(index -> g.setGene(index, rand.nextDouble()));
    }
}
