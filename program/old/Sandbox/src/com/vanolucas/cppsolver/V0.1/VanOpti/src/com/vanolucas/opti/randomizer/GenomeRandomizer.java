package com.vanolucas.opti.randomizer;

import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.solution.Individual;
import com.vanolucas.opti.solution.Solution;

/**
 * Fully randomizes a given Genome.
 */
public abstract class GenomeRandomizer extends SolutionRandomizer {
    @Override
    public void randomize(Solution solution) {
        Individual indiv = (Individual) solution;
        indiv.randomizeGenomeUsing(this);
    }

    /**
     * Override this method to implement the randomization of a given Genome.
     * @param genome Genome to randomize.
     */
    public abstract void randomizeGenome(Genome genome);
}
