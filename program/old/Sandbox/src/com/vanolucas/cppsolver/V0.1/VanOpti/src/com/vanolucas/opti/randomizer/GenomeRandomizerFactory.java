package com.vanolucas.opti.randomizer;

public interface GenomeRandomizerFactory extends SolutionRandomizerFactory {

    GenomeRandomizer newGenomeRandomizer();
}
