package com.vanolucas.opti.randomizer;

import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.genome.IntGenome;

import java.util.Random;
import java.util.stream.IntStream;

public class IntGenomeRandomizer extends GenomeRandomizer {

    private Random rand;

    public IntGenomeRandomizer() {
        this.rand = new Random();
    }

    @Override
    public void randomizeGenome(Genome genome) {
        IntGenome g = (IntGenome) genome;

        IntStream.range(0, g.size())
                .forEach(index -> g.setGene(index, Math.abs(rand.nextInt())));
    }
}
