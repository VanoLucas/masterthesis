package com.vanolucas.opti.randomizer;

import com.vanolucas.opti.solution.Phenotype;
import com.vanolucas.opti.solution.Solution;

/**
 * Fully randomizes a given Phenotype.
 */
public abstract class PhenotypeRandomizer extends SolutionRandomizer {

    @Override
    public void randomize(Solution solution) {
        solution.randomizePhenotypeUsing(this);
    }

    /**
     * Override this method to implement randomization of a given solution Phenotype.
     * @param phenotype Phenotype (solution) to randomize.
     */
    public abstract void randomizePhenotype(Phenotype phenotype);
}
