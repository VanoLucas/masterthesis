package com.vanolucas.opti.randomizer;

public interface PhenotypeRandomizerFactory extends SolutionRandomizerFactory {

    PhenotypeRandomizer newPhenotypeRandomizer();
}
