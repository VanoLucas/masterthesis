package com.vanolucas.opti.randomizer;

import com.vanolucas.opti.solution.Solution;

/**
 * Performs randomization of a given Solution.
 */
public abstract class SolutionRandomizer {

    public abstract void randomize(Solution solution);
}
