package com.vanolucas.opti.randomizer;

public interface SolutionRandomizerFactory {

    SolutionRandomizer newSolutionRandomizer();
}
