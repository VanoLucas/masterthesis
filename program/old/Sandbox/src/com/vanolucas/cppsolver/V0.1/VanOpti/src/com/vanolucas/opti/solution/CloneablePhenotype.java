package com.vanolucas.opti.solution;

import com.vanolucas.opti.util.Cloneable;

public class CloneablePhenotype extends Phenotype implements Cloneable {

    /*
    Constructors
     */

    public CloneablePhenotype() {
    }

    public CloneablePhenotype(Object phenotype) {
        super(phenotype);
    }

    /*
    Cloneable
     */

    @Override
    public CloneablePhenotype clone() {
        return new CloneablePhenotype(((Cloneable)phenotype).clone());
    }
}
