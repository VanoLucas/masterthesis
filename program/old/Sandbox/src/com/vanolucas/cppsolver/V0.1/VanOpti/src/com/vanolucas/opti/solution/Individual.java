package com.vanolucas.opti.solution;

import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.genome.decoder.GenomeDecoder;
import com.vanolucas.opti.mutation.GeneticMutationOperator;
import com.vanolucas.opti.mutation.MutationOperator;
import com.vanolucas.opti.problem.GeneticProblem;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.randomizer.GenomeRandomizer;
import com.vanolucas.opti.randomizer.SolutionRandomizer;

/**
 * Materializes a genetic solution by its Genome, Phenotype and Quality.
 */
public class Individual extends Solution {

    private Genome genome;
    private boolean areGenomePhenotypeAligned = false;

    /*
    Constructors
     */

    public Individual() {
        super();
    }
    public Individual(IndividualFactory factory) {
        this(factory.newGenome(), factory.newPhenotype(), factory.newQuality());
    }
    public Individual(Genome genome, Phenotype phenotype, Quality quality) {
        super(phenotype, quality);
        this.genome = genome;
    }

    /*
    Commands
     */

    @Override
    public void randomizeUsing(SolutionRandomizer randomizer) {
        GenomeRandomizer genomeRandomizer = (GenomeRandomizer) randomizer;
        randomizeGenomeUsing(genomeRandomizer);
    }

    public void randomizeGenomeUsing(GenomeRandomizer genomeRandomizer) {
        areGenomePhenotypeAligned = false;
        genomeRandomizer.randomizeGenome(genome);
    }

    @Override
    public void mutateUsing(MutationOperator mutationOperator) {
        GeneticMutationOperator geneticMutationOperator = (GeneticMutationOperator) mutationOperator;
        mutateGenomeUsing(geneticMutationOperator);
    }

    public void mutateGenomeUsing(GeneticMutationOperator mutationOperator) {
        areGenomePhenotypeAligned = false;
        mutationOperator.mutateGenome(genome);
    }

    public void decodeGenomeUsing(GenomeDecoder genomeDecoder) {
        phenotype = genomeDecoder.decodeGenome(genome);
        areGenomePhenotypeAligned = true;
    }

    /*
    Cloneable
     */

    @Override
    public Solution clone() {
        Individual copy = new Individual();

        copy.genome = genome.clone();
        copy.phenotype = ((CloneablePhenotype) phenotype).clone();
        copy.quality = quality.clone();
        copy.areGenomePhenotypeAligned = areGenomePhenotypeAligned;

        return copy;
    }

    /*
    toString
     */

    @Override
    public String toString() {
        return "Genome: " + genomeToString() + "\n" + phenotypeToString() + "\nQuality: " + qualityToString();
    }

    public String genomeToString() {
        return genome.toString();
    }
}
