package com.vanolucas.opti.solution;

import com.vanolucas.opti.genome.GenomeFactory;

public interface IndividualFactory extends SolutionFactory, GenomeFactory {

    Individual newIndividual();
}
