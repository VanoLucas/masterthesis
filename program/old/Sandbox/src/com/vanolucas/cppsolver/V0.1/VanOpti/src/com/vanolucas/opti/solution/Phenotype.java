package com.vanolucas.opti.solution;

public class Phenotype {
    protected Object phenotype;

    /*
    Constructors
     */

    public Phenotype() {}
    public Phenotype(Object phenotype) {
        set(phenotype);
    }

    /*
    Accessors
     */

    public Object get() {
        return phenotype;
    }

    public void set(Object phenotype) {
        this.phenotype = phenotype;
    }

    /*
    toString
     */

    @Override
    public String toString() {
        return phenotype.toString();
    }
}
