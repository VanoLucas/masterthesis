package com.vanolucas.opti.solution;

public interface PhenotypeFactory {

    Phenotype newPhenotype();
}
