package com.vanolucas.opti.solution;

import com.vanolucas.opti.eval.EvaluationFunction;
import com.vanolucas.opti.math.ParetoFront;
import com.vanolucas.opti.mutation.MutationOperator;
import com.vanolucas.opti.mutation.PhenotypeMutationOperator;
import com.vanolucas.opti.problem.Problem;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.randomizer.PhenotypeRandomizer;
import com.vanolucas.opti.randomizer.SolutionRandomizer;

/**
 * Materializes a Solution by its Phenotype and Quality.
 */
public class Solution implements Comparable, Cloneable {

    protected Phenotype phenotype;
    protected Quality quality;

    /*
    Constructor
     */

    public Solution() {}
    public Solution(Problem problem) {
        this(problem.newPhenotype(), problem.newQuality());
    }
    public Solution(Phenotype phenotype, Quality quality) {
        this.phenotype = phenotype;
        this.quality = quality;
    }

    /*
    Accessors
     */

    public Quality getQuality() {
        return quality;
    }

    /*
    Commands
     */

    public void randomizeUsing(SolutionRandomizer randomizer) {
        PhenotypeRandomizer phenotypeRandomizer = (PhenotypeRandomizer) randomizer;
        randomizePhenotypeUsing(phenotypeRandomizer);
    }

    public void randomizePhenotypeUsing(PhenotypeRandomizer phenotypeRandomizer) {
        phenotypeRandomizer.randomizePhenotype(phenotype);
    }

    public void mutateUsing(MutationOperator mutationOperator) {
        PhenotypeMutationOperator phenotypeMutationOperator = (PhenotypeMutationOperator) mutationOperator;
        mutatePhenotypeUsing(phenotypeMutationOperator);
    }

    public void mutatePhenotypeUsing(PhenotypeMutationOperator mutationOperator) {
        mutationOperator.mutatePhenotype(phenotype);
    }

    public void evaluateUsing(EvaluationFunction evaluationFunction) {
        quality = evaluationFunction.evaluatePhenotype(phenotype);
    }

    /**
     * Propose this Solution as new good solution in the given Pareto Front.
     * @param bestQualities Pareto Front collection of best qualities known.
     * @return Result of the comparison of the new item with previous items of the pareto front.
     */
    public ParetoFront.NewItemResult proposeNewBestIn(ParetoFront<Quality> bestQualities) {
        // propose this solution's quality to the pareto front and get the result
        ParetoFront.NewItemResult submissionResult = bestQualities.submit(quality);

        // if the new quality has been stored in the pareto front, we need to
        // use a new copy so that we don't modify the one kept in the pareto front
        if (submissionResult == ParetoFront.NewItemResult.DOMINANT
                || submissionResult == ParetoFront.NewItemResult.EQUIVALENT_AND_KEPT) {
            quality = quality.clone();
        }

        return submissionResult;
    }

    /*
    Comparable
     */

    /**
     * Compare this Solution to another Solution or to a given Quality.
     * @param o The other Solution or Quality to compare this Solution to.
     * @return Result of the comparison of the two Qualities (if positive, this is better).
     */
    @Override
    public int compareTo(Object o) {
        if (o instanceof Solution) {
            Solution other = (Solution) o;
            return this.quality.compareTo(other.quality);
        } else {
            Quality other = (Quality) o;
            return this.quality.compareTo(other);
        }
    }

    public boolean isBetterThan(Solution other) {
        return this.quality.isBetterThan(other.quality);
    }

    public boolean isBetterThan(Quality quality) {
        return this.quality.isBetterThan(quality);
    }

    public boolean isWorseThan(Solution other) {
        return this.quality.isWorseThan(other.quality);
    }

    public boolean isWorseThan(Quality quality) {
        return this.quality.isWorseThan(quality);
    }

    public boolean isEquivalentOrBetterThan(Solution other) {
        return this.quality.isEquivalentOrBetterThan(other.quality);
    }

    public boolean isEquivalentOrBetterThan(Quality quality) {
        return this.quality.isEquivalentOrBetterThan(quality);
    }

    /*
    Cloneable
     */

    @Override
    public Solution clone() {
        Solution copy = new Solution();

        copy.phenotype = ((CloneablePhenotype) phenotype).clone();
        copy.quality = quality.clone();

        return copy;
    }

    /*
    toString
     */

    @Override
    public String toString() {
        return phenotypeToString() + "\nQuality: " + qualityToString();
    }

    public String phenotypeToString() {
        return phenotype.toString();
    }

    public String qualityToString() {
        return quality.toString();
    }
}
