package com.vanolucas.opti.solution;

import com.vanolucas.opti.quality.QualityFactory;

public interface SolutionFactory extends PhenotypeFactory, QualityFactory {

    Solution newSolution();
}
