package com.vanolucas.opti.test;

import com.vanolucas.opti.algo.OptiAlgo;
import com.vanolucas.opti.algo.random.RandomSearch;
import com.vanolucas.opti.math.ParetoFront;
import com.vanolucas.opti.problem.sample.TargetNumberProblem;
import com.vanolucas.opti.quality.LongCost;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.solution.Solution;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TargetNumberProblemTest {

    @Test
    void randomSearch() {
        TargetNumberProblem problem = new TargetNumberProblem(100);

        RandomSearch algo = problem.newRandomSearch();

        algo.pauseOnReachingQualityBetterOrEquivalentTo(new LongCost(99999));

        algo.addListener(new OptiAlgo.Listener() {
            @Override
            public void onIterationStart(long iteration, ParetoFront<Quality> bestQualities) {
//                System.out.println("iteration start: " + iteration);
            }

            @Override
            public void onIterationEnd(long iteration, ParetoFront<Quality> bestQualities) {
//                System.out.println("iteration end: " + iteration);
//                System.out.println("Pareto front: " + bestQualities.toString());
//                System.out.println();
            }

            @Override
            public void onNewBetterSolutionFound(Solution newBetterSolution, long iteration) {
//                System.out.println("New better: " + newBetterSolution.phenotypeToString() + "\tCost: " + newBetterSolution.qualityToString());
            }

            @Override
            public void onNewEquivalentSolutionFound(Solution equivalentSolution, long iteration) {
//                System.out.println("New equivalent: " + equivalentSolution.phenotypeToString() + "\tCost: " + equivalentSolution.qualityToString());
            }

            @Override
            public void onPause(long iteration, ParetoFront<Quality> bestQualities) {
//                System.out.println("Paused at iteration: " + iteration);
//                System.out.println("Pareto front: " + bestQualities.toString());
            }
        });

        algo.run();

        LongCost best = (LongCost) algo.getLastBetterSolutionFoundQuality();

//        System.out.println(best);
        assertEquals(0d, best.doubleValue(), 99999d);
    }

}