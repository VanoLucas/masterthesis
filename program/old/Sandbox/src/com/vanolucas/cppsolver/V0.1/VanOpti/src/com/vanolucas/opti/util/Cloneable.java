package com.vanolucas.opti.util;

public interface Cloneable extends java.lang.Cloneable {

    Cloneable clone();
}
