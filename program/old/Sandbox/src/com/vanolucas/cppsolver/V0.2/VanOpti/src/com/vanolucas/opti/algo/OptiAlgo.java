package com.vanolucas.opti.algo;

import com.vanolucas.opti.math.ParetoFront;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.solution.Solution;
import com.vanolucas.opti.util.Observable;

import java.util.ArrayList;
import java.util.List;

public abstract class OptiAlgo extends Observable<OptiAlgo.Listener> {

    /*
    Default params
     */

    private static final int DEFAULT_MAX_PARETO_FRONT_SIZE = 5;

    /*
    Listener
     */

    /**
     * Listener interface to create objects that react to this Optimization Algorithm's events.
     */
    public interface Listener {
        void onIterationStart(long iteration, ParetoFront<Quality> bestQualities);

        void onIterationEnd(long iteration, ParetoFront<Quality> bestQualities);

        void onNewBetterSolutionFound(Solution newBetterSolution, long iteration);

        void onNewEquivalentSolutionFound(Solution equivalentSolution, long iteration);

        void onPause(long iteration, ParetoFront<Quality> bestQualities);
    }

    /*
    Attributes
     */

    /**
     * Current iteration number of this algorithm.
     */
    protected long iteration = 0l;
    /**
     * Pareto front of the best known solution qualities.
     */
    private ParetoFront<Quality> bestQualities;

    /**
     * Iteration numbers at the end of which this optimization algorithm should pause.
     */
    private List<Long> pauseAfterIterations = new ArrayList<>(1);
    /**
     * When we find a solution equivalent or better than one of these Quality values,
     * this optimization algorithm should pause.
     */
    private List<Quality> pauseAtTargetQualities = new ArrayList<>(1);
    /**
     * When we find a solution strictly better than one of these Quality values,
     * this optimization algorithm should pause.
     */
    private List<Quality> pauseAtQualitiesBetterThan = new ArrayList<>(1);

    /*
    Constructors
     */

    public OptiAlgo() {
        this(DEFAULT_MAX_PARETO_FRONT_SIZE);
    }

    public OptiAlgo(int maxParetoFrontSize) {
        bestQualities = new ParetoFront<>(maxParetoFrontSize);
    }

    /*
    Accessors
     */

    public long getIteration() {
        return iteration;
    }

    public Quality getLastBetterSolutionFoundQuality() {
        return bestQualities.getLastBetterItemAdded();
    }

    /*
    Commands
     */

    public void run() {
        // run this optimization algorithm until we reach a pause criterion
        while (!shouldPause()) {
            // start next iteration
            iteration++;

            // notify listeners that we start a new iteration
            notify(listener -> listener.onIterationStart(iteration, bestQualities));

            // execute one iteration of this optimization algorithm
            runIteration();

            // notify listeners that we finished the current iteration
            notify(listener -> listener.onIterationEnd(iteration, bestQualities));
        }

        // we get here when run gets on pause
        onPause();
    }

    public void runUntilIteration(long iteration) {
        pauseAfterIteration(iteration);
        run();
    }

    public void runUntilQuality(Quality targetQuality) {
        pauseOnReachingQualityBetterOrEquivalentTo(targetQuality);
        run();
    }

    /*
    Pause Commands
     */

    /**
     * Request this optimization algorithm to pause at the end of the specified iteration number.
     *
     * @param iteration Iteration number at the end of which this optimization algorithm should pause.
     */
    public void pauseAfterIteration(long iteration) {
        if (!pauseAfterIterations.contains(iteration)) {
            pauseAfterIterations.add(iteration);
        }
    }

    public void pauseOnReachingQualityBetterOrEquivalentTo(Quality targetQuality) {
        // add this target quality only if we don't already have another target that is equivalent
        if (!pauseAtTargetQualities.stream()
                .anyMatch(q -> q.isEquivalentTo(targetQuality))) {
            pauseAtTargetQualities.add(targetQuality);
        }
    }

    public void pauseOnReachingQualityBetterThan(Quality qualityToSurpass) {
        pauseAtQualitiesBetterThan.add(qualityToSurpass);
    }

    /*
    Helpers
     */

    /**
     * Override this to implement an iteration of an optimization algorithm.
     */
    protected abstract void runIteration();

    /**
     * Submit a candidate Solution to be compared against the current best known solution Qualities.
     * Best known qualities are stored in a Pareto Front collection.
     *
     * @param solution Candidate Solution to compare against best known qualities to date.
     * @return Result of the insertion of the new solution in the Pareto Front of best qualities.
     */
    protected ParetoFront.NewItemResult proposeNewSolution(Solution solution) {
        ParetoFront.NewItemResult result = solution.proposeNewBestIn(bestQualities);

        // notify listeners if the new solution we found is good
        switch (result) {
            // notify listeners that we found a new better solution
            case DOMINANT:
                notify(listener -> listener.onNewBetterSolutionFound(solution, iteration));
                break;
            // notify listeners that we found a new equivalent solution
            case EQUIVALENT_AND_KEPT:
            case EQUIVALENT_BUT_NOT_KEPT:
                notify(listener -> listener.onNewEquivalentSolutionFound(solution, iteration));
                break;
        }

        return result;
    }

    /**
     * Check if this optimization algorithm should pause before next iteration.
     *
     * @return True is this optimization algorithm should pause running before next iteration.
     */
    private boolean shouldPause() {
        // check if we should pause at this iteration number
        if (pauseAfterIterations.contains(iteration)) {
            return true;
        }

        // check if we should pause because we reached a target quality
        // i.e. if any of the best known qualities if equivalent or better than a target
        if (bestQualities.stream()
                .anyMatch(bestQuality -> pauseAtTargetQualities.stream()
                        .anyMatch(targetQuality -> bestQuality.isEquivalentOrBetterThan(targetQuality)))) {
            return true;
        }

        // check if we should pause because we surpassed a target quality
        // i.e. if any of the best known qualities is better than a target
        if (bestQualities.stream()
                .anyMatch(bestQuality -> pauseAtQualitiesBetterThan.stream()
                        .anyMatch(targetQuality -> bestQuality.isBetterThan(targetQuality)))) {
            return true;
        }

        // we reached no pause criterion yet, keep going
        return false;
    }

    /**
     * When a run of this optimization algorithm starts to pause,
     * perform actions that have to be done when pausing.
     */
    private void onPause() {
        // remove request to pause at this iteration number if any
        pauseAfterIterations.remove(iteration);

        // remove request to pause at target quality if we reached it
        pauseAtTargetQualities.removeIf(targetQuality -> bestQualities.stream()
                .anyMatch(bestQuality -> bestQuality.isEquivalentOrBetterThan(targetQuality))
        );

        // remove request to pause at quality better than target if we reached it
        pauseAtQualitiesBetterThan.removeIf(targetQuality -> bestQualities.stream()
                .anyMatch(bestQuality -> bestQuality.isBetterThan(targetQuality))
        );

        // notify listeners that we paused
        notify(listener -> listener.onPause(iteration, bestQualities));
    }
}
