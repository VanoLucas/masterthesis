package com.vanolucas.opti.algo.randomsearch;

import com.vanolucas.opti.algo.OptiAlgo;
import com.vanolucas.opti.solution.Solution;
import com.vanolucas.opti.solution.SolutionFactory;

public class RandomSearch extends OptiAlgo {

    /*
    Attributes
     */

    private Solution currentSolution;
    private SolutionFactory solutionFactory;

    /*
    Constructor
     */

    public RandomSearch(SolutionFactory solutionFactory) {
        this.solutionFactory = solutionFactory;
    }

    /*
    Accessors
     */

    public void setCurrentSolution(Solution currentSolution) {
        this.currentSolution = currentSolution;
    }

    public void setSolutionFactory(SolutionFactory solutionFactory) {
        this.solutionFactory = solutionFactory;
    }

    /*
    Helpers
     */

    @Override
    protected void runIteration() {
        initIfNeeded();

        currentSolution.randomize();
        currentSolution.evaluate();

        proposeNewSolution(currentSolution);
    }

    private void initIfNeeded() {
        if (currentSolution == null) {
            currentSolution = solutionFactory.newSolution();
        }
    }
}
