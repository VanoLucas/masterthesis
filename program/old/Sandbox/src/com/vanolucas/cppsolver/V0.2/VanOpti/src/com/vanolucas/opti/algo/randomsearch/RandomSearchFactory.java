package com.vanolucas.opti.algo.randomsearch;

public interface RandomSearchFactory {

    RandomSearch newRandomSearch();
}
