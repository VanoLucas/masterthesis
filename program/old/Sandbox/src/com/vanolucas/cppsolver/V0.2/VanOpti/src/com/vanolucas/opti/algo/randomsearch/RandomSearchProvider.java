package com.vanolucas.opti.algo.randomsearch;

import com.vanolucas.opti.phenotype.Optimizable;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.solution.phenotype.AOptimizableSolutionFactory;
import com.vanolucas.opti.util.DeepCloneable;

public abstract class RandomSearchProvider<TPhenotype, TQuality extends Quality> implements AOptimizableSolutionFactory, RandomSearchFactory {

    class OurPhenotype implements Optimizable<TQuality> {

        private TPhenotype phenotype;
        private RandomSearchProvider<TPhenotype, TQuality> provider;

        public OurPhenotype(RandomSearchProvider<TPhenotype, TQuality> provider) {
            this(provider, provider.defaultPhenotype());
        }
        public OurPhenotype(RandomSearchProvider<TPhenotype, TQuality> provider, TPhenotype phenotype) {
            this.provider = provider;
            this.phenotype = phenotype;
        }

        @Override
        public TQuality evaluate() {
            return provider.evaluate(phenotype);
        }

        @Override
        public DeepCloneable clone() {
            return new OurPhenotype(provider, provider.clone(phenotype));
        }

        @Override
        public void copyFrom(Object other) {
            provider.copy(((OurPhenotype) other).phenotype, this.phenotype);
        }

        @Override
        public void randomize() {
            provider.randomize(phenotype);
        }

        @Override
        public String toString() {
            return provider.toString(phenotype);
        }
    }

    @Override
    public OurPhenotype newPhenotype() {
        return new OurPhenotype(this);
    }

    @Override
    public TQuality newQuality() {
        return defaultQuality();
    }

    public abstract TPhenotype defaultPhenotype();
    public abstract TQuality defaultQuality();

    public abstract void copy(TPhenotype src, TPhenotype dst);
    public abstract TPhenotype clone(TPhenotype phenotype);
    public abstract void randomize(TPhenotype phenotype);
    public abstract TQuality evaluate(TPhenotype phenotype);

    public String toString(TPhenotype phenotype) {
        return phenotype.toString();
    }

    @Override
    public RandomSearch newRandomSearch() {
        return new RandomSearch(this);
    }
}
