package com.vanolucas.opti.eval;

import com.vanolucas.opti.quality.Quality;

public interface AEvaluable {

    Quality evaluate();
}
