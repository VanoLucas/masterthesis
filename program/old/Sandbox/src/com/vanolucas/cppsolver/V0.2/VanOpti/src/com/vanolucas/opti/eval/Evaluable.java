package com.vanolucas.opti.eval;

import com.vanolucas.opti.quality.Quality;

public interface Evaluable<TQuality extends Quality> extends AEvaluable {

    @Override
    TQuality evaluate();
}
