package com.vanolucas.opti.genome;

public interface ADecodableFromGenome {

    void decodeFromAGenome(Genome genome);
}
