package com.vanolucas.opti.genome;

public interface DecodableFromGenome<TGenome extends Genome> extends ADecodableFromGenome {

    @Override
    default void decodeFromAGenome(Genome genome) {
        decodeFromGenome((TGenome) genome);
    }

    void decodeFromGenome(TGenome genome);
}
