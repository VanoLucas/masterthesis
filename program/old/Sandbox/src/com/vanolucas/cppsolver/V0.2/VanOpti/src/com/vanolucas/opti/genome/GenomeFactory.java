package com.vanolucas.opti.genome;

public interface GenomeFactory {

    Genome newGenome();
}
