package com.vanolucas.opti.phenotype;

public interface APhenotypeFactory {

    APhenotype newPhenotype();
}
