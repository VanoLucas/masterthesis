package com.vanolucas.opti.phenotype;

import com.vanolucas.opti.genome.DecodableFromGenome;
import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.phenotype.Phenotype;

public interface GeneticallyOptimizable<TGenome extends Genome, TQuality extends Quality>
        extends Phenotype<TQuality>, DecodableFromGenome<TGenome> {
}
