package com.vanolucas.opti.phenotype;

import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.phenotype.Phenotype;
import com.vanolucas.opti.util.DeepCloneable;
import com.vanolucas.opti.util.Randomizable;

public interface Optimizable<TQuality extends Quality> extends Phenotype<TQuality>, DeepCloneable, Randomizable {
}
