package com.vanolucas.opti.phenotype;

import com.vanolucas.opti.eval.Evaluable;
import com.vanolucas.opti.quality.Quality;

public interface Phenotype<TQuality extends Quality> extends APhenotype, Evaluable<TQuality> {
}
