package com.vanolucas.opti.quality;

public abstract class SingleValueQuality<T> extends Quality {
    protected T value;

    /*
    Constructors
     */

    public SingleValueQuality(T value) {
        set(value);
    }

    /*
    Accessors
     */

    public T get() {
        return value;
    }

    public SingleValueQuality<T> set(T value) {
        this.value = value;
        return this;
    }

    /*
    Comparison
     */

    @Override
    public boolean isEquivalentTo(Quality o) {
        SingleValueQuality<T> other = (SingleValueQuality<T>) o;
        return this.value.equals(other.value);
    }

    /*
    toString
     */

    @Override
    public String toString() {
        return value.toString();
    }
}

