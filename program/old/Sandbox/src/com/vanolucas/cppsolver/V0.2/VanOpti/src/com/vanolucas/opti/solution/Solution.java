package com.vanolucas.opti.solution;

import com.vanolucas.opti.eval.AEvaluable;
import com.vanolucas.opti.math.ParetoFront;
import com.vanolucas.opti.phenotype.APhenotype;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.util.Randomizable;

/**
 * Abstracts a solution of an optimization algorithm.
 * Convenient class to store a solution we can work with in an optimization algorithm.
 */
public abstract class Solution implements Randomizable, AEvaluable {

    /*
    Attributes
     */

    protected APhenotype phenotype;
    private Quality quality;

    /*
    Constructors
     */

    public Solution(APhenotype phenotype) {
        this(phenotype, null);
    }
    public Solution(APhenotype phenotype, Quality quality) {
        this.phenotype = phenotype;
        this.quality = quality;
    }

    /*
    Accessors
     */

    public void setPhenotype(APhenotype phenotype) {
        this.phenotype = phenotype;
    }

    public void setQuality(Quality quality) {
        this.quality = quality;
    }

    /*
    Commands
     */

    /**
     * Propose this Solution as new good solution in the given Pareto Front.
     * @param bestQualities Pareto Front collection of best qualities known.
     * @return Result of the comparison of the new item with previous items of the pareto front.
     */
    public ParetoFront.NewItemResult proposeNewBestIn(ParetoFront<Quality> bestQualities) {
        // propose this solution's quality to the pareto front and get the result
        ParetoFront.NewItemResult submissionResult = bestQualities.submit(quality);

        // if the new quality has been stored in the pareto front, we need to
        // use a new copy so that we don't modify the one kept in the pareto front
        if (submissionResult == ParetoFront.NewItemResult.DOMINANT
                || submissionResult == ParetoFront.NewItemResult.EQUIVALENT_AND_KEPT) {
            quality = quality.clone();
        }

        return submissionResult;
    }

    /*
    Evaluable
     */

    /**
     * Evaluate this solution.
     * Call the evaluation function of the phenotype of this solution.
     */
    @Override
    public Quality evaluate() {
        quality = phenotype.evaluate();
        return quality;
    }

    /*
    Randomizable
     */

    /**
     * Override and call this in subclasses to implement randomizing a solution.
     */
    @Override
    public void randomize() {
        quality = null;
    }

    /*
    toString
     */

    @Override
    public String toString() {
        return "Phenotype:\n" + phenotype.toString() + "\nQuality: " + quality.toString();
    }
}
