package com.vanolucas.opti.solution;

import com.vanolucas.opti.phenotype.APhenotypeFactory;
import com.vanolucas.opti.quality.QualityFactory;

public interface SolutionFactory extends APhenotypeFactory, QualityFactory {

    Solution newSolution();
}
