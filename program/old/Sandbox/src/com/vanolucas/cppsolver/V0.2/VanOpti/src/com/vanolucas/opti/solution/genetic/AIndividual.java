package com.vanolucas.opti.solution.genetic;

import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.phenotype.GeneticallyOptimizable;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.solution.Solution;

/**
 * Abstracts a solution of a genetic optimization algorithm.
 * Convenient class to store a solution we can work with in a genetic optimization algorithm.
 */
public class AIndividual extends Solution {

    private Genome genome;

    /*
    Constructors
     */

    // todo see if better to take GeneticallyOptimizable or APhenotype here
    public AIndividual(Genome genome, GeneticallyOptimizable phenotype) {
        this(genome, phenotype, null);
    }

    public AIndividual(Genome genome, GeneticallyOptimizable phenotype, Quality quality) {
        super(phenotype, quality);
        this.genome = genome;
    }

    /*
    Randomizable
     */

    @Override
    public void randomize() {
        super.randomize();
        genome.randomize();
    }

    /*
    Evaluable
     */

    @Override
    public Quality evaluate() {
        ((GeneticallyOptimizable) phenotype).decodeFromAGenome(genome);
        return super.evaluate();
    }

    /*
    toString
     */

    @Override
    public String toString() {
        return "Genome:\n" + genome.toString() + "\n" + super.toString();
    }
}
