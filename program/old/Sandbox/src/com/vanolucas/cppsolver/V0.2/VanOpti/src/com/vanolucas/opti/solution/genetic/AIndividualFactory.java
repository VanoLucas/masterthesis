package com.vanolucas.opti.solution.genetic;

import com.vanolucas.opti.genome.GenomeFactory;
import com.vanolucas.opti.phenotype.GeneticallyOptimizable;
import com.vanolucas.opti.solution.SolutionFactory;

public interface AIndividualFactory extends SolutionFactory, GenomeFactory {

    @Override
    GeneticallyOptimizable newPhenotype();

    @Override
    default AIndividual newSolution() {
        return newIndividual();
    }

    default AIndividual newIndividual() {
        return new AIndividual(newGenome(), newPhenotype(), newQuality());
    }
}
