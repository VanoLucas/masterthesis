package com.vanolucas.opti.solution.genetic;

import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.phenotype.GeneticallyOptimizable;
import com.vanolucas.opti.quality.Quality;

public class Individual
        <TGenome extends Genome,
                TPhenotype extends GeneticallyOptimizable<TGenome, TQuality>,
                TQuality extends Quality>
        extends AIndividual {

    /*
    Constructors
     */

    public Individual(TGenome genome, TPhenotype phenotype) {
        super(genome, phenotype);
    }

    public Individual(TGenome genome, TPhenotype phenotype, TQuality quality) {
        super(genome, phenotype, quality);
    }
}
