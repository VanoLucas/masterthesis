package com.vanolucas.opti.solution.phenotype;

import com.vanolucas.opti.phenotype.Optimizable;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.solution.Solution;

/**
 * Abstracts a solution of an optimization algorithm.
 * Convenient class to store a solution we can work with in an optimization algorithm.
 */
public class AOptimizableSolution extends Solution {

    /*
    Constructors
     */

    public AOptimizableSolution(Optimizable phenotype) {
        super(phenotype);
    }

    public AOptimizableSolution(Optimizable phenotype, Quality quality) {
        super(phenotype, quality);
    }

    /*
    Randomizable
     */

    @Override
    public void randomize() {
        super.randomize();
        ((Optimizable) this.phenotype).randomize();
    }
}
