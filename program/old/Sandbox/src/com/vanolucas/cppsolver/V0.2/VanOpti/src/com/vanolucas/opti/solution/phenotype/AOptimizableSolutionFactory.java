package com.vanolucas.opti.solution.phenotype;

import com.vanolucas.opti.phenotype.Optimizable;
import com.vanolucas.opti.solution.SolutionFactory;

public interface AOptimizableSolutionFactory extends SolutionFactory {

    @Override
    Optimizable newPhenotype();

    @Override
    default AOptimizableSolution newSolution() {
        return new AOptimizableSolution(newPhenotype(), newQuality());
    }
}
