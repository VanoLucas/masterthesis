package com.vanolucas.opti.solution.phenotype;

import com.vanolucas.opti.phenotype.Optimizable;
import com.vanolucas.opti.quality.Quality;

public class OptimizableSolution
        <TPhenotype extends Optimizable<TQuality>,
                TQuality extends Quality>
        extends AOptimizableSolution {

    /*
    Constructors
     */

    public OptimizableSolution(TPhenotype phenotype) {
        super(phenotype);
    }

    public OptimizableSolution(TPhenotype phenotype, TQuality quality) {
        super(phenotype, quality);
    }
}
