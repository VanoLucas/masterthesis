package com.vanolucas.opti.util;

public interface DeepCloneable extends Copyable, Cloneable {

    DeepCloneable clone();
}
