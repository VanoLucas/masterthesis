package com.vanolucas.opti.util;

public interface Randomizable {

    void randomize();
}
