package com.vanolucas.opti.algo.randomsearch;

import com.vanolucas.opti.algo.OptiAlgo;
import com.vanolucas.opti.math.ParetoFront;
import com.vanolucas.opti.phenotype.APhenotype;
import com.vanolucas.opti.phenotype.Optimizable;
import com.vanolucas.opti.quality.LongCost;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.solution.Solution;
import com.vanolucas.opti.solution.SolutionFactory;
import com.vanolucas.opti.solution.phenotype.AOptimizableSolutionFactory;
import com.vanolucas.opti.util.DeepCloneable;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class RandomSearchTest {

    @Test
    void targetNumber() {

        class MyPhenotype {
            private int value;

            public MyPhenotype(int value) {
                this.value = value;
            }

            public int getValue() {
                return value;
            }

            public void setValue(int value) {
                this.value = value;
            }

            @Override
            public String toString() {
                return String.valueOf(value);
            }
        }

        RandomSearchProvider<MyPhenotype, LongCost> provider = new RandomSearchProvider<MyPhenotype, LongCost>() {
            @Override
            public MyPhenotype defaultPhenotype() {
                return new MyPhenotype(1111);
            }

            @Override
            public LongCost defaultQuality() {
                return new LongCost();
            }

            @Override
            public void copy(MyPhenotype src, MyPhenotype dst) {
                dst.setValue(src.getValue());
            }

            @Override
            public MyPhenotype clone(MyPhenotype phenotype) {
                return new MyPhenotype(phenotype.getValue());
            }

            @Override
            public void randomize(MyPhenotype phenotype) {
                phenotype.setValue(new Random().nextInt(10000));
            }

            @Override
            public LongCost evaluate(MyPhenotype phenotype) {
                final int TARGET = 3333;
                long cost = Math.abs(phenotype.getValue() - TARGET);
                return new LongCost(cost);
            }
        };

        RandomSearch randomSearch = provider.newRandomSearch();

        randomSearch.addListener(new OptiAlgo.Listener() {
            @Override
            public void onIterationStart(long iteration, ParetoFront<Quality> bestQualities) {

            }

            @Override
            public void onIterationEnd(long iteration, ParetoFront<Quality> bestQualities) {

            }

            @Override
            public void onNewBetterSolutionFound(Solution newBetterSolution, long iteration) {
//                System.out.println("\nIteration: " + iteration);
//                System.out.println(newBetterSolution);
            }

            @Override
            public void onNewEquivalentSolutionFound(Solution equivalentSolution, long iteration) {

            }

            @Override
            public void onPause(long iteration, ParetoFront<Quality> bestQualities) {

            }
        });

        randomSearch.runUntilQuality(new LongCost(0));

        assertEquals(new LongCost(0), randomSearch.getLastBetterSolutionFoundQuality());
    }

}