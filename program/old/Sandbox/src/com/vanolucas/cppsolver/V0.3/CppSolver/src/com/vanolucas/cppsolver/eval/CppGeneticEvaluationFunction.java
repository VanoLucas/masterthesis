package com.vanolucas.cppsolver.eval;

import com.vanolucas.cppsolver.genome.CppGenome;
import com.vanolucas.cppsolver.solution.CppSolution;
import com.vanolucas.opti.eval.GeneticEvaluationFunction;
import com.vanolucas.opti.quality.DoubleCost;

public interface CppGeneticEvaluationFunction extends GeneticEvaluationFunction<CppGenome, CppSolution, DoubleCost> {

    @Override
    default DoubleCost evaluatePhenotype(CppSolution cppSolution) {
        return new DoubleCost(cppSolution.getTotalCost());
    }
}
