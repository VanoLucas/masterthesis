package com.vanolucas.cppsolver.eval;

import com.vanolucas.cppsolver.genome.CppGenome;
import com.vanolucas.cppsolver.genome.NSDoubleGenome;
import com.vanolucas.cppsolver.problem.ICoverPrintingProblemInstance;
import com.vanolucas.cppsolver.solution.BookCoverDemand;
import com.vanolucas.cppsolver.solution.BookCoverDemandPart;
import com.vanolucas.cppsolver.solution.CppSolution;
import com.vanolucas.opti.math.CutInteger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class NSDoubleEvaluationFunction implements CppGeneticEvaluationFunction {

    /*
    Useful CPP instance params
     */

    private final List<BookCoverDemand> bookCovers;
    private final int NB_BOOK_COVERS;
    private final int TOTAL_NB_SLOTS;

    /*
    Attributes
     */

    private final CutInteger nbSlotsSplitter;

    /*
    Constructor
     */

    public NSDoubleEvaluationFunction(ICoverPrintingProblemInstance cppInstance) {
        this.bookCovers = cppInstance.getBookCoverDemands();
        this.NB_BOOK_COVERS = cppInstance.getNbBookCovers();
        this.TOTAL_NB_SLOTS = cppInstance.getTotalNbSlots();

        // each book cover will be assigned at least 1 slot,
        // so the remaining number of slots to assign is all slots - nb book covers
        this.nbSlotsSplitter = new CutInteger(cppInstance.getTotalNbSlots() - cppInstance.getNbBookCovers());
    }

    /*
    GenomeDecoder
     */

    @Override
    public void decodeGenome(CppGenome genome, CppSolution cppSolution) {

        // cast to N-S CPP genome
        NSDoubleGenome g = (NSDoubleGenome) genome;

        /*
        Extract keys (weights) from the two parts of the N-S genome
         */

        List<Double> keysNbSlotsForEachBookCover;
        List<Double> keysSizeEachPart;

        // extract gene values
        keysNbSlotsForEachBookCover = g.getGenes(0, NB_BOOK_COVERS);
        keysSizeEachPart = g.getGenes(NB_BOOK_COVERS, g.size());

        /*
        Calculate the number of slots each book cover will be assigned to
         */

        // split the number of slots based on the keys from the genome
        List<Integer> nbSlotsForEachBookCover = nbSlotsSplitter.cutCake(keysNbSlotsForEachBookCover).stream()
                // add 1 since each book cover must be assigned to at least 1 slot
                .mapToInt(nbSlots -> nbSlots + 1)
                .boxed()
                .collect(Collectors.toList());

        /*
        Split each book cover demand over multiple slots
         */

        // list that will contain the nb of printings for each book allocated to each slot
        List<BookCoverDemandPart> bookCoverDemandParts = new ArrayList<>(TOTAL_NB_SLOTS);
        // current position in the genome of keys that define the nb of printings for each slot
        int cursorKeys = 0;

        // for each book cover, divide its demand over multiple slots
        for (int bookCoverIndex = 0; bookCoverIndex < NB_BOOK_COVERS; bookCoverIndex++) {
            // get the current book cover
            final BookCoverDemand bookCover = bookCovers.get(bookCoverIndex);

            // prepare splitter function object that will divide the demand for this book cover in multiple values
            CutInteger nbPrintingsSplitter = new CutInteger(bookCover.getDemand());

            // get nb of values we should divide this book cover's demand over
            final int NB_SLOTS_FOR_THIS_BOOK_COVER = nbSlotsForEachBookCover.get(bookCoverIndex);

            // get keys that define how to split the demand for this book cover from the genome
            List<Double> keysSizeEachPartForThisBookCover = keysSizeEachPart.subList(cursorKeys, cursorKeys + NB_SLOTS_FOR_THIS_BOOK_COVER);

            // convert keys to actual number of printings by splitting the demand for this book cover
            List<Integer> nbPrintingsEachSlot = nbPrintingsSplitter.cutCake(keysSizeEachPartForThisBookCover);

            // store the demand that got split over multiple slots for this book cover
            bookCoverDemandParts.addAll(
                    nbPrintingsEachSlot.stream()
                            .map(nbPrintings -> new BookCoverDemandPart(bookCover, nbPrintings))
                            .collect(Collectors.toList())
            );

            // advance genome cursor to keys for next book cover
            cursorKeys += NB_SLOTS_FOR_THIS_BOOK_COVER;
        }

        /*
        Sort demand slices by number of printings descending
         */

        // sort by number of printings so that we group slots with similar
        // required number of printings on the same offset plates
        Collections.sort(bookCoverDemandParts, Collections.reverseOrder());

        // assign book cover demand parts in slots of the solution's offset plates
        cppSolution.assignBookCovers(bookCoverDemandParts);
    }
}
