package com.vanolucas.cppsolver.eval;

import com.vanolucas.cppsolver.genome.CppGenome;
import com.vanolucas.cppsolver.genome.SSBDoubleGenome;
import com.vanolucas.cppsolver.problem.ICoverPrintingProblemInstance;
import com.vanolucas.cppsolver.solution.BookCoverDemand;
import com.vanolucas.cppsolver.solution.BookCoverDemandPart;
import com.vanolucas.cppsolver.solution.CppSolution;
import com.vanolucas.opti.math.CutInteger;
import com.vanolucas.opti.math.Scale;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class SSBDoubleEvaluationFunction implements CppGeneticEvaluationFunction {

    /*
    Useful CPP instance params
     */

    private final List<BookCoverDemand> bookCovers;
    private final int NB_BOOK_COVERS;
    private final int TOTAL_NB_SLOTS;

    /*
    Constructor
     */

    public SSBDoubleEvaluationFunction(ICoverPrintingProblemInstance cppInstance) {
        this.bookCovers = cppInstance.getBookCoverDemands();
        this.NB_BOOK_COVERS = cppInstance.getNbBookCovers();
        this.TOTAL_NB_SLOTS = cppInstance.getTotalNbSlots();
    }

    /*
    GenomeDecoder
     */

    @Override
    public void decodeGenome(CppGenome genome, CppSolution cppSolution) {

        // cast to S-SB CPP genome
        SSBDoubleGenome g = (SSBDoubleGenome) genome;

        /*
        Extract keys (weights) from the two parts of the S-SB genome
         */

        // for each book cover, the key that determines the size of the first demand slice
        List<Double> keysFirstSlice = g.getGenes(0, NB_BOOK_COVERS);
        // keys that determine the size of other slices and to which book cover it is attributed
        List<Double> keysOtherSlices = g.getGenes(NB_BOOK_COVERS, g.size());

        /*
        Construct list of keys that determine the size of demand slices for each book cover
         */

        // for each book cover, the keys that determine how to split its demand
        List<List<Double>> keysSplitSize = new ArrayList<>(NB_BOOK_COVERS);

        // add the first split to each book cover
        for (int bookCoverIndex = 0; bookCoverIndex < NB_BOOK_COVERS; bookCoverIndex++) {
            keysSplitSize.add(new ArrayList<>());
            keysSplitSize.get(bookCoverIndex).add(keysFirstSlice.get(bookCoverIndex));
        }

        // read all remaining genome keys to determine demand slices
        for (int cursor = 0; cursor < keysOtherSlices.size(); cursor += 2) {
            final double KEY_SLICE_SIZE = keysOtherSlices.get(cursor);
            final double KEY_BOOK_COVER = keysOtherSlices.get(cursor + 1);

            final int BOOK_COVER_INDEX = (int) Scale.scale(KEY_BOOK_COVER, 1.0d, NB_BOOK_COVERS);

            keysSplitSize.get(BOOK_COVER_INDEX).add(KEY_SLICE_SIZE);
        }

        /*
        Construct the list of nb of printings for each book cover-slot
         */

        // list that will contain the nb of printings for each book allocated to each slot
        List<BookCoverDemandPart> bookCoverDemandParts = new ArrayList<>(TOTAL_NB_SLOTS);

        // cut integer algorithm to split the demand of each book cover
        CutInteger demandSplitter = new CutInteger();

        // for each book cover, split its demand according to keys
        for (int bookCoverIndex = 0; bookCoverIndex < NB_BOOK_COVERS; bookCoverIndex++) {
            // get demand for this book cover
            final int DEMAND = bookCovers.get(bookCoverIndex).getDemand();
            // get keys that determine the size of each demand slice for this book cover
            final List<Double> keys = keysSplitSize.get(bookCoverIndex);

            // calculate each demand slice size based on the keys
            final List<Integer> nbPrintings = demandSplitter.cutCake(DEMAND, keys);

            // store the demand slices for this book cover
            BookCoverDemand bookCover = bookCovers.get(bookCoverIndex);
            bookCoverDemandParts.addAll(
                    nbPrintings.stream()
                            .map(printings -> new BookCoverDemandPart(bookCover, printings))
                            .collect(Collectors.toList())
            );
        }

        /*
        Sort demand slices by number of printings descending
         */

        // sort by number of printings so that we group slots with similar
        // required number of printings on the same offset plates
        Collections.sort(bookCoverDemandParts, Collections.reverseOrder());

        // assign book cover demand parts in slots of the solution's offset plates
        cppSolution.assignBookCovers(bookCoverDemandParts);
    }
}
