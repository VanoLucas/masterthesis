package com.vanolucas.cppsolver.genome;

import com.vanolucas.opti.genome.Double01Genome;

public class CppDoubleGenome extends Double01Genome implements CppGenome {

    public CppDoubleGenome(int size) {
        super(size);
    }
}
