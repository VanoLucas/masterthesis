package com.vanolucas.cppsolver.genome;

import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.random.Randomizable;

public interface CppGenome extends Genome, Randomizable {
}
