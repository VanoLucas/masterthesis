package com.vanolucas.cppsolver.genome;

public class NSDoubleGenome extends CppDoubleGenome {

    public NSDoubleGenome(final int nbBookCovers, final int totalNbSlots) {
        // calculate size of the genome based on CPP params
        super(nbBookCovers + totalNbSlots);
    }
}
