package com.vanolucas.cppsolver.genome;

public class PSDoubleGenome extends CppDoubleGenome {

    public PSDoubleGenome(final int nbBookCovers, final int totalNbSlots) {
        // calculate size of the genome based on CPP params
        super((totalNbSlots - nbBookCovers) * 2);
    }
}
