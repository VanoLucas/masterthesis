package com.vanolucas.cppsolver.genome;

public class SSBDoubleGenome extends CppDoubleGenome {

    public SSBDoubleGenome(final int nbBookCovers, final int totalNbSlots) {
        // calculate size of the genome based on CPP params
        super(nbBookCovers + 2 * (totalNbSlots - nbBookCovers));
    }
}
