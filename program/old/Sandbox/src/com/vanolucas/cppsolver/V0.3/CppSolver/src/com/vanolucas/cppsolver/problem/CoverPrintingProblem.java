package com.vanolucas.cppsolver.problem;

import com.vanolucas.cppsolver.eval.CppGeneticEvaluationFunction;
import com.vanolucas.cppsolver.eval.NSDoubleEvaluationFunction;
import com.vanolucas.cppsolver.eval.PSDoubleEvaluationFunction;
import com.vanolucas.cppsolver.eval.SSBDoubleEvaluationFunction;
import com.vanolucas.cppsolver.genome.CppGenome;
import com.vanolucas.cppsolver.genome.NSDoubleGenome;
import com.vanolucas.cppsolver.genome.PSDoubleGenome;
import com.vanolucas.cppsolver.genome.SSBDoubleGenome;
import com.vanolucas.cppsolver.solution.BookCoverDemand;
import com.vanolucas.cppsolver.solution.CppSolution;
import com.vanolucas.opti.algo.localsearch.GeneticLocalSearchFactory;
import com.vanolucas.opti.mutation.AGenomeMutation;
import com.vanolucas.opti.quality.DoubleCost;
import com.vanolucas.opti.random.genome.GenomeRandomizer;

import java.util.List;

/**
 * Stores an instance of the General Cover Printing Problem.
 */
public abstract class CoverPrintingProblem
        implements ICoverPrintingProblemInstance,
        GeneticLocalSearchFactory<CppGenome, CppSolution, DoubleCost> {

    /*
    Defaults
     */

    private static final int DEFAULT_NB_SLOTS_PER_OFFSET_PLATE = 4;
    private static final double DEFAULT_COST_OFFSET_PLATE = 0d;
    private static final double DEFAULT_COST_ONE_PRINTING = 1d;

    /*
    Attributes
     */

    private final List<BookCoverDemand> bookCoverDemands;
    private final int NB_OFFSET_PLATES;
    private final int NB_SLOTS_PER_OFFSET_PLATE;
    private final double COST_OFFSET_PLATE;
    private final double COST_ONE_PRINTING;

    private final GenomeType genomeType;

    /*
    Enumerations
     */

    /**
     * Possible types of genome we can use with the Cover Printing Problem.
     */
    public enum GenomeType {
        NS_DOUBLE, SSB_DOUBLE, PS_DOUBLE
    }

    /*
    Constructors
     */

    public CoverPrintingProblem(List<BookCoverDemand> bookCoverDemands, int nbOffsetPlates, GenomeType genomeType) {
        this(bookCoverDemands, nbOffsetPlates, genomeType, DEFAULT_NB_SLOTS_PER_OFFSET_PLATE);
    }

    public CoverPrintingProblem(List<BookCoverDemand> bookCoverDemands, int nbOffsetPlates, GenomeType genomeType, int nbSlotsPerOffsetPlate) {
        this(bookCoverDemands, nbOffsetPlates, genomeType, nbSlotsPerOffsetPlate,
                DEFAULT_COST_OFFSET_PLATE, DEFAULT_COST_ONE_PRINTING);
    }

    public CoverPrintingProblem(List<BookCoverDemand> bookCoverDemands, int nbOffsetPlates, GenomeType genomeType, int nbSlotsPerOffsetPlate,
                                double costOffsetPlate, double costOnePrinting) {
        this.bookCoverDemands = bookCoverDemands;
        this.NB_OFFSET_PLATES = nbOffsetPlates;
        this.NB_SLOTS_PER_OFFSET_PLATE = nbSlotsPerOffsetPlate;
        this.COST_OFFSET_PLATE = costOffsetPlate;
        this.COST_ONE_PRINTING = costOnePrinting;

        this.genomeType = genomeType;
    }

    /*
    Accessors to General Cover Printing Problem input params
     */

    @Override
    public List<BookCoverDemand> getBookCoverDemands() {
        return bookCoverDemands;
    }

    @Override
    public int getNbOffsetPlates() {
        return NB_OFFSET_PLATES;
    }

    @Override
    public int getNbSlotsPerOffsetPlate() {
        return NB_SLOTS_PER_OFFSET_PLATE;
    }

    @Override
    public double getCostOffsetPlate() {
        return COST_OFFSET_PLATE;
    }

    @Override
    public double getCostOnePrinting() {
        return COST_ONE_PRINTING;
    }

    /*
    Genetic optimization algorithms factories.
     */

    @Override
    public CppGenome newGenome() {
        switch (genomeType) {
            case NS_DOUBLE:
                return new NSDoubleGenome(getNbBookCovers(), getTotalNbSlots());
            case SSB_DOUBLE:
                return new SSBDoubleGenome(getNbBookCovers(), getTotalNbSlots());
            case PS_DOUBLE:
                return new PSDoubleGenome(getNbBookCovers(), getTotalNbSlots());
            default:
                return null;
        }
    }

    @Override
    public CppSolution newPhenotype() {
        return new CppSolution(this);
    }

    @Override
    public DoubleCost newQuality() {
        return new DoubleCost();
    }

    @Override
    public GenomeRandomizer<CppGenome> newGenomeRandomizer() {
        return genome -> genome.randomize();
    }

    @Override
    public CppGeneticEvaluationFunction newGeneticEvaluationFunction() {
        switch (genomeType) {
            case NS_DOUBLE:
                return new NSDoubleEvaluationFunction(this);
            case SSB_DOUBLE:
                return new SSBDoubleEvaluationFunction(this);
            case PS_DOUBLE:
                return new PSDoubleEvaluationFunction(this);
            default:
                return null;
        }
    }

    @Override
    public abstract List<AGenomeMutation> newGeneticMutationOperators();
}
