package com.vanolucas.cppsolver.problem;

import com.vanolucas.cppsolver.solution.BookCoverDemand;

import java.util.List;

/**
 * Ability to provide all necessary input params for the General Cover Printing Problem.
 */
public interface ICoverPrintingProblemInstance {

    List<BookCoverDemand> getBookCoverDemands();

    default int getNbBookCovers() {
        return getBookCoverDemands().size();
    }

    int getNbOffsetPlates();
    int getNbSlotsPerOffsetPlate();

    double getCostOffsetPlate();
    double getCostOnePrinting();

    default int getTotalNbSlots() {
        return getNbOffsetPlates() * getNbSlotsPerOffsetPlate();
    }

    default int getTotalDemands() {
        return getBookCoverDemands().stream()
                .mapToInt(BookCoverDemand::getDemand)
                .sum();
    }
}
