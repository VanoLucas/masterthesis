package com.vanolucas.cppsolver.problem.instance;

import com.vanolucas.cppsolver.problem.CoverPrintingProblem;
import com.vanolucas.cppsolver.solution.BookCoverDemand;

import java.util.ArrayList;
import java.util.List;

public abstract class Cpp30BookCovers extends CoverPrintingProblem {

    /*
    Defaults
     */

    private static final int DEFAULT_NB_OFFSET_PLATES = 10;
    private static final int DEFAULT_NB_SLOTS_PER_OFFSET_PLATE = 4;
    private static final double DEFAULT_COST_OFFSET_PLATE = 0d;
    private static final double DEFAULT_COST_ONE_PRINTING = 1d;

    /*
    Book cover demands
     */

    private final static List<BookCoverDemand> BOOK_COVER_DEMANDS = new ArrayList<BookCoverDemand>() {{
        add(new BookCoverDemand("01", 30000));
        add(new BookCoverDemand("02", 28000));
        add(new BookCoverDemand("03", 27000));
        add(new BookCoverDemand("04", 26000));
        add(new BookCoverDemand("05", 26000));
        add(new BookCoverDemand("06", 23000));
        add(new BookCoverDemand("07", 22000));
        add(new BookCoverDemand("08", 22000));
        add(new BookCoverDemand("09", 20000));
        add(new BookCoverDemand("10", 20000));
        add(new BookCoverDemand("11", 19000));
        add(new BookCoverDemand("12", 18000));
        add(new BookCoverDemand("13", 17000));
        add(new BookCoverDemand("14", 16000));
        add(new BookCoverDemand("15", 15000));
        add(new BookCoverDemand("16", 15000));
        add(new BookCoverDemand("17", 14000));
        add(new BookCoverDemand("18", 13500));
        add(new BookCoverDemand("19", 13000));
        add(new BookCoverDemand("20", 11000));
        add(new BookCoverDemand("21", 10500));
        add(new BookCoverDemand("22", 10000));
        add(new BookCoverDemand("23", 9000));
        add(new BookCoverDemand("24", 9000));
        add(new BookCoverDemand("25", 7500));
        add(new BookCoverDemand("26", 6000));
        add(new BookCoverDemand("27", 5000));
        add(new BookCoverDemand("28", 2500));
        add(new BookCoverDemand("29", 1500));
        add(new BookCoverDemand("30", 1000));
    }};

    /*
    Constructors
     */

    public Cpp30BookCovers(GenomeType genomeType) {
        this(DEFAULT_NB_OFFSET_PLATES, genomeType);
    }
    public Cpp30BookCovers(int nbOffsetPlates,
                           GenomeType genomeType) {
        this(nbOffsetPlates, genomeType, DEFAULT_NB_SLOTS_PER_OFFSET_PLATE);
    }
    public Cpp30BookCovers(int nbOffsetPlates,
                           GenomeType genomeType,
                           int nbSlotsPerOffsetPlate) {
        this(nbOffsetPlates, genomeType, nbSlotsPerOffsetPlate, DEFAULT_COST_OFFSET_PLATE, DEFAULT_COST_ONE_PRINTING);
    }
    public Cpp30BookCovers(int nbOffsetPlates,
                           GenomeType genomeType,
                           int nbSlotsPerOffsetPlate,
                           double costOffsetPlate,
                           double costOnePrinting) {
        super(BOOK_COVER_DEMANDS, nbOffsetPlates, genomeType, nbSlotsPerOffsetPlate, costOffsetPlate, costOnePrinting);
    }
}