package com.vanolucas.cppsolver.solution;

import java.util.Random;

public class BookCoverDemandPart implements Comparable<BookCoverDemandPart>, Cloneable {

    /**
     * The book cover and its demand.
     */
    private BookCoverDemand bookCoverDemand;
    /**
     * This number of printings must be a fraction of the demand (nbPrintings <= demand).
     */
    private int nbPrintings;

    /*
    Constructors
     */

    public BookCoverDemandPart() {
        this(null);
    }
    public BookCoverDemandPart(BookCoverDemand bookCoverDemand) {
        this(bookCoverDemand, 0);
    }
    public BookCoverDemandPart(BookCoverDemand bookCoverDemand, int nbPrintings) {
        setBookCoverDemand(bookCoverDemand);
        setNbPrintings(nbPrintings);
    }

    /*
    Accessors
     */

    public void set(BookCoverDemand bookCoverDemand, int nbPrintings) {
        setBookCoverDemand(bookCoverDemand);
        setNbPrintings(nbPrintings);
    }

    public void setBookCoverDemand(BookCoverDemand bookCoverDemand) {
        this.bookCoverDemand = bookCoverDemand;
    }

    public void setNbPrintings(int nbPrintings) {
        this.nbPrintings = nbPrintings;
    }

    public BookCoverDemand getBookCover() {
        return bookCoverDemand;
    }

    public int getNbPrintings() {
        return nbPrintings;
    }

    /*
    Comparable
     */

    @Override
    public int compareTo(BookCoverDemandPart o) {
        return Integer.valueOf(nbPrintings).compareTo(o.nbPrintings);
    }

    /*
    Cloneable
     */

    @Override
    public BookCoverDemandPart clone() {
        return new BookCoverDemandPart(bookCoverDemand, nbPrintings);
    }

    /*
    Command
     */

    public BookCoverDemandPart splitInTwoOfRandomSize() {
        return splitInTwo(new Random().nextDouble());
    }

    public BookCoverDemandPart splitInTwo(final double cutFractionOut) {
        final int otherNbPrintings = (int) (cutFractionOut * nbPrintings);
        nbPrintings -= otherNbPrintings;
        return new BookCoverDemandPart(bookCoverDemand, otherNbPrintings);
    }

    /*
    toString
     */

    @Override
    public String toString() {
        return bookCoverDemand.getName() + ":" + nbPrintings + "/" + bookCoverDemand.getDemand();
    }
}

