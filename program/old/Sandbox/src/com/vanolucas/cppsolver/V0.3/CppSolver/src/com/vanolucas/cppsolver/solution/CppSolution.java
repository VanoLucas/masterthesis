package com.vanolucas.cppsolver.solution;

import com.vanolucas.cppsolver.problem.ICoverPrintingProblemInstance;
import com.vanolucas.opti.random.Randomizable;
import com.vanolucas.opti.util.Copyable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * This stores a solution of the Cover Printing Problem.
 */
public class CppSolution implements Randomizable, Copyable {

    /*
    Attributes
     */

    /**
     * Actual solution: offset plates that contain the book cover assignments.
     */
    private List<OffsetPlate> offsetPlates;
    /**
     * The General CPP instance this solution is meant for.
     */
    private ICoverPrintingProblemInstance cppInstance;

    /*
    Constructors
     */

    public CppSolution(ICoverPrintingProblemInstance cppInstance) {
        this(cppInstance, null);
    }
    public CppSolution(ICoverPrintingProblemInstance cppInstance, List<BookCoverDemandPart> bookCoverDemandParts) {
        this.offsetPlates = new ArrayList<>(cppInstance.getNbOffsetPlates());
        this.cppInstance = cppInstance;

        if (bookCoverDemandParts != null) {
            assignBookCovers(bookCoverDemandParts);
        }
    }

    /*
    Accessors
     */

    /**
     * Get the total number of printings required to realize this solution.
     * It is the sum of the number of printings required for each offset plate.
     *
     * @return Total number of printings required to realize this solution.
     */
    public int getTotalNbPrintings() {
        return offsetPlates.stream()
                .mapToInt(OffsetPlate::getNbPrintings)
                .sum();
    }

    /**
     * Evaluation function to calculate this solution's total cost.
     *
     * @return Total cost of the solution based on the params of the Cover Printing Problem instance.
     */
    public double getTotalCost() {
        double totalCost = cppInstance.getCostOffsetPlate() * cppInstance.getNbOffsetPlates();
        totalCost += cppInstance.getCostOnePrinting() * getTotalNbPrintings();
        return totalCost;
    }

    /**
     * For each book cover, get how many times it gets printed when realizing this solution.
     * @return The actual number of printings for each book cover when realizing this solution.
     */
    public List<BookCoverActualPrintings> getActualPrintings() {
        // for each book cover, calculate the actual number of printings produced
        return cppInstance.getBookCoverDemands().stream()
                .map(bookCover ->
                        new BookCoverActualPrintings(bookCover,
                                // for each offset plate, get the number of printings of this book cover it produces
                                offsetPlates.stream()
                                        // get the actual nb of printings of this book cover from this offset plate
                                        .mapToInt(offsetPlate -> offsetPlate.getNbPrintingsOfBookCover(bookCover))
                                        // sum for all offset plates that print this book cover
                                        .sum()
                        )
                )
                .collect(Collectors.toList());
    }

    /*
    Commands
     */

    /**
     * Assign book covers and their number of printings to slots of the offset plates in the order we received them.
     *
     * @param bookCoverDemandParts Split book cover demands with the number of printings required for each slot.
     */
    public void assignBookCovers(List<BookCoverDemandPart> bookCoverDemandParts) {
        final int NB_SLOTS_PER_OFFSET_PLATE = cppInstance.getNbSlotsPerOffsetPlate();

        // erase any old offset plate
        offsetPlates.clear();

        // for each offset plate to fill
        IntStream.range(0, cppInstance.getNbOffsetPlates())
                // split the list of book cover assignments for each offset plate (each list has n items for n slots of the offset plate)
                .mapToObj(offsetPlate ->
                        bookCoverDemandParts.subList(offsetPlate * NB_SLOTS_PER_OFFSET_PLATE,
                                offsetPlate * NB_SLOTS_PER_OFFSET_PLATE + NB_SLOTS_PER_OFFSET_PLATE)
                )
                // put the book cover assignments in the offset plates
                .forEach(bookCoversForThisOffsetPlate ->
                        offsetPlates.add(new OffsetPlate(bookCoversForThisOffsetPlate))
                );

        // sort offset plates by number of printings (descending)
        Collections.sort(offsetPlates, Collections.reverseOrder());
    }

    /**
     * Randomize this solution.
     */
    public void randomize() {
        final boolean SORT_BY_NB_OF_PRINTINGS_BEFORE_ASSIGNMENT_TO_SLOTS = true;

        final List<BookCoverDemand> bookCoverDemands = cppInstance.getBookCoverDemands();

        // list that will contain the book cover assignments
        List<BookCoverDemandPart> bookCoverDemandParts = bookCoverDemands.stream()
                .map(b -> new BookCoverDemandPart(b, b.getDemand()))
                .collect(Collectors.toList());

        // random generator
        Random rand = new Random();

        // spread the demand over all available slots
        while (bookCoverDemandParts.size() < cppInstance.getTotalNbSlots()) {
            // pick a random book cover demand assignment
            int indexToSplit = rand.nextInt(bookCoverDemandParts.size());

            // split it over two slots
            bookCoverDemandParts.add(
                    bookCoverDemandParts.get(indexToSplit)
                            .splitInTwoOfRandomSize()
            );
        }

        // sort by number of printings assigned to each slot (descending)
        if (SORT_BY_NB_OF_PRINTINGS_BEFORE_ASSIGNMENT_TO_SLOTS) {
            Collections.sort(bookCoverDemandParts, Collections.reverseOrder());
        }

        // fill this solution's offset plates with proposed assignments
        assignBookCovers(bookCoverDemandParts);
    }

    /*
    Copyable
     */

    @Override
    public void copyFrom(Object other) {
        CppSolution o = (CppSolution) other;

        offsetPlates = o.offsetPlates.stream()
                .map(offsetPlate -> offsetPlate.clone())
                .collect(Collectors.toList());
        cppInstance = o.cppInstance;
    }

    /*
    toString
     */

    @Override
    public String toString() {
        String str = "";

        /*
        Offset plates
         */

        str += cppInstance.getNbOffsetPlates() + " offset plates:";
        str += offsetPlates.stream()
                .map(offsetPlate -> offsetPlate.toString())
                .collect(Collectors.joining("\n\t", "\n\t", ""));
        str += "\n\tTotal printings: " + getTotalNbPrintings();

        /*
        Actual printings
         */

        str += "\nActually printed:";
        str += getActualPrintings().stream()
                .map(BookCoverActualPrintings::toString)
                .collect(Collectors.joining("\n\t", "\n\t", ""));

        /*
        Total printings
         */

        str += "\nBook covers printed: " + offsetPlates.stream()
                .mapToInt(o -> o.getNbPrintings() * cppInstance.getNbSlotsPerOffsetPlate())
                .sum();
        str += " (demand was " + cppInstance.getTotalDemands() + ")";

        /*
        Waste
         */

        str += "\nTotal waste: ";

        final double totalWaste = getActualPrintings().stream()
                .mapToInt(BookCoverActualPrintings::getWaste)
                .sum();
        final double TOTAL_DEMANDS = cppInstance.getTotalDemands();

        str += (int)totalWaste + "/" + TOTAL_DEMANDS;
        str += " (" + totalWaste/TOTAL_DEMANDS*100d + "%)";

        /*
        (Non) realizable solution
         */

        if (getActualPrintings().stream()
                .map(BookCoverActualPrintings::isDemandFulfilled)
                .anyMatch(p -> p == false)
                ) {
            str += "\n/!\\ Non realizable solution.";
        }

        /*
        Total cost
         */

        str += "\nTotal cost: " + getTotalCost();

        return str;
    }
}
