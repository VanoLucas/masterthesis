package com.vanolucas.cppsolver.solution;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class OffsetPlate implements Comparable<OffsetPlate>, Cloneable {

    private List<BookCoverDemandPart> slots;

    /*
    Constructors
     */

    public OffsetPlate(int nbSlots) {
        setSlots(new ArrayList<>(nbSlots));
    }

    public OffsetPlate(List<BookCoverDemandPart> slots) {
        setSlots(slots);
    }

    /*
    Accessors
     */

    public void setSlots(List<BookCoverDemandPart> slots) {
        this.slots = slots;
    }

    /**
     * Get the number of slots of this offset plate.
     *
     * @return Nb of slots of this offset plate.
     */
    public int getNbSlots() {
        return slots.size();
    }

    public int getNbSlotsHavingBookCover(BookCover bookCover) {
        return (int) slots.stream()
                .filter(b -> b.getBookCover() == bookCover)
                .count();
    }

    /**
     * Get the number of printings required for this Offset Plate.
     *
     * @return The number of printings required for this Offset Plate.
     */
    public int getNbPrintings() {
        // the number of printings of this offset plate is equal
        // to the max nb of printings assigned to a slot of this offset plate
        return slots.stream()
                .mapToInt(BookCoverDemandPart::getNbPrintings)
                .max()
                .getAsInt();
    }

    public int getNbPrintingsOfBookCover(BookCover bookCover) {
        return getNbSlotsHavingBookCover(bookCover) * getNbPrintings();
    }

    /*
    Comparable
     */

    @Override
    public int compareTo(OffsetPlate o) {
        return Integer.valueOf(this.getNbPrintings()).compareTo(o.getNbPrintings());
    }

    /*
    Cloneable
     */

    @Override
    public OffsetPlate clone() {
        return new OffsetPlate(slots.stream()
                .map(slot -> slot.clone())
                .collect(Collectors.toList())
        );
    }

    /*
    toString
     */

    @Override
    public String toString() {
        String str = getNbPrintings() + " printings of:\t";

        str += slots.stream()
                .map(slot -> slot.toString())
                .collect(Collectors.joining(" | "));

        return str;
    }
}

