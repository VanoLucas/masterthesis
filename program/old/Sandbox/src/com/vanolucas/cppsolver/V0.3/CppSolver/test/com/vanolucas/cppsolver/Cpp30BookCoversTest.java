package com.vanolucas.cppsolver;

import com.vanolucas.cppsolver.problem.CoverPrintingProblem;
import com.vanolucas.cppsolver.problem.instance.Cpp30BookCovers;
import com.vanolucas.opti.algo.OptiAlgo;
import com.vanolucas.opti.algo.localsearch.LocalSearch;
import com.vanolucas.opti.algo.randomsearch.RandomSearch;
import com.vanolucas.opti.mutation.AGenomeMutation;
import com.vanolucas.opti.mutation.Double01GeneRand;
import com.vanolucas.opti.mutation.RandomGenesMutation;
import com.vanolucas.opti.quality.DoubleCost;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.solution.Solution;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class Cpp30BookCoversTest {

    @Test
    public void randomSearch() {

        final DoubleCost TARGET_QUALITY = new DoubleCost(123000d);

        Cpp30BookCovers cppInstance = new Cpp30BookCovers(CoverPrintingProblem.GenomeType.SSB_DOUBLE) {
            @Override
            public List<AGenomeMutation> newGeneticMutationOperators() {
                return null;
            }
        };

        RandomSearch randomSearch = cppInstance.newRandomSearch();

        randomSearch.addListener(new OptiAlgo.Listener() {
            @Override
            public void onNewBetterSolutionFound(Solution newBetterSolution, long iteration) {
//                System.out.println("\nIteration: " + iteration);
//                System.out.println(newBetterSolution);
            }
        });

        randomSearch.runUntilQuality(new DoubleCost(TARGET_QUALITY));

        assertTrue(randomSearch.getLastBetterSolutionFoundQuality().isEquivalentOrBetterThan(TARGET_QUALITY));
    }


    @Test
    public void localSearch() {

        final DoubleCost TARGET_QUALITY = new DoubleCost(115000d);

        // this config gave 116500 cost on a good start
        Cpp30BookCovers cppInstance = new Cpp30BookCovers(CoverPrintingProblem.GenomeType.PS_DOUBLE) {
            @Override
            public List<AGenomeMutation> newGeneticMutationOperators() {
                // quite fast with those operators
                return new ArrayList<AGenomeMutation>() {{
                    addAll(DoubleStream.iterate(0.01d, p -> p + 0.0025d).limit(80)
                            .mapToObj(proba -> new RandomGenesMutation(new Double01GeneRand(), proba))
                            .collect(Collectors.toList())
                    );
                    addAll(DoubleStream.iterate(0.05d, p -> p + 0.0025d).limit(80)
                            .mapToObj(proba -> new RandomGenesMutation(new Double01GeneRand(), proba))
                            .collect(Collectors.toList())
                    );
                    addAll(DoubleStream.iterate(0.1d, p -> p + 0.005d).limit(0)
                            .mapToObj(proba -> new RandomGenesMutation(new Double01GeneRand(), proba))
                            .collect(Collectors.toList())
                    );
                    addAll(DoubleStream.iterate(0.90d, p -> p + 0.01d).limit(5)
                            .mapToObj(proba -> new RandomGenesMutation(new Double01GeneRand(), proba))
                            .collect(Collectors.toList())
                    );
                }};
            }
        };

        LocalSearch localSearch = cppInstance.newLocalSearch();

        localSearch.addListener(new OptiAlgo.Listener() {
            @Override
            public void onNewBetterSolutionFound(Solution newBetterSolution, long iteration) {
                System.out.println("\nIteration: " + iteration);
                System.out.println(newBetterSolution);
            }
        });

        localSearch.runUntilQuality(new DoubleCost(TARGET_QUALITY));

        assertTrue(localSearch.getLastBetterSolutionFoundQuality().isEquivalentOrBetterThan(TARGET_QUALITY));
    }



    @Test
    public void parallelLocalSearches() {

        Cpp30BookCovers cppInstance = new Cpp30BookCovers(CoverPrintingProblem.GenomeType.PS_DOUBLE) {
            @Override
            public List<AGenomeMutation> newGeneticMutationOperators() {
                // quite fast with those operators
                return new ArrayList<AGenomeMutation>() {{
                    addAll(DoubleStream.iterate(0.01d, p -> p + 0.0025d).limit(80)
                            .mapToObj(proba -> new RandomGenesMutation(new Double01GeneRand(), proba))
                            .collect(Collectors.toList())
                    );
                    addAll(DoubleStream.iterate(0.05d, p -> p + 0.0025d).limit(80)
                            .mapToObj(proba -> new RandomGenesMutation(new Double01GeneRand(), proba))
                            .collect(Collectors.toList())
                    );
                    addAll(DoubleStream.iterate(0.1d, p -> p + 0.005d).limit(0)
                            .mapToObj(proba -> new RandomGenesMutation(new Double01GeneRand(), proba))
                            .collect(Collectors.toList())
                    );
                    addAll(DoubleStream.iterate(0.90d, p -> p + 0.01d).limit(5)
                            .mapToObj(proba -> new RandomGenesMutation(new Double01GeneRand(), proba))
                            .collect(Collectors.toList())
                    );
                }};
            }
        };

        LocalSearch.Listener listener = new LocalSearch.Listener() {
            public Quality bestFound = null;

            @Override
            public synchronized void onNewBetterSolutionFound(Solution newBetterSolution, long iteration) {
                if (bestFound == null
                        || newBetterSolution.getQuality().isBetterThan(bestFound)) {
//                    System.out.println(newBetterSolution);
                    bestFound = newBetterSolution.getQuality();
                }
            }
        };

        final int NB_THREADS = 4;
        final int NB_ITERATIONS = 5;
        final String bestQualities = IntStream.range(0, NB_THREADS)
                .mapToObj(i -> {
                    LocalSearch algo = cppInstance.newLocalSearch();
                    algo.addListener(listener);
                    return algo;
                })
                .parallel()
                .map(algo -> {
                    algo.runUntilIteration(NB_ITERATIONS);
                    return algo.getLastBetterSolutionFoundQuality().toString();
                })
                .sorted()
                .collect(Collectors.joining(", "));

//        System.out.println(bestQualities);
    }



    @Test
    void evolutionaryAlgo() {
        System.out.println("evol algo test");
    }
}