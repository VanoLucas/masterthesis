package com.vanolucas.opti.algo;

import com.vanolucas.opti.eval.AGeneticEvaluationFunctionFactory;
import com.vanolucas.opti.genome.AGenomeDecoderFactory;
import com.vanolucas.opti.random.genome.AGenomeRandomizerFactory;
import com.vanolucas.opti.solution.IndividualFactory;

public interface AGeneticOptiAlgoFactory extends AOptiAlgoFactory, IndividualFactory, AGenomeRandomizerFactory, AGeneticEvaluationFunctionFactory {
}
