package com.vanolucas.opti.algo;

import com.vanolucas.opti.eval.AEvaluationFunctionFactory;
import com.vanolucas.opti.random.solution.SolutionRandomizerFactory;
import com.vanolucas.opti.solution.SolutionFactory;

public interface AOptiAlgoFactory extends SolutionFactory, SolutionRandomizerFactory, AEvaluationFunctionFactory {

}
