package com.vanolucas.opti.algo;

import com.vanolucas.opti.random.phenotype.APhenotypeRandomizerFactory;
import com.vanolucas.opti.solution.PhenotypeSolutionFactory;

public interface APhenotypeOptiAlgoFactory extends AOptiAlgoFactory, PhenotypeSolutionFactory, APhenotypeRandomizerFactory {
}
