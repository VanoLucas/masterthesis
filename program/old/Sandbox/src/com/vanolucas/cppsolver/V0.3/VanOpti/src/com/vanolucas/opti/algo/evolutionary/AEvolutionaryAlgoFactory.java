package com.vanolucas.opti.algo.evolutionary;

import com.vanolucas.opti.algo.localsearch.ALocalSearchFactory;
import com.vanolucas.opti.crossover.CrossoverOperatorPickerFactory;
import com.vanolucas.opti.mutation.collection.MutationOperatorPickerFactory;
import com.vanolucas.opti.selection.SelectionOperatorFactory;
import com.vanolucas.opti.solution.PopulationFactory;

public interface AEvolutionaryAlgoFactory
        extends ALocalSearchFactory,
        PopulationFactory,
        SelectionOperatorFactory,
        CrossoverOperatorPickerFactory,
        MutationOperatorPickerFactory {

    default EvolutionaryAlgo newEvolutionaryAlgo() {
        return new EvolutionaryAlgo(this);
    }
}
