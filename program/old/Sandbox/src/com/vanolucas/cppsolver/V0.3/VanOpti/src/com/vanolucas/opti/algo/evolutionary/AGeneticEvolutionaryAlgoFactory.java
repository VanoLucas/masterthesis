package com.vanolucas.opti.algo.evolutionary;

import com.vanolucas.opti.algo.localsearch.AGeneticLocalSearchFactory;
import com.vanolucas.opti.mutation.collection.GeneticMutationOperatorPickerFactory;

public interface AGeneticEvolutionaryAlgoFactory
        extends AEvolutionaryAlgoFactory,
        AGeneticLocalSearchFactory,
        GeneticMutationOperatorPickerFactory {
}
