package com.vanolucas.opti.algo.evolutionary;

import com.vanolucas.opti.algo.OptiAlgo;
import com.vanolucas.opti.crossover.CrossoverOperatorPicker;
import com.vanolucas.opti.crossover.CrossoverOperatorPickerFactory;
import com.vanolucas.opti.eval.AEvaluationFunction;
import com.vanolucas.opti.eval.AEvaluationFunctionFactory;
import com.vanolucas.opti.math.ParetoFront;
import com.vanolucas.opti.mutation.collection.MutationOperatorPicker;
import com.vanolucas.opti.mutation.collection.MutationOperatorPickerFactory;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.random.solution.SolutionRandomizer;
import com.vanolucas.opti.random.solution.SolutionRandomizerFactory;
import com.vanolucas.opti.selection.SelectionOperator;
import com.vanolucas.opti.selection.SelectionOperatorFactory;
import com.vanolucas.opti.solution.Population;
import com.vanolucas.opti.solution.PopulationFactory;
import com.vanolucas.opti.solution.Solution;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

public class EvolutionaryAlgo extends OptiAlgo {

    /*
    Defaults
     */

    private static final int DEFAULT_POPULATION_SIZE = 10;
    private static final double DEFAULT_CROSSOVER_PROBA = 0.8d;
    private static final double DEFAULT_MUTATION_PROBA = 0.1d;

    /*
    Listener
     */

    public interface Listener extends OptiAlgo.Listener {
        default void onGenerationStart(long generation, ParetoFront<Quality> bestQualities) {
        }

        default void onGenerationEnd(long generation, ParetoFront<Quality> bestQualities) {
        }
    }

    /*
    Attributes
     */

    private Population population;
    private Population nextPopulation;
    private List<List<Solution>> matingGroups;
    private int populationSize;
    private double crossoverProba;
    private double mutationProba;
    private boolean randomizePopulation = true;

    private SolutionRandomizer randomizer;
    private SelectionOperator selectionOperator;
    private CrossoverOperatorPicker crossoverPicker;
    private MutationOperatorPicker mutationPicker;
    private AEvaluationFunction evaluationFunction;

    private PopulationFactory populationFactory;
    private SolutionRandomizerFactory randomizerFactory;
    private SelectionOperatorFactory selectionOperatorFactory;
    private CrossoverOperatorPickerFactory crossoverPickerFactory;
    private MutationOperatorPickerFactory mutationPickerFactory;
    private AEvaluationFunctionFactory evaluationFunctionFactory;

    private Random random;

    /*
    Constructors
     */

    public EvolutionaryAlgo(AEvolutionaryAlgoFactory factory) {
        this(DEFAULT_POPULATION_SIZE, DEFAULT_CROSSOVER_PROBA, DEFAULT_MUTATION_PROBA, factory);
    }

    public EvolutionaryAlgo(final int populationSize,
                            final double crossoverProba,
                            final double mutationProba,
                            AEvolutionaryAlgoFactory factory) {
        this(populationSize, crossoverProba, mutationProba, factory, factory, factory, factory, factory, factory);
    }

    public EvolutionaryAlgo(final int populationSize,
                            final double crossoverProba,
                            final double mutationProba,
                            PopulationFactory populationFactory,
                            SolutionRandomizerFactory randomizerFactory,
                            SelectionOperatorFactory selectionOperatorFactory,
                            CrossoverOperatorPickerFactory crossoverPickerFactory,
                            MutationOperatorPickerFactory mutationPickerFactory,
                            AEvaluationFunctionFactory evaluationFunctionFactory) {
        this.populationSize = populationSize;
        this.crossoverProba = crossoverProba;
        this.mutationProba = mutationProba;
        this.populationFactory = populationFactory;
        this.randomizerFactory = randomizerFactory;
        this.selectionOperatorFactory = selectionOperatorFactory;
        this.crossoverPickerFactory = crossoverPickerFactory;
        this.mutationPickerFactory = mutationPickerFactory;
        this.evaluationFunctionFactory = evaluationFunctionFactory;
    }

    /*
    Helpers
     */

    @Override
    protected void runIteration() {
        initIfNeeded();

        // notify listeners that we start a new generation
        notify(listener -> {
            if (listener instanceof EvolutionaryAlgo.Listener) {
                ((EvolutionaryAlgo.Listener) listener).onGenerationStart(iteration, getBestQualities());
            }
        });

        // if we need to randomize the whole population at this generation
        if (randomizePopulation) {
            // randomize population
            population.randomizeUsing(randomizer);
            // evaluate population
            population.evaluateUsing(evaluationFunction);
            // propose new best solutions from population
            proposeNewSolutions(population);
            randomizePopulation = false;
        }

        // otherwise it is a normal evolution generation
        else {
            // selection of groups of parents
            selectionOfParents();

            // crossover to produce children
            crossover();

            // mutation of children
            mutation();

            // children become the new current population
            Population tmp = population;
            population = nextPopulation;
            nextPopulation = tmp;

            // evaluate population
            population.evaluateUsing(evaluationFunction);

            // propose new best solutions from population
            proposeNewSolutions(population);
        }

        // notify listeners that this is the end of the current generation
        notify(listener -> {
            if (listener instanceof EvolutionaryAlgo.Listener) {
                ((EvolutionaryAlgo.Listener) listener).onGenerationEnd(iteration, getBestQualities());
            }
        });
    }

    protected void initIfNeeded() {
        if (random == null) {
            random = new Random();
        }

        if (population == null) {
            population = populationFactory.newPopulation(populationSize);
        }
        if (nextPopulation == null) {
            nextPopulation = populationFactory.newPopulation(populationSize);
        }

        if (randomizer == null) {
            randomizer = randomizerFactory.newSolutionRandomizer();
        }

        if (selectionOperator == null) {
            selectionOperator = selectionOperatorFactory.newSelectionOperator();
        }

        if (crossoverPicker == null) {
            crossoverPicker = crossoverPickerFactory.newCrossoverOperatorPicker();
        }

        if (mutationPicker == null) {
            mutationPicker = mutationPickerFactory.newMutationOperatorPicker();
        }

        if (evaluationFunction == null) {
            evaluationFunction = evaluationFunctionFactory.newEvaluationFunction();
        }
    }

    protected void selectionOfParents() {
        matingGroups.clear();
        // todo select populationSize parents in groups
    }

    protected void crossover() {
        // create children groups to match parent groups
        List<List<Solution>> childrenGroups = new ArrayList<>(matingGroups.size());
        List<Solution> allChildren = nextPopulation.get();
        int childCursor = 0;
        for (List<Solution> matingGroup : matingGroups) {
            final int MATING_GROUP_SIZE = matingGroup.size();
            List<Solution> childrenGroup = new ArrayList<>(MATING_GROUP_SIZE);
            childrenGroup.addAll(allChildren.subList(childCursor, childCursor + MATING_GROUP_SIZE));
            childrenGroups.add(childrenGroup);
            childCursor += MATING_GROUP_SIZE;
        }

        // perform crossover to produce children for each group
        IntStream.range(0, matingGroups.size())
                .forEach(matingGroupIndex -> {
                    List<Solution> parents = matingGroups.get(matingGroupIndex);
                    List<Solution> children = childrenGroups.get(matingGroupIndex);

                    // decide if we perform a crossover based on crossover proba param
                    if (random.nextDouble() < crossoverProba) {
                        // perform crossover
                        crossoverPicker.crossover(parents, children);
                    }
                    // otherwise just pass the parents through to next generation
                    else {
                        IntStream.range(0, parents.size())
                                .forEach(i -> parents.get(i).reproduceTo(children.get(i)));
                    }
                });
    }

    protected void mutation() {
        // decide whether or not to mutate each child based on mutation proba param
        nextPopulation.forEach(child -> {
            if (random.nextDouble() < mutationProba) {
                // perform mutation
                mutationPicker.mutate(child);
            }
        });
    }

    /*
    Commands
     */

    public void randomizePopulationAtNextGeneration() {
        randomizePopulation = true;
    }
}
