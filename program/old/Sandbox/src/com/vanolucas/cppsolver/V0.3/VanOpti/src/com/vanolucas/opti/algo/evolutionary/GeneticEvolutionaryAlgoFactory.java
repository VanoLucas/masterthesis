package com.vanolucas.opti.algo.evolutionary;

import com.vanolucas.opti.algo.localsearch.GeneticLocalSearchFactory;
import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.quality.Quality;

public interface GeneticEvolutionaryAlgoFactory<TGenome extends Genome, TPhenotype, TQuality extends Quality>
        extends AGeneticEvolutionaryAlgoFactory,
        GeneticLocalSearchFactory<TGenome, TPhenotype, TQuality> {
}
