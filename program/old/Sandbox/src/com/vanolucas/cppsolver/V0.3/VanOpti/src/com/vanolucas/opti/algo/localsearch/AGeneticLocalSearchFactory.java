package com.vanolucas.opti.algo.localsearch;

import com.vanolucas.opti.algo.randomsearch.AGeneticRandomSearchFactory;
import com.vanolucas.opti.mutation.collection.GeneticMutationOperatorsFactory;

public interface AGeneticLocalSearchFactory
        extends ALocalSearchFactory,
        AGeneticRandomSearchFactory,
        GeneticMutationOperatorsFactory {

}
