package com.vanolucas.opti.algo.localsearch;

import com.vanolucas.opti.algo.randomsearch.ARandomSearchFactory;
import com.vanolucas.opti.mutation.collection.MutationOperatorsFactory;

public interface ALocalSearchFactory extends ARandomSearchFactory, MutationOperatorsFactory {

    default LocalSearch newLocalSearch() {
        return new LocalSearch(this);
    }
}
