package com.vanolucas.opti.algo.localsearch;

import com.vanolucas.opti.algo.randomsearch.APhenotypeRandomSearchFactory;
import com.vanolucas.opti.mutation.collection.PhenotypeMutationOperatorsFactory;

public interface APhenotypeLocalSearchFactory extends ALocalSearchFactory, APhenotypeRandomSearchFactory, PhenotypeMutationOperatorsFactory {
}
