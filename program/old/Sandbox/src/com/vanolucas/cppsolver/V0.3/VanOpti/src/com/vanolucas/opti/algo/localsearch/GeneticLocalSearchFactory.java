package com.vanolucas.opti.algo.localsearch;

import com.vanolucas.opti.eval.GeneticEvaluationFunction;
import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.mutation.AGenomeMutation;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.random.genome.GenomeRandomizer;

import java.util.List;

public interface GeneticLocalSearchFactory<TGenome extends Genome, TPhenotype, TQuality extends Quality>
        extends AGeneticLocalSearchFactory {

    @Override
    TGenome newGenome();

    @Override
    TPhenotype newPhenotype();

    @Override
    TQuality newQuality();

    @Override
    GenomeRandomizer<TGenome> newGenomeRandomizer();

    @Override
    GeneticEvaluationFunction<TGenome, TPhenotype, TQuality> newGeneticEvaluationFunction();
}
