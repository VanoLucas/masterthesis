package com.vanolucas.opti.algo.localsearch;

import com.vanolucas.opti.algo.OptiAlgo;
import com.vanolucas.opti.eval.AEvaluationFunction;
import com.vanolucas.opti.eval.AEvaluationFunctionFactory;
import com.vanolucas.opti.mutation.collection.MutationOperators;
import com.vanolucas.opti.mutation.collection.MutationOperatorsFactory;
import com.vanolucas.opti.random.solution.SolutionRandomizer;
import com.vanolucas.opti.random.solution.SolutionRandomizerFactory;
import com.vanolucas.opti.solution.Solution;
import com.vanolucas.opti.solution.SolutionFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class LocalSearch extends OptiAlgo {

    /*
    Listener
     */

    public interface Listener extends OptiAlgo.Listener {
        default void onLocalOptimumReached(long iteration, Solution solution) {}
    }

    /*
    Attributes
     */

    private Solution currentSolution;
    protected List<Solution> neighbors;
    private boolean noMoreGoodNeighbor = false;

    private SolutionRandomizer randomizer;
    private MutationOperators mutationOperators;
    protected AEvaluationFunction evaluationFunction;

    private SolutionFactory solutionFactory;
    private SolutionRandomizerFactory randomizerFactory;
    private MutationOperatorsFactory mutationOperatorsFactory;
    private AEvaluationFunctionFactory evaluationFunctionFactory;

    /*
    Constructors
     */

    public LocalSearch(ALocalSearchFactory factory) {
        this(factory, DEFAULT_MAX_PARETO_FRONT_SIZE);
    }

    public LocalSearch(ALocalSearchFactory factory, final int maxParetoFrontSize) {
        this(factory, factory, factory, factory, maxParetoFrontSize);
    }

    public LocalSearch(SolutionFactory solutionFactory,
                       SolutionRandomizerFactory randomizerFactory,
                       MutationOperatorsFactory mutationOperatorsFactory,
                       AEvaluationFunctionFactory evaluationFunctionFactory,
                       final int maxParetoFrontSize) {
        super(maxParetoFrontSize);
        this.solutionFactory = solutionFactory;
        this.randomizerFactory = randomizerFactory;
        this.mutationOperatorsFactory = mutationOperatorsFactory;
        this.evaluationFunctionFactory = evaluationFunctionFactory;
    }

    /*
    Helpers
     */

    @Override
    protected void runIteration() {
        initIfNeeded();

        // when previous local search ended, randomize to start a new one
        if (noMoreGoodNeighbor) {
            randomizer.randomize(currentSolution);
            noMoreGoodNeighbor = false;
        }

        // reproduce current solution to neighbors
        neighbors.stream()
                .forEach(neighbor -> neighbor.reproduceFrom(currentSolution));
        // mutate neighbors
        mutationOperators.mutate(neighbors);
        // evaluate neighbors
        evaluateNeighbors();

        // move to best neighbor (if there is a good one)
        noMoreGoodNeighbor = !moveToBestNeighbor();

        // propose that new best neighbor we moved to as a new better solution
        if (!noMoreGoodNeighbor) {
            proposeNewSolution(currentSolution);
        }
    }

    protected void initIfNeeded() {
        if (randomizer == null) {
            randomizer = randomizerFactory.newSolutionRandomizer();
        }

        if (currentSolution == null) {
            currentSolution = solutionFactory.newSolution();
            randomizer.randomize(currentSolution);
        }

        if (mutationOperators == null) {
            mutationOperators = mutationOperatorsFactory.newMutationOperators();
        }

        if (neighbors == null) {
            neighbors = new ArrayList<>(mutationOperators.size());
        }
        if (neighbors.size() != mutationOperators.size()) {
            neighbors = IntStream.range(0, mutationOperators.size())
                    .mapToObj(i -> solutionFactory.newSolution())
                    .collect(Collectors.toList());
        }

        if (evaluationFunction == null) {
            evaluationFunction = evaluationFunctionFactory.newEvaluationFunction();
        }
    }

    protected void evaluateNeighbors() {
        neighbors.stream()
                .forEach(neighbor -> evaluateSolution(neighbor, evaluationFunction));
    }

    private boolean moveToBestNeighbor() {
        // find the best neighbor
        Solution bestNeighbor = Collections.max(neighbors);

        // if the best neighbor is at least equivalent to the current solution, move to it
        if (bestNeighbor.isEquivalentOrBetterThan(currentSolution)) {
            // swap the best neighbor with our current solution
            Solution tmp = currentSolution;
            currentSolution = bestNeighbor;
            neighbors.remove(bestNeighbor);
            neighbors.add(tmp);

            return true;
        }

        // else, no more good neighbor: the local search is over
        else {
            // notify listeners that we reached a local optimum
            notify(listener -> {
                if (listener instanceof Listener) {
                    ((Listener) listener).onLocalOptimumReached(iteration, currentSolution);
                }
            });

            return false;
        }
    }
}
