package com.vanolucas.opti.algo.localsearch;

import com.vanolucas.opti.eval.EvaluationFunction;
import com.vanolucas.opti.mutation.APhenotypeMutation;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.random.phenotype.PhenotypeRandomizer;
import com.vanolucas.opti.util.Copyable;

import java.util.List;

public interface PhenotypeLocalSearchFactory<TPhenotype extends Copyable, TQuality extends Quality>
        extends APhenotypeLocalSearchFactory {

    @Override
    TPhenotype newPhenotype();

    @Override
    TQuality newQuality();

    @Override
    PhenotypeRandomizer<TPhenotype> newPhenotypeRandomizer();

    @Override
    public EvaluationFunction<TPhenotype, TQuality> newEvaluationFunction();

    @Override
    public List<APhenotypeMutation> newPhenotypeMutationOperators();
}
