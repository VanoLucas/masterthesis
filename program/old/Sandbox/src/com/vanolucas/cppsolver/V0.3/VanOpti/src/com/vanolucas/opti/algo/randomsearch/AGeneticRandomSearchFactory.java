package com.vanolucas.opti.algo.randomsearch;

import com.vanolucas.opti.algo.AGeneticOptiAlgoFactory;

public interface AGeneticRandomSearchFactory extends ARandomSearchFactory, AGeneticOptiAlgoFactory {

}
