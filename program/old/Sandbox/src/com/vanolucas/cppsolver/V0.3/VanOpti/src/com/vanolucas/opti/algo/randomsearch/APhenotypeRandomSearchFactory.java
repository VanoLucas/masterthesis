package com.vanolucas.opti.algo.randomsearch;

import com.vanolucas.opti.algo.APhenotypeOptiAlgoFactory;

public interface APhenotypeRandomSearchFactory extends ARandomSearchFactory, APhenotypeOptiAlgoFactory {
}
