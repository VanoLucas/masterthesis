package com.vanolucas.opti.algo.randomsearch;


import com.vanolucas.opti.algo.AOptiAlgoFactory;

public interface ARandomSearchFactory extends AOptiAlgoFactory {

    default RandomSearch newRandomSearch() {
        return new RandomSearch(this);
    }
}
