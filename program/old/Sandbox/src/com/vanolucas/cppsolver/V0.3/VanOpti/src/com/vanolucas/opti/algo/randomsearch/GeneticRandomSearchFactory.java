package com.vanolucas.opti.algo.randomsearch;

import com.vanolucas.opti.eval.EvaluationFunction;
import com.vanolucas.opti.eval.GeneticEvaluationFunction;
import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.genome.GenomeDecoder;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.random.genome.GenomeRandomizer;

public interface GeneticRandomSearchFactory<TGenome extends Genome, TPhenotype, TQuality extends Quality>
        extends AGeneticRandomSearchFactory {

    @Override
    TGenome newGenome();

    @Override
    TPhenotype newPhenotype();

    @Override
    TQuality newQuality();

    @Override
    GenomeRandomizer<TGenome> newGenomeRandomizer();

    @Override
    GeneticEvaluationFunction<TGenome, TPhenotype, TQuality> newGeneticEvaluationFunction();
}
