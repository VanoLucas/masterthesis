package com.vanolucas.opti.algo.randomsearch;

import com.vanolucas.opti.eval.EvaluationFunction;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.random.phenotype.PhenotypeRandomizer;
import com.vanolucas.opti.util.Copyable;

public interface PhenotypeRandomSearchFactory<TPhenotype extends Copyable, TQuality extends Quality>
        extends APhenotypeRandomSearchFactory {

    @Override
    TPhenotype newPhenotype();

    @Override
    TQuality newQuality();

    @Override
    PhenotypeRandomizer<TPhenotype> newPhenotypeRandomizer();

    @Override
    EvaluationFunction<TPhenotype, TQuality> newEvaluationFunction();
}
