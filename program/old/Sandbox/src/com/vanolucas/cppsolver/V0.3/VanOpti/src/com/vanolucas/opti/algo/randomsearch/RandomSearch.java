package com.vanolucas.opti.algo.randomsearch;

import com.vanolucas.opti.algo.OptiAlgo;
import com.vanolucas.opti.eval.AEvaluationFunction;
import com.vanolucas.opti.eval.AEvaluationFunctionFactory;
import com.vanolucas.opti.random.solution.SolutionRandomizer;
import com.vanolucas.opti.random.solution.SolutionRandomizerFactory;
import com.vanolucas.opti.solution.Solution;
import com.vanolucas.opti.solution.SolutionFactory;

public class RandomSearch extends OptiAlgo {

    /*
    Attributes
     */

    protected Solution currentSolution;
    protected SolutionRandomizer randomizer;
    protected AEvaluationFunction evaluationFunction;

    private SolutionFactory solutionFactory;
    private SolutionRandomizerFactory randomizerFactory;
    private AEvaluationFunctionFactory evaluationFunctionFactory;

    /*
    Constructor
     */

    public RandomSearch(ARandomSearchFactory factory) {
        this(factory, DEFAULT_MAX_PARETO_FRONT_SIZE);
    }
    public RandomSearch(ARandomSearchFactory factory, final int maxParetoFrontSize) {
        this(factory, factory, factory, maxParetoFrontSize);
    }
    public RandomSearch(SolutionFactory solutionFactory,
                        SolutionRandomizerFactory randomizerFactory,
                        AEvaluationFunctionFactory evaluationFunctionFactory,
                        final int maxParetoFrontSize) {
        super(maxParetoFrontSize);
        this.solutionFactory = solutionFactory;
        this.randomizerFactory = randomizerFactory;
        this.evaluationFunctionFactory = evaluationFunctionFactory;
    }

    /*
    Helpers
     */

    @Override
    protected void runIteration() {
        initIfNeeded();

        currentSolution.randomizeUsing(randomizer);
        evaluateCurrentSolution();

        proposeNewSolution(currentSolution);
    }

    protected void initIfNeeded() {
        if (currentSolution == null) {
            currentSolution = solutionFactory.newSolution();
        }

        if (randomizer == null) {
            randomizer = randomizerFactory.newSolutionRandomizer();
        }

        if (evaluationFunction == null) {
            evaluationFunction = evaluationFunctionFactory.newEvaluationFunction();
        }
    }

    protected void evaluateCurrentSolution() {
        evaluateSolution(currentSolution, evaluationFunction);
    }
}
