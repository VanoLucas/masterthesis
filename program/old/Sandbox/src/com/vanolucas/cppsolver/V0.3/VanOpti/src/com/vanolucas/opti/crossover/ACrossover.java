package com.vanolucas.opti.crossover;

import com.vanolucas.opti.solution.Population;
import com.vanolucas.opti.solution.Solution;

import java.util.List;

public interface ACrossover {

    default void crossover(final Population parents, Population outChildren) {
        crossover(parents.get(), outChildren.get());
    }

    void crossover(final List<Solution> parents, List<Solution> outChildren);
}
