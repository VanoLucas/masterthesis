package com.vanolucas.opti.crossover;

import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.solution.Individual;
import com.vanolucas.opti.solution.Solution;

import java.util.List;
import java.util.stream.Collectors;

public interface AGeneticCrossover extends ACrossover {

    @Override
    default void crossover(final List<Solution> parents, List<Solution> outChildren) {
        crossoverIndividuals(
                parents.stream()
                .map(parent -> (Individual) parent)
                .collect(Collectors.toList()),
                outChildren.stream()
                .map(child -> (Individual) child)
                .collect(Collectors.toList())
        );
    }

    default void crossoverIndividuals(final List<Individual> parents, List<Individual> outChildren) {
        crossoverGenomes(
                parents.stream()
                .map(parent -> parent.getGenome())
                .collect(Collectors.toList()),
                outChildren.stream()
                .map(child -> child.getGenome())
                .collect(Collectors.toList())
        );
    }

    void crossoverGenomes(final List<Genome> parents, List<Genome> outChildren);
}
