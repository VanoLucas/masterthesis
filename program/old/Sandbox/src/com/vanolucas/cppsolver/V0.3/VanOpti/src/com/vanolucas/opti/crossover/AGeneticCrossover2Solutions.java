package com.vanolucas.opti.crossover;

import com.vanolucas.opti.genome.Genome;

import java.util.List;

public interface AGeneticCrossover2Solutions extends AGeneticCrossover {

    @Override
    default void crossoverGenomes(final List<Genome> parents, List<Genome> outChildren) {
        crossoverAGenomes(parents.get(0), parents.get(1), outChildren.get(0), outChildren.get(1));
    }

    void crossoverAGenomes(final Genome parent1, final Genome parent2, Genome outChild1, Genome outChild2);
}
