package com.vanolucas.opti.crossover;

import com.vanolucas.opti.phenotype.APhenotypeWrap;
import com.vanolucas.opti.solution.PhenotypeSolution;
import com.vanolucas.opti.solution.Solution;

import java.util.List;
import java.util.stream.Collectors;

public interface APhenotypeCrossover extends ACrossover {

    @Override
    default void crossover(final List<Solution> parents, List<Solution> outChildren) {
        crossoverPhenotypeSolutions(
                parents.stream()
                .map(parent -> (PhenotypeSolution) parent)
                .collect(Collectors.toList()),
                outChildren.stream()
                .map(child -> (PhenotypeSolution) child)
                .collect(Collectors.toList())
        );
    }

    default void crossoverPhenotypeSolutions(final List<PhenotypeSolution> parents, List<PhenotypeSolution> outChildren) {
        crossoverPhenotypes(
                parents.stream()
                .map(parent -> parent.getPhenotypeWrap())
                .collect(Collectors.toList()),
                outChildren.stream()
                .map(child -> child.getPhenotypeWrap())
                .collect(Collectors.toList())
        );
    }

    void crossoverPhenotypes(final List<APhenotypeWrap> parents, List<APhenotypeWrap> outChildren);
}
