package com.vanolucas.opti.crossover;

import com.vanolucas.opti.phenotype.APhenotypeWrap;

import java.util.List;

public interface APhenotypeCrossover2Solutions extends APhenotypeCrossover {

    @Override
    default void crossoverPhenotypes(final List<APhenotypeWrap> parents, List<APhenotypeWrap> outChildren) {
        crossoverPhenotypes(parents.get(0), parents.get(1), outChildren.get(0), outChildren.get(1));
    }

    void crossoverPhenotypes(final APhenotypeWrap parent1, final APhenotypeWrap parent2, APhenotypeWrap outChild1, APhenotypeWrap outChild2);
}
