package com.vanolucas.opti.crossover;

import com.vanolucas.opti.math.Picker;
import com.vanolucas.opti.solution.Solution;
import com.vanolucas.opti.util.ObjWithProba;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CrossoverOperatorPicker {

    /*
    Attributes
     */

    private List<ACrossover> crossoverOperators;
    private Picker<ACrossover> crossoverPicker;

    /*
    Constructors
     */

    public CrossoverOperatorPicker(Map<ACrossover, Double> crossoverOperatorsWithProba) {
        this(
                // get crossover operators
                new ArrayList<>(crossoverOperatorsWithProba.keySet()),
                // get their corresponding probability
                new ArrayList<>(crossoverOperatorsWithProba.keySet()).stream()
                .mapToDouble(crossoverOperator -> crossoverOperatorsWithProba.get(crossoverOperator))
                .toArray()
        );
    }

    public CrossoverOperatorPicker(List<ObjWithProba<ACrossover>> crossoverOperatorsWithProba) {
        this(
                // get crossover operators
                crossoverOperatorsWithProba.stream()
                .map(ObjWithProba::getObj)
                .collect(Collectors.toList()),
                // get their corresponding probability
                crossoverOperatorsWithProba.stream()
                .mapToDouble(ObjWithProba::getProba)
                .toArray()
        );
    }

    public CrossoverOperatorPicker(List<ACrossover> crossoverOperators, double[] probabilities) {
        this.crossoverOperators = crossoverOperators;
        crossoverPicker = new Picker<>(this.crossoverOperators, probabilities);
    }

    /*
    Commands
     */

//    public void crossover(final List<List<Solution>> matingGroups, List<List<Solution>> childrenGroups) {
//        final int NB_GROUPS = Math.min(matingGroups.size(), childrenGroups.size());
//        IntStream.range(0, NB_GROUPS)
//                .forEach(groupIndex -> crossover(matingGroups.get(groupIndex), childrenGroups.get(groupIndex))
//                );
//    }

    public void crossover(final List<Solution> parents, List<Solution> children) {
        crossoverPicker.pickOne().crossover(parents, children);
    }

    public void crossoverParallel(final List<List<Solution>> matingGroups, List<List<Solution>> childrenGroups) {
        final int NB_GROUPS = Math.min(matingGroups.size(), childrenGroups.size());
        IntStream.range(0, NB_GROUPS)
                .parallel()
                .forEach(groupIndex -> {
                    ACrossover crossoverToUse;
                    synchronized (crossoverPicker) {
                        crossoverToUse = crossoverPicker.pickOne();
                    }
                    synchronized (crossoverToUse) {
                        crossoverToUse.crossover(matingGroups.get(groupIndex), childrenGroups.get(groupIndex));
                    }
                });
    }
}
