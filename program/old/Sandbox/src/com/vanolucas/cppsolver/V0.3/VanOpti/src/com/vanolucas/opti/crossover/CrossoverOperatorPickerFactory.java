package com.vanolucas.opti.crossover;

import com.vanolucas.opti.util.ObjWithProba;

import java.util.List;

public interface CrossoverOperatorPickerFactory {

    default CrossoverOperatorPicker newCrossoverOperatorPicker() {
        return new CrossoverOperatorPicker(newCrossoverOperatorsWithProba());
    }

    List<ObjWithProba<ACrossover>> newCrossoverOperatorsWithProba();
}
