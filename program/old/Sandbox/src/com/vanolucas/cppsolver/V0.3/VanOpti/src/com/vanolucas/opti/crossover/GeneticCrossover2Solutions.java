package com.vanolucas.opti.crossover;

import com.vanolucas.opti.genome.Genome;

public interface GeneticCrossover2Solutions<TGenome extends Genome> extends AGeneticCrossover2Solutions {

    @Override
    default void crossoverAGenomes(final Genome parent1, final Genome parent2, Genome outChild1, Genome outChild2) {
        crossoverGenomes(((TGenome) parent1), ((TGenome) parent2), ((TGenome) outChild1), ((TGenome) outChild2));
    }

    void crossoverGenomes(final TGenome parent1, final TGenome parent2, TGenome outChild1, TGenome outChild2);
}
