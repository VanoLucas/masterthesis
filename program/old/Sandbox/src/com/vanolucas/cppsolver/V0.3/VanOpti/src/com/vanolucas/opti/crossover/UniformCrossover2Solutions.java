package com.vanolucas.opti.crossover;

import com.vanolucas.opti.genome.ListGenome;

import java.util.Random;
import java.util.stream.IntStream;

public class UniformCrossover2Solutions implements GeneticCrossover2Solutions<ListGenome> {

    private static final double DEFAULT_SWAP_PROBA = 0.5d;

    private double swapProba;
    private Random random;

    public UniformCrossover2Solutions() {
        this(DEFAULT_SWAP_PROBA);
    }

    public UniformCrossover2Solutions(final double swapProba) {
        this.swapProba = swapProba;
    }

    @Override
    public void crossoverGenomes(ListGenome parent1, ListGenome parent2, ListGenome outChild1, ListGenome outChild2) {
        if (random == null) {
            this.random = new Random();
        }

        final int GENOME_LENGTH = parent1.size();

        IntStream.range(0, GENOME_LENGTH)
                .forEach(geneIndex -> {
                    if (random.nextDouble() < swapProba) {
                        outChild1.set(geneIndex, parent2.get(geneIndex));
                        outChild2.set(geneIndex, parent1.get(geneIndex));
                    } else {
                        outChild1.set(geneIndex, parent1.get(geneIndex));
                        outChild2.set(geneIndex, parent2.get(geneIndex));
                    }
                });
    }
}
