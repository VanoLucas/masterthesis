package com.vanolucas.opti.eval;

import com.vanolucas.opti.phenotype.APhenotypeWrap;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.solution.Solution;

public interface AEvaluationFunction {
    default Quality evaluate(Solution solution) {
        return evaluate(solution.getPhenotype());
    }

    default Quality evaluate(APhenotypeWrap phenotypeWrap) {
        return evaluate(phenotypeWrap.get());
    }

    Quality evaluate(Object phenotype);
}
