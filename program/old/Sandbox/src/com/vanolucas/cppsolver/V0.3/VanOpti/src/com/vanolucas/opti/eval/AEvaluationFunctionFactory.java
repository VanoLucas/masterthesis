package com.vanolucas.opti.eval;

public interface AEvaluationFunctionFactory {
    AEvaluationFunction newEvaluationFunction();
}
