package com.vanolucas.opti.eval;

import com.vanolucas.opti.genome.AGenomeDecoder;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.solution.Individual;
import com.vanolucas.opti.solution.Solution;

public interface AGeneticEvaluationFunction extends AEvaluationFunction, AGenomeDecoder {

    @Override
    default Quality evaluate(Solution solution) {
        Individual indiv = (Individual) solution;
        indiv.decodeGenomeUsing(this);
        return evaluate(solution.getPhenotypeWrap().get());
    }
}
