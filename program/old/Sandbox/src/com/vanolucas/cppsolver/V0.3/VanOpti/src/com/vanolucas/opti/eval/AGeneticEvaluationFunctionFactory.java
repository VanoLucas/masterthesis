package com.vanolucas.opti.eval;

import com.vanolucas.opti.genome.AGenomeDecoderFactory;

public interface AGeneticEvaluationFunctionFactory extends AEvaluationFunctionFactory, AGenomeDecoderFactory {

    @Override
    default AGeneticEvaluationFunction newGenomeDecoder() {
        return newGeneticEvaluationFunction();
    }

    @Override
    default AGeneticEvaluationFunction newEvaluationFunction() {
        return newGeneticEvaluationFunction();
    }

    AGeneticEvaluationFunction newGeneticEvaluationFunction();
}
