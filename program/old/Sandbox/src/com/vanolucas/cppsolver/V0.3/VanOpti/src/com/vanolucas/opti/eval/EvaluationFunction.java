package com.vanolucas.opti.eval;

import com.vanolucas.opti.quality.Quality;

public interface EvaluationFunction<TPhenotype, TQuality extends Quality> extends AEvaluationFunction {

    @Override
    default TQuality evaluate(Object phenotype) {
        return evaluatePhenotype((TPhenotype) phenotype);
    }

    TQuality evaluatePhenotype(TPhenotype phenotype);
}
