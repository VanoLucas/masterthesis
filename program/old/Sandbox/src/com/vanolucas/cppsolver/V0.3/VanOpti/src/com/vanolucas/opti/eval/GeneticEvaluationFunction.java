package com.vanolucas.opti.eval;

import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.genome.GenomeDecoder;
import com.vanolucas.opti.quality.Quality;

public interface GeneticEvaluationFunction<TGenome extends Genome, TPhenotype, TQuality extends Quality>
        extends AGeneticEvaluationFunction, GenomeDecoder<TGenome, TPhenotype> {

    @Override
    default TQuality evaluate(Object phenotype) {
        return evaluatePhenotype((TPhenotype) phenotype);
    }

    TQuality evaluatePhenotype(TPhenotype phenotype);
}
