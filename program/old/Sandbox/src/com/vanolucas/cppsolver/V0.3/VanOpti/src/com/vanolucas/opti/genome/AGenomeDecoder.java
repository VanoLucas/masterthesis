package com.vanolucas.opti.genome;

import com.vanolucas.opti.phenotype.APhenotypeWrap;

public interface AGenomeDecoder {

    default void decode(Genome genome, APhenotypeWrap phenotypeWrap) {
        decodeAGenome(genome, phenotypeWrap.get());
    }

    void decodeAGenome(Genome genome, Object phenotype);
}
