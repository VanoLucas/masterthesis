package com.vanolucas.opti.genome;

public interface AGenomeDecoderFactory {

    AGenomeDecoder newGenomeDecoder();
}
