package com.vanolucas.opti.genome;

import com.vanolucas.opti.random.Randomizable;

import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

public class Double01Genome extends DoubleGenome implements Randomizable {

    private Random random;

    /*
    Constructors
     */

    public Double01Genome(int size) {
        super(size);
    }

    public Double01Genome(List<Double> genes) {
        super(genes);
    }

    /*
    Randomizable
     */

    @Override
    public void randomize() {
        if (random == null) {
            random = new Random();
        }

        final int SIZE = size();
        IntStream.range(0, SIZE)
                .forEach(i -> genes.set(i, random.nextDouble()));
    }
}
