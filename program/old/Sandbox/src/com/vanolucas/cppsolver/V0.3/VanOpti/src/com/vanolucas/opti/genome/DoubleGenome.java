package com.vanolucas.opti.genome;

import com.vanolucas.opti.random.Randomizable;

import java.util.List;

public class DoubleGenome extends ListGenome<Double> {

    private static final double DEFAULT_GENE_VALUE = 0d;

    /*
    Constructors
     */

    public DoubleGenome(int size) {
        super(size, DEFAULT_GENE_VALUE);
    }

    public DoubleGenome(List<Double> genes) {
        super(genes);
    }
}
