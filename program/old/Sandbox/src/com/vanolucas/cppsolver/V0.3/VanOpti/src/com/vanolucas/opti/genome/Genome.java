package com.vanolucas.opti.genome;

import com.vanolucas.opti.util.Copyable;

public interface Genome extends Copyable {

    /*
    Copyable
     */

    @Override
    default void copyFrom(Object other) {
        copyFrom((Genome) other);
    }

    void copyFrom(Genome other);

    /*
    toString
     */

    @Override
    String toString();
}
