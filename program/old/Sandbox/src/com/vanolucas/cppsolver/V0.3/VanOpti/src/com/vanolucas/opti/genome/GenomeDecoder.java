package com.vanolucas.opti.genome;

public interface GenomeDecoder<TGenome extends Genome, TPhenotype> extends AGenomeDecoder {

    @Override
    default void decodeAGenome(Genome genome, Object phenotype) {
        decodeGenome((TGenome) genome, (TPhenotype) phenotype);
    }

    void decodeGenome(TGenome genome, TPhenotype phenotype);
}
