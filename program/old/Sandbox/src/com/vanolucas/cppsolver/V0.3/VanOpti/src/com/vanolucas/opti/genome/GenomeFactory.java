package com.vanolucas.opti.genome;

/**
 * Ability to create new genomes.
 */
public interface GenomeFactory {
    Genome newGenome();
}
