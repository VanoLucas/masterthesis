package com.vanolucas.opti.genome;

import java.util.List;

public class IntGenome extends ListGenome<Integer> {

    private static final int DEFAULT_GENE_VALUE = 0;

    /*
    Constructors
     */

    public IntGenome(int size) {
        super(size, DEFAULT_GENE_VALUE);
    }

    public IntGenome(List<Integer> genes) {
        super(genes);
    }
}
