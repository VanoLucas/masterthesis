package com.vanolucas.opti.genome;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ListGenome<TGene extends Number> implements Genome {

    protected List<TGene> genes;

    /*
    Constructors
     */

    public ListGenome(final int size, final TGene defaultValue) {
        this(new ArrayList<>(Collections.nCopies(size, defaultValue)));
    }

    public ListGenome(List<TGene> genes) {
        this.genes = genes;
    }

    /*
    Accessors
     */

    public int size() {
        return genes.size();
    }

    public List<TGene> getGenes() {
        return genes;
    }

    public List<TGene> getGenes(int fromIndexInclusive, int toIndexExclusive) {
        return genes.subList(fromIndexInclusive, toIndexExclusive);
    }

    public TGene get(final int index) {
        return genes.get(index);
    }

    public void set(final int index, final TGene value) {
        genes.set(index, value);
    }

    /*
    Copyable
     */

    @Override
    public void copyFrom(Genome other) {
        ListGenome<TGene> o = (ListGenome<TGene>) other;

        final int THIS_SIZE = size();

        if (THIS_SIZE != o.size()) {
            genes.clear();
            genes.addAll(o.genes);
        } else {
            IntStream.range(0, THIS_SIZE)
                    .forEach(i -> genes.set(i, o.genes.get(i)));
        }
    }

    /*
    toString
     */

    @Override
    public String toString() {
        return "(" + size() + " genes) " + genes.stream()
                .map(gene -> gene.toString())
                .collect(Collectors.joining(","));
    }
}
