package com.vanolucas.opti.math;

import org.apache.commons.math3.distribution.EnumeratedIntegerDistribution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Randomly choose objects based on discrete probabilities.
 */
public class Picker<TObject> {

    private List<TObject> objects;
    private EnumeratedIntegerDistribution distribution;

    /*
    Constructors
     */

    public Picker() {
        this(new ArrayList<>(), new double[0]);
    }
    public Picker(List<TObject> objectsToPickFrom, List<Double> probabilities) {
        set(objectsToPickFrom, probabilities);
    }
    public Picker(List<TObject> objectsToPickFrom, double[] probabilities) {
        set(objectsToPickFrom, probabilities);
    }

    /*
    Accessors
     */

    public void setProbabilities(List<Double> probabilities) {
        set(objects, probabilities.stream().mapToDouble(p -> p).toArray());
    }

    public void set(List<TObject> objectsToPickFrom, List<Double> probabilities) {
        set(objectsToPickFrom, probabilities.stream().mapToDouble(p -> p).toArray());
    }

    public void set(List<TObject> objectsToPickFrom, double[] probabilities) {
        objects = objectsToPickFrom;
        int[] indexes = IntStream.range(0, objects.size()).toArray();
        distribution = new EnumeratedIntegerDistribution(indexes, probabilities);
    }

    /*
    Commands
     */

    public TObject pickOne() {
        return objects.get(distribution.sample());
    }

    public List<TObject> pickN(final int count) {
        int[] picked = distribution.sample(count);
        return Arrays.stream(picked)
                .mapToObj(index -> objects.get(index))
                .collect(Collectors.toList());
    }

}
