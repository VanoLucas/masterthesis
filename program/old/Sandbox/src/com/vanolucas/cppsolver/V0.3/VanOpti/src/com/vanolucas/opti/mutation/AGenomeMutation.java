package com.vanolucas.opti.mutation;

import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.solution.Individual;
import com.vanolucas.opti.solution.Solution;

public interface AGenomeMutation extends AMutation {

    @Override
    default void mutate(Solution solution) {
        mutate(((Individual) solution).getGenome());
    }

    void mutate(Genome genome);
}
