package com.vanolucas.opti.mutation;

import com.vanolucas.opti.solution.Solution;

public interface AMutation {

    void mutate(Solution solution);
}
