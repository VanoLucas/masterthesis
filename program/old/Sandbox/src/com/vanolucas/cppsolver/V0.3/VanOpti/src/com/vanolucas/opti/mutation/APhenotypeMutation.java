package com.vanolucas.opti.mutation;

import com.vanolucas.opti.solution.Solution;

public interface APhenotypeMutation extends AMutation {

    @Override
    default void mutate(Solution solution) {
        mutate(solution.getPhenotype());
    }

    void mutate(Object phenotype);
}
