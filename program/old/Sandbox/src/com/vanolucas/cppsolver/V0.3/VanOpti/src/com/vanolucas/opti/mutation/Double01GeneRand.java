package com.vanolucas.opti.mutation;

import com.vanolucas.opti.genome.ListGenome;

import java.util.Random;

public class Double01GeneRand implements SingleGeneMutation<Double> {

    /*
    Attributes
     */

    protected Random random;

    /*
    GenomeMutation
     */

    @Override
    public void mutateGene(ListGenome<Double> genome, int index) {
        if (random == null) {
            random = new Random();
        }
        genome.set(index, random.nextDouble());
    }
}
