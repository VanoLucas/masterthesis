package com.vanolucas.opti.mutation;

import com.vanolucas.opti.genome.Genome;

public interface GenomeMutation<TGenome extends Genome> extends AGenomeMutation {

    @Override
    default void mutate(Genome genome) {
        mutateGenome((TGenome) genome);
    }

    void mutateGenome(TGenome genome);
}
