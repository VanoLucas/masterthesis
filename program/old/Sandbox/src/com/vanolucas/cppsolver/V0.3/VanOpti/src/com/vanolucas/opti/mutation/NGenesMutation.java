package com.vanolucas.opti.mutation;

import com.vanolucas.opti.genome.ListGenome;

import java.util.Random;
import java.util.stream.IntStream;

public class NGenesMutation<TGene extends Number> implements GenomeMutation<ListGenome<TGene>> {

    /*
    Attributes
     */

    private SingleGeneMutation<TGene> singleGeneMutation;
    private final int nbGeneMutations;

    private Random random;

    /*
    Constructors
     */

    public NGenesMutation(SingleGeneMutation<TGene> singleGeneMutation, final int nbGeneMutations) {
        this.singleGeneMutation = singleGeneMutation;
        this.nbGeneMutations = nbGeneMutations;
    }


    /*
    GenomeMutation
     */

    @Override
    public void mutateGenome(ListGenome<TGene> genome) {
        if (random == null) {
            random = new Random();
        }

        final int SIZE = genome.size();
        final int MUTATIONS_TO_DO = nbGeneMutations;

        IntStream.range(0, MUTATIONS_TO_DO)
                .forEach(mutation -> {
                    singleGeneMutation.mutateGene(genome, random.nextInt(SIZE));
                });
    }

}
