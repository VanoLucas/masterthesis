package com.vanolucas.opti.mutation;

public interface PhenotypeMutation<TPhenotype> extends APhenotypeMutation {

    @Override
    default void mutate(Object phenotype) {
        mutatePhenotype((TPhenotype) phenotype);
    }

    void mutatePhenotype(TPhenotype phenotype);
}
