package com.vanolucas.opti.mutation;

import com.vanolucas.opti.genome.ListGenome;

import java.util.Random;
import java.util.stream.IntStream;

public class RandomGenesMutation<TGene extends Number> implements GenomeMutation<ListGenome<TGene>> {

    /*
    Attributes
     */

    private SingleGeneMutation<TGene> singleGeneMutation;
    private double geneMutationProba;

    private Random random;

    /*
    Constructors
     */

    public RandomGenesMutation(SingleGeneMutation<TGene> singleGeneMutation, final double geneMutationProba) {
        this.singleGeneMutation = singleGeneMutation;
        this.geneMutationProba = geneMutationProba;
    }

    /*
    GenomeMutation
     */

    @Override
    public void mutateGenome(ListGenome<TGene> genome) {
        if (random == null) {
            random = new Random();
        }

        final int SIZE = genome.size();

        IntStream.range(0, SIZE)
                .forEach(geneIndex -> {
                    if (random.nextDouble() < geneMutationProba) {
                        singleGeneMutation.mutateGene(genome, geneIndex);
                    }
                });
    }
}
