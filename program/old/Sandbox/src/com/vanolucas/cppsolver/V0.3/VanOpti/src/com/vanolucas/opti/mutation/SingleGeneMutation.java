package com.vanolucas.opti.mutation;

import com.vanolucas.opti.genome.ListGenome;

public interface SingleGeneMutation<TGene extends Number> {

    void mutateGene(ListGenome<TGene> genome, final int geneIndex);
}
