package com.vanolucas.opti.mutation.collection;

import com.vanolucas.opti.mutation.AGenomeMutation;
import com.vanolucas.opti.mutation.AMutation;
import com.vanolucas.opti.util.ObjWithProba;

import java.util.List;
import java.util.stream.Collectors;

public interface GeneticMutationOperatorPickerFactory extends MutationOperatorPickerFactory, GeneticMutationOperatorsFactory {
    default MutationOperatorPicker newGeneticMutationOperatorPicker() {
        return newMutationOperatorPicker();
    }

    @Override
    default MutationOperatorPicker newMutationOperators() {
        return MutationOperatorPickerFactory.super.newMutationOperators();
    }

    @Override
    default List<AMutation> newMutationOperatorsList() {
        return MutationOperatorPickerFactory.super.newMutationOperatorsList();
    }

    @Override
    default List<AGenomeMutation> newGeneticMutationOperators() {
        return newGeneticMutationOperatorsWithProba().stream()
                .map(ObjWithProba::getObj)
                .collect(Collectors.toList());
    }

    @Override
    default List<ObjWithProba<AMutation>> newMutationOperatorsWithProba() {
        return newGeneticMutationOperatorsWithProba().stream()
                .map(mutation -> new ObjWithProba<AMutation>(mutation.getObj(), mutation.getProba()))
                .collect(Collectors.toList());
    }

    List<ObjWithProba<AGenomeMutation>> newGeneticMutationOperatorsWithProba();
}
