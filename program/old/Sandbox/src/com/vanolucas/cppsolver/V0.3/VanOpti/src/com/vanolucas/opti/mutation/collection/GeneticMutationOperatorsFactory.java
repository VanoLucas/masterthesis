package com.vanolucas.opti.mutation.collection;

import com.vanolucas.opti.mutation.AGenomeMutation;
import com.vanolucas.opti.mutation.AMutation;

import java.util.List;
import java.util.stream.Collectors;

public interface GeneticMutationOperatorsFactory extends MutationOperatorsFactory {

    @Override
    default MutationOperators newMutationOperators() {
        return new MutationOperators(newMutationOperatorsList());
    }

    @Override
    default List<AMutation> newMutationOperatorsList() {
        return newGeneticMutationOperators().stream()
                .map(geneticMutation -> (AMutation) geneticMutation)
                .collect(Collectors.toList());
    }

    List<AGenomeMutation> newGeneticMutationOperators();
}
