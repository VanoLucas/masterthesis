package com.vanolucas.opti.mutation.collection;

import com.vanolucas.opti.math.Picker;
import com.vanolucas.opti.mutation.AMutation;
import com.vanolucas.opti.solution.Solution;
import com.vanolucas.opti.util.ObjWithProba;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class MutationOperatorPicker extends MutationOperators {

    private Picker<AMutation> mutationPicker;

    /*
    Constructors
     */

    public MutationOperatorPicker(List<ObjWithProba<AMutation>> mutationOperatorsWithProba) {
        this(
                // get mutation operators
                mutationOperatorsWithProba.stream()
                        .map(ObjWithProba::getObj)
                        .collect(Collectors.toList()),
                // get their corresponding probability
                mutationOperatorsWithProba.stream()
                        .mapToDouble(ObjWithProba::getProba)
                        .toArray()
        );
    }

    public MutationOperatorPicker(List<AMutation> mutationOperators, double[] probabilities) {
        super(mutationOperators);
        mutationPicker = new Picker<>(mutationOperators, probabilities);
    }

    /*
    Commands
     */

    @Override
    public void mutate(List<Solution> solutions) {
        solutions.forEach(this::mutate);
    }

    public void mutate(Solution solution) {
        solution.mutateUsing(mutationPicker.pickOne());
    }

    public void mutateParallel(List<Solution> solutions) {
        solutions.parallelStream().forEach(solution -> {
            AMutation mutationToUse;
            synchronized (mutationPicker) {
                mutationToUse = mutationPicker.pickOne();
            }
            synchronized (mutationToUse) {
                solution.mutateUsing(mutationToUse);
            }
        });
    }
}
