package com.vanolucas.opti.mutation.collection;

import com.vanolucas.opti.mutation.AMutation;
import com.vanolucas.opti.util.ObjWithProba;

import java.util.List;
import java.util.stream.Collectors;

public interface MutationOperatorPickerFactory extends MutationOperatorsFactory {

    @Override
    default MutationOperatorPicker newMutationOperators() {
        return newMutationOperatorPicker();
    }

    default MutationOperatorPicker newMutationOperatorPicker() {
        return new MutationOperatorPicker(newMutationOperatorsWithProba());
    }

    @Override
    default List<AMutation> newMutationOperatorsList() {
        return newMutationOperatorsWithProba().stream()
                .map(ObjWithProba::getObj)
                .collect(Collectors.toList());
    }

    List<ObjWithProba<AMutation>> newMutationOperatorsWithProba();
}
