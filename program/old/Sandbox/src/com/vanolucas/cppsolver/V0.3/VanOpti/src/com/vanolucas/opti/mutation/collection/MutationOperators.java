package com.vanolucas.opti.mutation.collection;

import com.vanolucas.opti.mutation.AMutation;
import com.vanolucas.opti.solution.Solution;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class MutationOperators {

    /*
    Attributes
     */

    protected List<AMutation> mutationOperators;

    /*
    Constructors
     */

    public MutationOperators() {
        this(new ArrayList<>(2));
    }
    public MutationOperators(List<AMutation> mutationOperators) {
        this.mutationOperators = mutationOperators;
    }

    /*
    Accessors
     */

    public int size() {
        return mutationOperators.size();
    }

    /*
    Commands
     */

    public void addMutation(AMutation mutation) {
        mutationOperators.add(mutation);
    }

    public void mutate(List<Solution> solutions) {
        final int NB_MUTATION_OPERATORS = mutationOperators.size();

        // mutate each solution with a mutation operator
        IntStream.range(0, solutions.size())
                .forEach(i -> solutions.get(i)
                        .mutateUsing(mutationOperators.get(i % NB_MUTATION_OPERATORS)));
    }

    public void mutateParallel(List<Solution> solutions) {
        final int NB_MUTATION_OPERATORS = mutationOperators.size();

        // mutate each solution with a mutation operator
        IntStream.range(0, solutions.size())
                .parallel()
                .forEach(i -> solutions.get(i)
                        .mutateUsing(mutationOperators.get(i % NB_MUTATION_OPERATORS)));
    }
}
