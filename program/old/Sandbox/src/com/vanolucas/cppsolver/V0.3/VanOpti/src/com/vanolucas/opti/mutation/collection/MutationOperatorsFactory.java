package com.vanolucas.opti.mutation.collection;

import com.vanolucas.opti.mutation.AMutation;

import java.util.List;

public interface MutationOperatorsFactory {

    default MutationOperators newMutationOperators() {
        return new MutationOperators(newMutationOperatorsList());
    }

    List<AMutation> newMutationOperatorsList();
}
