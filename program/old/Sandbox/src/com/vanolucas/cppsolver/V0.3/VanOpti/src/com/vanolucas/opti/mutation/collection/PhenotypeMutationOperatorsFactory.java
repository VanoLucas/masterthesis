package com.vanolucas.opti.mutation.collection;

import com.vanolucas.opti.mutation.AMutation;
import com.vanolucas.opti.mutation.APhenotypeMutation;

import java.util.List;
import java.util.stream.Collectors;

public interface PhenotypeMutationOperatorsFactory extends MutationOperatorsFactory {

    @Override
    default MutationOperators newMutationOperators() {
        return new MutationOperators(newMutationOperatorsList());
    }

    @Override
    default List<AMutation> newMutationOperatorsList() {
        return newPhenotypeMutationOperators().stream()
                .map(phenotypeMutation -> (AMutation) phenotypeMutation)
                .collect(Collectors.toList());
    }

    List<APhenotypeMutation> newPhenotypeMutationOperators();
}
