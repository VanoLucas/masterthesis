package com.vanolucas.opti.phenotype;

import com.vanolucas.opti.util.Copyable;

public class ACopyablePhenotypeWrap extends APhenotypeWrap implements Copyable {

    /*
    Constructors
     */

    public ACopyablePhenotypeWrap(Copyable phenotype) {
        super(phenotype);
    }

    /*
    Accessors
     */

    public Copyable get() {
        return (Copyable) phenotype;
    }

    public void set(Copyable phenotype) {
        this.phenotype = phenotype;
    }


    /*
    Copyable
     */

    @Override
    public void copyFrom(Object other) {
        copyFrom((ACopyablePhenotypeWrap) other);
    }

    public void copyFrom(ACopyablePhenotypeWrap other) {
        ((Copyable) phenotype).copyFrom(other.phenotype);
    }

}
