package com.vanolucas.opti.phenotype;

import com.vanolucas.opti.util.Copyable;

public interface ACopyablePhenotypeWrapFactory extends APhenotypeWrapFactory, CopyablePhenotypeFactory {

    @Override
    default ACopyablePhenotypeWrap newPhenotypeWrap() {
        return newCopyablePhenotypeWrap();
    }

    default ACopyablePhenotypeWrap newCopyablePhenotypeWrap() {
        return new ACopyablePhenotypeWrap(newPhenotype());
    }
}
