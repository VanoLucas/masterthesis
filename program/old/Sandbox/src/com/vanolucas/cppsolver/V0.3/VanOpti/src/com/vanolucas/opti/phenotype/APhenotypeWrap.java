package com.vanolucas.opti.phenotype;

/**
 * Abstracts a phenotype.
 * A phenotype can be of any type.
 * A phenotype is what the user considers as a solution.
 */
public class APhenotypeWrap {

    /**
     * The actual phenotype object.
     */
    protected Object phenotype;

    /*
    Constructors
     */

    public APhenotypeWrap(Object phenotype) {
        this.phenotype = phenotype;
    }

    /*
    Accessors
     */

    public Object get() {
        return phenotype;
    }

    public void set(Object phenotype) {
        this.phenotype = phenotype;
    }

    /*
    toString
     */

    @Override
    public String toString() {
        return phenotype.toString();
    }
}
