package com.vanolucas.opti.phenotype;

public interface APhenotypeWrapFactory extends PhenotypeFactory {

    default APhenotypeWrap newPhenotypeWrap() {
        return new APhenotypeWrap(newPhenotype());
    }
}
