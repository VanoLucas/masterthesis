package com.vanolucas.opti.phenotype;

import com.vanolucas.opti.util.Copyable;

public interface CopyablePhenotypeFactory extends PhenotypeFactory {

    @Override
    Copyable newPhenotype();
}
