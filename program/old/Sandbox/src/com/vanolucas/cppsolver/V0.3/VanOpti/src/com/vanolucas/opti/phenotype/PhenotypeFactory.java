package com.vanolucas.opti.phenotype;

/**
 * Ability to create new phenotype objects.
 * Phenotype objects can be of any type.
 * The phenotype is the users' solution object.
 */
public interface PhenotypeFactory {

    Object newPhenotype();
}
