package com.vanolucas.opti.problem;

import com.vanolucas.opti.algo.evolutionary.GeneticEvolutionaryAlgoFactory;
import com.vanolucas.opti.crossover.ACrossover;
import com.vanolucas.opti.crossover.UniformCrossover2Solutions;
import com.vanolucas.opti.eval.GeneticEvaluationFunction;
import com.vanolucas.opti.genome.IntGenome;
import com.vanolucas.opti.mutation.AGenomeMutation;
import com.vanolucas.opti.mutation.AMutation;
import com.vanolucas.opti.mutation.NGenesMutation;
import com.vanolucas.opti.quality.LongCost;
import com.vanolucas.opti.random.Randomizable;
import com.vanolucas.opti.random.genome.GenomeRandomizer;
import com.vanolucas.opti.selection.RandomSelection;
import com.vanolucas.opti.selection.SelectionOperator;
import com.vanolucas.opti.util.ObjWithProba;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

/**
 * Simple sample problem of finding the target number.
 */
public class TargetNumberProblem implements GeneticEvolutionaryAlgoFactory<TargetNumberProblem.MyGenome, TargetNumberProblem.MyNumber, LongCost> {

    private final int TARGET_NUMBER;

    /**
     * Phenotype
     */
    class MyNumber {
        private int value;

        public MyNumber(int value) {
            this.value = value;
        }

        public int get() {
            return value;
        }

        public void set(int value) {
            this.value = value;
        }
    }

    /**
     * Genome
     */
    class MyGenome extends IntGenome implements Randomizable {
        private Random random;

        public MyGenome() {
            super(10);
            this.random = new Random();
        }

        @Override
        public void randomize() {
            IntStream.range(0, size())
                    .forEach(i -> genes.set(i, random.nextInt()));
        }
    }

    /*
    Constructor
     */

    public TargetNumberProblem(final int targetNumber) {
        this.TARGET_NUMBER = targetNumber;
    }

    /*
    Opti Factories
     */

    @Override
    public MyGenome newGenome() {
        MyGenome genome = new MyGenome();
        genome.randomize();
        return genome;
    }

    @Override
    public MyNumber newPhenotype() {
        return new MyNumber(Integer.MAX_VALUE);
    }

    @Override
    public LongCost newQuality() {
        return new LongCost();
    }

    @Override
    public GenomeRandomizer<MyGenome> newGenomeRandomizer() {
        return MyGenome::randomize;
    }

    @Override
    public SelectionOperator newSelectionOperator() {
        return new RandomSelection(2);
    }

    @Override
    public List<ObjWithProba<ACrossover>> newCrossoverOperatorsWithProba() {
        return new ArrayList<ObjWithProba<ACrossover>>() {{
            add(new ObjWithProba<>(new UniformCrossover2Solutions()));
        }};
    }

    @Override
    public List<ObjWithProba<AGenomeMutation>> newGeneticMutationOperatorsWithProba() {
        return new ArrayList<ObjWithProba<AGenomeMutation>>() {{
//            add(new ObjWithProba<AGenomeMutation>(new NGenesMutation<>(new )));
        }};
    }

    @Override
    public GeneticEvaluationFunction<MyGenome, MyNumber, LongCost> newGeneticEvaluationFunction() {
        return null;
    }
}
