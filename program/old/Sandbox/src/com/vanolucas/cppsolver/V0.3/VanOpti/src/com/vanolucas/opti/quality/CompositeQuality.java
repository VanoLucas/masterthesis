package com.vanolucas.opti.quality;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CompositeQuality implements Quality {

    /**
     * Components of this composite quality.
     */
    private List<Quality> components;

    /*
    Constructors
     */

    public CompositeQuality(int size) {
        this.components = new ArrayList<>(size);
    }

    public CompositeQuality(List<Quality> compositeQualities) {
        this.components = compositeQualities;
    }

    /*
    Accessors
     */

    @Override
    public CompositeQuality set(Object components) {
        return setComponents((List<Quality>) components);
    }

    public CompositeQuality setComponents(List<Quality> components) {
        this.components = components;
        return this;
    }

    @Override
    public CompositeQuality setToWorst() {
        components.forEach(Quality::setToWorst);
        return this;
    }

    /*
    Quality comparison
     */

    @Override
    public boolean isBetterThan(Quality other) {
        CompositeQuality o = (CompositeQuality) other;

        final int NB_QUALITIES = components.size();
        boolean atLeastOneBetter = false;
        // for each composite quality
        for (int i = 0; i < NB_QUALITIES; i++) {
            // are all components at least equivalent?
            // if any quality component is worse than the other, we're not better
            if (this.components.get(i).isWorseThan(o.components.get(i))) {
                return false;
            }
            // we need at least one component to be better than the other to be better overall
            else if (!atLeastOneBetter && this.components.get(i).isBetterThan(o.components.get(i))) {
                atLeastOneBetter = true;
            }
        }
        return atLeastOneBetter;
    }

    @Override
    public boolean isWorseThan(Quality other) {
        CompositeQuality o = (CompositeQuality) other;

        final int NB_QUALITIES = components.size();
        boolean atLeastOneWorse = false;
        // for each composite quality
        for (int i = 0; i < NB_QUALITIES; i++) {
            // if any quality component is better than the other, we're not worse
            if (this.components.get(i).isBetterThan(o.components.get(i))) {
                return false;
            }
            // we need at least one component to be worse than the other to be worse overall
            else if (!atLeastOneWorse && this.components.get(i).isWorseThan(o.components.get(i))) {
                atLeastOneWorse = true;
            }
        }
        return atLeastOneWorse;
    }

    @Override
    public boolean isEquivalentTo(Quality o) {
        return !this.dominates(o) && !this.isDominatedBy(o);
    }

    @Override
    public double differenceWith(Quality other) {
        CompositeQuality o = (CompositeQuality) other;
        int nbComponents = Math.min(components.size(), o.components.size());
        double sum = 0.0d;
        for (int i = 0; i < nbComponents; i++) {
            sum += components.get(i).differenceWith(o.components.get(i));
        }
        return sum;
    }

    @Override
    public boolean equals(Object other) {
        return isEquivalentTo(((CompositeQuality) other));
    }

    /*
    DeepCloneable
     */

    @Override
    public CompositeQuality deepClone() {
        return new CompositeQuality(
                components.stream()
                        .map(Quality::deepClone)
                        .collect(Collectors.toList())
        );
    }

    @Override
    public void copyFrom(Object other) {
        CompositeQuality o = (CompositeQuality) other;
        components.clear();
        components.addAll(
                o.components.stream()
                        .map(Quality::deepClone)
                        .collect(Collectors.toList())
        );
    }

    /*
    toString
     */

    @Override
    public String toString() {
        return components.stream()
                .map(Quality::toString)
                .collect(Collectors.joining(","));
    }
}