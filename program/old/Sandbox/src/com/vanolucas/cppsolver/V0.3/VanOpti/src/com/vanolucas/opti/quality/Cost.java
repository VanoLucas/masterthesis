package com.vanolucas.opti.quality;

public abstract class Cost<T extends Number & Comparable<T>>
        extends SingleValueQuality<T> {

    /*
    Constructors
     */

    public Cost(Cost<T> o) {
        this(o.value);
    }
    public Cost(T value) {
        super(value);
    }

    /*
    Quality comparison
     */

    /**
     * Compare to another Cost.
     * A lower cost is better.
     * @param other The other Cost to compare this one to.
     * @return True if this cost is better than the other one, false if worse or equal.
     */
    @Override
    public boolean isBetterThan(Quality other) {
        Cost<T> o = (Cost<T>) other;
        return this.value.compareTo(o.value) < 0;
    }
}
