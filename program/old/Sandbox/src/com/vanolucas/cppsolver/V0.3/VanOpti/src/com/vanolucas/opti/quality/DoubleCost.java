package com.vanolucas.opti.quality;

public class DoubleCost extends Cost<Double> {

    private static final double WORST_VALUE = Double.MAX_VALUE;

    /*
    Constructors
     */

    public DoubleCost() {
        super(WORST_VALUE);
    }
    public DoubleCost(Cost<Double> other) {
        super(other);
    }
    public DoubleCost(double cost) {
        super(cost);
    }

    /*
    Accessors
     */

    public double doubleValue() {
        return this.value;
    }

    @Override
    public DoubleCost setToWorst() {
        return (DoubleCost) set(WORST_VALUE);
    }

    /*
    Quality comparison
     */

    @Override
    public double differenceWith(Quality other) {
        DoubleCost o = (DoubleCost) other;
        return (this.value - o.value) * (this.value - o.value);
    }

    /*
    DeepCloneable
     */

    @Override
    public DoubleCost deepClone() {
        return new DoubleCost(this);
    }
}
