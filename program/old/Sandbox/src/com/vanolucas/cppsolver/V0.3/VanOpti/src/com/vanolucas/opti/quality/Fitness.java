package com.vanolucas.opti.quality;

public abstract class Fitness<T extends Number & Comparable<T>>
        extends SingleValueQuality<T> {

    /*
    Constructors
     */

    public Fitness(Fitness<T> o) {
        this(o.value);
    }
    public Fitness(T value) {
        super(value);
    }

    /*
    Quality comparison
     */

    /**
     * Compare to another Fitness.
     * A higher fitness is better.
     * @param other The other Fitness to compare this one to.
     * @return True if this fitness is better than the other one, false if worse or equal.
     */
    @Override
    public boolean isBetterThan(Quality other) {
        Fitness<T> o = (Fitness<T>) other;
        return this.value.compareTo(o.value) > 0;
    }
}
