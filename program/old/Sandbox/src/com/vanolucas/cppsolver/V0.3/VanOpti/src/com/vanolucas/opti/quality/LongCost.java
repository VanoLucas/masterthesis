package com.vanolucas.opti.quality;

public class LongCost extends Cost<Long> {

    private static final long WORST_VALUE = Long.MAX_VALUE;

    /*
    Constructors
     */

    public LongCost() {
        super(WORST_VALUE);
    }
    public LongCost(Cost<Long> other) {
        super(other);
    }
    public LongCost(long cost) {
        super(cost);
    }

    /*
    Accessors
     */

    public double doubleValue() {
        return this.value.doubleValue();
    }

    @Override
    public LongCost setToWorst() {
        return (LongCost) set(WORST_VALUE);
    }

    /*
    Quality comparison
     */

    @Override
    public double differenceWith(Quality other) {
        LongCost o = (LongCost) other;
        return (this.value - o.value) * (this.value - o.value);
    }

    /*
    DeepCloneable
     */

    @Override
    public LongCost deepClone() {
        return new LongCost(this);
    }
}
