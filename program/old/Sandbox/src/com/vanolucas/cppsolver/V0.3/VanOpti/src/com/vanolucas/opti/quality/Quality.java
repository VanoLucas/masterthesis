package com.vanolucas.opti.quality;

import com.vanolucas.opti.util.DeepCloneable;

public interface Quality extends Comparable<Quality>, DeepCloneable {

    /*
    Accessors
     */

    Quality set(Object qualityValue);

    /**
     * Sets this quality to its worst possible value.
     */
    Quality setToWorst();

    /*
    Quality comparison
     */

    /**
     * Override this to define how a Quality is better than another.
     *
     * @param other Other Quality to compare this one to.
     * @return True if this Quality is better than the other one.
     */
    boolean isBetterThan(Quality other);

    /**
     * Override this to define how a Quality is equivalent to another.
     *
     * @param other Other Quality to compare this one to.
     * @return True if this Quality is equivalent to the other one.
     */
    boolean isEquivalentTo(Quality other);


    default boolean isEquivalentOrBetterThan(Quality other) {
        return isBetterThan(other) || isEquivalentTo(other);
    }

    default boolean isWorseOrEquivalentTo(Quality other) {
        return !isBetterThan(other);
    }

    default boolean isWorseThan(Quality other) {
        return !isEquivalentOrBetterThan(other);
    }

    default boolean dominates(Quality other) {
        return isBetterThan(other);
    }

    default boolean isDominatedBy(Quality other) {
        return isWorseThan(other);
    }


    /**
     * Returns the Squared Euclidean distance between this Quality and the one provided as arg.
     *
     * @param other The other Quality we want to get the distance with.
     * @return The Squared Euclidean distance between this Quality and the one provided as arg.
     */
    double differenceWith(Quality other);


    @Override
    boolean equals(Object other);


    /*
    Comparable
     */

    @Override
    default int compareTo(Quality other) {
        if (this.isBetterThan(other)) {
            return 1;
        } else if (this.isEquivalentTo(other)) {
            return 0;
        } else {
            return -1;
        }
    }

    /*
    DeepCloneable
     */

    @Override
    Quality deepClone();

    /*
    toString
     */

    @Override
    String toString();
}
