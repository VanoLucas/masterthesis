package com.vanolucas.opti.quality;

public interface QualityFactory {

    Quality newQuality();
}
