package com.vanolucas.opti.quality;

/**
 * Quality of a solution expressed as a single value.
 * @param <T> Type of the quality value.
 */
public abstract class SingleValueQuality<T> implements Quality {

    /**
     * The single value of this Quality.
     */
    protected T value;

    /*
    Constructor
     */

    public SingleValueQuality(T value) {
        this.value = value;
    }

    /*
    Accessors
     */

    public T get() {
        return value;
    }

    @Override
    public SingleValueQuality<T> set(Object value) {
        return setValue((T) value);
    }

    public SingleValueQuality<T> setValue(T value) {
        this.value = value;
        return this;
    }

    /*
    Quality comparison
     */

    @Override
    public boolean isEquivalentTo(Quality other) {
        SingleValueQuality<T> o = (SingleValueQuality<T>) other;
        return this.value.equals(o.value);
    }

    @Override
    public boolean equals(Object other) {
        return isEquivalentTo(((SingleValueQuality<T>) other));
    }

    /*
    Copyable (DeepCloneable)
     */

    @Override
    public void copyFrom(Object other) {
        SingleValueQuality<T> o = (SingleValueQuality<T>) other;
        this.value = o.value;
    }

    /*
    toString
     */

    @Override
    public String toString() {
        return value.toString();
    }
}
