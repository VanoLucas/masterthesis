package com.vanolucas.opti.random.genome;

import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.random.solution.IndividualRandomizer;
import com.vanolucas.opti.solution.Individual;

/**
 * Ability to randomize a given genome.
 */

// todo make inner class of genome?
public interface AGenomeRandomizer extends IndividualRandomizer {

    @Override
    default void randomize(Individual individual) {
        randomize(individual.getGenome());
    }

    void randomize(Genome genome);
}
