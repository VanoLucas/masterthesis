package com.vanolucas.opti.random.genome;

import com.vanolucas.opti.random.solution.SolutionRandomizerFactory;

public interface AGenomeRandomizerFactory extends SolutionRandomizerFactory {

    @Override
    default AGenomeRandomizer newSolutionRandomizer() {
        return newGenomeRandomizer();
    }

    AGenomeRandomizer newGenomeRandomizer();
}
