package com.vanolucas.opti.random.genome;

import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.random.genome.AGenomeRandomizer;

public interface GenomeRandomizer<TGenome extends Genome> extends AGenomeRandomizer {

    @Override
    default void randomize(Genome genome) {
        randomizeGenome((TGenome) genome);
    }

    void randomizeGenome(TGenome genome);
}
