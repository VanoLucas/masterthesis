package com.vanolucas.opti.random.phenotype;

import com.vanolucas.opti.phenotype.APhenotypeWrap;
import com.vanolucas.opti.random.solution.SolutionRandomizer;
import com.vanolucas.opti.solution.PhenotypeSolution;
import com.vanolucas.opti.solution.Solution;

/**
 * Ability to randomize a given phenotype.
 */
public interface APhenotypeRandomizer extends SolutionRandomizer {

    @Override
    default void randomize(Solution solution) {
        randomize(((PhenotypeSolution) solution));
    }

    default void randomize(PhenotypeSolution solution) {
        randomize(solution.getPhenotypeWrap());
    }

    void randomize(APhenotypeWrap phenotype);
}
