package com.vanolucas.opti.random.phenotype;

import com.vanolucas.opti.random.solution.SolutionRandomizerFactory;

public interface APhenotypeRandomizerFactory extends SolutionRandomizerFactory {

    @Override
    default APhenotypeRandomizer newSolutionRandomizer() {
        return newPhenotypeRandomizer();
    }

    APhenotypeRandomizer newPhenotypeRandomizer();
}
