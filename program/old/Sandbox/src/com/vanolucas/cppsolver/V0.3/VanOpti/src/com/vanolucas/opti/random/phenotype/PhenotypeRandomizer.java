package com.vanolucas.opti.random.phenotype;

import com.vanolucas.opti.phenotype.APhenotypeWrap;
import com.vanolucas.opti.random.phenotype.APhenotypeRandomizer;

public interface PhenotypeRandomizer<TPhenotype> extends APhenotypeRandomizer {

    @Override
    default void randomize(APhenotypeWrap phenotype) {
        randomize((TPhenotype) phenotype.get());
    }

    void randomize(TPhenotype phenotype);
}
