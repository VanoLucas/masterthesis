package com.vanolucas.opti.random.solution;

import com.vanolucas.opti.solution.Individual;
import com.vanolucas.opti.solution.Solution;

public interface IndividualRandomizer extends SolutionRandomizer {

    @Override
    default void randomize(Solution solution) {
        randomize(((Individual) solution));
    }

    void randomize(Individual individual);
}
