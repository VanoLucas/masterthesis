package com.vanolucas.opti.random.solution;

public interface IndividualRandomizerFactory extends SolutionRandomizerFactory {

    @Override
    default IndividualRandomizer newSolutionRandomizer() {
        return newIndividualRandomizer();
    }

    IndividualRandomizer newIndividualRandomizer();
}
