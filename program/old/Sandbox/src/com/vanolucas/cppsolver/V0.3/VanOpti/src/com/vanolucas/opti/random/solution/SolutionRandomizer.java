package com.vanolucas.opti.random.solution;

import com.vanolucas.opti.solution.Solution;

public interface SolutionRandomizer {
    void randomize(Solution solution);
}
