package com.vanolucas.opti.random.solution;

public interface SolutionRandomizerFactory {
    SolutionRandomizer newSolutionRandomizer();
}
