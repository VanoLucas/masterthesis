package com.vanolucas.opti.selection;

import com.vanolucas.opti.solution.Solution;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class RandomSelection implements SelectionOperator {

    /*
    Defaults
     */

    private static final int DEFAULT_GROUP_SIZE = 2;

    /*
    Attributes
     */

    private int groupSize;
    private Random random;

    /*
    Constructors
     */

    public RandomSelection() {
        this(DEFAULT_GROUP_SIZE);
    }

    public RandomSelection(final int groupSize) {
        this.groupSize = groupSize;
    }

    /*
    SelectionOperator
     */

    @Override
    public List<List<Solution>> selection(List<Solution> population) {
        // init random if needed
        if (random == null) {
            random = new Random();
        }

        final int SIZE = population.size();

        // calculate nb of mating groups to generate
        final int NB_GROUPS = (int) Math.ceil((double) SIZE / (double) groupSize);

        // generate random mating groups
        return IntStream.range(0, NB_GROUPS)
                // pick random solutions in each mating group
                .mapToObj(groupIndex -> IntStream.range(0, groupSize)
                        .mapToObj(solutionIndex -> population.get(random.nextInt(SIZE)))
                        .collect(Collectors.toList())
                )
                .collect(Collectors.toList());
    }
}
