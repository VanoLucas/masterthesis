package com.vanolucas.opti.selection;

import com.vanolucas.opti.solution.Population;
import com.vanolucas.opti.solution.Solution;

import java.util.List;

public interface SelectionOperator {

    default List<List<Solution>> selection(Population population) {
        return selection(population.get());
    }

    List<List<Solution>> selection(List<Solution> population);
}
