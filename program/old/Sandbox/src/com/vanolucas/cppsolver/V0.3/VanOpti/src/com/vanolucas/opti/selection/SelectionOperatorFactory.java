package com.vanolucas.opti.selection;

public interface SelectionOperatorFactory {
    SelectionOperator newSelectionOperator();
}
