package com.vanolucas.opti.solution;

import com.vanolucas.opti.eval.AEvaluationFunction;
import com.vanolucas.opti.genome.AGenomeDecoder;
import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.phenotype.APhenotypeWrap;
import com.vanolucas.opti.quality.Quality;

public class Individual extends Solution {

    /*
    Attributes
     */

    private Genome genome;

    /*
    Constructors
     */

    public Individual(Genome genome) {
        this(genome, null);
    }

    public Individual(Genome genome, Object phenotype) {
        this(genome, phenotype, null);
    }

    public Individual(Genome genome, APhenotypeWrap phenotypeWrap) {
        this(genome, phenotypeWrap, null);
    }

    public Individual(Genome genome, Object phenotype, Quality quality) {
        this(genome, new APhenotypeWrap(phenotype), quality);
    }

    public Individual(Genome genome, APhenotypeWrap phenotypeWrap, Quality quality) {
        super(phenotypeWrap, quality);
        this.genome = genome;
    }

    /*
    Accessors
     */

    public Genome getGenome() {
        return genome;
    }

    /*
    Commands
     */

    public void decodeGenomeUsing(AGenomeDecoder genomeDecoder) {
        genomeDecoder.decode(genome, phenotypeWrap);
        quality = null; // reset quality
    }

    @Override
    public void reproduceFrom(Solution other) {
        genome.copyFrom(((Individual) other).genome);
    }

    /*
    toString
     */

    @Override
    public String toString() {
        String str = "Genome:";
        String genomeStr = genome.toString();

        // if genome string is short, fit it all on a single line
        if (genomeStr.length() > 80 - "Genome: ".length()) {
            str += "\n" + genomeStr;
        } else {
            str += " " + genomeStr;
        }

        // get string for the phenotype and quality from Solution
        str += "\n" + super.toString();

        return str;
    }
}
