package com.vanolucas.opti.solution;

import com.vanolucas.opti.genome.GenomeFactory;

public interface IndividualFactory extends SolutionFactory, GenomeFactory {

    @Override
    default Individual newSolution() {
        return newIndividual();
    }

    default Individual newIndividual() {
        return new Individual(newGenome(), newPhenotypeWrap(), newQuality());
    }
}
