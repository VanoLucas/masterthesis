package com.vanolucas.opti.solution;

import com.vanolucas.opti.phenotype.ACopyablePhenotypeWrap;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.util.Copyable;

public class PhenotypeSolution extends Solution {

    /*
    Constructors
     */

    public PhenotypeSolution(Object phenotype) {
        super(phenotype);
    }

    public PhenotypeSolution(ACopyablePhenotypeWrap phenotypeWrap) {
        super(phenotypeWrap);
    }

    public PhenotypeSolution(Object phenotype, Quality quality) {
        super(phenotype, quality);
    }

    public PhenotypeSolution(ACopyablePhenotypeWrap phenotypeWrap, Quality quality) {
        super(phenotypeWrap, quality);
    }

    /*
    Accessors
     */

    public ACopyablePhenotypeWrap getPhenotypeWrap() {
        return (ACopyablePhenotypeWrap) phenotypeWrap;
    }

    public Copyable getPhenotype() {
        return (Copyable) phenotypeWrap.get();
    }

    /*
    Commands
     */

    @Override
    public void reproduceFrom(Solution other) {
        getPhenotypeWrap().copyFrom(((PhenotypeSolution) other).getPhenotypeWrap());
    }
}
