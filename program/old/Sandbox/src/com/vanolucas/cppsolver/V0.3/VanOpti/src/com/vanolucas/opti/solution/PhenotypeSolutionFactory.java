package com.vanolucas.opti.solution;

import com.vanolucas.opti.phenotype.ACopyablePhenotypeWrapFactory;

public interface PhenotypeSolutionFactory extends SolutionFactory, ACopyablePhenotypeWrapFactory {

    @Override
    default PhenotypeSolution newSolution() {
        return newPhenotypeSolution();
    }

    default PhenotypeSolution newPhenotypeSolution() {
        return new PhenotypeSolution(newCopyablePhenotypeWrap(), newQuality());
    }
}
