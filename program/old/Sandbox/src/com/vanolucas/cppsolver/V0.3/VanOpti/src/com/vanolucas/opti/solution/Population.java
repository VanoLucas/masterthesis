package com.vanolucas.opti.solution;

import com.vanolucas.opti.eval.AEvaluationFunction;
import com.vanolucas.opti.mutation.AMutation;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.random.solution.SolutionRandomizer;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Population {

    private List<Solution> solutions;
    private SolutionFactory solutionFactory;

    /*
    Constructors
     */

    public Population(final int expectedSize) {
        this.solutions = new ArrayList<>(expectedSize);
    }

    public Population(SolutionFactory solutionFactory) {
        this(solutionFactory, 0);
    }

    public Population(SolutionFactory solutionFactory, final int size) {
        this.solutionFactory = solutionFactory;
        this.solutions = IntStream.range(0, size)
                .mapToObj(i -> solutionFactory.newSolution())
                .collect(Collectors.toList());
    }

    public Population(List<Solution> solutions) {
        this.solutions = solutions;
    }

    /*
    Accessors
     */

    public List<Solution> get() {
        return solutions;
    }

    public Population getNBest(final int count) {
        return new Population(solutions.stream()
                .sorted(Comparator.reverseOrder())
                .limit(count)
                .collect(Collectors.toList())
        );
    }

    public Stream<Solution> stream() {
        return solutions.stream();
    }

    public void forEach(Consumer<? super Solution> action) {
        solutions.forEach(action);
    }

    /*
    Commands
     */

    public void addSolution(Solution solution) {
        solutions.add(solution);
    }

    public void randomizeUsing(SolutionRandomizer randomizer) {
        solutions.stream()
                .forEach(solution -> solution.randomizeUsing(randomizer));
    }

    public void mutateUsing(AMutation mutation) {
        solutions.stream()
                .forEach(solution -> solution.mutateUsing(mutation));
    }

    /**
     * Evaluate all solutions of this Population using the provided evaluation function.
     *
     * @param evaluationFunction Evaluation function to use to evaluate each solution of this Population.
     * @return The best quality of this evaluated population.
     */
    public Quality evaluateUsing(AEvaluationFunction evaluationFunction) {
        return solutions.stream()
                .map(solution -> solution.evaluateUsing(evaluationFunction))
                .max(Quality::compareTo)
                .get();
    }
}
