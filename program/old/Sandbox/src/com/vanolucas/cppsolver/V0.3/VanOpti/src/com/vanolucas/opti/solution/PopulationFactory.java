package com.vanolucas.opti.solution;

public interface PopulationFactory extends SolutionFactory {

    default Population newPopulation(final int SIZE) {
        return new Population(this, SIZE);
    }
}
