package com.vanolucas.opti.solution;

import com.vanolucas.opti.eval.AEvaluationFunction;
import com.vanolucas.opti.math.ParetoFront;
import com.vanolucas.opti.mutation.AMutation;
import com.vanolucas.opti.phenotype.APhenotypeWrap;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.random.solution.SolutionRandomizer;

public abstract class Solution implements Comparable<Solution> {

    /*
    Attributes
     */

    protected APhenotypeWrap phenotypeWrap;
    protected Quality quality;

    /*
    Constructors
     */

    public Solution(Object phenotype) {
        this(phenotype, null);
    }

    public Solution(APhenotypeWrap phenotypeWrap) {
        this(phenotypeWrap, null);
    }

    public Solution(Object phenotype, Quality quality) {
        this(new APhenotypeWrap(phenotype), quality);
    }

    public Solution(APhenotypeWrap phenotypeWrap, Quality quality) {
        this.phenotypeWrap = phenotypeWrap;
        this.quality = quality;
    }

    /*
    Accessors
     */

    public APhenotypeWrap getPhenotypeWrap() {
        return phenotypeWrap;
    }

    public Object getPhenotype() {
        return phenotypeWrap.get();
    }

    public Quality getQuality() {
        return quality;
    }

    /*
    Commands
     */

    /**
     * Propose this Solution as new good solution in the given Pareto Front.
     *
     * @param bestQualities Pareto Front collection of best qualities known.
     * @return Result of the comparison of the new item with previous items of the pareto front.
     */
    public ParetoFront.NewItemResult proposeNewBestIn(ParetoFront<Quality> bestQualities) {
        // propose this solution's quality to the pareto front and get the result
        ParetoFront.NewItemResult submissionResult = bestQualities.submit(quality);

        // if the new quality has been stored in the pareto front, we need to
        // use a new copy so that we don't modify the one kept in the pareto front
        if (submissionResult == ParetoFront.NewItemResult.DOMINANT
                || submissionResult == ParetoFront.NewItemResult.EQUIVALENT_AND_KEPT) {
            quality = quality.deepClone();
        }

        return submissionResult;
    }

    public void randomizeUsing(SolutionRandomizer randomizer) {
        randomizer.randomize(this);
        quality = null; // reset quality
    }

    public void mutateUsing(AMutation mutation) {
        mutation.mutate(this);
        quality = null; // reset quality
    }

    public Quality evaluateUsing(AEvaluationFunction evaluationFunction) {
        quality = evaluationFunction.evaluate(this);
        return quality;
    }

    public abstract void reproduceFrom(Solution other);

    public void reproduceTo(Solution other) {
        other.reproduceFrom(this);
    }

    /*
    Comparable
     */

    @Override
    public int compareTo(Solution other) {
        if (quality == null) {
            if (other.quality == null) {
                return 0;
            } else {
                return -1;
            }
        } else if (other.quality == null) {
            return 1;
        }

        return this.quality.compareTo(other.quality);
    }

    public boolean isBetterThan(Solution other) {
        if (quality == null) {
            return false;
        } else if (other.quality == null) {
            return true;
        }

        return this.quality.isBetterThan(other.quality);
    }

    public boolean isWorseThan(Solution other) {
        if (quality == null) {
            if (other.quality == null) {
                return false;
            } else {
                return true;
            }
        } else if (other.quality == null) {
            return false;
        }

        return this.quality.isWorseThan(other.quality);
    }

    public boolean isEquivalentOrBetterThan(Solution other) {
        if (quality == null) {
            if (other.quality == null) {
                return true;
            } else {
                return false;
            }
        } else if (other.quality == null) {
            return true;
        }

        return this.quality.isEquivalentOrBetterThan(other.quality);
    }

    /*
    toString
     */

    @Override
    public String toString() {
        String str = "Phenotype:";
        String phenotypeStr = phenotypeWrap.toString();

        // if phenotype string is short, fit it all on a single line
        if (phenotypeStr.length() > 80 - "Phenotype: ".length()) {
            str += "\n" + phenotypeStr;
        } else {
            str += " " + phenotypeStr;
        }

        // if there is a quality
        if (quality != null) {
            str += "\nQuality: " + quality.toString();
        }

        return str;
    }
}
