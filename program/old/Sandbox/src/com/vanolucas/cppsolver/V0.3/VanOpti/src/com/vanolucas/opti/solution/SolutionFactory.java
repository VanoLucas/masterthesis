package com.vanolucas.opti.solution;

import com.vanolucas.opti.phenotype.APhenotypeWrapFactory;
import com.vanolucas.opti.quality.QualityFactory;

public interface SolutionFactory extends APhenotypeWrapFactory, QualityFactory {
    Solution newSolution();
}
