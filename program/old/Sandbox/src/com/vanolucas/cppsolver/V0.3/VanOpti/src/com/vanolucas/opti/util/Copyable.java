package com.vanolucas.opti.util;

public interface Copyable {

    void copyFrom(Object other);

    default void copyTo(Copyable other) {
        other.copyFrom(this);
    }
}
