package com.vanolucas.opti.util;

public class ObjWithProba<TObj> {

    /*
    Defaults
     */

    private static final double DEFAULT_PROBA = 1d;

    /*
    Attributes
     */

    private TObj obj;
    private double proba;

    /*
    Constructors
     */

    public ObjWithProba(TObj obj) {
        this(obj, DEFAULT_PROBA);
    }
    public ObjWithProba(TObj obj, double proba) {
        this.obj = obj;
        this.proba = proba;
    }

    /*
    Accessors
     */

    public TObj getObj() {
        return obj;
    }

    public void setObj(TObj obj) {
        this.obj = obj;
    }

    public double getProba() {
        return proba;
    }

    public void setProba(double proba) {
        this.proba = proba;
    }
}
