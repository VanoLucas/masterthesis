package com.vanolucas.opti.util;


import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Observable object of the observer design pattern.
 * @param <TListener> Type of listener objects.
 */
public class Observable<TListener> {

    /**
     * Observers to notify upon events.
     */
    private List<TListener> listeners = new ArrayList<>(1);

    /*
    Commands
     */

    public void addListener(TListener listener) {
        listeners.add(listener);
    }

    public void removeListener(TListener listener) {
        listeners.remove(listener);
    }

    public void removeAllListeners() {
        listeners.clear();
    }

    protected void forEachListener(Consumer<? super TListener> action) {
        listeners.forEach(action);
    }

    protected void notify(Consumer<? super TListener> action) {
        forEachListener(action);
    }
}

