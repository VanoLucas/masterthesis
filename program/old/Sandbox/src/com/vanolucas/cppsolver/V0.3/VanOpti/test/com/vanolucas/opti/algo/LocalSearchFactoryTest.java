package com.vanolucas.opti.algo;

import com.vanolucas.opti.algo.localsearch.GeneticLocalSearchFactory;
import com.vanolucas.opti.algo.localsearch.LocalSearch;
import com.vanolucas.opti.algo.localsearch.PhenotypeLocalSearchFactory;
import com.vanolucas.opti.eval.EvaluationFunction;
import com.vanolucas.opti.eval.GeneticEvaluationFunction;
import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.mutation.*;
import com.vanolucas.opti.quality.LongCost;
import com.vanolucas.opti.random.Randomizable;
import com.vanolucas.opti.random.genome.GenomeRandomizer;
import com.vanolucas.opti.random.phenotype.PhenotypeRandomizer;
import com.vanolucas.opti.solution.Solution;
import com.vanolucas.opti.util.Copyable;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LocalSearchFactoryTest {

    /**
     * Test phenotype local search factory with dummy target number problem.
     */
    @Test
    void phenotypeLocalSearch() {

        class MyPhenotype implements Copyable {
            private int value;

            public MyPhenotype(int value) {
                this.value = value;
            }

            public int getValue() {
                return value;
            }

            public void setValue(int value) {
                this.value = value;
            }

            @Override
            public String toString() {
                return String.valueOf(value);
            }

            @Override
            public void copyFrom(Object other) {
                MyPhenotype o = (MyPhenotype) other;
                this.value = o.value;
            }
        }

        PhenotypeLocalSearchFactory<MyPhenotype, LongCost> factory = new PhenotypeLocalSearchFactory<MyPhenotype, LongCost>() {
            @Override
            public MyPhenotype newPhenotype() {
                return new MyPhenotype(0);
            }

            @Override
            public LongCost newQuality() {
                return new LongCost();
            }

            @Override
            public PhenotypeRandomizer<MyPhenotype> newPhenotypeRandomizer() {
                return new PhenotypeRandomizer<MyPhenotype>() {
                    @Override
                    public void randomize(MyPhenotype phenotype) {
                        phenotype.setValue(new Random().nextInt(100));
                    }
                };
            }

            @Override
            public EvaluationFunction<MyPhenotype, LongCost> newEvaluationFunction() {
                return new EvaluationFunction<MyPhenotype, LongCost>() {
                    @Override
                    public LongCost evaluatePhenotype(MyPhenotype phenotype) {
                        final int TARGET = 33;
                        long cost = Math.abs(phenotype.getValue() - TARGET);
                        return new LongCost(cost);
                    }
                };
            }

            @Override
            public List<APhenotypeMutation> newPhenotypeMutationOperators() {
                return new ArrayList<APhenotypeMutation>() {{
                    add((PhenotypeMutation<MyPhenotype>) phenotype -> newPhenotypeRandomizer().randomize(phenotype));
                    add((PhenotypeMutation<MyPhenotype>) phenotype -> newPhenotypeRandomizer().randomize(phenotype));
                    add((PhenotypeMutation<MyPhenotype>) phenotype -> newPhenotypeRandomizer().randomize(phenotype));
                }};
            }
        };

        LocalSearch localSearch = factory.newLocalSearch();

        localSearch.addListener(new LocalSearch.Listener() {
            @Override
            public void onNewBetterSolutionFound(Solution newBetterSolution, long iteration) {
//                System.out.println("\nIteration: " + iteration);
//                System.out.println(newBetterSolution);
            }
        });

        localSearch.runUntilQuality(factory.newQuality().set(0l));

        assertEquals(new LongCost(0), localSearch.getLastBetterSolutionFoundQuality());

    }

    /**
     * Test genetic local search factory with dummy target number problem.
     */
    @Test
    void geneticLocalSearch() {

        class MyGenome implements Genome, Randomizable {
            private int value1;
            private int value2;

            public MyGenome() {
                this(0, 0);
            }
            public MyGenome(int value1, int value2) {
                this.value1 = value1;
                this.value2 = value2;
            }

            public int getValue1() {
                return value1;
            }

            public void setValue1(int value1) {
                this.value1 = value1;
            }

            public int getValue2() {
                return value2;
            }

            public void setValue2(int value2) {
                this.value2 = value2;
            }

            @Override
            public void randomize() {
                Random rand = new Random();
                this.value1 = rand.nextInt(100);
                this.value2 = rand.nextInt(100);
            }

            @Override
            public void copyFrom(Genome other) {
                MyGenome o = (MyGenome) other;
                this.value1 = o.value1;
                this.value2 = o.value2;
            }

            @Override
            public String toString() {
                return "" + value1 + "+" + value2;
            }
        }

        class MyPhenotype {
            private int value;

            public MyPhenotype(int value) {
                this.value = value;
            }

            public int getValue() {
                return value;
            }

            public void setValue(int value) {
                this.value = value;
            }

            @Override
            public String toString() {
                return String.valueOf(value);
            }
        }

        GeneticLocalSearchFactory<MyGenome, MyPhenotype, LongCost> factory = new GeneticLocalSearchFactory<MyGenome, MyPhenotype, LongCost>() {

            @Override
            public MyGenome newGenome() {
                return new MyGenome();
            }

            @Override
            public MyPhenotype newPhenotype() {
                return new MyPhenotype(0);
            }

            @Override
            public LongCost newQuality() {
                return new LongCost();
            }

            @Override
            public GenomeRandomizer<MyGenome> newGenomeRandomizer() {
                return new GenomeRandomizer<MyGenome>() {
                    @Override
                    public void randomizeGenome(MyGenome genome) {
                        genome.randomize();
                    }
                };
            }

            @Override
            public GeneticEvaluationFunction<MyGenome, MyPhenotype, LongCost> newGeneticEvaluationFunction() {
                return new GeneticEvaluationFunction<MyGenome, MyPhenotype, LongCost>() {
                    @Override
                    public LongCost evaluatePhenotype(MyPhenotype phenotype) {
                        final int TARGET = 33;
                        long cost = Math.abs(phenotype.getValue() - TARGET);
                        return new LongCost(cost);
                    }

                    @Override
                    public void decodeGenome(MyGenome genome, MyPhenotype phenotype) {
                        phenotype.setValue(genome.getValue1() + genome.getValue2());
                    }
                };
            }

            @Override
            public List<AGenomeMutation> newGeneticMutationOperators() {
                return new ArrayList<AGenomeMutation>() {{
                    add((GenomeMutation<MyGenome>) genome -> genome.setValue1(new Random().nextInt(100)));
                    add((GenomeMutation<MyGenome>) genome -> genome.setValue1(new Random().nextInt(100)));
                    add((GenomeMutation<MyGenome>) genome -> genome.setValue2(new Random().nextInt(100)));
                    add((GenomeMutation<MyGenome>) genome -> genome.setValue2(new Random().nextInt(100)));
                }};
            }
        };

        LocalSearch localSearch = factory.newLocalSearch();

        localSearch.addListener(new LocalSearch.Listener() {
            @Override
            public void onNewBetterSolutionFound(Solution newBetterSolution, long iteration) {
//                System.out.println("\nIteration: " + iteration);
//                System.out.println(newBetterSolution);
            }
        });

        localSearch.runUntilQuality(factory.newQuality().set(0l));

        assertEquals(new LongCost(0), localSearch.getLastBetterSolutionFoundQuality());
    }

}