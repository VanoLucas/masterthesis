package com.vanolucas.opti.math;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class PickerTest {

    @Test
    void pickOne() {
        List<Integer> objectsToPick = new ArrayList<Integer>() {{
            add(1);
            add(2);
            add(3);
        }};
        double[] proba = new double[] {1d, 1d, 2d};

        Picker<Integer> picker = new Picker<>(objectsToPick, proba);

        List<Integer> picked = IntStream.range(0, 10000)
                .map(i -> picker.pickOne())
                .boxed()
                .collect(Collectors.toList());

        List<Integer> count = objectsToPick.stream()
                .mapToInt(pickableValue -> (int) picked.stream()
                        .filter(pickedValue -> pickedValue == pickableValue)
                        .count()
                )
                .boxed()
                .collect(Collectors.toList());

        IntStream.range(0, objectsToPick.size())
                .forEach(i -> {
//                    System.out.println(objectsToPick.get(i) + ": " + count.get(i));
                });

        assertEquals(5000, count.get(2), 200);
    }


    @Test
    void pickN() {
        List<Integer> objectsToPick = new ArrayList<Integer>() {{
            add(1);
            add(2);
            add(3);
        }};
        double[] proba = new double[] {1d, 1d, 2d};

        Picker<Integer> picker = new Picker<>(objectsToPick, proba);

        List<Integer> picked = picker.pickN(10000);

        List<Integer> count = objectsToPick.stream()
                .mapToInt(pickableValue -> (int) picked.stream()
                        .filter(pickedValue -> pickedValue == pickableValue)
                        .count()
                )
                .boxed()
                .collect(Collectors.toList());

        IntStream.range(0, objectsToPick.size())
                .forEach(i -> {
//                    System.out.println(objectsToPick.get(i) + ": " + count.get(i));
                });

        assertEquals(5000, count.get(2), 200);
    }

}