package com.vanolucas.cppsolver.eval;

import com.vanolucas.cppsolver.genome.CppGenomeType;
import com.vanolucas.cppsolver.instance.CppParams;
import com.vanolucas.cppsolver.problem.CppSolution;
import com.vanolucas.opti.eval.EvaluationFunction;
import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.genome.GenomeVector;
import com.vanolucas.opti.quality.CostDouble;
import com.vanolucas.opti.quality.Quality;

public class CppEvaluationFunction implements EvaluationFunction {

    private CppGenomeDecoder decoder;

    public CppEvaluationFunction(CppParams cppParams, CppGenomeType genomeType) {
        switch (genomeType) {
            case NS:
                this.decoder = new CppGenomeDecoderNS(cppParams);
                break;
            case SSB:
                this.decoder = new CppGenomeDecoderSSB(cppParams);
                break;
            case PS:
                this.decoder = new CppGenomeDecoderPS(cppParams);
                break;
            default:
                this.decoder = null;
                break;
        }
    }

    @Override
    public void evaluate(Object solution, Quality outQuality) {
        CppSolution cppSolution = (CppSolution) solution;
        double cost = cppSolution.getCost();
        CostDouble quality = (CostDouble) outQuality;

        quality.set(cost);
    }

    @Override
    public void decode(Genome genome, Object outSolution) {
        decoder.decode(((GenomeVector) genome), ((CppSolution) outSolution));
    }
}
