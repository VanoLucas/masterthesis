package com.vanolucas.cppsolver.eval;

import com.vanolucas.cppsolver.problem.CppSolution;
import com.vanolucas.opti.genome.GenomeVector;

public interface CppGenomeDecoder {
    void decode(GenomeVector genome, CppSolution outSolution);
}
