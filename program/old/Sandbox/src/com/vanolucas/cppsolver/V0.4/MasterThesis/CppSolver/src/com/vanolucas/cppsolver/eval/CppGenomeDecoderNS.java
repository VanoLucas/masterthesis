package com.vanolucas.cppsolver.eval;

import com.vanolucas.cppsolver.genome.GenomeNS;
import com.vanolucas.cppsolver.instance.CppParams;
import com.vanolucas.cppsolver.problem.BookCover;
import com.vanolucas.cppsolver.problem.CppSolution;
import com.vanolucas.cppsolver.problem.Slot;
import com.vanolucas.opti.genome.GenomeVector;
import com.vanolucas.opti.util.CutInteger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class CppGenomeDecoderNS implements CppGenomeDecoder {

    /*
    Attributes
     */

    private CppParams cppParams;

    private final CutInteger nbSlotsSplitter;

    /*
    Constructor
     */

    public CppGenomeDecoderNS(CppParams cppParams) {
        this.cppParams = cppParams;

        // each book cover will be assigned at least 1 slot,
        // so the remaining number of slots to assign is all slots - nb book covers
        this.nbSlotsSplitter = new CutInteger(cppParams.getTotalNbSlots() - cppParams.getNbBookCovers());
    }

    /*
    Genome decoder
     */

    @Override
    public void decode(GenomeVector g, CppSolution outSolution) {
        final int NB_BOOK_COVERS = cppParams.getNbBookCovers();

        // fetch our N-S CPP genome
        GenomeNS genome = (GenomeNS) g;

        /*
        Extract keys (weights) from the two parts of the N-S genome
         */

        List<Double> keysNbSlotsForEachBookCover = genome.getValues(0, NB_BOOK_COVERS);
        List<Double> keysSizeEachPart = genome.getValues(NB_BOOK_COVERS, genome.size());

        /*
        Calculate the number of slots each book cover will be assigned to
         */

        // split the number of slots based on the keys from the genome
        List<Integer> nbSlotsForEachBookCover = nbSlotsSplitter.cutCake(keysNbSlotsForEachBookCover).stream()
                // add 1 since each book cover must be assigned to at least 1 slot
                .mapToInt(nbSlots -> nbSlots + 1)
                .boxed()
                .collect(Collectors.toList());

        /*
        Split each book cover demand over multiple slots
         */

        final List<BookCover> bookCovers = cppParams.getBooks();
        List<Slot> slots = new ArrayList<>(cppParams.getTotalNbSlots());

        // current position in the genome of keys that define the nb of printings for each slot
        int cursorKeys = 0;

        // for each book cover, divide its demand over multiple slots
        for (int bookIndex = 0; bookIndex < NB_BOOK_COVERS; bookIndex++) {
            // get the current book cover
            final BookCover bookCover = bookCovers.get(bookIndex);

            // prepare splitter function object that will divide the demand for this book cover in multiple values
            CutInteger nbPrintingsSplitter = new CutInteger(bookCover.getDemand());

            // get nb of values we should divide this book cover's demand over
            final int NB_SLOTS_FOR_THIS_BOOK_COVER = nbSlotsForEachBookCover.get(bookIndex);

            // get keys that define how to split the demand for this book cover from the genome
            List<Double> keysSizeEachPartForThisBookCover = keysSizeEachPart.subList(cursorKeys, cursorKeys + NB_SLOTS_FOR_THIS_BOOK_COVER);

            // convert keys to actual number of printings by splitting the demand for this book cover
            List<Integer> nbPrintingsEachSlot = nbPrintingsSplitter.cutCake(keysSizeEachPartForThisBookCover);

            // allocate this book cover to the slots, with the required number of printings
            slots.addAll(
                    nbPrintingsEachSlot.stream()
                            .map(nbPrintings -> new Slot(bookCover, nbPrintings))
                            .collect(Collectors.toList())
            );

            // advance genome cursor to the right keys for the next book cover
            cursorKeys += NB_SLOTS_FOR_THIS_BOOK_COVER;
        }

        // sort slots by number of printings descending:
        // sort by number of printings so that we tend to group slots with similar
        // required number of printings on the same offset plates
        Collections.sort(slots, Collections.reverseOrder());

        // apply our slots to the solution's offset plates
        outSolution.setSlots(slots);
    }
}
