package com.vanolucas.cppsolver.eval;

import com.vanolucas.cppsolver.genome.GenomePS;
import com.vanolucas.cppsolver.instance.CppParams;
import com.vanolucas.cppsolver.problem.CppSolution;
import com.vanolucas.cppsolver.problem.Slot;
import com.vanolucas.opti.genome.GenomeVector;
import com.vanolucas.opti.util.Scale;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class CppGenomeDecoderPS implements CppGenomeDecoder {

    /*
    Attributes
     */

    private CppParams cppParams;

    /*
    Constructor
     */

    public CppGenomeDecoderPS(CppParams cppParams) {
        this.cppParams = cppParams;
    }

    /*
    Genome decoder
     */

    @Override
    public void decode(GenomeVector g, CppSolution outSolution) {
        final int NB_BOOK_COVERS = cppParams.getNbBookCovers();

        // fetch our PS CPP genome
        GenomePS genome = (GenomePS) g;

        // extract keys from genome
        List<Double> keys = g.getValues();

        // initially allocate each book to a single slot that fills its whole demand
        List<Slot> slots = cppParams.getBooks().stream()
                .map(book -> new Slot(book, book.getDemand()))
                .collect(Collectors.toList());

        // now we need to divide these printings over multiple slots to use all the available slots
        // for each extra slot to fill
        // (each slot is defined by 2 keys: the book to put in it and its allocated nb of printings)
        for (int k = 0; k < keys.size(); k += 2) {
            // read params from genome
            final double keyPartToSplit = keys.get(k);
            final double cutFractionSize = keys.get(k + 1);

            // calculate index of the book cover/slot to split over two slots
            final int indexToSplit = (int) Scale.scale(keyPartToSplit, 1.0d, NB_BOOK_COVERS);

            // split the chosen slot in two slots, according to the provided cut size
            slots.add(
                    slots.get(indexToSplit).splitInTwo(cutFractionSize)
            );
        }

        // sort slots by number of printings descending:
        // sort by number of printings so that we tend to group slots with similar
        // required number of printings on the same offset plates
        Collections.sort(slots, Collections.reverseOrder());

        // apply our slots to the solution's offset plates
        outSolution.setSlots(slots);
    }
}
