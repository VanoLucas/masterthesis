package com.vanolucas.cppsolver.eval;

import com.vanolucas.cppsolver.genome.GenomeSSB;
import com.vanolucas.cppsolver.instance.CppParams;
import com.vanolucas.cppsolver.problem.BookCover;
import com.vanolucas.cppsolver.problem.CppSolution;
import com.vanolucas.cppsolver.problem.Slot;
import com.vanolucas.opti.genome.GenomeVector;
import com.vanolucas.opti.util.CutInteger;
import com.vanolucas.opti.util.Scale;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class CppGenomeDecoderSSB implements CppGenomeDecoder {

    /*
    Attributes
     */

    private CppParams cppParams;

    /*
    Constructor
     */

    public CppGenomeDecoderSSB(CppParams cppParams) {
        this.cppParams = cppParams;
    }

    /*
    Genome decoder
     */

    @Override
    public void decode(GenomeVector g, CppSolution outSolution) {
        final int NB_BOOK_COVERS = cppParams.getNbBookCovers();

        // fetch our S-SB CPP genome
        GenomeSSB genome = (GenomeSSB) g;

        /*
        Extract keys (weights) from the two parts of the S-SB genome
         */

        // for each book cover, the key that determines the size of the first demand slice
        List<Double> keysFirstSlice = genome.getValues(0, NB_BOOK_COVERS);
        // keys that determine the size of other slices and to which book cover it is attributed
        List<Double> keysOtherSlices = genome.getValues(NB_BOOK_COVERS, g.size());

        /*
        Construct list of keys that determine the size of demand slices for each book cover
         */

        // for each book cover, the keys that determine how to split its demand
        List<List<Double>> keysSplitSize = new ArrayList<>(NB_BOOK_COVERS);

        // add the first split to each book cover
        for (int bookCoverIndex = 0; bookCoverIndex < NB_BOOK_COVERS; bookCoverIndex++) {
            keysSplitSize.add(new ArrayList<>());
            keysSplitSize.get(bookCoverIndex).add(
                    keysFirstSlice.get(bookCoverIndex)
            );
        }

        // read all remaining genome keys to determine demand slices
        for (int cursor = 0; cursor < keysOtherSlices.size(); cursor += 2) {
            final double KEY_SLICE_SIZE = keysOtherSlices.get(cursor);
            final double KEY_BOOK_COVER = keysOtherSlices.get(cursor + 1);

            final int BOOK_COVER_INDEX = (int) Scale.scale(KEY_BOOK_COVER, 1.0d, NB_BOOK_COVERS);

            keysSplitSize.get(BOOK_COVER_INDEX).add(KEY_SLICE_SIZE);
        }

        /*
        Construct the list of nb of printings for each slot
         */

        // list that will contain the nb of printings for each book allocated to each slot
        List<Slot> slots = new ArrayList<>(cppParams.getTotalNbSlots());

        // cut integer algorithm to split the demand of each book cover
        CutInteger demandSplitter = new CutInteger();

        // for each book cover, split its demand according to keys
        for (int bookIndex = 0; bookIndex < NB_BOOK_COVERS; bookIndex++) {
            // fetch the current book cover
            final BookCover bookCover = cppParams.getBook(bookIndex);
            // get demand for this book cover
            final int DEMAND = bookCover.getDemand();

            // get keys that determine the size of each demand slice for this book cover
            final List<Double> keys = keysSplitSize.get(bookIndex);

            // calculate each demand slice size based on the keys
            final List<Integer> nbPrintings = demandSplitter.cutCake(DEMAND, keys);

            // store the demand slices for this book cover
            slots.addAll(
                    nbPrintings.stream()
                            .map(printings -> new Slot(bookCover, printings))
                            .collect(Collectors.toList())
            );
        }

        // sort slots by number of printings descending:
        // sort by number of printings so that we tend to group slots with similar
        // required number of printings on the same offset plates
        Collections.sort(slots, Collections.reverseOrder());

        // apply our slots to the solution's offset plates
        outSolution.setSlots(slots);
    }
}
