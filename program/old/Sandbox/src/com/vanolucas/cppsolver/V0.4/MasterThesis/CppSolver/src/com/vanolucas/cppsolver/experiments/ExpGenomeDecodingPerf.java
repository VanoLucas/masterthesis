package com.vanolucas.cppsolver.experiments;

import com.vanolucas.cppsolver.eval.CppEvaluationFunction;
import com.vanolucas.cppsolver.genome.CppGenomeType;
import com.vanolucas.cppsolver.genome.GenomeNS;
import com.vanolucas.cppsolver.genome.GenomePS;
import com.vanolucas.cppsolver.genome.GenomeSSB;
import com.vanolucas.cppsolver.instance.CppParams;
import com.vanolucas.cppsolver.problem.CppSolution;
import com.vanolucas.opti.eval.EvaluationFunction;
import com.vanolucas.opti.genome.GenomeVector;

import java.time.Duration;
import java.time.Instant;

/**
 * Experiment:
 * Compare the performance of the evaluation function to decode the CPP genomes:
 * - N-S genome
 * - S-SB genome
 * - PS genome
 */
public class ExpGenomeDecodingPerf implements Runnable {

    private final CppParams cppInstance;
    private final long NB_GENOMES_TO_DECODE;

    public ExpGenomeDecodingPerf(CppParams cppInstance, long nbGenomesToDecode) {
        this.cppInstance = cppInstance;
        this.NB_GENOMES_TO_DECODE = nbGenomesToDecode;
    }

    @Override
    public void run() {
        runExperiment(CppGenomeType.NS);
        runExperiment(CppGenomeType.SSB);
        runExperiment(CppGenomeType.PS);
    }

    private void runExperiment(CppGenomeType genomeType) {
        final EvaluationFunction evaluationFunction = new CppEvaluationFunction(cppInstance, genomeType);
        GenomeVector genome;
        CppSolution solution = new CppSolution(cppInstance);

        switch (genomeType) {
            case NS:
                genome = new GenomeNS(cppInstance);
                break;
            case SSB:
                genome = new GenomeSSB(cppInstance);
                break;
            case PS:
                genome = new GenomePS(cppInstance);
                break;
            default:
                genome = null;
                break;
        }

        // begin experiment
        Instant start = Instant.now();

        // run experiment: decode all genomes
        for (long g = 0L; g < NB_GENOMES_TO_DECODE; g++) {
            // randomize genome
            genome.randomize();
            // decode it
            evaluationFunction.decode(genome, solution);
        }

        // end experiment
        Instant end = Instant.now();

        // print result of experiment
//        System.out.println(
//                String.format(
//                        "Decoded %d %s genomes in %d seconds.",
//                        NB_GENOMES_TO_DECODE,
//                        genomeType.toString(),
//                        Duration.between(start, end).getSeconds()
//                )
//        );
    }
}

/*
RESULTS

30 book covers
10 offset plates
4 slots per plate

Decoded 1000000 NS genomes in 40 seconds.
Decoded 1000000 SSB genomes in 30 seconds.
Decoded 1000000 PS genomes in 3 seconds.

Decoded 2000000 NS genomes in 79 seconds.
Decoded 2000000 SSB genomes in 58 seconds.
Decoded 2000000 PS genomes in 6 seconds.

Decoded 10000000 NS genomes in 416 seconds.
Decoded 10000000 SSB genomes in 305 seconds.
Decoded 10000000 PS genomes in 32 seconds.

40 book covers
14 offset plates
4 slots per plate

Decoded 1000000 NS genomes in 56 seconds.
Decoded 1000000 SSB genomes in 41 seconds.
Decoded 1000000 PS genomes in 5 seconds.

Decoded 2000000 NS genomes in 110 seconds.
Decoded 2000000 SSB genomes in 84 seconds.
Decoded 2000000 PS genomes in 10 seconds.

Decoded 10000000 NS genomes in 548 seconds.
Decoded 10000000 SSB genomes in 439 seconds.
Decoded 10000000 PS genomes in 58 seconds.

 */
