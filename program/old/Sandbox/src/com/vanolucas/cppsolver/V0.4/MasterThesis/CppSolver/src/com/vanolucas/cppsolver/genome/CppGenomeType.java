package com.vanolucas.cppsolver.genome;

public enum CppGenomeType {
    NS,
    SSB,
    PS
}
