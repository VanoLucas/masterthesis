package com.vanolucas.cppsolver.genome;

import com.vanolucas.cppsolver.instance.CppParams;
import com.vanolucas.opti.genome.GenomeVectorDouble01;

public class GenomeNS extends GenomeVectorDouble01 {

    public GenomeNS(CppParams cppParams) {
        super(getSize(cppParams),
                0d);
    }

    /**
     * The size of a N-S genome is given by:
     * size = n.S + m
     * @param cppParams CPP instance parameters.
     * @return The length a N-S genome should have to encode a solution for the given CPP instance.
     */
    public static int getSize(CppParams cppParams) {
        return cppParams.getNbBookCovers() + cppParams.getTotalNbSlots();
    }
}
