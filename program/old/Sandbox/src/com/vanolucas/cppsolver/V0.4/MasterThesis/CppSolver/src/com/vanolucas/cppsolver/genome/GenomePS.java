package com.vanolucas.cppsolver.genome;

import com.vanolucas.cppsolver.instance.CppParams;
import com.vanolucas.opti.genome.GenomeVectorDouble01;

public class GenomePS extends GenomeVectorDouble01 {

    public GenomePS(CppParams cppParams) {
        super(getSize(cppParams),
                0d);
    }

    /**
     * The size of a PS genome is given by:
     * size = 2 * (n.S - m) = 2.n.S - 2m
     * @param cppParams CPP instance parameters.
     * @return The length a PS genome should have to encode a solution for the given CPP instance.
     */
    public static int getSize(CppParams cppParams) {
        return (cppParams.getTotalNbSlots() - cppParams.getNbBookCovers()) * 2;
    }
}
