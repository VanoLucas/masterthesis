package com.vanolucas.cppsolver.genome;

import com.vanolucas.cppsolver.instance.CppParams;
import com.vanolucas.opti.genome.GenomeVectorDouble01;

public class GenomeSSB extends GenomeVectorDouble01 {

    public GenomeSSB(CppParams cppParams) {
        super(getSize(cppParams),
                0d);
    }

    /**
     * The size of an S-SB genome is given by:
     * size = m + 2*(n.S - m) = 2.n.S - m
     * @param cppParams CPP instance parameters.
     * @return The length a S-SB genome should have to encode a solution for the given CPP instance.
     */
    public static int getSize(CppParams cppParams) {
        return cppParams.getNbBookCovers() + 2 * (cppParams.getTotalNbSlots() - cppParams.getNbBookCovers());
    }
}
