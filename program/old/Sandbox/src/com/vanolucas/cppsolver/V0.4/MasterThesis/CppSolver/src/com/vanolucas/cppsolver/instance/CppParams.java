package com.vanolucas.cppsolver.instance;

import com.vanolucas.cppsolver.problem.BookCover;

import java.util.List;
import java.util.stream.Collectors;

public class CppParams {

    /*
    Defaults
     */

    private static final double DEFAULT_COST_PLATE = 0d;
    private static final double DEFAULT_COST_ONE_PRINTING = 1d;

    /*
    Attributes
     */

    private List<BookCover> books;

    private int nbPlates;
    private int nbSlotsPerPlate;

    private double costOffsetPlate;
    private double costOnePrinting;

    /*
    Constructors
     */

    public CppParams(List<BookCover> books, int nbPlates, int nbSlotsPerPlate) {
        this(books, nbPlates, nbSlotsPerPlate, DEFAULT_COST_PLATE, DEFAULT_COST_ONE_PRINTING);
    }

    public CppParams(List<BookCover> books, int nbPlates, int nbSlotsPerPlate, double costOffsetPlate, double costOnePrinting) {
        this.books = books;
        this.nbPlates = nbPlates;
        this.nbSlotsPerPlate = nbSlotsPerPlate;
        this.costOffsetPlate = costOffsetPlate;
        this.costOnePrinting = costOnePrinting;
    }

    /*
    Accessors
     */

    public BookCover getBook(int index) {
        return books.get(index);
    }

    public List<BookCover> getBooks() {
        return books;
    }

    public int getNbBookCovers() {
        return books.size();
    }

    public void setBooks(List<BookCover> books) {
        this.books = books;
    }

    public void setBookDemands(List<Integer> bookDemands) {
        this.books = bookDemands.stream()
                .map(demand -> new BookCover(demand))
                .collect(Collectors.toList());
    }

    public int getTotalDemand() {
        return books.stream()
                .mapToInt(BookCover::getDemand)
                .sum();
    }

    public int getNbPlates() {
        return nbPlates;
    }

    public void setNbPlates(int nbPlates) {
        this.nbPlates = nbPlates;
    }

    public int getNbSlotsPerPlate() {
        return nbSlotsPerPlate;
    }

    public void setNbSlotsPerPlate(int nbSlotsPerPlate) {
        this.nbSlotsPerPlate = nbSlotsPerPlate;
    }

    public int getTotalNbSlots() {
        return nbPlates * nbSlotsPerPlate;
    }

    public double getCostOffsetPlate() {
        return costOffsetPlate;
    }

    public void setCostOffsetPlate(double costOffsetPlate) {
        this.costOffsetPlate = costOffsetPlate;
    }

    public double getCostOnePrinting() {
        return costOnePrinting;
    }

    public void setCostOnePrinting(double costOnePrinting) {
        this.costOnePrinting = costOnePrinting;
    }
}
