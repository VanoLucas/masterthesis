package com.vanolucas.cppsolver.instance;

import com.vanolucas.cppsolver.problem.BookCover;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CppParams30 extends CppParams {
    private static final int DEFAULT_NB_OFFSET_PLATES = 10;
    private static final int DEFAULT_NB_SLOTS_PER_PLATE = 4;


    private static final int[] DEMANDS = new int[]{
            30000,
            28000,
            27000,
            26000,
            26000,
            23000,
            22000,
            22000,
            20000,
            20000,
            19000,
            18000,
            17000,
            16000,
            15000,
            15000,
            14000,
            13500,
            13000,
            11000,
            10500,
            10000,
            9000,
            9000,
            7500,
            6000,
            5000,
            2500,
            1500,
            1000
    };

    private static final List<BookCover> BOOKS = IntStream.range(0, DEMANDS.length)
            .mapToObj(bookIndex -> new BookCover(
                            String.format("%2d", bookIndex + 1),
                            DEMANDS[bookIndex]
                    )
            )
            .collect(Collectors.toList());

    public CppParams30() {
        this(DEFAULT_NB_OFFSET_PLATES);
    }

    public CppParams30(int nbPlates) {
        this(nbPlates, DEFAULT_NB_SLOTS_PER_PLATE);
    }

    public CppParams30(int nbPlates, int nbSlotsPerPlate) {
        super(BOOKS, nbPlates, nbSlotsPerPlate);
    }
}
