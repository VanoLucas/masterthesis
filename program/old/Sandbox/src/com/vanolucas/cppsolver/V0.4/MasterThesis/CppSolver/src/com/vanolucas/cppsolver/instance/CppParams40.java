package com.vanolucas.cppsolver.instance;

import com.vanolucas.cppsolver.problem.BookCover;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CppParams40 extends CppParams {
    private static final int DEFAULT_NB_OFFSET_PLATES = 14;
    private static final int DEFAULT_NB_SLOTS_PER_PLATE = 4;

    private static final int[] DEMANDS = new int[]{
            50000,
            47000,
            45500,
            41500,
            37000,
            32500,
            30500,
            29000,
            27000,
            27000,
            26700,
            25000,
            22000,
            19000,
            16100,
            15000,
            14000,
            13500,
            13000,
            13000,
            12900,
            12000,
            12000,
            11300,
            10700,
            10000,
            10000,
            9100,
            8000,
            6300,
            5000,
            5000,
            4300,
            4200,
            4000,
            3000,
            2650,
            1800,
            1100,
            700
    };

    private static final List<BookCover> BOOKS = IntStream.range(0, DEMANDS.length)
            .mapToObj(bookIndex -> new BookCover(
                    String.format("%2d", bookIndex + 1),
                    DEMANDS[bookIndex]
                    )
            )
            .collect(Collectors.toList());

    public CppParams40() {
        this(DEFAULT_NB_OFFSET_PLATES);
    }

    public CppParams40(int nbPlates) {
        this(nbPlates, DEFAULT_NB_SLOTS_PER_PLATE);
    }

    public CppParams40(int nbPlates, int nbSlotsPerPlate) {
        super(BOOKS, nbPlates, nbSlotsPerPlate);
    }
}
