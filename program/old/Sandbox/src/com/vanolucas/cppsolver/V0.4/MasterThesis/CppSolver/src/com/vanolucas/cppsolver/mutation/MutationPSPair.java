package com.vanolucas.cppsolver.mutation;

import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.genome.GenomeVector;
import com.vanolucas.opti.mutation.MutationGene;
import com.vanolucas.opti.mutation.MutationSequence;
import com.vanolucas.opti.mutation.MutationSingleGene;
import com.vanolucas.opti.random.RandGenerator;

public class MutationPSPair extends MutationSequence {

    private RandGenerator rand;

    public MutationPSPair() {
        super(
                new MutationSingleGene(),
                new MutationSingleGene()
        );
    }

    public MutationPSPair(MutationGene pGeneMutationOperator, MutationGene sGeneMutationOperator) {
        super(
                new MutationSingleGene(pGeneMutationOperator),
                new MutationSingleGene(sGeneMutationOperator)
        );
    }

    @Override
    public void mutate(Genome g) {
        GenomeVector genome = (GenomeVector) g;

        if (rand == null) {
            rand = new RandGenerator();
        }

        int indexPairToMutate = rand.nextIntBetween0And(genome.size());
        if (indexPairToMutate % 2 == 1) {
            indexPairToMutate--;
        }

        ((MutationSingleGene) mutationSequence.get(0)).setIndexGeneToMutate(indexPairToMutate);
        ((MutationSingleGene) mutationSequence.get(1)).setIndexGeneToMutate(indexPairToMutate + 1);

        super.mutate(genome);
    }
}
