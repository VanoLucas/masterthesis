package com.vanolucas.cppsolver.problem;

public class BookCover implements Comparable<BookCover> {
    private String name;
    private int demand;

    public BookCover(String name, int demand) {
        this.name = name;
        this.demand = demand;
    }

    public BookCover(int demand) {
        this("", demand);
    }

    public BookCover() {
        this(0);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDemand() {
        return demand;
    }

    public void setDemand(int demand) {
        this.demand = demand;
    }

    @Override
    public int compareTo(BookCover o) {
        return Integer.compare(demand, o.demand);
    }
}
