package com.vanolucas.cppsolver.problem;

import com.vanolucas.cppsolver.eval.CppEvaluationFunction;
import com.vanolucas.cppsolver.genome.CppGenomeType;
import com.vanolucas.cppsolver.genome.GenomeNS;
import com.vanolucas.cppsolver.genome.GenomePS;
import com.vanolucas.cppsolver.genome.GenomeSSB;
import com.vanolucas.cppsolver.instance.CppParams;
import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.genome.GenomeVectorDouble01;
import com.vanolucas.opti.quality.CostDouble;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.solution.Individual;
import com.vanolucas.opti.solution.IndividualFactory;
import org.moeaframework.core.Solution;
import org.moeaframework.core.variable.RealVariable;
import org.moeaframework.problem.AbstractProblem;

public class CppInstance {
    private CppParams cppParams;
    private CppGenomeType genomeType;

    private IndividualFactory individualFactory = new IndividualFactory() {
        @Override
        public Genome newGenome() {
            switch (genomeType) {
                case NS:
                    return new GenomeNS(cppParams);
                case SSB:
                    return new GenomeSSB(cppParams);
                case PS:
                    return new GenomePS(cppParams);
            }
            return null;
        }

        @Override
        public Quality newQuality() {
            return new CostDouble();
        }

        @Override
        public Object newSolution() {
            return new CppSolution(cppParams);
        }
    };

    public CppInstance(CppParams cppParams, CppGenomeType genomeType) {
        this.cppParams = cppParams;
        this.genomeType = genomeType;
    }

    public CppParams getCppParams() {
        return cppParams;
    }

    public void setCppParams(CppParams cppParams) {
        this.cppParams = cppParams;
    }

    public CppGenomeType getGenomeType() {
        return genomeType;
    }

    public void setGenomeType(CppGenomeType genomeType) {
        this.genomeType = genomeType;
    }

    public Integer getGenomeSize() {
        switch (genomeType) {
            case NS:
                return GenomeNS.getSize(cppParams);
            case SSB:
                return GenomeSSB.getSize(cppParams);
            case PS:
                return GenomePS.getSize(cppParams);
        }
        return null;
    }

    public CppSolution decodeAndEvaluate(Solution moeaSolution) {
        final Individual individual = individualFactory.newIndividual();

        final GenomeVectorDouble01 genome = (GenomeVectorDouble01) individual.getGenome();
        final CppSolution cppSolution = (CppSolution) individual.getSolution();
        final CostDouble quality = (CostDouble) individual.getQuality();

        genome.setGeneValues(moeaSolution);
        new CppEvaluationFunction(cppParams, genomeType)
                .evaluate(genome, cppSolution, quality);

        return cppSolution;
    }

    /*
    MOEA Framework problem
     */

    public AbstractProblem getMoeaFrameworkProblem() {
        return new AbstractProblem(getGenomeSize(), 1) {
            private CppEvaluationFunction evaluationFunction = new CppEvaluationFunction(cppParams, genomeType);
            private Individual individual = individualFactory.newIndividual();

            @Override
            public Solution newSolution() {
                Solution solution = new Solution(getNumberOfVariables(),
                        getNumberOfObjectives());

                for (int i = 0; i < getNumberOfVariables(); i++) {
                    solution.setVariable(i, new RealVariable(0.0, 1.0));
                }

                return solution;
            }

            @Override
            public void evaluate(Solution solution) {
                final GenomeVectorDouble01 genome = (GenomeVectorDouble01) individual.getGenome();
                final CppSolution cppSolution = (CppSolution) individual.getSolution();
                final CostDouble quality = (CostDouble) individual.getQuality();

                genome.setGeneValues(solution);
                evaluationFunction.evaluate(genome, cppSolution, quality);

                solution.setObjectives(new double[]{quality.get()});
            }
        };
    }
}
