package com.vanolucas.cppsolver.problem;

import com.vanolucas.cppsolver.instance.CppParams;
import com.vanolucas.cppsolver.problem.OffsetPlate;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Stores an actual solution of the Cover Printing Problem.
 */
public class CppSolution {
    /**
     * Cover Printing Problem instance params.
     */
    private CppParams cppParams;

    /**
     * Offset plates of this solution.
     */
    private List<OffsetPlate> plates;

    /*
    Constructors
     */

    public CppSolution(CppParams cppParams) {
        this.cppParams = cppParams;

        this.plates = IntStream.range(0, cppParams.getNbPlates())
                .mapToObj(i -> new OffsetPlate())
                .collect(Collectors.toList());
    }

    /*
    Accessors
     */

    public CppParams getCppParams() {
        return cppParams;
    }

    public void setCppParams(CppParams cppParams) {
        this.cppParams = cppParams;
    }

    public List<OffsetPlate> getPlates() {
        return plates;
    }

    public void setPlates(List<OffsetPlate> plates) {
        this.plates = plates;
    }

    /**
     * Get the total nb of printings to realize this solution.
     *
     * @return Total nb of printings required to realize this solution.
     */
    public int getNbPrintings() {
        return plates.stream()
                .mapToInt(OffsetPlate::getNbPrintings)
                .sum();
    }

    /**
     * Calculate the total cost of realizing this solution.
     *
     * @return The total cost to realize this solution.
     */
    public double getCost() {
        return plates.size() * cppParams.getCostOffsetPlate() + getNbPrintings() * cppParams.getCostOnePrinting();
    }

    /*
    Commands
     */

    /**
     * Allocate the provided slots to this solution's offset plates.
     *
     * @param slots The slots to distribute on this solution's offset plates.
     */
    public void setSlots(List<Slot> slots) {
        final int NB_SLOTS_PER_PLATE = cppParams.getNbSlotsPerPlate();
        final int NB_PLATES = cppParams.getNbPlates();

        // for each offset plate to fill
        for (int p = 0; p < NB_PLATES; p++) {
            // fetch the current plate
            final OffsetPlate plate = plates.get(p);

            // grab the slots to allocate to this plate
            final int firstSlot = p * NB_SLOTS_PER_PLATE;
            final List<Slot> slotsThisPlate = slots.subList(firstSlot, firstSlot + NB_SLOTS_PER_PLATE);

            // allocate the slots to this plate
            plate.setSlots(slotsThisPlate);
        }
    }

    /*
    toString
     */

    @Override
    public String toString() {
        final String formatCppSolution = "(%d offset plates, %d printings):\n%s\n" +
                "Book covers actually printed:\n%s\n" +
                "Total book covers actually printed: %d (demand was %d)\n" +
                "Total waste: %d/%d (%.4f%%)\n" +
                "Total cost: %.2f";
        final String formatOffsetPlate = "\t%3d: %s";
        final String formatBookCoverDetail = "\t\"%s\": %d/%d (waste: %d) on plates: %s";

        final String strOffsetPlates = IntStream.range(0, plates.size())
                .mapToObj(plateIndex -> String.format(formatOffsetPlate, plateIndex + 1, plates.get(plateIndex).toString()))
                .collect(Collectors.joining("\n"));

        final String strBookCoverDetails = cppParams.getBooks().stream()
                .map(b -> {
                    final int actualPrintings = plates.stream()
                            .mapToInt(p -> p.getNbPrintingsOfBook(b))
                            .sum();
                    return String.format(formatBookCoverDetail,
                            b.getName(),
                            actualPrintings,
                            b.getDemand(),
                            actualPrintings - b.getDemand(),
                            IntStream.range(0, plates.size())
                                    .filter(plateIndex -> plates.get(plateIndex).hasBook(b))
                                    .mapToObj(plateIndex -> String.valueOf(plateIndex + 1))
                                    .collect(Collectors.joining(","))
                    );
                })
                .collect(Collectors.joining("\n"));

        final int TOTAL_BOOK_COVERS_PRINTED = plates.stream()
                .mapToInt(p -> p.getNbPrintings() * p.getNbSlots())
                .sum();
        final int TOTAL_DEMAND = cppParams.getTotalDemand();
        final int NB_WASTED_PRINTINGS = TOTAL_BOOK_COVERS_PRINTED - TOTAL_DEMAND;
        final double WASTE_PERCENT = (double) NB_WASTED_PRINTINGS / (double) TOTAL_DEMAND * 100;
        final double TOTAL_COST = getCost();

        return String.format(formatCppSolution, // format
                plates.size(), // nb of offset plates
                getNbPrintings(), // nb of printings
                strOffsetPlates, // detail of offset plates
                strBookCoverDetails, // detail of book covers
                TOTAL_BOOK_COVERS_PRINTED, // total book covers printed
                TOTAL_DEMAND, // total book cover printings demanded
                NB_WASTED_PRINTINGS, // nb of book cover printings wasted
                TOTAL_DEMAND, // total book cover printings demanded
                WASTE_PERCENT, // percentage wasted
                TOTAL_COST // total cost of this solution
        );
    }
}
