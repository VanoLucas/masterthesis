package com.vanolucas.cppsolver.problem;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class OffsetPlate implements Comparable<OffsetPlate> {
    private List<Slot> slots;

    public OffsetPlate() {
        this(new ArrayList<>(4));
    }

    public OffsetPlate(List<Slot> slots) {
        this.slots = slots;
    }

    public List<Slot> getSlots() {
        return slots;
    }

    public void setSlots(List<Slot> slots) {
        this.slots = slots;
    }

    public int getNbSlots() {
        return slots.size();
    }

    public int getNbPrintings() {
        return slots.stream()
                .mapToInt(Slot::getNbPrintings)
                .max()
                .orElse(0);
    }

    public boolean hasBook(BookCover bookCover) {
        return slots.stream()
                .anyMatch(s -> s.getAllocatedBook().equals(bookCover));
    }

    public int getNbPrintingsOfBook(BookCover bookCover) {
        // count to how many slots the provided book cover is allocated
        final int COUNT_SLOTS = (int) slots.stream()
                .filter(s -> s.getAllocatedBook().equals(bookCover))
                .count();

        // calculate how many copies of this book cover is produced by this offset plate
        return COUNT_SLOTS * getNbPrintings();
    }

    @Override
    public int compareTo(OffsetPlate o) {
        return Integer.compare(getNbPrintings(), o.getNbPrintings());
    }

    @Override
    public String toString() {
        final String format = "(%7d printings): %s";
        final String strSlots = slots.stream()
                .map(Slot::toString)
                .collect(Collectors.joining(" | "));
        return String.format(format,
                getNbPrintings(),
                strSlots
        );
    }
}
