package com.vanolucas.cppsolver.problem;

public class Slot implements Comparable<Slot> {
    private BookCover allocatedBook;
    private int nbPrintings;

    public Slot(BookCover allocatedBook, int nbPrintings) {
        this.allocatedBook = allocatedBook;
        this.nbPrintings = nbPrintings;
    }

    public BookCover getAllocatedBook() {
        return allocatedBook;
    }

    public void setAllocatedBook(BookCover allocatedBook) {
        this.allocatedBook = allocatedBook;
    }

    public int getNbPrintings() {
        return nbPrintings;
    }

    public void setNbPrintings(int nbPrintings) {
        this.nbPrintings = nbPrintings;
    }

    public Slot splitInTwo(double cutFractionOut) {
        final int otherNbPrintings = (int) (cutFractionOut * nbPrintings);
        nbPrintings -= otherNbPrintings;
        return new Slot(allocatedBook, otherNbPrintings);
    }

    @Override
    public int compareTo(Slot o) {
        return Integer.compare(nbPrintings, o.nbPrintings);
    }

    @Override
    public String toString() {
        return String.format("\"%s\":%d",
                allocatedBook.getName(),
                nbPrintings
        );
    }
}
