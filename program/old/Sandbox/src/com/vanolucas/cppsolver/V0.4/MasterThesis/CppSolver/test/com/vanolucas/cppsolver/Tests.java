package com.vanolucas.cppsolver;

import com.vanolucas.cppsolver.eval.CppEvaluationFunction;
import com.vanolucas.cppsolver.genome.CppGenomeType;
import com.vanolucas.cppsolver.genome.GenomeNS;
import com.vanolucas.cppsolver.genome.GenomePS;
import com.vanolucas.cppsolver.genome.GenomeSSB;
import com.vanolucas.cppsolver.instance.CppParams;
import com.vanolucas.cppsolver.instance.CppParams30;
import com.vanolucas.cppsolver.instance.CppParams40;
import com.vanolucas.cppsolver.mutation.MutationPSPair;
import com.vanolucas.cppsolver.problem.CppInstance;
import com.vanolucas.cppsolver.problem.CppSolution;
import com.vanolucas.opti.algo.Algo;
import com.vanolucas.opti.algo.AlgoEvolutionary;
import com.vanolucas.opti.algo.AlgoHillClimbing;
import com.vanolucas.opti.crossover.Crossover;
import com.vanolucas.opti.crossover.CrossoverCoupleUniform;
import com.vanolucas.opti.eval.EvaluationFunction;
import com.vanolucas.opti.genome.GenomeVector;
import com.vanolucas.opti.genome.GenomeVectorDouble01;
import com.vanolucas.opti.grouping.Grouping;
import com.vanolucas.opti.grouping.GroupingCouplesRandom;
import com.vanolucas.opti.monitor.Monitor;
import com.vanolucas.opti.mutation.Mutation;
import com.vanolucas.opti.mutation.MutationRandomGenes;
import com.vanolucas.opti.mutation.MutationSingleGene;
import com.vanolucas.opti.quality.CostDouble;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.selection.Selector;
import com.vanolucas.opti.selection.SelectorProbaByRankArithmetic;
import com.vanolucas.opti.selection.SelectorRouletteWheel;
import com.vanolucas.opti.solution.Individual;
import com.vanolucas.opti.solution.IndividualFactory;
import com.vanolucas.opti.util.Picker;
import org.junit.jupiter.api.Test;
import org.moeaframework.Executor;
import org.moeaframework.core.NondominatedPopulation;
import org.moeaframework.problem.AbstractProblem;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;

class Tests {

    @Test
    void hillClimbing() {
        // cover printing problem instance params
        final CppParams cppParams = new CppParams30(10);

        // local search params
        final int NB_NEIGHBORS = 93;

        // evaluation function
        EvaluationFunction evaluationFunction = new CppEvaluationFunction(cppParams, CppGenomeType.PS);

        // mutation/neighboring operators
        List<Mutation> mutationOperators = IntStream.range(0, NB_NEIGHBORS)
                .mapToObj(i -> {
                    if (i % 2 == 0) {
                        return new MutationSingleGene();
                    } else {
                        return new MutationRandomGenes(2d / GenomePS.getSize(cppParams));
                    }
                })
                .collect(Collectors.toList());

        // individual factory
        IndividualFactory individualFactory = new IndividualFactory() {
            @Override
            public GenomeVector newGenome() {
                return new GenomePS(cppParams);
            }

            @Override
            public CostDouble newQuality() {
                return new CostDouble();
            }

            @Override
            public CppSolution newSolution() {
                return new CppSolution(cppParams);
            }
        };

        // local search algo
        Algo algo = new AlgoHillClimbing(evaluationFunction, mutationOperators, individualFactory);

        // listener
        algo.addListener(new AlgoHillClimbing.Listener() {
            private String allTimeBest;

            @Override
            public void onNewBetterIndividualFound(Individual newBetterIndividual, long iteration) {
                allTimeBest = newBetterIndividual.toString();
//                System.out.println("Iteration: " + iteration + "\n" + allTimeBest);
            }

            @Override
            public void onLocalSearchEnd(long iteration, Individual currentSolution, Individual bestNeighbor) {
//                System.out.println("Iteration: " + iteration + "\n" +
//                        "All time best: " + allTimeBest + "\n" +
//                        "Current solution: " + currentSolution.getQuality() + "\n" +
//                        "Best neighbor: " + bestNeighbor.getQuality());
            }
        });

        // run algo
//        algo.run();
        algo.runUntilIteration(3);
    }

    @Test
    void hillClimbingTweakedMutations() {
        // cover printing problem instance params
        final CppParams cppParams = new CppParams30(10);
//        final CppParams cppParams = new CppParams40();

        // evaluation function
        EvaluationFunction evaluationFunction = new CppEvaluationFunction(cppParams, CppGenomeType.PS);

        // mutation / neighboring operators
        List<Mutation> mutationOperators = new ArrayList<Mutation>() {{
            addAll(DoubleStream.iterate(0.01d, p -> p + 0.0025d)
                    .mapToObj(MutationRandomGenes::new)
                    .limit(4) // 6, 4, 5
                    .collect(Collectors.toList())
            );
            addAll(DoubleStream.iterate(0.05d, p -> p + 0.0025d)
                    .mapToObj(MutationRandomGenes::new)
                    .limit(3) // 2, 3, 3
                    .collect(Collectors.toList())
            );
            addAll(DoubleStream.iterate(0.9d, p -> p + 0.01d)
                    .mapToObj(MutationRandomGenes::new)
                    .limit(2) // 4, 2, 2
                    .collect(Collectors.toList())
            );
        }};
        mutationOperators.addAll(
                Stream.of(new MutationPSPair())
                        .limit(7) // 6, 7, 9
                        .collect(Collectors.toList())
        );

        // individual factory
        IndividualFactory individualFactory = new IndividualFactory() {
            @Override
            public GenomeVector newGenome() {
                return new GenomePS(cppParams);
            }

            @Override
            public CostDouble newQuality() {
                return new CostDouble();
            }

            @Override
            public CppSolution newSolution() {
                return new CppSolution(cppParams);
            }
        };

        // local search algo
        Algo algo = new AlgoHillClimbing(evaluationFunction, mutationOperators, individualFactory);

        // listener
        algo.addListener(new AlgoHillClimbing.Listener() {
            private String allTimeBest;
            private int countLocalSearches = 0;

            @Override
            public void onNewBetterIndividualFound(Individual newBetterIndividual, long iteration) {
                allTimeBest = newBetterIndividual.toString();
//                System.out.println("Iteration: " + iteration + "\n" + allTimeBest);
            }

            @Override
            public void onLocalSearchEnd(long iteration, Individual currentSolution, Individual bestNeighbor) {
                countLocalSearches++;
//                System.out.println(
//                        "Iteration: " + iteration + "\n" +
//                                "Current solution: " + currentSolution.getQuality() + "\n" +
//                                "Best neighbor: " + bestNeighbor.getQuality() + "\n" +
//                                "All time best: " + allTimeBest + "\n" +
//                                "Local search: " + countLocalSearches
//                );
            }
        });

        // run algo
//        algo.runFor(Duration.of(5L, ChronoUnit.MINUTES));
        algo.runUntilIteration(3);
    }

    @Test
    void hillClimbingTweakedMutationsMonitor() {
        // cover printing problem instance params
        final CppParams cppParams = new CppParams30(10);
//        final CppParams cppParams = new CppParams40();

        // evaluation function
//        EvaluationFunction evaluationFunction = new CppEvaluationFunction(cppParams, CppGenomeType.NS);
//        EvaluationFunction evaluationFunction = new CppEvaluationFunction(cppParams, CppGenomeType.SSB);
        EvaluationFunction evaluationFunction = new CppEvaluationFunction(cppParams, CppGenomeType.PS);

        // mutation / neighboring operators
        List<Mutation> mutationOperators = new ArrayList<Mutation>() {{
            addAll(DoubleStream.iterate(0.01d, p -> p + 0.0025d)
                            .mapToObj(MutationRandomGenes::new)
//                    .limit(80)
                            .limit(40)
                            .collect(Collectors.toList())
            );
            addAll(DoubleStream.iterate(0.05d, p -> p + 0.0025d)
                            .mapToObj(MutationRandomGenes::new)
//                    .limit(80)
                            .limit(40)
                            .collect(Collectors.toList())
            );
            addAll(DoubleStream.iterate(0.9d, p -> p + 0.01d)
                    .mapToObj(MutationRandomGenes::new)
                    .limit(5)
                    .collect(Collectors.toList())
            );
        }};
        mutationOperators.addAll(
                Stream.of(new MutationPSPair())
                        .limit(100)
                        .collect(Collectors.toList())
        );

        // individual factory
        IndividualFactory individualFactory = new IndividualFactory() {
            @Override
            public GenomeVector newGenome() {
//                return new GenomeNS(cppParams);
//                return new GenomeSSB(cppParams);
                return new GenomePS(cppParams);
            }

            @Override
            public CostDouble newQuality() {
                return new CostDouble();
            }

            @Override
            public CppSolution newSolution() {
                return new CppSolution(cppParams);
            }
        };

        // monitor
        Duration runDuration = Duration.of(100L, ChronoUnit.MILLIS);
//        Duration runDuration = Duration.of(15L, ChronoUnit.SECONDS);
//        Duration runDuration = Duration.of(20L, ChronoUnit.SECONDS);
        Duration fetchDataPeriod = Duration.of(1L, ChronoUnit.SECONDS);
        Monitor monitor = new Monitor(runDuration, fetchDataPeriod);

        // experiment runs
        final int NB_RUNS = 1;
//        final int NB_RUNS = Integer.MAX_VALUE;
        Quality bestQualityYet = individualFactory.newQuality();

        for (int run = 0; run < NB_RUNS; run++) {

            // local search algo
            Algo algo = new AlgoHillClimbing(evaluationFunction, mutationOperators, individualFactory);

            // listener
            algo.addListener(new AlgoHillClimbing.Listener() {
                @Override
                public void onNewBetterIndividualFound(Individual newBetterIndividual, long iteration) {
                    if (newBetterIndividual.getQuality().isBetterThan(bestQualityYet)) {
//                        System.out.println("NEW BETTER: " + newBetterIndividual + "\n");
                        bestQualityYet.copyFrom(newBetterIndividual.getQuality());
                    }
                }
            });

            monitor.setAlgo(algo);
            monitor.runAlgo();

//            System.out.println(monitor);
//            System.out.println("Current best: " + bestQualityYet);
        }
    }


    @Test
    void evolutionary() {
        // cover printing problem instance params
        final CppParams cppParams = new CppParams30(10);
//        final CppParams cppParams = new CppParams40();

        // evolutionary algo params
        final int POPULATION_SIZE = 18;
        final double CROSSOVER_PROBA = 0.8;
        final double CHILD_MUTATION_PROBA = 0.1;
        final int MAX_PARETO_FRONT_SIZE = 1;

        // evaluation function
        EvaluationFunction evaluationFunction = new CppEvaluationFunction(cppParams, CppGenomeType.PS);

        // selection operator
        Selector selector = new SelectorProbaByRankArithmetic();

        // grouping operator
        Grouping grouping = new GroupingCouplesRandom();

        // crossover operator
        Crossover crossoverOperator = new CrossoverCoupleUniform(1.0d);
        List<Crossover> crossoverOperators = new ArrayList<Crossover>(1) {{
            add(crossoverOperator);
        }};
        Picker<Crossover> crossoverPicker = new Picker<>(crossoverOperators);

        // mutation operator
        Mutation mutationOperator = new MutationSingleGene();
        List<Mutation> mutationOperators = new ArrayList<Mutation>(1) {{
            add(mutationOperator);
        }};
        Picker<Mutation> mutationPicker = new Picker<>(mutationOperators);

        // individual factory
        IndividualFactory individualFactory = new IndividualFactory() {
            @Override
            public GenomePS newGenome() {
                return new GenomePS(cppParams);
            }

            @Override
            public CostDouble newQuality() {
                return new CostDouble();
            }

            @Override
            public CppSolution newSolution() {
                return new CppSolution(cppParams);
            }
        };

        // evolutionary algorithm
        AlgoEvolutionary algo = new AlgoEvolutionary(
                evaluationFunction,
                selector,
                grouping,
                crossoverPicker,
                mutationPicker,
                individualFactory,
                POPULATION_SIZE,
                CROSSOVER_PROBA,
                CHILD_MUTATION_PROBA,
                MAX_PARETO_FRONT_SIZE
        );

        // listeners
        algo.addListener(new AlgoEvolutionary.Listener() {
            @Override
            public void onNewBetterIndividualFound(Individual newBetterIndividual, long iteration) {
//                System.out.println("Iteration: " + iteration + "\n" + newBetterIndividual + "\n");
            }
        });

        // run algo
//        algo.runUntilIteration(3);
//        algo.runFor(Duration.of(7L, ChronoUnit.SECONDS));
        algo.runMaxDurationWithoutImprovement(Duration.of(50L, ChronoUnit.MILLIS));
    }

    @Test
    void evolutionaryMonitor() {
        // cover printing problem instance params
        final CppParams cppParams = new CppParams30(10);

        // evolutionary algo params
        final int POPULATION_SIZE = 20;
        final double CROSSOVER_PROBA = 0.8;
        final double CHILD_MUTATION_PROBA = 0.1;
        final int MAX_PARETO_FRONT_SIZE = 1;

        // evaluation function
        EvaluationFunction evaluationFunction = new CppEvaluationFunction(cppParams, CppGenomeType.PS);

        // selection operator
        Selector selector = new SelectorProbaByRankArithmetic();
//        Selector selector = new SelectorRouletteWheel();

        // grouping operator
        Grouping grouping = new GroupingCouplesRandom();

        // crossover operator
        List<Crossover> crossoverOperators = Arrays.asList(new CrossoverCoupleUniform(1.0d));
        Picker<Crossover> crossoverPicker = new Picker<>(crossoverOperators);

        // mutation operator
        List<Mutation> mutationOperators = Arrays.asList(
                new MutationSingleGene()
                , new MutationRandomGenes(2d / GenomePS.getSize(cppParams))
        );
        Picker<Mutation> mutationPicker = new Picker<>(mutationOperators);

        // individual factory
        IndividualFactory individualFactory = new IndividualFactory() {
            @Override
            public GenomePS newGenome() {
                return new GenomePS(cppParams);
            }

            @Override
            public CostDouble newQuality() {
                return new CostDouble();
            }

            @Override
            public CppSolution newSolution() {
                return new CppSolution(cppParams);
            }
        };

        // monitor
        Duration runDuration = Duration.of(100L, ChronoUnit.MILLIS);
//        Duration runDuration = Duration.of(10L, ChronoUnit.SECONDS);
        Duration fetchDataPeriod = Duration.of(1L, ChronoUnit.SECONDS);
        Monitor monitor = new Monitor(runDuration, fetchDataPeriod);

        // experiment runs
        final int NB_RUNS = 1;
//        final int NB_RUNS = 10;
        Quality bestYet = individualFactory.newQuality();
        for (int run = 0; run < NB_RUNS; run++) {
            // evolutionary algorithm
            Algo algo = new AlgoEvolutionary(evaluationFunction, selector, grouping, crossoverPicker, mutationPicker, individualFactory,
                    POPULATION_SIZE, CROSSOVER_PROBA, CHILD_MUTATION_PROBA, MAX_PARETO_FRONT_SIZE);

            // listener
            algo.addListener(new AlgoEvolutionary.Listener() {
                @Override
                public void onNewBetterIndividualFound(Individual newBetterIndividual, long iteration) {
                    if (newBetterIndividual.getQuality().isBetterThan(bestYet)) {
//                        System.out.println("New better: " + newBetterIndividual + "\n");
                        bestYet.copyFrom(newBetterIndividual.getQuality());
                    }
                }
            });

            monitor.setAlgo(algo);
            monitor.runAlgo();

//            System.out.println(monitor);
        }
    }

    @Test
    void evolutionaryTweakedMutationMonitor() {
        // cover printing problem instance params
        final CppParams cppParams = new CppParams30(10);

        // evolutionary algo params
        final int POPULATION_SIZE = 20;
        final double CROSSOVER_PROBA = 0.8d;
        final double CHILD_MUTATION_PROBA = 0.1d;
        final int MAX_PARETO_FRONT_SIZE = 1;

        // evaluation function
        EvaluationFunction evaluationFunction = new CppEvaluationFunction(cppParams, CppGenomeType.PS);

        // selection operator
        Selector selector = new SelectorProbaByRankArithmetic(1d, 0.1d);
//        Selector selector = new SelectorProbaByRankArithmetic(1d, 10d);

//        Selector selector = new SelectorProbaByRankGeometric(1d, 1.10d);
//        Selector selector = new SelectorProbaByRankGeometric(20d, 1.10d);
//        Selector selector = new SelectorProbaByRankGeometric(1d, 2.00d);

        // grouping operator
        Grouping grouping = new GroupingCouplesRandom();

        // crossover operator
        List<Crossover> crossoverOperators = Arrays.asList(new CrossoverCoupleUniform(1.0d));
        Picker<Crossover> crossoverPicker = new Picker<>(crossoverOperators);

        // mutation operators
        List<Mutation> mutationOperators = new ArrayList<Mutation>() {{
            addAll(DoubleStream.iterate(0.01d, p -> p + 0.0025d)
                    .mapToObj(p -> new MutationRandomGenes(p))
                    .limit(80)
                    .collect(Collectors.toList())
            );
            addAll(DoubleStream.iterate(0.05d, p -> p + 0.0025d)
                    .mapToObj(p -> new MutationRandomGenes(p))
                    .limit(80)
                    .collect(Collectors.toList())
            );
            addAll(DoubleStream.iterate(0.9d, p -> p + 0.01d)
                    .mapToObj(p -> new MutationRandomGenes(p))
                    .limit(5)
                    .collect(Collectors.toList())
            );
        }};
        Picker<Mutation> mutationPicker = new Picker<>(mutationOperators);

        // individual factory
        IndividualFactory individualFactory = new IndividualFactory() {
            @Override
            public GenomePS newGenome() {
                return new GenomePS(cppParams);
            }

            @Override
            public CostDouble newQuality() {
                return new CostDouble();
            }

            @Override
            public CppSolution newSolution() {
                return new CppSolution(cppParams);
            }
        };

        // monitor
        Duration runDuration = Duration.of(100L, ChronoUnit.MILLIS);
//        Duration runDuration = Duration.of(5L, ChronoUnit.SECONDS);
        Duration fetchDataPeriod = Duration.of(1L, ChronoUnit.SECONDS);
        Monitor monitor = new Monitor(runDuration, fetchDataPeriod);

        // experiment runs
        final int NB_RUNS = 1;
//        final int NB_RUNS = Integer.MAX_VALUE;
        Quality bestYet = individualFactory.newQuality();
        for (int run = 0; run < NB_RUNS; run++) {
            // evolutionary algorithm
            Algo algo = new AlgoEvolutionary(evaluationFunction, selector, grouping, crossoverPicker, mutationPicker, individualFactory,
                    POPULATION_SIZE, CROSSOVER_PROBA, CHILD_MUTATION_PROBA, MAX_PARETO_FRONT_SIZE);

            // listener
            algo.addListener(new AlgoEvolutionary.Listener() {
                @Override
                public void onNewBetterIndividualFound(Individual newBetterIndividual, long iteration) {
                    if (newBetterIndividual.getQuality().isBetterThan(bestYet)) {
//                        System.out.println("New better: " + newBetterIndividual + "\n");
                        bestYet.copyFrom(newBetterIndividual.getQuality());
                    }
                }
            });

            monitor.setAlgo(algo);
            monitor.runAlgo();

//            System.out.println(monitor);
//            System.out.println("Current best: " + bestYet);
        }
    }

    @Test
    void moea() {
        CppParams cppParams = new CppParams30();
        CppGenomeType genomeType = CppGenomeType.PS;

        CppInstance cppInstance = new CppInstance(cppParams, genomeType);

        AbstractProblem problem = cppInstance.getMoeaFrameworkProblem();

        Executor executor = new Executor()
                .withProblem(problem)
                .withAlgorithm("GA").withProperty("populationSize", 50)
//                .withAlgorithm("NSGA-II")
//                .withAlgorithm("IBEA")
//                .withAlgorithm("NSGA-III")
//                .withAlgorithm("AbYSS")
//                .withAlgorithm("FastPGA")
//                .withAlgorithm("MOCell")
                .withMaxTime(5000);

        CppSolution best = null;

        while (true) {
            final NondominatedPopulation result = executor.run();
            final CppSolution cppSolution = cppInstance.decodeAndEvaluate(result.get(0));

            if (best == null
                    || cppSolution.getCost() < best.getCost()) {
                best = cppSolution;
            }

            System.out.println("Current: " + cppSolution);
            System.out.println("Best: " + best);
        }
    }

}
