package com.vanolucas.cppsolver.experiments;

import com.vanolucas.cppsolver.instance.CppParams;
import com.vanolucas.cppsolver.instance.CppParams30;
import com.vanolucas.cppsolver.instance.CppParams40;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ExpGenomeDecodingPerfTest {

    @Test
    void run() {
        final long NB_GENOMES_TO_DECODE = 100L;
//        final long NB_GENOMES_TO_DECODE = 2000000L;
//        final long NB_GENOMES_TO_DECODE = 10000000L;

        CppParams cppParams = new CppParams30(10, 4);
//        CppParams cppParams = new CppParams40(14, 4);

        ExpGenomeDecodingPerf experiment = new ExpGenomeDecodingPerf(cppParams, NB_GENOMES_TO_DECODE);

        experiment.run();
    }

}
