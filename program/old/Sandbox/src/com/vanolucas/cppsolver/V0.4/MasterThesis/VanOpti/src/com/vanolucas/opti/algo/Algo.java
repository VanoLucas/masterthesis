package com.vanolucas.opti.algo;

import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.solution.Individual;
import com.vanolucas.opti.util.ObservableTS;
import com.vanolucas.opti.util.ParetoFront;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * Base class for an optimization algorithm.
 */
public abstract class Algo {

    /*
    Defaults
     */

    private static final int DEFAULT_MAX_PARETO_FRONT_SIZE = 5;

    /*
    Listener
     */

    /**
     * Listener interface to create objects that react to this Optimization Algorithm's events.
     */
    public interface Listener {
        default void onRun(long iteration) {
        }

        default void onIterationStart(long iteration, ParetoFront<Quality> bestQualities) {
        }

        default void onIterationEnd(long iteration, ParetoFront<Quality> bestQualities) {
        }

        default void onNewBetterIndividualFound(Individual newBetterIndividual, long iteration) {
        }

        default void onNewEquivalentIndividualFound(Individual equivalentIndividual, long iteration) {
        }

        default void onPause(long iteration, ParetoFront<Quality> bestQualities) {
        }
    }

    /*
    Attributes
     */

    private boolean isRunning = false;

    /**
     * Current iteration number of this algorithm.
     */
    protected long iteration = 0l;
    /**
     * Pareto front of the best known solution qualities.
     */
    protected ParetoFront<Quality> bestQualities;
    protected Object lockBestQualities;

    /**
     * Time at which the run() started.
     */
    private Instant instantRun;
    /**
     * Time at which the last better solution was found.
     */
    private Instant instantLastImprovement;

    /**
     * Iteration numbers at the end of which this optimization algorithm should pause.
     */
    private List<Long> pauseAfterIterations = new ArrayList<>(1);
    /**
     * When we find a solution equivalent or better than one of these Quality values,
     * this optimization algorithm should pause.
     */
    private List<Quality> pauseAtTargetQualities = new ArrayList<>(1);
    /**
     * When we find a solution strictly better than one of these Quality values,
     * this optimization algorithm should pause.
     */
    private List<Quality> pauseAtQualitiesBetterThan = new ArrayList<>(1);
    /**
     * Instant in time after which this optimization algorithm should pause.
     */
    private Instant pauseAtInstant;
    /**
     * Pause when this optimization algorithm does not find any better solution during this duration.
     */
    private Duration pauseWhenNoImprovementForDuration;

    /**
     * Make this Algo observable so that listeners can react to its events.
     */
    protected ObservableTS<Listener> observable;

    /*
    Constructors
     */

    public Algo() {
        this(DEFAULT_MAX_PARETO_FRONT_SIZE);
    }

    public Algo(final int maxParetoFrontSize) {
        this.bestQualities = new ParetoFront<>(maxParetoFrontSize);
        this.lockBestQualities = new Object();

        this.observable = new ObservableTS<>();
    }

    /*
    Accessors
     */

    public boolean isRunning() {
        return isRunning;
    }

    public long getIteration() {
        return iteration;
    }

    public Quality getLastBetterSolutionFoundQuality() {
        synchronized (lockBestQualities) {
            return bestQualities.getLastBetterItemAdded();
        }
    }

    /*
    Observable
     */

    public void addListener(Listener listener) {
        observable.addListener(listener);
    }

    public void removeListener(Listener listener) {
        observable.removeListener(listener);
    }

    /*
    Commands
     */

    public void run() {
        isRunning = true;

        // notify listeners that we start running
        observable.notifyListeners(listener -> listener.onRun(iteration));

        // store the time at which we started this run
        instantRun = Instant.now();

        // run this optimization algorithm until we reach a pause criterion
        while (!shouldPause()) {
            // start next iteration
            iteration++;

            // notify listeners that we start a new iteration
            observable.notifyListeners(listener -> listener.onIterationStart(iteration, bestQualities));

            // execute one iteration of this optimization algorithm
            runIteration();

            // notify listeners that we finished the current iteration
            observable.notifyListeners(listener -> listener.onIterationEnd(iteration, bestQualities));
        }

        // we get here when run gets on pause
        onPause();

        isRunning = false;
    }

    public void runUntilIteration(long iteration) {
        pauseAfterIteration(iteration);
        run();
    }

    public void runUntilQuality(Quality targetQuality) {
        pauseOnReachingQualityBetterOrEquivalentTo(targetQuality);
        run();
    }

    public void runFor(Duration minDuration) {
        pauseIn(minDuration);
        run();
    }

    public void runMaxDurationWithoutImprovement(Duration pauseAfterDurationWithoutImprovement) {
        pauseWhenNoImprovementFor(pauseAfterDurationWithoutImprovement);
        run();
    }

    /*
    Pause Commands
     */

    /**
     * Request to pause at the end of the current iteration
     */
    public void pause() {
        pauseAfterIteration(iteration);
    }

    /**
     * Request this optimization algorithm to pause at the end of the specified iteration number.
     *
     * @param iteration Iteration number at the end of which this optimization algorithm should pause.
     */
    public void pauseAfterIteration(long iteration) {
        if (!pauseAfterIterations.contains(iteration)) {
            pauseAfterIterations.add(iteration);
        }
    }

    public void pauseOnReachingQualityBetterOrEquivalentTo(Quality targetQuality) {
        // add this target quality only if we don't already have another target that is equivalent
        if (!pauseAtTargetQualities.stream()
                .anyMatch(q -> q.isEquivalentTo(targetQuality))) {
            pauseAtTargetQualities.add(targetQuality);
        }
    }

    public void pauseOnReachingQualityBetterThan(Quality qualityToSurpass) {
        pauseAtQualitiesBetterThan.add(qualityToSurpass);
    }

    public void pauseIn(Duration duration) {
        pauseAt(Instant.now().plus(duration));
    }

    public void pauseAt(Instant instant) {
        pauseAtInstant = instant;
    }

    public void pauseWhenNoImprovementFor(Duration duration) {
        pauseWhenNoImprovementForDuration = duration;
    }

    /*
    Helpers
     */

    /**
     * Override this to implement an iteration of an optimization algorithm.
     */
    protected abstract void runIteration();

    /**
     * Submit a candidate Solution to be compared against the current best known solution Qualities.
     * Best known qualities are stored in a Pareto Front collection.
     *
     * @param individual Candidate Solution to compare against best known qualities to date.
     * @return Result of the insertion of the new solution in the Pareto Front of best qualities.
     */
    protected ParetoFront.NewItemResult proposeNewSolution(Individual individual) {
        final ParetoFront.NewItemResult RESULT;

        synchronized (lockBestQualities) {
            RESULT = individual.proposeQualityInParetoFront(bestQualities);
        }

        // if we found a new better solution, log the time of this discovery
        if (RESULT == ParetoFront.NewItemResult.DOMINANT) {
            instantLastImprovement = Instant.now();
        }

        // notify listeners if the new solution we found is good
        switch (RESULT) {
            // notify listeners that we found a new better solution
            case DOMINANT:
                observable.notifyListeners(listener -> listener.onNewBetterIndividualFound(individual, iteration));
                break;
            // notify listeners that we found a new equivalent solution
            case EQUIVALENT_AND_KEPT:
            case EQUIVALENT_BUT_NOT_KEPT:
                observable.notifyListeners(listener -> listener.onNewEquivalentIndividualFound(individual, iteration));
                break;
        }

        return RESULT;
    }

    /**
     * Check if this optimization algorithm should pause before next iteration.
     *
     * @return True if this optimization algorithm should pause running before next iteration.
     */
    private boolean shouldPause() {
        // check if we should pause at this iteration number
        if (pauseAfterIterations.contains(iteration)) {
            return true;
        }

        // check if we should pause because we reached a target quality
        // i.e. if any of the best known qualities if equivalent or better than a target
        if (bestQualities.stream()
                .anyMatch(bestQuality -> pauseAtTargetQualities.stream()
                        .anyMatch(targetQuality -> bestQuality.isEquivalentOrBetterThan(targetQuality)))) {
            return true;
        }

        // check if we should pause because we surpassed a target quality
        // i.e. if any of the best known qualities is better than a target
        if (bestQualities.stream()
                .anyMatch(bestQuality -> pauseAtQualitiesBetterThan.stream()
                        .anyMatch(targetQuality -> bestQuality.isBetterThan(targetQuality)))) {
            return true;
        }

        // check if we should pause because the pause instant was reached
        if (pauseAtInstant != null
                && Instant.now().isAfter(pauseAtInstant)) {
            return true;
        }

        // check if we should pause because the last improvement was too long ago
        if (pauseWhenNoImprovementForDuration != null
                && instantLastImprovement != null
                && Duration.between(instantLastImprovement, Instant.now())
                .compareTo(pauseWhenNoImprovementForDuration) > 0) {
            return true;
        }

        // we reached no pause criterion yet, keep going
        return false;
    }

    /**
     * When a run of this optimization algorithm starts to pause,
     * perform actions that have to be done when pausing.
     */
    private void onPause() {
        // remove request to pause at this iteration number if any
        pauseAfterIterations.remove(iteration);

        // remove request to pause at target quality if we reached it
        pauseAtTargetQualities.removeIf(targetQuality -> bestQualities.stream()
                .anyMatch(bestQuality -> bestQuality.isEquivalentOrBetterThan(targetQuality))
        );

        // remove request to pause at quality better than target if we reached it
        pauseAtQualitiesBetterThan.removeIf(targetQuality -> bestQualities.stream()
                .anyMatch(bestQuality -> bestQuality.isBetterThan(targetQuality))
        );

        // remove request to pause at target instant if we reached it
        if (pauseAtInstant != null
                && Instant.now().isAfter(pauseAtInstant)) {
            pauseAtInstant = null;
        }

        // notify listeners that we paused
        observable.notifyListeners(listener -> listener.onPause(iteration, bestQualities));
    }

}
