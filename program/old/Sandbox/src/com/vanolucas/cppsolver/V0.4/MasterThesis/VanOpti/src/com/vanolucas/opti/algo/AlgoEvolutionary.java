package com.vanolucas.opti.algo;

import com.vanolucas.opti.crossover.Crossover;
import com.vanolucas.opti.eval.EvaluationFunction;
import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.grouping.Grouping;
import com.vanolucas.opti.mutation.Mutation;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.random.RandGenerator;
import com.vanolucas.opti.selection.Selector;
import com.vanolucas.opti.solution.Family;
import com.vanolucas.opti.solution.Individual;
import com.vanolucas.opti.solution.IndividualFactory;
import com.vanolucas.opti.util.ParetoFront;
import com.vanolucas.opti.util.Picker;
import com.vanolucas.opti.util.Pool;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class AlgoEvolutionary extends Algo {

    /*
    Listener
     */

    /**
     * Listener interface to create objects that react to this Genetic Evolutionary Algorithm's events.
     */
    public interface Listener extends Algo.Listener {
        default void onGenerationStart(long generation, ParetoFront<Quality> bestQualities) {
        }

        default void onGenerationEnd(long generation, Set<Individual> population, ParetoFront<Quality> bestQualities) {
        }
    }

    /*
    Attributes
     */

    private int populationSize;
    private double crossoverProba;
    private double childMutationProba;

    private Set<Individual> population;
    private Pool<Individual> individualsPool;

    private EvaluationFunction evaluationFunction;
    private Selector selector;
    private Grouping grouping;
    private Picker<Crossover> crossoverPicker;
    private Picker<Mutation> mutationPicker;

    private boolean randomizePopulation = true;

    private RandGenerator rand;

    /*
    Constructors
     */

    public AlgoEvolutionary(EvaluationFunction evaluationFunction,
                            Selector selector,
                            Grouping grouping,
                            Picker<Crossover> crossoverPicker,
                            Picker<Mutation> mutationPicker,
                            IndividualFactory individualFactory,
                            final int populationSize,
                            final double crossoverProba,
                            final double childMutationProba,
                            final int maxParetoFrontSize) {
        super(maxParetoFrontSize);

        this.populationSize = populationSize;
        setCrossoverProba(crossoverProba);
        setChildMutationProba(childMutationProba);

        this.individualsPool = new Pool<>(individualFactory, populationSize * 2);
        this.evaluationFunction = evaluationFunction;
        this.selector = selector;
        this.grouping = grouping;
        this.crossoverPicker = crossoverPicker;
        this.mutationPicker = mutationPicker;
    }

    /*
    Accessors
     */

    public void setCrossoverProba(double crossoverProba) {
        if (crossoverProba < 0 || crossoverProba > 1) {
            throw new IllegalArgumentException("The crossover probability must be in the range [0..1].");
        }
        this.crossoverProba = crossoverProba;
    }

    public void setChildMutationProba(double childMutationProba) {
        if (childMutationProba < 0 || childMutationProba > 1) {
            throw new IllegalArgumentException("The child mutation probability must be in the range [0..1].");
        }
        this.childMutationProba = childMutationProba;
    }

    /*
    Helpers
     */

    private void initIfNeeded() {
        if (population == null) {
            population = new HashSet<>();
        }

        rand = new RandGenerator();
    }

    @Override
    protected void runIteration() {
        initIfNeeded();

        // notify listeners that we start a new generation
        observable.notifyListeners(listener -> {
            if (listener instanceof Listener) {
                ((Listener) listener).onGenerationStart(iteration, bestQualities);
            }
        });

        // detect if we need to randomize the whole population at this iteration
        if (randomizePopulation) {
            runRandomizedGeneration();
            randomizePopulation = false;
        } else {
            runEvolutionaryGeneration();
        }

        // notify listeners that this generation ends
        observable.notifyListeners(listener -> {
            if (listener instanceof Listener) {
                ((Listener) listener).onGenerationEnd(iteration, population, bestQualities);
            }
        });
    }

    private void runRandomizedGeneration() {
        // resize population to its normal size if needed
        if (population.size() != populationSize) {
            individualsPool.returnToPool(population);
            population.clear();
            population.addAll(individualsPool.getNew(populationSize));
        }

        // randomize the whole population
        population.forEach(Individual::randomize);

        // evaluate and propose the whole population
        population.forEach(indiv -> {
            indiv.evaluateUsing(evaluationFunction);
            proposeNewSolution(indiv);
        });
    }

    private void runEvolutionaryGeneration() {
        /*
        Selection
         */

        // perform selection of individuals to keep
        List<Individual> selected;

        if (population.size() != populationSize) {
            selected = selector.select(populationSize, population);
        } else {
            selected = new ArrayList<>(population);
        }

        // remove selected individuals from population so only the unselected ones remain
        population.removeAll(selected);
        // return unselected individuals to pool for later reuse
        individualsPool.returnToPool(population);
        population.clear();
        // put selected individuals back into the current population
        population.addAll(selected);

        // form mating groups from the selected population
        List<List<Individual>> mates = grouping.makeGroups(population);

        /*
        Crossover
         */

        // put mates as potential parents into families
        List<Family> families = mates.stream()
                .map(group -> new Family(group, individualsPool))
                .collect(Collectors.toList());

        // perform crossover (reproduction) with proba
        families.forEach(family -> {
            if (rand.nextDouble01() < crossoverProba) {
                family.crossoverUsing(crossoverPicker.pickOne());
            }
        });

        // fetch children from all families
        List<Individual> children = families.stream()
                .flatMap(family -> family.getChildren().stream())
                .collect(Collectors.toList());

        /*
        Mutation
         */

        // perform mutation on children with proba
        children.forEach(child -> {
            if (rand.nextDouble01() < childMutationProba) {
                child.mutateUsing(mutationPicker.pickOne());
            }
        });

        /*
        Evaluation
         */

        // evaluate and propose all children
        children.forEach(child -> {
            child.evaluateUsing(evaluationFunction);
            proposeNewSolution(child);
        });

        /*
        Merge population
         */

        // get all parents that made children
        List<Individual> parents = families.stream()
                // select families that have children
                .filter(Family::hasChildren)
                // get their parents
                .flatMap(family -> family.getParents().stream())
                .collect(Collectors.toList());

        // remove all parents that made children from the population
        population.removeAll(parents);
        individualsPool.returnToPool(parents);

        // merge the children with the current population
        population.addAll(children);
    }
}
