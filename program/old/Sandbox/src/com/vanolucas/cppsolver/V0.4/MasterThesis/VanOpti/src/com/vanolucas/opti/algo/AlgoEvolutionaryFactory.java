package com.vanolucas.opti.algo;

public interface AlgoEvolutionaryFactory extends AlgoFactory {
    @Override
    default AlgoEvolutionary newAlgo() {
        return newAlgoEvolutionary();
    }

    AlgoEvolutionary newAlgoEvolutionary();
}
