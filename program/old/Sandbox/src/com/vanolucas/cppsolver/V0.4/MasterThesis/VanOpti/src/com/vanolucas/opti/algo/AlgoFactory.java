package com.vanolucas.opti.algo;

import com.vanolucas.opti.eval.EvaluationFunctionFactory;
import com.vanolucas.opti.solution.IndividualFactory;

public interface AlgoFactory
        extends IndividualFactory, EvaluationFunctionFactory {
    @Override
    default Object getNew() {
        return newAlgo();
    }

    Algo newAlgo();
}
