package com.vanolucas.opti.algo;

import com.vanolucas.opti.eval.EvaluationFunction;
import com.vanolucas.opti.mutation.Mutation;
import com.vanolucas.opti.random.RandGenerator;
import com.vanolucas.opti.solution.Individual;
import com.vanolucas.opti.solution.IndividualFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class AlgoHillClimbing extends Algo {

    /*
    Defaults
     */

    private static final Variant DEFAULT_VARIANT = Variant.STEEPEST_ASCENT;

    /*
    Hill climbing variant
     */
    public enum Variant {
        FIRST_BETTER_NEIGHBOR,
        STEEPEST_ASCENT
    }

    /*
    Listener
     */

    /**
     * Listener interface to create objects that react to this Hill Climbing algorithm's events.
     */
    public interface Listener extends Algo.Listener {
        /**
         * Called when the local optimum is reached (this is when no more good neighbor can be found).
         *
         * @param iteration       Current iteration number (at which we found no more equivalent neighbor).
         * @param currentSolution The current solution in this local search algorithm.
         * @param bestNeighbor    The best neighbor found in the current iteration of the local search.
         */
        default void onLocalSearchEnd(long iteration, Individual currentSolution, Individual bestNeighbor) {
        }
    }

    /*
    Attributes
     */

    private Variant variant;

    private Individual currentSolution;
    private List<Individual> neighbors;
    private boolean randomizeAtNextIteration = true;

    private EvaluationFunction evaluationFunction;
    private List<Mutation> mutationOperators;

    private IndividualFactory individualFactory;

    private RandGenerator rand;

    /*
    Constructors
     */

    public AlgoHillClimbing(EvaluationFunction evaluationFunction,
                            List<Mutation> mutationOperators,
                            IndividualFactory individualFactory) {
        this(DEFAULT_VARIANT,
                evaluationFunction,
                mutationOperators,
                individualFactory);
    }

    public AlgoHillClimbing(Variant variant,
                            EvaluationFunction evaluationFunction,
                            List<Mutation> mutationOperators,
                            IndividualFactory individualFactory) {
        this.variant = variant;
        this.evaluationFunction = evaluationFunction;
        this.mutationOperators = mutationOperators;
        this.individualFactory = individualFactory;
    }

    /*
    Helpers
     */

    private void initIfNeeded() {
        // create the current solution if it doesn't exist yet
        if (currentSolution == null) {
            currentSolution = individualFactory.newIndividual();
        }

        // create the neighbors if they don't exist yet
        // or if we don't have the right number of them
        if (neighbors == null
                || neighbors.size() != mutationOperators.size()) {
            neighbors = IntStream.range(0, mutationOperators.size())
                    .mapToObj(__ -> individualFactory.newIndividual())
                    .collect(Collectors.toList());
        }
    }

    /**
     * Main method for an iteration of the hill climbing algorithm.
     */
    @Override
    protected void runIteration() {
        // initialize necessary components
        initIfNeeded();

        // if we need to randomize the current solution at this iteration
        if (randomizeAtNextIteration) {
            randomizeAtNextIteration = false;
            runRandomizedIteration();
        }
        // else run a normal hill climbing iteration
        else {
            switch (variant) {
                case STEEPEST_ASCENT:
                    runHillClimbingSteepestAscentIteration();
                    break;
                case FIRST_BETTER_NEIGHBOR:
                    runHillClimbingFirstBetterNeighborIteration();
                    break;
            }
        }
    }

    /**
     * Run randomized iteration: randomize the current
     * solution and evaluate it.
     */
    private void runRandomizedIteration() {
        // randomize current solution
        currentSolution.randomize();

        // evaluate current solution
        currentSolution.evaluateUsing(evaluationFunction);

        // propose current solution
        proposeNewSolution(currentSolution);
    }

    /**
     * Hill climbing iteration of the
     * using steepest ascent strategy:
     * evaluate all neighbors of the current solution
     * then move to the best solution among these neighbors.
     */
    private void runHillClimbingSteepestAscentIteration() {
        // copy current solution to neighbors
        neighbors
                .forEach(neighbor -> neighbor.copyGenomeFrom(currentSolution));

        // for each neighbor, mutate then evaluate it
        for (int n = 0; n < neighbors.size(); n++) {
            // fetch the neighbor and mutation operator to use on it
            Individual neighbor = neighbors.get(n);
            Mutation mutation = mutationOperators.get(n);

            // mutate neighbor
            neighbor.mutateUsing(mutation);
            // evaluate neighbor
            neighbor.evaluateUsing(evaluationFunction);
            // propose neighbor
            proposeNewSolution(neighbor);
        }

        // get the best neighbor
        final int BEST_NEIGHBOR_INDEX = (int) IntStream.range(0, neighbors.size())
                .boxed()
                .max(Comparator.comparing(neighbors::get))
                .get();
        final Individual bestNeighbor = neighbors.get(BEST_NEIGHBOR_INDEX);

        // if the best neighbor is at least as good as the current solution
        if (bestNeighbor.isEquivalentOrBetterThan(currentSolution)) {
            // move to best neighbor
            Individual tmp = currentSolution;
            currentSolution = neighbors.get(BEST_NEIGHBOR_INDEX);
            neighbors.set(BEST_NEIGHBOR_INDEX, tmp);
        }
        // else, no more good neighbor
        // randomize to start a fresh new local search at next iteration
        else {
            randomizeAtNextIteration = true;

            // notify listeners that we end the current local search
            observable.notifyListeners(listener -> {
                if (listener instanceof Listener) {
                    ((Listener) listener).onLocalSearchEnd(iteration, currentSolution, bestNeighbor);
                }
            });
        }
    }

    /**
     * Run a hill climbing iteration with the first better neighbor strategy.
     * As soon as a neighbor is better than the current solution, move to it even
     * if it may not be the best.
     */
    private void runHillClimbingFirstBetterNeighborIteration() {
        // to store indexes of all neighbors that have a quality equivalent to the current solution
        List<Integer> equivalentNeighbors = new ArrayList<>(neighbors.size());

        // for each neighbor
        for (int n = 0; n < neighbors.size(); n++) {
            // fetch the neighbor and mutation operator to use on it
            Individual neighbor = neighbors.get(n);
            Mutation mutation = mutationOperators.get(n);

            // copy current solution to neighbor
            neighbor.copyGenomeFrom(currentSolution);

            // mutate the neighbor
            neighbor.mutateUsing(mutation);
            // evaluate the neighbor
            neighbor.evaluateUsing(evaluationFunction);

            // if this neighbor is better than the current solution, move to it directly!
            if (neighbor.isBetterThan(currentSolution)) {
                // submit this neighbor to our best solutions
                proposeNewSolution(neighbor);
                // move to this neighbor
                Individual tmp = currentSolution;
                currentSolution = neighbor;
                neighbors.set(n, tmp);

                // no need to process further neighbors, done for this iteration
                return;
            }

            // else if this neighbor is equivalent to the current solution, just remember it for now
            else if (neighbor.isEquivalentTo(currentSolution)) {
                // submit this neighbor, it may be kept among best solutions
                proposeNewSolution(neighbor);
                // remember this neighbor's index
                equivalentNeighbors.add(n);
            }

            // we get here when this neighbor is worse: do nothing
        }

        // if we only found neighbors that are at best equivalent to the current solution,
        // randomly pick one to move to
        if (!equivalentNeighbors.isEmpty()) {
            // init random generator if needed
            if (rand == null) {
                rand = new RandGenerator();
            }

            // choose one of the equivalent neighbors at random
            final int CHOSEN_NEIGHBOR_INDEX = rand.pickOneAmong(equivalentNeighbors);
            final Individual chosenNeighbor = neighbors.get(CHOSEN_NEIGHBOR_INDEX);

            // move to this neighbor
            Individual tmp = currentSolution;
            currentSolution = chosenNeighbor;
            neighbors.set(CHOSEN_NEIGHBOR_INDEX, tmp);
        }

        // there is no more good neighbor
        else {
            randomizeAtNextIteration = true;

            // notify listeners that we end the current local search
            observable.notifyListeners(listener -> {
                if (listener instanceof Listener) {
                    final Individual bestNeighbor = Collections.max(neighbors);
                    ((Listener) listener).onLocalSearchEnd(iteration, currentSolution, bestNeighbor);
                }
            });
        }
    }
}
