package com.vanolucas.opti.crossover;

import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.solution.Family;

import java.util.List;

/**
 * Crossover operator that produces children out of parents.
 */
public interface Crossover {

    default void crossover(Family family) {
        crossover(family.getParentGenomes(), family);
    }

    void crossover(final List<? extends Genome> parents, Family family);

    default Genome newChild(Family family) {
        return family.newChild().getGenome();
    }
}
