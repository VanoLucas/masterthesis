package com.vanolucas.opti.crossover;

import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.solution.Family;

import java.util.List;

/**
 * Crossover operator that produces 2 children out of 2 parents.
 */
public interface CrossoverCouple extends Crossover {

    @Override
    default void crossover(final List<? extends Genome> parents, Family family) {
        crossover(parents.get(0), parents.get(1), family.newChild().getGenome(), family.newChild().getGenome());
    }

    void crossover(final Genome parent1, final Genome parent2, Genome child1, Genome child2);
}
