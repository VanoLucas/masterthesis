package com.vanolucas.opti.crossover;

import com.vanolucas.opti.genome.Gene;
import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.genome.GenomeVector;
import com.vanolucas.opti.random.RandGenerator;

public class CrossoverCoupleUniform implements CrossoverCouple {

    /*
    Defaults
     */

    private static final double DEFAULT_MIX_RATE = 1.0;

    /*
    Attributes
     */

    private final double mixRate;
    private RandGenerator rand;

    /*
    Constructors
     */

    public CrossoverCoupleUniform() {
        this(DEFAULT_MIX_RATE);
    }

    public CrossoverCoupleUniform(final double mixRate) {
        this.mixRate = mixRate;
    }

    /*
    Commands
     */

    @Override
    public void crossover(final Genome parent1, final Genome parent2, Genome child1, Genome child2) {
        crossover((GenomeVector) parent1, (GenomeVector) parent2, (GenomeVector) child1, (GenomeVector) child2);
    }

    public void crossover(final GenomeVector parent1, final GenomeVector parent2, GenomeVector child1, GenomeVector child2) {
        final int SIZE = parent1.size();

        if (rand == null) {
            rand = new RandGenerator();
        }

        // for each gene
        for (int g = 0; g < SIZE; g++) {
            // decide whether or not to swap this gene pair based on the mix rate
            if (rand.nextDouble01() < mixRate / 2) {
                // do a mix: put parent1 to child2 and parent2 to child1
                child1.getGene(g).copyFrom(parent2.getGene(g));
                child2.getGene(g).copyFrom(parent1.getGene(g));
            } else {
                // don't mix: put parent1 to child1 and parent2 to child2
                child1.getGene(g).copyFrom(parent1.getGene(g));
                child2.getGene(g).copyFrom(parent2.getGene(g));
            }
        }
    }
}
