package com.vanolucas.opti.eval;

import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.quality.Quality;

public interface EvaluationFunction extends GenomeDecoder {

    default void evaluate(final Genome genome, Object outSolution, Quality outQuality) {
        decode(genome, outSolution);
        evaluate(outSolution, outQuality);
    }

    void evaluate(final Object solution, Quality outQuality);
}
