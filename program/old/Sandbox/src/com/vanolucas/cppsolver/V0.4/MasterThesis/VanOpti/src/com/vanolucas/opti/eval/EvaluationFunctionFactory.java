package com.vanolucas.opti.eval;

import com.vanolucas.opti.util.Factory;

public interface EvaluationFunctionFactory extends GenomeDecoderFactory, Factory {

    @Override
    default Object getNew() {
        return newEvaluationFunction();
    }

    @Override
    default EvaluationFunction newGenomeDecoder() {
        return newEvaluationFunction();
    }

    EvaluationFunction newEvaluationFunction();
}
