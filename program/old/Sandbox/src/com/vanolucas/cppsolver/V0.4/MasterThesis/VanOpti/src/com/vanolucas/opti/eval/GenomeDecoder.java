package com.vanolucas.opti.eval;

import com.vanolucas.opti.genome.Genome;

public interface GenomeDecoder {
    void decode(final Genome genome, Object outSolution);
}
