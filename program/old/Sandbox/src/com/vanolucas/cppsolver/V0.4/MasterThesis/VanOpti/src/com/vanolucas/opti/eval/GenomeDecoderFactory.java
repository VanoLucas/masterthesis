package com.vanolucas.opti.eval;

import com.vanolucas.opti.util.Factory;

public interface GenomeDecoderFactory extends Factory {

    @Override
    default Object getNew() {
        return newGenomeDecoder();
    }

    GenomeDecoder newGenomeDecoder();
}
