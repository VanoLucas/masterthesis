package com.vanolucas.opti.genome;

import com.vanolucas.opti.random.RandGenerator;
import com.vanolucas.opti.random.Randomizable;
import com.vanolucas.opti.util.Copyable;

/**
 * Smallest piece of a Genome.
 * A gene is a simple atomic element of a Genome, it has a single value.
 * @param <T> T must be a primitive type (Double, Integer, String,...).
 */
public abstract class Gene<T> implements Randomizable, Copyable, Cloneable {

    /*
    Attributes
     */

    /**
     * Current value of this gene.
     */
    protected T value;

    /**
     * Random numbers generator.
     * Useful for randomization of this gene.
     */
    protected RandGenerator randGenerator;

    /*
    Constructors
     */

    /**
     * Construct this gene and set its value.
     *
     * @param value Value to set for this gene.
     */
    public Gene(T value) {
        this.value = value;
    }

    /*
    Accessors
     */

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    /*
    Commands
     */

    /**
     * Swap the values between this gene and the one provided as param.
     *
     * @param other The other gene to swap this one's value with.
     */
    public void swapValueWith(Gene<T> other) {
        T tmp = this.value;
        this.value = other.value;
        other.value = tmp;
    }

    /*
    Equals
     */

    @Override
    public boolean equals(Object other) {
        return equals((Gene<T>) other);
    }

    public boolean equals(Gene<T> other) {
        return value.equals(other.value);
    }

    /*
    Cloneable
     */

    @Override
    public abstract Gene<T> clone();

    /*
    toString
     */

    @Override
    public String toString() {
        return value.toString();
    }

    /*
    Helpers
     */

    protected RandGenerator randGenerator() {
        if (randGenerator == null) {
            randGenerator = new RandGenerator();
        }
        return randGenerator;
    }
}
