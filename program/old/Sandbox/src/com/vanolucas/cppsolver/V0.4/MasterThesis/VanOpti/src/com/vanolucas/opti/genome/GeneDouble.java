package com.vanolucas.opti.genome;

/**
 * A gene for which the value is a double number.
 */
public class GeneDouble extends Gene<Double> {

    /*
    Attributes
     */

    /**
     * Lowest possible value of this gene (inclusive).
     */
    private double lowerBound;
    /**
     * Highest possible value of this gene (exclusive).
     */
    private double higherBound;

    /*
    Constructors
     */

    /**
     * Construct this double gene by providing its range of allowed values.
     * The value of this gene is initialized with the lower bound value of the range.
     *
     * @param lowerBoundInclusive  Minimum value allowed for this double gene (inclusive).
     *                             This gene is also initialized with this value.
     * @param higherBoundExclusive Maximum value allowed for this double gene (exclusive).
     */
    public GeneDouble(final double lowerBoundInclusive,
                      final double higherBoundExclusive) {
        this(lowerBoundInclusive, lowerBoundInclusive, higherBoundExclusive);
    }

    /**
     * Construct this double gene, set its value and its range of allowed values.
     *
     * @param value                Value to set to this double gene.
     * @param lowerBoundInclusive  Minimum value allowed for this double gene (inclusive).
     * @param higherBoundExclusive Maximum value allowed for this double gene (exclusive).
     */
    public GeneDouble(final double value,
                      final double lowerBoundInclusive,
                      final double higherBoundExclusive) {
        // provide value to this gene
        super(value);

        // set range of allowed values
        this.lowerBound = lowerBoundInclusive;
        this.higherBound = higherBoundExclusive;

        // check that the range of allowed values is legal
        checkValidRange();
        // check that the value is within the allowed range
        checkValueWithinAllowedRange();
    }

    /*
    Sanity checks
     */

    /**
     * Check if the range of allowed values for this gene is legal.
     * Throw an exception if it isn't.
     * We need to call this to make sure the range is fine every time lowerBound or higherBound is set.
     */
    protected void checkValidRange() {
        if (Double.valueOf(higherBound - lowerBound).isInfinite()) {
            throw new IllegalArgumentException(
                    "The range of allowed values for this Gene is invalid. Probably too big.");
        }
    }

    /**
     * Check if the value of this gene falls within the range of allowed values.
     * Throw an exception if it doesn't.
     */
    private void checkValueWithinAllowedRange() {
        if (!isValueWithinAllowedRange()) {
            throw new IllegalArgumentException(
                    "The value set to this double gene is out of the allowed range.");
        }
    }

    /*
    Helpers
     */

    /**
     * Check if this gene's value falls within the range of allowed values.
     *
     * @return True if this gene's value falls within the range of allowed values. False otherwise.
     */
    private boolean isValueWithinAllowedRange() {
        return lowerBound <= this.value && this.value < higherBound;
    }

    /*
    Randomizable
     */

    /**
     * Randomize this gene's value within the allowed range.
     */
    @Override
    public void randomize() {
        this.value = randGenerator().nextDoubleInRange(lowerBound, higherBound);
    }

    /*
    Copyable
     */

    @Override
    public void copyFrom(Object other) {
        GeneDouble o = (GeneDouble) other;
        this.value = o.value;
        this.lowerBound = o.lowerBound;
        this.higherBound = o.higherBound;
    }

    /*
    Cloneable
     */

    @Override
    public GeneDouble clone() {
        return new GeneDouble(value, lowerBound, higherBound);
    }
}
