package com.vanolucas.opti.genome;

/**
 * A gene for which the value is a double number in the range 0..1.
 * o inclusive and 1 exclusive.
 */
public class GeneDouble01 extends GeneDouble {

    /*
    Constructors
     */

    /**
     * Construct this double gene and set its value to 0.
     */
    public GeneDouble01() {
        this(0d);
    }

    /**
     * Construct this double gene and set its value.
     *
     * @param value Value to set to this double gene (must be between 0 (inclusive) and 1 (exclusive)).
     */
    public GeneDouble01(final double value) {
        super(value, 0d, 1d);
    }

    /*
    Sanity checks
     */

    /**
     * No need to check if the range of allowed is valid for this gene
     * because we know the range 0..1 is always valid.
     */
    @Override
    protected void checkValidRange() {
    }

    /*
    Randomizable
     */

    /**
     * Randomize this gene's value within the 0..1 range.
     */
    @Override
    public void randomize() {
        this.value = randGenerator().nextDouble01();
    }

    /*
    Cloneable
     */

    @Override
    public GeneDouble01 clone() {
        return new GeneDouble01(value);
    }
}
