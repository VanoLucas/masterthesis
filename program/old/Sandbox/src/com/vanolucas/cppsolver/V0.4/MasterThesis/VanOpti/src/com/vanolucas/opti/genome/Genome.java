package com.vanolucas.opti.genome;


import com.vanolucas.opti.random.Randomizable;
import com.vanolucas.opti.util.Copyable;

/**
 * Encoded representation of a solution.
 * Useful to represent solutions so we can work with them in genetic algorithms.
 */
public interface Genome extends Randomizable, Copyable {

    boolean equals(Object other);
}
