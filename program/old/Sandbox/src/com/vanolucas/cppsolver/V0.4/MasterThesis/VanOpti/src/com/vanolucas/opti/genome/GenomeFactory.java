package com.vanolucas.opti.genome;

import com.vanolucas.opti.util.Factory;

public interface GenomeFactory extends Factory {

    @Override
    default Object getNew() {
        return newGenome();
    }

    Genome newGenome();
}
