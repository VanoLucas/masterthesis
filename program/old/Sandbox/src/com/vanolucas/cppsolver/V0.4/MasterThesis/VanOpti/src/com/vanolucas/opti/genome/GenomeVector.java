package com.vanolucas.opti.genome;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A genome for which the representation is a simple 1D vector of genes.
 *
 * @param <TGeneValue> Type of the gene values.
 */
public class GenomeVector<TGeneValue> implements Genome {

    /*
    Attributes
     */

    /**
     * Genes that compose this vector genome.
     */
    private List<Gene<TGeneValue>> genes;

    /*
    Constructors
     */

    /**
     * Construct this vector genome and store its genes.
     *
     * @param genes The genes that compose this vector genomes.
     */
    public GenomeVector(List<Gene<TGeneValue>> genes) {
        this.genes = genes;
    }

    /*
    Accessors
     */

    public Gene<TGeneValue> getGene(final int index) {
        return genes.get(index);
    }

    public List<TGeneValue> getValues() {
        return genes.stream()
                .map(Gene::getValue)
                .collect(Collectors.toList());
    }

    public List<TGeneValue> getValues(int fromIndex, int endIndex) {
        return genes.stream()
                .map(Gene::getValue)
                .skip(fromIndex)
                .limit(endIndex - fromIndex)
                .collect(Collectors.toList());
    }

    public void setGeneValue(int index, TGeneValue value) {
        genes.get(index).setValue(value);
    }

    public void setGeneValues(List<TGeneValue> values) {
        final int SIZE = values.size();

        if (SIZE != size()) {
            throw new IllegalArgumentException(
                    "Wrong number of gene values provided to this vector genome."
            );
        }

        for (int g = 0; g < SIZE; g++) {
            setGeneValue(g, values.get(g));
        }
    }

    public int size() {
        return genes.size();
    }

    public Stream<Gene<TGeneValue>> stream() {
        return genes.stream();
    }

    /*
    Randomizable
     */

    /**
     * Randomize this vector genome by randomizing all of its genes.
     */
    @Override
    public void randomize() {
        genes.forEach(Gene<TGeneValue>::randomize);
    }

    /*
    Equals
     */

    @Override
    public boolean equals(Object other) {
        return equals((GenomeVector<TGeneValue>) other);
    }

    public boolean equals(GenomeVector<TGeneValue> other) {
        return genes.equals(other.genes);
    }

    /*
    Copyable
     */

    @Override
    public void copyFrom(Object other) {
        GenomeVector<TGeneValue> o = (GenomeVector<TGeneValue>) other;

        final int SIZE = genes.size();
        final int OTHER_SIZE = o.genes.size();

        if (SIZE != OTHER_SIZE) {
            genes = o.genes.stream()
                    .map(gene -> gene.clone())
                    .collect(Collectors.toList());
        } else {
            for (int g = 0; g < SIZE; g++) {
                genes.get(g).copyFrom(o.genes.get(g));
            }
        }
    }

    /*
    toString
     */

    @Override
    public String toString() {
        String str = "(%d genes): %s";
        final String strGenes = genes.stream()
                .map(Gene::toString)
                .collect(Collectors.joining(","));
        return String.format(str, size(), strGenes);
    }
}
