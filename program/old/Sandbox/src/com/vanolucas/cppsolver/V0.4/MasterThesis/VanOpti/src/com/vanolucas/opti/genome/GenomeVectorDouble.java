package com.vanolucas.opti.genome;

import org.moeaframework.core.Solution;
import org.moeaframework.core.variable.EncodingUtils;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * A vector genome for which all genes are double numbers.
 */
public class GenomeVectorDouble extends GenomeVector<Double> {

    /*
    Constructors
     */

    /**
     * Construct this double vector genome with the specified length and initialize all genes to the provided value.
     *
     * @param length                Size of this double vector genome (number of genes that compose it).
     * @param initValue             Default initial value to set to all double genes of this vector genome.
     * @param minGeneValueInclusive Minimum allowed value for the genes (inclusive).
     * @param maxGeneValueExclusive Maximum allowed value for the genes (exclusive).
     */
    public GenomeVectorDouble(final int length,
                              final double initValue,
                              final double minGeneValueInclusive,
                              final double maxGeneValueExclusive) {
        this(
                IntStream.range(0, length)
                        .mapToObj(gene -> new GeneDouble(initValue, minGeneValueInclusive, maxGeneValueExclusive))
                        .collect(Collectors.toList())
        );
    }

    /**
     * Construct this double vector genome and store its genes.
     *
     * @param genes Genes to store in this double vector genome.
     */
    public GenomeVectorDouble(List<Gene<Double>> genes) {
        super(genes);
    }

    public void setGeneValues(Solution moeaSolution) {
        double[] geneValues = EncodingUtils.getReal(moeaSolution);
        setGeneValues(geneValues);
    }

    public void setGeneValues(double[] geneValues) {
        final int SIZE = geneValues.length;

        if (SIZE != size()) {
            throw new IllegalArgumentException(
                    "Wrong number of gene values provided to this vector of double genome."
            );
        }

        for (int g = 0; g < SIZE; g++) {
            setGeneValue(g, geneValues[g]);
        }
    }
}
