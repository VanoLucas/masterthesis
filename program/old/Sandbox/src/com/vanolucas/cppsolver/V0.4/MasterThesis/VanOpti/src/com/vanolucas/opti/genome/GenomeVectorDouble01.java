package com.vanolucas.opti.genome;

import com.vanolucas.opti.random.RandGenerator;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * A double vector genome for which all gene values must be in the range 0 (inclusive) to 1 (exclusive).
 */
public class GenomeVectorDouble01 extends GenomeVectorDouble {

    /*
    Attributes
     */

    private RandGenerator rand;

    /*
    Constructors
     */

    /**
     * Construct this vector genome of double numbers in the range 0..1.
     *
     * @param length Size of this vector genome (its number of genes).
     */
    public GenomeVectorDouble01(final int length) {
        this(length, 0d);
    }

    /**
     * Construct this double vector genome of the specified size and initialize all of its genes to the provided value.
     *
     * @param length    Size of this vector genome (its number of genes).
     * @param initValue Value to initialize all double genes of this genome with (must be in 0..1).
     */
    public GenomeVectorDouble01(final int length, final double initValue) {
        super(
                IntStream.range(0, length)
                        .mapToObj(gene -> new GeneDouble01(initValue))
                        .collect(Collectors.toList())
        );
    }
}
