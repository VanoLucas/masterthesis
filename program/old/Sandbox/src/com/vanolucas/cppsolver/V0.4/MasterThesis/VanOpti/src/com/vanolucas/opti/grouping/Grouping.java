package com.vanolucas.opti.grouping;

import com.vanolucas.opti.solution.Individual;

import java.util.Collection;
import java.util.List;

public interface Grouping {
    List<List<Individual>> makeGroups(Collection<Individual> population);
}
