package com.vanolucas.opti.grouping;

import com.vanolucas.opti.solution.Individual;
import com.vanolucas.opti.util.Picker;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class GroupingCouplesRandom implements Grouping {
    @Override
    public List<List<Individual>> makeGroups(Collection<Individual> population) {
        final int POPULATION_SIZE = population.size();

        Picker<Individual> individualPicker = new Picker<>(new ArrayList<>(population));

        // calculate number of couples to make
        final int NB_COUPLES;
        if (POPULATION_SIZE % 2 == 0) {
            NB_COUPLES = POPULATION_SIZE / 2;
        } else {
            NB_COUPLES = POPULATION_SIZE / 2 + 1;
        }

        // make couples
        return IntStream.range(0, NB_COUPLES)
                .mapToObj(i -> new ArrayList<Individual>(2) {{
                    add(individualPicker.pickOne());
                    add(individualPicker.pickOne());
                }})
                .collect(Collectors.toList());
    }
}
