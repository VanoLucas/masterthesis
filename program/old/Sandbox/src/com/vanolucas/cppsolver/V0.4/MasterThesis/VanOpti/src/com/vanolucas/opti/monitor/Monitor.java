package com.vanolucas.opti.monitor;

import com.vanolucas.opti.algo.Algo;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.util.ParetoFront;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Monitor an optimization algorithm.
 */
public class Monitor {

    /**
     * Optimization algorithm to monitor.
     */
    private Algo algo;

    private Duration runDuration;
    private Duration fetchDataPeriod;

    private Instant instantRunStarted;
    private Instant instantPaused;

    /**
     * History of the evolution of the best quality for each algo run.
     */
    private List<List<Quality>> qualityHistory;

    private boolean stopMonitoring = false;
    private Object lockMonitoring = new Object();

    /**
     * Handler to react to events from the monitored optimization algorithm.
     */
    private Algo.Listener algoHandler = new Algo.Listener() {
        @Override
        public void onRun(long iteration) {
            instantRunStarted = Instant.now();
            qualityHistory.add(new ArrayList<>());
            startMonitoring();
        }

        @Override
        public void onPause(long iteration, ParetoFront<Quality> bestQualities) {
            instantPaused = Instant.now();
            stopMonitoring = true;
        }

        @Override
        public void onIterationEnd(long iteration, ParetoFront<Quality> bestQualities) {
            // if our algo has been running for the target run time
            if (Duration.between(instantRunStarted, Instant.now()).compareTo(runDuration) > 0) {
                // we can pause it
                algo.pause();
            }
        }
    };

    public Monitor(Duration fetchDataPeriod) {
        this(null, fetchDataPeriod);
    }

    public Monitor(Duration runDuration, Duration fetchDataPeriod) {
        this(null, runDuration, fetchDataPeriod);
    }

    public Monitor(Algo algo, Duration runDuration, Duration fetchDataPeriod) {
        setAlgo(algo);

        this.runDuration = runDuration;
        this.fetchDataPeriod = fetchDataPeriod;

        this.qualityHistory = new ArrayList<>();
    }

    public void setAlgo(Algo algo) {
        if (this.algo != null) {
            this.algo.removeListener(algoHandler);
        }

        this.algo = algo;

        if (this.algo != null) {
            this.algo.addListener(algoHandler);

            if (this.algo.isRunning()) {
                startMonitoring();
            }
        }
    }

    public void setFetchDataPeriod(Duration fetchDataPeriod) {
        this.fetchDataPeriod = fetchDataPeriod;
    }

    public void runAlgo() {
        algo.run();
    }

    public void startMonitoring() {
        // start monitoring thread if when not already running
        synchronized (lockMonitoring) {
            stopMonitoring = false;
            ExecutorService executor = Executors.newSingleThreadExecutor();

            executor.submit(this::monitoringThread);
        }
    }

    /**
     * Monitoring thread
     */
    public void monitoringThread() {
        synchronized (lockMonitoring) {
            List<Quality> qualities = qualityHistory.get(qualityHistory.size() - 1);

            while (!stopMonitoring) {
                try {
                    Thread.sleep(fetchDataPeriod.toMillis());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                qualities.add(algo.getLastBetterSolutionFoundQuality().clone());
            }
        }
    }

    /**
     * Calculate median quality at each history point across all runs.
     *
     * @return
     */
    public List<Quality> getMedianQualityHistory() {
        final int MAX_POINTS = (int) qualityHistory.stream()
                .mapToInt(List::size)
                .average()
                .orElse(0d);

        // for each point
        return IntStream.range(0, MAX_POINTS)
                .mapToObj(point -> {
                    // get all runs that got to that point
                    List<Quality> sortedQualities = qualityHistory.stream()
                            .filter(run -> run.size() > point)
                            // get quality for each run at that point
                            .map(run -> run.get(point))
                            // sort by quality
                            .sorted()
                            .collect(Collectors.toList());

                    // get median quality for the current point
                    return sortedQualities.get(sortedQualities.size() / 2);
                })
                .collect(Collectors.toList());
    }

    @Override
    public String toString() {
        String str = qualityHistory.size() + " runs\n";
        str += "Last run took " + Duration.between(instantRunStarted, instantPaused).getSeconds() + " seconds.\n";
        str += getMedianQualityHistory().stream()
                .map(quality -> {
                    String qualityStr = quality.toString();
                    if (qualityStr.endsWith(".0")) {
                        return qualityStr.replace(".0", "");
                    } else {
                        return qualityStr;
                    }
                })
                .collect(Collectors.joining(","));

        return str;
    }
}
