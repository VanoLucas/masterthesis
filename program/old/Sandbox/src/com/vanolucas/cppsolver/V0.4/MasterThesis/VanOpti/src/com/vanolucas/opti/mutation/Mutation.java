package com.vanolucas.opti.mutation;

import com.vanolucas.opti.genome.Genome;

public interface Mutation {
    void mutate(Genome genome);
}
