package com.vanolucas.opti.mutation;

import com.vanolucas.opti.util.Factory;

public interface MutationFactory extends Factory {
    @Override
    default Object getNew() {
        return newMutation();
    }

    Mutation newMutation();
}
