package com.vanolucas.opti.mutation;

import com.vanolucas.opti.genome.Gene;

public interface MutationGene {
    void mutate(Gene gene);
}
