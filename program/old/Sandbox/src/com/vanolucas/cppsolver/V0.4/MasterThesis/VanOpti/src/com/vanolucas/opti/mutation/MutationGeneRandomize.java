package com.vanolucas.opti.mutation;

import com.vanolucas.opti.genome.Gene;

public class MutationGeneRandomize implements MutationGene {
    @Override
    public void mutate(Gene gene) {
        gene.randomize();
    }
}
