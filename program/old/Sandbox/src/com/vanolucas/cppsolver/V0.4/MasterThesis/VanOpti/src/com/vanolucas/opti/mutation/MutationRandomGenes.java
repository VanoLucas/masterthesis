package com.vanolucas.opti.mutation;

import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.genome.GenomeVector;
import com.vanolucas.opti.random.RandGenerator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MutationRandomGenes extends MutationSequence {

    private List<MutationGene> geneMutations;
    private double geneMutationProba;
    private RandGenerator rand;

    public MutationRandomGenes(double geneMutationProba) {
        this(Arrays.asList(new MutationGeneRandomize()),
                geneMutationProba
        );
    }

    public MutationRandomGenes(List<MutationGene> geneMutations, double geneMutationProba) {
        this.geneMutations = geneMutations;
        this.geneMutationProba = geneMutationProba;
    }

    public void setGeneMutations(List<MutationGene> geneMutations) {
        this.geneMutations = geneMutations;
    }

    public void setGeneMutationProba(double geneMutationProba) {
        this.geneMutationProba = geneMutationProba;
    }

    private void updateMutationSequence(List<Integer> geneIndexes) {
        if (geneMutations == null
                || geneMutations.isEmpty()) {
            setMutationSequence(null);
            return;
        }

        final int NB_GENES_TO_MUTATE = geneIndexes.size();
        List<Mutation> mutationSequence = new ArrayList<>(NB_GENES_TO_MUTATE);

        int mutationOperatorCursor = 0;
        // for each gene mutation to operate
        for (int m = 0; m < NB_GENES_TO_MUTATE; m++) {
            // construct its single gene mutation operator
            mutationSequence.add(new MutationSingleGene(
                    geneMutations.get(mutationOperatorCursor),
                    geneIndexes.get(m)
            ));
            // move to the next mutation operator available
            mutationOperatorCursor++;
            if (mutationOperatorCursor >= geneMutations.size()) {
                mutationOperatorCursor = 0;
            }
        }

        setMutationSequence(mutationSequence);
    }

    @Override
    public void mutate(Genome genome) {
        mutate((GenomeVector) genome);
    }

    public void mutate(GenomeVector genome) {
        final int SIZE = genome.size();

        if (rand == null) {
            rand = new RandGenerator();
        }

        // for each gene of this genome
        List<Integer> indexGenesToMutate = IntStream.range(0, SIZE)
                // decide whether or not to mutate it
                .filter(geneIndex -> rand.nextDouble01() < geneMutationProba)
                .boxed()
                .collect(Collectors.toList());

        updateMutationSequence(indexGenesToMutate);
        super.mutate(genome);
    }
}
