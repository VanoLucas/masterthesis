package com.vanolucas.opti.mutation;

import com.vanolucas.opti.genome.Genome;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MutationSequence implements Mutation {

    /*
    Attributes
     */

    protected List<Mutation> mutationSequence;

    /*
    Constructors
     */

    public MutationSequence() {
        this(new ArrayList<>(2));
    }

    public MutationSequence(Mutation mutation1, Mutation mutation2) {
        setMutationSequence(mutation1, mutation2);
    }

    public MutationSequence(List<Mutation> mutationSequence) {
        setMutationSequence(mutationSequence);
    }

    /*
    Accessors
     */

    public void setMutationSequence(Mutation mutation1, Mutation mutation2) {
        setMutationSequence(
                Arrays.asList(mutation1, mutation2)
        );
    }

    public void setMutationSequence(List<Mutation> mutationSequence) {
        this.mutationSequence = mutationSequence;
    }

    /*
    Commands
     */

    @Override
    public void mutate(Genome genome) {
        mutationSequence.forEach(mutation -> mutation.mutate(genome));
    }
}
