package com.vanolucas.opti.mutation;

import com.vanolucas.opti.genome.Gene;
import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.genome.GenomeVector;
import com.vanolucas.opti.random.RandGenerator;

public class MutationSingleGene implements Mutation {

    private MutationGene geneMutation;
    private Integer geneIndex;
    private RandGenerator rand;

    public MutationSingleGene() {
        this(new MutationGeneRandomize());
    }

    public MutationSingleGene(MutationGene geneMutation) {
        this(geneMutation, null);
    }

    public MutationSingleGene(MutationGene geneMutation, Integer geneIndex) {
        this.geneMutation = geneMutation;
        this.geneIndex = geneIndex;
    }

    public void setIndexGeneToMutate(Integer geneIndex) {
        this.geneIndex = geneIndex;
    }

    @Override
    public void mutate(Genome genome) {
        GenomeVector genomeVector = (GenomeVector) genome;
        mutate(genomeVector);
    }

    public void mutate(GenomeVector genomeVector) {
        // if no gene index is provided, use random gene mode
        if (geneIndex == null) {
            if (rand == null) {
                rand = new RandGenerator();
            }
            final int geneIndex = rand.nextIntBetween0And(genomeVector.size());
            geneMutation.mutate(genomeVector.getGene(geneIndex));
        }

        // otherwise just mutate the gene at the provided index
        else {
            geneMutation.mutate(genomeVector.getGene(geneIndex));
        }
    }
}
