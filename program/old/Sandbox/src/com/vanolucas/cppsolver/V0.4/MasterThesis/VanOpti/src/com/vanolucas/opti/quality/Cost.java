package com.vanolucas.opti.quality;

public abstract class Cost<TValue> extends QualitySingle<TValue> {

    /*
    Constructors
     */

    /**
     * Construct this cost.
     *
     * @param value Value to set for this cost.
     */
    public Cost(final TValue value) {
        super(value);
    }

}
