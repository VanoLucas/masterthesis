package com.vanolucas.opti.quality;

public class CostDouble extends Cost<Double> {

    /*
    Constructor
     */

    public CostDouble() {
        this(Double.MAX_VALUE);
    }

    /**
     * Construct this cost.
     *
     * @param value Double value to set for this cost.
     */
    public CostDouble(final double value) {
        super(value);
    }

    /*
    Comparison
     */

    @Override
    public boolean isBetterThan(Object other) {
        return isBetterThan((CostDouble) other);
    }

    public boolean isBetterThan(CostDouble other) {
        return value < other.value;
    }

    /*
    Copyable
     */

    @Override
    public void copyFrom(Object other) {
        copyFrom((CostDouble) other);
    }

    public void copyFrom(CostDouble other) {
        this.value = other.value;
    }

    /*
    Cloneable
     */

    @Override
    public CostDouble clone() {
        return new CostDouble(value);
    }

    /*
    toString
     */

    @Override
    public String toString() {
        String str = super.toString();

        // remove any unnecessary .0 decimal digit
        if (str.endsWith(".0")) {
            return str.replace(".0", "");
        } else {
            return str;
        }
    }
}
