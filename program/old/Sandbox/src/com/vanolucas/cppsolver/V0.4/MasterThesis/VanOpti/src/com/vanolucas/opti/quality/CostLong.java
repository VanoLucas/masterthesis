package com.vanolucas.opti.quality;

public class CostLong extends Cost<Long> {

    /*
    Constructor
     */

    public CostLong() {
        super(Long.MAX_VALUE);
    }

    /**
     * Construct this cost.
     *
     * @param value Long value to set for this cost.
     */
    public CostLong(final long value) {
        super(value);
    }

    /*
    Comparison
     */

    @Override
    public boolean isBetterThan(Object other) {
        return isBetterThan((CostLong) other);
    }

    public boolean isBetterThan(CostLong other) {
        return value < other.value;
    }

    /*
    Copyable
     */

    @Override
    public void copyFrom(Object other) {
        copyFrom((CostLong) other);
    }

    public void copyFrom(CostLong other) {
        this.value = other.value;
    }

    /*
    Cloneable
     */

    @Override
    public CostLong clone() {
        return new CostLong(value);
    }
}
