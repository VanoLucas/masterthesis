package com.vanolucas.opti.quality;

import com.vanolucas.opti.util.Copyable;

/**
 * Represents the quality of a solution.
 */
public abstract class Quality implements Comparable, Copyable, Cloneable {

    /*
    Comparison
     */

    /**
     * Compare this quality to check if it is better than the one passed as parameter.
     * @param other Other object or quality.
     * @return True if this quality is strictly better than the other one. False otherwise.
     */
    public abstract boolean isBetterThan(Object other);

    /**
     * Checks if this quality is equivalent to the one passed as parameter.
     * @param other Other object or quality to compare this one to.
     * @return True if this quality is equivalent to the one passed as parameter. False otherwise.
     */
    public abstract boolean isEquivalentTo(Object other);

    /**
     * Checks if this quality is at least equivalent or better to the one passed as parameter.
     * @param other Other object or quality to compare this one to.
     * @return True if this quality is at least as good as the one passed as parameter. False otherwise.
     */
    public boolean isEquivalentOrBetterThan(Object other) {
        return isBetterThan(other) || isEquivalentTo(other);
    }

    /**
     * Checks if this quality is worse than the one passed as parameter.
     * @param other Other object or quality to compare this one to.
     * @return True if this quality is worse than the one passed as parameter. False otherwise.
     */
    public boolean isWorseThan(Object other) {
        return !isEquivalentOrBetterThan(other);
    }

    /**
     * Check if this quality dominates the one passed as parameter
     * @param other The other quality to compare this one to.
     * @return True if this quality dominates the other one. False otherwise.
     */
    public boolean dominates(Object other) {
        if (other instanceof Quality) {
            return isBetterThan((Quality) other);
        } else {
            throw new IllegalArgumentException(
                    "The parameter to dominates() must be a Quality.");
        }
    }

    /**
     * Check if this quality is dominated by the one passed as parameter.
     * @param other The other quality to compare this one to.
     * @return True if this quality is dominated by the other one. False otherwise.
     */
    public boolean isDominatedBy(Object other) {
        if (other instanceof Quality) {
            return isWorseThan((Quality) other);
        } else {
            throw new IllegalArgumentException(
                    "The parameter to isDominatedBy() must be a Quality.");
        }
    }

    /*
    Equals
     */

    /**
     * Check if this quality equals another, that is when they are equivalent.
     * @param other The other quality to compare this one to.
     * @return True if the two qualities are equivalent.
     */
    @Override
    public boolean equals(Object other) {
        return isEquivalentTo(other);
    }

    /*
    Comparable
     */

    /**
     * Compare this quality to the one passed as parameter.
     * @param o Other object or quality to compare this one to.
     * @return The result of the comparison between this quality and the param.
     */
    @Override
    public int compareTo(Object o) {
        if (isBetterThan(o)) {
            return 1;
        } else if (isEquivalentTo(o)) {
            return 0;
        } else {
            return -1;
        }
    }

    /**
     * Create a new deep copy of this Quality.
     * @return A deep copy of this Quality object.
     */
    public abstract Quality clone();

    /*
    toString
     */

    @Override
    public abstract String toString();
}
