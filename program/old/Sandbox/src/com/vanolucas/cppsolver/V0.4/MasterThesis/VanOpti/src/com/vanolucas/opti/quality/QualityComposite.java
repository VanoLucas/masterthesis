package com.vanolucas.opti.quality;

import java.util.List;
import java.util.stream.Collectors;

public class QualityComposite extends Quality {

    /*
    Attributes
     */

    /**
     * Component qualities that compose this composite quality.
     */
    private List<Quality> components;

    /*
    Constructors
     */

    public QualityComposite(List<Quality> components) {
        this.components = components;
    }

    /*
    Comparison
     */

    @Override
    public boolean isBetterThan(Object other) {
        if (other instanceof QualityComposite) {
            return isBetterThan((QualityComposite) other);
        } else if (other instanceof List) {
            return isBetterThan((List<Quality>) other);
        } else {
            throw new IllegalArgumentException(
                    "The parameter of isBetterThan() must either be a composite quality or quality components (list of qualities)."
            );
        }
    }

    public boolean isBetterThan(QualityComposite other) {
        return isBetterThan(other.components);
    }

    public boolean isBetterThan(List<Quality> other) {
        // get number of components in this composite quality
        final int NB_COMPONENTS = components.size();

        // verify that the two composite qualities we want to compare have the same number of components
        if (NB_COMPONENTS != other.size()) {
            throw new IllegalArgumentException(
                    "You can only compare two composite quality objects if they have the same number of components.");
        }

        // to remember if we find at least one component that is better than the other's
        boolean atLeastOneBetter = false;

        // are all components at least equivalent?
        for (int c = 0; c < NB_COMPONENTS; c++) {
            // if any quality component is worse than the other, we are not better
            if (this.components.get(c).isWorseThan(other.get(c))) {
                return false;
            }
            // we need at least one component to be better than the other to be better overall
            else if (!atLeastOneBetter && this.components.get(c).isBetterThan(other.get(c))) {
                atLeastOneBetter = true;
            }
        }

        return atLeastOneBetter;
    }

    @Override
    public boolean isEquivalentTo(Object other) {
        return !dominates(other) && !isDominatedBy(other);
    }

    /*
    Equals
     */

    @Override
    public boolean equals(Object other) {
        return equals((QualityComposite) other);
    }

    public boolean equals(QualityComposite other) {
        return components.equals(other.components);
    }

    /*
    Copyable
     */

    @Override
    public void copyFrom(Object other) {
        copyFrom((QualityComposite) other);
    }

    public void copyFrom(QualityComposite other) {
        final int SIZE = components.size();
        final int OTHER_SIZE = other.components.size();

        if (SIZE != OTHER_SIZE) {
            components = other.components.stream()
                    .map(component -> component.clone())
                    .collect(Collectors.toList());
        } else {
            for (int c = 0; c < SIZE; c++) {
                components.get(c).copyFrom(other.components.get(c));
            }
        }
    }

    /*
    Cloneable
     */

    /**
     * New deep copy of this composite quality and all of its components.
     *
     * @return A new deep clone of this composite quality.
     */
    @Override
    public QualityComposite clone() {
        // clone all components
        return new QualityComposite(components.stream()
                .map(component -> component.clone())
                .collect(Collectors.toList()));
    }

    /*
    toString
     */

    @Override
    public String toString() {
        return null; //todo
    }
}
