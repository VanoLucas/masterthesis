package com.vanolucas.opti.quality;

import com.vanolucas.opti.util.Factory;

public interface QualityFactory extends Factory {

    @Override
    default Object getNew() {
        return newQuality();
    }

    Quality newQuality();
}
