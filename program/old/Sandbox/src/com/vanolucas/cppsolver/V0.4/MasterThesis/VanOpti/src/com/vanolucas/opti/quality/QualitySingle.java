package com.vanolucas.opti.quality;

/**
 * Quality stored as a single value.
 * @param <TValue> Type of the value of this quality.
 */
public abstract class QualitySingle<TValue> extends Quality {

    /*
    Attributes
     */

    /**
     * Value of this quality.
     */
    protected TValue value;

    /*
    Constructors
     */

    /**
     * Construct this single quality.
     * @param value Value to set for this quality.
     */
    public QualitySingle(final TValue value) {
        this.value = value;
    }

    /*
    Accessors
     */

    public TValue get() {
        return value;
    }

    public void set(TValue value) {
        this.value = value;
    }

    /*
    Comparison
     */

    @Override
    public boolean isEquivalentTo(Object other) {
        return isEquivalentTo((QualitySingle<TValue>) other);
    }

    public boolean isEquivalentTo(QualitySingle<TValue> other) {
        return value.equals(other.value);
    }

    /*
    Cloneable
     */

    @Override
    public abstract QualitySingle<TValue> clone();

    /*
    toString
     */

    @Override
    public String toString() {
        return value.toString();
    }
}
