package com.vanolucas.opti.random;

import java.util.List;
import java.util.Random;
import java.util.stream.DoubleStream;

/**
 * Convenience class to generate random numbers.
 */
public class RandGenerator {

    /*
    Attributes
     */

    /**
     * Java random numbers generator.
     */
    private Random random;
    /**
     * Optional seed for the Java random numbers generator.
     */
    private Long seed;

    /*
    Constructors
     */

    /**
     * The Java random will be constructed with a random seed.
     */
    public RandGenerator() {
        this(null);
    }

    /**
     * The Java random will be constructed with the provided seed.
     *
     * @param seed Seed for the Java random numbers generator.
     */
    public RandGenerator(final Long seed) {
        this.seed = seed;
    }

    /*
    Helpers
     */

    /**
     * Init the Java random numbers generator if it isn't created yet.
     */
    private void initRandomGeneratorIfNeeded() {
        // if random generator not initialized yet
        if (random == null) {
            // if we have no seed
            if (seed == null) {
                random = new Random();
            }
            // else use our seed
            else {
                random = new Random(seed);
            }
        }
    }

    /*
    Commands
     */

    /**
     * Generate a random int between 0 (inclusive) and max (exclusive).
     *
     * @param maxExclusive The max value (excluded).
     * @return An integer between 0 (inclusive) and max (exclusive).
     */
    public int nextIntBetween0And(final int maxExclusive) {
        initRandomGeneratorIfNeeded();
        return random.nextInt(maxExclusive);
    }

    /**
     * Generate a random double between 0 (inclusive) and 1 (exclusive).
     *
     * @return A random double between 0 (inclusive) and 1 (exclusive).
     */
    public double nextDouble01() {
        initRandomGeneratorIfNeeded();
        return random.nextDouble();
    }

    public DoubleStream doubles01(long count) {
        initRandomGeneratorIfNeeded();
        return random.doubles(count);
    }

    /**
     * Generate a random double between provided min (inclusive) and max (exclusive) values.
     *
     * @param minInclusive Minimum allowed value of the random double to be generated (inclusive).
     * @param maxExclusive Maximum values of the random double to be generated (exclusive).
     * @return A random double between min (inclusive) and max (exclusive).
     */
    public double nextDoubleInRange(final double minInclusive, final double maxExclusive) {
        // check that our range is valid
        if (Double.valueOf(maxExclusive - minInclusive).isInfinite()) {
            throw new IllegalArgumentException(
                    "RandGenerator: illegal range of double provided. The range is probably too big.");
        }

        initRandomGeneratorIfNeeded();

        // generate new random double in the specified range
        return minInclusive + (maxExclusive - minInclusive) * random.nextDouble();
    }

    public <T> T pickOneAmong(List<T> items) {
        return items.get(nextIntBetween0And(items.size()));
    }
}
