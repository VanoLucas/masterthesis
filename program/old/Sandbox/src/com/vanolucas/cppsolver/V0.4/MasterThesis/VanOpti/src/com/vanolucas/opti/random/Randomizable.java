package com.vanolucas.opti.random;

/**
 * Ability to be randomized.
 */
public interface Randomizable {
    void randomize();
}
