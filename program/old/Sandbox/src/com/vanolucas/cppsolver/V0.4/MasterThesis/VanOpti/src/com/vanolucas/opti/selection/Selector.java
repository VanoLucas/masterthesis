package com.vanolucas.opti.selection;

import com.vanolucas.opti.solution.Individual;

import java.util.Collection;
import java.util.List;

public interface Selector {
    List<Individual> select(final int count, Collection<Individual> population);
}
