package com.vanolucas.opti.selection;

import com.vanolucas.opti.util.Factory;

public interface SelectorFactory extends Factory {

    @Override
    default Object getNew() {
        return newSelector();
    }

    Selector newSelector();
}
