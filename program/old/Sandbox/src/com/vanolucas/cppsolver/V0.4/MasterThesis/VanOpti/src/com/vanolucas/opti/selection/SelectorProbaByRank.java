package com.vanolucas.opti.selection;

import com.vanolucas.opti.solution.Individual;
import com.vanolucas.opti.util.PickerUnique;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public abstract class SelectorProbaByRank implements Selector {

    /*
    Defaults
     */

    protected static final double DEFAULT_WORST_INDIV_PROBA = 1d;

    /*
    Attributes
     */

    protected double worstIndivProba;
    protected PickerUnique<Individual> individualsPicker;

    /*
    Constructors
     */

    public SelectorProbaByRank() {
        this(DEFAULT_WORST_INDIV_PROBA);
    }

    public SelectorProbaByRank(double worstIndivProba) {
        this.worstIndivProba = worstIndivProba;
    }

    /*
    Selection operator
     */

    @Override
    public List<Individual> select(final int count, Collection<Individual> population) {
        final int POPULATION_SIZE = population.size();

        // sort population by quality (ascending)
        List<Individual> rankedPopulation = population.stream()
                .sorted()
                .collect(Collectors.toList());

        // generate the random picker that will select some individuals
        PickerUnique<Individual> individualPicker = getPreparedIndividualsPicker(rankedPopulation);

        // select some individuals based on the generated picker
        return individualPicker.pickN(count);
    }

    private PickerUnique<Individual> getPreparedIndividualsPicker(List<Individual> rankedPopulation) {
        if (individualsPicker == null) {
            individualsPicker = new PickerUnique<>();
        }

        individualsPicker.setObjects(rankedPopulation);
        initIndividualsPickerProbas();

        return individualsPicker;
    }

    /**
     * Probabilities of the individual picker are set by subclasses.
     * SelectorProbaByRankArithmetic and SelectorProbaByRankGeometric assign
     * probabilities differently.
     */
    protected abstract void initIndividualsPickerProbas();
}

