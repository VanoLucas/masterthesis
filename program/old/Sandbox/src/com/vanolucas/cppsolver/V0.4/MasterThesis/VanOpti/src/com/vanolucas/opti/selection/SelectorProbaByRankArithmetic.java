package com.vanolucas.opti.selection;

public class SelectorProbaByRankArithmetic extends SelectorProbaByRank {

    private static final double DEFAULT_INCREMENT = 1d;

    private double increment;

    public SelectorProbaByRankArithmetic() {
        this(DEFAULT_INCREMENT);
    }

    public SelectorProbaByRankArithmetic(double increment) {
        this(DEFAULT_WORST_INDIV_PROBA, increment);
    }

    public SelectorProbaByRankArithmetic(double worstIndivProba, double increment) {
        super(worstIndivProba);
        this.increment = increment;
    }

    @Override
    protected void initIndividualsPickerProbas() {
        // the picker calculate the probability to pick each individual
        // based on their rank. Probabilities follow an arithmetic progression.
        // The better the quality, the higher the probability (linearly)
        individualsPicker.setRelativeProbasArithmeticProgression(worstIndivProba, increment);
    }
}
