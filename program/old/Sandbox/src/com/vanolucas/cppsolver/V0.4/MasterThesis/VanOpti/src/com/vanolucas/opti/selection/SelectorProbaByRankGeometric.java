package com.vanolucas.opti.selection;

public class SelectorProbaByRankGeometric extends SelectorProbaByRank {

    /**
     * By default, 10% increase for each rank
     */
    private static final double DEFAULT_RATIO = 1.1d;

    private double ratio;

    public SelectorProbaByRankGeometric() {
        this(DEFAULT_RATIO);
    }

    public SelectorProbaByRankGeometric(double ratio) {
        this(DEFAULT_WORST_INDIV_PROBA, ratio);
    }

    public SelectorProbaByRankGeometric(double worstIndivProba, double ratio) {
        super(worstIndivProba);
        this.ratio = ratio;
    }

    @Override
    protected void initIndividualsPickerProbas() {
        // the picker calculate the probability to pick each individual
        // based on their rank. Probabilities follow a geometric progression.
        // The better the quality, the higher the probability (exponentially)
        individualsPicker.setRelativeProbasGeometricProgression(worstIndivProba, ratio);
    }
}
