package com.vanolucas.opti.selection;

import com.vanolucas.opti.quality.Cost;
import com.vanolucas.opti.quality.QualitySingle;
import com.vanolucas.opti.solution.Individual;
import com.vanolucas.opti.util.PickerUnique;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class SelectorRouletteWheel implements Selector {

    @Override
    public List<Individual> select(int count, Collection<Individual> population) {
        final List<Individual> pop = new ArrayList<>(population);
        final boolean COST_MODE = pop.get(0).getQuality() instanceof Cost;

        return new PickerUnique<>(
                pop,
                pop.stream()
                        .mapToDouble(indiv -> {
                            if (COST_MODE) {
                                return 1d / ((Double) ((Cost) indiv.getQuality()).get());
                            } else {
                                return ((Double) ((QualitySingle) indiv.getQuality()).get());
                            }
                        })
                        .boxed()
                        .collect(Collectors.toList())
        )
                .pickN(count);
    }
}
