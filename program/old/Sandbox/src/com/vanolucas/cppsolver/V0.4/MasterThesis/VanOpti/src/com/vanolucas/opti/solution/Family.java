package com.vanolucas.opti.solution;

import com.vanolucas.opti.crossover.Crossover;
import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.util.Factory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Family {

    /*
    Attributes
     */

    private List<Individual> parents;
    private List<Individual> children;

    private Factory individualFactory;

    /*
    Constructors
     */

    public Family(List<Individual> parents, Factory individualFactory) {
        this.parents = parents;
        this.children = new ArrayList<>();
        this.individualFactory = individualFactory;
    }

    /*
    Accessors
     */

    public List<Individual> getParents() {
        return parents;
    }

    public List<Genome> getParentGenomes() {
        return parents.stream()
                .map(Individual::getGenome)
                .collect(Collectors.toList());
    }

    public boolean hasChildren() {
        return children.size() > 0;
    }

    public List<Individual> getChildren() {
        return children;
    }

    /*
    Commands
     */

    public Individual newChild() {
        Individual newChild = (Individual) individualFactory.getNew();
        children.add(newChild);
        return newChild;
    }

    public void crossoverUsing(Crossover crossover) {
        crossover.crossover(this);
    }


}
