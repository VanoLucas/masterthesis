package com.vanolucas.opti.solution;

import com.vanolucas.opti.eval.EvaluationFunction;
import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.mutation.Mutation;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.random.Randomizable;
import com.vanolucas.opti.util.Copyable;
import com.vanolucas.opti.util.ParetoFront;

/**
 * Stores a solution for an optimization algorithm.
 * The individual contains the phenotype solution itself, its genome and its quality.
 */
public class Individual implements Randomizable, Copyable, Comparable {

    /*
    Attributes
     */

    private Genome genome;
    private Object solution;
    private Quality quality;

    /*
    Constructors
     */

    public Individual(IndividualFactory factory) {
        this(factory.newGenome(), factory.newSolution(), factory.newQuality());
    }

    public Individual(Genome genome, Object solution, Quality quality) {
        this.genome = genome;
        this.solution = solution;
        this.quality = quality;
    }

    /*
    Accessors
     */

    public Genome getGenome() {
        return genome;
    }

    public Object getSolution() {
        return solution;
    }

    public Quality getQuality() {
        return quality;
    }

    /*
    Copyable
     */

    @Override
    public void copyFrom(Object other) {
        copyFrom((Individual) other);
    }

    public void copyFrom(Individual other) {
        genome.copyFrom(other.genome);
        quality.copyFrom(other.quality);
    }

    public void copyGenomeFrom(Individual other) {
        genome.copyFrom(other.genome);
    }

    /*
    Comparable
     */

    @Override
    public int compareTo(Object other) {
        if (other instanceof Individual) {
            Individual o = (Individual) other;
            return quality.compareTo(o.quality);
        }

        return quality.compareTo(other);
    }

    public boolean isBetterThan(Individual other) {
        return quality.isBetterThan(other.quality);
    }

    public boolean isEquivalentTo(Individual other) {
        return quality.isEquivalentTo(other.quality);
    }

    public boolean isEquivalentOrBetterThan(Individual other) {
        return quality.isEquivalentOrBetterThan(other.quality);
    }

    /*
    Randomizable
     */

    @Override
    public void randomize() {
        genome.randomize();
    }

    /*
    toString
     */

    @Override
    public String toString() {
        String skeleton = "Genome: %s\n";
        skeleton += "Solution: %s\n";
        skeleton += "Quality: %s";
        return String.format(skeleton, genome, solution, quality);
    }

    /*
    Commands
     */

    public void evaluateUsing(EvaluationFunction evaluationFunction) {
        evaluationFunction.evaluate(genome, solution, quality);
    }

    public void mutateUsing(Mutation mutation) {
        mutation.mutate(genome);
    }

    /**
     * Submit this individual's quality in the provided pareto front collection.
     *
     * @param paretoFront The pareto front collection in which to propose this individual's quality.
     * @param <TQuality>  Type of quality objects stored in the pareto front.
     * @return Result of the submission of this individual's quality to the pareto front: what has been done with the proposed quality.
     */
    public <TQuality extends Quality> ParetoFront.NewItemResult proposeQualityInParetoFront(ParetoFront<TQuality> paretoFront) {
        // propose this individual's quality to the pareto front and get comparison result
        ParetoFront.NewItemResult submissionResult = paretoFront.submit((TQuality) quality);

        // if this individual's quality got stored in the pareto front, clone it so we don't work on the same copy
        if (submissionResult == ParetoFront.NewItemResult.DOMINANT
                || submissionResult == ParetoFront.NewItemResult.EQUIVALENT_AND_KEPT) {
            quality = quality.clone();
        }

        return submissionResult;
    }
}
