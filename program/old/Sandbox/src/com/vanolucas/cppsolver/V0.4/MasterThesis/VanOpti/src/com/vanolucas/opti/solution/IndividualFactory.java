package com.vanolucas.opti.solution;

import com.vanolucas.opti.genome.GenomeFactory;
import com.vanolucas.opti.quality.QualityFactory;
import com.vanolucas.opti.util.Factory;

public interface IndividualFactory
        extends GenomeFactory, SolutionFactory, QualityFactory, Factory {
    @Override
    default Object getNew() {
        return newIndividual();
    }

    default Individual newIndividual() {
        return new Individual(this);
    }
}
