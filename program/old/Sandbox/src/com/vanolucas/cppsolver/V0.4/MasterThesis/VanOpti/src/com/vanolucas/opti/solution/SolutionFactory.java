package com.vanolucas.opti.solution;

import com.vanolucas.opti.util.Factory;

public interface SolutionFactory extends Factory {

    @Override
    default Object getNew() {
        return newSolution();
    }

    Object newSolution();
}
