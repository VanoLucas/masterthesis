package com.vanolucas.opti.util;

public interface Copyable {

    default void copyTo(Copyable other) {
        other.copyFrom(this);
    }

    void copyFrom(Object other);
}
