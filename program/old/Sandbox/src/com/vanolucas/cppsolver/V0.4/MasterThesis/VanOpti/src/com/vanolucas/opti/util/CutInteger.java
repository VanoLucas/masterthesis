package com.vanolucas.opti.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Tool object that provides an algorithm to cut a cake (given number)
 * into pieces. The relative size of each piece is defined by a given weight.
 * The sum of all resulting pieces = the initial number.
 *
 * Cut a cake (positive number) in slices such that
 * relative slice sizes reflect relative values of the weights array.
 * Each input weight determines the size of the corresponding output slice.
 * The order of output slices corresponds to the order of input weights.
 */
public class CutInteger {
    /**
     * The entire cake to be sliced.
     */
    private int cakeSize;
    /**
     * Relative weights that determine the size of each cake slice.
     */
    private Map<Integer, Double> weights;

    /*
    Constructors
     */

    public CutInteger() {
        this(0);
    }
    public CutInteger(int cakeSize) {
        this(cakeSize, new ArrayList<>());
    }
    public CutInteger(int cakeSize, List<Double> weights) {
        setCakeSize(cakeSize);
        setWeights(weights);
    }

    /*
    Accessors
     */

    public int getCakeSize() {
        return cakeSize;
    }

    public void setCakeSize(int cakeSize) {
        this.cakeSize = cakeSize;
    }

    public double[] getWeights() {
        return weights.keySet().stream()
                .sorted()
                .mapToDouble(k -> weights.get(k))
                .toArray();
    }

    public void setWeights(List<Double> weights) {
        setWeights(IntStream.range(0, weights.size())
                .boxed()
                .collect(Collectors.toMap(
                        i -> i,
                        i -> weights.get(i))
                )
        );
    }

    public void setWeights(Map<Integer, Double> weights) {
        this.weights = weights;
    }

    /*
    Commands
    */

    public List<Integer> cutCake(int cakeSize, List<Double> weights) {
        setCakeSize(cakeSize);
        return cutCake(weights);
    }

    public List<Integer> cutCake(List<Double> weights) {
        setWeights(weights);
        return cutCake();
    }

    public List<Integer> cutCake() {
        // init list of output slices
        List<Integer> slices = new ArrayList<>(weights.size());
        IntStream.range(0, weights.size())
                .forEach(s -> slices.add(0));

        // indexes of weights sorted ascending so that bigger weights are later processed first
        int[] sortedWeightsIndexes = weights.entrySet().stream()
                .sorted(Comparator.comparing(Map.Entry::getValue))
                .mapToInt(Map.Entry::getKey)
                .toArray();

        // qty of cake remaining to cut
        int remainingCake = cakeSize;
        // remaining weights to process
        double remainingWeights = weights.values().stream()
                .mapToDouble(Double::doubleValue)
                .sum();

        // for each piece of cake to cut (from biggest to smallest) except the last
        int currentSliceIndex;
        double currentWeight;
        for (int p=sortedWeightsIndexes.length-1 ; p>0 ; p--) {
            // get current slice index
            currentSliceIndex = sortedWeightsIndexes[p];

            // get next biggest weight
            currentWeight = weights.get(currentSliceIndex);

            // cut the slice
            int slice = (int) Math.round(currentWeight / remainingWeights * remainingCake);

            // keep the slice
            slices.set(currentSliceIndex, slice);

            // update remaining cake
            remainingCake -= slice;

            // update remaining weights
            remainingWeights -= currentWeight;
        }

        // the last (and smallest) piece is the remaining cake
        currentSliceIndex = sortedWeightsIndexes[0];
        slices.set(currentSliceIndex, remainingCake);

        // return sliced cake
        return slices;
    }
}

