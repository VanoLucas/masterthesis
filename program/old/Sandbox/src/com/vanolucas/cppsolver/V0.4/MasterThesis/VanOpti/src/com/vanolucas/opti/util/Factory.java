package com.vanolucas.opti.util;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Ability to construct an object.
 */
public interface Factory {
    Object getNew();
}
