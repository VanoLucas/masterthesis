package com.vanolucas.opti.util;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Observable object of the observer design pattern.
 * Listeners can subscribe to it so they get notified.
 * @param <TListener> Type of the listener objects that can subscribe to this Observable.
 */
public class Observable<TListener> {

    /**
     * Currently subscribed listeners.
     */
    private List<TListener> listeners;

    /*
    Constructors
     */

    public Observable() {
        this.listeners = new ArrayList<>(1);
    }

    /*
    Commands
     */

    public void addListener(TListener listener) {
        listeners.add(listener);
    }

    public void removeListener(TListener listener) {
        listeners.remove(listener);
    }

    public void removeAllListeners() {
        listeners.clear();
    }

    /**
     * Perform the specified action on each listener.
     * @param action Action to perform for each listener that subscribed to this Observable.
     */
    public void notifyListeners(Consumer<? super TListener> action) {
        listeners.forEach(action);
    }
}
