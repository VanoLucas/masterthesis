package com.vanolucas.opti.util;

import java.util.function.Consumer;

/**
 * Thread-safe implementation of the Observable object of the observer design pattern.
 * @param <TListener> Type of the listener objects that can subscribe to this Observable.
 */
public class ObservableTS<TListener> extends Observable<TListener> {

    /*
    Attributes
     */

    private Object lock;

    /*
    Constructors
     */

    public ObservableTS() {
        super();
        this.lock = new Object();
    }

    /*
    Commands
     */

    @Override
    public void addListener(TListener listener) {
        synchronized (lock) {
            super.addListener(listener);
        }
    }

    @Override
    public void removeListener(TListener listener) {
        synchronized (lock) {
            super.removeListener(listener);
        }
    }

    @Override
    public void removeAllListeners() {
        synchronized (lock) {
            super.removeAllListeners();
        }
    }

    @Override
    public void notifyListeners(Consumer<? super TListener> action) {
        synchronized (lock) {
            super.notifyListeners(action);
        }
    }
}
