package com.vanolucas.opti.util;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ParetoFront<T extends Comparable> {

    /*
    Defaults
     */

    private static final int DEFAULT_MAX_SIZE = 1;

    /*
    Enumerations
     */

    /**
     * Possible results for newly proposed item.
     */
    public enum NewItemResult {
        DOMINATED, EQUIVALENT_BUT_NOT_KEPT, EQUIVALENT_AND_KEPT, DOMINANT
    }

    /*
    Attributes
     */

    /**
     * Items of this pareto front.
     */
    private List<T> paretoFront;
    /**
     * Max number of items to store in the Pareto Front.
     */
    private int maxSize;
    /**
     * Keep a reference to the last item that dominated other items of the pareto front.
     */
    private T lastBetterItemAdded;

    /*
    Constructors
     */

    public ParetoFront() {
        this(DEFAULT_MAX_SIZE);
    }

    public ParetoFront(final int maxSize) {
        this.maxSize = maxSize;
        this.paretoFront = new ArrayList<>(maxSize);
    }

    /*
    Accessors
     */

    public int getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(final int maxSize) {
        this.maxSize = maxSize;
    }

    public T getLastBetterItemAdded() {
        return lastBetterItemAdded;
    }

    /*
    Stream
     */

    public Stream<T> stream() {
        return paretoFront.stream();
    }

    /*
    Commands
     */

    /**
     * Submit a new item to the Pareto Front.
     * The new item is added only if it dominates at least one item of the pareto front,
     * or if it is equivalent to other items and there is still room before reaching max size.
     *
     * @param newItem Candidate item to be added to this Pareto Front if good enough.
     * @return The result which tells if the submitted item was better, equivalent or worse than
     * the preexisting pareto front.
     */
    public NewItemResult submit(T newItem) {
        // check if the item is worse than pareto front
        if (isDominatedByParetoFront(newItem)) {
            // new item is dominated by pareto front, don't add it
            return NewItemResult.DOMINATED;
        }

        // retrieve all items of the pareto front that are dominated by the new item
        List<T> dominatedItems = paretoFront.stream()
                .filter(paretoFrontItem -> newItem.compareTo(paretoFrontItem) > 0)
                .collect(Collectors.toList());

        // if pareto front is empty
        if (paretoFront.isEmpty()) {
            // indeed add the new item
            handleNewDominantItem(newItem, dominatedItems);
            return NewItemResult.DOMINANT;
        }

        // if the new item dominates at least one pareto front item
        else if (!dominatedItems.isEmpty()) {
            // it needs to replace all dominated items in the pareto front
            handleNewDominantItem(newItem, dominatedItems);
            return NewItemResult.DOMINANT;
        }

        // else, the new item doesn't dominate any pareto front item, it's only equivalent
        else {
            if (handleNewEquivalentItem(newItem)) {
                return NewItemResult.EQUIVALENT_AND_KEPT;
            }

            return NewItemResult.EQUIVALENT_BUT_NOT_KEPT;
        }
    }

    /*
    Helpers
     */

    private boolean isDominatedByParetoFront(T item) {
        // check if the new item is dominated by any item of the current pareto front
        for (T paretoFrontItem : paretoFront) {
            // if this pareto front item dominates the new candidate item
            if (paretoFrontItem.compareTo(item) > 0) {
                // the proposed item is dominated and should not be added to pareto front
                return true;
            }
        }

        return false;
    }

    /**
     * Add new pareto-equivalent item to the pareto front
     * if there is still room before reaching its max size.
     * <p>
     * todo make specialization of the Pareto Front class that behave differently when adding equivalent item
     *
     * @param newItem Pareto-equivalent item to add to the pareto front
     *                (if there is still room for it).
     * @return True if the new item got inserted in the pareto front.
     */
    protected boolean handleNewEquivalentItem(T newItem) {
        // if there is still room in the pareto front
        if (paretoFront.size() < maxSize) {
            // add the new pareto-equivalent item
            paretoFront.add(newItem);
            return true;
        }

        return false;
    }

    /**
     * Add new dominant item to the pareto front and remove all the items it dominates.
     *
     * @param newItem        The new item that will replace dominated pareto front items.
     * @param dominatedItems The pareto front items that are dominated by the new item
     *                       and should therefore be removed from the pareto front.
     */
    private void handleNewDominantItem(T newItem, List<T> dominatedItems) {
        paretoFront.removeAll(dominatedItems);
        paretoFront.add(newItem);
        this.lastBetterItemAdded = newItem;
    }

    /*
    toString
     */

    @Override
    public String toString() {
        return paretoFront.stream()
                .map(item -> item.toString())
                .collect(Collectors.joining(","));
    }
}
