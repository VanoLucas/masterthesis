package com.vanolucas.opti.util;

import org.apache.commons.math3.distribution.EnumeratedIntegerDistribution;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

/**
 * Random object picker that follows the specified discrete distribution.
 *
 * @param <T> Type of objects to be picked.
 */
public class Picker<T> {

    /*
    Attributes
     */

    protected List<T> objects;
    protected EnumeratedIntegerDistribution distribution;

    /*
    Constructors
     */

    public Picker() {
    }

    public Picker(List<T> objects) {
        double[] uniformProbas = IntStream.range(0, objects.size())
                .mapToDouble(i -> 1d)
                .toArray();
        set(objects, uniformProbas);
    }

    public Picker(List<T> objects, List<Double> relativeProbas) {
        set(objects, relativeProbas);
    }

    public Picker(List<T> objects, double[] relativeProbas) {
        set(objects, relativeProbas);
    }

    /*
    Accessors
     */

    public int size() {
        if (objects == null) {
            return 0;
        }
        return objects.size();
    }

    public void set(List<T> objects, List<Double> relativeProbas) {
        setObjects(objects);
        setRelativeProbas(relativeProbas);
    }

    public void set(List<T> objects, double[] relativeProbas) {
        setObjects(objects);
        setRelativeProbas(relativeProbas);
    }

    public void setObjects(List<T> objects) {
        this.objects = objects;
    }

    /**
     * Set probabilities as a progression of numbers.
     *
     * @param firstProba The first value of this progression of probabilities.
     * @param increment  The common difference (increment) between each value of this progression.
     */
    public void setRelativeProbasArithmeticProgression(double firstProba, double increment) {
        setRelativeProbasArithmeticProgression(firstProba, increment, objects.size());
    }

    /**
     * Set probabilities as a progression of numbers.
     *
     * @param firstProba The first value of this progression of probabilities.
     * @param increment  The common difference (increment) between each value of this progression.
     * @param count      The number of proba values to generate.
     */
    public void setRelativeProbasArithmeticProgression(double firstProba, double increment, int count) {
        setRelativeProbas(
                DoubleStream.iterate(firstProba, p -> p + increment)
                        .limit(count)
                        .boxed()
                        .collect(Collectors.toList())
        );
    }

    public void setRelativeProbasGeometricProgression(double firstProba, double ratio) {
        setRelativeProbasGeometricProgression(firstProba, ratio, objects.size());
    }

    public void setRelativeProbasGeometricProgression(double firstProba, double ratio, int count) {
        setRelativeProbas(
                DoubleStream.iterate(firstProba, p -> p * ratio)
                        .limit(count)
                        .boxed()
                        .collect(Collectors.toList())
        );
    }

    public void setRelativeProbas(List<Double> relativeProbas) {
        setRelativeProbas(relativeProbas.stream()
                .mapToDouble(Double::doubleValue)
                .toArray());
    }

    public void setRelativeProbas(double[] relativeProbas) {
        int[] indexes = IntStream.range(0, objects.size())
                .toArray();
        this.distribution = new EnumeratedIntegerDistribution(indexes, relativeProbas);
    }

    /*
    Commands
     */

    public T pickOne() {
        if (objects.size() == 1) {
            return objects.get(0);
        }
        return objects.get(distribution.sample());
    }

    public List<T> pickN(final int count) {
        int[] picked = distribution.sample(count);
        return Arrays.stream(picked)
                .mapToObj(index -> objects.get(index))
                .collect(Collectors.toList());
    }
}
