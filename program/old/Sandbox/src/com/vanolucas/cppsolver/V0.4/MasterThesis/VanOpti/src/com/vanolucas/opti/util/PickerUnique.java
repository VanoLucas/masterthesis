package com.vanolucas.opti.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * With this Picker, each object can only be picked a single time:
 * its probability to be picked again becomes 0 once it has already been picked.
 */
public class PickerUnique<T> extends Picker<T> {

    /*
    Attributes
     */

    private double[] relativeProbas;

    /*
    Constructors
     */

    public PickerUnique() {
    }

    public PickerUnique(List<T> objects) {
        super(objects);
    }

    public PickerUnique(List<T> objects, List<Double> relativeProbas) {
        super(objects, relativeProbas);
    }

    public PickerUnique(List<T> objects, double[] relativeProbas) {
        super(objects, relativeProbas);
    }

    /*
    Accessors
     */

    @Override
    public void setRelativeProbas(double[] relativeProbas) {
        this.relativeProbas = relativeProbas;
        super.setRelativeProbas(relativeProbas);
    }

    /*
    Commands
     */

    @Override
    public T pickOne() {
        // pick an item
        final int indexPicked = distribution.sample();
        final T picked = objects.get(indexPicked);

        // now that it has been picked, it can't be picked again: set its proba to 0
        relativeProbas[indexPicked] = 0d;

        // if all proba are 0, set them all to 1 (because otherwise it makes the distribution fail)
        if (Arrays.stream(relativeProbas)
                .allMatch(p -> p == 0d)) {
            Arrays.fill(relativeProbas, 1d);
        }

        // update probas in the distribution
        super.setRelativeProbas(relativeProbas);

        return picked;
    }

    @Override
    public List<T> pickN(int count) {
        return IntStream.range(0, count)
                .mapToObj(i -> pickOne())
                .collect(Collectors.toList());
    }
}
