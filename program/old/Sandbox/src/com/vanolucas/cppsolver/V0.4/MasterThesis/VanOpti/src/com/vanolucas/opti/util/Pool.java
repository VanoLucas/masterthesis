package com.vanolucas.opti.util;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Pool<T> implements Factory {

    /*
    Attributes
     */

    private Set<T> inUse;
    private Set<T> unused;
    private Factory factory;

    /*
    Constructors
     */

    public Pool(Factory itemFactory, final int initialSize) {
        this.factory = itemFactory;

        this.inUse = new HashSet<>(initialSize);
        // construct initial pool of objects
        this.unused = new HashSet<>(IntStream.range(0, initialSize)
                .mapToObj(item -> (T) itemFactory.getNew())
                .collect(Collectors.toList())
        );
    }

    /*
    Commands
     */

    @Override
    public T getNew() {
        // if we have no more item available, construct a new one
        if (unused.isEmpty()) {
            unused.add((T) factory.getNew());
        }

        // fetch a recycled item
        T item = unused.iterator().next();

        // if this object is aware it is pooled, let it know in which Pool
        if (item instanceof Poolable) {
            ((Poolable) item).setParentPool(this);
        }

        // mark the recycled item as now in use
        unused.remove(item);
        inUse.add(item);

        return item;
    }

    public List<T> getNew(final int count) {
        return IntStream.range(0, count)
                .mapToObj(i -> getNew())
                .collect(Collectors.toList());
    }

    public void returnToPool(Collection<T> items) {
        inUse.removeAll(items);
        unused.addAll(items);
    }

    public void returnToPool(T item) {
        inUse.remove(item);
        unused.add(item);
    }
}
