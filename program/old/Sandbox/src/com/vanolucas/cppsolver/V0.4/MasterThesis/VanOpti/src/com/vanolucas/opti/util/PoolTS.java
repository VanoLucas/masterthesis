package com.vanolucas.opti.util;

import java.util.Collection;

public class PoolTS<T> extends Pool<T> {

    /*
    Attributes
     */

    private Object lock;

    /*
    Constructors
     */

    public PoolTS(Factory itemFactory, int initialSize) {
        super(itemFactory, initialSize);
        this.lock = new Object();
    }

    @Override
    public T getNew() {
        synchronized (lock) {
            return super.getNew();
        }
    }

    @Override
    public void returnToPool(Collection<T> items) {
        synchronized (lock) {
            super.returnToPool(items);
        }
    }

    @Override
    public void returnToPool(T item) {
        synchronized (lock) {
            super.returnToPool(item);
        }
    }
}
