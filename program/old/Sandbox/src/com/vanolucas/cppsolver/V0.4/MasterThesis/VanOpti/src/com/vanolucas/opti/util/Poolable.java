package com.vanolucas.opti.util;

public abstract class Poolable {

    /*
    Attributes
     */

    private Pool parentPool;

    /*
    Accessors
     */

    public void setParentPool(Pool parentPool) {
        this.parentPool = parentPool;
    }

    /*
    Commands
     */

    public void recycle() {
        parentPool.returnToPool(this);
    }
}
