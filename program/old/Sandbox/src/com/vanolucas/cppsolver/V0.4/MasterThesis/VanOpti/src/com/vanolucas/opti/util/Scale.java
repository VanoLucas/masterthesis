package com.vanolucas.opti.util;

public class Scale {
    /**
     * Scale linearly a value from source range to destination range.
     * https://stackoverflow.com/a/5295202
     * @param value Value in src range to convert to dst range.
     * @param srcMin
     * @param srcMax
     * @param dstMin
     * @param dstMax
     * @return The value translated to the target range.
     */
    public static double scale(final double value, final double srcMin, final double srcMax, final double dstMin, final double dstMax) {
        return ((dstMax - dstMin) * (value - srcMin) / (srcMax - srcMin)) + dstMin;
    }

    public static double scale(final double value, final double srcMax, final double dstMax) {
        return dstMax * value / srcMax;
    }
}
