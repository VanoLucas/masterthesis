package com.vanolucas.opti;

import org.junit.jupiter.api.Test;
import org.moeaframework.Executor;
import org.moeaframework.core.NondominatedPopulation;

import static org.junit.jupiter.api.Assertions.*;

public class MoeaTest {

    @Test
    void simpleRun() {
        NondominatedPopulation result = new Executor()
                .withProblem("UF1")
                .withAlgorithm("NSGAII")
                .withMaxEvaluations(10000)
                .run();
    }
}
