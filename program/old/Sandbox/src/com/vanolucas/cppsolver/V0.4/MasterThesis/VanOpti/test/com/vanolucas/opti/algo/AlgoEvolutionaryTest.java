package com.vanolucas.opti.algo;

import com.vanolucas.opti.crossover.Crossover;
import com.vanolucas.opti.crossover.CrossoverCoupleUniform;
import com.vanolucas.opti.eval.EvaluationFunction;
import com.vanolucas.opti.genome.Genome;
import com.vanolucas.opti.genome.GenomeVectorDouble01;
import com.vanolucas.opti.grouping.Grouping;
import com.vanolucas.opti.grouping.GroupingCouplesRandom;
import com.vanolucas.opti.mutation.Mutation;
import com.vanolucas.opti.mutation.MutationSingleGene;
import com.vanolucas.opti.quality.CostLong;
import com.vanolucas.opti.quality.Quality;
import com.vanolucas.opti.selection.Selector;
import com.vanolucas.opti.selection.SelectorProbaByRank;
import com.vanolucas.opti.selection.SelectorProbaByRankArithmetic;
import com.vanolucas.opti.solution.Individual;
import com.vanolucas.opti.solution.IndividualFactory;
import com.vanolucas.opti.util.ParetoFront;
import com.vanolucas.opti.util.Picker;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

class AlgoEvolutionaryTest {

    @Test
    void targetNumber() {
        // target number problem
        final int TARGET_NUMBER = 10000;
        final int MAX_NUMBER = 100000;
        final int GENOME_LENGTH = 2;

        // evolutionary algo params
        final int POPULATION_SIZE = 20;
        final double CROSSOVER_PROBA = 0.8;
        final double CHILD_MUTATION_PROBA = 0.1;
        final int MAX_PARETO_FRONT_SIZE = 1;

        // our solution (phenotype) class
        class Phenotype {
            private int value;

            public int getValue() {
                return value;
            }

            public void setValue(int value) {
                this.value = value;
            }

            @Override
            public String toString() {
                return String.valueOf(value);
            }
        }

        // evaluation function
        EvaluationFunction evaluationFunction = new EvaluationFunction() {
            @Override
            public void evaluate(Object solution, Quality outQuality) {
                Phenotype phenotype = (Phenotype) solution;
                final long distanceToTarget = Math.abs(phenotype.getValue() - TARGET_NUMBER);
                ((CostLong) outQuality).set(distanceToTarget);
            }

            @Override
            public void decode(Genome genome, Object outSolution) {
                GenomeVectorDouble01 g = (GenomeVectorDouble01) genome;
                Phenotype solution = (Phenotype) outSolution;

                List<Double> genes = g.getValues();
                int sum = 0;

                for (Double d : genes) {
                    sum += d * MAX_NUMBER;
                }

                solution.setValue(sum);
            }
        };

        // selection operator
        Selector selector = new SelectorProbaByRankArithmetic();

        // grouping operator
        Grouping grouping = new GroupingCouplesRandom();

        // crossover operator
        Crossover crossoverOperator = new CrossoverCoupleUniform(1.0d);
        List<Crossover> crossoverOperators = new ArrayList<Crossover>(1) {{
            add(crossoverOperator);
        }};
        Picker<Crossover> crossoverPicker = new Picker<>(crossoverOperators);

        // mutation operator
        Mutation mutationOperator = new MutationSingleGene();
        List<Mutation> mutationOperators = new ArrayList<Mutation>(1) {{
            add(mutationOperator);
        }};
        Picker<Mutation> mutationPicker = new Picker<>(mutationOperators);

        // individual factory
        IndividualFactory individualFactory = new IndividualFactory() {
            @Override
            public Genome newGenome() {
                return new GenomeVectorDouble01(GENOME_LENGTH, 0.5d);
            }

            @Override
            public Quality newQuality() {
                return new CostLong();
            }

            @Override
            public Object newSolution() {
                return new Phenotype();
            }
        };

        // evolutionary algorithm
        AlgoEvolutionary algo = new AlgoEvolutionary(evaluationFunction, selector, grouping, crossoverPicker, mutationPicker, individualFactory,
                POPULATION_SIZE, CROSSOVER_PROBA, CHILD_MUTATION_PROBA, MAX_PARETO_FRONT_SIZE);

        algo.addListener(new AlgoEvolutionary.Listener() {
//            @Override
//            public void onGenerationEnd(long generation, Set<Individual> population, ParetoFront<Quality> bestQualities) {
//                if (generation % 1000 != 0) {
//                    return;
//                }
//
//                final double avgCost = population.stream()
//                        .mapToLong(indiv -> ((CostLong) indiv.getQuality()).get())
//                        .average()
//                        .getAsDouble();
//                System.out.print(avgCost + ";");
//            }

//            @Override
//            public void onNewBetterIndividualFound(Individual newBetterIndividual, long iteration) {
//                System.out.println();
//                System.out.println("Iteration: " + iteration);
//                System.out.println("New better: " + newBetterIndividual);
//            }
        });

        algo.addListener(new AlgoEvolutionary.Listener() {
            @Override
            public void onGenerationEnd(long generation, Set<Individual> population, ParetoFront<Quality> bestQualities) {
//                System.out.println(population.size());
//                System.out.println(generation);
            }
        });

        // run algo
        algo.runUntilIteration(6);
//        algo.runUntilQuality(new CostLong(0L));
    }

}