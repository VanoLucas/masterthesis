package com.vanolucas.opti.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ObservableTSTest {

    @Test
    void observableTS() {
        Observable<String> observable = new ObservableTS<>();
        observable.addListener("Hi");
        observable.notifyListeners(listener -> assertEquals("Hi", listener));
    }

}