package com.vanolucas.opti.util;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class PickerTest {

    @Test
    void pickOne() {
        Picker<Integer> picker = new Picker<>();

        picker.set(new ArrayList<Integer>() {{
            add(0);
            add(1);
            add(2);
        }}, new double[]{1d, 2d, 1d});

        int[] count = new int[3];

        for (int i = 0; i < 10000; i++) {
            count[picker.pickOne()]++;
        }

        assertEquals(2500, count[0], 500);
        assertEquals(5000, count[1], 1000);
        assertEquals(2500, count[2], 500);
    }

}