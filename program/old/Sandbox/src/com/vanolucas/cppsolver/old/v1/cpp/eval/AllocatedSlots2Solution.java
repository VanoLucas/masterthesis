package com.vanolucas.cppsolver.old.v1.cpp.eval;

import com.vanolucas.cppsolver.old.v1.cpp.problem.StandardCppInstance;
import com.vanolucas.cppsolver.old.v1.cpp.solution.AllocatedSlots;
import com.vanolucas.cppsolver.old.v1.cpp.solution.OffsetPlatePrintings;
import com.vanolucas.cppsolver.old.v1.cpp.solution.StandardCppSolution;
import com.vanolucas.cppsolver.old.v1.genetic.Phenotype;
import com.vanolucas.cppsolver.old.v1.genetic.PhenotypeDecoder;

import java.util.ArrayList;
import java.util.List;

/**
 * Converts an AllocatedSlot phenotype to the corresponding CPP solution.
 */
public class AllocatedSlots2Solution implements PhenotypeDecoder {

    private final int nbOffsetPlates;
    private final int nbSlotsPerPlate;

    public AllocatedSlots2Solution(StandardCppInstance problem) {
        this(problem.getNbOffsetPlates(), problem.getNbSlotsPerPlate());
    }

    public AllocatedSlots2Solution(int nbOffsetPlates, int nbSlotsPerPlate) {
        this.nbOffsetPlates = nbOffsetPlates;
        this.nbSlotsPerPlate = nbSlotsPerPlate;
    }

    @Override
    public StandardCppSolution decode(Phenotype phenotype) {
        return decode((AllocatedSlots) phenotype);
    }

    public StandardCppSolution decode(AllocatedSlots slots) {
        List<OffsetPlatePrintings> offsetPlatePrintings = new ArrayList<>(nbOffsetPlates);

        int cursorSlot = 0;
        for (int i = 0; i < nbOffsetPlates; i++) {
            // create the offset plate printings object from our slots
            offsetPlatePrintings.add(
                    OffsetPlatePrintings.fromSlots(
                            slots.getSlots(cursorSlot, cursorSlot + nbSlotsPerPlate)
                    )
            );
            // advance to slots for the next offset plate
            cursorSlot += nbSlotsPerPlate;
        }

        // create our standard CPP solution representation with our filled offset plates
        return StandardCppSolution.withOffsetPlatePrintings(offsetPlatePrintings);
    }
}
