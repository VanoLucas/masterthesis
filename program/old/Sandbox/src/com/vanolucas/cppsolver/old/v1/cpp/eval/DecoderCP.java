package com.vanolucas.cppsolver.old.v1.cpp.eval;

import com.vanolucas.cppsolver.old.v1.cpp.genome.GenomeCP;
import com.vanolucas.cppsolver.old.v1.cpp.problem.StandardCppInstance;
import com.vanolucas.cppsolver.old.v1.cpp.solution.AllocatedSlots;
import com.vanolucas.cppsolver.old.v1.cpp.solution.Slot;
import com.vanolucas.cppsolver.old.v1.genetic.Genotype;
import com.vanolucas.cppsolver.old.v1.genetic.GenotypeDecoder;
import com.vanolucas.cppsolver.old.v1.genetic.Phenotype;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Decodes GenomeCP genomes to the corresponding AllocatedSlots phenotype.
 */
public class DecoderCP implements GenotypeDecoder {

    private final int nbBookCovers;
    private final int nbSlots;
    private final List<Integer> copies;

    private DecoderCP(StandardCppInstance problem) {
        this(problem.getNbBookCovers(), problem.getTotalNbSlots(), problem.getCopies());
    }

    public DecoderCP(int nbBookCovers, int nbSlots, List<Integer> copies) {
        this.nbBookCovers = nbBookCovers;
        this.nbSlots = nbSlots;
        this.copies = copies;
    }

    public static DecoderCP forProblem(StandardCppInstance problem) {
        return new DecoderCP(problem);
    }

    @Override
    public Phenotype decode(Genotype genotype) {
        return decode((GenomeCP) genotype);
    }

    public AllocatedSlots decode(GenomeCP genome) {
        // extract keys from genome
        List<Double> keys = genome.getKeys();

        // to store our allocated slots
        List<Slot> slots = new ArrayList<>(nbSlots);

        // initially allocate each book cover to a single slot that fills 100% of its copies demand
        slots.addAll(IntStream.range(0, copies.size())
                .mapToObj(bookCoverIndex -> new Slot(bookCoverIndex, copies.get(bookCoverIndex)))
                .collect(Collectors.toList())
        );

        // then we need to divide these printings over multiple slots to use all the available slots

        // for each extra slot to fill
        for (int k = 0; k < keys.size(); k += 2) {
            // get the keys that characterize this slot
            final double keyIndexBookCoverToSplit = keys.get(k);
            final double fractionToSplitOut = keys.get(k + 1);

            // calculate the index of the book cover/slot to split over an extra slot
            final int indexBookCoverToSplit = (int) (keyIndexBookCoverToSplit * (double) nbBookCovers);

            // split a fraction of the printings from the chosen index over an extra slot
            slots.add(
                    slots.get(indexBookCoverToSplit)
                            .split(fractionToSplitOut)
            );
        }

        // place all slots in our phenotype
        return new AllocatedSlots(slots);
    }
}
