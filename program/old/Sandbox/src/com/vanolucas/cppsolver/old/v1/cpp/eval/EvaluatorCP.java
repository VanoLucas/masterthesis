package com.vanolucas.cppsolver.old.v1.cpp.eval;

import com.vanolucas.cppsolver.old.v1.cpp.problem.StandardCppInstance;
import com.vanolucas.cppsolver.old.v1.opti.evaluation.GeneticIndividualEvaluator;

public class EvaluatorCP extends GeneticIndividualEvaluator {

    public EvaluatorCP(StandardCppInstance problem) {
        super(
                DecoderCP.forProblem(problem),
                new AllocatedSlots2Solution(problem),
                new StandardCppSolutionEvaluationFunction()
        );
    }
}
