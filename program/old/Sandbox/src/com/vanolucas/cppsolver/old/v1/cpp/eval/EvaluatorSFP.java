package com.vanolucas.cppsolver.old.v1.cpp.eval;

import com.vanolucas.cppsolver.old.v1.cpp.problem.StandardCppInstance;
import com.vanolucas.cppsolver.old.v1.opti.evaluation.GeneticIndividualEvaluator;

public class EvaluatorSFP extends GeneticIndividualEvaluator {

    public EvaluatorSFP(StandardCppInstance problem) {
        super(
                DecoderSFP.forProblem(problem),
                new AllocatedSlots2Solution(problem),
                new StandardCppSolutionEvaluationFunction()
        );
    }
}
