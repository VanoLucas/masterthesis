package com.vanolucas.cppsolver.old.v1.cpp.genome;

import com.vanolucas.cppsolver.old.v1.cpp.problem.StandardCppInstance;
import com.vanolucas.cppsolver.old.v1.genetic.RandomKeyVectorGenome;

/**
 * GenomeCP structure:
 * groups of 2 genes:
 * - choice of the book Cover to allocate to the slot
 * - nb of printings to allocate to the slot
 */
public class GenomeCP extends RandomKeyVectorGenome {

    private static final double INIT_GENE_VALUE = 0d;

    protected GenomeCP(StandardCppInstance problem) {
        this(getGenomeLengthFor(problem));
    }

    protected GenomeCP(int length) {
        super(length, INIT_GENE_VALUE);
    }

    public static GenomeCP forProblem(StandardCppInstance problem) {
        return new GenomeCP(problem);
    }

    /**
     * The length of a type CP genome is 2 * (n.S - m).
     *
     * @return The size of the CP genome that can describe solutions for the provided problem.
     */
    private static int getGenomeLengthFor(StandardCppInstance problem) {
        return (problem.getTotalNbSlots() - problem.getNbBookCovers()) * 2;
    }

    @Override
    public String toString() {
        return String.format("%s\n%s",
                getClass().getSimpleName(),
                super.toString()
        );
    }
}
