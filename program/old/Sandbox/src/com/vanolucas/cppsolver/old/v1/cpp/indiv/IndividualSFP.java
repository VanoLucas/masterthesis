package com.vanolucas.cppsolver.old.v1.cpp.indiv;

import com.vanolucas.cppsolver.old.v1.cpp.genome.GenomeSFP;
import com.vanolucas.cppsolver.old.v1.cpp.problem.StandardCppInstance;
import com.vanolucas.cppsolver.old.v1.opti.indiv.GeneticIndividual;

public class IndividualSFP extends GeneticIndividual {

    public IndividualSFP(StandardCppInstance problem) {
        super(GenomeSFP.forProblem(problem));
    }
}
