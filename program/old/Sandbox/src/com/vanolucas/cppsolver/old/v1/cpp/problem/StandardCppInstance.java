package com.vanolucas.cppsolver.old.v1.cpp.problem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Defines an instance of the Standard Cover Printing Problem.
 */
public class StandardCppInstance {

    private static final int DEFAULT_NB_SLOTS_PER_PLATE = 4;

    /**
     * Number of copies requested for each book cover.
     * (m items = m book covers).
     */
    private List<Integer> copies;
    /**
     * Number of offset plates (n).
     */
    private int nbOffsetPlates;
    /**
     * Number of slots per plate (S).
     */
    private int nbSlotsPerPlate;

    protected StandardCppInstance(List<Integer> copies, int nbOffsetPlates, int nbSlotsPerPlate) {
        this.copies = copies;
        this.nbOffsetPlates = nbOffsetPlates;
        this.nbSlotsPerPlate = nbSlotsPerPlate;
    }

    public static StandardCppInstance newInstance(Collection<Integer> copies, int nbOffsetPlates) {
        return newInstance(copies, nbOffsetPlates, DEFAULT_NB_SLOTS_PER_PLATE);
    }

    public static StandardCppInstance newInstance(Collection<Integer> copies, int nbOffsetPlates, int nbSlotsPerPlate) {
        // check validity of params
        checkCopiesValidity(copies);
        checkNbOffsetPlatesValidity(nbOffsetPlates);
        checkNbSlotsPerPlateValidity(nbSlotsPerPlate);

        // sort requested copies from greatest to lowest
        final List<Integer> sortedCopies = new ArrayList<>(copies);
        sortedCopies.sort(Collections.reverseOrder());

        // construct a standard cover printing problem instance with those params
        return new StandardCppInstance(sortedCopies, nbOffsetPlates, nbSlotsPerPlate);
    }

    private static void checkCopiesValidity(Collection<Integer> copies) {
        // check if at least 1 book cover
        if (copies == null
                || copies.size() < 1) {
            throw new IllegalArgumentException(
                    "Illegal copies param: there must be at least 1 book cover."
            );
        }
        // check if valid number of copies
        for (Integer item : copies) {
            if (item <= 1) {
                throw new IllegalArgumentException(
                        "Illegal copies param: there must be at least 1 copy of each book cover requested."
                );
            }
        }
    }

    private static void checkNbOffsetPlatesValidity(int nbOffsetPlates) {
        // check that we have at least 1 offset plate
        if (nbOffsetPlates < 1) {
            throw new IllegalArgumentException(
                    "Illegal number of offset plates param: there must be at least 1 offset plate."
            );
        }
    }

    private static void checkNbSlotsPerPlateValidity(int nbSlotsPerPlate) {
        // check that we have at least 1 slot per plate
        if (nbSlotsPerPlate < 1) {
            throw new IllegalArgumentException(
                    "Illegal number of slots per offset plate param: there must be at least 1 slot per offset plate."
            );
        }
    }

    public List<Integer> getCopies() {
        return copies;
    }

    public int getCopies(int bookCoverIndex) {
        return copies.get(bookCoverIndex);
    }

    public int getNbBookCovers() {
        return copies.size();
    }

    public int getNbOffsetPlates() {
        return nbOffsetPlates;
    }

    public int getNbSlotsPerPlate() {
        return nbSlotsPerPlate;
    }

    public int getTotalNbSlots() {
        return nbOffsetPlates * nbSlotsPerPlate;
    }

    @Override
    public String toString() {
        final String bookCovers = IntStream.range(0, copies.size())
                .mapToObj(i -> String.format("%3d:%6d", i + 1, copies.get(i)))
                .collect(Collectors.joining(", "));
        return String.format("Book covers: %s\n" +
                        "Over %d offset plates, each containing %d slots.",
                bookCovers,
                nbOffsetPlates,
                nbSlotsPerPlate
        );
    }
}
