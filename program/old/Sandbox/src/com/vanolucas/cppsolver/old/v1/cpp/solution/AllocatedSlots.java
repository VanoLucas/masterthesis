package com.vanolucas.cppsolver.old.v1.cpp.solution;

import com.vanolucas.cppsolver.old.v1.genetic.Phenotype;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class AllocatedSlots implements Phenotype {

    private List<Slot> slots;

    public AllocatedSlots(List<Slot> slots) {
        setSlots(slots);
    }

    public List<Slot> getSlots(int startIndex, int endIndex) {
        return slots.subList(startIndex, endIndex);
    }

    public void setSlots(List<Slot> slots) {
        this.slots = slots;
        Collections.sort(this.slots);
    }

    @Override
    public String toString() {
        return String.format("Slots: %s",
                slots.stream()
                        .map(Objects::toString)
                        .collect(Collectors.joining(" | "))
        );
    }
}
