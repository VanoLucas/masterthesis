package com.vanolucas.cppsolver.old.v1.cpp.solution;

import java.util.Collection;

/**
 * Represents an offset plate and its number of printings.
 */
public class OffsetPlatePrintings implements Comparable<OffsetPlatePrintings> {

    private OffsetPlate offsetPlate;
    private int nbPrintings;

    private OffsetPlatePrintings(Collection<Slot> slots) {
        this(OffsetPlate.withSlots(slots), maxNbPrintings(slots));
    }

    public OffsetPlatePrintings(OffsetPlate offsetPlate, int nbPrintings) {
        this.offsetPlate = offsetPlate;
        this.nbPrintings = nbPrintings;
    }

    public static OffsetPlatePrintings fromSlots(Collection<Slot> slots) {
        return new OffsetPlatePrintings(slots);
    }

    private static int maxNbPrintings(Collection<Slot> slots) {
        return slots.stream()
                .mapToInt(Slot::getNbPrintingsRequired)
                .max()
                .getAsInt();
    }

    public OffsetPlate getOffsetPlate() {
        return offsetPlate;
    }

    public int getNbPrintings() {
        return nbPrintings;
    }

    @Override
    public int compareTo(OffsetPlatePrintings o) {
        return Integer.compare(nbPrintings, o.nbPrintings);
    }

    @Override
    public String toString() {
        return String.format("%s: %d printings",
                offsetPlate.toString(),
                nbPrintings
        );
    }
}
