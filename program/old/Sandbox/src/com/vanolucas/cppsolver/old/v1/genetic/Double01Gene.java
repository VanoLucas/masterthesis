package com.vanolucas.cppsolver.old.v1.genetic;

import com.vanolucas.cppsolver.old.v1.util.math.Rand;

/**
 * A gene that is a double in the range [0..1).
 */
public class Double01Gene implements Gene {

    /**
     * Value of this gene.
     * Double in the range 0.0d (inclusive) to 1.0d (exclusive).
     */
    private Double value;

    public Double01Gene(Double value) {
        setValue(value);
    }

    public static Double01Gene newRandom(Rand rng) {
        return new Double01Gene(rng.double01());
    }

    public static Double01Gene newInit(Double initValue) {
        return new Double01Gene(initValue);
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        checkValid(value);
        this.value = value;
    }

    public void plus(double valueToAdd) {
        value += valueToAdd;

        // block on [0..1) bounds
        if (value >= 1.0d) {
            value = Math.nextDown(1.0d);
        } else if (value < 0.0d) {
            value = 0.0d;
        }
    }

    private void checkValid(Double value) {
        if (value != null &&
                (value < 0d || value >= 1d)) {
            throw new IllegalArgumentException(
                    String.format(
                            "A %s can only store a double value between 0.0d (inclusive) and 1.0d (exclusive). " +
                                    "%f is therefore not a legal value.",
                            getClass().getSimpleName(),
                            value
                    )
            );
        }
    }

    @Override
    public void randomize(Rand rng) {
        value = rng.double01();
    }

    @Override
    public Double01Gene clone() throws CloneNotSupportedException {
        Double01Gene cloned = (Double01Gene) super.clone();
        cloned.value = value;
        return cloned;
    }

    @Override
    public String toString() {
        return value.toString();
    }
}
