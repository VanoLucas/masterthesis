package com.vanolucas.cppsolver.old.v1.genetic;

/**
 * Represents the genotype (genetic) representation of a solution.
 */
public interface Genotype extends Cloneable {

    Genotype clone() throws CloneNotSupportedException;
}
