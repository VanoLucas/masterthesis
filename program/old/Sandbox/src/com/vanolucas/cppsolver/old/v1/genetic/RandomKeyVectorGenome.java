package com.vanolucas.cppsolver.old.v1.genetic;

import com.vanolucas.cppsolver.old.v1.util.math.Rand;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

/**
 * A random-key vector genome is a genome for which the genes are a sequence of
 * double in the range 0.0d (inclusive) to 1.0d (exclusive).
 */
public class RandomKeyVectorGenome extends Genome {

    protected RandomKeyVectorGenome(int length, Double initValue) {
        this(createGenes(length, initValue));
    }

    protected RandomKeyVectorGenome(int length, Rand rng) {
        this(createRandomGenes(length, rng));
    }

    protected RandomKeyVectorGenome(List<Gene> genes) {
        super(genes);
    }

    public static RandomKeyVectorGenome newInit(int length, Double initValue) {
        return new RandomKeyVectorGenome(length, initValue);
    }

    private static List<Gene> createGenes(int length, Double initValue) {
        // create random 0..1 double genes
        List<Gene> genes = new ArrayList<>(length);
        for (int i = 0; i < length; i++) {
            genes.add(
                    Double01Gene.newInit(initValue)
            );
        }
        // construct the genome
        return genes;
    }

    private static List<Gene> createRandomGenes(int length, Rand rng) {
        // create random 0..1 double genes
        List<Gene> genes = new ArrayList<>(length);
        for (int i = 0; i < length; i++) {
            genes.add(
                    Double01Gene.newRandom(rng)
            );
        }
        // construct the genome
        return genes;
    }

    public List<Double> getKeys() {
        return genes.stream()
                .map(gene -> ((Double01Gene) gene).getValue())
                .collect(Collectors.toList());
    }

    public List<Double> getKeys(int startIndex, int endIndex) {
        return genes
                .subList(startIndex, endIndex)
                .stream()
                .map(gene -> ((Double01Gene) gene).getValue())
                .collect(Collectors.toList());
    }

    public Stream<Double01Gene> streamOfGenes() {
        return genes.stream()
                .map(gene -> (Double01Gene) gene);
    }

    public DoubleStream doubleStream() {
        return stream()
                .mapToDouble(gene -> {
                    final Double01Gene double01Gene = (Double01Gene) gene;
                    return double01Gene.getValue();
                });
    }

    public void forEachGene(Consumer<Double01Gene> action) {
        streamOfGenes().forEach(action);
    }
}
