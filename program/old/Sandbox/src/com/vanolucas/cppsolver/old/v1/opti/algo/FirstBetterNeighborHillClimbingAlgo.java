package com.vanolucas.cppsolver.old.v1.opti.algo;

import com.vanolucas.cppsolver.old.v1.opti.clone.IndividualCloner;
import com.vanolucas.cppsolver.old.v1.opti.evaluation.IndividualEvaluator;
import com.vanolucas.cppsolver.old.v1.opti.indiv.Individual;
import com.vanolucas.cppsolver.old.v1.opti.mutation.IndividualMutator;
import com.vanolucas.cppsolver.old.v1.opti.randomizer.IndividualRandomizer;
import com.vanolucas.cppsolver.old.v1.util.math.Rand;

import java.util.ArrayList;
import java.util.List;

public class FirstBetterNeighborHillClimbingAlgo extends HillClimbingAlgo {

    /**
     * Random numbers generator.
     * Used when we need to pick an equivalent neighbor at random.
     */
    private Rand rand = new Rand();

    public FirstBetterNeighborHillClimbingAlgo(Individual startIndividual,
                                               IndividualRandomizer randomizer,
                                               IndividualCloner cloner,
                                               List<IndividualMutator> mutators,
                                               IndividualEvaluator evaluator) {
        super(startIndividual, randomizer, cloner, mutators, evaluator);
    }

    @Override
    protected Individual selectNeighbor() throws CloneNotSupportedException {
        final int nbNeighbors = mutators.size();

        // to remember neighbors that have an equivalent quality to the current solution
        List<Individual> equivalentNeighbors = new ArrayList<>(nbNeighbors);

        // generate and evaluate each neighbor
        for (int i = 0; i < nbNeighbors; i++) {
            // create a clone of the current solution
            final Individual neighbor = cloner.clone(currentIndividual);
            // mutate it using the mutation operator for this neighbor
            neighbor.mutateUsing(mutators.get(i));
            // evaluate the mutated neighbor
            neighbor.evaluateUsing(evaluator);

            // if it is better than the current solution, choose it
            if (neighbor.isBetterThan(currentIndividual)) {
                // this is the first better neighbor, fine to stop here
                return neighbor;
            }
            // else if it is as good as the current solution, keep it as potential next
            else if (neighbor.isEquivalentTo(currentIndividual)) {
                equivalentNeighbors.add(neighbor);
            }
        }

        // we get here if there was no better neighbor
        // notify listeners that we have no better neighbor at this iteration
        observable.notifyListeners(HillClimbingAlgoListener.class, listener ->
                listener.onNoBetterNeighbor(iteration, currentIndividual)
        );

        // if there is at least an equivalent neighbor, pick one at random
        if (!equivalentNeighbors.isEmpty()) {
            // pick an equivalent neighbor at random
            if (equivalentNeighbors.size() == 1) {
                return equivalentNeighbors.get(0);
            } else {
                return rand.uniformPick(equivalentNeighbors);
            }
        }

        // we get here when all neighbors are worse

        // notify listeners that we reached a local optimum
        observable.notifyListeners(HillClimbingAlgoListener.class, listener ->
                listener.onAllNeighborsWorse(iteration, currentIndividual)
        );

        return null;
    }
}
