package com.vanolucas.cppsolver.old.v1.opti.algo;

import com.vanolucas.cppsolver.old.v1.opti.clone.IndividualCloner;
import com.vanolucas.cppsolver.old.v1.opti.evaluation.IndividualEvaluator;
import com.vanolucas.cppsolver.old.v1.opti.indiv.Individual;
import com.vanolucas.cppsolver.old.v1.opti.mutation.IndividualMutator;
import com.vanolucas.cppsolver.old.v1.opti.quality.Quality;
import com.vanolucas.cppsolver.old.v1.opti.randomizer.IndividualRandomizer;
import com.vanolucas.cppsolver.old.v1.util.math.Rand;

import java.util.List;

public abstract class HillClimbingAlgo extends OptiAlgo {

    /**
     * Current solution of this hill climbing search.
     */
    protected Individual currentIndividual;
    /**
     * Algorithm that randomizes the solution.
     */
    private IndividualRandomizer randomizer;
    /**
     * Cloner to create copies of Individuals.
     */
    protected IndividualCloner cloner;
    /**
     * Mutation (neighboring) operators.
     */
    protected List<IndividualMutator> mutators;
    /**
     * Evaluation function of the solutions.
     */
    protected IndividualEvaluator evaluator;
    /**
     * Tells whether or not we should randomize the current solution at next iteration.
     * This is to trigger a random restart of the hill climbing search, usually to get out of a local optimum.
     */
    protected boolean randomRestartAtNextIteration = false;
    /**
     * The random number generator that this algo is using.
     */
    private Rand rand;

    public HillClimbingAlgo(Individual startIndividual,
                            IndividualRandomizer randomizer,
                            IndividualCloner cloner,
                            List<IndividualMutator> mutators,
                            IndividualEvaluator evaluator) {
        this.currentIndividual = startIndividual;
        this.randomizer = randomizer;
        this.cloner = cloner;
        this.mutators = mutators;
        this.evaluator = evaluator;

        // init random numbers generator
        this.rand = new Rand();
    }

    @Override
    protected void runIteration() throws CloneNotSupportedException {
        // perform random restart if needed
        if (randomRestartAtNextIteration) {
            randomRestartAtNextIteration = false;
            final Quality qualityBeforeRestart = currentIndividual.getQuality();
            // randomize the current solution
            currentIndividual.randomizeUsing(randomizer, rand);
            // notify listeners that we are performing a random restart of this hill climbing
            observable.notifyListeners(HillClimbingAlgoListener.class, listener ->
                    listener.onRandomRestart(iteration, qualityBeforeRestart)
            );
        }

        // generate, evaluate and choose a neighbor
        final Individual selectedNeighbor = selectNeighbor();

        // if we have a neighbor to move to
        if (selectedNeighbor != null) {
            // if the neighbor we are moving to is better than the current solution, let listeners know we made progress
            if (selectedNeighbor.isBetterThan(currentIndividual)) {
                observable.notifyListeners(HillClimbingAlgoListener.class, listener ->
                        listener.onQualityProgressSinceLastIteration(currentIndividual.getQuality(), selectedNeighbor.getQuality())
                );
            }

            // make that neighbor our new current solution
            currentIndividual = selectedNeighbor;

            // update best
            proposeNewBetter(currentIndividual);
        }
    }

    protected abstract Individual selectNeighbor() throws CloneNotSupportedException;

    public void randomRestartAtNextIteration() {
        this.randomRestartAtNextIteration = true;
    }
}
