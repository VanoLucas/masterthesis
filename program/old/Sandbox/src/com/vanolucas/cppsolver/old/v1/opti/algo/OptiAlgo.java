package com.vanolucas.cppsolver.old.v1.opti.algo;

import com.vanolucas.cppsolver.old.v1.opti.indiv.Individual;
import com.vanolucas.cppsolver.old.v1.opti.pause.PauseCriterion;
import com.vanolucas.cppsolver.old.v1.opti.quality.Quality;
import com.vanolucas.cppsolver.old.v1.util.Observable;

import java.util.ArrayList;
import java.util.List;

/**
 * Base class for an iterative single-objective optimization algorithm.
 */
public abstract class OptiAlgo {

    /**
     * Current iteration of this optimization algorithm.
     * 0 if no optimization iteration has started yet.
     */
    protected long iteration = 0L;

    /**
     * The best quality found so far.
     */
    private Quality bestQuality = null;

    /**
     * Criteria to check against before each iteration to determine
     * whether or not to pause this optimization algo.
     */
    private final List<PauseCriterion> pauseCriteria = new ArrayList<>(2);

    /**
     * This opti algo is observable, it can notify listeners of its events.
     */
    protected final Observable<OptiAlgoListener> observable = new Observable<>(4);

    /**
     * @return Current iteration number of this optimization algorithm.
     */
    public long getIteration() {
        return iteration;
    }

    /**
     * @return The best quality found so far.
     */
    public Quality getBestQuality() {
        return bestQuality;
    }

    /**
     * Start running this optimization algorithm.
     */
    public void run() throws CloneNotSupportedException {
        // remove expired pause criteria
        removeExpiredPauseCriteria();

        // run this opti algo until we reach a pause criterion
        while (!shouldPause()) {
            // remove expired pause criteria
            removeExpiredPauseCriteria();

            // start next iteration
            iteration++;

            // execute one iteration of this opti algo
            runIteration();
            // notify listeners that this iteration finished
            observable.notifyListeners(listener -> listener.onIterationEnd(iteration));
        }
    }

    /**
     * Execute one optimization iteration.
     * To be overridden for each specific optimization strategy.
     */
    protected abstract void runIteration() throws CloneNotSupportedException;

    public void addListener(OptiAlgoListener listener) {
        observable.addListener(listener);
    }

    public void removeListener(OptiAlgoListener listenerToRemove) {
        observable.removeListener(listenerToRemove);
    }

    public void addPauseCriterion(PauseCriterion pauseCriterion) {
        // if it is a listener, also add this pause criterion as listener
        if (pauseCriterion instanceof OptiAlgoListener) {
            addListener((OptiAlgoListener) pauseCriterion);
        }
        // add this pause criterion
        pauseCriteria.add(pauseCriterion);
    }

    public void removePauseCriterion(PauseCriterion pauseCriterionToRemove) {
        // remove pause criterion
        pauseCriteria.remove(pauseCriterionToRemove);
        // also remove the pause criterion from listeners if it is a listener
        if (pauseCriterionToRemove instanceof OptiAlgoListener) {
            removeListener((OptiAlgoListener) pauseCriterionToRemove);
        }
    }

    /**
     * @param candidate Newly found solution to submit as candidate for new best.
     * @return True if the provided individual's quality is better than the previous best known one.
     */
    protected boolean proposeNewBetter(Individual candidate) throws CloneNotSupportedException {
        // if our candidate is the new best
        if (proposeNewBetter(candidate.getQuality())) {
            // notify: new better solution found
            observable.notifyListeners(listener -> listener.onNewBetter(iteration, candidate));
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param candidate Newly found quality to submit as candidate for new best.
     * @return True if the provided quality is better than the previous best one known.
     */
    private boolean proposeNewBetter(Quality candidate) throws CloneNotSupportedException {
        // if the candidate is better than current best (or we have no best yet)
        if (bestQuality == null
                || candidate.isBetterThan(bestQuality)) {
            // update new best
            bestQuality = candidate.clone();
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return True if any pause criterion asks to pause.
     */
    private boolean shouldPause() {
        return pauseCriteria.stream()
                .anyMatch(pauseCriterion ->
                        pauseCriterion.shouldPause(this)
                );
    }

    /**
     * Remove pause criteria that have expired.
     */
    private void removeExpiredPauseCriteria() {
        if (pauseCriteria.size() > 0) {
            pauseCriteria.stream()
                    .filter(pauseCriterion ->
                            !pauseCriterion.shouldKeepCriterion(this)
                    )
                    .forEach(this::removePauseCriterion);
        }
    }
}
