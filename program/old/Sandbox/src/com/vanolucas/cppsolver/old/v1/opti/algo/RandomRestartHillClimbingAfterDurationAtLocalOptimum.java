package com.vanolucas.cppsolver.old.v1.opti.algo;

import com.vanolucas.cppsolver.old.v1.opti.indiv.Individual;
import com.vanolucas.cppsolver.old.v1.opti.quality.Quality;

import java.time.Duration;
import java.time.Instant;

/**
 * Tells a Hill Climbing algo to perform a random restart when it has been stagnating at a local optimum for too long.
 */
public class RandomRestartHillClimbingAfterDurationAtLocalOptimum implements HillClimbingAlgoListener {

    private final HillClimbingAlgo algo;
    private Duration durationStuckAtLocalOptimum;
    private Instant timeLastQualityProgress;

    private RandomRestartHillClimbingAfterDurationAtLocalOptimum(HillClimbingAlgo algo, Duration durationStuckAtLocalOptimum) {
        this.algo = algo;
        this.durationStuckAtLocalOptimum = durationStuckAtLocalOptimum;
        this.algo.addListener(this);
    }

    public static RandomRestartHillClimbingAfterDurationAtLocalOptimum applyTo(HillClimbingAlgo algo, Duration durationStuckAtLocalOptimum) {
        return new RandomRestartHillClimbingAfterDurationAtLocalOptimum(algo, durationStuckAtLocalOptimum);
    }

    @Override
    public void onQualityProgressSinceLastIteration(Quality prevSolutionQuality, Quality currentSolutionQuality) {
        timeLastQualityProgress = Instant.now();
    }

    @Override
    public void onRandomRestart(long iteration, Quality qualityBeforeRestart) {
        timeLastQualityProgress = Instant.now();
    }

    @Override
    public void onNoBetterNeighbor(long iteration, Individual currentIndiv) {
        // if we have been stuck at this local optimum for more than the allowed duration
        if (timeLastQualityProgress != null
                && timeLastQualityProgress.plus(durationStuckAtLocalOptimum).isBefore(Instant.now())) {
            // ask the algo to perform a random restart at next iteration
            algo.randomRestartAtNextIteration();
        }
    }
}
