package com.vanolucas.cppsolver.old.v1.opti.algo;

import com.vanolucas.cppsolver.old.v1.util.math.Rand;
import com.vanolucas.cppsolver.old.v1.opti.indiv.Individual;
import com.vanolucas.cppsolver.old.v1.opti.evaluation.IndividualEvaluator;
import com.vanolucas.cppsolver.old.v1.opti.randomizer.IndividualRandomizer;

/**
 * An example very naive optimization algorithm.
 * It just generates and evaluates a random solution at each iteration.
 */
public class RandomSearchAlgo extends OptiAlgo {

    /**
     * Current solution of this random search.
     */
    private Individual currentIndividual;
    /**
     * Algorithm that randomizes the solution.
     */
    private IndividualRandomizer randomizer;
    /**
     * Evaluation function of the solutions.
     */
    private IndividualEvaluator evaluator;
    /**
     * The random number generator that this algo is using.
     */
    private Rand rng;

    public RandomSearchAlgo(Individual individual,
                            IndividualRandomizer randomizer,
                            IndividualEvaluator evaluator) {
        this.currentIndividual = individual;
        this.randomizer = randomizer;
        this.evaluator = evaluator;

        // init random numbers generator
        this.rng = new Rand();
    }

    /**
     * One iteration of the random search.
     */
    @Override
    protected void runIteration() throws CloneNotSupportedException {
        // randomize the current solution
        currentIndividual.randomizeUsing(randomizer, rng);
        // evaluate it
        currentIndividual.evaluateUsing(evaluator);
        // compare to the current best
        proposeNewBetter(currentIndividual);
    }
}
