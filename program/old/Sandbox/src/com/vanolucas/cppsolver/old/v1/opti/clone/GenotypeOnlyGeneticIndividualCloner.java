package com.vanolucas.cppsolver.old.v1.opti.clone;

import com.vanolucas.cppsolver.old.v1.opti.indiv.GeneticIndividual;
import com.vanolucas.cppsolver.old.v1.opti.indiv.Individual;

public class GenotypeOnlyGeneticIndividualCloner implements GeneticIndividualCloner {

    @Override
    public GeneticIndividual clone(Individual original) throws CloneNotSupportedException {
        return clone((GeneticIndividual) original);
    }

    public GeneticIndividual clone(GeneticIndividual original) throws CloneNotSupportedException {
        return new GeneticIndividual(
                original.getGenotype().clone()
        );
    }
}
