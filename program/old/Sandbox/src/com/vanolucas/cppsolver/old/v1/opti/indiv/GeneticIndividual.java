package com.vanolucas.cppsolver.old.v1.opti.indiv;

import com.vanolucas.cppsolver.old.v1.genetic.Genotype;
import com.vanolucas.cppsolver.old.v1.genetic.GenotypeDecoder;
import com.vanolucas.cppsolver.old.v1.genetic.Phenotype;
import com.vanolucas.cppsolver.old.v1.genetic.PhenotypeDecoder;
import com.vanolucas.cppsolver.old.v1.opti.mutation.GenotypeMutator;

/**
 * Represents a solution that can be defined by a genotype.
 * An optimization algorithm can work with this.
 */
public class GeneticIndividual extends Individual {

    private Genotype genotype;
    private Phenotype phenotype;
    private Object solution;

    public GeneticIndividual(Genotype genotype) {
        this(genotype, null, null);
    }

    public GeneticIndividual(Genotype genotype, Phenotype phenotype, Object solution) {
        this.genotype = genotype;
        this.phenotype = phenotype;
        this.solution = solution;
    }

    public Genotype getGenotype() {
        return genotype;
    }

    public Object getSolution() {
        return solution;
    }

    public void decodeGenotypeUsing(GenotypeDecoder decoder) {
        phenotype = decoder.decode(genotype);
    }

    public void decodePhenotypeUsing(PhenotypeDecoder decoder) {
        solution = decoder.decode(phenotype);
    }

    public void mutateGenotypeUsing(GenotypeMutator mutator) {
        mutator.mutate(genotype);
    }

    @Override
    public String toString() {
        return String.format("Genotype:\n%s\n" +
                        "Phenotype:\n%s\n" +
                        "Solution:\n%s",
                genotype.toString(),
                phenotype.toString(),
                solution.toString()
        );
    }
}
