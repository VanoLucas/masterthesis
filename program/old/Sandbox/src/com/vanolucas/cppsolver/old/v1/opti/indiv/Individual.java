package com.vanolucas.cppsolver.old.v1.opti.indiv;

import com.vanolucas.cppsolver.old.v1.opti.mutation.IndividualMutator;
import com.vanolucas.cppsolver.old.v1.util.math.Rand;
import com.vanolucas.cppsolver.old.v1.opti.evaluation.IndividualEvaluator;
import com.vanolucas.cppsolver.old.v1.opti.quality.Quality;
import com.vanolucas.cppsolver.old.v1.opti.randomizer.IndividualRandomizer;

/**
 * Represents a solution that an optimization algorithm can work with.
 */
public abstract class Individual {

    /**
     * Quality of this solution.
     */
    private Quality quality = null;

    /**
     * @return This individual's quality.
     */
    public Quality getQuality() {
        return quality;
    }

    public boolean isBetterThan(Individual other) {
        if (other == null || other.quality == null) {
            return true;
        }
        return quality.isBetterThan(other.quality);
    }

    public boolean isEquivalentTo(Individual other) {
        if (other == null) {
            return false;
        }
        return quality.isEquivalentTo(other.quality);
    }

    /**
     * Update the quality of this solution using the provided evaluation function.
     *
     * @param individualEvaluator Evaluation function to use to evaluate this solution.
     */
    public void evaluateUsing(IndividualEvaluator individualEvaluator) {
        quality = individualEvaluator.evaluate(this);
    }

    public void mutateUsing(IndividualMutator individualMutator) {
        quality = null;
        individualMutator.mutate(this);
    }

    /**
     * Randomize this solution using the provided IndividualRandomizer.
     *
     * @param rng Random number provider to use.
     */
    public void randomizeUsing(IndividualRandomizer randomizer, Rand rng) {
        quality = null;
        randomizer.randomize(this, rng);
    }
}
