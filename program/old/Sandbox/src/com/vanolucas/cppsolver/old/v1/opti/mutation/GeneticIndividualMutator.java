package com.vanolucas.cppsolver.old.v1.opti.mutation;

import com.vanolucas.cppsolver.old.v1.opti.indiv.GeneticIndividual;
import com.vanolucas.cppsolver.old.v1.opti.indiv.Individual;

public interface GeneticIndividualMutator extends IndividualMutator {

    @Override
    default void mutateIndividual(Individual individual) {
        mutateIndividual((GeneticIndividual) individual);
    }

    void mutateIndividual(GeneticIndividual individual);
}
