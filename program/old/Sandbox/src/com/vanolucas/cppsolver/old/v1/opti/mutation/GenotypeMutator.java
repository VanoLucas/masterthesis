package com.vanolucas.cppsolver.old.v1.opti.mutation;

import com.vanolucas.cppsolver.old.v1.genetic.Genotype;

public interface GenotypeMutator {

    void mutate(Genotype genotype);
}
