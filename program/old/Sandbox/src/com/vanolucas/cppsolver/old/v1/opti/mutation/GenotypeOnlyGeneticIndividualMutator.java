package com.vanolucas.cppsolver.old.v1.opti.mutation;

import com.vanolucas.cppsolver.old.v1.opti.indiv.GeneticIndividual;

public class GenotypeOnlyGeneticIndividualMutator implements GeneticIndividualMutator {

    private GenotypeMutator genotypeMutator;

    public GenotypeOnlyGeneticIndividualMutator(GenotypeMutator genotypeMutator) {
        this.genotypeMutator = genotypeMutator;
    }

    @Override
    public void mutateIndividual(GeneticIndividual individual) {
        individual.mutateGenotypeUsing(genotypeMutator);
    }
}
