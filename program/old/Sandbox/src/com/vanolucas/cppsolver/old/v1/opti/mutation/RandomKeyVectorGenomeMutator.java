package com.vanolucas.cppsolver.old.v1.opti.mutation;

import com.vanolucas.cppsolver.old.v1.genetic.Genome;
import com.vanolucas.cppsolver.old.v1.genetic.RandomKeyVectorGenome;
import com.vanolucas.cppsolver.old.v1.opti.mutation.genome.GenomeMutator;

public interface RandomKeyVectorGenomeMutator extends GenomeMutator {

    @Override
    default void mutate(Genome genome) {
        mutate((RandomKeyVectorGenome) genome);
    }

    void mutate(RandomKeyVectorGenome genome);
}
