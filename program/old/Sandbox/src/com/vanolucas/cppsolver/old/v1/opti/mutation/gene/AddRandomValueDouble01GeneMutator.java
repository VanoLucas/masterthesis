package com.vanolucas.cppsolver.old.v1.opti.mutation.gene;

import com.vanolucas.cppsolver.old.v1.genetic.Double01Gene;
import com.vanolucas.cppsolver.old.v1.util.math.Rand;

public class AddRandomValueDouble01GeneMutator implements Double01GeneMutator {

    private double minValueToAdd;
    private double maxValueToAdd;

    private Rand rand;

    public AddRandomValueDouble01GeneMutator(double minValueToAdd, double maxValueToAdd) {
        this(minValueToAdd, maxValueToAdd, new Rand());
    }

    public AddRandomValueDouble01GeneMutator(double minValueToAdd, double maxValueToAdd, Rand rand) {
        this.minValueToAdd = minValueToAdd;
        this.maxValueToAdd = maxValueToAdd;
        this.rand = rand;
    }

    @Override
    public void mutateGene(Double01Gene gene) {
        gene.plus(
                rand.doubleInRange(minValueToAdd, maxValueToAdd)
        );
    }
}
