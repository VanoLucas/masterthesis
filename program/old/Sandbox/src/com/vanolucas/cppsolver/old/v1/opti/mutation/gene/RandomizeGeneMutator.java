package com.vanolucas.cppsolver.old.v1.opti.mutation.gene;

import com.vanolucas.cppsolver.old.v1.genetic.Gene;
import com.vanolucas.cppsolver.old.v1.util.math.Rand;

public class RandomizeGeneMutator implements GeneMutator {

    private Rand rand;

    public RandomizeGeneMutator() {
        this(new Rand());
    }

    public RandomizeGeneMutator(Rand rand) {
        this.rand = rand;
    }

    @Override
    public void mutateGene(Gene gene) {
        gene.randomize(rand);
    }
}
