package com.vanolucas.cppsolver.old.v1.opti.mutation.genome;

import com.vanolucas.cppsolver.old.v1.genetic.Genome;
import com.vanolucas.cppsolver.old.v1.genetic.Genotype;
import com.vanolucas.cppsolver.old.v1.opti.mutation.GenotypeMutator;

public interface GenomeMutator extends GenotypeMutator {

    @Override
    default void mutate(Genotype genotype) {
        mutate((Genome) genotype);
    }

    void mutate(Genome genome);
}
