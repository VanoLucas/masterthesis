package com.vanolucas.cppsolver.old.v1.opti.quality;

public class Cost extends Quality {

    public Cost(double value) {
        super(value);
    }

    @Override
    public boolean isBetterThan(Quality o) {
        return value < o.value;
    }
}
