package com.vanolucas.cppsolver.old.v1.opti.randomizer;

import com.vanolucas.cppsolver.old.v1.util.math.Rand;
import com.vanolucas.cppsolver.old.v1.opti.indiv.GeneticIndividual;
import com.vanolucas.cppsolver.old.v1.opti.indiv.Individual;

/**
 * Randomizes a given genetic individual.
 */
public abstract class GeneticIndividualRandomizer extends IndividualRandomizer {

    public GeneticIndividualRandomizer() {
        super();
    }

    public GeneticIndividualRandomizer(Rand rand) {
        super(rand);
    }

    @Override
    public void randomize(Individual individual, Rand rng) {
        randomize((GeneticIndividual) individual, rng);
    }

    public abstract void randomize(GeneticIndividual individual, Rand rng);
}
