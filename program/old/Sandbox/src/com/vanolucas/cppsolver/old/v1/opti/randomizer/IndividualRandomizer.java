package com.vanolucas.cppsolver.old.v1.opti.randomizer;

import com.vanolucas.cppsolver.old.v1.util.math.Rand;
import com.vanolucas.cppsolver.old.v1.opti.indiv.Individual;

/**
 * Randomizes a given Individual.
 */
public abstract class IndividualRandomizer {

    /**
     * Random numbers generator.
     */
    private Rand rand;

    public IndividualRandomizer() {
        this(null);
    }

    public IndividualRandomizer(Rand rand) {
        this.rand = rand;
    }

    public void randomize(Individual individual) {
        if (rand == null) {
            rand = new Rand();
        }
        randomize(individual, rand);
    }

    /**
     * @param individual Individual to randomize.
     * @param rng        Random numbers provider to use.
     */
    public abstract void randomize(Individual individual, Rand rng);
}
