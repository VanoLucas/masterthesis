package com.vanolucas.cppsolver.old.v1.opti.randomizer;

import com.vanolucas.cppsolver.old.v1.genetic.Genotype;
import com.vanolucas.cppsolver.old.v1.util.math.Rand;
import com.vanolucas.cppsolver.old.v1.util.Randomizable;
import com.vanolucas.cppsolver.old.v1.opti.indiv.GeneticIndividual;

/**
 * To randomize genetic individuals that have a Randomizable genotype.
 */
public class RandomizableGenotypeRandomizer extends GeneticIndividualRandomizer {

    public RandomizableGenotypeRandomizer() {
        super();
    }

    public RandomizableGenotypeRandomizer(Rand rand) {
        super(rand);
    }

    @Override
    public void randomize(GeneticIndividual individual, Rand rng) {
        final Genotype genotype = individual.getGenotype();
        if (genotype instanceof Randomizable) {
            final Randomizable randomizableGenotype = (Randomizable) genotype;
            randomizableGenotype.randomize(rng);
        } else {
            throw new UnsupportedOperationException(String.format(
                    "The genotype of the provided individual does not implement Randomizable. " +
                            "It can therefore not be randomized using %s.",
                    getClass().getSimpleName()
            ));
        }
    }
}
