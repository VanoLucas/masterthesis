package com.vanolucas.cppsolver.old.v1.sumproblem;

import com.vanolucas.cppsolver.old.v1.genetic.RandomKeyVectorGenome;
import com.vanolucas.cppsolver.old.v1.opti.indiv.GeneticIndividual;

public class SumGeneticIndividual extends GeneticIndividual {

    public SumGeneticIndividual() {
        super(
                RandomKeyVectorGenome.newInit(2, null)
        );
    }
}
