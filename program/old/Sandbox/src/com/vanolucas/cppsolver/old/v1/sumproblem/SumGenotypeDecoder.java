package com.vanolucas.cppsolver.old.v1.sumproblem;

import com.vanolucas.cppsolver.old.v1.genetic.Genotype;
import com.vanolucas.cppsolver.old.v1.genetic.GenotypeDecoder;
import com.vanolucas.cppsolver.old.v1.genetic.RandomKeyVectorGenome;

import java.util.stream.Collectors;

public class SumGenotypeDecoder implements GenotypeDecoder {
    @Override
    public SumPhenotype decode(Genotype genotype) {
        return decode((RandomKeyVectorGenome) genotype);
    }

    public SumPhenotype decode(RandomKeyVectorGenome genome) {
        return new SumPhenotype(
                genome.doubleStream()
                        // map each key of the genome to a number in 0..1000.
                        .mapToInt(key -> (int) (key * 1000d))
                        .boxed()
                        .collect(Collectors.toList())
        );
    }
}
