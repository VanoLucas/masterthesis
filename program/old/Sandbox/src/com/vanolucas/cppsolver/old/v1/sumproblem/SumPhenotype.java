package com.vanolucas.cppsolver.old.v1.sumproblem;

import com.vanolucas.cppsolver.old.v1.genetic.Phenotype;

import java.util.List;
import java.util.stream.Collectors;

public class SumPhenotype implements Phenotype {

    private List<Integer> numbers;

    public SumPhenotype(List<Integer> numbers) {
        this.numbers = numbers;
    }

    public int sum() {
        return numbers.stream().mapToInt(__ -> __).sum();
    }

    @Override
    public String toString() {
        return numbers.stream()
                .map(Object::toString)
                .collect(Collectors.joining(";"));
    }
}
