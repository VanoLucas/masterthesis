package com.vanolucas.cppsolver.old.v1.util.math;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Rand {

    /**
     * Java's random number generator.
     */
    private Random rng;

    public Rand() {
        rng = new Random();
    }

    public Rand(long seed) {
        rng = new Random(seed);
    }

    public int int0ToMaxExclusive(int max) {
        return rng.nextInt(max);
    }

    public int intInRange(int min, int max) {
        return rng.nextInt((max - min) + 1) + min;
    }

    public double doubleInRange(double minInclusive, double maxExclusive) {
        return minInclusive + (maxExclusive - minInclusive) * rng.nextDouble();
    }

    /**
     * @return Random double in the range 0.0d (inclusive) to 1.0d (exclusive).
     */
    public double double01() {
        return rng.nextDouble();
    }

    public <T> T uniformPick(List<T> items) {
        return items.get(
                int0ToMaxExclusive(items.size())
        );
    }

    /**
     * @param keepProba Probability that an item is kept.
     * @return List of items that contains a subset of the initial item list.
     */
    public <T> List<T> keepWithProba(List<T> items, double keepProba) {
        return items.stream()
                .filter(item -> double01() < keepProba)
                .collect(Collectors.toList());
    }
}
