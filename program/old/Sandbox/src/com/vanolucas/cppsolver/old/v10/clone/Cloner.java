package com.vanolucas.cppsolver.old.v10.clone;

/**
 * Function that clones the provided object. It returns a copy of the provided object.
 *
 * @param <T> Type of the objects to clone.
 */
public interface Cloner<T> {
    T clone(T obj);
}
