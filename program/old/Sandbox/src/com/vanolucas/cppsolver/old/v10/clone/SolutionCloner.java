package com.vanolucas.cppsolver.old.v10.clone;

import com.vanolucas.cppsolver.old.v10.solution.Solution;

public interface SolutionCloner extends Cloner<Solution> {
    @Override
    Solution clone(Solution solution);
}
