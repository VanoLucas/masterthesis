package com.vanolucas.cppsolver.old.v10.random;

public interface Randomizable {
    void randomize(Rand rand);
}
