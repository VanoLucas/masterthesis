package com.vanolucas.cppsolver.old.v10.solution;

import com.vanolucas.cppsolver.old.v10.clone.SolutionCloner;
import com.vanolucas.cppsolver.old.v10.random.SolutionRandomizer;

public interface Solution {
    default void randomizeUsing(SolutionRandomizer solutionRandomizer) {
        solutionRandomizer.randomize(this);
    }

    default Solution cloneUsing(SolutionCloner solutionCloner) {
        return solutionCloner.clone(this);
    }
}
