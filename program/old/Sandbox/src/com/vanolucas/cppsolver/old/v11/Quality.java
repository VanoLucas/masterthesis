package com.vanolucas.cppsolver.old.v11;

public interface Quality {

    boolean isBetterThan(Quality other);

    boolean isEquivalentTo(Quality other);

    default boolean isBetterOrEquivalentTo(Quality other) {
        return isBetterThan(other) || isEquivalentTo(other);
    }

    default RelativeQuality relativeTo(Quality other) {
        if (other == null || isBetterThan(other)) {
            return RelativeQuality.BETTER;
        } else if (isEquivalentTo(other)) {
            return RelativeQuality.EQUIVALENT;
        } else {
            return RelativeQuality.WORSE;
        }
    }
}
