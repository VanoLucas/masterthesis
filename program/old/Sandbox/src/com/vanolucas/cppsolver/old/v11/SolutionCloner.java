package com.vanolucas.cppsolver.old.v11;

public interface SolutionCloner extends Cloner<Solution> {
    @Override
    Solution clone(Solution solution);
}
