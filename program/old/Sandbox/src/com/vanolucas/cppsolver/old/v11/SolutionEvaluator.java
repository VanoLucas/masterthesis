package com.vanolucas.cppsolver.old.v11;

public interface SolutionEvaluator extends Evaluator<Solution> {
    @Override
    Quality evaluate(Solution solution);
}
