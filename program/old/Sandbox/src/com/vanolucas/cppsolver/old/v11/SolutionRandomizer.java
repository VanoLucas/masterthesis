package com.vanolucas.cppsolver.old.v11;

public interface SolutionRandomizer extends Randomizer<Solution> {
    @Override
    void randomize(Solution solution);
}
