package com.vanolucas.cppsolver.old.v2.gene;

public abstract class Gene<T> {

    private T value;

    public Gene(T value) {
        setValue(value);
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value.toString();
    }
}
