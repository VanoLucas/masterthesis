package com.vanolucas.cppsolver.old.v3.opti.algo;

import com.vanolucas.cppsolver.old.v3.opti.solution.SolutionWithQuality;
import com.vanolucas.cppsolver.old.v3.util.observable.Observable;

/**
 * Keeps track of the best solutions that are provided to it.
 */
public abstract class BestKeeper {

    /**
     * Makes this BestKeeper observable so that listeners can react to its events.
     */
    private Observable<BestKeeperListener> observable = new Observable<>(2);

    public void addListener(BestKeeperListener listener) {
        observable.addListener(listener);
    }

    public void removeListener(BestKeeperListener listener) {
        observable.removeListener(listener);
    }

    /**
     * @param candidate Solution to propose to this BestKeeper.
     */
    public abstract void propose(SolutionWithQuality candidate);

    /**
     * @param betterSolution Better solution received by this BestKeeper.
     */
    protected void onBetterSolution(SolutionWithQuality betterSolution) {
        // notify listeners that this best keeper received a better solution
        observable.notifyListeners(listener ->
                listener.onBetterSolution(betterSolution)
        );
    }
}
