package com.vanolucas.cppsolver.old.v3.opti.algo;

import com.vanolucas.cppsolver.old.v3.opti.solution.SolutionWithQuality;

public interface BestKeeperListener {

    default void onBetterSolution(SolutionWithQuality betterSolution) {
    }
}
