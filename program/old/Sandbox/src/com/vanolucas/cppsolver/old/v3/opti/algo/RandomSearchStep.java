package com.vanolucas.cppsolver.old.v3.opti.algo;

import com.vanolucas.cppsolver.old.v3.opti.solution.Solution;
import com.vanolucas.cppsolver.old.v3.opti.solution.SolutionWithQuality;
import com.vanolucas.cppsolver.old.v3.util.randomizable.Randomizer;

public class RandomSearchStep extends Step {

    private SolutionWithQuality currentSolution;
    private Randomizer<Solution> randomizer;

    @Override
    protected void runStep() {
        randomizer.randomize(currentSolution.getSolution());

    }
}
