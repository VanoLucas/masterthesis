package com.vanolucas.cppsolver.old.v3.opti.algo;

import com.vanolucas.cppsolver.old.v1.opti.quality.Quality;
import com.vanolucas.cppsolver.old.v3.opti.solution.SolutionWithQuality;

/**
 * Keeps track of the single best Quality that it receives.
 */
public class SingleBestQualityKeeper extends BestKeeper {

    private Quality bestQuality;

    @Override
    public void propose(SolutionWithQuality candidate) {
    }
}
