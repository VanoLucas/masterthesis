package com.vanolucas.cppsolver.old.v3.opti.mutation;

import com.vanolucas.cppsolver.old.v3.opti.solution.Solution;

/**
 * Mutation operator.
 * It performs a mutation operation (modification) on the provided Solution.
 */
public interface Mutator {

    void mutate(Solution solution);
}
