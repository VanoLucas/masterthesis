package com.vanolucas.cppsolver.old.v3.opti.quality;

/**
 * A Cost Quality quantified as a single double value.
 */
public class DoubleCost extends SingleNumberQuality<Double> {

    /**
     * A better cost is a lower one.
     */
    @Override
    public boolean isBetterThan(Quality other) {
        DoubleCost o = (DoubleCost) other;
        return value < o.value;
    }
}
