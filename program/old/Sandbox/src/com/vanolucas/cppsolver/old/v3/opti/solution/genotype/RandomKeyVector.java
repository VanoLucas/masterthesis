package com.vanolucas.cppsolver.old.v3.opti.solution.genotype;

import com.vanolucas.cppsolver.old.v3.util.math.Rand;
import com.vanolucas.cppsolver.old.v3.util.randomizable.Randomizable;

import java.util.List;

public class RandomKeyVector implements Genotype, Randomizable {

    private List<Double> keys;

    public int length() {
        return keys.size();
    }

    @Override
    public void randomize(Rand rand) {
        keys = rand.doubles01(length());
    }
}
