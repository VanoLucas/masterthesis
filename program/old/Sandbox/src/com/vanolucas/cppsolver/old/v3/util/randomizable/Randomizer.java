package com.vanolucas.cppsolver.old.v3.util.randomizable;

import com.vanolucas.cppsolver.old.v3.util.math.Rand;

public abstract class Randomizer<T> {

    private Rand rand;

    public void randomize(T obj) {
        randomize(obj, rand != null ? rand : new Rand());
    }

    public void randomize(Randomizable randomizable, Rand rand) {
        randomizable.randomize(rand);
    }

    public abstract void randomize(T obj, Rand rand);
}
