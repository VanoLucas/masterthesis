package com.vanolucas.cppsolver.old.v4.cpp.evaluation;

import com.vanolucas.cppsolver.old.v4.cpp.genotype.GenomeCP;
import com.vanolucas.cppsolver.old.v4.cpp.instance.CppInstance;
import com.vanolucas.cppsolver.old.v4.cpp.phenotype.AllocatedSlots;
import com.vanolucas.cppsolver.old.v4.cpp.phenotype.SlotPrintings;
import com.vanolucas.cppsolver.old.v4.opti.genotype.GenotypeDecoder;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class DecoderCP implements GenotypeDecoder<GenomeCP, AllocatedSlots> {
    private final int nbBookCovers;
    private final int nbSlots;
    private final List<Integer> copies;

    public DecoderCP(CppInstance cppInstance) {
        this(cppInstance.getNbBookCovers(), cppInstance.getTotalNbSlots(), cppInstance.getCopies());
    }

    public DecoderCP(int nbBookCovers, int nbSlots, List<Integer> copies) {
        this.nbBookCovers = nbBookCovers;
        this.nbSlots = nbSlots;
        this.copies = copies;
    }

    @Override
    public AllocatedSlots convert(GenomeCP genome) {
        // to store our allocated slots
        List<SlotPrintings> slots = new ArrayList<>(nbSlots);

        // initially allocate each book cover to a single slot that fills 100% of its copies demand
        slots.addAll(IntStream.range(0, copies.size())
                .mapToObj(bookCoverIndex -> new SlotPrintings(bookCoverIndex, copies.get(bookCoverIndex)))
                .collect(Collectors.toList())
        );

        // then we need to divide these printings over multiple slots to use all the available slots

        // for each extra slot to fill
        for (int k = 0; k < genome.length(); k += 2) {
            // get the keys that characterize this slot
            final double keyIndexBookCoverToSplit = genome.getKey(k);
            final double fractionToSplitOut = genome.getKey(k + 1);

            // calculate the index of the book cover/slot to split over an extra slot
            final int indexBookCoverToSplit = (int) (keyIndexBookCoverToSplit * (double) nbBookCovers);

            // split a fraction of the printings from the chosen index over an extra slot
            slots.add(
                    slots.get(indexBookCoverToSplit)
                            .split(fractionToSplitOut)
            );
        }

        // place all slots in our phenotype
        return new AllocatedSlots(slots);
    }
}
