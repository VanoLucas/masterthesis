package com.vanolucas.cppsolver.old.v4.cpp.hyperopti;

import com.vanolucas.cppsolver.old.v4.opti.evaluation.EvaluationFunction;
import com.vanolucas.cppsolver.old.v4.opti.genotype.GenotypeDecoder;
import com.vanolucas.cppsolver.old.v4.opti.genotype.RandomKeyVector;
import com.vanolucas.cppsolver.old.v4.opti.genotype.RandomKeyVector2ListDoubleDecoder;
import com.vanolucas.cppsolver.old.v4.opti.problem.GeneticProblem;
import com.vanolucas.cppsolver.old.v4.opti.quality.DoubleCost;
import com.vanolucas.cppsolver.old.v4.util.converter.Converter;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class CppHyperparamsProblem extends GeneticProblem<RandomKeyVector, List<Double>, CppHyperparamsSolution, DoubleCost> {
    private final int nbHyperparams;
    private final int nbRunsPerEval;
    private final int maxEvalPerRun;
    private final List<Double> hyperparamsMin;
    private final List<Double> hyperparamsMax;

    public CppHyperparamsProblem(int nbRunsPerEval, int maxEvalPerRun, List<Double> hyperparamsMin, List<Double> hyperparamsMax) {
        this.nbHyperparams = hyperparamsMin.size();
        this.nbRunsPerEval = nbRunsPerEval;
        this.maxEvalPerRun = maxEvalPerRun;
        this.hyperparamsMin = hyperparamsMin;
        this.hyperparamsMax = hyperparamsMax;
    }

    @Override
    public RandomKeyVector newGenotype() {
        return new RandomKeyVector(nbHyperparams, 0.0d);
    }

    @Override
    public GenotypeDecoder<RandomKeyVector, List<Double>> newGenotypeDecoder() {
        return new RandomKeyVector2ListDoubleDecoder(hyperparamsMin, hyperparamsMax);
    }

    @Override
    public Converter<List<Double>, CppHyperparamsSolution> newPhenotypeDecoder() {
        return new ListDouble2CppHyperparamsSolution(nbRunsPerEval, maxEvalPerRun);
    }

    @Override
    public EvaluationFunction<CppHyperparamsSolution, DoubleCost> newEvaluationFunction() {
        return solution -> {
            try {
                return new DoubleCost(solution.getMedianBestCost() + solution.getAverageBestCost() * 0.00000001d);
            } catch (ExecutionException | InterruptedException e) {
                throw new IllegalStateException(String.format("The evaluation of a %s stopped unexpectedly.",
                        solution.getClass().getSimpleName()
                ));
            }
        };
    }
}
