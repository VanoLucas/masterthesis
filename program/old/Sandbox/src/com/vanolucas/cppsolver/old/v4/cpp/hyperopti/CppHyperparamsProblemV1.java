package com.vanolucas.cppsolver.old.v4.cpp.hyperopti;

import java.util.Arrays;
import java.util.List;

public class CppHyperparamsProblemV1 extends CppHyperparamsProblem {
    private static final List<Double> HYPERPARAMS_MIN = Arrays.asList(
            // full randomization of the genome
            1d,
            // add rand value to all keys (-0.01d, +0.01d)
            0d,
            // add rand value to all keys (-0.001d, +0.001d)
            0d,
            // add rand value to all keys (-0.0001d, +0.0001d)
            0d,
            // randomize 1 random key
            0d,
            // add rand value to 1 random key (-0.01d, +0.01d)
            0d,
            // add rand value to 1 random key (-0.001d, +0.001d)
            0d,
            // add rand value to 1 random key (-0.0001d, +0.0001d)
            0d,
            // randomize 2 random consecutive keys
            0d,
            // add rand value to 2 random consecutive keys (-0.01d, +0.01d)
            0d,
            // add rand value to 2 random consecutive keys (-0.001d, +0.001d)
            0d,
            // add rand value to 2 random consecutive keys (-0.0001d, +0.0001d)
            0d,
            // add rand value to 2 random keys (-0.01d, +0.01d)
            0d,
            // add rand value to 2 random keys (-0.001d, +0.001d)
            0d,
            // add rand value to 2 random keys (-0.0001d, +0.0001d)
            0d
    );
    private static final List<Double> HYPERPARAMS_MAX = Arrays.asList(
            // full randomization of the genome
            2d,
            // add rand value to all keys (-0.01d, +0.01d)
            20d,
            // add rand value to all keys (-0.001d, +0.001d)
            20d,
            // add rand value to all keys (-0.0001d, +0.0001d)
            20d,
            // randomize 1 random key
            20d,
            // add rand value to 1 random key (-0.01d, +0.01d)
            20d,
            // add rand value to 1 random key (-0.001d, +0.001d)
            20d,
            // add rand value to 1 random key (-0.0001d, +0.0001d)
            20d,
            // randomize 2 random consecutive keys
            20d,
            // add rand value to 2 random consecutive keys (-0.01d, +0.01d)
            20d,
            // add rand value to 2 random consecutive keys (-0.001d, +0.001d)
            20d,
            // add rand value to 2 random consecutive keys (-0.0001d, +0.0001d)
            20d,
            // add rand value to 2 random keys (-0.01d, +0.01d)
            20d,
            // add rand value to 2 random keys (-0.001d, +0.001d)
            20d,
            // add rand value to 2 random keys (-0.0001d, +0.0001d)
            20d
    );

    public CppHyperparamsProblemV1(int nbRunsPerEval, int maxEvalPerRun) {
        super(nbRunsPerEval, maxEvalPerRun, HYPERPARAMS_MIN, HYPERPARAMS_MAX);
    }
}
