package com.vanolucas.cppsolver.old.v4.cpp.instance;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Definition of an instance of the Cover Printing Problem.
 */
public class CppInstance {
    private static final int DEFAULT_NB_SLOTS_PER_PLATE = 4;

    /**
     * Number of copies requested for each book cover.
     * (m items = m book covers).
     */
    private List<Integer> copies;
    /**
     * Number of offset plates (n).
     */
    private int nbOffsetPlates;
    /**
     * Number of slots per plate (S).
     */
    private int nbSlotsPerPlate;

    public CppInstance() {
    }

    public CppInstance(List<Integer> copies, int nbOffsetPlates) {
        this(copies, nbOffsetPlates, DEFAULT_NB_SLOTS_PER_PLATE);
    }

    public CppInstance(List<Integer> copies, int nbOffsetPlates, int nbSlotsPerPlate) {
        setCopies(copies);
        setNbOffsetPlates(nbOffsetPlates);
        setNbSlotsPerPlate(nbSlotsPerPlate);
    }

    public CppInstance withCopies(List<Integer> copies) {
        setCopies(copies);
        return this;
    }

    public CppInstance withOffsetPlates(int nbOffsetPlates) {
        setNbOffsetPlates(nbOffsetPlates);
        return this;
    }

    public CppInstance withSlotsPerPlate(int nbSlotsPerPlate) {
        setNbSlotsPerPlate(nbSlotsPerPlate);
        return this;
    }

    public List<Integer> getCopies() {
        return copies;
    }

    public void setCopies(List<Integer> copies) {
        // check if at least 1 book cover
        if (copies == null
                || copies.size() < 1) {
            throw new IllegalArgumentException(
                    "Illegal copies param: there must be at least 1 book cover."
            );
        }
        // check if valid number of copies
        for (Integer item : copies) {
            if (item <= 1) {
                throw new IllegalArgumentException(
                        "Illegal copies param: there must be at least 1 copy of each book cover requested."
                );
            }
        }

        // sort requested copies from greatest to lowest
        final List<Integer> sortedCopies = new ArrayList<>(copies);
        sortedCopies.sort(Collections.reverseOrder());

        this.copies = sortedCopies;
    }

    public int getCopies(int bookCoverIndex) {
        return copies.get(bookCoverIndex);
    }

    public int getNbBookCovers() {
        return copies.size();
    }

    public int getNbOffsetPlates() {
        return nbOffsetPlates;
    }

    public void setNbOffsetPlates(int nbOffsetPlates) {
        // check that we have at least 1 offset plate
        if (nbOffsetPlates < 1) {
            throw new IllegalArgumentException(
                    "Illegal number of offset plates param: there must be at least 1 offset plate."
            );
        }

        this.nbOffsetPlates = nbOffsetPlates;
    }

    public int getNbSlotsPerPlate() {
        return nbSlotsPerPlate;
    }

    public void setNbSlotsPerPlate(int nbSlotsPerPlate) {
        // check that we have at least 1 slot per plate
        if (nbSlotsPerPlate < 1) {
            throw new IllegalArgumentException(
                    "Illegal number of slots per offset plate param: there must be at least 1 slot per offset plate."
            );
        }

        this.nbSlotsPerPlate = nbSlotsPerPlate;
    }

    public int getTotalNbSlots() {
        return nbOffsetPlates * nbSlotsPerPlate;
    }

    @Override
    public String toString() {
        final String bookCovers = IntStream.range(0, copies.size())
                .mapToObj(i -> String.format("%d: %d", i + 1, copies.get(i)))
                .collect(Collectors.joining(", "));
        return String.format("%d book covers: %s\n" +
                        "Over %d offset plates of %d slots.",
                copies.size(),
                bookCovers,
                nbOffsetPlates,
                nbSlotsPerPlate
        );
    }
}
