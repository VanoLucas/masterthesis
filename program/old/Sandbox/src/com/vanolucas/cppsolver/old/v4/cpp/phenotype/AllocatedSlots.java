package com.vanolucas.cppsolver.old.v4.cpp.phenotype;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class AllocatedSlots {

    private List<SlotPrintings> slots;

    public AllocatedSlots(List<SlotPrintings> slots) {
        setSlots(slots);
    }

    public List<SlotPrintings> getSlots(int startIndex, int endIndex) {
        return slots.subList(startIndex, endIndex);
    }

    public void setSlots(List<SlotPrintings> slots) {
        this.slots = slots;
        Collections.sort(this.slots, Collections.reverseOrder());
    }

    @Override
    public String toString() {
        return String.format("%d slots: %s",
                slots.size(),
                slots.stream()
                        .map(Objects::toString)
                        .collect(Collectors.joining(" | "))
        );
    }
}
