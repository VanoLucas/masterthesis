package com.vanolucas.cppsolver.old.v4.cpp.phenotype;

/**
 * Represents the allocation of a book cover to a slot with this slot's minimum required number of printings.
 */
public class SlotPrintings implements Comparable<SlotPrintings> {
    private int bookCoverIndex;
    private int nbPrintingsRequired;

    public SlotPrintings(int bookCoverIndex, int nbPrintingsRequired) {
        this.bookCoverIndex = bookCoverIndex;
        this.nbPrintingsRequired = nbPrintingsRequired;
    }

    public int getBookCoverIndex() {
        return bookCoverIndex;
    }

    public int getNbPrintingsRequired() {
        return nbPrintingsRequired;
    }

    /**
     * Splits the number of printings for this slot over two slots.
     *
     * @param portionToExtract Portion (0..1) of the nb of printings of the initial slot to move to the newly created slot.
     * @return The new slot with a portion of the printings allocated to it.
     */
    public SlotPrintings split(double portionToExtract) {
        final int nbPrintingsToExtract = (int) ((double) nbPrintingsRequired * portionToExtract);
        this.nbPrintingsRequired -= nbPrintingsToExtract;
        return new SlotPrintings(bookCoverIndex, nbPrintingsToExtract);
    }

    @Override
    public int compareTo(SlotPrintings o) {
        return Integer.compare(nbPrintingsRequired, o.nbPrintingsRequired);
    }

    @Override
    public String toString() {
        return String.format("%d (%d required)", bookCoverIndex + 1, nbPrintingsRequired);
    }
}
