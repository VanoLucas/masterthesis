package com.vanolucas.cppsolver.old.v4.cpp.problem;

import com.vanolucas.cppsolver.old.v4.cpp.evaluation.AllocatedSlots2CppSolution;
import com.vanolucas.cppsolver.old.v4.cpp.instance.CppInstance;
import com.vanolucas.cppsolver.old.v4.cpp.phenotype.AllocatedSlots;
import com.vanolucas.cppsolver.old.v4.cpp.solution.CppSolution;
import com.vanolucas.cppsolver.old.v4.opti.evaluation.EvaluationFunction;
import com.vanolucas.cppsolver.old.v4.opti.genotype.Genotype;
import com.vanolucas.cppsolver.old.v4.opti.problem.GeneticProblem;
import com.vanolucas.cppsolver.old.v4.opti.quality.IntCost;
import com.vanolucas.cppsolver.old.v4.util.converter.Converter;

public abstract class CppGeneticProblem<TGenotype extends Genotype> extends GeneticProblem<TGenotype, AllocatedSlots, CppSolution, IntCost> {
    protected final CppInstance cppInstance;

    public CppGeneticProblem(CppInstance cppInstance) {
        this.cppInstance = cppInstance;
    }

    @Override
    public Converter<AllocatedSlots, CppSolution> newPhenotypeDecoder() {
        return new AllocatedSlots2CppSolution(cppInstance.getCopies(), cppInstance.getNbOffsetPlates(), cppInstance.getNbSlotsPerPlate());
    }

    @Override
    public EvaluationFunction<CppSolution, IntCost> newEvaluationFunction() {
        return cppSolution -> new IntCost(cppSolution.getCost());
    }
}
