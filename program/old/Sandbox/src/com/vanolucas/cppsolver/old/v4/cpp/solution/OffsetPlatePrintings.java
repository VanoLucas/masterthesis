package com.vanolucas.cppsolver.old.v4.cpp.solution;

import com.vanolucas.cppsolver.old.v4.cpp.phenotype.SlotPrintings;

import java.util.List;

/**
 * Represents an offset plate and its number of printings.
 */
public class OffsetPlatePrintings implements Comparable<OffsetPlatePrintings> {
    private OffsetPlate offsetPlate;
    private int nbPrintings;

    public OffsetPlatePrintings(List<SlotPrintings> slots) {
        this(new OffsetPlate(slots), maxNbPrintings(slots));
    }

    public OffsetPlatePrintings(OffsetPlate offsetPlate, int nbPrintings) {
        this.offsetPlate = offsetPlate;
        this.nbPrintings = nbPrintings;
    }

    private static int maxNbPrintings(List<SlotPrintings> slots) {
        // the slots we receive here are already sorted, so the first has the most printings
        return slots.get(0).getNbPrintingsRequired();
        /*
        return slots.stream()
                .mapToInt(SlotPrintings::getNbPrintingsRequired)
                .max()
                .getAsInt();
        */
    }

    public OffsetPlate getOffsetPlate() {
        return offsetPlate;
    }

    public int getNbPrintings() {
        return nbPrintings;
    }

    @Override
    public int compareTo(OffsetPlatePrintings o) {
        return Integer.compare(nbPrintings, o.nbPrintings);
    }

    @Override
    public String toString() {
        return String.format("%s: %d printings",
                offsetPlate.toString(),
                nbPrintings
        );
    }
}
