package com.vanolucas.cppsolver.old.v4.opti.algo;

import com.vanolucas.cppsolver.old.v4.opti.bestkeeper.BestKeeper;
import com.vanolucas.cppsolver.old.v4.opti.bestkeeper.BestKeeperListener;
import com.vanolucas.cppsolver.old.v4.opti.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v4.opti.solution.Solution;
import com.vanolucas.cppsolver.old.v4.util.observable.Listeners;
import com.vanolucas.cppsolver.old.v4.util.observable.Observable;

import java.util.concurrent.ExecutionException;

public class Algo implements Observable<AlgoListener> {
    private long iteration = 0L;
    private Step step;
    private BestKeeper bestKeeper;
    private boolean pauseBeforeNextIteration = false;
    private Listeners<AlgoListener> listeners = new Listeners<>(4);

    public Algo(Step step, BestKeeper bestKeeper) {
        setStep(step);
        setBestKeeper(bestKeeper);
    }

    private void setStep(Step step) {
        if (this.step != null) {
            this.step.removeListener(this.stepListener);
        }
        this.step = step;
        this.step.addListener(this.stepListener);
    }

    private void setBestKeeper(BestKeeper bestKeeper) {
        if (this.bestKeeper != null) {
            this.bestKeeper.removeListener(this.bestKeeperListener);
        }
        this.bestKeeper = bestKeeper;
        this.bestKeeper.addListener(this.bestKeeperListener);
    }

    public void run() throws ExecutionException, InterruptedException {
        for (; ; ) {
            if (pauseBeforeNextIteration) {
                pause();
                return;
            }
            step.run(++iteration);
        }
    }

    public void pauseBeforeNextIteration() {
        pauseBeforeNextIteration = true;
    }

    private void pause() {
        pauseBeforeNextIteration = false;
        listeners.notifyListeners(listener ->
                listener.onPause(iteration)
        );
    }

    private StepListener stepListener = new StepListener() {
        @Override
        public void onStepStart(long iteration) {
            listeners.notifyListeners(listener ->
                    listener.onIterationStart(iteration)
            );
        }

        @Override
        public void onStepEnd(long iteration) {
            listeners.notifyListeners(listener ->
                    listener.onIterationEnd(iteration)
            );
        }

        @Override
        public void onRandomized(Solution randomizedSolution, long iteration) {
            listeners.notifyListeners(listener ->
                    listener.onRandomized(randomizedSolution, iteration)
            );
        }

        @Override
        public void onEvaluated(EvaluatedSolution evaluatedSolution, long iteration) {
            listeners.notifyListeners(listener ->
                    listener.onEvaluated(evaluatedSolution, iteration)
            );
        }

        @Override
        public void onSolutionProposal(EvaluatedSolution solution) {
            bestKeeper.proposeSolution(solution);
        }
    };

    private BestKeeperListener bestKeeperListener = new BestKeeperListener() {
        @Override
        public void onBetterSolution(EvaluatedSolution betterSolution) {
            listeners.notifyListeners(listener ->
                    listener.onBetterSolution(betterSolution, iteration)
            );
        }
    };

    @Override
    public void addListener(AlgoListener algoListener) {
        listeners.addListener(algoListener);
        algoListener.onListeningTo(this);
    }

    @Override
    public void removeListener(AlgoListener algoListener) {
        listeners.removeListener(algoListener);
    }

    @Override
    public void removeAllListeners() {
        listeners.removeAllListeners();
    }

    public Algo withListener(AlgoListener algoListener) {
        addListener(algoListener);
        return this;
    }
}
