package com.vanolucas.cppsolver.old.v4.opti.algo;

import com.vanolucas.cppsolver.old.v4.opti.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v4.opti.solution.Solution;

public interface AlgoListener {
    default void onListeningTo(Algo algo) {
    }

    default void onIterationStart(long iteration) {
    }

    default void onIterationEnd(long iteration) {
    }

    default void onRandomized(Solution randomizedSolution, long iteration) {
    }

    default void onEvaluated(EvaluatedSolution evaluatedSolution, long iteration) {
    }

    default void onPause(long finishedIteration) {
    }

    default void onBetterSolution(EvaluatedSolution betterSolution, long iteration) {
    }
}
