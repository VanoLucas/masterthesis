package com.vanolucas.cppsolver.old.v4.opti.algo.hillclimbing;

import com.vanolucas.cppsolver.old.v4.opti.algo.StepListener;
import com.vanolucas.cppsolver.old.v4.opti.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v4.opti.solution.Solution;
import com.vanolucas.cppsolver.old.v4.util.observable.Listeners;

import java.util.concurrent.ExecutionException;

public interface HillClimbingStrategy {
    EvaluatedSolution chooseNeighborOf(Solution currentSolution, long iteration, Listeners<StepListener> listeners) throws ExecutionException, InterruptedException;
}
