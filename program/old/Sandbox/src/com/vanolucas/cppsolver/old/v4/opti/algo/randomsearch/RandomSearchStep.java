package com.vanolucas.cppsolver.old.v4.opti.algo.randomsearch;

import com.vanolucas.cppsolver.old.v4.opti.algo.Step;
import com.vanolucas.cppsolver.old.v4.opti.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v4.opti.evaluation.Evaluator;
import com.vanolucas.cppsolver.old.v4.opti.randomizer.Randomizer;
import com.vanolucas.cppsolver.old.v4.opti.solution.Solution;

public class RandomSearchStep extends Step {
    private Solution solution;
    private Randomizer randomizer;
    private Evaluator evaluator;

    public RandomSearchStep(Solution solution, Randomizer randomizer, Evaluator evaluator) {
        this.solution = solution;
        this.randomizer = randomizer;
        this.evaluator = evaluator;
    }

    @Override
    protected void runStep(long iteration) {
        randomizer.randomize(solution);
        listeners.notifyListeners(listener -> listener.onRandomized(solution, iteration));

        final EvaluatedSolution evaluatedSolution = evaluator.evaluate(solution);
        listeners.notifyListeners(listener -> listener.onEvaluated(evaluatedSolution, iteration));

        proposeSolution(evaluatedSolution);
    }
}
