package com.vanolucas.cppsolver.old.v4.opti.bestkeeper;

import com.vanolucas.cppsolver.old.v4.opti.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v4.util.observable.Listeners;
import com.vanolucas.cppsolver.old.v4.util.observable.Observable;

public abstract class BestKeeper implements Observable<BestKeeperListener> {
    private Listeners<BestKeeperListener> listeners = new Listeners<>(2);

    public abstract void proposeSolution(EvaluatedSolution candidate);

    protected void notifyBetterSolution(EvaluatedSolution betterSolution) {
        listeners.notifyListeners(listener ->
                listener.onBetterSolution(betterSolution)
        );
    }

    @Override
    public void addListener(BestKeeperListener bestKeeperListener) {
        listeners.addListener(bestKeeperListener);
    }

    @Override
    public void removeListener(BestKeeperListener bestKeeperListener) {
        listeners.removeListener(bestKeeperListener);
    }

    @Override
    public void removeAllListeners() {
        listeners.removeAllListeners();
    }
}
