package com.vanolucas.cppsolver.old.v4.opti.bestkeeper;

import com.vanolucas.cppsolver.old.v4.opti.solution.EvaluatedSolution;

public interface BestKeeperListener {
    default void onBetterSolution(EvaluatedSolution betterSolution) {
    }
}
