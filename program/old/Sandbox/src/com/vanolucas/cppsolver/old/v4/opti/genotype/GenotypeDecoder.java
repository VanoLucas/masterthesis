package com.vanolucas.cppsolver.old.v4.opti.genotype;

import com.vanolucas.cppsolver.old.v4.util.converter.Converter;

public interface GenotypeDecoder<TGenotype extends Genotype, TPhenotype> extends Converter<TGenotype, TPhenotype> {
}
