package com.vanolucas.cppsolver.old.v4.opti.genotype;

import com.vanolucas.cppsolver.old.v4.util.math.Scale;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class RandomKeyVector2ListDoubleDecoder implements GenotypeDecoder<RandomKeyVector, List<Double>> {
    private List<Double> targetMin;
    private List<Double> targetMax;

    public RandomKeyVector2ListDoubleDecoder(List<Double> targetMin, List<Double> targetMax) {
        this.targetMin = targetMin;
        this.targetMax = targetMax;
    }

    @Override
    public List<Double> convert(RandomKeyVector genome) {
        return IntStream.range(0, genome.length())
                .mapToObj(i -> Scale.scale(genome.getKey(i), 0d, 1d, targetMin.get(i), targetMax.get(i)))
                .collect(Collectors.toList());
    }
}
