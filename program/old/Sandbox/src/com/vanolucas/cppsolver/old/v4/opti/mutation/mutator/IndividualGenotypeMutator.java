package com.vanolucas.cppsolver.old.v4.opti.mutation.mutator;

import com.vanolucas.cppsolver.old.v4.opti.mutation.Mutation;
import com.vanolucas.cppsolver.old.v4.opti.solution.indiv.Individual;

/**
 * Mutator that only mutates the genotype of an Individual.
 */
public class IndividualGenotypeMutator implements Mutator<Individual> {
    private Mutation mutation;

    public IndividualGenotypeMutator(Mutation mutation) {
        this.mutation = mutation;
    }

    @Override
    public void mutate(Individual solution) {
        solution.mutateGenotypeUsing(mutation);
    }
}
