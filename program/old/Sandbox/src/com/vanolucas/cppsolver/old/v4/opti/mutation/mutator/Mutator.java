package com.vanolucas.cppsolver.old.v4.opti.mutation.mutator;

import com.vanolucas.cppsolver.old.v4.opti.solution.Solution;

/**
 * Mutates optimization solutions.
 */
public interface Mutator<TOptiSolution extends Solution> {
    void mutate(TOptiSolution solution);
}
