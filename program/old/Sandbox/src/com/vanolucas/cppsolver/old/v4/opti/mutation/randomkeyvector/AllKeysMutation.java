package com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector;

import com.vanolucas.cppsolver.old.v4.opti.genotype.RandomKeyVector;
import com.vanolucas.cppsolver.old.v4.opti.mutation.Mutation;
import com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector.key.KeyMutation;

public class AllKeysMutation implements Mutation<RandomKeyVector> {
    private KeyMutation keyMutation;

    public AllKeysMutation(KeyMutation keyMutation) {
        this.keyMutation = keyMutation;
    }

    @Override
    public void mutate(RandomKeyVector randomKeyVector) {
        final int vectorLength = randomKeyVector.length();
        for (int keyIndex = 0; keyIndex < vectorLength; keyIndex++) {
            keyMutation.mutateKey(keyIndex, randomKeyVector);
        }
    }
}
