package com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector;

import com.vanolucas.cppsolver.old.v4.opti.genotype.RandomKeyVector;
import com.vanolucas.cppsolver.old.v4.opti.mutation.Mutation;
import com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector.key.KeyMutation;
import com.vanolucas.cppsolver.old.v4.util.rand.Rand;

public class RandomConsecutiveKeysMutation implements Mutation<RandomKeyVector> {
    private int nbConsecutiveKeys;
    private KeyMutation keyMutation;
    private Rand rand;

    public RandomConsecutiveKeysMutation(int nbConsecutiveKeys, KeyMutation keyMutation) {
        this(nbConsecutiveKeys, keyMutation, null);
    }

    public RandomConsecutiveKeysMutation(int nbConsecutiveKeys, KeyMutation keyMutation, Rand rand) {
        this.nbConsecutiveKeys = nbConsecutiveKeys;
        this.keyMutation = keyMutation;
        this.rand = rand;
    }

    @Override
    public void mutate(RandomKeyVector randomKeyVector) {
        if (rand == null) {
            rand = new Rand();
        }
        final int firstKey = rand.int0To(randomKeyVector.length() - nbConsecutiveKeys);
        for (int i = 0; i < nbConsecutiveKeys; i++) {
            keyMutation.mutateKey(firstKey + i, randomKeyVector);
        }
    }
}
