package com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector.key;

import com.vanolucas.cppsolver.old.v4.opti.genotype.RandomKeyVector;
import com.vanolucas.cppsolver.old.v4.util.rand.Rand;

public class RandomizeKeyMutation extends KeyMutation {
    private Rand rand;

    public RandomizeKeyMutation() {
        this(null);
    }

    public RandomizeKeyMutation(Rand rand) {
        this(null, rand);
    }

    public RandomizeKeyMutation(Integer keyIndex, Rand rand) {
        super(keyIndex);
        this.rand = rand;
    }

    @Override
    public void mutateKey(int keyIndex, RandomKeyVector randomKeyVector) {
        if (rand == null) {
            rand = new Rand();
        }
        randomKeyVector.randomizeKey(keyIndex, rand);
    }
}
