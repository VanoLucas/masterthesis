package com.vanolucas.cppsolver.old.v4.opti.problem;

import com.vanolucas.cppsolver.old.v4.opti.algo.Algo;
import com.vanolucas.cppsolver.old.v4.opti.algo.hillclimbing.HillClimbingStep;
import com.vanolucas.cppsolver.old.v4.opti.algo.hillclimbing.HillClimbingStrategy;
import com.vanolucas.cppsolver.old.v4.opti.algo.hillclimbing.ParallelSteepestAscentHillClimbingStrategy;
import com.vanolucas.cppsolver.old.v4.opti.algo.hillclimbing.SteepestAscentHillClimbingStrategy;
import com.vanolucas.cppsolver.old.v4.opti.algo.randomsearch.RandomSearchStep;
import com.vanolucas.cppsolver.old.v4.opti.bestkeeper.BestKeeper;
import com.vanolucas.cppsolver.old.v4.opti.bestkeeper.SingleBestQualityKeeper;
import com.vanolucas.cppsolver.old.v4.opti.cloner.Cloner;
import com.vanolucas.cppsolver.old.v4.opti.evaluation.Evaluator;
import com.vanolucas.cppsolver.old.v4.opti.mutation.mutator.Mutator;
import com.vanolucas.cppsolver.old.v4.opti.quality.Quality;
import com.vanolucas.cppsolver.old.v4.opti.randomizer.Randomizer;
import com.vanolucas.cppsolver.old.v4.opti.solution.Solution;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public abstract class Problem<TOptiSolution extends Solution, TQuality extends Quality> {
    public abstract TOptiSolution newSolution();

    public abstract Evaluator<TOptiSolution, TQuality> newEvaluator();

    public abstract Randomizer<TOptiSolution> newRandomizer();

    public abstract Cloner<TOptiSolution> newCloner();

    public BestKeeper newBestKeeper() {
        return new SingleBestQualityKeeper();
    }

    /*
    Random Search
     */

    public RandomSearchStep newRandomSearchStep() {
        return new RandomSearchStep(newSolution(), newRandomizer(), newEvaluator());
    }

    public Algo newRandomSearchAlgo() {
        return newRandomSearchAlgo(null);
    }

    public Algo newRandomSearchAlgo(BestKeeper bestKeeper) {
        return new Algo(
                newRandomSearchStep(),
                bestKeeper != null ? bestKeeper : newBestKeeper()
        );
    }

    /*
    Hill Climbing
     */

    public SteepestAscentHillClimbingStrategy newSteepestAscentHillClimbingStrategy(List<Mutator> mutators) {
        return new SteepestAscentHillClimbingStrategy(newCloner(), mutators, newEvaluator());
    }

    public ParallelSteepestAscentHillClimbingStrategy newParallelSteepestAscentHillClimbingStrategy(List<Mutator> mutators) {
        return newParallelSteepestAscentHillClimbingStrategy(null, mutators);
    }

    public ParallelSteepestAscentHillClimbingStrategy newParallelSteepestAscentHillClimbingStrategy(Integer nbThreads, List<Mutator> mutators) {
        nbThreads = nbThreads != null ? nbThreads : Runtime.getRuntime().availableProcessors();
        final List<Cloner> cloners = IntStream.range(0, nbThreads)
                .mapToObj(i -> newCloner())
                .collect(Collectors.toList());
        final List<Evaluator> evaluators = IntStream.range(0, nbThreads)
                .mapToObj(i -> newEvaluator())
                .collect(Collectors.toList());
        return new ParallelSteepestAscentHillClimbingStrategy(cloners, mutators, evaluators);
    }

    public HillClimbingStep newHillClimbingStep(List<Mutator> mutators) {
        return newHillClimbingStep(
                newParallelSteepestAscentHillClimbingStrategy(mutators)
        );
    }

    public HillClimbingStep newHillClimbingStep(HillClimbingStrategy strategy) {
        return newHillClimbingStep(strategy, null);
    }

    public HillClimbingStep newHillClimbingStep(HillClimbingStrategy strategy, Solution initialSolution) {
        return new HillClimbingStep(
                strategy,
                initialSolution != null ? initialSolution : newSolution()
        );
    }

    public Algo newHillClimbingAlgo(List<Mutator> mutators) {
        return newHillClimbingAlgo(mutators, null);
    }

    public Algo newHillClimbingAlgo(List<Mutator> mutators, BestKeeper bestKeeper) {
        return new Algo(
                newHillClimbingStep(mutators),
                bestKeeper != null ? bestKeeper : newBestKeeper()
        );
    }

    public Algo newHillClimbingAlgo(HillClimbingStep step) {
        return newHillClimbingAlgo(step, null);
    }

    public Algo newHillClimbingAlgo(HillClimbingStep step, BestKeeper bestKeeper) {
        return new Algo(
                step,
                bestKeeper != null ? bestKeeper : newBestKeeper()
        );
    }
}
