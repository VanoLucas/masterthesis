package com.vanolucas.cppsolver.old.v4.opti.randomizer;

import com.vanolucas.cppsolver.old.v4.opti.solution.Solution;
import com.vanolucas.cppsolver.old.v4.util.rand.Rand;

public abstract class Randomizer<TOptiSolution extends Solution> {
    private Rand rand;

    public void randomize(TOptiSolution solution) {
        if (rand == null) {
            rand = new Rand();
        }
        randomize(solution, rand);
    }

    public abstract void randomize(TOptiSolution solution, Rand rand);
}
