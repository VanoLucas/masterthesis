package com.vanolucas.cppsolver.old.v4.test;

import com.vanolucas.cppsolver.old.v4.cpp.instance.CppInstance30BookCovers;
import com.vanolucas.cppsolver.old.v4.cpp.problem.CppGeneticProblemCP;
import com.vanolucas.cppsolver.old.v4.opti.algo.Algo;
import com.vanolucas.cppsolver.old.v4.opti.algo.AlgoListener;
import com.vanolucas.cppsolver.old.v4.opti.mutation.RandomizeRandomizableMutation;
import com.vanolucas.cppsolver.old.v4.opti.mutation.mutator.IndividualGenotypeMutator;
import com.vanolucas.cppsolver.old.v4.opti.mutation.mutator.MultipleMutator;
import com.vanolucas.cppsolver.old.v4.opti.mutation.mutator.Mutator;
import com.vanolucas.cppsolver.old.v4.opti.mutation.mutator.RepeatedMutator;
import com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector.AllKeysMutation;
import com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector.OneRandomKeyMutation;
import com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector.RandomConsecutiveKeysMutation;
import com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector.key.AddRandomValueKeyMutation;
import com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector.key.RandomizeKeyMutation;
import com.vanolucas.cppsolver.old.v4.opti.problem.Problem;
import com.vanolucas.cppsolver.old.v4.opti.quality.IntCost;
import com.vanolucas.cppsolver.old.v4.opti.quality.Quality;
import com.vanolucas.cppsolver.old.v4.opti.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v4.util.rand.Rand;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class Test {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
//        new Test().cpp30();
        new Test().hyperparamOpti();
    }

    private void hyperparamOpti() throws ExecutionException, InterruptedException {
        /*
        Interesting mutations for the RandomKeyVector:
        - IndividualGenotypeMutator
            - RandomizeRandomizableMutation -> randomize the whole genome.
            - AllKeysMutation
                - AddValueKeyMutation(value) -> add a fixed value to all keys
                - AddRandomValueKeyMutation(max) -> add a random value within 0d..max to all keys.
            - OneRandomKeyMutation
                - RandomizeKeyMutation -> randomize one random key (chainable)
                - AddValueKeyMutation(value) -> add fixed value to one random key (chainable)
                - AddRandomValueKeyMutation(max) -> add a random value within 0d..max to one random key (chainable).
            - RandomConsecutiveKeysMutation(count)
                - RandomizeKeyMutation -> randomize n consecutive random keys (chainable)
                - AddValueKeyMutation(value) -> add fixed value to n consecutive random keys (chainable)
                - AddRandomValueKeyMutation(max) -> add a random value within 0d..max to n consecutive random keys (chainable).
            - Single key mutation
                - RandomizeKeyMutation(keyIndex) -> randomize one fixed key
                - AddValueKeyMutation(keyIndex, value) -> add fixed value to one fixed key (chainable)
                - AddRandomValueKeyMutation(keyIndex, max) -> add a random value within 0d..max to one fixed key (chainable).

        Mutators to have:
        - n RandomizeRandomizableMutation -> full rand
        - n AllKeysMutation.AddRandomValueKeyMutation(max=0.01) -> add rand value to all keys
        - n AllKeysMutation.AddRandomValueKeyMutation(max=0.001) -> add rand value to all keys
        - n AllKeysMutation.AddRandomValueKeyMutation(max=0.0001) -> add rand value to all keys
        - n OneRandomKeyMutation.RandomizeKeyMutation -> randomize 1 random key
        - n OneRandomKeyMutation.AddRandomValueKeyMutation(min/max=+-0.01) -> add rand value to 1 random key
        - n OneRandomKeyMutation.AddRandomValueKeyMutation(min/max=+-0.001) -> add rand value to 1 random key
        - n OneRandomKeyMutation.AddRandomValueKeyMutation(min/max=+-0.0001) -> add rand value to 1 random key
        - n RandomConsecutiveKeysMutation(2).RandomizeKeyMutation -> randomize 2 random consecutive keys
        - n RandomConsecutiveKeysMutation(2).AddRandomValueKeyMutation(max=+-0.01) -> add rand value to 2 random consecutive keys
        - n RandomConsecutiveKeysMutation(2).AddRandomValueKeyMutation(max=+-0.001) -> add rand value to 2 random consecutive keys
        - n RandomConsecutiveKeysMutation(2).AddRandomValueKeyMutation(max=+-0.0001) -> add rand value to 2 random consecutive keys
        - n RepeatedMutator(2).OneRandomKeyMutation.AddRandomValueKeyMutation(min/max=+-0.01) -> add rand value to two random keys
        - n RepeatedMutator(2).OneRandomKeyMutation.AddRandomValueKeyMutation(min/max=+-0.001) -> add rand value to two random keys
        - n RepeatedMutator(2).OneRandomKeyMutation.AddRandomValueKeyMutation(min/max=+-0.0001) -> add rand value to two random keys
         */

        // number of runs to evaluate each config
        final int nbRunsPerEval = 10;
        final int maxEvaluationsPerRun = 50_000;

        final List<Integer> mutatorCount = Arrays.asList(2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2);

        List<Integer> bestCost = new ArrayList<>(nbRunsPerEval);
        for (int runId = 0; runId < nbRunsPerEval; runId++) {
            bestCost.add(
                    run1Algo(maxEvaluationsPerRun, mutatorCount)
            );
        }

        Collections.sort(bestCost);
        System.out.println("Best costs: " + bestCost);
        System.out.println("Median best cost: " + bestCost.get(bestCost.size() / 2));
        System.out.println("Average best cost: " + bestCost.stream().mapToDouble(__ -> __).average().getAsDouble());
    }

    // run eval of the provided config
    private int run1Algo(int maxEvaluations, List<Integer> mutatorCount) throws ExecutionException, InterruptedException {
        List<Mutator> mutators = new ArrayList<>();
        int mutatorIndex = 0;

        // full randomization of the genome
        mutators.addAll(Collections.nCopies(mutatorCount.get(mutatorIndex++),
                new IndividualGenotypeMutator(new RandomizeRandomizableMutation(new Rand()))
        ));
        // add rand value to all keys
        mutators.addAll(Collections.nCopies(mutatorCount.get(mutatorIndex++),
                new IndividualGenotypeMutator(new AllKeysMutation(new AddRandomValueKeyMutation(-0.01d, +0.01d, new Rand())))
        ));
        mutators.addAll(Collections.nCopies(mutatorCount.get(mutatorIndex++),
                new IndividualGenotypeMutator(new AllKeysMutation(new AddRandomValueKeyMutation(-0.001d, +0.001d, new Rand())))
        ));
        mutators.addAll(Collections.nCopies(mutatorCount.get(mutatorIndex++),
                new IndividualGenotypeMutator(new AllKeysMutation(new AddRandomValueKeyMutation(-0.0001d, +0.0001d, new Rand())))
        ));
        // randomize 1 random key
        mutators.addAll(Collections.nCopies(mutatorCount.get(mutatorIndex++),
                new IndividualGenotypeMutator(new OneRandomKeyMutation(new RandomizeKeyMutation(new Rand())))
        ));
        // add rand value to 1 random key
        mutators.addAll(Collections.nCopies(mutatorCount.get(mutatorIndex++),
                new IndividualGenotypeMutator(new OneRandomKeyMutation(new AddRandomValueKeyMutation(-0.01d, +0.01d, new Rand())))
        ));
        mutators.addAll(Collections.nCopies(mutatorCount.get(mutatorIndex++),
                new IndividualGenotypeMutator(new OneRandomKeyMutation(new AddRandomValueKeyMutation(-0.001d, +0.001d, new Rand())))
        ));
        mutators.addAll(Collections.nCopies(mutatorCount.get(mutatorIndex++),
                new IndividualGenotypeMutator(new OneRandomKeyMutation(new AddRandomValueKeyMutation(-0.0001d, +0.0001d, new Rand())))
        ));
        // randomize 2 random consecutive keys
        mutators.addAll(Collections.nCopies(mutatorCount.get(mutatorIndex++),
                new IndividualGenotypeMutator(new RandomConsecutiveKeysMutation(2, new RandomizeKeyMutation(new Rand()), new Rand()))
        ));
        // add rand value to 2 random consecutive keys
        mutators.addAll(Collections.nCopies(mutatorCount.get(mutatorIndex++),
                new IndividualGenotypeMutator(new RandomConsecutiveKeysMutation(2, new AddRandomValueKeyMutation(-0.01d, +0.01d, new Rand())))
        ));
        mutators.addAll(Collections.nCopies(mutatorCount.get(mutatorIndex++),
                new IndividualGenotypeMutator(new RandomConsecutiveKeysMutation(2, new AddRandomValueKeyMutation(-0.001d, +0.001d, new Rand())))
        ));
        mutators.addAll(Collections.nCopies(mutatorCount.get(mutatorIndex++),
                new IndividualGenotypeMutator(new RandomConsecutiveKeysMutation(2, new AddRandomValueKeyMutation(-0.0001d, +0.0001d, new Rand())))
        ));
        // add rand value to 2 random keys
        mutators.addAll(Collections.nCopies(mutatorCount.get(mutatorIndex++),
                new RepeatedMutator<>(2,
                        new IndividualGenotypeMutator(new OneRandomKeyMutation(new AddRandomValueKeyMutation(-0.01d, +0.01d, new Rand())))
                )
        ));
        mutators.addAll(Collections.nCopies(mutatorCount.get(mutatorIndex++),
                new RepeatedMutator<>(2,
                        new IndividualGenotypeMutator(new OneRandomKeyMutation(new AddRandomValueKeyMutation(-0.001d, +0.001d, new Rand())))
                )
        ));
        mutators.addAll(Collections.nCopies(mutatorCount.get(mutatorIndex++),
                new RepeatedMutator<>(2,
                        new IndividualGenotypeMutator(new OneRandomKeyMutation(new AddRandomValueKeyMutation(-0.0001d, +0.0001d, new Rand())))
                )
        ));

        Problem problem = new CppGeneticProblemCP(new CppInstance30BookCovers(10, 4));

        Algo algo = problem.newHillClimbingAlgo(mutators);

        class AlgoHandler implements AlgoListener {
            private int countEval = 0;
            private Quality bestQuality = null;

            private Quality getBestQuality() {
                return bestQuality;
            }

            @Override
            public synchronized void onEvaluated(EvaluatedSolution evaluatedSolution, long iteration) {
                countEval++;
                if (countEval >= maxEvaluations - mutators.size()) {
                    algo.pauseBeforeNextIteration();
                }
            }

            @Override
            public synchronized void onBetterSolution(EvaluatedSolution betterSolution, long iteration) {
                if (bestQuality == null || betterSolution.isBetterThan(bestQuality)) {
                    bestQuality = betterSolution.getQuality();
                }
            }
        }

        AlgoHandler algoHandler = new AlgoHandler();
        algo.addListener(algoHandler);

        algo.run();

        return ((IntCost) algoHandler.getBestQuality()).getValue();
    }


    private void cpp30() throws ExecutionException, InterruptedException {
        final List<Mutator> mutators = new ArrayList<>();
        // randomize whole genome
        mutators.addAll(Collections.nCopies(40,
                new IndividualGenotypeMutator(new RandomizeRandomizableMutation())
        ));
        // randomize 1 random key of the genome
        mutators.addAll(Collections.nCopies(500,
                new IndividualGenotypeMutator(
                        new OneRandomKeyMutation(new RandomizeKeyMutation(new Rand()))
                )
        ));
        // randomize 1 random key then 1 again
        mutators.addAll(Collections.nCopies(500,
                new MultipleMutator<>(Arrays.asList(
                        new IndividualGenotypeMutator(
                                new OneRandomKeyMutation(new RandomizeKeyMutation(new Rand()))
                        ),
                        new IndividualGenotypeMutator(
                                new OneRandomKeyMutation(new RandomizeKeyMutation(new Rand()))
                        )
                ))
        ));
        // randomize 2 random consecutive keys
        mutators.addAll(Collections.nCopies(500,
                new IndividualGenotypeMutator(new RandomConsecutiveKeysMutation(2,
                        new RandomizeKeyMutation(new Rand())
                ))
        ));
//        // randomize 2 random consecutive keys then 2 again
//        mutators.addAll(Collections.nCopies(500,
//                new MultipleMutator<>(Arrays.asList(
//                        new IndividualGenotypeMutator(new RandomConsecutiveKeysMutation(2,
//                                new RandomizeKeyMutation(new Rand())
//                        )),
//                        new IndividualGenotypeMutator(new RandomConsecutiveKeysMutation(2,
//                                new RandomizeKeyMutation(new Rand())
//                        ))
//                ))
//        ));

        final Instant timeStart = Instant.now();

        new CppGeneticProblemCP(new CppInstance30BookCovers())
                .newHillClimbingAlgo(mutators)
//                .newRandomSearchAlgo()
                .withListener(new AlgoListener() {
                    private Algo algo;
                    private long evaluations = 0L;

                    @Override
                    public void onListeningTo(Algo algo) {
                        this.algo = algo;
                    }

//                    @Override
//                    public void onEvaluated(EvaluatedSolution evaluatedSolution, long iteration) {
//                        evaluations++;
//                        if (evaluations >= 3_000_000L) {
//                            algo.pauseBeforeNextIteration();
//                        }
//                    }

                    @Override
                    public void onBetterSolution(EvaluatedSolution betterSolution, long iteration) {
                        // log best solutions
                        System.out.println(String.format("\n[%s]\n" +
                                        "New better solution at iteration %d:\n" +
                                        "%s",
                                Instant.now(),
                                iteration,
                                betterSolution.getSolution()
                        ));
                        // stop at target quality
                        if (((IntCost) betterSolution.getQuality()).getValue() <= 115000) {
                            algo.pauseBeforeNextIteration();
                        }
                    }
                })
                .run();

        System.out.println("Run duration: " +
                Duration.between(timeStart, Instant.now())
        );
        // Run duration: PT21.489S, PT21.243S, PT22.231S
        // after OffsetPlatePrintings::maxNbPrintings optimization: PT15.964S
    }
}
