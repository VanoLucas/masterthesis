package com.vanolucas.cppsolver.old.v4.util.converter;

public interface Converter<TSrc, TDst> {
    TDst convert(TSrc obj);
}
