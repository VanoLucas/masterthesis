package com.vanolucas.cppsolver.old.v4.util.observable;

public interface Observable<TListener> {
    void addListener(TListener listener);

    void removeListener(TListener listener);

    void removeAllListeners();
}
