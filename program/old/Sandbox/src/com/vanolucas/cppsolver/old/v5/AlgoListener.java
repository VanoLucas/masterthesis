package com.vanolucas.cppsolver.old.v5;

public interface AlgoListener {
    default void onListenerAdded(AlgoListener listener, Algo algo) {
    }

    default void onBetterSolutionFound(EvaluatedSolution solution, long iteration) {
    }

    default void onEquivalentOrBetterSolutionFound(EvaluatedSolution solution, long iteration) {
    }

    default void onEquivalentSolutionFound(EvaluatedSolution solution, long iteration) {
    }
}
