package com.vanolucas.cppsolver.old.v5;

public interface EliteDeciderListener {
    default void onListenerAdded(EliteDeciderListener listener, EliteDecider eliteDecider) {
    }

    default void onBetterSolution(EvaluatedSolution solution) {
    }

    default void onEquivalentSolution(EvaluatedSolution solution) {
    }
}
