package com.vanolucas.cppsolver.old.v5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Component that takes solutions, evaluates them and returns the corresponding evaluated solutions.
 */
public interface Evaluator {
    default List<EvaluatedSolution> evaluate(List<Solution> solutions) {
        if (solutions == null) {
            throw new IllegalArgumentException(String.format(
                    "The list of solutions provided to %s for evaluation can't be null.",
                    getClass().getSimpleName()
            ));
        }

        if (solutions.isEmpty()) {
            return new ArrayList<>(0);
        } else {
            return Collections.singletonList(evaluate(solutions.get(0)));
        }
    }

    default EvaluatedSolution evaluate(Solution solution) {
        if (solution == null) {
            throw new IllegalArgumentException(String.format(
                    "The solution provided to %s for evaluation can't be null.",
                    getClass().getSimpleName()
            ));
        }

        return evaluate(Collections.singletonList(solution)).get(0);
    }
}
