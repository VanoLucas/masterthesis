package com.vanolucas.cppsolver.old.v5;

public interface OptiStepListener {
    default void onListenerAdded(OptiStepListener listener, OptiStep optiStep) {
    }

    default void onProposedSolution(EvaluatedSolution candidate) {
    }
}
