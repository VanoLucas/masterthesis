package com.vanolucas.cppsolver.old.v7;

import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Executor (pool of threads) that can run optimization algorithm tasks.
 */
public class AlgoExecutor {
    public static class Params {
        private final int DEFAULT_MAX_THREADS = Runtime.getRuntime().availableProcessors();
        private final long DEFAULT_STOP_AFTER_IDLE_SECONDS = 10L;

        public int maxThreads = DEFAULT_MAX_THREADS;
        public long stopAfterIdleSeconds = DEFAULT_STOP_AFTER_IDLE_SECONDS;

        public Params withMaxThreads(int maxThreads) {
            this.maxThreads = maxThreads;
            return this;
        }

        public Params withStopAfterIdleSeconds(long stopAfterIdleSeconds) {
            this.stopAfterIdleSeconds = stopAfterIdleSeconds;
            return this;
        }
    }

    private ThreadPoolExecutor executor;

    public AlgoExecutor(Params params) {
        this.executor = new ThreadPoolExecutor(
                0, // min threads
                params.maxThreads,
                params.stopAfterIdleSeconds, TimeUnit.SECONDS, // stop idle thread after this period
                new SynchronousQueue<>()
        );
    }
}
