package com.vanolucas.cppsolver.old.v8.algo;

import com.vanolucas.cppsolver.old.v8.solution.EvaluatedSolution;

public interface AlgoListener {
    default void onListenerAdded(AlgoListener listener, Algo algo) {
    }

    default void onNewIteration(long iteration) {
    }

    default void onEvaluated(EvaluatedSolution evaluatedSolution, long iteration) {
    }

    default void onBetterSolutionFound(EvaluatedSolution solution, long iteration) {
    }

    default void onEquivalentOrBetterSolutionFound(EvaluatedSolution solution, long iteration) {
    }

    default void onEquivalentSolutionFound(EvaluatedSolution solution, long iteration) {
    }
}
