package com.vanolucas.cppsolver.old.v8.algo;

import com.vanolucas.cppsolver.old.v8.judge.Judge;
import com.vanolucas.cppsolver.old.v8.mutation.Mutator;
import com.vanolucas.cppsolver.old.v8.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v8.solution.Solution;
import com.vanolucas.cppsolver.old.v8.solution.SolutionCloner;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class HillClimbingStrategy<TPhenotype> extends Strategy {
    private Solution<TPhenotype> currentSolution;
    private SolutionCloner<TPhenotype> cloner;
    private List<Mutator<TPhenotype>> mutators;
    private boolean finishedIteration;

    public HillClimbingStrategy(Solution<TPhenotype> initialSolution, SolutionCloner<TPhenotype> cloner, List<Mutator<TPhenotype>> mutators) {
        this.currentSolution = initialSolution;
        this.cloner = cloner;
        this.mutators = mutators;
    }

    @Override
    void onNewIteration(long iteration) {
        super.onNewIteration(iteration);
        this.finishedIteration = false;
    }

    @Override
    List<Solution> nextSolutionsToEvaluate() {
        if (finishedIteration) {
            return null;
        } else {
            // each mutator defines a neighbor
            return mutators.stream()
                    .map(mutator -> {
                        // create the neighbor by cloning the current solution
                        Solution<TPhenotype> neighbor = cloner.clone(currentSolution);
                        // mutate the neighbor
                        mutator.mutate(neighbor);
                        return neighbor;
                    })
                    // collect all neighbors for evaluation
                    .collect(Collectors.toList());
        }
    }

    @Override
    List<EvaluatedSolution> nextProposedSolutions() {
        if (finishedIteration) {
            return null;
        } else {
            // get the best neighbor
            final EvaluatedSolution bestNeighbor = Collections.max(lastEvaluatedSolutions);
            // move to it
            currentSolution = bestNeighbor.getSolution();
            // propose it
            return Collections.singletonList(bestNeighbor);
        }
    }

    @Override
    void onVerdicts(List<Judge.Verdict> verdicts) {
        this.finishedIteration = true;
    }
}
