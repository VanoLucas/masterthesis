package com.vanolucas.cppsolver.old.v8.algo;

import com.vanolucas.cppsolver.old.v8.judge.Judge;
import com.vanolucas.cppsolver.old.v8.mutation.Mutator;
import com.vanolucas.cppsolver.old.v8.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v8.solution.Solution;

import java.util.Collections;
import java.util.List;

public class SingleMutationStrategy<TPhenotype> extends Strategy {
    private Solution<TPhenotype> solution;
    private Mutator<TPhenotype> mutator;
    private boolean finishedIteration;

    public SingleMutationStrategy(Solution<TPhenotype> initialSolution, Mutator<TPhenotype> mutator) {
        this.solution = initialSolution;
        this.mutator = mutator;
    }

    @Override
    void onNewIteration(long iteration) {
        super.onNewIteration(iteration);
        this.finishedIteration = false;
    }

    @Override
    List<Solution> nextSolutionsToEvaluate() {
        if (!finishedIteration) {
            mutator.mutate(solution);
            return Collections.singletonList(solution);
        } else {
            return null;
        }
    }

    @Override
    List<EvaluatedSolution> nextProposedSolutions() {
        return !finishedIteration ? lastEvaluatedSolutions : null;
    }

    @Override
    void onVerdicts(List<Judge.Verdict> verdicts) {
        this.finishedIteration = true;
    }
}
