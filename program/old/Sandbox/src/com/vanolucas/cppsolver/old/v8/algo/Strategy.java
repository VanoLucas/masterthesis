package com.vanolucas.cppsolver.old.v8.algo;

import com.vanolucas.cppsolver.old.v8.judge.Judge;
import com.vanolucas.cppsolver.old.v8.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v8.solution.Solution;

import java.util.List;

/**
 * An optimization strategy.
 * E.g. a metaheuristic optimization step.
 */
public abstract class Strategy {
    protected long iteration = 0L;
    List<EvaluatedSolution> lastEvaluatedSolutions;

    /**
     * Called when the optimization algorithm starts a new iteration.
     *
     * @param iteration The optimization algo's current iteration number.
     */
    void onNewIteration(long iteration) {
        this.iteration = iteration;
    }

    /**
     * @return The next solutions that this Strategy wants to evaluate.
     */
    abstract List<Solution> nextSolutionsToEvaluate();

    /**
     * Called when we receive our evaluated solutions.
     */
    void onEvaluated(List<EvaluatedSolution> evaluatedSolutions) {
        this.lastEvaluatedSolutions = evaluatedSolutions;
    }

    /**
     * @return The next solutions that this Strategy proposes to compare to the best.
     */
    abstract List<EvaluatedSolution> nextProposedSolutions();

    /**
     * @param verdicts Verdicts respectively for each proposed solution.
     */
    abstract void onVerdicts(List<Judge.Verdict> verdicts);
}
