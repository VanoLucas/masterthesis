package com.vanolucas.cppsolver.old.v8.eval;

import com.vanolucas.cppsolver.old.v8.quality.Quality;
import com.vanolucas.cppsolver.old.v8.solution.GeneticSolution1Intermediate;
import com.vanolucas.cppsolver.old.v8.solution.Solution;
import com.vanolucas.cppsolver.old.v8.util.Converter;

public class GeneticEvaluator1Intermediate<TGenotype, TIntermediate, TPhenotype> extends Evaluator<TPhenotype> {
    private GenomeDecoder<TGenotype, TIntermediate> genomeDecoder;
    private Converter<TIntermediate, TPhenotype> intermediateDecoder;

    public GeneticEvaluator1Intermediate(GenomeDecoder<TGenotype, TIntermediate> genomeDecoder,
                                         Converter<TIntermediate, TPhenotype> intermediateDecoder,
                                         EvaluationFunction<TPhenotype> phenotypeEvaluationFunction) {
        super(phenotypeEvaluationFunction);
        this.genomeDecoder = genomeDecoder;
        this.intermediateDecoder = intermediateDecoder;
    }

    @Override
    public Quality getQuality(Solution<TPhenotype> solution) {
        return getQuality((GeneticSolution1Intermediate<TGenotype, TIntermediate, TPhenotype>) solution);
    }

    private Quality getQuality(GeneticSolution1Intermediate<TGenotype, TIntermediate, TPhenotype> solution) {
        // first, decode the genome to the corresponding intermediate representation
        solution.decodeGenotype2IntermediateUsing(genomeDecoder);
        // then, convert the intermediate representation to the corresponding phenotype
        solution.convertIntermediate2PhenotypeUsing(intermediateDecoder);
        // finally, evaluate the phenotype
        return super.getQuality(solution);
    }
}
