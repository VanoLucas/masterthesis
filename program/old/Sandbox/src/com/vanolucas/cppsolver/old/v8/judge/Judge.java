package com.vanolucas.cppsolver.old.v8.judge;

import com.vanolucas.cppsolver.old.v8.solution.EvaluatedSolution;

public interface Judge {
    enum Verdict {
        BETTER,
        EQUIVALENT,
        WORSE
    }

    Verdict submit(EvaluatedSolution solution);
}
