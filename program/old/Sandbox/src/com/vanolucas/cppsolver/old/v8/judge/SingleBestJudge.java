package com.vanolucas.cppsolver.old.v8.judge;

import com.vanolucas.cppsolver.old.v8.quality.Quality;
import com.vanolucas.cppsolver.old.v8.solution.EvaluatedSolution;

public class SingleBestJudge implements Judge {
    private Quality best = null;

    @Override
    public Verdict submit(EvaluatedSolution solution) {
        if (best == null || solution.isBetterThan(best)) {
            best = solution.getQuality();
            return Verdict.BETTER;
        } else if (solution.isEquivalentTo(best)) {
            return Verdict.EQUIVALENT;
        } else {
            return Verdict.WORSE;
        }
    }
}
