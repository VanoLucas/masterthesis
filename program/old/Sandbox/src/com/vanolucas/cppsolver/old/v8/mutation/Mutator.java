package com.vanolucas.cppsolver.old.v8.mutation;

import com.vanolucas.cppsolver.old.v8.solution.Solution;

public class Mutator<TPhenotype> {
    private Mutation<TPhenotype> phenotypeMutationOperator;

    public Mutator() {
        this(null);
    }

    public Mutator(Mutation<TPhenotype> phenotypeMutationOperator) {
        this.phenotypeMutationOperator = phenotypeMutationOperator;
    }

    public void mutate(Solution<TPhenotype> solution) {
        if (phenotypeMutationOperator != null) {
            solution.mutatePhenotypeUsing(phenotypeMutationOperator);
        }
    }
}
