package com.vanolucas.cppsolver.old.v8.solution;

import com.vanolucas.cppsolver.old.v8.quality.Quality;

/**
 * A solution and its quality.
 */
public class EvaluatedSolution implements Comparable<EvaluatedSolution> {
    private Solution solution;
    private Quality quality;

    public EvaluatedSolution(Solution solution, Quality quality) {
        this.solution = solution;
        this.quality = quality;
    }

    public Solution getSolution() {
        return solution;
    }

    public Quality getQuality() {
        return quality;
    }

    public boolean isBetterThan(Quality quality) {
        return this.quality.isBetterThan(quality);
    }

    public boolean isEquivalentTo(Quality quality) {
        return this.quality.isEquivalentTo(quality);
    }

    @Override
    public int compareTo(EvaluatedSolution other) {
        return quality.compareTo(other.quality);
    }

    @Override
    public String toString() {
        return String.format("%s%nQuality: %s",
                solution, quality
        );
    }
}
