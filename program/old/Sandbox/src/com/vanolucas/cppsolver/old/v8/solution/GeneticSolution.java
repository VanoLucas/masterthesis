package com.vanolucas.cppsolver.old.v8.solution;

import com.vanolucas.cppsolver.old.v8.mutation.Mutation;

import java.util.function.Function;

public class GeneticSolution<TGenotype, TPhenotype> extends Solution<TPhenotype> {
    protected TGenotype genotype;

    public GeneticSolution(TGenotype genotype) {
        this(genotype, null);
    }

    public GeneticSolution(TGenotype genotype, TPhenotype phenotype) {
        super(phenotype);
        this.genotype = genotype;
    }

    public TGenotype getGenotype() {
        return genotype;
    }

    public TPhenotype decodeGenotype2PhenotypeUsing(Function<TGenotype, TPhenotype> genomeDecoder) {
        phenotype = genomeDecoder.apply(genotype);
        return phenotype;
    }

    public void mutateGenotypeUsing(Mutation<TGenotype> mutationOperator) {
        mutationOperator.mutate(genotype);
    }

    @Override
    public String toString() {
        return String.format("Genotype:%s%nPhenotype:%s",
                genotype, phenotype
        );
    }
}
