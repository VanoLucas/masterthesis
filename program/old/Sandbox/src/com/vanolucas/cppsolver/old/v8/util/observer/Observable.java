package com.vanolucas.cppsolver.old.v8.util.observer;

public interface Observable<TListener> {
    void addListener(TListener listener);

    void removeListener(TListener listener);

    void removeAllListeners();
}
