package com.vanolucas.cppsolver.old.v9.algo;

import com.vanolucas.cppsolver.old.v9.crossover.Breeder;
import com.vanolucas.cppsolver.old.v9.selection.Merger;
import com.vanolucas.cppsolver.old.v9.selection.Selector;
import com.vanolucas.cppsolver.old.v9.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v9.solution.Solution;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class EvolutionaryAlgoStep extends AlgoStep {
    private List<EvaluatedSolution> population;
    private final Selector selector;
    private final Breeder breeder;
    private final Merger merger;

    public EvolutionaryAlgoStep(List<EvaluatedSolution> initialPopulation, Selector selector, Breeder breeder, Merger merger) {
        this.population = initialPopulation;
        this.selector = selector;
        this.breeder = breeder;
        this.merger = merger;
    }

    @Override
    void runStep(Algo algo) throws ExecutionException, InterruptedException {
        // select individuals for reproduction
        final List<List<EvaluatedSolution>> groups = selector.selectGroups(population);
        // breed to give birth to offspring
        final List<Solution> offspring = groups.stream()
                .flatMap(parents -> breeder.breed(parents).stream())
                .collect(Collectors.toList());
        // evaluate new individuals
        final List<EvaluatedSolution> evaluatedOffspring = algo.evaluate(offspring);
        // update population
        population = merger.merge(population, evaluatedOffspring);
    }
}
