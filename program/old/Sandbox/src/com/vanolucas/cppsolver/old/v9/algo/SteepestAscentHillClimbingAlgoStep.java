package com.vanolucas.cppsolver.old.v9.algo;

import com.vanolucas.cppsolver.old.v9.mutation.Mutator;
import com.vanolucas.cppsolver.old.v9.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v9.solution.Solution;
import com.vanolucas.cppsolver.old.v9.solution.SolutionCloner;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

class SteepestAscentHillClimbingAlgoStep extends AlgoStep {
    private Solution currentSolution;
    private final SolutionCloner cloner;
    private final List<Mutator> mutators;

    SteepestAscentHillClimbingAlgoStep(Solution initialSolution, SolutionCloner cloner, List<Mutator> mutators) {
        this.currentSolution = initialSolution;
        this.cloner = cloner;
        this.mutators = mutators;
    }

    @Override
    void runStep(Algo algo) throws ExecutionException, InterruptedException {
        // create neighbors as clones of the current solution
        final List<Solution> neighbors = mutators.stream()
                .map(m -> cloner.clone(currentSolution))
                .collect(Collectors.toList());
        // mutate each neighbor using the corresponding mutation operator
        for (int n = 0; n < neighbors.size(); n++) {
            final Mutator mutator = mutators.get(n);
            final Solution neighbor = neighbors.get(n);
            mutator.mutate(neighbor);
        }
        // evaluate neighbors
        final List<EvaluatedSolution> evaluatedNeighbors = algo.evaluate(neighbors);
        // get best neighbor
        final EvaluatedSolution bestNeighbor = Collections.max(evaluatedNeighbors);
        // propose best neighbor as new best
        algo.proposeNewBest(bestNeighbor);
        // move to best neighbor as new current solution
        currentSolution = bestNeighbor.getSolution();
    }
}
