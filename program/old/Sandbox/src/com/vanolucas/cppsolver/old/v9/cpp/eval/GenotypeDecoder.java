package com.vanolucas.cppsolver.old.v9.cpp.eval;

import com.vanolucas.cppsolver.old.v9.genome.RandomKeyVector;
import com.vanolucas.cppsolver.old.v9.util.Converter;

public interface GenotypeDecoder extends Converter<RandomKeyVector, Slots> {
    @Override
    default Slots convert(RandomKeyVector genome) {
        return decode(genome);
    }

    Slots decode(RandomKeyVector genome);
}
