package com.vanolucas.cppsolver.old.v9.cpp.eval;

public class Slot implements Comparable<Slot> {
    private final int coverIndex;
    private int nbPrintings;

    Slot(int coverIndex, int nbPrintings) {
        this.coverIndex = coverIndex;
        this.nbPrintings = nbPrintings;
    }

    public int getCoverIndex() {
        return coverIndex;
    }

    public int getNbPrintings() {
        return nbPrintings;
    }

    /**
     * Splits the number of printings for this slot over two slots.
     *
     * @param portionToExtract Portion (0..1) of the nb of printings of the initial slot to move to the newly created slot.
     * @return The new slot with a portion of the printings allocated to it.
     */
    Slot splitFraction(double portionToExtract) {
        final int nbPrintingsToExtract = (int) ((double) nbPrintings * portionToExtract);
        return splitPrintings(nbPrintingsToExtract);
    }

    Slot splitPrintings(int nbPrintingsToExtract) {
        this.nbPrintings -= nbPrintingsToExtract;
        return new Slot(coverIndex, nbPrintingsToExtract);
    }

    @Override
    public int compareTo(Slot other) {
        return Integer.compare(nbPrintings, other.nbPrintings);
    }

    @Override
    public String toString() {
        return String.format("%d:%d",
                coverIndex, nbPrintings
        );
    }
}
