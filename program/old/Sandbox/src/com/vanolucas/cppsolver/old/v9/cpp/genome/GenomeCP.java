package com.vanolucas.cppsolver.old.v9.cpp.genome;

import com.vanolucas.cppsolver.old.v9.cpp.instance.CppInstance;
import com.vanolucas.cppsolver.old.v9.genome.RandomKeyVector;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Sequence of pairs of keys. Each pair defines which book cover and how many printings to allocate to each slot.
 * The first key of each pair points to one of the book covers.
 * The second key of each pair gives the fraction of the demand to allocate to the slot.
 * Each book cover needs to be allocated to at least 1 slot. We therefore only need to describe all other (nbSlots - nbCovers) slots.
 */
public class GenomeCP extends RandomKeyVector {

    public GenomeCP(CppInstance cppInstance) {
        super(lengthForCppInstance(cppInstance));
    }

    public GenomeCP(int nbCovers, int totalNbSlots) {
        super(calculateLength(nbCovers, totalNbSlots));
    }

    public static int lengthForCppInstance(CppInstance cppInstance) {
        return calculateLength(cppInstance.getNbCovers(), cppInstance.getTotalNbSlots());
    }

    public static int calculateLength(int nbCovers, int totalNbSlots) {
        return (totalNbSlots - nbCovers) * 2;
    }

    @Override
    public GenomeCP clone() throws CloneNotSupportedException {
        return (GenomeCP) super.clone();
    }

    @Override
    public String toString() {
        final String strGenes = IntStream.iterate(0, i -> i + 2)
                .limit(length() / 2)
                .mapToObj(i -> String.format("%s %s",
                        getKey(i), getKey(i + 1)))
                .collect(Collectors.joining(" | "));
        return String.format("(%d genes) %s",
                length(),
                strGenes
        );
    }
}
