package com.vanolucas.cppsolver.old.v9.cpp.instance;

import java.util.Arrays;
import java.util.List;

public class CppInstance2 extends CppInstance {
    private static final List<Integer> COVERS = Arrays.asList(
            10,
            5
    );

    private static final int DEFAULT_NB_PLATES = 1;

    public CppInstance2() {
        this(DEFAULT_NB_PLATES);
    }

    public CppInstance2(int nbPlates) {
        super(COVERS, nbPlates);
    }

    public CppInstance2(int nbPlates, int nbSlotsPerPlate) {
        super(COVERS, nbPlates, nbSlotsPerPlate);
    }

    public CppInstance2(int nbPlates, int nbSlotsPerPlate, double costPlate, double costPrinting) {
        super(COVERS, nbPlates, nbSlotsPerPlate, costPlate, costPrinting);
    }
}
