package com.vanolucas.cppsolver.old.v9.cpp.solution;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CppSolution {
    private final List<Plate> plates;

    public CppSolution(List<Plate> plates) {
        this.plates = plates;
    }

    public int getTotalPrintings() {
        return plates.stream()
                .mapToInt(Plate::getNbPrintings)
                .sum();
    }

    @Override
    public String toString() {
        return String.format("Offset plates:%n" +
                        "%s%n" +
                        "Total printings: %d",
                plates.stream()
                        .sorted(Comparator.reverseOrder())
                        .map(Plate::toString)
                        .collect(Collectors.joining(System.lineSeparator())),
                getTotalPrintings()
        );
    }
}
