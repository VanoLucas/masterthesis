package com.vanolucas.cppsolver.old.v9.crossover;

import com.vanolucas.cppsolver.old.v9.solution.Solution;

import java.util.List;
import java.util.function.BiFunction;

/**
 * Performs a crossover operation on a pair of individuals.
 * The crossover produces children (offspring) out of the 2 parents.
 */
public interface Crossover extends BiFunction<Solution, Solution, List<Solution>> {
    @Override
    default List<Solution> apply(Solution parentA, Solution parentB) {
        return cross(parentA, parentB);
    }

    List<Solution> cross(Solution parentA, Solution parentB);
}
