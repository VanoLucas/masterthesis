package com.vanolucas.cppsolver.old.v9.crossover;

import com.vanolucas.cppsolver.old.v9.mutation.Mutator;
import com.vanolucas.cppsolver.old.v9.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v9.solution.Solution;
import com.vanolucas.cppsolver.old.v9.util.Rand;

import java.util.List;

public class CrossoverThenMutationBreeder implements Breeder {
    private final Crossover crossover;
    private final Mutator mutator;
    private final double mutationProba;
    private final Rand rand;

    public CrossoverThenMutationBreeder(Crossover crossover, Mutator mutator, double mutationProba) {
        this(crossover, mutator, mutationProba, new Rand());
    }

    public CrossoverThenMutationBreeder(Crossover crossover, Mutator mutator, double mutationProba, Rand rand) {
        this.crossover = crossover;
        this.mutator = mutator;
        this.mutationProba = mutationProba;
        this.rand = rand;
    }

    @Override
    public List<Solution> breed(List<EvaluatedSolution> parents) {
        final Solution parentA = parents.get(0).getSolution();
        final Solution parentB = parents.get(1).getSolution();
        // perform crossover
        final List<Solution> children = crossover.cross(parentA, parentB);
        // optionally perform mutation
        for (Solution child : children) {
            if (rand.double01() < mutationProba) {
                mutator.mutate(child);
            }
        }
        return children;
    }
}
