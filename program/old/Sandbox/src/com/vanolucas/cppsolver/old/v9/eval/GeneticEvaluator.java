package com.vanolucas.cppsolver.old.v9.eval;

import com.vanolucas.cppsolver.old.v9.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v9.solution.GeneticSolution;
import com.vanolucas.cppsolver.old.v9.solution.Solution;
import com.vanolucas.cppsolver.old.v9.util.Converter;

import java.util.Collections;
import java.util.List;

public class GeneticEvaluator extends Evaluator {
    private final Converter genotypeDecoder;
    private final List<Converter> intermediateConverters;

    public GeneticEvaluator(Converter genotypeDecoder, EvaluationFunction phenotypeEvalFn) {
        this(genotypeDecoder, (Converter) null, phenotypeEvalFn);
    }

    public GeneticEvaluator(Converter genotypeDecoder, Converter intermediateConverter, EvaluationFunction phenotypeEvalFn) {
        this(genotypeDecoder,
                intermediateConverter != null ? Collections.singletonList(intermediateConverter) : null,
                phenotypeEvalFn
        );
    }

    public GeneticEvaluator(Converter genotypeDecoder, List<Converter> intermediateConverters, EvaluationFunction phenotypeEvalFn) {
        super(phenotypeEvalFn);
        this.genotypeDecoder = genotypeDecoder;
        this.intermediateConverters = intermediateConverters;
    }

    @Override
    public EvaluatedSolution evaluate(Solution solution) {
        final GeneticSolution geneticSolution = (GeneticSolution) solution;
        // first decode the genotype
        geneticSolution.decodeGenotype(genotypeDecoder);
        // if we use intermediate representations, decode them
        if (intermediateConverters != null && !intermediateConverters.isEmpty()) {
            for (int i = 0; i < intermediateConverters.size(); i++) {
                geneticSolution.convertIntermediateRepresentation(i, intermediateConverters.get(i));
            }
        }
        // finally, evaluate the decoded phenotype
        return super.evaluate(geneticSolution);
    }
}
