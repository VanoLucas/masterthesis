package com.vanolucas.cppsolver.old.v9.genome;

import com.vanolucas.cppsolver.old.v9.mutation.rkv.key.KeyMutation;
import com.vanolucas.cppsolver.old.v9.util.Rand;
import com.vanolucas.cppsolver.old.v9.util.Randomizable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

public class RandomKeyVector implements Randomizable, Cloneable {
    private List<Double> keys;

    public RandomKeyVector(int length) {
        this(length, 0d);
    }

    public RandomKeyVector(int length, Double initValue) {
        this(new ArrayList<>(
                Collections.nCopies(length, initValue)
        ));
    }

    public RandomKeyVector(ArrayList<Double> keys) {
        this.keys = keys;
    }

    public int length() {
        return keys.size();
    }

    public Double getKey(int index) {
        return keys.get(index);
    }

    private void setKey(int index, Double value) {
        keys.set(index, value);
    }

    public void mutateKey(int index, KeyMutation keyMutation) {
        setKey(index, keyMutation.newKeyValue(index, this));
    }

    private void setKeyValues(List<Double> keyValues) {
        this.keys = new ArrayList<>(keyValues);
    }

    public DoubleStream doubleStream() {
        return keys.stream().mapToDouble(__ -> __);
    }

    @Override
    public void randomize(Rand rand) {
        setKeyValues(
                rand.doubles01(length())
        );
    }

    public Double randomizeKey(int index, Rand rand) {
        keys.set(index, rand.double01());
        return keys.get(index);
    }

    @Override
    public RandomKeyVector clone() throws CloneNotSupportedException {
        RandomKeyVector cloned = (RandomKeyVector) super.clone();
        cloned.setKeyValues(keys);
        return cloned;
    }

    @Override
    public String toString() {
        final String strKeys = keys.stream()
                .map(Object::toString)
                .collect(Collectors.joining(";"));
        return String.format("(%d genes) %s",
                keys.size(),
                strKeys
        );
    }
}
