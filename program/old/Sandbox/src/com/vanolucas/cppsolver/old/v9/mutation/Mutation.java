package com.vanolucas.cppsolver.old.v9.mutation;

import java.util.function.Consumer;

/**
 * A mutation operator. It performs a mutation on the provided object.
 * @param <T> Type of object that this mutation operator handles.
 */
public interface Mutation<T> extends Consumer<T> {
    @Override
    default void accept(T toMutate) {
        mutate(toMutate);
    }

    default void mutateObj(Object toMutate) {
        //noinspection unchecked
        mutate((T) toMutate);
    }

    void mutate(T toMutate);
}
