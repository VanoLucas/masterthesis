package com.vanolucas.cppsolver.old.v9.mutation;

import com.vanolucas.cppsolver.old.v9.solution.Solution;

/**
 * In charge of running mutation operators on the provided solution.
 */
public interface Mutator {
    Solution mutate(Solution solution);
}
