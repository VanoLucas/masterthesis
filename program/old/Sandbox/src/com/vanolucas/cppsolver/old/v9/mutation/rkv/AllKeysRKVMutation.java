package com.vanolucas.cppsolver.old.v9.mutation.rkv;

import com.vanolucas.cppsolver.old.v9.genome.RandomKeyVector;
import com.vanolucas.cppsolver.old.v9.mutation.Mutation;
import com.vanolucas.cppsolver.old.v9.mutation.rkv.key.KeyMutation;
import com.vanolucas.cppsolver.old.v9.util.Rand;

public class AllKeysRKVMutation implements Mutation<RandomKeyVector> {
    private final KeyMutation keyMutation;
    private final Rand rand;

    public AllKeysRKVMutation(KeyMutation keyMutation, Rand rand) {
        this.keyMutation = keyMutation;
        this.rand = rand;
    }

    @Override
    public void mutate(RandomKeyVector rkv) {
        final int length = rkv.length();
        for (int k = 0; k < length; k++) {
            keyMutation.mutateKey(k, rkv);
        }
    }
}
