package com.vanolucas.cppsolver.old.v9.mutation.rkv;

import com.vanolucas.cppsolver.old.v9.genome.RandomKeyVector;
import com.vanolucas.cppsolver.old.v9.mutation.Mutation;
import com.vanolucas.cppsolver.old.v9.mutation.rkv.key.KeyMutation;
import com.vanolucas.cppsolver.old.v9.util.Rand;

public class ConsecutiveKeysRKVMutation implements Mutation<RandomKeyVector> {
    private final int nbConsecutive;
    private final KeyMutation keyMutation;
    private final Rand rand;

    public ConsecutiveKeysRKVMutation(int nbConsecutive, KeyMutation keyMutation, Rand rand) {
        this.nbConsecutive = nbConsecutive;
        this.keyMutation = keyMutation;
        this.rand = rand;
    }

    @Override
    public void mutate(RandomKeyVector rkv) {
        // pick first key to mutate
        final int startIndex = rand.int0To(rkv.length() - nbConsecutive + 1);
        // mutate the picked and subsequent keys
        for (int i = startIndex; i < startIndex + nbConsecutive; i++) {
            keyMutation.mutateKey(i, rkv);
        }
    }
}
