package com.vanolucas.cppsolver.old.v9.mutation.rkv.key;

import com.vanolucas.cppsolver.old.v9.genome.RandomKeyVector;
import com.vanolucas.cppsolver.old.v9.util.Rand;
import com.vanolucas.cppsolver.old.v9.util.rangeguard.BlockOnBoundsDoubleRangeGuard;
import com.vanolucas.cppsolver.old.v9.util.rangeguard.DoubleRangeGuard;

public class AddRandValueKeyMutation implements KeyMutation {
    private final Rand rand;
    private final double minValue;
    private final double maxValue;
    private final DoubleRangeGuard rangeGuard;

    public AddRandValueKeyMutation(Rand rand, double minValue, double maxValue) {
        this(rand, minValue, maxValue, new BlockOnBoundsDoubleRangeGuard());
    }

    public AddRandValueKeyMutation(Rand rand, double minValue, double maxValue, DoubleRangeGuard rangeGuard) {
        this.rand = rand;
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.rangeGuard = rangeGuard;
    }

    @Override
    public Double newKeyValue(int index, RandomKeyVector rkv) {
        final double oldValue = rkv.getKey(index);
        final double newValue = oldValue + rand.doubleBetween(minValue, maxValue);
        return rangeGuard.bringBackToRange(newValue, 0d, 1d);
    }
}
