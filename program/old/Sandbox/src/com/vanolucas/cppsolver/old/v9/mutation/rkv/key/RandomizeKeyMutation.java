package com.vanolucas.cppsolver.old.v9.mutation.rkv.key;

import com.vanolucas.cppsolver.old.v9.genome.RandomKeyVector;
import com.vanolucas.cppsolver.old.v9.util.Rand;

public class RandomizeKeyMutation implements KeyMutation {
    private final Rand rand;

    public RandomizeKeyMutation(Rand rand) {
        this.rand = rand;
    }

    @Override
    public void mutateKey(int index, RandomKeyVector rkv) {
        rkv.randomizeKey(index, rand);
    }

    @Override
    public Double newKeyValue(int index, RandomKeyVector rkv) {
        return rand.double01();
    }
}
