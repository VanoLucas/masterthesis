package com.vanolucas.cppsolver.old.v9.quality;

/**
 * A Cost Quality quantified as a single integer value.
 */
public class IntCost extends SingleNumberQuality<Integer> {

    public IntCost(Integer value) {
        super(value);
    }

    /**
     * A better cost is a lower one.
     */
    @Override
    public boolean isBetterThan(Quality other) {
        IntCost o = (IntCost) other;
        return value < o.value;
    }
}
