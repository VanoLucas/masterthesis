package com.vanolucas.cppsolver.old.v9.quality;

/**
 * A Quality that is quantified as a single value.
 *
 * @param <T> Type of the number for this Quality's value.
 */
public abstract class SingleNumberQuality<T extends Number> extends Quality {

    final T value;

    SingleNumberQuality(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    /**
     * Two single number quality are equivalent if they have the same value.
     */
    @Override
    public boolean isEquivalentTo(Quality other) {
        SingleNumberQuality o = (SingleNumberQuality) other;
        return value.equals(o.value);
    }

    @Override
    public String toString() {
        return value.toString();
    }
}
