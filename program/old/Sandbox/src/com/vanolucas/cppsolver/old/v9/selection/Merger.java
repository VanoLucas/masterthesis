package com.vanolucas.cppsolver.old.v9.selection;

import com.vanolucas.cppsolver.old.v9.solution.EvaluatedSolution;

import java.util.List;
import java.util.function.BiFunction;

/**
 * Merges a population and its offspring to a single population.
 */
public interface Merger extends BiFunction<List<EvaluatedSolution>, List<EvaluatedSolution>, List<EvaluatedSolution>> {
    @Override
    default List<EvaluatedSolution> apply(List<EvaluatedSolution> population, List<EvaluatedSolution> offspring) {
        return merge(population, offspring);
    }

    List<EvaluatedSolution> merge(List<EvaluatedSolution> population, List<EvaluatedSolution> offspring);
}
