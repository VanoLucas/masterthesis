package com.vanolucas.cppsolver.old.v9.selection;

import com.vanolucas.cppsolver.old.v9.solution.EvaluatedSolution;

import java.util.List;
import java.util.function.Function;

/**
 * Generates groups of mates that are selected for reproduction in an evolutionary algorithm.
 */
public interface Selector extends Function<List<EvaluatedSolution>, List<List<EvaluatedSolution>>> {
    @Override
    default List<List<EvaluatedSolution>> apply(List<EvaluatedSolution> population) {
        return selectGroups(population);
    }

    List<List<EvaluatedSolution>> selectGroups(List<EvaluatedSolution> population);
}
