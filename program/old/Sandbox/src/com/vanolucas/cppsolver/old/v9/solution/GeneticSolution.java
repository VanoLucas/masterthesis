package com.vanolucas.cppsolver.old.v9.solution;

import com.vanolucas.cppsolver.old.v9.mutation.Mutation;
import com.vanolucas.cppsolver.old.v9.util.Cloner;
import com.vanolucas.cppsolver.old.v9.util.Converter;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class GeneticSolution extends Solution {
    private final Object genotype;
    private final Object[] intermediates;

    public GeneticSolution(Object genotype) {
        this(genotype, 0);
    }

    public GeneticSolution(Object genotype, int nbIntermediateRepresentations) {
        this(genotype, nbIntermediateRepresentations, null);
    }

    public GeneticSolution(Object genotype, int nbIntermediateRepresentations, Object phenotype) {
        super(phenotype);
        this.genotype = genotype;
        this.intermediates = new Object[nbIntermediateRepresentations];
    }

    public Object getGenotype() {
        return genotype;
    }

    public void mutateGenotype(Mutation mutation) {
        mutation.mutateObj(genotype);
    }

    public void decodeGenotype(Converter decoder) {
        // decode the genotype using the provided decoder
        final Object decoded = decoder.convertObj(genotype);
        // if we use intermediate representations, the result is our first intermediate representation
        if (intermediates.length > 0) {
            intermediates[0] = decoded;
        }
        // else, the result is our phenotype
        else {
            phenotype = decoded;
        }
    }

    public void convertIntermediateRepresentation(int index, Converter converter) {
        // decode the intermediate representation using the provided converter
        final Object converted = converter.convertObj(intermediates[index]);
        // if we decoded the last intermediate representation, the result is our phenotype
        if (index == intermediates.length - 1) {
            phenotype = converted;
        }
        // else, store this next intermediate representation
        else {
            intermediates[index + 1] = converted;
        }
    }

    private Object cloneGenotype(Cloner cloner) {
        return cloner.cloneObj(genotype);
    }

    @Override
    GeneticSolution clone(Cloner genotypeCloner) {
        return clone(genotypeCloner, null);
    }

    GeneticSolution clone(Cloner genotypeCloner, Cloner phenotypeCloner) {
        final Object clonedGenotype = cloneGenotype(genotypeCloner);
        if (phenotypeCloner != null) {
            final Object clonedPhenotype = clonePhenotype(phenotypeCloner);
            return new GeneticSolution(
                    clonedGenotype,
                    intermediates.length,
                    clonedPhenotype
            );
        } else {
            return new GeneticSolution(
                    clonedGenotype,
                    intermediates.length
            );
        }
    }

    @Override
    public String toString() {
        final String strIntermediates = IntStream.range(0, intermediates.length)
                .mapToObj(i -> {
                    if (intermediates.length >= 2) {
                        return String.format("Intermediate %d:%n" +
                                        "%s",
                                i,
                                intermediates[i]
                        );
                    } else {
                        return String.format("Intermediate:%n" +
                                        "%s",
                                intermediates[i]
                        );
                    }
                })
                .collect(Collectors.joining(String.format("%n")));
        return String.format("Genotype:%n" +
                        "%s%n" +
                        "%s%n" +
                        "Phenotype:%n" +
                        "%s",
                genotype,
                strIntermediates,
                phenotype
        );
    }
}
