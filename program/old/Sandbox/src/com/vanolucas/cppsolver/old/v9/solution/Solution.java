package com.vanolucas.cppsolver.old.v9.solution;

import com.vanolucas.cppsolver.old.v9.eval.EvaluationFunction;
import com.vanolucas.cppsolver.old.v9.mutation.Mutation;
import com.vanolucas.cppsolver.old.v9.quality.Quality;
import com.vanolucas.cppsolver.old.v9.util.Cloner;

public class Solution {
    Object phenotype;

    public Solution(Object phenotype) {
        this.phenotype = phenotype;
    }

    public Quality evaluatePhenotype(EvaluationFunction phenotypeEvalFn) {
        return phenotypeEvalFn.evaluate(phenotype);
    }

    public void mutatePhenotype(Mutation mutation) {
        mutation.mutateObj(phenotype);
    }

    Object clonePhenotype(Cloner cloner) {
        return cloner.cloneObj(phenotype);
    }

    Solution clone(Cloner phenotypeCloner) {
        return new Solution(
                clonePhenotype(phenotypeCloner)
        );
    }

    @Override
    public String toString() {
        return String.format("%s", phenotype);
    }
}
