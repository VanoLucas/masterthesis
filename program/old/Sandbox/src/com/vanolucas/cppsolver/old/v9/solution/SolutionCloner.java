package com.vanolucas.cppsolver.old.v9.solution;

import com.vanolucas.cppsolver.old.v9.util.Cloner;

public class SolutionCloner {
    final Cloner phenotypeCloner;

    public SolutionCloner(Cloner phenotypeCloner) {
        this.phenotypeCloner = phenotypeCloner;
    }

    public Solution clone(Solution solution) {
        return solution.clone(phenotypeCloner);
    }
}
