package com.vanolucas.cppsolver.old.v9.util;

import java.util.function.Function;

public interface Converter<TSrc, TDst> extends Function<TSrc, TDst> {
    @Override
    default TDst apply(TSrc src) {
        return convert(src);
    }

    default TDst convertObj(Object src) {
        //noinspection unchecked
        return convert((TSrc) src);
    }

    TDst convert(TSrc src);
}
