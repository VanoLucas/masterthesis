package com.vanolucas.cppsolver.old.v9.util;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Supplier;

public class Pool<T> {
    private final Supplier<T> factory;
    private final ConcurrentLinkedQueue<T> pool = new ConcurrentLinkedQueue<>();

    public Pool(Supplier<T> factory) {
        this.factory = factory;
    }

    public T fetch() {
        T item = pool.poll();
        while (item == null) {
            pool.offer(factory.get());
            item = pool.poll();
        }
        return item;
    }

    public void returnToPool(T item) {
        pool.offer(item);
    }
}
