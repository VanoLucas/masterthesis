package com.vanolucas.cppsolver.old.v9.util.rangeguard;

public class BlockOnBoundsDoubleRangeGuard implements DoubleRangeGuard {
    @Override
    public double bringBackToRange(double value, double minInclusive, double maxExclusive) {
        // bring superior values back to the max allowed value
        if (value >= maxExclusive) {
            return Math.nextDown(maxExclusive);
        }
        // bring inferior values back to the min allowed value
        else if (value < minInclusive) {
            return minInclusive;
        }
        // keep values in range
        else {
            return value;
        }
    }
}
