package com.vanolucas.cppsolver.old.v1.cpp.eval;

import com.vanolucas.cppsolver.old.v1.cpp.genome.GenomeSFP;
import com.vanolucas.cppsolver.old.v1.cpp.problem.StandardCppInstance;
import com.vanolucas.cppsolver.old.v1.cpp.solution.AllocatedSlots;
import com.vanolucas.cppsolver.old.v1.cpp.solution.Slot;
import com.vanolucas.cppsolver.old.v1.genetic.Genotype;
import com.vanolucas.cppsolver.old.v1.genetic.GenotypeDecoder;
import com.vanolucas.cppsolver.old.v1.genetic.Phenotype;
import com.vanolucas.cppsolver.old.v1.util.math.CutCake;

import java.util.ArrayList;
import java.util.List;

/**
 * Decodes GenomeSFP genomes to the corresponding AllocatedSlots phenotype.
 */
public class DecoderSFP implements GenotypeDecoder {

    private final int nbBookCovers;
    private final int nbSlots;
    private final List<Integer> copies;

    private DecoderSFP(StandardCppInstance problem) {
        this(problem.getNbBookCovers(), problem.getTotalNbSlots(), problem.getCopies());
    }

    public DecoderSFP(int nbBookCovers, int nbSlots, List<Integer> copies) {
        this.nbBookCovers = nbBookCovers;
        this.nbSlots = nbSlots;
        this.copies = copies;
    }

    public static DecoderSFP forProblem(StandardCppInstance problem) {
        return new DecoderSFP(problem);
    }

    @Override
    public Phenotype decode(Genotype genotype) {
        return decode((GenomeSFP) genotype);
    }

    public AllocatedSlots decode(GenomeSFP genome) {
        // get genome keys that we will use to determine the nb of slots for each book cover
        final List<Double> keysNbExtraSlotsPerBookCover = genome.getKeys(0, nbBookCovers);

        // calculate nb of extra slots for each book cover
        int[] nbExtraSlotsForEachBookCover = CutCake.getShares(
                nbSlots - nbBookCovers,
                keysNbExtraSlotsPerBookCover.stream().mapToDouble(__ -> __).toArray()
        );

        // get genome keys that we will use to determine the nb of printings allocated to the first slot of each book cover
        final List<Double> keysNbPrintingsFirstSlotOfEachBookCover = genome.getKeys(nbBookCovers, 2 * nbBookCovers);

        // get genome keys that we will use to determine the nb of printings allocated to the first slot of each book cover
        final List<Double> keysNbPrintingsForEachExtraSlot = genome.getKeys(2 * nbBookCovers, genome.length());

        // determine the slots allocation for each book cover
        List<Slot> slots = new ArrayList<>(nbSlots);
        int cursorExtraSlot = 0;
        for (int bookCoverIndex = 0; bookCoverIndex < nbBookCovers; bookCoverIndex++) {
            // get nb of extra slots we should have for this book cover
            final int nbExtraSlotsWithThisBookCover = nbExtraSlotsForEachBookCover[bookCoverIndex];
            // get key that will determine the nb of printings in the first slot with this book cover
            final double keyNbPrintingsFirstSlot = keysNbPrintingsFirstSlotOfEachBookCover.get(bookCoverIndex);

            // array to store the keys that will determine the nb of printings in each slot with this book cover
            double[] keysNbPrintingsForEachSlot = new double[nbExtraSlotsWithThisBookCover + 1];
            // the first slot's key is known
            keysNbPrintingsForEachSlot[0] = keyNbPrintingsFirstSlot;

            // get the key for each extra slot to which this book cover should be allocated
            for (int i = 0; i < nbExtraSlotsWithThisBookCover; i++) {
                // read the genome keys that determine the nb of printings for each extra slot with our book cover
                keysNbPrintingsForEachSlot[i + 1] = keysNbPrintingsForEachExtraSlot.get(cursorExtraSlot);
                // move to next key
                cursorExtraSlot++;
            }

            // determine nb of printings to allocate to each slot with this book cover
            final int[] nbPrintings = CutCake.getShares(copies.get(bookCoverIndex), keysNbPrintingsForEachSlot);

            // we now know all the necessary params, create the slots for this book cover
            for (int i = 0; i < nbExtraSlotsWithThisBookCover + 1; i++) {
                slots.add(new Slot(bookCoverIndex, nbPrintings[i]));
            }
        }

        // place all slots in our phenotype
        return new AllocatedSlots(slots);
    }
}
