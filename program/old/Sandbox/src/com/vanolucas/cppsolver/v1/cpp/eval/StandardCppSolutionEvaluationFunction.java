package com.vanolucas.cppsolver.old.v1.cpp.eval;

import com.vanolucas.cppsolver.old.v1.cpp.solution.StandardCppSolution;
import com.vanolucas.cppsolver.old.v1.opti.evaluation.EvaluationFunction;
import com.vanolucas.cppsolver.old.v1.opti.quality.Cost;

public class StandardCppSolutionEvaluationFunction implements EvaluationFunction {
    @Override
    public Cost evaluate(Object solution) {
        return evaluate((StandardCppSolution) solution);
    }

    public Cost evaluate(StandardCppSolution solution) {
        return new Cost((double) solution.getCost());
    }
}
