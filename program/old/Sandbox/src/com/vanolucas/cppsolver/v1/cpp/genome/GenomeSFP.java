package com.vanolucas.cppsolver.old.v1.cpp.genome;

import com.vanolucas.cppsolver.old.v1.cpp.problem.StandardCppInstance;
import com.vanolucas.cppsolver.old.v1.genetic.RandomKeyVectorGenome;

/**
 * GenomeSFP parts:
 * - nb of extra Slots for each book cover (length = nbBookCovers)
 * - nb of printings of First slot for each book cover (length = nbBookCovers)
 * - nb of Printings for all other slots (length = nbSlots - nbBookCovers)
 */
public class GenomeSFP extends RandomKeyVectorGenome {

    private static final double INIT_GENE_VALUE = 0d;

    protected GenomeSFP(StandardCppInstance problem) {
        this(getGenomeLengthFor(problem));
    }

    protected GenomeSFP(int length) {
        super(length, INIT_GENE_VALUE);
    }

    public static GenomeSFP forProblem(StandardCppInstance problem) {
        return new GenomeSFP(problem);
    }

    /**
     * The length of a type SFP genome is the number of book covers + number of slots.
     * Length = m + n*S
     *
     * @return The size of the SFP genome that can describe solutions for the provided problem.
     */
    private static int getGenomeLengthFor(StandardCppInstance problem) {
        return problem.getNbBookCovers() + problem.getTotalNbSlots();
    }

    @Override
    public String toString() {
        return String.format("%s\n%s",
                getClass().getSimpleName(),
                super.toString()
        );
    }
}
