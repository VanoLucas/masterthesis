package com.vanolucas.cppsolver.old.v1.cpp.indiv;

import com.vanolucas.cppsolver.old.v1.cpp.genome.GenomeCP;
import com.vanolucas.cppsolver.old.v1.cpp.problem.StandardCppInstance;
import com.vanolucas.cppsolver.old.v1.opti.indiv.GeneticIndividual;

public class IndividualCP extends GeneticIndividual {

    public IndividualCP(StandardCppInstance problem) {
        super(GenomeCP.forProblem(problem));
    }
}
