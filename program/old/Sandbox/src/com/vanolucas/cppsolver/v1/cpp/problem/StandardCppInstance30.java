package com.vanolucas.cppsolver.old.v1.cpp.problem;

import java.util.Arrays;
import java.util.List;

public class StandardCppInstance30 extends StandardCppInstance {

    private static List<Integer> COPIES = Arrays.asList(
            30000,
            28000,
            27000,
            26000,
            26000,
            23000,
            22000,
            22000,
            20000,
            20000,
            19000,
            18000,
            17000,
            16000,
            15000,
            15000,
            14000,
            13500,
            13000,
            11000,
            10500,
            10000,
            9000,
            9000,
            7500,
            6000,
            5000,
            2500,
            1500,
            1000
    );

    private static final int NB_OFFSET_PLATES = 10;
    private static final int NB_SLOTS_PER_PLATE = 4;

    public StandardCppInstance30() {
        super(COPIES, NB_OFFSET_PLATES, NB_SLOTS_PER_PLATE);
    }
}
