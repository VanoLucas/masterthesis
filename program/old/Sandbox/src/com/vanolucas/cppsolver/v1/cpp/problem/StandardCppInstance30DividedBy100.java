package com.vanolucas.cppsolver.old.v1.cpp.problem;

import java.util.List;
import java.util.stream.Collectors;

public class StandardCppInstance30DividedBy100 extends StandardCppInstance {

    private static List<Integer> COPIES = new StandardCppInstance30().getCopies().stream()
            .map(copy -> copy / 100)
            .collect(Collectors.toList());

    private static final int NB_OFFSET_PLATES = 10;
    private static final int NB_SLOTS_PER_PLATE = 4;

    public StandardCppInstance30DividedBy100() {
        super(COPIES, NB_OFFSET_PLATES, NB_SLOTS_PER_PLATE);
    }
}
