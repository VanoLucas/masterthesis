package com.vanolucas.cppsolver.old.v1.cpp.solution;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents an offset plate.
 * An offset plate has S slots.
 * Each slot can be assigned a book cover index.
 */
public class OffsetPlate {

    /**
     * Slots of this offset plate.
     * Each slot can be assigned a book cover index.
     */
    private List<Integer> slots;

    private OffsetPlate(List<Integer> slots) {
        this.slots = slots;
    }

    /**
     * @return Mew offset plate with allocated book covers from the provides slots.
     */
    public static OffsetPlate withSlots(Collection<Slot> slots) {
        return new OffsetPlate(
                // get the allocated book cover indexes from each slot
                slots.stream()
                        .mapToInt(Slot::getBookCoverIndex)
                        .boxed()
                        .collect(Collectors.toList())
        );
    }

    /**
     * @return New offset plate with empty (null) slots.
     */
    public static OffsetPlate withNbSlots(int nbSlots) {
        // init empty slots
        final List<Integer> slots = new ArrayList<>(nbSlots);
        for (int i = 0; i < nbSlots; i++) {
            slots.add(null);
        }
        // create offset plate with those slots
        return new OffsetPlate(slots);
    }

    /**
     * @return New offset plate with the book cover indexes assigned to the slots respectively of the order.
     */
    public static OffsetPlate withBookCovers(Collection<Integer> bookCoverIndexes) {
        // create offset plate containing the provided book cover indexes in the slots
        return new OffsetPlate(new ArrayList<>(bookCoverIndexes));
    }

    /**
     * Assign the first free slot to the provided book cover index.
     */
    public void addBookCover(int bookCoverIndex) {
        final int nbSlots = slots.size();
        for (int i = 0; i < nbSlots; i++) {
            if (slots.get(i) == null) {
                slots.set(i, bookCoverIndex);
                return;
            }
        }
        throw new IndexOutOfBoundsException(
                "Offset plate slots already fully assigned. No more book cover can be assigned to this offset plate."
        );
    }

    @Override
    public String toString() {
        return slots.stream()
                .sorted()
                .map(bookCoverIndex -> String.format("%3d", bookCoverIndex != null ? bookCoverIndex : -99))
                .collect(Collectors.joining("|"));
    }
}
