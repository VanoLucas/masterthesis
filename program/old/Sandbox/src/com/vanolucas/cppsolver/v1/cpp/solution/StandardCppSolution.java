package com.vanolucas.cppsolver.old.v1.cpp.solution;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Defines a solution to an instance of the Standard Cover Printing Problem.
 */
public class StandardCppSolution {

    /**
     * Offset plates and their number of printings.
     */
    private List<OffsetPlatePrintings> offsetPlatePrintings;

    private StandardCppSolution(List<OffsetPlatePrintings> offsetPlatePrintings) {
        this.offsetPlatePrintings = offsetPlatePrintings;
    }

    public static StandardCppSolution withOffsetPlatePrintings(Collection<OffsetPlatePrintings> offsetPlatePrintings) {
        // sort offset plates by number of printings descending
        List<OffsetPlatePrintings> sortedOffsetPlatePrintings = new ArrayList<>(offsetPlatePrintings);
        sortedOffsetPlatePrintings.sort(Comparator.reverseOrder());
        return new StandardCppSolution(sortedOffsetPlatePrintings);
    }

    public int getTotalNbPrintings() {
        return offsetPlatePrintings.stream()
                .mapToInt(OffsetPlatePrintings::getNbPrintings)
                .sum();
    }

    public int getCost() {
        return getTotalNbPrintings();
    }

    @Override
    public String toString() {
        final String strOffsetPlates = offsetPlatePrintings.stream()
                .map(OffsetPlatePrintings::toString)
                .collect(Collectors.joining("\n"));
        return String.format("Offset plates:\n%s\nCost = %d",
                strOffsetPlates,
                getCost()
        );
    }
}
