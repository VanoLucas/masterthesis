package com.vanolucas.cppsolver.old.v1.genetic;

import com.vanolucas.cppsolver.old.v1.util.math.Rand;
import com.vanolucas.cppsolver.old.v1.util.Randomizable;

/**
 * Represents a gene.
 * A gene is an atomic value of a Genome.
 */
public interface Gene extends Randomizable, Cloneable {

    /**
     * Randomize this Gene's value.
     *
     * @param rng The random number generator to use.
     */
    @Override
    void randomize(Rand rng);

    Gene clone() throws CloneNotSupportedException;
}
