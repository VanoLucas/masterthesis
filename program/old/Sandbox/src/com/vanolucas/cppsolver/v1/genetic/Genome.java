package com.vanolucas.cppsolver.old.v1.genetic;

import com.vanolucas.cppsolver.old.v1.util.math.Rand;
import com.vanolucas.cppsolver.old.v1.util.Randomizable;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Represents a genome.
 * A genome is a sequence of genes.
 */
public class Genome implements Genotype, Randomizable {

    /**
     * The sequence of genes of this Genome.
     */
    protected List<Gene> genes;

    protected Genome(List<Gene> genes) {
        this.genes = genes;
    }

    public List<Gene> getGenes() {
        return genes;
    }

    public int length() {
        return genes.size();
    }

    public Stream<Gene> stream() {
        return genes.stream();
    }

    /**
     * Randomize all genes of this Genome.
     *
     * @param rng The random number generator to use to randomize our genes.
     */
    @Override
    public void randomize(Rand rng) {
        for (Gene gene : genes) {
            gene.randomize(rng);
        }
    }

    @Override
    public Genome clone() throws CloneNotSupportedException {
        Genome cloned = (Genome) super.clone();
        cloned.genes = new ArrayList<>(genes.size());
        for (Gene gene : genes) {
            cloned.genes.add(gene.clone());
        }
        return cloned;
    }

    @Override
    public String toString() {
        final String strGenes = genes.stream()
                .map(Gene::toString)
                .collect(Collectors.joining(";"));
        return String.format("%s\n(%d genes)", strGenes, length());
    }
}
