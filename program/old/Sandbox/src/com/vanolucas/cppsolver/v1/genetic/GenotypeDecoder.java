package com.vanolucas.cppsolver.old.v1.genetic;

/**
 * Function that decodes a given genotype to its corresponding phenotype.
 */
public interface GenotypeDecoder {

    Phenotype decode(Genotype genotype);
}
