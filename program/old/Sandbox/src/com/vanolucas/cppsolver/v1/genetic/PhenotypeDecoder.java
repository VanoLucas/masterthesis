package com.vanolucas.cppsolver.old.v1.genetic;

/**
 * Converts a given phenotype to the corresponding actual solution of a problem.
 */
public interface PhenotypeDecoder {

    Object decode(Phenotype phenotype);
}
