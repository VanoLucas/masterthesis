package com.vanolucas.cppsolver.old.v1.opti.algo;

import com.vanolucas.cppsolver.old.v1.opti.indiv.Individual;
import com.vanolucas.cppsolver.old.v1.opti.quality.Quality;

public interface HillClimbingAlgoListener extends OptiAlgoListener {

    default void onQualityProgressSinceLastIteration(Quality prevSolutionQuality, Quality currentSolutionQuality) {
    }

    default void onNoBetterNeighbor(long iteration, Individual currentIndiv) {
    }

    default void onAllNeighborsWorse(long iteration, Individual localOptimum) {
    }

    default void onRandomRestart(long iteration, Quality qualityBeforeRestart) {
    }
}
