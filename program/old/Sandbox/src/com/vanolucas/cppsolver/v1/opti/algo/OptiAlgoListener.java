package com.vanolucas.cppsolver.old.v1.opti.algo;

import com.vanolucas.cppsolver.old.v1.opti.indiv.Individual;

/**
 * Listener that can react to Algo events.
 */
public interface OptiAlgoListener {

    default void onIterationEnd(long iteration) {
    }

    default void onNewBetter(long iteration, Individual newBetterIndiv) {
    }
}
