package com.vanolucas.cppsolver.old.v1.opti.clone;

import com.vanolucas.cppsolver.old.v1.opti.indiv.Individual;

/**
 * Creates a clone of the provided Individual.
 */
public interface IndividualCloner {

    Individual clone(Individual original) throws CloneNotSupportedException;
}
