package com.vanolucas.cppsolver.old.v1.opti.evaluation;

import com.vanolucas.cppsolver.old.v1.opti.quality.Quality;

/**
 * Evaluates the quality of a given solution.
 */
public interface EvaluationFunction {

    Quality evaluate(Object solution);
}
