package com.vanolucas.cppsolver.old.v1.opti.evaluation;

import com.vanolucas.cppsolver.old.v1.genetic.GenotypeDecoder;
import com.vanolucas.cppsolver.old.v1.opti.indiv.GeneticIndividual;
import com.vanolucas.cppsolver.old.v1.opti.indiv.Individual;
import com.vanolucas.cppsolver.old.v1.genetic.PhenotypeDecoder;
import com.vanolucas.cppsolver.old.v1.opti.quality.Quality;

public class GeneticIndividualEvaluator implements IndividualEvaluator {

    private GenotypeDecoder genotypeDecoder;
    private PhenotypeDecoder phenotypeDecoder;
    private EvaluationFunction evaluationFunction;

    public GeneticIndividualEvaluator(GenotypeDecoder genotypeDecoder,
                                      PhenotypeDecoder phenotypeDecoder,
                                      EvaluationFunction evaluationFunction) {
        this.genotypeDecoder = genotypeDecoder;
        this.phenotypeDecoder = phenotypeDecoder;
        this.evaluationFunction = evaluationFunction;
    }

    @Override
    public Quality evaluate(Individual individual) {
        return evaluate((GeneticIndividual) individual);
    }

    public Quality evaluate(GeneticIndividual individual) {
        // first decode from the genotype
        individual.decodeGenotypeUsing(genotypeDecoder);
        // then convert the phenotype to the corresponding solution
        individual.decodePhenotypeUsing(phenotypeDecoder);
        // finally, evaluate the decoded solution
        return evaluationFunction.evaluate(individual.getSolution());
    }
}
