package com.vanolucas.cppsolver.old.v1.opti.evaluation;

import com.vanolucas.cppsolver.old.v1.opti.indiv.Individual;
import com.vanolucas.cppsolver.old.v1.opti.quality.Quality;

/**
 * An IndividualEvaluator takes a Individual as input.
 * It evaluates and returns its quality.
 */
public interface IndividualEvaluator {

    /**
     * @param individual Individual to evaluate.
     * @return The quality of the provided individual.
     */
    Quality evaluate(Individual individual);
}
