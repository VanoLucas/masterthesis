package com.vanolucas.cppsolver.old.v1.opti.mutation;

import com.vanolucas.cppsolver.old.v1.genetic.RandomKeyVectorGenome;
import com.vanolucas.cppsolver.old.v1.util.math.Rand;

public class AddSameRandomValueToAllKeys implements RandomKeyVectorGenomeMutator {

    private double minValueToAdd;
    private double maxValueToAdd;

    private Rand rand;

    public AddSameRandomValueToAllKeys(double minValueToAdd, double maxValueToAdd) {
        this(minValueToAdd, maxValueToAdd, new Rand());
    }

    public AddSameRandomValueToAllKeys(double minValueToAdd, double maxValueToAdd, Rand rand) {
        this.minValueToAdd = minValueToAdd;
        this.maxValueToAdd = maxValueToAdd;
        this.rand = rand;
    }

    @Override
    public void mutate(RandomKeyVectorGenome genome) {
        final double valueToAdd = rand.doubleInRange(minValueToAdd, maxValueToAdd);
        genome.forEachGene(gene -> gene.plus(valueToAdd));
    }
}
