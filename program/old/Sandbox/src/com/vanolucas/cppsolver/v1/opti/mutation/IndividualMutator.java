package com.vanolucas.cppsolver.old.v1.opti.mutation;

import com.vanolucas.cppsolver.old.v1.opti.indiv.Individual;

/**
 * Mutation operator.
 * It performs a mutation operation on the provided individual.
 */
public interface IndividualMutator {

    default Individual mutate(Individual individual) {
        mutateIndividual(individual);
        return individual;
    }

    void mutateIndividual(Individual individual);
}
