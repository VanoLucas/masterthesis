package com.vanolucas.cppsolver.old.v1.opti.mutation.gene;

import com.vanolucas.cppsolver.old.v1.genetic.Double01Gene;
import com.vanolucas.cppsolver.old.v1.genetic.Gene;

public interface Double01GeneMutator extends GeneMutator {

    @Override
    default void mutateGene(Gene gene) {
        mutateGene((Double01Gene) gene);
    }

    void mutateGene(Double01Gene gene);
}
