package com.vanolucas.cppsolver.old.v1.opti.mutation.gene;

import com.vanolucas.cppsolver.old.v1.genetic.Gene;

public interface GeneMutator {

    void mutateGene(Gene gene);
}
