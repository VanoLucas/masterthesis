package com.vanolucas.cppsolver.old.v1.opti.mutation.genome;

import com.vanolucas.cppsolver.old.v1.genetic.Genome;
import com.vanolucas.cppsolver.old.v1.opti.mutation.gene.GeneMutator;

/**
 * Mutation operator that applies a gene mutation to each gene of a Genome.
 */
public class AllGenesGenomeMutator implements GenomeMutator {

    private GeneMutator geneMutator;

    public AllGenesGenomeMutator(GeneMutator geneMutator) {
        this.geneMutator = geneMutator;
    }

    @Override
    public void mutate(Genome genome) {
        genome.getGenes().forEach(gene ->
                geneMutator.mutateGene(gene)
        );
    }
}
