package com.vanolucas.cppsolver.old.v1.opti.mutation.genome;

import com.vanolucas.cppsolver.old.v1.genetic.Genome;
import com.vanolucas.cppsolver.old.v1.opti.mutation.gene.GeneMutator;
import com.vanolucas.cppsolver.old.v1.util.math.Rand;

/**
 * Mutation operator that mutates one gene chosen at random in a Genome.
 */
public class OneRandomGeneGenomeMutator implements GenomeMutator {

    private GeneMutator geneMutator;

    protected Rand rand;

    public OneRandomGeneGenomeMutator(GeneMutator geneMutator) {
        this(geneMutator, new Rand());
    }

    public OneRandomGeneGenomeMutator(GeneMutator geneMutator, Rand rand) {
        this.geneMutator = geneMutator;
        this.rand = rand;
    }

    @Override
    public void mutate(Genome genome) {
        geneMutator.mutateGene(
                rand.uniformPick(genome.getGenes())
        );
    }
}
