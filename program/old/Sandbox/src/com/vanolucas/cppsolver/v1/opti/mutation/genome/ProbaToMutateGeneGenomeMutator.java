package com.vanolucas.cppsolver.old.v1.opti.mutation.genome;

import com.vanolucas.cppsolver.old.v1.genetic.Genome;
import com.vanolucas.cppsolver.old.v1.opti.mutation.gene.GeneMutator;
import com.vanolucas.cppsolver.old.v1.util.math.Rand;

/**
 * Mutation operator that decides for each gene of a Genome whether or not to apply a mutation on it based on a probability.
 */
public class ProbaToMutateGeneGenomeMutator implements GenomeMutator {

    private double geneMutationProba;
    private GeneMutator geneMutator;

    protected Rand rand;

    public ProbaToMutateGeneGenomeMutator(double geneMutationProba, GeneMutator geneMutator) {
        this(geneMutationProba, geneMutator, new Rand());
    }

    public ProbaToMutateGeneGenomeMutator(double geneMutationProba, GeneMutator geneMutator, Rand rand) {
        if (geneMutationProba < 0d || geneMutationProba > 1d) {
            throw new IllegalArgumentException(String.format("The gene mutation proba in a %s must be in the range [0d..1d].",
                    getClass().getSimpleName()
            ));
        }
        this.geneMutationProba = geneMutationProba;
        this.geneMutator = geneMutator;
        this.rand = rand;
    }

    @Override
    public void mutate(Genome genome) {
        rand.keepWithProba(genome.getGenes(), geneMutationProba)
                .forEach(gene -> geneMutator.mutateGene(gene));
    }
}
