package com.vanolucas.cppsolver.old.v1.opti.mutation.genome;

import com.vanolucas.cppsolver.old.v1.genetic.Genome;
import com.vanolucas.cppsolver.old.v1.util.math.Rand;

public class RandomizeGenomeMutator implements GenomeMutator {

    private Rand rand;

    public RandomizeGenomeMutator() {
        this(new Rand());
    }

    public RandomizeGenomeMutator(Rand rand) {
        this.rand = rand;
    }

    @Override
    public void mutate(Genome genome) {
        genome.randomize(rand);
    }
}
