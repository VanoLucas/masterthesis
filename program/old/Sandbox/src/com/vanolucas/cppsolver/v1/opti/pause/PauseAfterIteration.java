package com.vanolucas.cppsolver.old.v1.opti.pause;

import com.vanolucas.cppsolver.old.v1.opti.algo.OptiAlgo;

/**
 * Criterion to pause an opti algo once it has performed the target iteration.
 */
public class PauseAfterIteration implements PauseCriterion {

    private long targetIteration;

    public PauseAfterIteration(long targetIteration) {
        this.targetIteration = targetIteration;
    }

    @Override
    public boolean shouldPause(OptiAlgo optiAlgo) {
        return optiAlgo.getIteration() == targetIteration;
    }

    @Override
    public boolean shouldKeepCriterion(OptiAlgo optiAlgo) {
        return optiAlgo.getIteration() < targetIteration;
    }
}
