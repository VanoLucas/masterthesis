package com.vanolucas.cppsolver.old.v1.opti.pause;

import com.vanolucas.cppsolver.old.v1.opti.algo.OptiAlgo;

public interface PauseCriterion {

    /**
     * @return True if we want the provided opti algo to pause at this point.
     */
    boolean shouldPause(OptiAlgo optiAlgo);

    /**
     * Tells whether the optimization algo should keep
     * checking for this pause criterion or just get rid of it.
     *
     * @return True if we want this pause criterion to stay.
     */
    default boolean shouldKeepCriterion(OptiAlgo optiAlgo) {
        return !shouldPause(optiAlgo);
    }

    /**
     * Apply (add) this pause criterion to the provided opti algo.
     */
    default void applyTo(OptiAlgo optiAlgo) {
        optiAlgo.addPauseCriterion(this);
    }
}
