package com.vanolucas.cppsolver.old.v1.opti.pause;

import com.vanolucas.cppsolver.old.v1.opti.algo.OptiAlgo;
import com.vanolucas.cppsolver.old.v1.opti.quality.Quality;

public class PauseQualityReached implements PauseCriterion {

    private Quality targetQuality;

    public PauseQualityReached(Quality targetQuality) {
        this.targetQuality = targetQuality;
    }

    @Override
    public boolean shouldPause(OptiAlgo optiAlgo) {
        if (optiAlgo.getBestQuality() == null) {
            return false;
        }
        return optiAlgo.getBestQuality().isBetterOrEquivalentTo(targetQuality);
    }
}
