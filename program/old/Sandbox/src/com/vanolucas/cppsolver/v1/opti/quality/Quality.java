package com.vanolucas.cppsolver.old.v1.opti.quality;

/**
 * Represents the quality of a solution.
 * E.g. a quality could be a score, fitness or cost.
 */
public abstract class Quality implements Comparable<Quality>, Cloneable {

    /**
     * Value of this quality.
     */
    protected double value;

    public Quality(double value) {
        this.value = value;
    }

    public abstract boolean isBetterThan(Quality o);

    public boolean isEquivalentTo(Quality o) {
        return value == o.value;
    }

    public boolean isBetterOrEquivalentTo(Quality o) {
        return isBetterThan(o) || isEquivalentTo(o);
    }

    @Override
    public int compareTo(Quality o) {
        if (isBetterThan(o)) {
            return 1;
        } else if (isEquivalentTo(o)) {
            return 0;
        } else {
            return -1;
        }
    }

    @Override
    public Quality clone() throws CloneNotSupportedException {
        Quality cloned = (Quality) super.clone();
        cloned.value = value;
        return cloned;
    }

    @Override
    public boolean equals(Object other) {
        Quality o = (Quality) other;
        return isEquivalentTo(o);
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
