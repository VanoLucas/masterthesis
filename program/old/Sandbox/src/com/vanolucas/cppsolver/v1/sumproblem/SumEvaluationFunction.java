package com.vanolucas.cppsolver.old.v1.sumproblem;

import com.vanolucas.cppsolver.old.v1.opti.evaluation.EvaluationFunction;
import com.vanolucas.cppsolver.old.v1.opti.quality.Cost;
import com.vanolucas.cppsolver.old.v1.opti.quality.Quality;

public class SumEvaluationFunction implements EvaluationFunction {

    private final int target;

    public SumEvaluationFunction(int target) {
        this.target = target;
    }

    @Override
    public Quality evaluate(Object solution) {
        return evaluate((Integer) solution);
    }

    public Quality evaluate(Integer solution) {
        final int distance = Math.abs(solution - target);
        return new Cost(distance);
    }
}
