package com.vanolucas.cppsolver.old.v1.sumproblem;

import com.vanolucas.cppsolver.old.v1.opti.evaluation.GeneticIndividualEvaluator;

public class SumGeneticIndividualEvaluator extends GeneticIndividualEvaluator {
    public SumGeneticIndividualEvaluator(int target) {
        super(new SumGenotypeDecoder(), new SumPhenotypeDecoder(), new SumEvaluationFunction(target));
    }
}
