package com.vanolucas.cppsolver.old.v1.sumproblem;

import com.vanolucas.cppsolver.old.v1.genetic.Phenotype;
import com.vanolucas.cppsolver.old.v1.genetic.PhenotypeDecoder;

public class SumPhenotypeDecoder implements PhenotypeDecoder {
    @Override
    public Object decode(Phenotype phenotype) {
        return decode((SumPhenotype) phenotype);
    }

    public Integer decode(SumPhenotype phenotype) {
        return phenotype.sum();
    }
}
