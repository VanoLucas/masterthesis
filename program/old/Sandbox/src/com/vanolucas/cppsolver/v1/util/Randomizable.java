package com.vanolucas.cppsolver.old.v1.util;

import com.vanolucas.cppsolver.old.v1.util.math.Rand;

/**
 * Ability to be randomized.
 */
public interface Randomizable {

    /**
     * Randomize this using the provided random numbers provider.
     * @param rng The random number generator to use.
     */
    void randomize(Rand rng);
}
