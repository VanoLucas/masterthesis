package com.vanolucas.cppsolver.old.v10.clone;

public interface Cloneable<T> {
    T newClone();
}
