package com.vanolucas.cppsolver.old.v10.clone;

import com.vanolucas.cppsolver.old.v10.genome.Genome;
import com.vanolucas.cppsolver.old.v10.solution.GeneticSolution;
import com.vanolucas.cppsolver.old.v10.solution.Solution;

import java.util.List;

public class GeneticSolutionCloner<T extends Genome> implements SolutionCloner {
    private final Cloner<T> genotypeCloner;
    private final List<Cloner> intermediateCloners;
    private final Cloner phenotypeCloner;

    public GeneticSolutionCloner() {
        this(true, null, null);
    }

    public GeneticSolutionCloner(boolean useDefaultGenotypeCloner, List<Cloner> intermediateCloners, Cloner phenotypeCloner) {
        this(
                useDefaultGenotypeCloner ? genotype -> (T) genotype.newClone() : null,
                intermediateCloners,
                phenotypeCloner
        );
    }

    public GeneticSolutionCloner(Cloner<T> genotypeCloner, List<Cloner> intermediateCloners, Cloner phenotypeCloner) {
        this.genotypeCloner = genotypeCloner;
        this.intermediateCloners = intermediateCloners;
        this.phenotypeCloner = phenotypeCloner;
    }

    @Override
    public GeneticSolution clone(Solution solution) {
        return clone((GeneticSolution) solution);
    }

    public GeneticSolution clone(GeneticSolution solution) {
        // clone genotype if needed
        final Genome clonedGenotype;
        if (genotypeCloner != null) {
            clonedGenotype = solution.cloneGenotypeUsing(genotypeCloner);
        } else {
            clonedGenotype = null;
        }

        // clone intermediate representations if needed
        final List<Object> clonedIntermediates;
        if (intermediateCloners != null) {
            clonedIntermediates = solution.cloneIntermediatesUsing(intermediateCloners);
        } else {
            clonedIntermediates = null;
        }

        // clone phenotype if needed
        final Object clonedPhenotype;
        if (phenotypeCloner != null) {
            clonedPhenotype = solution.clonePhenotypeUsing(phenotypeCloner);
        } else {
            clonedPhenotype = null;
        }

        // create the new genetic solution with its cloned attributes
        return new GeneticSolution(
                clonedGenotype,
                clonedIntermediates,
                clonedPhenotype
        );
    }
}
