package com.vanolucas.cppsolver.old.v10.clone;

import com.vanolucas.cppsolver.old.v10.solution.SimpleSolution;
import com.vanolucas.cppsolver.old.v10.solution.Solution;

public class SimpleSolutionCloner implements SolutionCloner {
    private final Cloner actualSolutionCloner;

    public SimpleSolutionCloner(Cloner actualSolutionCloner) {
        this.actualSolutionCloner = actualSolutionCloner;
    }

    @Override
    public SimpleSolution clone(Solution solution) {
        return clone((SimpleSolution) solution);
    }

    public SimpleSolution clone(SimpleSolution solution) {
        return new SimpleSolution(
                solution.cloneActualSolutionUsing(actualSolutionCloner)
        );
    }
}
