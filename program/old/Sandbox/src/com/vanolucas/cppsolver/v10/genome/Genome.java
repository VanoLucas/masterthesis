package com.vanolucas.cppsolver.old.v10.genome;

import com.vanolucas.cppsolver.old.v10.clone.Cloneable;
import com.vanolucas.cppsolver.old.v10.random.Rand;
import com.vanolucas.cppsolver.old.v10.random.Randomizable;

public interface Genome extends Randomizable, Cloneable<Genome> {

    @Override
    default void randomize(Rand rand) {
        throw new UnsupportedOperationException(String.format("Randomize not supported in %s.",
                getClass().getSimpleName()
        ));
    }

    @Override
    default Genome newClone() {
        throw new UnsupportedOperationException(String.format("Clone not supported in %s.",
                getClass().getSimpleName()
        ));
    }
}
