package com.vanolucas.cppsolver.old.v10.random;

import com.vanolucas.cppsolver.old.v10.genome.Genome;
import com.vanolucas.cppsolver.old.v10.solution.GeneticSolution;
import com.vanolucas.cppsolver.old.v10.solution.Solution;

public class GeneticSolutionRandomizer<T extends Genome> implements SolutionRandomizer {
    private final Randomizer<T> genotypeRandomizer;

    public GeneticSolutionRandomizer(Rand rand) {
        this(genotype -> genotype.randomize(rand));
    }

    public GeneticSolutionRandomizer(Randomizer<T> genotypeRandomizer) {
        this.genotypeRandomizer = genotypeRandomizer;
    }

    @Override
    public void randomize(Solution solution) {
        randomize((GeneticSolution) solution);
    }

    public void randomize(GeneticSolution solution) {
        solution.randomizeGenotypeUsing(genotypeRandomizer);
    }
}
