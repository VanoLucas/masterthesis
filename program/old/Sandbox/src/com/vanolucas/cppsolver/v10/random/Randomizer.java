package com.vanolucas.cppsolver.old.v10.random;

/**
 * Function that randomizes the provided object.
 *
 * @param <T> Type of objects to randomize.
 */
public interface Randomizer<T> {
    void randomize(T obj);
}
