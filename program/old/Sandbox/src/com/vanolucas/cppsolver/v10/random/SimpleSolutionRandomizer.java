package com.vanolucas.cppsolver.old.v10.random;

import com.vanolucas.cppsolver.old.v10.solution.SimpleSolution;
import com.vanolucas.cppsolver.old.v10.solution.Solution;

public class SimpleSolutionRandomizer implements SolutionRandomizer {
    private final Randomizer actualSolutionRandomizer;

    public SimpleSolutionRandomizer(Randomizer actualSolutionRandomizer) {
        this.actualSolutionRandomizer = actualSolutionRandomizer;
    }

    @Override
    public void randomize(Solution solution) {
        randomize((SimpleSolution) solution);
    }

    public void randomize(SimpleSolution solution) {
        solution.randomizeActualSolutionUsing(actualSolutionRandomizer);
    }
}
