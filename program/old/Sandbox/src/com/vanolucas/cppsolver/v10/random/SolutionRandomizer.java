package com.vanolucas.cppsolver.old.v10.random;

import com.vanolucas.cppsolver.old.v10.solution.Solution;

public interface SolutionRandomizer extends Randomizer<Solution> {
    @Override
    void randomize(Solution solution);
}
