package com.vanolucas.cppsolver.old.v10.solution;

import com.vanolucas.cppsolver.old.v10.clone.Cloner;
import com.vanolucas.cppsolver.old.v10.genome.Genome;
import com.vanolucas.cppsolver.old.v10.random.Rand;
import com.vanolucas.cppsolver.old.v10.random.Randomizer;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class GeneticSolution implements Solution {
    private final Genome genotype;
    private final List<Object> intermediates;
    private final Object phenotype;

    public GeneticSolution(Genome genotype) {
        this(genotype, null);
    }

    public GeneticSolution(Genome genotype, Object phenotype) {
        this(genotype, null, phenotype);
    }

    public GeneticSolution(Genome genotype, List<Object> intermediates, Object phenotype) {
        this.genotype = genotype;
        this.intermediates = intermediates;
        this.phenotype = phenotype;
    }

    public void randomizeGenotype(Rand rand) {
        genotype.randomize(rand);
    }

    public <T extends Genome> void randomizeGenotypeUsing(Randomizer<T> genotypeRandomizer) {
        genotypeRandomizer.randomize((T) genotype);
    }

    public Genome cloneGenotype() {
        return genotype.newClone();
    }

    public <T extends Genome> T cloneGenotypeUsing(Cloner<T> genotypeCloner) {
        return (T) genotypeCloner.clone((T) genotype);
    }

    public List<Object> cloneIntermediatesUsing(List<Cloner> intermediateCloners) {
        return IntStream.range(0, intermediateCloners.size())
                .mapToObj(i -> cloneIntermediateUsing(i, intermediateCloners.get(i)))
                .collect(Collectors.toList());
    }

    public Object cloneIntermediateUsing(int intermediateIndex, Cloner intermediateCloner) {
        return intermediateCloner.clone(intermediates.get(intermediateIndex));
    }

    public Object clonePhenotypeUsing(Cloner phenotypeCloner) {
        return phenotypeCloner.clone(phenotype);
    }

    @Override
    public String toString() {
        if (intermediates != null) {
            final String strIntermediates = IntStream.range(0, intermediates.size())
                    .mapToObj(i -> {
                        if (intermediates.size() >= 2) {
                            return String.format("Intermediate %d:%n" +
                                            "%s",
                                    i,
                                    intermediates.get(i)
                            );
                        } else {
                            return String.format("Intermediate:%n" +
                                            "%s",
                                    intermediates.get(i)
                            );
                        }
                    })
                    .collect(Collectors.joining(String.format("%n")));
            return String.format("Genotype:%n" +
                            "%s%n" +
                            "%s%n" +
                            "Phenotype:%n" +
                            "%s",
                    genotype,
                    strIntermediates,
                    phenotype
            );
        } else {
            return String.format("Genotype:%n" +
                            "%s%n" +
                            "Phenotype:%n" +
                            "%s",
                    genotype,
                    phenotype
            );
        }
    }
}
