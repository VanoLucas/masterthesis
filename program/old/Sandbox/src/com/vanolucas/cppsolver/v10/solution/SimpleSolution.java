package com.vanolucas.cppsolver.old.v10.solution;

import com.vanolucas.cppsolver.old.v10.clone.Cloner;
import com.vanolucas.cppsolver.old.v10.random.Randomizer;

public class SimpleSolution implements Solution {
    private final Object actualSolution;

    public SimpleSolution(Object actualSolution) {
        this.actualSolution = actualSolution;
    }

    public void randomizeActualSolutionUsing(Randomizer actualSolutionRandomizer) {
        actualSolutionRandomizer.randomize(actualSolution);
    }

    public Object cloneActualSolutionUsing(Cloner actualSolutionCloner) {
        return actualSolutionCloner.clone(actualSolution);
    }

    @Override
    public String toString() {
        return String.format("%s", actualSolution);
    }
}
