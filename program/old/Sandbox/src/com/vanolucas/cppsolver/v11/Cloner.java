package com.vanolucas.cppsolver.old.v11;

public interface Cloner<T> {
    T clone(T obj);
}
