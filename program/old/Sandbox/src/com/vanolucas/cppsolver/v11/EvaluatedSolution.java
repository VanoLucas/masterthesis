package com.vanolucas.cppsolver.old.v11;

public class EvaluatedSolution {
    private final Solution solution;
    private final Quality quality;

    public EvaluatedSolution(Solution solution, Quality quality) {
        this.solution = solution;
        this.quality = quality;
    }

    public RelativeQuality relativeTo(Quality quality) {
        return this.quality.relativeTo(quality);
    }
}
