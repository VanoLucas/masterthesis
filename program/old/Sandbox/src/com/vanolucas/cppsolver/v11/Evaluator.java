package com.vanolucas.cppsolver.old.v11;

public interface Evaluator<T> {
    Quality evaluate(T obj);
}
