package com.vanolucas.cppsolver.old.v11;

public class OptiAlgo {
    private long iteration = 0L;
    private final OptiStep step;
    private Quality bestQuality;

    public OptiAlgo(OptiStep step) {
        this.step = step;
    }

    public void run() {
        while (true) {
            iteration++;
            step.runStepForAlgo(this, iteration);
        }
    }
}
