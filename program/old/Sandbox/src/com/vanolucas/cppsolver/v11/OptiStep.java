package com.vanolucas.cppsolver.old.v11;

public abstract class OptiStep {
    private OptiAlgo algo;

    void runStepForAlgo(OptiAlgo algo, long iteration) {
        this.algo = algo;
        runStep(iteration);
    }

    abstract void runStep(long iteration);
}
