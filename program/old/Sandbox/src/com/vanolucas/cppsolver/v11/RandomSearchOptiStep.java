package com.vanolucas.cppsolver.old.v11;

public class RandomSearchOptiStep {
    private final Solution solution;
    private final SolutionRandomizer randomizer;
    private final SolutionEvaluator evaluator;

    public RandomSearchOptiStep(Solution solution, SolutionRandomizer randomizer, SolutionEvaluator evaluator) {
        this.solution = solution;
        this.randomizer = randomizer;
        this.evaluator = evaluator;
    }

    public void runStep(long iteration) {
        // randomize solution
        solution.randomizeUsing(randomizer);
        // evaluate solution
        final Quality quality = solution.evaluateUsing(evaluator);
    }
}
