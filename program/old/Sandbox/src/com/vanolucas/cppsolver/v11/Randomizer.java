package com.vanolucas.cppsolver.old.v11;

public interface Randomizer<T> {
    void randomize(T obj);
}
