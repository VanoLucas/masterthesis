package com.vanolucas.cppsolver.old.v11;

public enum RelativeQuality {
    BETTER, EQUIVALENT, WORSE
}
