package com.vanolucas.cppsolver.old.v11;

public interface Solution {
    default Quality evaluateUsing(SolutionEvaluator evaluator) {
        return evaluator.evaluate(this);
    }

    default void randomizeUsing(SolutionRandomizer randomizer) {
        randomizer.randomize(this);
    }

    default Solution cloneUsing(SolutionCloner cloner) {
        return cloner.clone(this);
    }
}
