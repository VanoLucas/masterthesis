package com.vanolucas.cppsolver.v12.algo;

import com.vanolucas.cppsolver.v12.eval.Evaluator;
import com.vanolucas.cppsolver.v12.evolutionary.Breeder;
import com.vanolucas.cppsolver.v12.evolutionary.MatesSelector;
import com.vanolucas.cppsolver.v12.evolutionary.PopulationMerger;
import com.vanolucas.cppsolver.v12.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.v12.solution.Solution;

import java.util.List;
import java.util.stream.Collectors;

public class EvolutionaryOptiStep extends OptiStep {
    private List<EvaluatedSolution> population;
    private final MatesSelector matesSelector;
    private final Breeder breeder;
    private final PopulationMerger merger;

    public EvolutionaryOptiStep(Opti opti, List<Solution> initialPopulation, MatesSelector matesSelector, Breeder breeder, PopulationMerger merger) {
        super(opti);
        this.population = evaluate(initialPopulation);
        this.matesSelector = matesSelector;
        this.breeder = breeder;
        this.merger = merger;
    }

    public EvolutionaryOptiStep(Opti opti, Evaluator evaluator, List<Solution> initialPopulation, MatesSelector matesSelector, Breeder breeder, PopulationMerger merger) {
        super(opti, evaluator);
        this.population = evaluate(initialPopulation);
        this.matesSelector = matesSelector;
        this.breeder = breeder;
        this.merger = merger;
    }

    @Override
    void runStep(long iteration) {
        // select individuals for reproduction (make mating groups)
        List<List<EvaluatedSolution>> groups = matesSelector.selectMates(population);
        // breed to give birth to offspring
        List<Solution> offspring = groups.stream()
                .flatMap(parents -> breeder.breed(parents).stream())
                .collect(Collectors.toList());
        // evaluate new individuals
        List<EvaluatedSolution> evaluatedOffspring = evaluate(offspring);
        // update population
        population = merger.merge(population, evaluatedOffspring);
    }
}
