package com.vanolucas.cppsolver.v12.algo;

import com.vanolucas.cppsolver.v12.algo.listener.*;
import com.vanolucas.cppsolver.v12.eval.Evaluator;
import com.vanolucas.cppsolver.v12.quality.Quality;
import com.vanolucas.cppsolver.v12.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.v12.solution.Solution;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Iterative optimization algorithm that uses evaluation of solutions.
 * to do steepest ascent hill climbing opti step
 * to do evolutionary opti step
 * to do cpp representation classes (Genome, CppSolution, Plate, Slot)
 */
public class Opti {
    private long iteration = 0L;
    private OptiStep step;
    private Evaluator evaluator;
    private final AtomicBoolean pauseBeforeNextIteration = new AtomicBoolean(false);

    private final List<OnRunStartListener> onRunStartListeners = new ArrayList<>(2);
    private final List<OnNewIterationListener> onNewIterationListeners = new ArrayList<>(2);
    private final List<OnEvaluatedListener> onEvaluatedListeners = new ArrayList<>(2);
    private final List<OnIterationEndListener> onIterationEndListeners = new ArrayList<>(2);
    private final List<OnPauseListener> onPauseListeners = new ArrayList<>(2);
    private BetterSolutionPrinter betterSolutionPrinter;

    void setStep(OptiStep step) {
        this.step = step;
    }

    void setEvaluator(Evaluator evaluator) {
        this.evaluator = evaluator;
    }

    public void run() {
        pauseBeforeNextIteration.set(false);

        // notify listeners that we start running
        onRunStartListeners.forEach(l -> l.onRunStart(iteration + 1L));

        // loop until asked to pause
        while (!pauseBeforeNextIteration.get()) {
            // start new iteration
            iteration++;
            // notify listeners that we start a new iteration
            onNewIterationListeners.forEach(l -> l.onNewIteration(iteration));
            // run the iteration
            step.runStep(iteration);
            // notify listeners that we finished this iteration
            onIterationEndListeners.forEach(l -> l.onIterationEnd(iteration));
        }

        // notify listeners that we pause
        onPauseListeners.forEach(l -> l.onPause(iteration));
    }

    public void runSingleIteration() {
        // notify listeners that we start running
        onRunStartListeners.forEach(l -> l.onRunStart(iteration + 1L));

        // start new iteration
        iteration++;
        // notify listeners that we start a new iteration
        onNewIterationListeners.forEach(l -> l.onNewIteration(iteration));

        // run the iteration
        step.runStep(iteration);

        // notify listeners that we finished this iteration
        onIterationEndListeners.forEach(l -> l.onIterationEnd(iteration));
        // notify listeners that we pause
        onPauseListeners.forEach(l -> l.onPause(iteration));
    }

    public void resetIterationNumber() {
        this.iteration = 0L;
    }

    public void pauseBeforeNextIteration() {
        pauseBeforeNextIteration.set(true);
    }

    List<EvaluatedSolution> evaluate(List<Solution> solutions) {
        // use the evaluator to evaluate all the provided solutions
        final List<EvaluatedSolution> evaluatedSolutions = evaluator.evaluate(solutions);
        // notify listeners that we evaluated each solution
        evaluatedSolutions.forEach(evaluatedSolution ->
                onEvaluatedListeners.forEach(l ->
                        l.onEvaluated(evaluatedSolution, iteration)
                )
        );
        return evaluatedSolutions;
    }

    EvaluatedSolution evaluate(Solution solution) {
        // use the evaluator to evaluate the provided solution
        final EvaluatedSolution evaluatedSolution = evaluator.evaluate(solution);
        // notify listeners that we evaluated this solution
        onEvaluatedListeners.forEach(l -> l.onEvaluated(evaluatedSolution, iteration));
        return evaluatedSolution;
    }

    public void enablePrintNewBetterSolutions() {
        if (betterSolutionPrinter == null) {
            betterSolutionPrinter = new BetterSolutionPrinter(this);
        }
    }

    public void enablePrintNewBetterSolutions(Quality minQuality) {
        disablePrintNewBetterSolutions();
        betterSolutionPrinter = new BetterSolutionPrinter(this, minQuality);
    }

    public void disablePrintNewBetterSolutions() {
        if (betterSolutionPrinter != null) {
            betterSolutionPrinter.disable();
            betterSolutionPrinter = null;
        }
    }

    public void addListener(OnRunStartListener listener) {
        addOnRunStartListener(listener);
    }

    public void addListener(OnNewIterationListener listener) {
        addOnNewIterationListener(listener);
    }

    public void addListener(OnEvaluatedListener listener) {
        addOnEvaluatedListener(listener);
    }

    public void addListener(OnIterationEndListener listener) {
        addOnIterationEndListener(listener);
    }

    public void addListener(OnPauseListener listener) {
        addOnPauseListener(listener);
    }

    public void addOnRunStartListener(OnRunStartListener listener) {
        onRunStartListeners.add(listener);
    }

    public void addOnNewIterationListener(OnNewIterationListener listener) {
        onNewIterationListeners.add(listener);
    }

    public void addOnEvaluatedListener(OnEvaluatedListener listener) {
        onEvaluatedListeners.add(listener);
    }

    public void addOnIterationEndListener(OnIterationEndListener listener) {
        onIterationEndListeners.add(listener);
    }

    public void addOnPauseListener(OnPauseListener listener) {
        onPauseListeners.add(listener);
    }

    public void removeListener(OnRunStartListener listener) {
        onRunStartListeners.remove(listener);
    }

    public void removeListener(OnNewIterationListener listener) {
        onNewIterationListeners.remove(listener);
    }

    public void removeListener(OnEvaluatedListener listener) {
        onEvaluatedListeners.remove(listener);
    }

    public void removeListener(OnIterationEndListener listener) {
        onIterationEndListeners.remove(listener);
    }

    public void removeListener(OnPauseListener listener) {
        onPauseListeners.remove(listener);
    }
}
