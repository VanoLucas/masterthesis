package com.vanolucas.cppsolver.v12.algo;

import com.vanolucas.cppsolver.v12.eval.Evaluator;
import com.vanolucas.cppsolver.v12.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.v12.solution.Solution;

import java.util.List;

/**
 * Base class for an iterative optimization algorithm implementation.
 */
public abstract class OptiStep {
    private final Opti opti;

    public OptiStep(Opti opti) {
        this.opti = opti;
        this.opti.setStep(this);
    }

    public OptiStep(Opti opti, Evaluator evaluator) {
        this.opti = opti;
        this.opti.setStep(this);
        this.opti.setEvaluator(evaluator);
    }

    public Opti getOpti() {
        return opti;
    }

    abstract void runStep(long iteration);

    protected List<EvaluatedSolution> evaluate(List<Solution> solutions) {
        return opti.evaluate(solutions);
    }

    protected EvaluatedSolution evaluate(Solution solution) {
        return opti.evaluate(solution);
    }
}
