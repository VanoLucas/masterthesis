package com.vanolucas.cppsolver.v12.algo;

import com.vanolucas.cppsolver.v12.eval.Evaluator;
import com.vanolucas.cppsolver.v12.mutation.SolutionMutator;
import com.vanolucas.cppsolver.v12.processor.SolutionProcessor;
import com.vanolucas.cppsolver.v12.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.v12.solution.Solution;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ParallelSingleMutationOptiStep extends OptiStep {
    private final List<Solution> solutions;
    private final List<SolutionMutator> mutators;
    private final SolutionProcessor<EvaluatedSolution> processor;

    public ParallelSingleMutationOptiStep(Opti opti,
                                          List<Solution> initialSolutions,
                                          List<SolutionMutator> mutators,
                                          SolutionProcessor<EvaluatedSolution> processor) {
        super(opti);
        this.solutions = initialSolutions;
        this.mutators = mutators;
        this.processor = processor;
    }

    public ParallelSingleMutationOptiStep(Opti opti,
                                          Evaluator evaluator,
                                          List<Solution> initialSolutions,
                                          List<SolutionMutator> mutators,
                                          SolutionProcessor<EvaluatedSolution> processor) {
        super(opti, evaluator);
        this.solutions = initialSolutions;
        this.mutators = mutators;
        this.processor = processor;
    }

    @Override
    void runStep(long iteration) {
        final List<Function<Solution, EvaluatedSolution>> tasks = mutators.stream()
                .map(mutator ->
                        (Function<Solution, EvaluatedSolution>) solution -> {
                            // mutate the solution
                            solution.mutateUsing(mutator);
                            // evaluate the solution
                            return evaluate(solution);
                        }
                )
                .collect(Collectors.toList());
        processor.runTasks(
                tasks,
                solutions
        );
    }
}
