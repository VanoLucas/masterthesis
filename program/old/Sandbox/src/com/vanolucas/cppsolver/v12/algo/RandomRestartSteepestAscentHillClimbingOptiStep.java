package com.vanolucas.cppsolver.v12.algo;

import com.vanolucas.cppsolver.v12.clone.SolutionCloner;
import com.vanolucas.cppsolver.v12.eval.Evaluator;
import com.vanolucas.cppsolver.v12.mutation.SolutionMutator;
import com.vanolucas.cppsolver.v12.rand.SolutionRandomizer;
import com.vanolucas.cppsolver.v12.solution.Solution;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;

public class RandomRestartSteepestAscentHillClimbingOptiStep extends SteepestAscentHillClimbingOptiStep {
    private final SolutionRandomizer randomizer;
    private final AtomicBoolean randomRestart = new AtomicBoolean(false);

    public RandomRestartSteepestAscentHillClimbingOptiStep(Opti opti, Solution initialSolution, SolutionCloner cloner, List<SolutionMutator> mutators, SolutionRandomizer randomizer) {
        super(opti, initialSolution, cloner, mutators);
        this.randomizer = randomizer;
    }

    public RandomRestartSteepestAscentHillClimbingOptiStep(Opti opti, Solution initialSolution, SolutionCloner cloner, List<SolutionMutator> mutators, SolutionRandomizer randomizer, int nbThreads) {
        super(opti, initialSolution, cloner, mutators, nbThreads);
        this.randomizer = randomizer;
    }

    public RandomRestartSteepestAscentHillClimbingOptiStep(Opti opti, Solution initialSolution, SolutionCloner cloner, List<SolutionMutator> mutators, SolutionRandomizer randomizer, ExecutorService executor) {
        super(opti, initialSolution, cloner, mutators, executor);
        this.randomizer = randomizer;
    }

    public RandomRestartSteepestAscentHillClimbingOptiStep(Opti opti, Evaluator evaluator, Solution initialSolution, SolutionCloner cloner, List<SolutionMutator> mutators, SolutionRandomizer randomizer) {
        super(opti, evaluator, initialSolution, cloner, mutators);
        this.randomizer = randomizer;
    }

    public RandomRestartSteepestAscentHillClimbingOptiStep(Opti opti, Evaluator evaluator, Solution initialSolution, SolutionCloner cloner, List<SolutionMutator> mutators, SolutionRandomizer randomizer, int nbThreads) {
        super(opti, evaluator, initialSolution, cloner, mutators, nbThreads);
        this.randomizer = randomizer;
    }

    public RandomRestartSteepestAscentHillClimbingOptiStep(Opti opti, Evaluator evaluator, Solution initialSolution, SolutionCloner cloner, List<SolutionMutator> mutators, SolutionRandomizer randomizer, ExecutorService executor) {
        super(opti, evaluator, initialSolution, cloner, mutators, executor);
        this.randomizer = randomizer;
    }

    @Override
    void runStep(long iteration) {
        // randomize current solution if random restart asked
        synchronized (randomRestart) {
            if (randomRestart.get()) {
                randomizeCurrentSolutionUsing(randomizer);
                randomRestart.set(false);
            }
        }
        // carry on with executing the hill climbing step
        super.runStep(iteration);
    }

    public void randomRestartAtNextIteration() {
        synchronized (randomRestart) {
            randomRestart.set(true);
        }
    }
}
