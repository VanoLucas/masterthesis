package com.vanolucas.cppsolver.v12.algo;

import com.vanolucas.cppsolver.v12.eval.Evaluator;
import com.vanolucas.cppsolver.v12.eval.SolutionEvaluationFunction;
import com.vanolucas.cppsolver.v12.rand.SolutionRandomizer;
import com.vanolucas.cppsolver.v12.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.v12.solution.Solution;

public class RandomSearchOptiStep extends OptiStep {
    private final Solution solution;
    private final SolutionRandomizer randomizer;

    public RandomSearchOptiStep(Opti opti, SolutionEvaluationFunction solutionEvalFn, Solution initialSolution, SolutionRandomizer randomizer) {
        this(opti, new Evaluator(solutionEvalFn), initialSolution, randomizer);
    }

    public RandomSearchOptiStep(Opti opti, Evaluator evaluator, Solution initialSolution, SolutionRandomizer randomizer) {
        super(opti, evaluator);
        this.solution = initialSolution;
        this.randomizer = randomizer;
    }

    @Override
    void runStep(long iteration) {
        // randomize current solution
        solution.randomizeUsing(randomizer);
        // evaluate solution
        final EvaluatedSolution evaluatedSolution = evaluate(solution);
    }
}
