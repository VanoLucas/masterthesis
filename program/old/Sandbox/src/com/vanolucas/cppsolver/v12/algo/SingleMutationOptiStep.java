package com.vanolucas.cppsolver.v12.algo;

import com.vanolucas.cppsolver.v12.eval.Evaluator;
import com.vanolucas.cppsolver.v12.eval.SolutionEvaluationFunction;
import com.vanolucas.cppsolver.v12.mutation.SolutionMutator;
import com.vanolucas.cppsolver.v12.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.v12.solution.Solution;

public class SingleMutationOptiStep extends OptiStep {
    private final Solution solution;
    private final SolutionMutator mutator;

    public SingleMutationOptiStep(Opti opti, SolutionEvaluationFunction solutionEvalFn, Solution initialSolution, SolutionMutator mutator) {
        this(opti, new Evaluator(solutionEvalFn), initialSolution, mutator);
    }

    public SingleMutationOptiStep(Opti opti, Evaluator evaluator, Solution initialSolution, SolutionMutator mutator) {
        super(opti, evaluator);
        this.solution = initialSolution;
        this.mutator = mutator;
    }

    @Override
    void runStep(long iteration) {
        // mutate current solution
        solution.mutateUsing(mutator);
        // evaluate solution
        final EvaluatedSolution evaluatedSolution = evaluate(solution);
    }
}
