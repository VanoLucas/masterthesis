package com.vanolucas.cppsolver.v12.algo;

import com.vanolucas.cppsolver.v12.clone.SolutionCloner;
import com.vanolucas.cppsolver.v12.eval.Evaluator;
import com.vanolucas.cppsolver.v12.mutation.SolutionMutator;
import com.vanolucas.cppsolver.v12.rand.SolutionRandomizer;
import com.vanolucas.cppsolver.v12.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.v12.solution.Solution;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class SteepestAscentHillClimbingOptiStep extends OptiStep {
    private Solution currentSolution;
    private final SolutionCloner cloner;
    private List<SolutionMutator> mutators;
    private final ExecutorService executor;
    private List<Callable<EvaluatedSolution>> neighborTasks;

    public SteepestAscentHillClimbingOptiStep(Opti opti,
                                              Solution initialSolution,
                                              SolutionCloner cloner,
                                              List<SolutionMutator> mutators) {
        this(opti, initialSolution, cloner, mutators, Runtime.getRuntime().availableProcessors());
    }

    public SteepestAscentHillClimbingOptiStep(Opti opti,
                                              Solution initialSolution,
                                              SolutionCloner cloner,
                                              List<SolutionMutator> mutators,
                                              int nbThreads) {
        this(opti, initialSolution, cloner, mutators, Executors.newFixedThreadPool(nbThreads));
    }

    public SteepestAscentHillClimbingOptiStep(Opti opti,
                                              Solution initialSolution,
                                              SolutionCloner cloner,
                                              List<SolutionMutator> mutators,
                                              ExecutorService executor) {
        super(opti);
        this.currentSolution = initialSolution;
        this.cloner = cloner;
        setMutators(mutators);
        this.executor = executor;
    }

    public SteepestAscentHillClimbingOptiStep(Opti opti,
                                              Evaluator evaluator,
                                              Solution initialSolution,
                                              SolutionCloner cloner,
                                              List<SolutionMutator> mutators) {
        this(opti, evaluator, initialSolution, cloner, mutators, Runtime.getRuntime().availableProcessors());
    }

    public SteepestAscentHillClimbingOptiStep(Opti opti,
                                              Evaluator evaluator,
                                              Solution initialSolution,
                                              SolutionCloner cloner,
                                              List<SolutionMutator> mutators,
                                              int nbThreads) {
        this(opti, evaluator, initialSolution, cloner, mutators, Executors.newFixedThreadPool(nbThreads));
    }

    public SteepestAscentHillClimbingOptiStep(Opti opti,
                                              Evaluator evaluator,
                                              Solution initialSolution,
                                              SolutionCloner cloner,
                                              List<SolutionMutator> mutators,
                                              ExecutorService executor) {
        super(opti, evaluator);
        this.currentSolution = initialSolution;
        this.cloner = cloner;
        setMutators(mutators);
        this.executor = executor;
    }

    public void setMutators(List<SolutionMutator> mutators) {
        this.mutators = mutators;

        // create a task for each neighboring operator to process
        neighborTasks = this.mutators.stream()
                .map(this::newNeighborProcessingTask)
                .collect(Collectors.toList());
    }

    private Callable<EvaluatedSolution> newNeighborProcessingTask(SolutionMutator mutator) {
        return () -> {
            // create neighbor as a clone of the current solution
            final Solution neighbor = currentSolution.cloneUsing(cloner);
            // mutate neighbor
            neighbor.mutateUsing(mutator);
            // evaluate neighbor
            final EvaluatedSolution evaluatedNeighbor = evaluate(neighbor);
            return evaluatedNeighbor;
        };
    }

    @Override
    void runStep(long iteration) {
        // submit neighbor processing tasks to thread pool
        final List<Future<EvaluatedSolution>> futures = neighborTasks.stream()
                .map(executor::submit)
                .collect(Collectors.toList());
        // wait and collect all neighbors that are calculated and evaluated on the thread pool
        final List<EvaluatedSolution> evaluatedNeighbors = futures.stream()
                .map(future -> {
                    try {
                        EvaluatedSolution evaluatedSolution = future.get();
                        return evaluatedSolution;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        throw new IllegalStateException("Interrupted a neighbor processing thread.");
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                        throw new IllegalStateException("Error in a neighbor processing thread.");
                    }
                })
                .collect(Collectors.toList());
        // get the best neighbor
        final EvaluatedSolution bestNeighbor = evaluatedNeighbors.stream()
                .max(Comparator.naturalOrder())
                .orElseThrow(() -> new IllegalStateException("There is no neighbor. There must be at least one so we can move to it."));
        // move to best neighbor
        currentSolution = bestNeighbor.getSolution();
    }

    void randomizeCurrentSolutionUsing(SolutionRandomizer randomizer) {
        currentSolution.randomizeUsing(randomizer);
    }
}
