package com.vanolucas.cppsolver.v12.algo.listener;

import com.vanolucas.cppsolver.v12.algo.Opti;
import com.vanolucas.cppsolver.v12.quality.Quality;
import com.vanolucas.cppsolver.v12.solution.EvaluatedSolution;

public class BetterSolutionPrinter implements OnEvaluatedListener {
    private final Opti opti;
    private final Quality minQuality;
    private Quality best;

    public BetterSolutionPrinter(Opti opti) {
        this(opti, null);
    }

    public BetterSolutionPrinter(Opti opti, Quality minQuality) {
        this.opti = opti;
        this.minQuality = minQuality;
        this.opti.addOnEvaluatedListener(this);
    }

    public void disable() {
        opti.removeListener(this);
    }

    @Override
    public synchronized void onEvaluated(EvaluatedSolution solution, long iteration) {
        if (best == null || solution.isBetterThan(best)) {
            if (minQuality == null || solution.isBetterOrEquivalentTo(minQuality)) {
                System.out.println(String.format("%nNew better solution at iteration %d:%n%s",
                        iteration, solution
                ));
            }
            best = solution.getQuality();
        }
    }
}
