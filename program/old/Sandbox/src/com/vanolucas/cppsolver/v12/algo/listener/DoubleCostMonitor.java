package com.vanolucas.cppsolver.v12.algo.listener;

import com.vanolucas.cppsolver.v12.algo.Opti;
import com.vanolucas.cppsolver.v12.quality.DoubleCost;
import com.vanolucas.cppsolver.v12.quality.Quality;
import com.vanolucas.cppsolver.v12.solution.EvaluatedSolution;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.LongStream;

public class DoubleCostMonitor implements OnNewIterationListener, OnEvaluatedListener, OnIterationEndListener {
    private final Opti opti;
    private final List<Double> costsCurrentIter = new ArrayList<>(10);
    private final TreeMap<Long, Double> bestCostOfIter = new TreeMap<>();
    private final TreeMap<Long, Double> worstCostOfIter = new TreeMap<>();
    private final TreeMap<Long, Double> avgCostOfIter = new TreeMap<>();
    private final TreeMap<Long, Double> medianCostOfIter = new TreeMap<>();
    private final TreeMap<Long, Double> betterCost = new TreeMap<>();
    private Quality best;

    public DoubleCostMonitor(Opti opti) {
        this.opti = opti;
        this.opti.addOnNewIterationListener(this);
        this.opti.addOnEvaluatedListener(this);
        this.opti.addOnIterationEndListener(this);
    }

    public void stopMonitoring() {
        this.opti.removeListener((OnNewIterationListener) this);
        this.opti.removeListener((OnEvaluatedListener) this);
        this.opti.removeListener((OnIterationEndListener) this);
    }

    @Override
    public void onNewIteration(long iteration) {
        costsCurrentIter.clear();
    }

    @Override
    public void onIterationEnd(long iteration) {
        bestCostOfIter.put(iteration, costsCurrentIter.stream().mapToDouble(__ -> __).min().getAsDouble());
        worstCostOfIter.put(iteration, costsCurrentIter.stream().mapToDouble(__ -> __).max().getAsDouble());
        avgCostOfIter.put(iteration, costsCurrentIter.stream().mapToDouble(__ -> __).average().getAsDouble());
        medianCostOfIter.put(iteration, median(costsCurrentIter));
    }

    @Override
    public synchronized void onEvaluated(EvaluatedSolution solution, long iteration) {
        final double cost = ((DoubleCost) solution.getQuality()).getValue();
        costsCurrentIter.add(cost);
        // update best known quality
        if (best == null || solution.isBetterThan(best)) {
            best = solution.getQuality();
            betterCost.put(iteration, cost);
        }
    }

    private double median(List<Double> values) {
        final int size = values.size();
        final DoubleStream sorted = values.stream().mapToDouble(__ -> __).sorted();
        return size % 2 == 0 ?
                sorted.skip(size / 2 - 1).limit(2).average().getAsDouble() :
                sorted.skip(size / 2).findFirst().getAsDouble();
    }

    public String getStatsOfIter(long iteration) {
        return String.format("Iter: %12d\tBest: %f\tWorst: %f\tAvg: %f\tMed: %f",
                iteration,
                bestCostOfIter.get(iteration),
                worstCostOfIter.get(iteration),
                avgCostOfIter.get(iteration),
                medianCostOfIter.get(iteration)
        );
    }

    public String getImprovementHistory() {
        return betterCost.keySet().stream()
                .map(iteration -> String.format("%8d : %f", iteration, betterCost.get(iteration)))
                .collect(Collectors.joining(System.lineSeparator()));
    }

    public Map<Long, Double> getBestKnownCostPerIteration() {
        final long lastIteration = bestCostOfIter.lastKey();
        return LongStream.range(1L, lastIteration + 1L)
                .unordered()
                .boxed()
                .collect(Collectors.toMap(
                        iteration -> iteration,
                        iteration -> betterCost.floorEntry(iteration).getValue()
                ));
    }

    public void printStatsOfIter(long iteration) {
        System.out.println(getStatsOfIter(iteration));
    }

    public void printStatsOfEachIter(long modulo) {
        bestCostOfIter.keySet()
                .stream()
                .filter(iteration -> iteration % modulo == 0 || iteration == 1L)
                .forEach(this::printStatsOfIter);
    }

    public void printImprovementHistory() {
        System.out.println(getImprovementHistory());
    }

    public void plotImprovementHistory() {
        final List<Long> iterations = new ArrayList<>(betterCost.keySet());
        final List<Double> bestCosts = new ArrayList<>(betterCost.values());

        final XYChart chart = new XYChartBuilder()
                .width(800).height(600)
                .title("Best Known Cost at Iteration")
                .xAxisTitle("Iteration")
                .yAxisTitle("Cost")
                .build();

        chart.addSeries("Best Known Cost",
                iterations, bestCosts
        );

        chart.getStyler()
                .setXAxisLogarithmic(true);

        new SwingWrapper<XYChart>(chart).displayChart();
    }
}
