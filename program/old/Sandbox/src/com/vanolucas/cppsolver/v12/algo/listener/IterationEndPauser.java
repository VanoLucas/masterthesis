package com.vanolucas.cppsolver.v12.algo.listener;

import com.vanolucas.cppsolver.v12.algo.Opti;

public class IterationEndPauser implements OnIterationEndListener {
    private final Opti opti;
    private final long targetIteration;

    public IterationEndPauser(Opti opti, long targetIteration) {
        this.opti = opti;
        this.targetIteration = targetIteration;
        this.opti.addOnIterationEndListener(this);
    }

    public void disable() {
        opti.removeListener(this);
    }

    @Override
    public void onIterationEnd(long iteration) {
        if (iteration == targetIteration) {
            opti.pauseBeforeNextIteration();
        }
    }
}
