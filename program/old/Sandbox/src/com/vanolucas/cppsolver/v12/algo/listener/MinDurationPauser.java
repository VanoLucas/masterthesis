package com.vanolucas.cppsolver.v12.algo.listener;

import com.vanolucas.cppsolver.v12.algo.Opti;
import com.vanolucas.cppsolver.v12.solution.EvaluatedSolution;

import java.time.Duration;
import java.time.Instant;

public class MinDurationPauser implements OnRunStartListener, OnEvaluatedListener, OnPauseListener {
    private final Opti opti;
    private final Duration minDuration;
    private Instant timeRunStarted;
    private Duration totalRunDuration = Duration.ZERO;

    public MinDurationPauser(Opti opti, Duration minDuration) {
        this.opti = opti;
        this.minDuration = minDuration;
        this.opti.addOnRunStartListener(this);
        this.opti.addOnEvaluatedListener(this);
        this.opti.addOnPauseListener(this);
    }

    public void disable() {
        opti.removeListener((OnEvaluatedListener) this);
        opti.removeListener((OnRunStartListener) this);
        opti.removeListener((OnPauseListener) this);
    }

    @Override
    public void onRunStart(long iteration) {
        this.timeRunStarted = Instant.now();
    }

    @Override
    public void onEvaluated(EvaluatedSolution solution, long iteration) {
        final Duration runDuration = Duration.between(timeRunStarted, Instant.now());
        if (totalRunDuration.plus(runDuration).minus(minDuration).negated().isNegative()) {
            opti.pauseBeforeNextIteration();
        }
    }

    @Override
    public void onPause(long finishedIteration) {
        final Duration runDuration = Duration.between(timeRunStarted, Instant.now());
        totalRunDuration = totalRunDuration.plus(runDuration);
    }
}
