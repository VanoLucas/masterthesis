package com.vanolucas.cppsolver.v12.algo.listener;

import com.vanolucas.cppsolver.v12.algo.Opti;
import com.vanolucas.cppsolver.v12.solution.EvaluatedSolution;

/**
 * Pause the opti algo after having executed at least the min nb of evaluations.
 */
public class MinEvaluationsPauser implements OnEvaluatedListener {
    private final Opti opti;
    private final long minEvaluations;
    private long countEval = 0L;

    public MinEvaluationsPauser(Opti opti, long minEvaluations) {
        this.opti = opti;
        this.minEvaluations = minEvaluations;
        this.opti.addOnEvaluatedListener(this);
    }

    public void disable() {
        opti.removeListener(this);
    }

    @Override
    public synchronized void onEvaluated(EvaluatedSolution solution, long iteration) {
        countEval++;
        if (countEval >= minEvaluations) {
            opti.pauseBeforeNextIteration();
        }
    }
}
