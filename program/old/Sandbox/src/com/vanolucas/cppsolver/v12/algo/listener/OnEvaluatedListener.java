package com.vanolucas.cppsolver.v12.algo.listener;

import com.vanolucas.cppsolver.v12.solution.EvaluatedSolution;

public interface OnEvaluatedListener {
    void onEvaluated(EvaluatedSolution solution, long iteration);
}
