package com.vanolucas.cppsolver.v12.algo.listener;

public interface OnIterationEndListener {
    void onIterationEnd(long iteration);
}
