package com.vanolucas.cppsolver.v12.algo.listener;

public interface OnNewIterationListener {
    void onNewIteration(long iteration);
}
