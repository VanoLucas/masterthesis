package com.vanolucas.cppsolver.v12.algo.listener;

public interface OnPauseListener {
    void onPause(long finishedIteration);
}
