package com.vanolucas.cppsolver.v12.algo.listener;

public interface OnRunStartListener {
    void onRunStart(long iteration);
}
