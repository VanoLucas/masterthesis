package com.vanolucas.cppsolver.v12.clone;

import java.util.function.Function;

public interface Cloner<T> extends Function<T, T> {
    @Override
    default T apply(T obj) {
        try {
            return clone(obj);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            throw new UnsupportedOperationException("Clone not supported.");
        }
    }

    T clone(T obj) throws CloneNotSupportedException;
}
