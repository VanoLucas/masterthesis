package com.vanolucas.cppsolver.v12.clone;

import com.vanolucas.cppsolver.v12.genome.Genome;
import com.vanolucas.cppsolver.v12.solution.ComplexGeneticSolution;
import com.vanolucas.cppsolver.v12.solution.Solution;

public class ComplexGeneticSolutionCloner implements SolutionCloner {
    private final GenomeCloner genomeCloner;

    public ComplexGeneticSolutionCloner() {
        this(GenomeCloner.getDefault());
    }

    public ComplexGeneticSolutionCloner(GenomeCloner genomeCloner) {
        this.genomeCloner = genomeCloner;
    }

    @Override
    public ComplexGeneticSolution clone(Solution solution) throws CloneNotSupportedException {
        return clone((ComplexGeneticSolution) solution);
    }

    public ComplexGeneticSolution clone(ComplexGeneticSolution solution) throws CloneNotSupportedException {
        final Genome clonedGenome = solution.cloneGenomeUsing(genomeCloner);
        final int nbIntermediates = solution.getNbIntermediates();
        return new ComplexGeneticSolution(clonedGenome, nbIntermediates);
    }
}
