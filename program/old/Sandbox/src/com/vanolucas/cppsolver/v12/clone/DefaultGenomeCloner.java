package com.vanolucas.cppsolver.v12.clone;

import com.vanolucas.cppsolver.v12.genome.Genome;

public class DefaultGenomeCloner implements GenomeCloner {
    @Override
    public Genome clone(Genome genome) throws CloneNotSupportedException {
        return genome.clone();
    }
}
