package com.vanolucas.cppsolver.v12.clone;

import com.vanolucas.cppsolver.v12.genome.Genome;
import com.vanolucas.cppsolver.v12.solution.GeneticSolution;
import com.vanolucas.cppsolver.v12.solution.Solution;

public class GeneticSolutionCloner implements SolutionCloner {
    private final GenomeCloner genomeCloner;

    public GeneticSolutionCloner() {
        this(GenomeCloner.getDefault());
    }

    public GeneticSolutionCloner(GenomeCloner genomeCloner) {
        this.genomeCloner = genomeCloner;
    }

    @Override
    public GeneticSolution clone(Solution solution) throws CloneNotSupportedException {
        return clone((GeneticSolution) solution);
    }

    public GeneticSolution clone(GeneticSolution solution) throws CloneNotSupportedException {
        final Genome clonedGenome = solution.cloneGenomeUsing(genomeCloner);
        return new GeneticSolution(clonedGenome);
    }
}
