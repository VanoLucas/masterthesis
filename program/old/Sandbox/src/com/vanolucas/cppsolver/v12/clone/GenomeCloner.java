package com.vanolucas.cppsolver.v12.clone;

import com.vanolucas.cppsolver.v12.genome.Genome;

public interface GenomeCloner extends Cloner<Genome> {
    @Override
    Genome clone(Genome genome) throws CloneNotSupportedException;

    static GenomeCloner getDefault() {
        return new DefaultGenomeCloner();
    }
}
