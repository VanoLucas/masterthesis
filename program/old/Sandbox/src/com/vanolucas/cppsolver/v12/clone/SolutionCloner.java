package com.vanolucas.cppsolver.v12.clone;

import com.vanolucas.cppsolver.v12.solution.Solution;

public interface SolutionCloner extends Cloner<Solution> {
    @Override
    Solution clone(Solution solution) throws CloneNotSupportedException;
}
