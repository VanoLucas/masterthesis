package com.vanolucas.cppsolver.v12.eval;

import com.vanolucas.cppsolver.v12.quality.Quality;
import com.vanolucas.cppsolver.v12.solution.ComplexGeneticSolution;
import com.vanolucas.cppsolver.v12.solution.GeneticSolution;

import java.util.List;
import java.util.function.Function;

public class ComplexGeneticSolutionEvaluationFunction extends GeneticSolutionEvaluationFunction {
    private final List<Function> intermediateDecoders;

    public ComplexGeneticSolutionEvaluationFunction(Function genomeDecoder, List<Function> intermediateDecoders, EvaluationFunction evalFn) {
        super(genomeDecoder, evalFn);
        this.intermediateDecoders = intermediateDecoders;
    }

    @Override
    public Quality evaluate(GeneticSolution solution) {
        return evaluate((ComplexGeneticSolution) solution);
    }

    public Quality evaluate(ComplexGeneticSolution solution) {
        // first, decode the genome to the corresponding first intermediate representation
        solution.decodeGenomeUsing(genomeDecoder);
        // then. decode each intermediate representation to get the corresponding phenotype
        solution.decodeIntermediatesUsing(intermediateDecoders);
        // finally, evaluate the phenotype
        return solution.evaluatePhenotypeUsing(evalFn);
    }
}
