package com.vanolucas.cppsolver.v12.eval;

import com.vanolucas.cppsolver.v12.quality.Quality;

import java.util.function.Function;

public interface EvaluationFunction<TToEval, TQuality extends Quality> extends Function<TToEval, TQuality> {
    @Override
    default TQuality apply(TToEval obj) {
        return evaluate(obj);
    }

    TQuality evaluate(TToEval obj);
}
