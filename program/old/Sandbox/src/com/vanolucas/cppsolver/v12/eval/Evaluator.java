package com.vanolucas.cppsolver.v12.eval;

import com.vanolucas.cppsolver.v12.quality.Quality;
import com.vanolucas.cppsolver.v12.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.v12.solution.Solution;
import com.vanolucas.cppsolver.v12.processor.SolutionProcessor;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Service that is able to evaluate solutions and return the evaluated solutions with their quality values.
 */
public class Evaluator {
    private final SolutionProcessor<Quality> solutionProcessor;

    public Evaluator(SolutionEvaluationFunction solutionEvalFn) {
        this(new SolutionProcessor<>(solution -> solution.evaluateUsing(solutionEvalFn)));
    }

    public Evaluator(SolutionProcessor<Quality> solutionProcessor) {
        this.solutionProcessor = solutionProcessor;
    }

    public List<EvaluatedSolution> evaluate(List<Solution> solutions) {
        // calculate quality of each solution
        final List<Quality> qualities = solutionProcessor.process(solutions);
        // store solutions and qualities in EvaluatedSolution objects
        return IntStream.range(0, solutions.size())
                .mapToObj(i -> new EvaluatedSolution(solutions.get(i), qualities.get(i)))
                .collect(Collectors.toList());
    }

    public EvaluatedSolution evaluate(Solution solution) {
        // calculate quality of the solution and store it in an EvaluatedSolution object
        return new EvaluatedSolution(solution,
                calculateQuality(solution)
        );
    }

    public Quality calculateQuality(Solution solution) {
        return solutionProcessor.process(solution);
    }
}
