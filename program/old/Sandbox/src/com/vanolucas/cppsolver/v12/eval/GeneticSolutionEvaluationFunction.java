package com.vanolucas.cppsolver.v12.eval;

import com.vanolucas.cppsolver.v12.quality.Quality;
import com.vanolucas.cppsolver.v12.solution.GeneticSolution;
import com.vanolucas.cppsolver.v12.solution.Solution;

import java.util.function.Function;

public class GeneticSolutionEvaluationFunction implements SolutionEvaluationFunction {
    protected final Function genomeDecoder;
    protected final EvaluationFunction evalFn;

    public GeneticSolutionEvaluationFunction(Function genomeDecoder, EvaluationFunction evalFn) {
        this.genomeDecoder = genomeDecoder;
        this.evalFn = evalFn;
    }

    @Override
    public Quality evaluate(Solution solution) {
        return evaluate((GeneticSolution) solution);
    }

    public Quality evaluate(GeneticSolution solution) {
        // first, decode the genome to the corresponding phenotype
        solution.decodeGenomeUsing(genomeDecoder);
        // then, evaluate the phenotype
        return solution.evaluatePhenotypeUsing(evalFn);
    }
}
