package com.vanolucas.cppsolver.v12.eval;

import com.vanolucas.cppsolver.v12.quality.Quality;
import com.vanolucas.cppsolver.v12.processor.ParallelSolutionProcessor;
import com.vanolucas.cppsolver.v12.processor.SolutionProcessor;

import java.util.concurrent.ExecutorService;

/**
 * Evaluator that is able to evaluate multiple solutions in parallel using a thread pool.
 */
public class ParallelEvaluator extends Evaluator {
    public ParallelEvaluator(SolutionEvaluationFunction solutionEvalFn) {
        this(new ParallelSolutionProcessor<>(solutionEvalFn));
    }

    public ParallelEvaluator(SolutionEvaluationFunction solutionEvalFn, int maxThreads) {
        this(new ParallelSolutionProcessor<>(solutionEvalFn, maxThreads));
    }

    public ParallelEvaluator(SolutionEvaluationFunction solutionEvalFn, ExecutorService executor) {
        this(new ParallelSolutionProcessor<>(solutionEvalFn, executor));
    }

    public ParallelEvaluator(SolutionProcessor<Quality> solutionProcessor) {
        super(solutionProcessor);
    }
}
