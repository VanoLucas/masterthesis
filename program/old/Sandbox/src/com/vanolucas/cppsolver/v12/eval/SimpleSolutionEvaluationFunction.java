package com.vanolucas.cppsolver.v12.eval;

import com.vanolucas.cppsolver.v12.quality.Quality;
import com.vanolucas.cppsolver.v12.solution.SimpleSolution;
import com.vanolucas.cppsolver.v12.solution.Solution;

public class SimpleSolutionEvaluationFunction implements SolutionEvaluationFunction {
    private final EvaluationFunction actualSolutionEvalFn;

    public SimpleSolutionEvaluationFunction(EvaluationFunction actualSolutionEvalFn) {
        this.actualSolutionEvalFn = actualSolutionEvalFn;
    }

    @Override
    public Quality evaluate(Solution solution) {
        return evaluate((SimpleSolution) solution);
    }

    public Quality evaluate(SimpleSolution solution) {
        return solution.evaluateActualSolutionUsing(actualSolutionEvalFn);
    }
}
