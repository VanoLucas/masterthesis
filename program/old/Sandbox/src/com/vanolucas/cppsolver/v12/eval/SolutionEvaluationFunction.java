package com.vanolucas.cppsolver.v12.eval;

import com.vanolucas.cppsolver.v12.quality.Quality;
import com.vanolucas.cppsolver.v12.solution.Solution;

public interface SolutionEvaluationFunction extends EvaluationFunction<Solution, Quality> {
    @Override
    Quality evaluate(Solution solution);
}
