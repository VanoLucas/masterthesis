package com.vanolucas.cppsolver.v12.evolutionary;

import com.vanolucas.cppsolver.v12.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.v12.solution.Solution;

import java.util.List;

public interface Breeder {
    List<Solution> breed(List<EvaluatedSolution> parents);
}
