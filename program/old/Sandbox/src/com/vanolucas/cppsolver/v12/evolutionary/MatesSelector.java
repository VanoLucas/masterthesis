package com.vanolucas.cppsolver.v12.evolutionary;

import com.vanolucas.cppsolver.v12.solution.EvaluatedSolution;

import java.util.List;

public interface MatesSelector {
    List<List<EvaluatedSolution>> selectMates(List<EvaluatedSolution> population);
}
