package com.vanolucas.cppsolver.v12.evolutionary;

import com.vanolucas.cppsolver.v12.solution.EvaluatedSolution;

import java.util.List;

public interface PopulationMerger {
    List<EvaluatedSolution> merge(List<EvaluatedSolution> original, List<EvaluatedSolution> offspring);
}
