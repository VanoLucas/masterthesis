package com.vanolucas.cppsolver.v12.genome;

import com.vanolucas.cppsolver.v12.mutation.GenomeMutator;
import com.vanolucas.cppsolver.v12.rand.GenomeRandomizer;
import com.vanolucas.cppsolver.v12.rand.Rand;
import com.vanolucas.cppsolver.v12.rand.Randomizable;

/**
 * Genetic representation.
 */
public abstract class Genome implements Randomizable, Cloneable {
    public void mutateUsing(GenomeMutator mutator) {
        mutator.mutate(this);
    }

    public void randomizeUsing(GenomeRandomizer randomizer) {
        randomizer.randomize(this);
    }

    @Override
    public void randomize(Rand rand) {
        throw new UnsupportedOperationException("Randomize not implemented.");
    }

    @Override
    public Genome clone() throws CloneNotSupportedException {
        return (Genome) super.clone();
    }
}
