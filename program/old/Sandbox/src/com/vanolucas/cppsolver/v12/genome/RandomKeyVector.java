package com.vanolucas.cppsolver.v12.genome;

import com.vanolucas.cppsolver.v12.mutation.rkv.KeyRkvMutator;
import com.vanolucas.cppsolver.v12.mutation.rkv.KeysRkvMutator;
import com.vanolucas.cppsolver.v12.mutation.rkv.RkvMutator;
import com.vanolucas.cppsolver.v12.rand.Rand;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.IntConsumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

public class RandomKeyVector extends Genome {
    private ArrayList<Double> keys;

    public RandomKeyVector(int length) {
        this(length, 0d);
    }

    public RandomKeyVector(int length, Double initValue) {
        this(new ArrayList<>(
                Collections.nCopies(length, initValue)
        ));
    }

    public RandomKeyVector(List<Double> keys) {
        setKeyValues(keys);
    }

    public RandomKeyVector(ArrayList<Double> keys) {
        this.keys = keys;
    }

    public int length() {
        return keys.size();
    }

    public Double getKey(int index) {
        return keys.get(index);
    }

    private void setKey(int index, Double value) {
        keys.set(index, value);
    }

    private void setKeyValues(List<Double> keyValues) {
        this.keys = new ArrayList<>(keyValues);
    }

    public void copyKeyValuesTo(RandomKeyVector other) {
        other.setKeyValues(this.keys);
    }

    public void mutateUsing(RkvMutator mutator) {
        mutator.mutate(this);
    }

    public void mutateKeyUsing(KeyRkvMutator keyMutator, int keyIndex) {
        setKey(keyIndex,
                keyMutator.getNewKeyValue(keyIndex, this)
        );
    }

    public void mutateKeysUsing(KeysRkvMutator keysMutator) {
        setKeyValues(
                keysMutator.getNewKeyValues(this)
        );
    }

    public DoubleStream doubleStream() {
        return keys.stream().mapToDouble(__ -> __);
    }

    public void forEachIndex(IntConsumer consumer) {
        IntStream.range(0, keys.size())
                .forEach(consumer);
    }

    @Override
    public void randomize(Rand rand) {
        setKeyValues(
                rand.doubles01(length())
        );
    }

    public void randomizeKey(int index, Rand rand) {
        setKey(index, rand.double01());
    }

    @Override
    public RandomKeyVector clone() throws CloneNotSupportedException {
        final RandomKeyVector cloned = (RandomKeyVector) super.clone();
        cloned.setKeyValues(keys);
        return cloned;
    }

    @Override
    public String toString() {
        final String strKeys = keys.stream()
                .map(Object::toString)
                .collect(Collectors.joining(";"));
        return String.format("(%d genes) %s",
                length(),
                strKeys
        );
    }

    public static RandomKeyVector parse(String rkvStr) {
        Pattern pattern = Pattern.compile("(0\\.\\d+)");
        Matcher matcher = pattern.matcher(rkvStr);
        List<String> matches = new ArrayList<>();
        while (matcher.find()) {
            matches.add(matcher.group(1));
        }
        List<Double> keys = matches.stream()
                .map(Double::parseDouble)
                .collect(Collectors.toList());
        return new RandomKeyVector(keys);
    }
}
