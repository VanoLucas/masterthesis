package com.vanolucas.cppsolver.v12.mutation;

import com.vanolucas.cppsolver.v12.solution.Solution;

import java.util.Arrays;
import java.util.List;

public class CompositeSolutionMutator implements SolutionMutator {
    private final List<SolutionMutator> mutators;

    public CompositeSolutionMutator(SolutionMutator mutator1, SolutionMutator mutator2) {
        this(Arrays.asList(mutator1, mutator2));
    }

    public CompositeSolutionMutator(List<SolutionMutator> mutators) {
        this.mutators = mutators;
    }

    @Override
    public void mutate(Solution solution) {
        mutators.forEach(solution::mutateUsing);
    }
}
