package com.vanolucas.cppsolver.v12.mutation;

import com.vanolucas.cppsolver.v12.solution.GeneticSolution;
import com.vanolucas.cppsolver.v12.solution.Solution;

public class GeneticSolutionMutator implements SolutionMutator {
    private final GenomeMutator mutator;

    public GeneticSolutionMutator(GenomeMutator mutator) {
        this.mutator = mutator;
    }

    @Override
    public void mutate(Solution solution) {
        mutate((GeneticSolution) solution);
    }

    public void mutate(GeneticSolution solution) {
        solution.mutateGenomeUsing(mutator);
    }
}
