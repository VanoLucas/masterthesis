package com.vanolucas.cppsolver.v12.mutation;

import com.vanolucas.cppsolver.v12.genome.Genome;

public interface GenomeMutator extends Mutator<Genome> {
    @Override
    void mutate(Genome genome);
}
