package com.vanolucas.cppsolver.v12.mutation;

import java.util.function.Consumer;

public interface Mutator<T> extends Consumer<T> {
    @Override
    default void accept(T obj) {
        mutate(obj);
    }

    void mutate(T obj);
}
