package com.vanolucas.cppsolver.v12.mutation;

import com.vanolucas.cppsolver.v12.genome.Genome;
import com.vanolucas.cppsolver.v12.rand.GenomeRandomizer;
import com.vanolucas.cppsolver.v12.rand.Rand;

public class RandomizeGenomeMutator implements GenomeMutator {
    private final GenomeRandomizer randomizer;

    public RandomizeGenomeMutator(GenomeRandomizer randomizer) {
        this.randomizer = randomizer;
    }

    @Override
    public void mutate(Genome genome) {
        genome.randomizeUsing(randomizer);
    }

    public static RandomizeGenomeMutator getDefault(Rand rand) {
        return new RandomizeGenomeMutator(
                GenomeRandomizer.getDefault(rand)
        );
    }
}
