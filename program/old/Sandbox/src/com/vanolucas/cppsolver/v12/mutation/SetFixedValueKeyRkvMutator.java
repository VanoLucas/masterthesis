package com.vanolucas.cppsolver.v12.mutation;

import com.vanolucas.cppsolver.v12.genome.RandomKeyVector;
import com.vanolucas.cppsolver.v12.mutation.rkv.KeyRkvMutator;

public class SetFixedValueKeyRkvMutator extends KeyRkvMutator {
    private final Double newValue;

    public SetFixedValueKeyRkvMutator(Double newValue) {
        this.newValue = newValue;
    }

    public SetFixedValueKeyRkvMutator(Integer keyIndex, Double newValue) {
        super(keyIndex);
        this.newValue = newValue;
    }

    @Override
    public Double getNewKeyValue(int keyIndex, RandomKeyVector rkv) {
        return newValue;
    }
}
