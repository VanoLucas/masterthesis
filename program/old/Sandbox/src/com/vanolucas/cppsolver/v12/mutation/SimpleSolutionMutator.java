package com.vanolucas.cppsolver.v12.mutation;

import com.vanolucas.cppsolver.v12.solution.SimpleSolution;
import com.vanolucas.cppsolver.v12.solution.Solution;

public class SimpleSolutionMutator implements SolutionMutator {
    private final Mutator mutator;

    public SimpleSolutionMutator(Mutator mutator) {
        this.mutator = mutator;
    }

    @Override
    public void mutate(Solution solution) {
        mutate((SimpleSolution) solution);
    }

    public void mutate(SimpleSolution solution) {
        solution.mutateActualSolutionUsing(mutator);
    }
}
