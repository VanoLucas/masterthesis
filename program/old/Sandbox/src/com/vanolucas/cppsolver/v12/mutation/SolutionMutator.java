package com.vanolucas.cppsolver.v12.mutation;

import com.vanolucas.cppsolver.v12.solution.Solution;

public interface SolutionMutator extends Mutator<Solution> {
    @Override
    void mutate(Solution solution);
}
