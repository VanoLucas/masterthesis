package com.vanolucas.cppsolver.v12.mutation.rkv;

import com.vanolucas.cppsolver.v12.genome.RandomKeyVector;
import com.vanolucas.cppsolver.v12.rand.Rand;
import com.vanolucas.cppsolver.v12.util.BlockOnBoundsDoubleRangeGuard;
import com.vanolucas.cppsolver.v12.util.DoubleRangeGuard;

public class AddRandValueKeyRkvMutator extends KeyRkvMutator {
    private final double minValueToAdd;
    private final double maxValueToAdd;
    private final DoubleRangeGuard rangeGuard;
    private final Rand rand;

    public AddRandValueKeyRkvMutator(double minValueToAdd, double maxValueToAdd, Rand rand) {
        this(minValueToAdd, maxValueToAdd, new BlockOnBoundsDoubleRangeGuard(0d, 1d), rand);
    }

    public AddRandValueKeyRkvMutator(double minValueToAdd, double maxValueToAdd, DoubleRangeGuard rangeGuard, Rand rand) {
        this.minValueToAdd = minValueToAdd;
        this.maxValueToAdd = maxValueToAdd;
        this.rangeGuard = rangeGuard;
        this.rand = rand;
    }

    public AddRandValueKeyRkvMutator(Integer keyIndex, double minValueToAdd, double maxValueToAdd, Rand rand) {
        this(keyIndex, minValueToAdd, maxValueToAdd, new BlockOnBoundsDoubleRangeGuard(0d, 1d), rand);
    }

    public AddRandValueKeyRkvMutator(Integer keyIndex, double minValueToAdd, double maxValueToAdd, DoubleRangeGuard rangeGuard, Rand rand) {
        super(keyIndex);
        this.minValueToAdd = minValueToAdd;
        this.maxValueToAdd = maxValueToAdd;
        this.rangeGuard = rangeGuard;
        this.rand = rand;
    }

    @Override
    public Double getNewKeyValue(int keyIndex, RandomKeyVector rkv) {
        final Double oldValue = rkv.getKey(keyIndex);
        if (oldValue == null) {
            return null;
        }

        double newValue = oldValue + rand.doubleBetween(minValueToAdd, maxValueToAdd);
        newValue = rangeGuard.bringBackToRange(newValue);

        return newValue;
    }
}
