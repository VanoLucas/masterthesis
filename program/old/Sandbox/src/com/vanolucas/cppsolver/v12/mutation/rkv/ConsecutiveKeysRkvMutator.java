package com.vanolucas.cppsolver.v12.mutation.rkv;

import com.vanolucas.cppsolver.v12.genome.RandomKeyVector;
import com.vanolucas.cppsolver.v12.rand.Rand;

import java.util.List;

public class ConsecutiveKeysRkvMutator implements KeysRkvMutator {
    private final int nbKeys;
    private final KeyRkvMutator keyMutator;
    private final Rand rand;

    public ConsecutiveKeysRkvMutator(int nbKeys, KeyRkvMutator keyMutator, Rand rand) {
        this.nbKeys = nbKeys;
        this.keyMutator = keyMutator;
        this.rand = rand;
    }

    @Override
    public void mutate(RandomKeyVector rkv) {
        // pick first key to mutate
        final int startIndex = rand.int0To(rkv.length() - nbKeys + 1);
        // mutate the picked and subsequent keys
        for (int i = startIndex; i < startIndex + nbKeys; i++) {
            keyMutator.mutate(rkv, i);
        }
    }

    @Override
    public List<Double> getNewKeyValues(RandomKeyVector rkv) {
        throw new IllegalStateException(String.format("Should not use getNewKeyValues when using %s.",
                getClass().getSimpleName()
        ));
    }
}
