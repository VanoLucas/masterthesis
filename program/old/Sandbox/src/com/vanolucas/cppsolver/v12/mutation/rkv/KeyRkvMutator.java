package com.vanolucas.cppsolver.v12.mutation.rkv;

import com.vanolucas.cppsolver.v12.genome.RandomKeyVector;

public abstract class KeyRkvMutator implements RkvMutator {
    private final Integer keyIndex;

    public KeyRkvMutator() {
        this(null);
    }

    public KeyRkvMutator(Integer keyIndex) {
        this.keyIndex = keyIndex;
    }

    @Override
    public void mutate(RandomKeyVector rkv) {
        if (keyIndex != null) {
            mutate(rkv, keyIndex);
        }
    }

    public void mutate(RandomKeyVector rkv, int keyIndex) {
        rkv.mutateKeyUsing(this, keyIndex);
    }

    public abstract Double getNewKeyValue(int keyIndex, RandomKeyVector rkv);
}
