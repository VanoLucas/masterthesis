package com.vanolucas.cppsolver.v12.mutation.rkv;

import com.vanolucas.cppsolver.v12.genome.RandomKeyVector;

import java.util.List;

public interface KeysRkvMutator extends RkvMutator {
    @Override
    default void mutate(RandomKeyVector rkv) {
        rkv.mutateKeysUsing(this);
    }

    List<Double> getNewKeyValues(RandomKeyVector rkv);
}
