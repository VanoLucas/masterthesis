package com.vanolucas.cppsolver.v12.mutation.rkv;

import com.vanolucas.cppsolver.v12.genome.RandomKeyVector;
import com.vanolucas.cppsolver.v12.rand.Rand;

public class ProbaKeyRkvMutator extends KeyRkvMutator {
    private final double proba;
    private final KeyRkvMutator mutator;
    private final Rand rand;

    public ProbaKeyRkvMutator(double proba, KeyRkvMutator mutator, Rand rand) {
        this.proba = proba;
        this.mutator = mutator;
        this.rand = rand;
    }

    public ProbaKeyRkvMutator(Integer keyIndex, double proba, KeyRkvMutator mutator, Rand rand) {
        super(keyIndex);
        this.proba = proba;
        this.mutator = mutator;
        this.rand = rand;
    }

    @Override
    public void mutate(RandomKeyVector rkv, int keyIndex) {
        if (rand.double01() < proba) {
            mutator.mutate(rkv, keyIndex);
        }
    }

    @Override
    public Double getNewKeyValue(int keyIndex, RandomKeyVector rkv) {
        throw new IllegalStateException(String.format("Should not use getNewKeyValue in %s. ",
                getClass().getSimpleName()
        ));
    }
}
