package com.vanolucas.cppsolver.v12.mutation.rkv;

import com.vanolucas.cppsolver.v12.genome.RandomKeyVector;
import com.vanolucas.cppsolver.v12.rand.Rand;

import java.util.List;

public class ProbaKeysRkvMutator implements KeysRkvMutator {
    private final ProbaKeyRkvMutator keyMutator;

    public ProbaKeysRkvMutator(double proba, KeyRkvMutator mutator, Rand rand) {
        this.keyMutator = new ProbaKeyRkvMutator(proba, mutator, rand);
    }

    @Override
    public void mutate(RandomKeyVector rkv) {
        rkv.forEachIndex(k -> keyMutator.mutate(rkv, k));
    }

    @Override
    public List<Double> getNewKeyValues(RandomKeyVector rkv) {
        throw new IllegalStateException(String.format("Should not use getNewKeyValues when using %s.",
                getClass().getSimpleName()
        ));
    }
}
