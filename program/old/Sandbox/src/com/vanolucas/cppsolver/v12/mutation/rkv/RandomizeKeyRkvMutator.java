package com.vanolucas.cppsolver.v12.mutation.rkv;

import com.vanolucas.cppsolver.v12.genome.RandomKeyVector;
import com.vanolucas.cppsolver.v12.rand.Rand;

public class RandomizeKeyRkvMutator extends KeyRkvMutator {
    private final Rand rand;

    public RandomizeKeyRkvMutator(Rand rand) {
        this.rand = rand;
    }

    public RandomizeKeyRkvMutator(Integer keyIndex, Rand rand) {
        super(keyIndex);
        this.rand = rand;
    }

    @Override
    public void mutate(RandomKeyVector rkv, int keyIndex) {
        rkv.randomizeKey(keyIndex, rand);
    }

    @Override
    public Double getNewKeyValue(int keyIndex, RandomKeyVector rkv) {
        throw new IllegalStateException(String.format("Should not use getNewKeyValue when randomizing a key of a %s using %s. " +
                        "The key should be randomized by the %s itself, as called in mutate().",
                rkv.getClass().getSimpleName(),
                getClass().getSimpleName(),
                rkv.getClass().getSimpleName()
        ));
    }
}
