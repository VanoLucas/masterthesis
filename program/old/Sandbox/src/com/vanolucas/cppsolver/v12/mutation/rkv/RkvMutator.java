package com.vanolucas.cppsolver.v12.mutation.rkv;

import com.vanolucas.cppsolver.v12.genome.Genome;
import com.vanolucas.cppsolver.v12.genome.RandomKeyVector;
import com.vanolucas.cppsolver.v12.mutation.GenomeMutator;

public interface RkvMutator extends GenomeMutator {
    @Override
    default void mutate(Genome rkv) {
        mutate((RandomKeyVector) rkv);
    }

    void mutate(RandomKeyVector rkv);
}
