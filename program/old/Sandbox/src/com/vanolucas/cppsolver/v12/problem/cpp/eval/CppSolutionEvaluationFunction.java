package com.vanolucas.cppsolver.v12.problem.cpp.eval;

import com.vanolucas.cppsolver.v12.eval.EvaluationFunction;
import com.vanolucas.cppsolver.v12.problem.cpp.solution.CppSolution;
import com.vanolucas.cppsolver.v12.problem.cpp.instance.CppInstance;
import com.vanolucas.cppsolver.v12.quality.DoubleCost;

public class CppSolutionEvaluationFunction implements EvaluationFunction<CppSolution, DoubleCost> {
    private final double costPlate;
    private final double costPrinting;

    public CppSolutionEvaluationFunction(CppInstance cppInstance) {
        this(cppInstance.getCostPlate(), cppInstance.getCostPrinting());
    }

    public CppSolutionEvaluationFunction(double costPlate, double costPrinting) {
        this.costPlate = costPlate;
        this.costPrinting = costPrinting;
    }

    @Override
    public DoubleCost evaluate(CppSolution solution) {
        final double totalCost = solution.getNbPlates() * costPlate + solution.getTotalPrintings() * costPrinting;
        return new DoubleCost(totalCost);
    }
}
