package com.vanolucas.cppsolver.v12.problem.cpp.eval;

import com.vanolucas.cppsolver.v12.problem.cpp.genome.GenomeCf;
import com.vanolucas.cppsolver.v12.problem.cpp.instance.CppInstance;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class GenomeCfDecoder implements Function<GenomeCf, Slots> {
    private final List<Integer> covers;
    private final int nbCovers;
    private final int nbSlots;

    public GenomeCfDecoder(CppInstance cppInstance) {
        this(cppInstance.getCovers(), cppInstance.getNbCovers(), cppInstance.getTotalNbSlots());
    }

    public GenomeCfDecoder(List<Integer> covers, int nbCovers, int nbSlots) {
        this.covers = covers;
        this.nbCovers = nbCovers;
        this.nbSlots = nbSlots;
    }

    @Override
    public Slots apply(GenomeCf genome) {
        return decode(genome);
    }

    public Slots decode(GenomeCf genome) {
        // to store our allocated slots
        final List<Slot> slots = new ArrayList<>(nbSlots);

        // initially allocate each book cover to a single slot that fills 100% of its copies demand
        slots.addAll(IntStream.range(0, nbCovers)
                .mapToObj(cover -> new Slot(cover, covers.get(cover)))
                .collect(Collectors.toList())
        );

        // then we need to divide these printings over multiple slots to use all the available slots
        {
            // for each extra slot to fill
            for (int k = 0; k < genome.length(); k += 2) {
                // get the keys that characterize this slot
                final double keyCoverToSplit = genome.getKey(k);
                final double fractionToSplitOut = genome.getKey(k + 1);

                // calculate the index of the book cover/slot to split over an extra slot
                final int coverToSplit = (int) (keyCoverToSplit * (double) nbCovers);

                // split a fraction of the printings from the chosen cover over an extra slot
                slots.add(
                        slots.get(coverToSplit)
                                .splitFraction(fractionToSplitOut)
                );
            }
        }

        // return the allocated slots intermediate representation
        return new Slots(slots);
    }
}
