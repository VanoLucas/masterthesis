package com.vanolucas.cppsolver.v12.problem.cpp.eval;

import com.vanolucas.cppsolver.v12.problem.cpp.genome.GenomePpc;
import com.vanolucas.cppsolver.v12.problem.cpp.instance.CppInstance;
import com.vanolucas.cppsolver.v12.util.CutCake;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Part 1: keys to determine the nb of printings of each cover's first slot.
 * Part 2: pairs of keys to determine the nb of printings of each extra slot and its allocated book cover.
 */
public class GenomePpcDecoder implements Function<GenomePpc, Slots> {
    private final List<Integer> covers;
    private final int nbCovers;
    private final int nbSlots;

    public GenomePpcDecoder(CppInstance cppInstance) {
        this(cppInstance.getCovers(), cppInstance.getNbCovers(), cppInstance.getTotalNbSlots());
    }

    public GenomePpcDecoder(List<Integer> covers, int nbCovers, int nbSlots) {
        this.covers = covers;
        this.nbCovers = nbCovers;
        this.nbSlots = nbSlots;
    }

    @Override
    public Slots apply(GenomePpc genome) {
        return decode(genome);
    }

    public Slots decode(GenomePpc genome) {
        // get the keys that determine the number of printings required for the slots of each book cover
        List<List<Double>> keysSlotsPrintingsByBookCover = getKeysSlotsPrintingsByBookCover(genome);

        // determine the allocated slots based on the keys
        final List<Slot> slots = new ArrayList<>(nbSlots);
        for (int cover = 0; cover < nbCovers; cover++) {
            // get keys that define slot printings for this book cover
            double[] keys = keysSlotsPrintingsByBookCover.get(cover)
                    .stream()
                    .mapToDouble(__ -> __)
                    .toArray();
            // calculate number of printings to allocate to each slot based on the keys
            int[] slotPrintings = CutCake.getShares(covers.get(cover), keys);
            // construct the allocated slots
            for (int nbPrintings : slotPrintings) {
                slots.add(new Slot(cover, nbPrintings));
            }
        }

        // return the allocated slots intermediate representation
        return new Slots(slots);
    }

    private List<List<Double>> getKeysSlotsPrintingsByBookCover(GenomePpc genome) {
        // to store, for each book cover, the keys that determine how to spread its printings over its slots
        List<List<Double>> keysByCover = IntStream.range(0, nbCovers)
                .mapToObj(__ -> new ArrayList<Double>(nbSlots - nbCovers))
                .collect(Collectors.toList());

        // get the initial key of each book cover
        for (int cover = 0; cover < nbCovers; cover++) {
            keysByCover.get(cover).add(
                    genome.getKey(cover)
            );
        }

        // read the extra slot keys
        final int genomeLen = genome.length();
        for (int i = nbCovers; i + 1 < genomeLen; i += 2) {
            // read keys for this extra slot
            double keyNbPrintings = genome.getKey(i);
            double keyBookCover = genome.getKey(i + 1);
            // determine the book cover index based on the key
            int cover = (int) (keyBookCover * (double) nbCovers);
            // store the printings key in this book cover's list of keys
            keysByCover.get(cover).add(keyNbPrintings);
        }

        return keysByCover;
    }
}
