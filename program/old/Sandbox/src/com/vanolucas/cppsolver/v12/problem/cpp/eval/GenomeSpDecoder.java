package com.vanolucas.cppsolver.v12.problem.cpp.eval;

import com.vanolucas.cppsolver.v12.problem.cpp.genome.GenomeSp;
import com.vanolucas.cppsolver.v12.problem.cpp.instance.CppInstance;
import com.vanolucas.cppsolver.v12.util.CutCake;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Part 1: keys to determine the number of extra slots for each book cover.
 * Part 2: keys to determine the number of printings to allocate each slot.
 */
public class GenomeSpDecoder implements Function<GenomeSp, Slots> {
    private final List<Integer> covers;
    private final int nbCovers;
    private final int nbSlots;

    public GenomeSpDecoder(CppInstance cppInstance) {
        this(cppInstance.getCovers(), cppInstance.getNbCovers(), cppInstance.getTotalNbSlots());
    }

    public GenomeSpDecoder(List<Integer> covers, int nbCovers, int nbSlots) {
        this.covers = covers;
        this.nbCovers = nbCovers;
        this.nbSlots = nbSlots;
    }

    @Override
    public Slots apply(GenomeSp genome) {
        return decode(genome);
    }

    public Slots decode(GenomeSp genome) {
        // determine the number of slots to allocate to each book cover
        int[] nbSlotsPerCover = getNbSlotsPerCover(genome);

        // get the keys that determine the number of printings required for the slots of each book cover
        List<List<Double>> keysPrintingsForBookCover = getKeysPrintingsForEachBookCover(genome, nbSlotsPerCover);

        // calculate the number of printings to require for each slot of each book cover
        List<List<Integer>> requiredPrintings = getRequiredPrintings(keysPrintingsForBookCover);

        // calculate our allocated slots
        final List<Slot> slots = IntStream.range(0, nbCovers)
                // for each cover, construct its slots
                .boxed()
                .flatMap(cover -> requiredPrintings.get(cover).stream()
                        .map(nbPrintings -> new Slot(cover, nbPrintings))
                )
                .collect(Collectors.toList());

        // return the allocated slots intermediate representation
        return new Slots(slots);
    }

    private int[] getNbSlotsPerCover(GenomeSp genome) {
        // get keys of the genome that define the number of slots for each book cover
        double[] keysNbSlots = genome.doubleStream()
                .limit(nbCovers)
                .toArray();
        // determine how many extra slots to allocate to each book cover based on the keys
        int[] nbExtraSlotsPerCover = CutCake.getShares(nbSlots - nbCovers, keysNbSlots);
        // calculate total nb of slots for each book cover
        return Arrays.stream(nbExtraSlotsPerCover)
                // each book cover must be allocated to at least 1 slot
                .map(nbExtraSlots -> nbExtraSlots + 1)
                .toArray();
    }

    private List<List<Double>> getKeysPrintingsForEachBookCover(GenomeSp genome, int[] nbSlotsPerCover) {
        // get keys of the genome that define the number of printings for each slot
        double[] keysPrintings = genome.doubleStream()
                .skip(nbCovers)
                .toArray();

        // to store the keys that define how to spread the printings for each book cover
        List<List<Double>> keysPrintingsForBookCover = IntStream.range(0, nbCovers)
                .mapToObj(cover -> new ArrayList<Double>(nbSlots - nbCovers))
                .collect(Collectors.toList());

        // assign each key to a book cover based on the number of slots per book cover
        int remainingSlotsForBook = 0;
        int cover = -1;
        for (int keyIndex = 0; keyIndex < nbSlots; keyIndex++) {
            if (remainingSlotsForBook == 0) {
                cover++;
                remainingSlotsForBook = nbSlotsPerCover[cover];
            }
            keysPrintingsForBookCover.get(cover)
                    .add(keysPrintings[keyIndex]);
            remainingSlotsForBook--;
        }

        return keysPrintingsForBookCover;
    }

    private List<List<Integer>> getRequiredPrintings(List<List<Double>> keysPrintingsForBookCover) {
        // to store the required number of printings for each slot of each book cover
        List<List<Integer>> requiredPrintings = IntStream.range(0, nbCovers)
                .mapToObj(cover -> new ArrayList<Integer>(keysPrintingsForBookCover.get(cover).size()))
                .collect(Collectors.toList());

        // for each book cover, determine the required number of printings for each of its slots based on the keys
        for (int cover = 0; cover < nbCovers; cover++) {
            // get demand for this book cover
            int demand = covers.get(cover);
            // get keys that determine the nb of printings of each slot for this book cover
            double[] keys = keysPrintingsForBookCover.get(cover)
                    .stream()
                    .mapToDouble(__ -> __)
                    .toArray();
            // spread the number of printings over the slots of this book cover
            int[] nbPrintingsPerSlot = CutCake.getShares(demand, keys);
            requiredPrintings.get(cover)
                    .addAll(Arrays.stream(nbPrintingsPerSlot)
                            .boxed()
                            .collect(Collectors.toList())
                    );
        }

        return requiredPrintings;
    }
}
