package com.vanolucas.cppsolver.v12.problem.cpp.eval;

public class Slot implements Comparable<Slot> {
    private final int coverIndex;
    private int minPrintings;

    public Slot(int coverIndex, int minPrintings) {
        this.coverIndex = coverIndex;
        this.minPrintings = minPrintings;
    }

    public int getCoverIndex() {
        return coverIndex;
    }

    public int getMinPrintings() {
        return minPrintings;
    }

    /**
     * Splits the number of printings for this slot over two slots.
     *
     * @param portionToExtract Portion (0..1) of the nb of printings of the initial slot to move to the newly created slot.
     * @return The new slot with a portion of the printings allocated to it.
     */
    Slot splitFraction(double portionToExtract) {
        final int nbPrintingsToExtract = (int) ((double) minPrintings * portionToExtract);
        return splitPrintings(nbPrintingsToExtract);
    }

    Slot splitPrintings(int nbPrintingsToExtract) {
        this.minPrintings -= nbPrintingsToExtract;
        return new Slot(coverIndex, nbPrintingsToExtract);
    }

    @Override
    public int compareTo(Slot other) {
        return Integer.compare(minPrintings, other.minPrintings);
    }

    @Override
    public String toString() {
        return String.format("%d:%d",
                coverIndex, minPrintings
        );
    }
}
