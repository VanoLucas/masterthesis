package com.vanolucas.cppsolver.v12.problem.cpp.eval;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Slots {
    private final List<Slot> slots;

    public Slots(List<Slot> slots) {
        this.slots = slots;
    }

    public Stream<Slot> stream() {
        return slots.stream();
    }

    @Override
    public String toString() {
        final String strSlots = slots.stream()
                .sorted(Comparator.reverseOrder())
                .map(Slot::toString)
                .collect(Collectors.joining(" | "));
        return String.format("(%d slots) %s",
                slots.size(),
                strSlots
        );
    }
}
