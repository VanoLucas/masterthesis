package com.vanolucas.cppsolver.v12.problem.cpp.eval;

import com.vanolucas.cppsolver.v12.problem.cpp.solution.CppSolution;
import com.vanolucas.cppsolver.v12.problem.cpp.solution.Plate;
import com.vanolucas.cppsolver.v12.problem.cpp.instance.CppInstance;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class SlotsDecoder implements Function<Slots, CppSolution> {
    private final int nbSlotsPerPlate;

    public SlotsDecoder(CppInstance cppInstance) {
        this(cppInstance.getNbSlotsPerPlate());
    }

    public SlotsDecoder(int nbSlotsPerPlate) {
        this.nbSlotsPerPlate = nbSlotsPerPlate;
    }

    @Override
    public CppSolution apply(Slots slots) {
        return decode(slots);
    }

    public CppSolution decode(Slots slots) {
        // sort slots by number of printings so that we regroup on the same offset plate slots that have a similar nb of printings to reduce waste
        final List<Slot> sortedSlots = slots.stream()
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());

        // build collection of offset plates based on our allocated slots
        final int nbSlots = sortedSlots.size();
        final List<Plate> plates = new ArrayList<>(nbSlots / nbSlotsPerPlate);
        for (int indexFirstSlotOfPlate = 0; indexFirstSlotOfPlate < nbSlots; indexFirstSlotOfPlate += nbSlotsPerPlate) {
            final int ixFirstSlotOfPlate = indexFirstSlotOfPlate;

            final List<Slot> slotsForThisPlate = IntStream.range(0, nbSlotsPerPlate)
                    .mapToObj(i -> sortedSlots.get(ixFirstSlotOfPlate + i))
                    .collect(Collectors.toList());

            plates.add(new Plate(slotsForThisPlate));
        }

        // construct CPP solution with our offset plates
        return new CppSolution(plates);
    }
}
