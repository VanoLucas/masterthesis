package com.vanolucas.cppsolver.v12.problem.cpp.genome;

import com.vanolucas.cppsolver.v12.genome.RandomKeyVector;
import com.vanolucas.cppsolver.v12.problem.cpp.instance.CppInstance;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Sequence of pairs of keys. Each pair defines which book cover and how many printings to allocate to each slot.
 * The first key of each pair points to one of the book covers.
 * The second key of each pair gives the fraction of the demand to allocate to the slot.
 * Each book cover needs to be allocated to at least 1 slot. We therefore only need to describe all other (nbSlots - nbCovers) slots.
 */
public class GenomeCf extends RandomKeyVector {
    public GenomeCf(CppInstance cppInstance) {
        this(lengthForCppInstance(cppInstance));
    }

    public GenomeCf(int length) {
        super(length);
    }

    @Override
    public GenomeCf clone() throws CloneNotSupportedException {
        return (GenomeCf) super.clone();
    }

    @Override
    public String toString() {
        final String strGenes = IntStream.iterate(0, i -> i + 2)
                .limit(length() / 2)
                .mapToObj(i -> String.format("%s %s",
                        getKey(i), getKey(i + 1)))
                .collect(Collectors.joining(" | "));
        return String.format("(%d genes) %s",
                length(),
                strGenes
        );
    }

    public static int lengthForCppInstance(CppInstance cppInstance) {
        return calculateLength(
                cppInstance.getNbCovers(),
                cppInstance.getTotalNbSlots()
        );
    }

    public static int calculateLength(int nbCovers, int totalNbSlots) {
        return (totalNbSlots - nbCovers) * 2;
    }
}
