package com.vanolucas.cppsolver.v12.problem.cpp.genome;

import com.vanolucas.cppsolver.v12.genome.RandomKeyVector;
import com.vanolucas.cppsolver.v12.problem.cpp.instance.CppInstance;

/**
 * Genome in two parts.
 * The first part contains one key per book cover.
 * Each key defines how many printings should be allocated to the first slot of each book cover.
 * The second part contains a sequence of pairs of keys. There are as many pairs as extra slots to fill.
 * The first key of each pair defines the number of printings required for the slot.
 * The second key of each pair defines which of the book covers should be allocated to the slot.
 */
public class GenomePpc extends RandomKeyVector {
    public GenomePpc(CppInstance cppInstance) {
        this(lengthForCppInstance(cppInstance));
    }

    public GenomePpc(int length) {
        super(length);
    }

    @Override
    public GenomePpc clone() throws CloneNotSupportedException {
        return (GenomePpc) super.clone();
    }

    public static int lengthForCppInstance(CppInstance cppInstance) {
        return calculateLength(
                cppInstance.getNbCovers(),
                cppInstance.getTotalNbSlots()
        );
    }

    public static int calculateLength(int nbCovers, int totalNbSlots) {
        return nbCovers + 2 * (totalNbSlots - nbCovers);
    }
}
