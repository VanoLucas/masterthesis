package com.vanolucas.cppsolver.v12.problem.cpp.genome;

import com.vanolucas.cppsolver.v12.genome.RandomKeyVector;
import com.vanolucas.cppsolver.v12.problem.cpp.instance.CppInstance;

public class GenomeSp extends RandomKeyVector {
    public GenomeSp(CppInstance cppInstance) {
        this(lengthForCppInstance(cppInstance));
    }

    public GenomeSp(int length) {
        super(length);
    }

    @Override
    public GenomeSp clone() throws CloneNotSupportedException {
        return (GenomeSp) super.clone();
    }

    public static int lengthForCppInstance(CppInstance cppInstance) {
        return calculateLength(
                cppInstance.getNbCovers(),
                cppInstance.getTotalNbSlots()
        );
    }

    public static int calculateLength(int nbCovers, int totalNbSlots) {
        return nbCovers + totalNbSlots;
    }
}
