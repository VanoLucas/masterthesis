package com.vanolucas.cppsolver.v12.problem.cpp.instance;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CppInstance {
    private static final double DEFAULT_COST_PLATE = 0d;
    private static final double DEFAULT_COST_PRINTING = 1d;
    private static final int DEFAULT_NB_SLOTS_PER_PLATE = 4;

    private List<Integer> covers;
    private int nbPlates;
    private int nbSlotsPerPlate;
    private double costPlate;
    private double costPrinting;

    public CppInstance(String instanceName, int nbPlates) throws IOException {
        this(loadCoversFromInstance(instanceName), nbPlates);
    }

    public CppInstance(List<Integer> covers, int nbPlates) {
        this(covers, nbPlates, DEFAULT_NB_SLOTS_PER_PLATE);
    }

    public CppInstance(List<Integer> covers, int nbPlates, int nbSlotsPerPlate) {
        this(covers, nbPlates, nbSlotsPerPlate, DEFAULT_COST_PLATE, DEFAULT_COST_PRINTING);
    }

    public CppInstance(String instanceName, int nbPlates, int nbSlotsPerPlate, double costPlate, double costPrinting) throws IOException {
        this(loadCoversFromInstance(instanceName), nbPlates, nbSlotsPerPlate, costPlate, costPrinting);
    }

    public CppInstance(List<Integer> covers, int nbPlates, int nbSlotsPerPlate, double costPlate, double costPrinting) {
        if (covers == null || covers.isEmpty()) {
            throw new IllegalArgumentException("We need least the demand for one book cover as input param.");
        }
        this.covers = covers;
        this.covers.sort(Comparator.reverseOrder());
        this.nbPlates = nbPlates;
        this.nbSlotsPerPlate = nbSlotsPerPlate;
        this.costPlate = costPlate;
        this.costPrinting = costPrinting;
    }

    public List<Integer> getCovers() {
        return covers;
    }

    public int getNbCovers() {
        return covers.size();
    }

    public int getNbSlotsPerPlate() {
        return nbSlotsPerPlate;
    }

    public int getTotalNbSlots() {
        return nbPlates * nbSlotsPerPlate;
    }

    public double getCostPlate() {
        return costPlate;
    }

    public double getCostPrinting() {
        return costPrinting;
    }

    public static List<Integer> loadCoversFromInstance(String instanceName) throws IOException {
        String filename = String.format("res/cpp_instances/%s.dat", instanceName);
        return Files.lines(Paths.get(filename))
                .map(Integer::parseInt)
                .collect(Collectors.toList());
    }

    @Override
    public String toString() {
        final String strCovers = IntStream.range(0, covers.size())
                .mapToObj(i -> String.format("%3d: %6d",
                        i + 1,
                        covers.get(i)
                ))
                .collect(Collectors.joining(System.lineSeparator()));
        return String.format("Book covers demand:%n" +
                        "%s%n" +
                        "Over %d offset plates of %d slots%n" +
                        "Cost of an offset plate: %f%n" +
                        "Cost of one printing: %f",
                strCovers,
                nbPlates, nbSlotsPerPlate,
                costPlate, costPrinting
        );
    }
}
