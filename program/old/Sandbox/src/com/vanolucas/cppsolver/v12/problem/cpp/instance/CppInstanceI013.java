package com.vanolucas.cppsolver.v12.problem.cpp.instance;

import java.io.IOException;
import java.util.List;

/**
 * 30 book covers reference instance of the Cover Printing Problem.
 */
public class CppInstanceI013 extends CppInstance {
    private static final int DEFAULT_NB_PLATES = 10;

    public CppInstanceI013() throws IOException {
        this(DEFAULT_NB_PLATES);
    }

    public CppInstanceI013(int nbPlates) throws IOException {
        super(loadCovers(), nbPlates);
    }

    public CppInstanceI013(int nbPlates, int nbSlotsPerPlate) throws IOException {
        super(loadCovers(), nbPlates, nbSlotsPerPlate);
    }

    public CppInstanceI013(int nbPlates, int nbSlotsPerPlate, double costPlate, double costPrinting) throws IOException {
        super(loadCovers(), nbPlates, nbSlotsPerPlate, costPlate, costPrinting);
    }

    private static List<Integer> loadCovers() throws IOException {
        return loadCoversFromInstance("I013");
    }
}
