package com.vanolucas.cppsolver.v12.problem.cpp.solution;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CppSolution {
    private final List<Plate> plates;
    private Map<Integer, Integer> printingsPerCover;

    public CppSolution(List<Plate> plates) {
        this.plates = plates;
    }

    public int getNbPlates() {
        return plates.size();
    }

    public int getTotalPrintings() {
        return plates.stream()
                .mapToInt(Plate::getNbPrintings)
                .sum();
    }

    public Map<Integer, Integer> getPrintingsPerCover() {
        if (printingsPerCover == null) {
            final int nbCovers = (int) plates.stream()
                    .flatMap(Plate::streamOfCovers)
                    .distinct()
                    .count();
            // for each book cover, count the total nb of printings
            printingsPerCover = IntStream.range(0, nbCovers)
                    .boxed()
                    .collect(Collectors.toMap(
                            cover -> cover,
                            // count the total number of printings for this book cover
                            cover -> plates.stream()
                                    .mapToInt(plate -> plate.getNbPrintingsOfCover(cover))
                                    .sum()
                    ));
        }
        return printingsPerCover;
    }

    @Override
    public String toString() {
        return String.format("Offset plates:%n" +
                        "%s%n" +
                        "Printings per book cover:%n" +
                        "%s%n" +
                        "Total printings: %d",
                plates.stream()
                        .sorted(Comparator.reverseOrder())
                        .map(Plate::toString)
                        .collect(Collectors.joining(System.lineSeparator())),
                getPrintingsPerCover().keySet().stream()
                        .sorted()
                        .map(cover -> String.format("%3d: %d printings",
                                cover + 1,
                                getPrintingsPerCover().get(cover)
                        ))
                        .collect(Collectors.joining(System.lineSeparator())),
                getTotalPrintings()
        );
    }
}
