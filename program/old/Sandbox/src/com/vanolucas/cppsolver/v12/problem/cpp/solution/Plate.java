package com.vanolucas.cppsolver.v12.problem.cpp.solution;

import com.vanolucas.cppsolver.v12.problem.cpp.eval.Slot;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Plate implements Comparable<Plate> {
    private final List<Integer> coverIndexes;
    private final int nbPrintings;

    public Plate(List<Slot> slots) {
        this(
                slots.stream()
                        .map(Slot::getCoverIndex)
                        .collect(Collectors.toList()),
                slots.stream()
                        .mapToInt(Slot::getMinPrintings)
                        .max().orElseThrow(IllegalStateException::new)
        );
    }

    public Plate(List<Integer> coverIndexes, int nbPrintings) {
        this.coverIndexes = coverIndexes;
        this.nbPrintings = nbPrintings;
    }

    public int getNbPrintingsOfCover(int coverIndex) {
        return (int) (coverIndexes.stream()
                .filter(coverInSlot -> coverInSlot.equals(coverIndex))
                .count() * nbPrintings);
    }

    public int getNbPrintings() {
        return nbPrintings;
    }

    public Stream<Integer> streamOfCovers() {
        return coverIndexes.stream();
    }

    @Override
    public int compareTo(Plate other) {
        return Integer.compare(nbPrintings, other.nbPrintings);
    }

    @Override
    public String toString() {
        final String strCovers = coverIndexes.stream()
                .sorted()
                .map(cover -> String.format("%3d", cover + 1))
                .collect(Collectors.joining("|", "|", "|"));
        return String.format("%s\t%6d printings",
                strCovers,
                nbPrintings
        );
    }
}
