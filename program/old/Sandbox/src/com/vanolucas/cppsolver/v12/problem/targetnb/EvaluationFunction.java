package com.vanolucas.cppsolver.v12.problem.targetnb;

import com.vanolucas.cppsolver.v12.quality.IntCost;

public class EvaluationFunction implements com.vanolucas.cppsolver.v12.eval.EvaluationFunction<Phenotype, IntCost> {
    private final int target;

    public EvaluationFunction(TargetNumberProblem problem) {
        this(problem.getTarget());
    }

    public EvaluationFunction(int target) {
        this.target = target;
    }

    @Override
    public IntCost evaluate(Phenotype phenotype) {
        // fake time consuming eval
//        try {
//            TimeUnit.SECONDS.sleep(1L);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        final int distance = Math.abs(phenotype.getValue() - target);
        return new IntCost(distance);
    }
}
