package com.vanolucas.cppsolver.v12.problem.targetnb;

import com.vanolucas.cppsolver.v12.genome.RandomKeyVector;
import com.vanolucas.cppsolver.v12.rand.Rand;

import java.util.stream.DoubleStream;

public class Genome extends RandomKeyVector {
    public Genome(int length) {
        super(length);
    }

    @Override
    public Genome clone() throws CloneNotSupportedException {
        return (Genome) super.clone();
    }
}
