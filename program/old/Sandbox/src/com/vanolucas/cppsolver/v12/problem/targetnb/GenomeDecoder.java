package com.vanolucas.cppsolver.v12.problem.targetnb;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class GenomeDecoder implements Function<Genome, Intermediate1> {
    private final int maxValuePerKey;

    public GenomeDecoder(TargetNumberProblem problem) {
        this(problem.getMaxValuePerGenomeKey());
    }

    public GenomeDecoder(int maxValuePerKey) {
        this.maxValuePerKey = maxValuePerKey;
    }

    @Override
    public Intermediate1 apply(Genome genome) {
        final List<Integer> values = genome.doubleStream()
                .mapToInt(k -> (int) (k * maxValuePerKey))
                .boxed()
                .collect(Collectors.toList());
        return new Intermediate1(values);
    }
}
