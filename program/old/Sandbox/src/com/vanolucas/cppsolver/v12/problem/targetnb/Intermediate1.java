package com.vanolucas.cppsolver.v12.problem.targetnb;

import java.util.List;

public class Intermediate1 {
    private final List<Integer> values;

    public Intermediate1(List<Integer> values) {
        this.values = values;
    }

    public int sum() {
        return values.stream()
                .mapToInt(__ -> __)
                .sum();
    }

    @Override
    public String toString() {
        return values.toString();
    }
}
