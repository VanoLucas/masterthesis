package com.vanolucas.cppsolver.v12.problem.targetnb;

import java.util.function.Function;

public class Intermediate1Decoder implements Function<Intermediate1, Intermediate2> {
    @Override
    public Intermediate2 apply(Intermediate1 intermediate1) {
        return new Intermediate2(intermediate1);
    }
}
