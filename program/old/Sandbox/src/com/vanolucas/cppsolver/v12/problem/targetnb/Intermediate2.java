package com.vanolucas.cppsolver.v12.problem.targetnb;

public class Intermediate2 {
    private final int sum;

    public Intermediate2(Intermediate1 fromIntermediate1) {
        this(fromIntermediate1.sum());
    }

    public Intermediate2(int sum) {
        this.sum = sum;
    }

    public int getSum() {
        return sum;
    }

    @Override
    public String toString() {
        return String.valueOf(sum);
    }
}
