package com.vanolucas.cppsolver.v12.problem.targetnb;

import java.util.function.Function;

public class Intermediate2Decoder implements Function<Intermediate2, Phenotype> {
    @Override
    public Phenotype apply(Intermediate2 intermediate2) {
        return new Phenotype(intermediate2);
    }
}
