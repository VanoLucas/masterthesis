package com.vanolucas.cppsolver.v12.problem.targetnb;

public class Phenotype {
    private final int value;

    public Phenotype(Intermediate2 fromIntermediate2) {
        this(fromIntermediate2.getSum());
    }

    public Phenotype(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
