package com.vanolucas.cppsolver.v12.problem.targetnb;

import com.vanolucas.cppsolver.v12.clone.ComplexGeneticSolutionCloner;
import com.vanolucas.cppsolver.v12.eval.ComplexGeneticSolutionEvaluationFunction;
import com.vanolucas.cppsolver.v12.solution.ComplexGeneticSolution;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class TargetNumberProblem {
    private final int target;
    private final int genomeLength;
    private final int maxValuePerGenomeKey;

    public TargetNumberProblem() {
        this(100);
    }

    public TargetNumberProblem(int target) {
        this(target, 2);
    }

    public TargetNumberProblem(int target, int genomeLength) {
        this(target, genomeLength, 100);
    }

    public TargetNumberProblem(int target, int genomeLength, int maxValuePerGenomeKey) {
        this.target = target;
        this.genomeLength = genomeLength;
        this.maxValuePerGenomeKey = maxValuePerGenomeKey;
    }

    public int getMaxValuePerGenomeKey() {
        return maxValuePerGenomeKey;
    }

    public int getTarget() {
        return target;
    }

    public int getGenomeLength() {
        return genomeLength;
    }

    public Genome newGenome() {
        return new Genome(getGenomeLength());
    }

    public GenomeDecoder newGenomeDecoder() {
        return new GenomeDecoder(this);
    }

    public Intermediate1Decoder newIntermediate1Decoder() {
        return new Intermediate1Decoder();
    }

    public Intermediate2Decoder newIntermediate2Decoder() {
        return new Intermediate2Decoder();
    }

    public List<Function> newIntermediateDecoders() {
        return Arrays.asList(
                newIntermediate1Decoder(),
                newIntermediate2Decoder()
        );
    }

    public EvaluationFunction newEvaluationFunction() {
        return new EvaluationFunction(this);
    }

    public ComplexGeneticSolutionEvaluationFunction newSolutionEvaluationFunction() {
        return new ComplexGeneticSolutionEvaluationFunction(
                newGenomeDecoder(),
                newIntermediateDecoders(),
                newEvaluationFunction()
        );
    }

    public ComplexGeneticSolutionCloner newSolutionCloner() {
        return new ComplexGeneticSolutionCloner();
    }

    public ComplexGeneticSolution newSolution() {
        return new ComplexGeneticSolution(
                newGenome(), 2
        );
    }
}
