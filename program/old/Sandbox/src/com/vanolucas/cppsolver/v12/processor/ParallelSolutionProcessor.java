package com.vanolucas.cppsolver.v12.processor;

import com.vanolucas.cppsolver.v12.solution.Solution;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ParallelSolutionProcessor<TOutput> extends SolutionProcessor<TOutput> {
    private final ExecutorService executor;

    public ParallelSolutionProcessor() {
        this(null);
    }

    public ParallelSolutionProcessor(int maxThreads) {
        this(null, maxThreads);
    }

    public ParallelSolutionProcessor(Function<Solution, TOutput> task) {
        this(task, Runtime.getRuntime().availableProcessors());
    }

    public ParallelSolutionProcessor(Function<Solution, TOutput> task, int maxThreads) {
        this(task, Executors.newFixedThreadPool(maxThreads));
    }

    public ParallelSolutionProcessor(Function<Solution, TOutput> task, ExecutorService executor) {
        super(task);
        this.executor = executor;
    }

    @Override
    public List<TOutput> runTask(Function<Solution, TOutput> task, List<Solution> solutions) {
        final List<Future<TOutput>> futures = solutions.stream()
                // perform the task on each solution, spread over the threads
                .map(solution ->
                        // submit task to thread pool
                        executor.submit(() ->
                                // process this solution
                                task.apply(solution)
                        )
                )
                .collect(Collectors.toList());
        return IntStream.range(0, futures.size())
                .mapToObj(i -> {
                    try {
                        return futures.get(i).get();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        throw new IllegalStateException(String.format("Solution processing thread has been interrupted in %s.",
                                getClass().getSimpleName()
                        ));
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                        throw new IllegalStateException(String.format("Processing failed in %s for solution:%n%s",
                                getClass().getSimpleName(),
                                solutions.get(i)
                        ));
                    }
                })
                .collect(Collectors.toList());
    }

    @Override
    public List<TOutput> runTasks(List<Function<Solution, TOutput>> tasks, List<Solution> solutions) {
        final List<Future<TOutput>> futures = IntStream.range(0, tasks.size())
                // perform each solution's task, spread over the threads
                .mapToObj(i -> {
                    final Function<Solution, TOutput> task = tasks.get(i);
                    final Solution solution = solutions.get(i);
                    // submit task to thread pool
                    return executor.submit(() ->
                            // process this solution
                            task.apply(solution)
                    );
                })
                .collect(Collectors.toList());
        return IntStream.range(0, futures.size())
                .mapToObj(i -> {
                    try {
                        return futures.get(i).get();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        throw new IllegalStateException(String.format("Solution processing thread has been interrupted in %s.",
                                getClass().getSimpleName()
                        ));
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                        throw new IllegalStateException(String.format("Processing failed in %s for solution:%n%s",
                                getClass().getSimpleName(),
                                solutions.get(i)
                        ));
                    }
                })
                .collect(Collectors.toList());
    }
}
