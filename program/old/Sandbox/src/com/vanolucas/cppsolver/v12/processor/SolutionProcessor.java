package com.vanolucas.cppsolver.v12.processor;

import com.vanolucas.cppsolver.v12.solution.Solution;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class SolutionProcessor<TOutput> {
    private final Function<Solution, TOutput> task;

    public SolutionProcessor() {
        this(null);
    }

    public SolutionProcessor(Function<Solution, TOutput> task) {
        this.task = task;
    }

    public List<TOutput> process(List<Solution> solutions) {
        return runTask(task, solutions);
    }

    public TOutput process(Solution solution) {
        return runTask(task, solution);
    }

    public List<TOutput> runTask(Function<Solution, TOutput> task, List<Solution> solutions) {
        return solutions.stream()
                .map(task)
                .collect(Collectors.toList());
    }

    public TOutput runTask(Function<Solution, TOutput> task, Solution solution) {
        return task.apply(solution);
    }

    public List<TOutput> runTasks(List<Function<Solution, TOutput>> tasks, List<Solution> solutions) {
        return IntStream.range(0, tasks.size())
                .mapToObj(i -> tasks.get(i).apply(solutions.get(i)))
                .collect(Collectors.toList());
    }
}
