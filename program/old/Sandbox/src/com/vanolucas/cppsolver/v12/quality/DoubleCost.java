package com.vanolucas.cppsolver.v12.quality;

/**
 * A Cost Quality quantified as a single double value.
 */
public class DoubleCost extends SingleNumberQuality<Double> {

    public DoubleCost(Double value) {
        super(value);
    }

    /**
     * A better cost is a lower one.
     */
    @Override
    public boolean isBetterThan(Quality other) {
        DoubleCost o = (DoubleCost) other;
        return value < o.value;
    }

    public static DoubleCost zero() {
        return new DoubleCost(0d);
    }
}
