package com.vanolucas.cppsolver.v12.quality;

/**
 * Represents the quantified, absolute quality value of something.
 */
public interface Quality extends Comparable<Quality> {

    boolean isBetterThan(Quality other);

    boolean isEquivalentTo(Quality other);

    default boolean isBetterOrEquivalentTo(Quality other) {
        return isBetterThan(other) || isEquivalentTo(other);
    }

    default RelativeQuality relativeTo(Quality other) {
        if (other == null || isBetterThan(other)) {
            return RelativeQuality.BETTER;
        } else if (isEquivalentTo(other)) {
            return RelativeQuality.EQUIVALENT;
        } else {
            return RelativeQuality.WORSE;
        }
    }

    @Override
    default int compareTo(Quality other) {
        if (isBetterThan(other)) {
            return 1;
        } else if (isEquivalentTo(other)) {
            return 0;
        } else {
            return -1;
        }
    }
}
