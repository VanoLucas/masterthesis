package com.vanolucas.cppsolver.v12.quality;

/**
 * A quality relative to another.
 */
public enum RelativeQuality {
    BETTER, EQUIVALENT, WORSE
}
