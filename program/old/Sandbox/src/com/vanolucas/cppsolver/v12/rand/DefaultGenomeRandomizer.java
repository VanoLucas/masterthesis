package com.vanolucas.cppsolver.v12.rand;

import com.vanolucas.cppsolver.v12.genome.Genome;

public class DefaultGenomeRandomizer implements GenomeRandomizer {
    private final Rand rand;

    public DefaultGenomeRandomizer(Rand rand) {
        this.rand = rand;
    }

    @Override
    public void randomize(Genome genome) {
        genome.randomize(rand);
    }
}
