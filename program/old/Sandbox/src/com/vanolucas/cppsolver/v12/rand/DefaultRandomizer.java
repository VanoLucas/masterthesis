package com.vanolucas.cppsolver.v12.rand;

public class DefaultRandomizer implements Randomizer<Randomizable> {
    private final Rand rand;

    public DefaultRandomizer(Rand rand) {
        this.rand = rand;
    }

    @Override
    public void randomize(Randomizable randomizable) {
        randomizable.randomize(rand);
    }
}
