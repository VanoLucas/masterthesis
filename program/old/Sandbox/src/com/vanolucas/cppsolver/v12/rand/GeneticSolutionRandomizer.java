package com.vanolucas.cppsolver.v12.rand;

import com.vanolucas.cppsolver.v12.solution.GeneticSolution;
import com.vanolucas.cppsolver.v12.solution.Solution;

public class GeneticSolutionRandomizer implements SolutionRandomizer {
    private final GenomeRandomizer randomizer;

    public GeneticSolutionRandomizer(GenomeRandomizer randomizer) {
        this.randomizer = randomizer;
    }

    @Override
    public void randomize(Solution solution) {
        randomize((GeneticSolution) solution);
    }

    public void randomize(GeneticSolution solution) {
        solution.randomizeGenomeUsing(randomizer);
    }
}
