package com.vanolucas.cppsolver.v12.rand;

import com.vanolucas.cppsolver.v12.genome.Genome;

public interface GenomeRandomizer extends Randomizer<Genome> {
    @Override
    void randomize(Genome genome);

    static GenomeRandomizer getDefault(Rand rand) {
        return new DefaultGenomeRandomizer(rand);
    }
}
