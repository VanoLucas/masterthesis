package com.vanolucas.cppsolver.v12.rand;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Conveniently provides random features based on a regular Java Random numbers generator.
 */
public class Rand {

    /**
     * Random numbers generator.
     */
    private final Random rng;

    public Rand() {
        this(new Random());
    }

    public Rand(long seed) {
        this(new Random(seed));
    }

    public Rand(Random rng) {
        this.rng = rng;
    }

    public int int0To(int maxExclusive) {
        return rng.nextInt(maxExclusive);
    }

    public double doubleBetween(double minInclusive, double maxExclusive) {
        return minInclusive + (maxExclusive - minInclusive) * rng.nextDouble();
    }

    /**
     * @return Random double in the range 0.0d (inclusive) to 1.0d (exclusive).
     */
    public double double01() {
        return rng.nextDouble();
    }

    public List<Double> doubles01(long count) {
        return rng.doubles(count)
                .boxed()
                .collect(Collectors.toList());
    }

    /**
     * @return One item at random from the provided list.
     */
    public <T> T uniformPick(List<T> items) {
        return items.get(
                int0To(items.size())
        );
    }

    /**
     * @param keepProba Probability that an item is kept.
     * @return List of items that contains a subset of the initial item list.
     */
    public <T> List<T> keepWithProba(List<T> items, double keepProba) {
        return items.stream()
                .filter(item -> double01() < keepProba)
                .collect(Collectors.toList());
    }
}
