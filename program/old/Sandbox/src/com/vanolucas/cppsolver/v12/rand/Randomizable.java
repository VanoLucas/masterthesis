package com.vanolucas.cppsolver.v12.rand;

public interface Randomizable {
    void randomize(Rand rand);
}
