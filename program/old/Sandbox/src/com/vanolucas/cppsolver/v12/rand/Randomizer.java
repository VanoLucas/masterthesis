package com.vanolucas.cppsolver.v12.rand;

import java.util.function.Consumer;

public interface Randomizer<T> extends Consumer<T> {
    @Override
    default void accept(T obj) {
        randomize(obj);
    }

    void randomize(T obj);

    static Randomizer<Randomizable> getDefault(Rand rand) {
        return new DefaultRandomizer(rand);
    }
}
