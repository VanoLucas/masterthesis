package com.vanolucas.cppsolver.v12.rand;

import com.vanolucas.cppsolver.v12.solution.SimpleSolution;
import com.vanolucas.cppsolver.v12.solution.Solution;

public class SimpleSolutionRandomizer implements SolutionRandomizer {
    private final Randomizer randomizer;

    public SimpleSolutionRandomizer(Randomizer randomizer) {
        this.randomizer = randomizer;
    }

    @Override
    public void randomize(Solution solution) {
        randomize((SimpleSolution) solution);
    }

    public void randomize(SimpleSolution solution) {
        solution.randomizeActualSolutionUsing(randomizer);
    }
}
