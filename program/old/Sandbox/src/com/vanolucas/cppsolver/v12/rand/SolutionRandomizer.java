package com.vanolucas.cppsolver.v12.rand;

import com.vanolucas.cppsolver.v12.solution.Solution;

public interface SolutionRandomizer extends Randomizer<Solution> {
    @Override
    void randomize(Solution solution);
}
