package com.vanolucas.cppsolver.v12.solution;

import com.vanolucas.cppsolver.v12.genome.Genome;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * A genetic solution that has intermediate representations in the decoding process between the genotype and the phenotype.
 */
public class ComplexGeneticSolution extends GeneticSolution {
    private final ArrayList<Object> intermediates;

    public ComplexGeneticSolution(Genome genome, int nbIntermediates) {
        this(
                genome,
                new ArrayList<>(Collections.nCopies(nbIntermediates, null)),
                null
        );
    }

    public ComplexGeneticSolution(Genome genome, List<Object> intermediates, Object phenotype) {
        super(genome, phenotype);
        if (intermediates instanceof ArrayList) {
            this.intermediates = (ArrayList<Object>) intermediates;
        } else {
            this.intermediates = new ArrayList<>(intermediates);
        }
    }

    public int getNbIntermediates() {
        return intermediates.size();
    }

    @Override
    public Object decodeGenomeUsing(Function decoder) {
        //noinspection unchecked
        final Object decoded = decoder.apply(genome);
        intermediates.set(0, decoded);
        return decoded;
    }

    public List<Object> decodeIntermediatesUsing(List<Function> decoders) {
        return IntStream.range(0, decoders.size())
                .mapToObj(i -> decodeIntermediateUsing(i, decoders.get(i)))
                .collect(Collectors.toList());
    }

    public Object decodeIntermediateUsing(int intermediateIndex, Function decoder) {
        //noinspection unchecked
        final Object decoded = decoder.apply(intermediates.get(intermediateIndex));
        if (intermediateIndex < intermediates.size() - 1) {
            intermediates.set(intermediateIndex + 1, decoded);
        } else {
            phenotype = decoded;
        }
        return decoded;
    }

    @Override
    public String toString() {
        final String strIntermediates = IntStream.range(0, intermediates.size())
                .mapToObj(i -> {
                    if (intermediates.size() >= 2) {
                        return String.format("Intermediate %d:%n" +
                                        "%s",
                                i + 1,
                                intermediates.get(i)
                        );
                    } else {
                        return String.format("Intermediate:%n" +
                                        "%s",
                                intermediates.get(i)
                        );
                    }
                })
                .collect(Collectors.joining(System.lineSeparator()));
        return String.format("Genome:%n" +
                        "%s%n" +
                        "%s%n" +
                        "Phenotype:%n" +
                        "%s",
                genome,
                strIntermediates,
                phenotype
        );
    }
}
