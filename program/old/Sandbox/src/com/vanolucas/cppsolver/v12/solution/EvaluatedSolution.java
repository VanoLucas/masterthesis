package com.vanolucas.cppsolver.v12.solution;

import com.vanolucas.cppsolver.v12.quality.Quality;

public class EvaluatedSolution implements Comparable<EvaluatedSolution> {
    private final Solution solution;
    private final Quality quality;

    public EvaluatedSolution(Solution solution, Quality quality) {
        this.solution = solution;
        this.quality = quality;
    }

    public Solution getSolution() {
        return solution;
    }

    public Quality getQuality() {
        return quality;
    }

    public boolean isBetterThan(Quality quality) {
        return this.quality.isBetterThan(quality);
    }

    public boolean isBetterOrEquivalentTo(Quality quality) {
        return this.quality.isBetterOrEquivalentTo(quality);
    }

    @Override
    public int compareTo(EvaluatedSolution other) {
        return quality.compareTo(other.quality);
    }

    @Override
    public String toString() {
        return String.format("%s%n" +
                        "Quality: %s",
                solution, quality
        );
    }
}
