package com.vanolucas.cppsolver.v12.solution;

import com.vanolucas.cppsolver.v12.clone.GenomeCloner;
import com.vanolucas.cppsolver.v12.eval.EvaluationFunction;
import com.vanolucas.cppsolver.v12.genome.Genome;
import com.vanolucas.cppsolver.v12.mutation.GenomeMutator;
import com.vanolucas.cppsolver.v12.quality.Quality;
import com.vanolucas.cppsolver.v12.rand.GenomeRandomizer;

import java.util.function.Function;

/**
 * A solution that has an encoded genetic (genotype) representation and the corresponding decoded phenotype.
 */
public class GeneticSolution implements Solution {
    protected final Genome genome;
    protected Object phenotype;

    public GeneticSolution(Genome genome) {
        this(genome, null);
    }

    public GeneticSolution(Genome genome, Object phenotype) {
        this.genome = genome;
        this.phenotype = phenotype;
    }

    public Object decodeGenomeUsing(Function decoder) {
        //noinspection unchecked
        phenotype = decoder.apply(genome);
        return phenotype;
    }

    public Quality evaluatePhenotypeUsing(EvaluationFunction evalFn) {
        //noinspection unchecked
        return evalFn.evaluate(phenotype);
    }

    public Genome mutateGenomeUsing(GenomeMutator mutator) {
        genome.mutateUsing(mutator);
        return genome;
    }

    public Genome randomizeGenomeUsing(GenomeRandomizer randomizer) {
        genome.randomizeUsing(randomizer);
        return genome;
    }

    public Genome cloneGenomeUsing(GenomeCloner cloner) throws CloneNotSupportedException {
        return cloner.clone(genome);
    }

    @Override
    public String toString() {
        return String.format("Genome:%n" +
                        "%s%n" +
                        "Phenotype:%n" +
                        "%s",
                genome,
                phenotype
        );
    }
}
