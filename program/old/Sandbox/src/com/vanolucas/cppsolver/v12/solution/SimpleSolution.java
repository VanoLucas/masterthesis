package com.vanolucas.cppsolver.v12.solution;

import com.vanolucas.cppsolver.v12.mutation.Mutator;
import com.vanolucas.cppsolver.v12.quality.Quality;
import com.vanolucas.cppsolver.v12.eval.EvaluationFunction;
import com.vanolucas.cppsolver.v12.rand.Randomizer;

public class SimpleSolution implements Solution {
    private final Object actualSolution;

    public SimpleSolution(Object actualSolution) {
        this.actualSolution = actualSolution;
    }

    public Quality evaluateActualSolutionUsing(EvaluationFunction evalFn) {
        //noinspection unchecked
        return evalFn.evaluate(actualSolution);
    }

    public void mutateActualSolutionUsing(Mutator mutator) {
        //noinspection unchecked
        mutator.mutate(actualSolution);
    }

    public void randomizeActualSolutionUsing(Randomizer randomizer) {
        //noinspection unchecked
        randomizer.randomize(actualSolution);
    }

    @Override
    public String toString() {
        return String.format("%s", actualSolution);
    }
}
