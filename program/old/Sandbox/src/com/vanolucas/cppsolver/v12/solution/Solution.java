package com.vanolucas.cppsolver.v12.solution;

import com.vanolucas.cppsolver.v12.clone.SolutionCloner;
import com.vanolucas.cppsolver.v12.eval.SolutionEvaluationFunction;
import com.vanolucas.cppsolver.v12.mutation.SolutionMutator;
import com.vanolucas.cppsolver.v12.quality.Quality;
import com.vanolucas.cppsolver.v12.rand.SolutionRandomizer;

/**
 * Represents a solution that an optimization algo can work with.
 */
public interface Solution {
    default Quality evaluateUsing(SolutionEvaluationFunction evalFn) {
        return evalFn.evaluate(this);
    }

    default void mutateUsing(SolutionMutator mutator) {
        mutator.mutate(this);
    }

    default void randomizeUsing(SolutionRandomizer randomizer) {
        randomizer.randomize(this);
    }

    default Solution cloneUsing(SolutionCloner cloner) throws CloneNotSupportedException {
        return cloner.clone(this);
    }
}
