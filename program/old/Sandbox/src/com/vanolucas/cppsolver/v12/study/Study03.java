package com.vanolucas.cppsolver.v12.study;

import com.vanolucas.cppsolver.v12.algo.Opti;
import com.vanolucas.cppsolver.v12.algo.OptiStep;
import com.vanolucas.cppsolver.v12.algo.SteepestAscentHillClimbingOptiStep;
import com.vanolucas.cppsolver.v12.algo.listener.DoubleCostMonitor;
import com.vanolucas.cppsolver.v12.algo.listener.IterationEndPauser;
import com.vanolucas.cppsolver.v12.clone.ComplexGeneticSolutionCloner;
import com.vanolucas.cppsolver.v12.clone.GenomeCloner;
import com.vanolucas.cppsolver.v12.clone.SolutionCloner;
import com.vanolucas.cppsolver.v12.eval.*;
import com.vanolucas.cppsolver.v12.genome.RandomKeyVector;
import com.vanolucas.cppsolver.v12.mutation.CompositeSolutionMutator;
import com.vanolucas.cppsolver.v12.mutation.GeneticSolutionMutator;
import com.vanolucas.cppsolver.v12.mutation.RandomizeGenomeMutator;
import com.vanolucas.cppsolver.v12.mutation.SolutionMutator;
import com.vanolucas.cppsolver.v12.mutation.rkv.AddRandValueKeyRkvMutator;
import com.vanolucas.cppsolver.v12.mutation.rkv.ConsecutiveKeysRkvMutator;
import com.vanolucas.cppsolver.v12.mutation.rkv.ProbaKeysRkvMutator;
import com.vanolucas.cppsolver.v12.mutation.rkv.RandomizeKeyRkvMutator;
import com.vanolucas.cppsolver.v12.problem.cpp.eval.CppSolutionEvaluationFunction;
import com.vanolucas.cppsolver.v12.problem.cpp.eval.GenomeCfDecoder;
import com.vanolucas.cppsolver.v12.problem.cpp.eval.SlotsDecoder;
import com.vanolucas.cppsolver.v12.problem.cpp.genome.GenomeCf;
import com.vanolucas.cppsolver.v12.problem.cpp.instance.CppInstance;
import com.vanolucas.cppsolver.v12.problem.cpp.instance.CppInstanceI013;
import com.vanolucas.cppsolver.v12.quality.DoubleCost;
import com.vanolucas.cppsolver.v12.rand.GenomeRandomizer;
import com.vanolucas.cppsolver.v12.rand.Rand;
import com.vanolucas.cppsolver.v12.solution.ComplexGeneticSolution;
import com.vanolucas.cppsolver.v12.solution.Solution;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.LongStream;

/**
 * Study03
 * <p>
 * Protocol:
 * Same as Study02 except we have many more copies of each neighboring operator (10x the Study02).
 * Run the steepest ascent hill climbing for a given number of iterations (15000) multiple times.
 * Keep track of the best cost known at each iteration.
 * <p>
 * Output:
 * Plot the average and median best known per iteration.
 * <p>
 * Opti strategy:
 * Steepest ascent hill climbing.
 * <p>
 * CPP instance:
 * 30 book covers reference instance (10 offset plates).
 * <p>
 * Solution representation:
 * GenomeCf (for each extra slot: the book cover to put on it and how many nb printings to split out of its initial slot).
 * <p>
 * Neighboring operators:
 * - 40 * (randomize the whole genome)
 * - 60 * (randomize 1 random key then randomize 1 random key)
 * - 60 * (2/20 = 10% proba to add a rand value between -0.05 (inclusive) and +0.05 (exclusive) to each key)
 * - 60 * (add a rand value between -0.05 (inclusive) and +0.05 (exclusive) to two random adjacent keys)
 */
public class Study03 {

    public static void main(String[] args) throws IOException {
        new Study03().run();
    }

    private void run() throws IOException {
        final long ITERATIONS_PER_RUN = 150_000L / 10L;
        final int EXPECTED_NB_RUNS = 300;
        final double PRINT_SOLUTIONS_BETTER_THAN_COST = 116528d;

        // CPP instance
        final CppInstance cppInstance = new CppInstanceI013();

        // evaluation
        final int nbThreadsEvaluator = 1;
        final Function genomeDecoder = new GenomeCfDecoder(cppInstance);
        final Function intermediateDecoder = new SlotsDecoder(cppInstance);
        final EvaluationFunction evalFn = new CppSolutionEvaluationFunction(cppInstance);
        final SolutionEvaluationFunction solutionEvalFn = new ComplexGeneticSolutionEvaluationFunction(
                genomeDecoder,
                Collections.singletonList(intermediateDecoder),
                evalFn
        );
        final Evaluator evaluator = new ParallelEvaluator(solutionEvalFn, nbThreadsEvaluator);

        // cloner
        final GenomeCloner genomeCloner = GenomeCloner.getDefault();
        final SolutionCloner solutionCloner = new ComplexGeneticSolutionCloner(genomeCloner);

        // mutators
        final Rand rand = new Rand();
        final List<SolutionMutator> solutionMutators = getMutators(new GenomeCf(cppInstance), rand);

        // hill climbing threads
        final int nbThreadsHillClimbing = 8;

        // metric: for each iteration, the best cost known at each run
        TreeMap<Long, List<Double>> bestPerIteration = new TreeMap<Long, List<Double>>() {{
            putAll(LongStream.range(1L, ITERATIONS_PER_RUN + 1L)
                    .boxed()
                    .collect(Collectors.toMap(
                            iteration -> iteration,
                            iteration -> new ArrayList<Double>(EXPECTED_NB_RUNS)
                    ))
            );
        }};

        // multiple runs of the same algo for empirical measures
        int runCount = 0;
        for (; ; ) {
            // opti algo
            final Opti opti = new Opti();

            // initial solution
            final GenomeCf genome = new GenomeCf(cppInstance);
            final Solution initialSolution = new ComplexGeneticSolution(genome, 1);

            // hill climbing algo
            final OptiStep step = new SteepestAscentHillClimbingOptiStep(
                    opti,
                    evaluator,
                    initialSolution,
                    solutionCloner,
                    solutionMutators,
                    nbThreadsHillClimbing
            );

            // track cost metrics
            DoubleCostMonitor costMonitor = new DoubleCostMonitor(opti);

            // print new better solutions
            opti.enablePrintNewBetterSolutions(new DoubleCost(PRINT_SOLUTIONS_BETTER_THAN_COST));

            // run opti for a limited nb of iterations
            new IterationEndPauser(opti, ITERATIONS_PER_RUN);
            opti.run();
            runCount++;

            // store metric
            Map<Long, Double> bestCostHistory = costMonitor.getBestKnownCostPerIteration();
            bestCostHistory.keySet().forEach(iteration ->
                    bestPerIteration.get(iteration)
                            .add(bestCostHistory.get(iteration)));

            // calculate average best per iteration
            Map<Long, Double> avgBestPerIteration = bestPerIteration.keySet().stream()
                    .collect(Collectors.toMap(
                            iteration -> iteration,
                            iteration -> bestPerIteration.get(iteration).stream()
                                    .mapToDouble(__ -> __)
                                    .average()
                                    .getAsDouble()
                    ));

            // calculate median best per iteration
            Map<Long, Double> medianBestPerIteration = bestPerIteration.keySet().stream()
                    .collect(Collectors.toMap(
                            iteration -> iteration,
                            iteration -> median(bestPerIteration.get(iteration))
                    ));

            // print run count
            System.out.println("Run count: " + runCount);

            // print average and median best per iteration
            System.out.println(avgBestPerIteration.keySet().stream()
                    .filter(iteration -> iteration % (ITERATIONS_PER_RUN / 1000) == 0 || iteration == 1)
                    .map(iteration -> String.format("%7d:\tavg = %f\tmedian = %f",
                            iteration,
                            avgBestPerIteration.get(iteration),
                            medianBestPerIteration.get(iteration)
                    ))
                    .collect(Collectors.joining(System.lineSeparator()))
            );
        }
    }

    /**
     * @return Neighboring operators.
     */
    private List<SolutionMutator> getMutators(RandomKeyVector genome, Rand rand) {
        final List<SolutionMutator> solutionMutators = new ArrayList<>();
        // randomize whole genome
        solutionMutators.addAll(Collections.nCopies(40, new GeneticSolutionMutator(
                new RandomizeGenomeMutator(GenomeRandomizer.getDefault(rand))
        )));
        // randomize 1 random key then 1 again
        solutionMutators.addAll(Collections.nCopies(60, new CompositeSolutionMutator(
                new GeneticSolutionMutator(new ConsecutiveKeysRkvMutator(1,
                        new RandomizeKeyRkvMutator(rand),
                        rand)),
                new GeneticSolutionMutator(new ConsecutiveKeysRkvMutator(1,
                        new RandomizeKeyRkvMutator(rand),
                        rand))
        )));
        // proba to add rand value to each key
        solutionMutators.addAll(Collections.nCopies(60, new GeneticSolutionMutator(
                new ProbaKeysRkvMutator(2d / genome.length(),
                        new AddRandValueKeyRkvMutator(-0.05, +0.05, rand),
                        rand)
        )));
        // add rand value to 2 consecutive keys
        solutionMutators.addAll(Collections.nCopies(60, new GeneticSolutionMutator(
                new ConsecutiveKeysRkvMutator(2,
                        new AddRandValueKeyRkvMutator(-0.05, +0.05, rand),
                        rand)
        )));
        return solutionMutators;
    }

    private double median(List<Double> values) {
        final int size = values.size();
        final DoubleStream sorted = values.stream().mapToDouble(__ -> __).sorted();
        return size % 2 == 0 ?
                sorted.skip(size / 2 - 1).limit(2).average().getAsDouble() :
                sorted.skip(size / 2).findFirst().getAsDouble();
    }
}
