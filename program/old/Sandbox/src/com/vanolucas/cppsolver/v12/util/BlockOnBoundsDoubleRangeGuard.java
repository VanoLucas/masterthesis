package com.vanolucas.cppsolver.v12.util;

public class BlockOnBoundsDoubleRangeGuard extends DoubleRangeGuard {
    public BlockOnBoundsDoubleRangeGuard(double min, double max) {
        super(min, max);
    }

    @Override
    public double bringBackToRange(double value, double minInclusive, double maxExclusive) {
        // bring superior values back to the max allowed value
        if (value >= maxExclusive) {
            return Math.nextDown(maxExclusive);
        }
        // bring inferior values back to the min allowed value
        else if (value < minInclusive) {
            return minInclusive;
        }
        // keep values in range
        else {
            return value;
        }
    }
}
