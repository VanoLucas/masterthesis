package com.vanolucas.cppsolver.v12.util;

public abstract class DoubleRangeGuard {
    private final double min; // inclusive
    private final double max; // exclusive

    public DoubleRangeGuard(double min, double max) {
        this.min = min;
        this.max = max;
    }

    public double bringBackToRange(double value) {
        return bringBackToRange(value, min, max);
    }

    public abstract double bringBackToRange(double value, double minInclusive, double maxExclusive);

    public static BlockOnBoundsDoubleRangeGuard getDefault(double minInclusive, double maxExclusive) {
        return new BlockOnBoundsDoubleRangeGuard(minInclusive, maxExclusive);
    }
}
