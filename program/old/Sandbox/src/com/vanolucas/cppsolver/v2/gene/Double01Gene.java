package com.vanolucas.cppsolver.old.v2.gene;

public class Double01Gene extends DoubleGene {

    public Double01Gene(Double value) {
        super(value);
    }

    @Override
    public void setValue(Number value) {
        // check validity
        assertValidValue(value);
        // set the new valid value
        super.setValue(value);
    }

    /**
     * Check that the provided value is a Double in the range 0d (inclusive) to 1d (exclusive) or null.
     */
    @Override
    protected void assertValidValue(Number value) {
        // check double type
        super.assertValidValue(value);
        // check in range [0..1)
        assertValidValue((Double) value);
    }

    /**
     * Check that the provided double value is in the range 0d (inclusive) to 1d (exclusive) or null.
     */
    private void assertValidValue(Double value) {
        if (value != null) {
            // check >= 0d
            if (value < 0.0d) {
                throw new IllegalArgumentException(String.format(
                        "Cannot store the negative value %f in a %s.",
                        value,
                        getClass().getSimpleName()
                ));
            }
            // check < 1d
            else if (value >= 1.0d) {
                throw new IllegalArgumentException(String.format(
                        "Cannot store the value %f in %s because it is >= 1.0d.",
                        value,
                        getClass().getSimpleName()
                ));
            }
        }
    }
}
