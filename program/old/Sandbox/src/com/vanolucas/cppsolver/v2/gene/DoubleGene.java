package com.vanolucas.cppsolver.old.v2.gene;

public class DoubleGene extends NumberGene {

    public DoubleGene(Double value) {
        super(value);
    }

    @Override
    public Double getValue() {
        return (Double) super.getValue();
    }

    @Override
    public void setValue(Number value) {
        assertValidValue(value);
        super.setValue(value);
    }

    /**
     * Check that the value is a Double.
     */
    protected void assertValidValue(Number value) {
        if (!(value instanceof Double)) {
            throw new IllegalArgumentException(String.format(
                    "The number value in a %s must be a Double.",
                    getClass().getSimpleName()
            ));
        }
    }
}
