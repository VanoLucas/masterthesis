package com.vanolucas.cppsolver.old.v3.opti.algo;

import com.vanolucas.cppsolver.old.v3.opti.solution.SolutionWithQuality;
import com.vanolucas.cppsolver.old.v3.util.observable.Observable;

/**
 * Runs an iterative optimization algorithm.
 */
public class Algo {

    /**
     * Current iteration of this opti algo. 0 when not started yet.
     */
    private long iteration = 0L;
    /**
     * The implementation of a specific opti algo iteration.
     */
    private Step step;
    /**
     * Object in charge of keeping the best solutions.
     */
    private BestKeeper bestKeeper;
    /**
     * Whether or not we should pause this opti algo before starting the next iteration.
     */
    private boolean pauseBeforeNextIteration = false;
    /**
     * Makes this opti algo observable so that listeners can react to its events.
     */
    private Observable<AlgoListener> observable = new Observable<>(4);

    public Algo(Step step, BestKeeper bestKeeper) {
        setStep(step);
        setBestKeeper(bestKeeper);
    }

    public void setStep(Step step) {
        // remove any old step
        if (this.step != null) {
            this.step.removeListener(this.stepListener);
        }
        // set the new step
        this.step = step;
        this.step.addListener(this.stepListener);
    }

    public void setBestKeeper(BestKeeper bestKeeper) {
        // remove any old bestKeeper
        if (this.bestKeeper != null) {
            this.bestKeeper.removeListener(this.bestKeeperListener);
        }
        // set the new bestKeeper
        this.bestKeeper = bestKeeper;
        this.bestKeeper.addListener(this.bestKeeperListener);
    }

    /**
     * Run this opti algo until it reaches a pause criterion.
     */
    public void run() {
        // loop over optimization iterations
        for (; ; ) {
            // exit this loop if we need to pause before starting the next iteration
            if (pauseBeforeNextIteration) {
                onPause();
                return;
            }

            // perform the next opti iteration
            step.run(++iteration);
        }
    }

    /**
     * Ask this opti algo to pause before starting the next iteration.
     */
    public void pauseBeforeNextIteration() {
        // set the flag that requests a pause
        this.pauseBeforeNextIteration = true;
    }

    /**
     * Actions to perform just before pausing this opti algo.
     */
    private void onPause() {
        // reset the pause flag
        this.pauseBeforeNextIteration = false;
        // notify listeners that we are going to pause
        observable.notifyListeners(listener ->
                listener.onPause(iteration)
        );
    }

    /**
     * Handle events of the step.
     */
    private StepListener stepListener = new StepListener() {
        /**
         * @param iteration Iteration just started by the step.
         */
        @Override
        public void onStepStart(long iteration) {
            // notify listeners that we started a new iteration
            observable.notifyListeners(listener ->
                    listener.onIterationStart(iteration)
            );
        }

        /**
         * @param iteration Iteration just finished by the step.
         */
        @Override
        public void onStepEnd(long iteration) {
            // notify listeners that we finished an iteration
            observable.notifyListeners(listener ->
                    listener.onIterationEnd(iteration)
            );
        }

        /**
         * @param candidate Solution proposed by the step.
         */
        @Override
        public void onSolutionProposal(long iteration, SolutionWithQuality candidate) {
            // propose this candidate solution to our bestKeeper
            bestKeeper.propose(candidate);
        }
    };

    /**
     * Handle events of the bestKeeper.
     */
    private BestKeeperListener bestKeeperListener = new BestKeeperListener() {
        /**
         * @param betterSolution Solution declared as better by the bestKeeper.
         */
        @Override
        public void onBetterSolution(SolutionWithQuality betterSolution) {
            // notify listeners that we found a better solution
            observable.notifyListeners(listener ->
                    listener.onBetterSolution(iteration, betterSolution)
            );
        }
    };
}
