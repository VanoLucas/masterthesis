package com.vanolucas.cppsolver.old.v3.opti.algo;

import com.vanolucas.cppsolver.old.v3.opti.solution.SolutionWithQuality;

public interface AlgoListener {

    default void onIterationStart(long iteration) {
    }

    default void onIterationEnd(long iteration) {
    }

    default void onPause(long lastIteration) {
    }

    default void onBetterSolution(long iteration, SolutionWithQuality betterSolution) {
    }
}
