package com.vanolucas.cppsolver.old.v3.opti.algo;

import com.vanolucas.cppsolver.old.v3.opti.solution.SolutionWithQuality;
import com.vanolucas.cppsolver.old.v3.util.observable.Observable;

/**
 * Runs one step of a specific iterative optimization algorithm.
 */
public abstract class Step {

    private long iteration;

    /**
     * Make this Step algo observable so that listeners car react to our events.
     */
    private Observable<StepListener> observable = new Observable<>(2);

    public void addListener(StepListener listener) {
        observable.addListener(listener);
    }

    public void removeListener(StepListener listener) {
        observable.removeListener(listener);
    }

    /**
     * Run this optimization step and let listeners know that we do.
     *
     * @param iteration Current iteration number.
     */
    public void run(long iteration) {
        this.iteration = iteration;

        // notify listeners that we start a new step
        observable.notifyListeners(listener ->
                listener.onStepStart(iteration)
        );

        // run this step
        runStep();

        // notify listeners that we finished this step
        observable.notifyListeners(listener ->
                listener.onStepEnd(iteration)
        );
    }

    /**
     * Run this specific implementation of an opti step.
     */
    protected abstract void runStep();

    /**
     * @param solution Solution proposed by this opti algo iteration.
     */
    protected void proposeSolution(SolutionWithQuality solution) {
        // notify listeners that this opti algo iteration is proposing a solution
        observable.notifyListeners(listener ->
                listener.onSolutionProposal(iteration, solution)
        );
    }
}
