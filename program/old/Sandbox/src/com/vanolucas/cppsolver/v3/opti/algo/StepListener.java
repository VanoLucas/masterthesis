package com.vanolucas.cppsolver.old.v3.opti.algo;

import com.vanolucas.cppsolver.old.v3.opti.solution.SolutionWithQuality;

public interface StepListener {

    default void onStepStart(long iteration) {
    }

    default void onStepEnd(long iteration) {
    }

    default void onSolutionProposal(long iteration, SolutionWithQuality candidate) {
    }
}
