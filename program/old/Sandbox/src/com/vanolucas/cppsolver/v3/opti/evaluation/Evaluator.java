package com.vanolucas.cppsolver.old.v3.opti.evaluation;

import com.vanolucas.cppsolver.old.v3.opti.quality.Quality;
import com.vanolucas.cppsolver.old.v3.opti.solution.Solution;

/**
 * Evaluates a Solution and returns its Quality.
 */
public interface Evaluator {

    Quality evaluate(Solution solution);
}
