package com.vanolucas.cppsolver.old.v3.opti.solution;

import com.vanolucas.cppsolver.old.v3.opti.quality.Quality;

/**
 * A Solution and its Quality.
 */
public class SolutionWithQuality {

    private Solution solution;
    private Quality quality;

    public SolutionWithQuality(Solution solution, Quality quality) {
        this.solution = solution;
        this.quality = quality;
    }

    public Solution getSolution() {
        return solution;
    }

    public Quality getQuality() {
        return quality;
    }

    public void setQuality(Quality quality) {
        this.quality = quality;
    }
}
