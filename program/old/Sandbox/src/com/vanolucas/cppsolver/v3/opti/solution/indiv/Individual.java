package com.vanolucas.cppsolver.old.v3.opti.solution.indiv;

import com.vanolucas.cppsolver.old.v3.opti.solution.Solution;
import com.vanolucas.cppsolver.old.v3.opti.solution.genotype.Genotype;

/**
 * A solution that has a genetic representation.
 * The genetic representation (genotype) defines a corresponding decoded representation (phenotype).
 * The phenotype can be converted to the actual problem-specific solution object.
 */
public class Individual<TSolution> implements Solution {

    /**
     * Genetic representation of this solution.
     */
    private Genotype genotype;
    /**
     * Representation of this solution decoded from the genotype.
     */
    private Phenotype phenotype;
    /**
     * Actual described solution.
     */
    private TSolution solution;
}
