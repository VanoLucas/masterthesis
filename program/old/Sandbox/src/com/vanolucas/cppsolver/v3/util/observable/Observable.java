package com.vanolucas.cppsolver.old.v3.util.observable;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Ability to be observed.
 * This has listeners that can be notified and react about events.
 *
 * @param <TListener> Type of listeners that this Observable can store and call.
 */
public class Observable<TListener> {

    private List<TListener> listeners;

    public Observable() {
        this(2);
    }

    public Observable(int expectedNbListeners) {
        this.listeners = new ArrayList<>(expectedNbListeners);
    }

    public void addListener(TListener listener) {
        listeners.add(listener);
    }

    public void removeListener(TListener listener) {
        listeners.remove(listener);
    }

    public void removeAllListeners() {
        listeners.clear();
    }

    public void notifyListeners(Consumer<TListener> action) {
        listeners.forEach(action);
    }

    public <T extends TListener> void notifyListeners(Class<T> listenerClass, Consumer<T> action) {
        listeners.forEach(listener -> {
            if (listenerClass.isInstance(listener)) {
                action.accept((T) listener);
            }
        });
    }
}
