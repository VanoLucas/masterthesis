package com.vanolucas.cppsolver.old.v3.util.randomizable;

import com.vanolucas.cppsolver.old.v3.util.math.Rand;

public interface Randomizable {

    void randomize(Rand rand);

    default void randomizeUsing(Randomizer randomizer) {
        randomizer.randomize(this);
    }

    default void randomizeUsing(Randomizer randomizer, Rand rand) {
        randomizer.randomize(this, rand);
    }
}
