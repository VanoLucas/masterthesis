package com.vanolucas.cppsolver.old.v4.cpp.evaluation;

import com.vanolucas.cppsolver.old.v4.cpp.phenotype.AllocatedSlots;
import com.vanolucas.cppsolver.old.v4.cpp.solution.CppSolution;
import com.vanolucas.cppsolver.old.v4.cpp.solution.OffsetPlatePrintings;
import com.vanolucas.cppsolver.old.v4.util.converter.Converter;

import java.util.ArrayList;
import java.util.List;

/**
 * Converts an AllocatedSlot phenotype to the corresponding CPP solution.
 */
public class AllocatedSlots2CppSolution implements Converter<AllocatedSlots, CppSolution> {
    private final List<Integer> demand;
    private final int nbOffsetPlates;
    private final int nbSlotsPerPlate;

    public AllocatedSlots2CppSolution(List<Integer> demand, int nbOffsetPlates, int nbSlotsPerPlate) {
        this.demand = demand;
        this.nbOffsetPlates = nbOffsetPlates;
        this.nbSlotsPerPlate = nbSlotsPerPlate;
    }

    @Override
    public CppSolution convert(AllocatedSlots slots) {
        List<OffsetPlatePrintings> offsetPlatePrintings = new ArrayList<>(nbOffsetPlates);

        // deduce offset plate printings from the allocated slots
        int cursorSlot = 0;
        for (int i = 0; i < nbOffsetPlates; i++) {
            // create the offset plate printings object from our slots
            offsetPlatePrintings.add(
                    new OffsetPlatePrintings(
                            slots.getSlots(cursorSlot, cursorSlot + nbSlotsPerPlate)
                    )
            );
            // advance to slots for the next offset plate
            cursorSlot += nbSlotsPerPlate;
        }

        // create our standard CPP solution representation with our filled offset plates
        return new CppSolution(demand, offsetPlatePrintings);
    }
}
