package com.vanolucas.cppsolver.old.v4.cpp.genotype;

import com.vanolucas.cppsolver.old.v4.cpp.instance.CppInstance;
import com.vanolucas.cppsolver.old.v4.opti.genotype.RandomKeyVector;

public class GenomeCP extends RandomKeyVector {

    public GenomeCP(CppInstance cppInstance) {
        this(lengthForCppInstance(cppInstance));
    }

    public GenomeCP(int length) {
        super(length);
    }

    /**
     * The length of a type CP genome is 2 * (n.S - m).
     *
     * @return The size of the CP genome for the provided CPP instance.
     */
    public static int lengthForCppInstance(CppInstance cppInstance) {
        return (cppInstance.getTotalNbSlots() - cppInstance.getNbBookCovers()) * 2;
    }
}
