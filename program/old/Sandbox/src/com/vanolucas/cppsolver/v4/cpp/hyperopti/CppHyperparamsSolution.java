package com.vanolucas.cppsolver.old.v4.cpp.hyperopti;

import com.vanolucas.cppsolver.old.v4.cpp.instance.CppInstance30BookCovers;
import com.vanolucas.cppsolver.old.v4.cpp.problem.CppGeneticProblemCP;
import com.vanolucas.cppsolver.old.v4.opti.algo.Algo;
import com.vanolucas.cppsolver.old.v4.opti.algo.AlgoListener;
import com.vanolucas.cppsolver.old.v4.opti.mutation.RandomizeRandomizableMutation;
import com.vanolucas.cppsolver.old.v4.opti.mutation.mutator.IndividualGenotypeMutator;
import com.vanolucas.cppsolver.old.v4.opti.mutation.mutator.Mutator;
import com.vanolucas.cppsolver.old.v4.opti.mutation.mutator.RepeatedMutator;
import com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector.AllKeysMutation;
import com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector.OneRandomKeyMutation;
import com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector.RandomConsecutiveKeysMutation;
import com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector.key.AddRandomValueKeyMutation;
import com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector.key.RandomizeKeyMutation;
import com.vanolucas.cppsolver.old.v4.opti.problem.Problem;
import com.vanolucas.cppsolver.old.v4.opti.quality.IntCost;
import com.vanolucas.cppsolver.old.v4.opti.quality.Quality;
import com.vanolucas.cppsolver.old.v4.opti.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v4.util.rand.Rand;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class CppHyperparamsSolution {
    private int nbRunsPerEval;
    private List<Double> hyperparams;
    private int maxEvalPerRun;
    private List<Integer> bestCosts;

    public CppHyperparamsSolution(int nbRunsPerEval, List<Double> hyperparams, int maxEvalPerRun) {
        this.nbRunsPerEval = nbRunsPerEval;
        this.hyperparams = hyperparams;
        this.maxEvalPerRun = maxEvalPerRun;
    }

    public List<Integer> getBestCosts() throws ExecutionException, InterruptedException {
        runEvaluationIfNeeded();
        return bestCosts;
    }

    public int getMedianBestCost() throws ExecutionException, InterruptedException {
        return getBestCosts().get(bestCosts.size() / 2);
    }

    public double getAverageBestCost() throws ExecutionException, InterruptedException {
        return getBestCosts().stream().mapToDouble(__ -> __).average().getAsDouble();
    }

    private void runEvaluationIfNeeded() throws ExecutionException, InterruptedException {
        if (bestCosts == null) {
            bestCosts = new ArrayList<>(nbRunsPerEval);
            for (int runId = 0; runId < nbRunsPerEval; runId++) {
                bestCosts.add(runAlgoOnceAndGetBestCost());
            }
            bestCosts.sort(Comparator.naturalOrder());
        }
    }

    private int runAlgoOnceAndGetBestCost() throws ExecutionException, InterruptedException {
        List<Integer> mutatorCount = hyperparams.stream()
                .map(Double::intValue)
                .collect(Collectors.toList());
        List<Mutator> mutators = new ArrayList<>();
        int mutatorIndex = 0;

        // full randomization of the genome
        mutators.addAll(Collections.nCopies(mutatorCount.get(mutatorIndex++),
                new IndividualGenotypeMutator(new RandomizeRandomizableMutation(new Rand()))
        ));
        // add rand value to all keys
        mutators.addAll(Collections.nCopies(mutatorCount.get(mutatorIndex++),
                new IndividualGenotypeMutator(new AllKeysMutation(new AddRandomValueKeyMutation(-0.01d, +0.01d, new Rand())))
        ));
        mutators.addAll(Collections.nCopies(mutatorCount.get(mutatorIndex++),
                new IndividualGenotypeMutator(new AllKeysMutation(new AddRandomValueKeyMutation(-0.001d, +0.001d, new Rand())))
        ));
        mutators.addAll(Collections.nCopies(mutatorCount.get(mutatorIndex++),
                new IndividualGenotypeMutator(new AllKeysMutation(new AddRandomValueKeyMutation(-0.0001d, +0.0001d, new Rand())))
        ));
        // randomize 1 random key
        mutators.addAll(Collections.nCopies(mutatorCount.get(mutatorIndex++),
                new IndividualGenotypeMutator(new OneRandomKeyMutation(new RandomizeKeyMutation(new Rand())))
        ));
        // add rand value to 1 random key
        mutators.addAll(Collections.nCopies(mutatorCount.get(mutatorIndex++),
                new IndividualGenotypeMutator(new OneRandomKeyMutation(new AddRandomValueKeyMutation(-0.01d, +0.01d, new Rand())))
        ));
        mutators.addAll(Collections.nCopies(mutatorCount.get(mutatorIndex++),
                new IndividualGenotypeMutator(new OneRandomKeyMutation(new AddRandomValueKeyMutation(-0.001d, +0.001d, new Rand())))
        ));
        mutators.addAll(Collections.nCopies(mutatorCount.get(mutatorIndex++),
                new IndividualGenotypeMutator(new OneRandomKeyMutation(new AddRandomValueKeyMutation(-0.0001d, +0.0001d, new Rand())))
        ));
        // randomize 2 random consecutive keys
        mutators.addAll(Collections.nCopies(mutatorCount.get(mutatorIndex++),
                new IndividualGenotypeMutator(new RandomConsecutiveKeysMutation(2, new RandomizeKeyMutation(new Rand()), new Rand()))
        ));
        // add rand value to 2 random consecutive keys
        mutators.addAll(Collections.nCopies(mutatorCount.get(mutatorIndex++),
                new IndividualGenotypeMutator(new RandomConsecutiveKeysMutation(2, new AddRandomValueKeyMutation(-0.01d, +0.01d, new Rand())))
        ));
        mutators.addAll(Collections.nCopies(mutatorCount.get(mutatorIndex++),
                new IndividualGenotypeMutator(new RandomConsecutiveKeysMutation(2, new AddRandomValueKeyMutation(-0.001d, +0.001d, new Rand())))
        ));
        mutators.addAll(Collections.nCopies(mutatorCount.get(mutatorIndex++),
                new IndividualGenotypeMutator(new RandomConsecutiveKeysMutation(2, new AddRandomValueKeyMutation(-0.0001d, +0.0001d, new Rand())))
        ));
        // add rand value to 2 random keys
        mutators.addAll(Collections.nCopies(mutatorCount.get(mutatorIndex++),
                new RepeatedMutator<>(2,
                        new IndividualGenotypeMutator(new OneRandomKeyMutation(new AddRandomValueKeyMutation(-0.01d, +0.01d, new Rand())))
                )
        ));
        mutators.addAll(Collections.nCopies(mutatorCount.get(mutatorIndex++),
                new RepeatedMutator<>(2,
                        new IndividualGenotypeMutator(new OneRandomKeyMutation(new AddRandomValueKeyMutation(-0.001d, +0.001d, new Rand())))
                )
        ));
        mutators.addAll(Collections.nCopies(mutatorCount.get(mutatorIndex++),
                new RepeatedMutator<>(2,
                        new IndividualGenotypeMutator(new OneRandomKeyMutation(new AddRandomValueKeyMutation(-0.0001d, +0.0001d, new Rand())))
                )
        ));

        Problem problem = new CppGeneticProblemCP(new CppInstance30BookCovers(10, 4));

//        Algo algo = problem.newHillClimbingAlgo(mutators);
        Algo algo = problem.newHillClimbingAlgo(
                problem.newHillClimbingStep(
                        problem.newSteepestAscentHillClimbingStrategy(mutators)
                )
        );

        class AlgoHandler implements AlgoListener {
            private int countEval = 0;
            private Quality bestQuality = null;

            private Quality getBestQuality() {
                return bestQuality;
            }

            @Override
            public synchronized void onEvaluated(EvaluatedSolution evaluatedSolution, long iteration) {
                countEval++;
                if (countEval >= maxEvalPerRun - mutators.size()) {
                    algo.pauseBeforeNextIteration();
                }
            }

            @Override
            public synchronized void onBetterSolution(EvaluatedSolution betterSolution, long iteration) {
                if (bestQuality == null || betterSolution.isBetterThan(bestQuality)) {
                    bestQuality = betterSolution.getQuality();
//                    // log best solutions
//                    System.out.println(String.format("\n[%s]\n" +
//                                    "New better solution at iteration %d:\n" +
//                                    "%s",
//                            Instant.now(),
//                            iteration,
//                            betterSolution.getSolution()
//                    ));
                }
            }
        }

        AlgoHandler algoHandler = new AlgoHandler();
        algo.addListener(algoHandler);

        algo.run();

        // return the best cost found during this algo run
        return ((IntCost) algoHandler.getBestQuality()).getValue();
    }

    @Override
    public String toString() {
        try {
            return String.format("Hyperparams: %s\n" +
                            "Best costs: %s\n" +
                            "Avg best cost: %d\n" +
                            "Median best cost: %d",
                    hyperparams.stream().map(Double::intValue).collect(Collectors.toList()),
                    bestCosts,
                    (int) getAverageBestCost(),
                    getMedianBestCost()
            );
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
            throw new IllegalStateException(String.format("Error occurred when evaluating a %s.",
                    getClass().getSimpleName()
            ));
        }
    }
}
