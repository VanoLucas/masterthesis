package com.vanolucas.cppsolver.old.v4.cpp.hyperopti;

import com.vanolucas.cppsolver.old.v4.util.converter.Converter;

import java.util.List;

public class ListDouble2CppHyperparamsSolution implements Converter<List<Double>, CppHyperparamsSolution> {
    private final int nbRunsPerEval;
    private final int maxEvalPerRun;

    public ListDouble2CppHyperparamsSolution(int nbRunsPerEval, int maxEvalPerRun) {
        this.nbRunsPerEval = nbRunsPerEval;
        this.maxEvalPerRun = maxEvalPerRun;
    }

    @Override
    public CppHyperparamsSolution convert(List<Double> hyperparameters) {
        return new CppHyperparamsSolution(nbRunsPerEval, hyperparameters, maxEvalPerRun);
    }
}
