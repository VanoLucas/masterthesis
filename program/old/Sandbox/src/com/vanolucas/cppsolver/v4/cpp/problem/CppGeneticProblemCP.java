package com.vanolucas.cppsolver.old.v4.cpp.problem;

import com.vanolucas.cppsolver.old.v4.cpp.evaluation.DecoderCP;
import com.vanolucas.cppsolver.old.v4.cpp.genotype.GenomeCP;
import com.vanolucas.cppsolver.old.v4.cpp.instance.CppInstance;
import com.vanolucas.cppsolver.old.v4.cpp.phenotype.AllocatedSlots;
import com.vanolucas.cppsolver.old.v4.opti.genotype.GenotypeDecoder;

public class CppGeneticProblemCP extends CppGeneticProblem<GenomeCP> {
    public CppGeneticProblemCP(CppInstance cppInstance) {
        super(cppInstance);
    }

    @Override
    public GenomeCP newGenotype() {
        return new GenomeCP(cppInstance);
    }

    @Override
    public GenotypeDecoder<GenomeCP, AllocatedSlots> newGenotypeDecoder() {
        return new DecoderCP(cppInstance);
    }
}
