package com.vanolucas.cppsolver.old.v4.cpp.solution;

import com.vanolucas.cppsolver.old.v4.cpp.instance.CppInstance;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Stores a solution of the Cover Printing Problem.
 */
public class CppSolution {
    private List<Integer> demand;
    /**
     * Offset plates and their number of printings.
     */
    private List<OffsetPlatePrintings> offsetPlatePrintings;
    private List<Integer> actualPrintings;

    public CppSolution(CppInstance cppInstance, Collection<OffsetPlatePrintings> offsetPlatePrintings) {
        this(cppInstance.getCopies(), offsetPlatePrintings);
    }

    public CppSolution(List<Integer> demand, Collection<OffsetPlatePrintings> offsetPlatePrintings) {
        this.demand = demand;
        setOffsetPlatePrintings(offsetPlatePrintings);
    }

    public void setOffsetPlatePrintings(Collection<OffsetPlatePrintings> offsetPlatePrintings) {
        // sort offset plates by number of printings descending
        List<OffsetPlatePrintings> sortedOffsetPlatePrintings = new ArrayList<>(offsetPlatePrintings);
        sortedOffsetPlatePrintings.sort(Comparator.reverseOrder());
        this.offsetPlatePrintings = sortedOffsetPlatePrintings;
    }

    public int getTotalNbPrintings() {
        return offsetPlatePrintings.stream()
                .mapToInt(OffsetPlatePrintings::getNbPrintings)
                .sum();
    }

    public int getCost() {
        return getTotalNbPrintings();
    }

    /**
     * @return The actual number of printings of each book cover produced with this solution.
     */
    public List<Integer> getActualPrintings() {
        if (actualPrintings == null) {
            Map<Integer, Integer> actualPrintingsForEachBookCover = new HashMap<>(demand.size());
            offsetPlatePrintings.forEach(offsetPlatePrintings -> {
                final List<Integer> bookCoversOnThisOffsetPlate = offsetPlatePrintings.getOffsetPlate().getBookCoverIndexes();
                final int nbPrintings = offsetPlatePrintings.getNbPrintings();
                for (int bookCoverIndex : bookCoversOnThisOffsetPlate) {
                    final int existingPrintings = actualPrintingsForEachBookCover.getOrDefault(bookCoverIndex, 0);
                    actualPrintingsForEachBookCover.put(bookCoverIndex, existingPrintings + nbPrintings);
                }
            });
            actualPrintings = IntStream.range(0, demand.size())
                    .mapToObj(actualPrintingsForEachBookCover::get)
                    .collect(Collectors.toList());
        }
        return actualPrintings;
    }

    /**
     * @return For each book cover, how many excess copies are produced if realizing this solution.
     */
    public List<Integer> getWaste() {
        final List<Integer> actualPrintings = getActualPrintings();
        return IntStream.range(0, demand.size())
                .mapToObj(bookCoverIndex -> actualPrintings.get(bookCoverIndex) - demand.get(bookCoverIndex))
                .collect(Collectors.toList());
    }

    public int getTotalWaste() {
        return getWaste().stream().mapToInt(__ -> __).sum();
    }

    @Override
    public String toString() {
        final String strOffsetPlates = offsetPlatePrintings.stream()
                .map(OffsetPlatePrintings::toString)
                .collect(Collectors.joining("\n"));

        final String strActualPrintings = IntStream.range(0, demand.size())
                .mapToObj(bookCoverIndex -> String.format("%3d: %6d / %6d printings (%d extra)",
                        bookCoverIndex + 1,
                        getActualPrintings().get(bookCoverIndex),
                        demand.get(bookCoverIndex),
                        getWaste().get(bookCoverIndex)
                ))
                .collect(Collectors.joining("\n"));

        return String.format("Offset plates:\n" +
                        "%s\n" +
                        "Actual printings:\n" +
                        "%s\n" +
                        "Total waste: %d\n" +
                        "Cost = %d",
                strOffsetPlates,
                strActualPrintings,
                getTotalWaste(),
                getCost()
        );
    }
}
