package com.vanolucas.cppsolver.old.v4.cpp.solution;

import com.vanolucas.cppsolver.old.v4.cpp.phenotype.SlotPrintings;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents an offset plate.
 * An offset plate has S slots.
 * Each slot can be assigned a book cover index.
 */
public class OffsetPlate {
    /**
     * Slots of this offset plate.
     * Each slot can be assigned a book cover index.
     */
    private List<Integer> slots;

    public OffsetPlate(Collection<SlotPrintings> slots) {
        this(
                // get the allocated book cover index of each slot
                slots.stream()
                        .map(SlotPrintings::getBookCoverIndex)
                        .collect(Collectors.toList())
        );
    }

    public OffsetPlate(List<Integer> slots) {
        this.slots = slots;
    }

    public List<Integer> getBookCoverIndexes() {
        return slots;
    }

    @Override
    public String toString() {
        return slots.stream()
                .sorted()
                .map(bookCoverIndex -> String.format("%3d", bookCoverIndex != null ? bookCoverIndex + 1 : -99))
                .collect(Collectors.joining("|"));
    }
}
