package com.vanolucas.cppsolver.old.v4.opti.algo;

import com.vanolucas.cppsolver.old.v4.opti.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v4.util.observable.Listeners;
import com.vanolucas.cppsolver.old.v4.util.observable.Observable;

import java.util.concurrent.ExecutionException;

public abstract class Step implements Observable<StepListener> {
    protected Listeners<StepListener> listeners = new Listeners<>(3);

    public void run(long iteration) throws ExecutionException, InterruptedException {
        listeners.notifyListeners(listener ->
                listener.onStepStart(iteration)
        );
        runStep(iteration);
        listeners.notifyListeners(listener ->
                listener.onStepEnd(iteration)
        );
    }

    protected abstract void runStep(long iteration) throws ExecutionException, InterruptedException;

    protected void proposeSolution(EvaluatedSolution evaluatedSolution) {
        listeners.notifyListeners(listener ->
                listener.onSolutionProposal(evaluatedSolution)
        );
    }

    @Override
    public void addListener(StepListener stepListener) {
        listeners.addListener(stepListener);
    }

    @Override
    public void removeListener(StepListener stepListener) {
        listeners.removeListener(stepListener);
    }

    @Override
    public void removeAllListeners() {
        listeners.removeAllListeners();
    }
}
