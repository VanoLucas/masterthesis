package com.vanolucas.cppsolver.old.v4.opti.algo;

import com.vanolucas.cppsolver.old.v4.opti.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v4.opti.solution.Solution;

public interface StepListener {
    default void onStepStart(long iteration) {
    }

    default void onStepEnd(long iteration) {
    }

    default void onRandomized(Solution randomizedSolution, long iteration) {
    }

    default void onEvaluated(EvaluatedSolution evaluatedSolution, long iteration) {
    }

    default void onSolutionProposal(EvaluatedSolution solution) {
    }
}
