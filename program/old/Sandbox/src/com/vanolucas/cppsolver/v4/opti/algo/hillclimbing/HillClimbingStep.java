package com.vanolucas.cppsolver.old.v4.opti.algo.hillclimbing;

import com.vanolucas.cppsolver.old.v4.opti.algo.Step;
import com.vanolucas.cppsolver.old.v4.opti.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v4.opti.solution.Solution;

import java.util.concurrent.ExecutionException;

public class HillClimbingStep extends Step {
    private HillClimbingStrategy strategy;
    private Solution currentSolution;

    public HillClimbingStep(HillClimbingStrategy strategy, Solution initialSolution) {
        this.strategy = strategy;
        this.currentSolution = initialSolution;
    }

    @Override
    protected void runStep(long iteration) throws ExecutionException, InterruptedException {
        // generate, evaluate and choose a neighbor
        final EvaluatedSolution chosenNeighbor = strategy.chooseNeighborOf(currentSolution, iteration, listeners);

        // if we have a neighbor to move to
        if (chosenNeighbor != null) {
            // make that neighbor our new current solution
            currentSolution = chosenNeighbor.getSolution();
            // propose this solution
            proposeSolution(chosenNeighbor);
        }
    }
}
