package com.vanolucas.cppsolver.old.v4.opti.algo.hillclimbing;

import com.vanolucas.cppsolver.old.v4.opti.algo.StepListener;
import com.vanolucas.cppsolver.old.v4.opti.cloner.Cloner;
import com.vanolucas.cppsolver.old.v4.opti.evaluation.Evaluator;
import com.vanolucas.cppsolver.old.v4.opti.mutation.mutator.Mutator;
import com.vanolucas.cppsolver.old.v4.opti.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v4.opti.solution.Solution;
import com.vanolucas.cppsolver.old.v4.util.observable.Listeners;

import java.util.*;
import java.util.concurrent.*;
import java.util.stream.IntStream;

public class ParallelSteepestAscentHillClimbingStrategy implements HillClimbingStrategy {
    private ConcurrentLinkedDeque<Cloner> cloners;
    private List<Mutator> mutators;
    private ConcurrentLinkedDeque<Evaluator> evaluators;
    private ForkJoinPool pool;

    public ParallelSteepestAscentHillClimbingStrategy(Collection<Cloner> cloners,
                                                      List<Mutator> mutators,
                                                      Collection<Evaluator> evaluators) {
        if (evaluators.size() < 1 || evaluators.size() != cloners.size()) {
            throw new IllegalArgumentException(String.format("There must be the same number of Cloner and Evaluator in %s. One for each thread.",
                    getClass().getSimpleName()
            ));
        }
        this.mutators = mutators;
        this.cloners = new ConcurrentLinkedDeque<>(cloners);
        this.evaluators = new ConcurrentLinkedDeque<>(evaluators);

        final int nbThreads = evaluators.size();
        this.pool = new ForkJoinPool(nbThreads);
    }

    @Override
    public EvaluatedSolution chooseNeighborOf(Solution currentSolution, long iteration, Listeners<StepListener> listeners) throws ExecutionException, InterruptedException {
        final int nbNeighbors = mutators.size();
        // submit each neighbor to process to the thread pool
        return pool.submit(() -> IntStream.range(0, nbNeighbors)
                .parallel()
                .mapToObj(neighborIndex -> {
                    // create neighbor that is a clone of the current solution
                    final Cloner cloner = cloners.pollFirst();
                    final Solution neighbor = cloner.clone(currentSolution);
                    cloners.addFirst(cloner);

                    // mutate neighbor
                    mutators.get(neighborIndex).mutate(neighbor);

                    // evaluate neighbor
                    final Evaluator evaluator = evaluators.pollFirst();
                    final EvaluatedSolution evaluatedNeighbor = evaluator.evaluate(neighbor);
                    evaluators.addFirst(evaluator);
                    listeners.notifyListeners(listener -> listener.onEvaluated(evaluatedNeighbor, iteration));

                    return evaluatedNeighbor;
                })
                // get the best neighbor
                .max(Comparator.naturalOrder())
        ).get().orElseThrow(() -> new IllegalStateException(String.format("The %s thread pool did not return a chosen best neighbor.",
                getClass().getSimpleName()
        )));
    }

}
