package com.vanolucas.cppsolver.old.v4.opti.algo.hillclimbing;

import com.vanolucas.cppsolver.old.v4.opti.algo.StepListener;
import com.vanolucas.cppsolver.old.v4.opti.cloner.Cloner;
import com.vanolucas.cppsolver.old.v4.opti.evaluation.Evaluator;
import com.vanolucas.cppsolver.old.v4.opti.mutation.mutator.Mutator;
import com.vanolucas.cppsolver.old.v4.opti.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v4.opti.solution.Solution;
import com.vanolucas.cppsolver.old.v4.util.observable.Listeners;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class SteepestAscentHillClimbingStrategy implements HillClimbingStrategy {
    private Cloner cloner;
    private List<Mutator> mutators;
    private Evaluator evaluator;

    public SteepestAscentHillClimbingStrategy(Cloner cloner, List<Mutator> mutators, Evaluator evaluator) {
        this.cloner = cloner;
        this.mutators = mutators;
        this.evaluator = evaluator;
    }

    @Override
    public EvaluatedSolution chooseNeighborOf(Solution currentSolution, long iteration, Listeners<StepListener> listeners) {
        // create neighbors that are clones of the current solution
        List<Solution> neighbors = mutators.stream()
                .map(__ -> cloner.clone(currentSolution))
                .collect(Collectors.toList());
        // mutate neighbors
        IntStream.range(0, mutators.size())
                .forEach(i -> mutators.get(i).mutate(neighbors.get(i)));
        // evaluate neighbors
        EvaluatedSolution bestNeighbor = null;
        for (Solution neighbor : neighbors) {
            final EvaluatedSolution evaluatedNeighbor = evaluator.evaluate(neighbor);
            listeners.notifyListeners(listener -> listener.onEvaluated(evaluatedNeighbor, iteration));
            if (bestNeighbor == null || evaluatedNeighbor.isBetterThan(bestNeighbor)) {
                bestNeighbor = evaluatedNeighbor;
            }
        }
        // choose the first best neighbor
        return bestNeighbor;
    }
}
