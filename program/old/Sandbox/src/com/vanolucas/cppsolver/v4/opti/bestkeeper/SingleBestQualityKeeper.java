package com.vanolucas.cppsolver.old.v4.opti.bestkeeper;

import com.vanolucas.cppsolver.old.v4.opti.quality.Quality;
import com.vanolucas.cppsolver.old.v4.opti.solution.EvaluatedSolution;

/**
 * Keeps track of the single best Quality that it receives.
 */
public class SingleBestQualityKeeper extends BestKeeper {

    private Quality bestQuality;

    @Override
    public void proposeSolution(EvaluatedSolution candidate) {
        if (bestQuality == null || candidate.isBetterThan(bestQuality)) {
            bestQuality = candidate.getQuality();
            notifyBetterSolution(candidate);
        }
    }
}
