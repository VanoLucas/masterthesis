package com.vanolucas.cppsolver.old.v4.opti.cloner;

import com.vanolucas.cppsolver.old.v4.opti.solution.Solution;

public interface Cloner<TOptiSolution extends Solution> {
    TOptiSolution clone(TOptiSolution solution);
}
