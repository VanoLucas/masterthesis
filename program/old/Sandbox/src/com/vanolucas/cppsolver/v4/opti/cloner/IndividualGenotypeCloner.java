package com.vanolucas.cppsolver.old.v4.opti.cloner;

import com.vanolucas.cppsolver.old.v4.opti.genotype.Genotype;
import com.vanolucas.cppsolver.old.v4.opti.solution.indiv.Individual;

/**
 * A cloner that only clones the Cloneable genotype of an Individual.
 */
public class IndividualGenotypeCloner implements Cloner<Individual> {
    @Override
    public Individual clone(Individual solution) {
        final Genotype genotype = solution.getGenotype();
        try {
            return new Individual(genotype.clone());
        } catch (CloneNotSupportedException e) {
            throw new UnsupportedOperationException(String.format("Clone not supported for genotype %s.",
                    genotype.getClass().getSimpleName()
            ));
        }
    }
}
