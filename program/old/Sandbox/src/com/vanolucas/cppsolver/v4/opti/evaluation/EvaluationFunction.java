package com.vanolucas.cppsolver.old.v4.opti.evaluation;

import com.vanolucas.cppsolver.old.v4.opti.quality.Quality;

public interface EvaluationFunction<TToEval, TQuality extends Quality> {
    TQuality getQuality(TToEval solution);
}
