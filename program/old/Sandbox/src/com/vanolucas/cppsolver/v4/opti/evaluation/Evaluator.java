package com.vanolucas.cppsolver.old.v4.opti.evaluation;

import com.vanolucas.cppsolver.old.v4.opti.quality.Quality;
import com.vanolucas.cppsolver.old.v4.opti.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v4.opti.solution.Solution;

public interface Evaluator<TOptiSolution extends Solution, TQuality extends Quality> extends EvaluationFunction<TOptiSolution, TQuality> {
    default EvaluatedSolution evaluate(TOptiSolution solution) {
        return new EvaluatedSolution(solution, getQuality(solution));
    }
}
