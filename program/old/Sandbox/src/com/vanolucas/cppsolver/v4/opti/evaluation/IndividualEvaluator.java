package com.vanolucas.cppsolver.old.v4.opti.evaluation;

import com.vanolucas.cppsolver.old.v4.opti.genotype.GenotypeDecoder;
import com.vanolucas.cppsolver.old.v4.opti.quality.Quality;
import com.vanolucas.cppsolver.old.v4.opti.solution.indiv.Individual;
import com.vanolucas.cppsolver.old.v4.util.converter.Converter;

public class IndividualEvaluator<TQuality extends Quality> implements Evaluator<Individual, TQuality> {
    private GenotypeDecoder genotypeDecoder;
    private Converter phenotypeDecoder;
    private EvaluationFunction evaluationFunction;

    public IndividualEvaluator(GenotypeDecoder genotypeDecoder, Converter phenotypeDecoder, EvaluationFunction evaluationFunction) {
        this.genotypeDecoder = genotypeDecoder;
        this.phenotypeDecoder = phenotypeDecoder;
        this.evaluationFunction = evaluationFunction;
    }

    @Override
    public TQuality getQuality(Individual indiv) {
        indiv.decodeGenotypeUsing(genotypeDecoder);
        indiv.decodePhenotypeUsing(phenotypeDecoder);
        return (TQuality) indiv.evaluateSolutionUsing(evaluationFunction);
    }
}
