package com.vanolucas.cppsolver.old.v4.opti.genotype;

import com.vanolucas.cppsolver.old.v4.opti.mutation.Mutation;

public interface Genotype extends Cloneable {
    Genotype clone() throws CloneNotSupportedException;

    default void mutateUsing(Mutation mutation) {
        mutation.mutate(this);
    }
}
