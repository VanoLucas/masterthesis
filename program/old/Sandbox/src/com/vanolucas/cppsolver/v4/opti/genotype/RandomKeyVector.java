package com.vanolucas.cppsolver.old.v4.opti.genotype;

import com.vanolucas.cppsolver.old.v4.util.rand.Rand;
import com.vanolucas.cppsolver.old.v4.util.rand.Randomizable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

public class RandomKeyVector implements Genotype, Randomizable {

    private List<Double> keys;

    public RandomKeyVector(int length) {
        this(length, 0d);
    }

    public RandomKeyVector(int length, Double initValue) {
        this(Collections.nCopies(length, initValue));
    }

    public RandomKeyVector(List<Double> keys) {
        setKeyValues(keys);
    }

    public int length() {
        return keys.size();
    }

    public Double getKey(int index) {
        return keys.get(index);
    }

    public void setKey(int index, Double value) {
        if (value != null) {
            if (value < 0d || value >= 1d) {
                throw new IllegalArgumentException(String.format(
                        "Tried to set an invalid key value %f at index %d of a %s. The value of a key must be in the range [0d..1d).",
                        value,
                        index,
                        getClass().getSimpleName()
                ));
            }
        }
        keys.set(index, value);
    }

    private void setKeyValues(List<Double> keys) {
        this.keys = new ArrayList<>(keys);
    }

    public DoubleStream doubleStream() {
        return keys.stream().mapToDouble(__ -> __);
    }

    @Override
    public void randomize(Rand rand) {
        setKeyValues(
                rand.doubles01(length())
        );
    }

    public Double randomizeKey(int index, Rand rand) {
        keys.set(index, rand.double01());
        return keys.get(index);
    }

    @Override
    public RandomKeyVector clone() throws CloneNotSupportedException {
        RandomKeyVector cloned = (RandomKeyVector) super.clone();
        cloned.keys = new ArrayList<>(keys);
        return cloned;
    }

    @Override
    public String toString() {
        final String strKeys = keys.stream()
                .map(Object::toString)
                .collect(Collectors.joining(";"));
        return String.format("%s%n" +
                        "(%d genes)",
                strKeys,
                keys.size()
        );
    }
}
