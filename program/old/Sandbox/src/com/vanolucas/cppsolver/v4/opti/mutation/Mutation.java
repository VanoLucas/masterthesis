package com.vanolucas.cppsolver.old.v4.opti.mutation;

/**
 * Mutation operator.
 * @param <TToMutate> Type of objects that this operator can mutate.
 */
public interface Mutation<TToMutate> {
    void mutate(TToMutate obj);
}
