package com.vanolucas.cppsolver.old.v4.opti.mutation;

import com.vanolucas.cppsolver.old.v4.util.rand.Rand;
import com.vanolucas.cppsolver.old.v4.util.rand.Randomizable;

/**
 * Mutation operator that randomizes objects that implement Randomizable.
 */
public class RandomizeRandomizableMutation implements Mutation<Randomizable> {
    private Rand rand;

    public RandomizeRandomizableMutation() {
        this(null);
    }

    public RandomizeRandomizableMutation(Rand rand) {
        this.rand = rand;
    }

    @Override
    public void mutate(Randomizable randomizable) {
        if (rand == null) {
            rand = new Rand();
        }
        randomizable.randomize(rand);
    }
}
