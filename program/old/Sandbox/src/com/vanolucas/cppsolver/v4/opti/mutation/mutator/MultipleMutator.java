package com.vanolucas.cppsolver.old.v4.opti.mutation.mutator;

import com.vanolucas.cppsolver.old.v4.opti.solution.Solution;

import java.util.List;

public class MultipleMutator<TOptiSolution extends Solution> implements Mutator<TOptiSolution> {
    private List<Mutator> mutators;

    public MultipleMutator(List<Mutator> mutators) {
        this.mutators = mutators;
    }

    @Override
    public void mutate(TOptiSolution solution) {
        mutators.forEach(mutator -> mutator.mutate(solution));
    }
}
