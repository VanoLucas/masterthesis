package com.vanolucas.cppsolver.old.v4.opti.mutation.mutator;

import com.vanolucas.cppsolver.old.v4.opti.solution.Solution;

import java.util.Collections;

public class RepeatedMutator<TOptiSolution extends Solution> extends MultipleMutator<TOptiSolution> {
    public RepeatedMutator(int count, Mutator mutator) {
        super(Collections.nCopies(count, mutator));
    }
}
