package com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector;

import com.vanolucas.cppsolver.old.v4.opti.genotype.RandomKeyVector;
import com.vanolucas.cppsolver.old.v4.opti.mutation.Mutation;
import com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector.key.KeyMutation;
import com.vanolucas.cppsolver.old.v4.util.rand.Rand;

public class OneRandomKeyMutation implements Mutation<RandomKeyVector> {
    private KeyMutation keyMutation;
    private Rand rand;

    public OneRandomKeyMutation(KeyMutation keyMutation) {
        this(keyMutation, null);
    }

    public OneRandomKeyMutation(KeyMutation keyMutation, Rand rand) {
        this.keyMutation = keyMutation;
        this.rand = rand;
    }

    @Override
    public void mutate(RandomKeyVector randomKeyVector) {
        if (rand == null) {
            rand = new Rand();
        }
        keyMutation.mutateKey(
                rand.int0To(randomKeyVector.length()),
                randomKeyVector
        );
    }
}
