package com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector.key;

import com.vanolucas.cppsolver.old.v4.opti.genotype.RandomKeyVector;
import com.vanolucas.cppsolver.old.v4.util.math.rangeguard.DoubleRangeGuard;
import com.vanolucas.cppsolver.old.v4.util.rand.Rand;

public class AddRandomValueKeyMutation extends AddValueKeyMutation {
    private double min;
    private double max;
    private Rand rand;

    public AddRandomValueKeyMutation(Double min, Double max) {
        this(min, max, null);
    }

    public AddRandomValueKeyMutation(Double min, Double max, Rand rand) {
        this(null, min, max, rand);
    }

    public AddRandomValueKeyMutation(Integer keyIndex, Double min, Double max) {
        this(keyIndex, min, max, null);
    }

    public AddRandomValueKeyMutation(Integer keyIndex, Double min, Double max, Rand rand) {
        this(keyIndex, min, max, rand, null);
    }

    public AddRandomValueKeyMutation(Integer keyIndex, Double min, Double max, Rand rand, DoubleRangeGuard rangeGuard) {
        super(keyIndex, null, rangeGuard);
        this.min = min != null ? min : 0d;
        this.max = max != null ? max : 1d;
        this.rand = rand;
    }

    @Override
    public void mutateKey(int keyIndex, RandomKeyVector randomKeyVector) {
        if (rand == null) {
            rand = new Rand();
        }

        if (min == 0d && max == 1d) {
            setValueToAdd(rand.double01());
        } else {
            setValueToAdd(rand.doubleBetween(min, max));
        }

        super.mutateKey(keyIndex, randomKeyVector);
    }
}
