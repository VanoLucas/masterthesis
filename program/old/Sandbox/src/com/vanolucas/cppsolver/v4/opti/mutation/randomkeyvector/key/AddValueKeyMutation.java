package com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector.key;

import com.vanolucas.cppsolver.old.v4.opti.genotype.RandomKeyVector;
import com.vanolucas.cppsolver.old.v4.util.math.rangeguard.BlockOnBoundsDoubleRangeGuard;
import com.vanolucas.cppsolver.old.v4.util.math.rangeguard.DoubleRangeGuard;

public class AddValueKeyMutation extends KeyMutation {
    private Double valueToAdd;
    private DoubleRangeGuard rangeGuard;

    public AddValueKeyMutation() {
        this((Double) null);
    }

    public AddValueKeyMutation(Double valueToAdd) {
        this(valueToAdd, new BlockOnBoundsDoubleRangeGuard());
    }

    public AddValueKeyMutation(DoubleRangeGuard rangeGuard) {
        this(null, rangeGuard);
    }

    public AddValueKeyMutation(Double valueToAdd, DoubleRangeGuard rangeGuard) {
        this(null, valueToAdd, rangeGuard);
    }

    public AddValueKeyMutation(Integer keyIndex, Double valueToAdd, DoubleRangeGuard rangeGuard) {
        super(keyIndex);
        this.valueToAdd = valueToAdd;
        this.rangeGuard = rangeGuard != null ? rangeGuard : new BlockOnBoundsDoubleRangeGuard();
    }

    public void setValueToAdd(Double valueToAdd) {
        this.valueToAdd = valueToAdd;
    }

    @Override
    public void mutateKey(int keyIndex, RandomKeyVector randomKeyVector) {
        addValueToKey(valueToAdd, keyIndex, randomKeyVector);
    }

    public void addValueToKey(double value, int keyIndex, RandomKeyVector randomKeyVector) {
        final Double oldValue = randomKeyVector.getKey(keyIndex);
        if (oldValue == null) {
            randomKeyVector.setKey(keyIndex, value);
        } else {
            randomKeyVector.setKey(keyIndex, rangeGuard.bringBackToRange(
                    oldValue + value, 0d, 1d
            ));
        }
    }
}
