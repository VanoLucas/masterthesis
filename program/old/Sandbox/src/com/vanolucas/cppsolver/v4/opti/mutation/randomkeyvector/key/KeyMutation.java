package com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector.key;

import com.vanolucas.cppsolver.old.v4.opti.genotype.RandomKeyVector;
import com.vanolucas.cppsolver.old.v4.opti.mutation.Mutation;

public abstract class KeyMutation implements Mutation<RandomKeyVector> {
    private Integer keyIndex;

    public KeyMutation() {
        this(null);
    }

    public KeyMutation(Integer keyIndex) {
        this.keyIndex = keyIndex;
    }

    @Override
    public void mutate(RandomKeyVector randomKeyVector) {
        mutateKey(keyIndex, randomKeyVector);
    }

    public abstract void mutateKey(int keyIndex, RandomKeyVector randomKeyVector);
}
