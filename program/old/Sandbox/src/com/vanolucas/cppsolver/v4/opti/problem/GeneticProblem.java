package com.vanolucas.cppsolver.old.v4.opti.problem;

import com.vanolucas.cppsolver.old.v4.opti.cloner.Cloner;
import com.vanolucas.cppsolver.old.v4.opti.cloner.IndividualGenotypeCloner;
import com.vanolucas.cppsolver.old.v4.opti.evaluation.EvaluationFunction;
import com.vanolucas.cppsolver.old.v4.opti.evaluation.IndividualEvaluator;
import com.vanolucas.cppsolver.old.v4.opti.genotype.Genotype;
import com.vanolucas.cppsolver.old.v4.opti.genotype.GenotypeDecoder;
import com.vanolucas.cppsolver.old.v4.opti.quality.Quality;
import com.vanolucas.cppsolver.old.v4.opti.randomizer.Randomizer;
import com.vanolucas.cppsolver.old.v4.opti.solution.indiv.Individual;
import com.vanolucas.cppsolver.old.v4.util.converter.Converter;
import com.vanolucas.cppsolver.old.v4.util.rand.Rand;
import com.vanolucas.cppsolver.old.v4.util.rand.Randomizable;

public abstract class GeneticProblem<TGenotype extends Genotype, TPhenotype, TSolution, TQuality extends Quality> extends Problem<Individual, TQuality> {
    @Override
    public Randomizer<Individual> newRandomizer() {
        return new Randomizer<Individual>() {
            @Override
            public void randomize(Individual indiv, Rand rand) {
                Randomizable genotype = (Randomizable) indiv.getGenotype();
                genotype.randomize(rand);
            }
        };
    }

    @Override
    public Cloner<Individual> newCloner() {
        return new IndividualGenotypeCloner();
    }

    public abstract TGenotype newGenotype();

    @Override
    public Individual newSolution() {
        return newIndividual();
    }

    public Individual newIndividual() {
        return new Individual(newGenotype());
    }

    public abstract GenotypeDecoder<TGenotype, TPhenotype> newGenotypeDecoder();

    public abstract Converter<TPhenotype, TSolution> newPhenotypeDecoder();

    public abstract EvaluationFunction<TSolution, TQuality> newEvaluationFunction();

    @Override
    public IndividualEvaluator<TQuality> newEvaluator() {
        return new IndividualEvaluator<TQuality>(newGenotypeDecoder(), newPhenotypeDecoder(), newEvaluationFunction());
    }
}
