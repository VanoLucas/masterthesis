package com.vanolucas.cppsolver.old.v4.opti.problem;

import com.vanolucas.cppsolver.old.v4.opti.cloner.Cloner;
import com.vanolucas.cppsolver.old.v4.opti.cloner.IndividualGenotypeCloner;
import com.vanolucas.cppsolver.old.v4.opti.evaluation.EvaluationFunction;
import com.vanolucas.cppsolver.old.v4.opti.genotype.GenotypeDecoder;
import com.vanolucas.cppsolver.old.v4.opti.genotype.RandomKeyVector;
import com.vanolucas.cppsolver.old.v4.opti.quality.IntCost;
import com.vanolucas.cppsolver.old.v4.opti.solution.indiv.Individual;
import com.vanolucas.cppsolver.old.v4.util.converter.Converter;

import java.util.List;
import java.util.stream.Collectors;

public class SumProblem extends GeneticProblem<RandomKeyVector, List<Integer>, Integer, IntCost> {
    final static int GENOME_LENGTH = 2;

    final int target;

    public SumProblem(int target) {
        this.target = target;
    }

    @Override
    public RandomKeyVector newGenotype() {
        return new RandomKeyVector(GENOME_LENGTH);
    }

    @Override
    public GenotypeDecoder<RandomKeyVector, List<Integer>> newGenotypeDecoder() {
        return genome -> genome.doubleStream()
                .mapToInt(key -> (int) (key * 1000d))
                .boxed()
                .collect(Collectors.toList());
    }

    @Override
    public Converter<List<Integer>, Integer> newPhenotypeDecoder() {
        return phenotype -> phenotype.stream().mapToInt(__ -> __).sum();
    }

    @Override
    public EvaluationFunction<Integer, IntCost> newEvaluationFunction() {
        return solution -> {
            final int distance = Math.abs(solution - target);
            return new IntCost(distance);
        };
    }

    @Override
    public Cloner<Individual> newCloner() {
        return new IndividualGenotypeCloner();
    }
}
