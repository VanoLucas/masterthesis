package com.vanolucas.cppsolver.old.v4.opti.quality;

/**
 * Represents a Quality.
 * It could be the Quality of a Solution.
 */
public abstract class Quality implements Comparable<Quality>, Cloneable {

    public abstract boolean isBetterThan(Quality o);

    public abstract boolean isEquivalentTo(Quality o);

    public boolean isBetterOrEquivalentTo(Quality o) {
        return isBetterThan(o) || isEquivalentTo(o);
    }

    @Override
    public int compareTo(Quality o) {
        if (isBetterThan(o)) {
            return 1;
        } else if (isEquivalentTo(o)) {
            return 0;
        } else {
            return -1;
        }
    }

    @Override
    public Quality clone() throws CloneNotSupportedException {
        return (Quality) super.clone();
    }
}
