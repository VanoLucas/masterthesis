package com.vanolucas.cppsolver.old.v4.opti.solution;

import com.vanolucas.cppsolver.old.v4.opti.quality.Quality;

public class EvaluatedSolution implements Comparable<EvaluatedSolution> {
    private Solution solution;
    private Quality quality;

    public EvaluatedSolution(Solution solution, Quality quality) {
        this.solution = solution;
        this.quality = quality;
    }

    public Solution getSolution() {
        return solution;
    }

    public Quality getQuality() {
        return quality;
    }

    public boolean isBetterThan(EvaluatedSolution o) {
        return quality.isBetterThan(o.quality);
    }

    public boolean isBetterThan(Quality otherQuality) {
        return quality.isBetterThan(otherQuality);
    }

    /**
     * Compare based on quality.
     */
    @Override
    public int compareTo(EvaluatedSolution o) {
        return quality.compareTo(o.quality);
    }
}
