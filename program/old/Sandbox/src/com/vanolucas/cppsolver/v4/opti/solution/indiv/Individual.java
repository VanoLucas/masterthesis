package com.vanolucas.cppsolver.old.v4.opti.solution.indiv;

import com.vanolucas.cppsolver.old.v4.opti.evaluation.EvaluationFunction;
import com.vanolucas.cppsolver.old.v4.opti.genotype.Genotype;
import com.vanolucas.cppsolver.old.v4.opti.mutation.Mutation;
import com.vanolucas.cppsolver.old.v4.opti.quality.Quality;
import com.vanolucas.cppsolver.old.v4.opti.solution.Solution;
import com.vanolucas.cppsolver.old.v4.util.converter.Converter;

/**
 * A solution that has a genetic representation.
 * The genetic representation (genotype) defines a corresponding decoded representation (phenotype).
 * The phenotype can be converted to the actual problem-specific solution object.
 */
public class Individual implements Solution {

    /**
     * Genetic representation of this solution.
     */
    private Genotype genotype;
    /**
     * Representation of this solution decoded from the genotype.
     */
    private Object phenotype;
    /**
     * Actual described solution.
     */
    private Object solution;

    public Individual(Genotype genotype) {
        this(genotype, null, null);
    }

    public Individual(Genotype genotype, Object phenotype, Object solution) {
        this.genotype = genotype;
        this.phenotype = phenotype;
        this.solution = solution;
    }

    public Genotype getGenotype() {
        return genotype;
    }

    public void decodeGenotypeUsing(Converter decoder) {
        phenotype = decoder.convert(genotype);
    }

    public void decodePhenotypeUsing(Converter decoder) {
        solution = decoder.convert(phenotype);
    }

    public Quality evaluateSolutionUsing(EvaluationFunction evaluationFunction) {
        return evaluationFunction.getQuality(solution);
    }

    public void mutateGenotypeUsing(Mutation mutation) {
        genotype.mutateUsing(mutation);
    }

    @Override
    public String toString() {
        return String.format("Genotype (%s):\n" +
                        "%s\n" +
                        "Phenotype (%s):\n" +
                        "%s\n" +
                        "Solution (%s):\n" +
                        "%s",
                genotype.getClass().getSimpleName(),
                genotype.toString(),
                phenotype.getClass().getSimpleName(),
                phenotype.toString(),
                solution.getClass().getSimpleName(),
                solution.toString()
        );
    }
}
