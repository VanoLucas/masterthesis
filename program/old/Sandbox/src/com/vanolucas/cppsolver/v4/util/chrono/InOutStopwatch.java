package com.vanolucas.cppsolver.old.v4.util.chrono;

import java.time.Duration;
import java.time.Instant;

public class InOutStopwatch {
    private Duration durationIn;
    private Duration durationOut;
    private Instant timeStartIn;
    private Instant timeStartOut;

    private Long printEveryCalls;
    private long calls = 0L;

    public InOutStopwatch() {
        this(null);
    }

    public InOutStopwatch(Long printEveryCalls) {
        this.printEveryCalls = printEveryCalls;
    }

    public void in() {
        if (timeStartOut != null) {
            if (durationOut == null) {
                durationOut = Duration.ZERO;
            }
            durationOut = durationOut.plus(Duration.between(timeStartOut, Instant.now()));
            timeStartOut = null;
        }
        if (timeStartIn == null) {
            calls++;
            if (printEveryCalls != null) {
                printEveryCalls(printEveryCalls);
            }
            timeStartIn = Instant.now();
        }
    }

    public void out() {
        if (timeStartIn != null) {
            if (durationIn == null) {
                durationIn = Duration.ZERO;
            }
            durationIn = durationIn.plus(Duration.between(timeStartIn, Instant.now()));
            timeStartIn = null;
        }
        if (timeStartOut == null) {
            calls++;
            if (printEveryCalls != null) {
                printEveryCalls(printEveryCalls);
            }
            timeStartOut = Instant.now();
        }
    }

    public void printEveryCalls(long nbCalls) {
        if (calls % nbCalls == 0L) {
            System.out.println(this);
        }
    }

    @Override
    public String toString() {
        final double percentIn;
        if (durationIn != null && durationOut != null) {
            percentIn = (double) durationIn.toMillis() / (durationIn.plus(durationOut).toMillis()) * 100d;
        } else {
            percentIn = 0d;
        }
        return String.format("in: %s out: %s (%.2f%% of the duration in)",
                durationIn, durationOut, percentIn
        );
    }
}
