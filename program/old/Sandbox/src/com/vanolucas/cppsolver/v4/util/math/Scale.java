package com.vanolucas.cppsolver.old.v4.util.math;

public class Scale {
    /**
     * Scale linearly a value from source range to destination range.
     *
     * @param value Value in src range to convert to dst range.
     * @return The value translated to the target range.
     */
    public static double scale(final double value, final double srcMin, final double srcMax, final double dstMin, final double dstMax) {
        return ((dstMax - dstMin) * (value - srcMin) / (srcMax - srcMin)) + dstMin;
    }
}
