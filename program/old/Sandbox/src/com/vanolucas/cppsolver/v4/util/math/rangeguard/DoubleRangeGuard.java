package com.vanolucas.cppsolver.old.v4.util.math.rangeguard;

public interface DoubleRangeGuard {
    double bringBackToRange(double value, double minInclusive, double maxExclusive);
}
