package com.vanolucas.cppsolver.old.v4.util.rand;

public interface Randomizable {
    void randomize(Rand rand);
}
