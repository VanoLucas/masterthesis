package com.vanolucas.cppsolver.old.v5;

public class Algo implements Observable<AlgoListener> {
    /**
     * Current iteration number.
     */
    private long iteration = 0L;
    /**
     * Specific optimization algorithm that we run.
     */
    private OptiStep optiStep;
    /**
     * Ability to evaluate solutions.
     */
    private Evaluator evaluator;
    /**
     * Keeps track of best qualities.
     */
    private EliteDecider eliteDecider;
    /**
     * Listeners that can react to events of this optimization algorithm.
     */
    private Listeners<AlgoListener> listeners = new Listeners<>(4);

    /*
    Setters
     */

    private void setOptiStep(OptiStep optiStep) {
        if (this.optiStep != null) {
            this.optiStep.removeListener(optiStepHandler);
        }
        this.optiStep = optiStep;
        this.optiStep.addListener(optiStepHandler);
    }

    private void setEliteDecider(EliteDecider eliteDecider) {
        if (this.eliteDecider != null) {
            this.eliteDecider.removeListener(eliteDeciderHandler);
        }
        this.eliteDecider = eliteDecider;
        this.eliteDecider.addListener(eliteDeciderHandler);
    }

    /*
    Handle opti step
     */

    private OptiStepListener optiStepHandler = new OptiStepListener() {
        /**
         * Submit the solution proposed by the opti algo step to the best keeper.
         * @param candidate Solution proposed by the optimization algorithm step.
         */
        @Override
        public void onProposedSolution(EvaluatedSolution candidate) {
            eliteDecider.submit(candidate);
        }
    };

    /*
    Handle elite decider
     */

    private EliteDeciderListener eliteDeciderHandler = new EliteDeciderListener() {
        @Override
        public void onBetterSolution(EvaluatedSolution solution) {
            listeners.notifyListeners(l -> l.onBetterSolutionFound(solution, iteration));
            listeners.notifyListeners(l -> l.onEquivalentOrBetterSolutionFound(solution, iteration));
        }

        @Override
        public void onEquivalentSolution(EvaluatedSolution solution) {
            listeners.notifyListeners(l -> l.onEquivalentSolutionFound(solution, iteration));
            listeners.notifyListeners(l -> l.onEquivalentOrBetterSolutionFound(solution, iteration));
        }
    };

    /*
    Manage listeners
     */

    @Override
    public void addListener(AlgoListener listener) {
        listeners.addListener(listener);
        listeners.notifyListeners(l ->
                l.onListenerAdded(listener, this)
        );
    }

    @Override
    public void removeListener(AlgoListener listener) {
        listeners.removeListener(listener);
    }

    @Override
    public void removeAllListeners() {
        listeners.removeAllListeners();
    }
}
