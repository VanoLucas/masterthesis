package com.vanolucas.cppsolver.old.v5;

public abstract class EliteDecider implements Observable<EliteDeciderListener> {
    /**
     * Listeners that can react to events of this elite decider.
     */
    private Listeners<EliteDeciderListener> listeners = new Listeners<>(2);

    /*
    Commands
     */

    public abstract void submit(EvaluatedSolution candidate);

    /*
    Manage listeners
     */

    @Override
    public void addListener(EliteDeciderListener listener) {
        listeners.addListener(listener);
        listeners.notifyListeners(l ->
                l.onListenerAdded(listener, this)
        );
    }

    @Override
    public void removeListener(EliteDeciderListener listener) {
        listeners.removeListener(listener);
    }

    @Override
    public void removeAllListeners() {
        listeners.removeAllListeners();
    }

    /*
    Notify listeners
     */

    protected void onBetterSolution(EvaluatedSolution solution) {
        listeners.notifyListeners(l -> l.onBetterSolution(solution));
    }

    protected void onEquivalentSolution(EvaluatedSolution solution) {
        listeners.notifyListeners(l -> l.onEquivalentSolution(solution));
    }
}
