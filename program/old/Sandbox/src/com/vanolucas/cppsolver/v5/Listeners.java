package com.vanolucas.cppsolver.old.v5;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Manages some listeners.
 * The listeners can be notified and react about events.
 *
 * @param <TListener> Type of listeners we can store and call.
 */
public class Listeners<TListener> {

    /**
     * Subscribed listeners.
     */
    private List<TListener> listeners;

    public Listeners() {
        this(2);
    }

    public Listeners(int expectedNbListeners) {
        this.listeners = new ArrayList<>(expectedNbListeners);
    }

    public void addListener(TListener listener) {
        listeners.add(listener);
    }

    public void removeListener(TListener listener) {
        listeners.remove(listener);
    }

    public void removeAllListeners() {
        listeners.clear();
    }

    public void notifyListeners(Consumer<TListener> action) {
        listeners.forEach(action);
    }

    public <T extends TListener> void notifyListeners(Class<T> listenerClass, Consumer<T> action) {
        listeners.forEach(listener -> {
            if (listenerClass.isInstance(listener)) {
                action.accept((T) listener);
            }
        });
    }
}
