package com.vanolucas.cppsolver.old.v5;

public interface Observable<TListener> {
    void addListener(TListener listener);

    void removeListener(TListener listener);

    void removeAllListeners();
}
