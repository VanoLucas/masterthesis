package com.vanolucas.cppsolver.old.v5;

/**
 * An optimization algorithm step.
 */
public class OptiStep implements Observable<OptiStepListener> {
    /**
     * Listeners that can react to events of this optimization algorithm step.
     */
    private Listeners<OptiStepListener> listeners = new Listeners<>(4);

    /*
    Manage listeners
     */

    @Override
    public void addListener(OptiStepListener listener) {
        listeners.addListener(listener);
        listeners.notifyListeners(l ->
                l.onListenerAdded(listener, this)
        );
    }

    @Override
    public void removeListener(OptiStepListener listener) {
        listeners.removeListener(listener);
    }

    @Override
    public void removeAllListeners() {
        listeners.removeAllListeners();
    }
}
