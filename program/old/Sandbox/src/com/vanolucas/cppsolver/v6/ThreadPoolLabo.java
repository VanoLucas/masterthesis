package com.vanolucas.cppsolver.old.v6;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class ThreadPoolLabo {
    public static void main(String[] args) throws InterruptedException {
        new ThreadPoolLabo().run();
    }

    public void run() throws InterruptedException {

        class Task implements Runnable {
            private final int nb;

            public Task(int nb) {
                this.nb = nb;
            }

            @Override
            public void run() {
                System.out.println(String.format("Start task %d", nb));
                try {
                    TimeUnit.SECONDS.sleep(2L);
                } catch (InterruptedException e) {
                    System.out.println(String.format("Task %d interrupted", nb));
                } finally {
                    System.out.println(String.format("End task %d", nb));
                }
            }
        }

        ExecutorService executor = Executors.newFixedThreadPool(2);

        final int taskCount = 5;
        IntStream.range(0, taskCount)
                .forEach(i -> {
                    executor.submit(new Task(i));
                    try {
                        TimeUnit.MILLISECONDS.sleep(100L);
                    } catch (InterruptedException e) {
                        System.out.println("main thread interrupted");
                    }
                });

        executor.shutdown();
//        executor.shutdownNow();
    }
}
