package com.vanolucas.cppsolver.old.v8.algo;

import com.vanolucas.cppsolver.old.v8.eval.Evaluator;
import com.vanolucas.cppsolver.old.v8.judge.Judge;
import com.vanolucas.cppsolver.old.v8.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v8.solution.Solution;
import com.vanolucas.cppsolver.old.v8.util.AutoGrowPool;
import com.vanolucas.cppsolver.old.v8.util.observer.Listeners;
import com.vanolucas.cppsolver.old.v8.util.observer.Observable;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Iterative optimization algorithm.
 */
public class Algo implements Observable<AlgoListener> {
    private static final long STOP_IDLE_THREAD_AFTER_SECONDS = 5L;

    private long iteration = 0L;
    private Strategy strategy;
    private int maxThreads = 0;
    private ForkJoinPool executor;
    private AutoGrowPool<Evaluator> evaluators;
    private Judge judge;
    private final AtomicBoolean pauseBeforeNextIteration = new AtomicBoolean(false);
    /**
     * Listeners that can react to events of this optimization algorithm.
     */
    private Listeners<AlgoListener> listeners = new Listeners<>(4);

    public Algo(Strategy strategy, Supplier<Evaluator> evaluatorsSupplier, Judge judge, Integer maxThreads) {
        this.strategy = strategy;
        setMaxThreads(maxThreads);
        this.executor = new ForkJoinPool(this.maxThreads);
        this.evaluators = new AutoGrowPool<>(evaluatorsSupplier);
        this.judge = judge;
    }

    public void run() throws ExecutionException, InterruptedException {
        // iterative optimization algo loop
        for (; ; ) {
            // check if need to pause
            synchronized (pauseBeforeNextIteration) {
                if (pauseBeforeNextIteration.get()) {
                    onPause();
                    break;
                }
            }

            // notify listeners that we start a new iteration
            listeners.notifyListeners(l ->
                    l.onNewIteration(++iteration)
            );
            // notify algo strategy that we start a new iteration
            strategy.onNewIteration(iteration);

            // process solutions: evaluate and propose best solutions of this strategy
            {
                List<Solution> solutionsToEval;
                List<EvaluatedSolution> candidateSolutions;
                boolean smtgToDo = true;
                // accept solutions from the strategy for as long as it provides some
                while (smtgToDo) {
                    smtgToDo = false;

                    // evaluate solutions
                    solutionsToEval = strategy.nextSolutionsToEvaluate();
                    if (solutionsToEval != null && !solutionsToEval.isEmpty()) {
                        evaluate(solutionsToEval);
                        smtgToDo = true;
                    }

                    // propose to compare and keep new best solutions
                    candidateSolutions = strategy.nextProposedSolutions();
                    if (candidateSolutions != null && !candidateSolutions.isEmpty()) {
                        propose(candidateSolutions);
                        smtgToDo = true;
                    }
                }
            }
        }
    }

    public void pauseBeforeNextIteration() {
        synchronized (pauseBeforeNextIteration) {
            pauseBeforeNextIteration.set(true);
        }
    }

    private void setMaxThreads(Integer maxThreads) {
        // default to the number of available logical CPU cores
        final int newMaxThreads = maxThreads != null ? maxThreads : Runtime.getRuntime().availableProcessors();

        // check value validity
        if (newMaxThreads >= 1) {
            this.maxThreads = newMaxThreads;
        } else {
            throw new IllegalArgumentException(String.format("The max number of threads in %s must be at least 1.", getClass().getSimpleName()));
        }
    }

    /*
    Helpers
     */

    private void onPause() {
        pauseBeforeNextIteration.set(false);
    }

    private void evaluate(List<Solution> solutions) throws ExecutionException, InterruptedException {
        // spread evaluation of solutions over the thread pool
        final List<EvaluatedSolution> evaluatedSolutions = executor.submit(() -> solutions.parallelStream()
                .map(solution -> {
                    // fetch an available evaluator from the pool and use it to evaluate this solution
                    final Evaluator evaluator = evaluators.fetch();
                    final EvaluatedSolution evaluatedSolution = evaluator.evaluate(solution);
                    evaluators.returnToPool(evaluator);
                    // notify listeners that we evaluated this solution
                    listeners.notifyListeners(l -> l.onEvaluated(evaluatedSolution, iteration));
                    // return the evaluated solution
                    return evaluatedSolution;
                })
                // collect all evaluated solutions
                .collect(Collectors.toList())
        ).get(); // wait for all solutions to be evaluated and collected

        // notify strategy that we evaluated the provided solutions
        strategy.onEvaluated(evaluatedSolutions);
    }

    private void propose(List<EvaluatedSolution> solutions) {
        // submit solutions to the judge so that it decides how good they are
        final List<Judge.Verdict> verdicts = solutions.stream()
                .map(this::propose)
                .collect(Collectors.toList());
        // notify strategy that we got verdicts for the proposed solutions
        strategy.onVerdicts(verdicts);
    }

    private Judge.Verdict propose(EvaluatedSolution solution) {
        // submit solution to judge and get its verdict
        final Judge.Verdict verdict = judge.submit(solution);
        // notify listeners about the quality verdict of this solution
        switch (verdict) {
            case BETTER:
                listeners.notifyListeners(l -> {
                    l.onBetterSolutionFound(solution, iteration);
                    l.onEquivalentOrBetterSolutionFound(solution, iteration);
                });
                break;
            case EQUIVALENT:
                listeners.notifyListeners(l -> {
                    l.onEquivalentSolutionFound(solution, iteration);
                    l.onEquivalentOrBetterSolutionFound(solution, iteration);
                });
                break;
        }
        return verdict;
    }

    /*
    Manage listeners
     */

    @Override
    public void addListener(AlgoListener listener) {
        listeners.addListener(listener);
        listeners.notifyListeners(l ->
                l.onListenerAdded(listener, this)
        );
    }

    @Override
    public void removeListener(AlgoListener listener) {
        listeners.removeListener(listener);
    }

    @Override
    public void removeAllListeners() {
        listeners.removeAllListeners();
    }
}
