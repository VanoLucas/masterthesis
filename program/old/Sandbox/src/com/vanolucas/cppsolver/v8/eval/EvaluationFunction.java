package com.vanolucas.cppsolver.old.v8.eval;

import com.vanolucas.cppsolver.old.v8.quality.Quality;

import java.util.function.Function;

public interface EvaluationFunction<T> extends Function<T, Quality> {
    @Override
    default Quality apply(T toEval) {
        return evaluate(toEval);
    }

    Quality evaluate(T toEval);
}
