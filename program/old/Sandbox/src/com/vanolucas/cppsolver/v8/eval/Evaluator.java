package com.vanolucas.cppsolver.old.v8.eval;

import com.vanolucas.cppsolver.old.v8.quality.Quality;
import com.vanolucas.cppsolver.old.v8.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v8.solution.Solution;

public class Evaluator<TPhenotype> {
    private EvaluationFunction<TPhenotype> phenotypeEvaluationFunction;

    public Evaluator(EvaluationFunction<TPhenotype> phenotypeEvaluationFunction) {
        this.phenotypeEvaluationFunction = phenotypeEvaluationFunction;
    }

    public EvaluatedSolution evaluate(Solution<TPhenotype> solution) {
        return new EvaluatedSolution(solution, getQuality(solution));
    }

    public Quality getQuality(Solution<TPhenotype> solution) {
        return phenotypeEvaluationFunction.evaluate(solution.getPhenotype());
    }
}
