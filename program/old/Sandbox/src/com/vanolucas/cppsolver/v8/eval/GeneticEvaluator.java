package com.vanolucas.cppsolver.old.v8.eval;

import com.vanolucas.cppsolver.old.v8.quality.Quality;
import com.vanolucas.cppsolver.old.v8.solution.GeneticSolution;
import com.vanolucas.cppsolver.old.v8.solution.Solution;

public class GeneticEvaluator<TGenotype, TPhenotype> extends Evaluator<TPhenotype> {
    private GenomeDecoder<TGenotype, TPhenotype> genomeDecoder;

    public GeneticEvaluator(GenomeDecoder<TGenotype, TPhenotype> genomeDecoder, EvaluationFunction<TPhenotype> phenotypeEvaluationFunction) {
        super(phenotypeEvaluationFunction);
        this.genomeDecoder = genomeDecoder;
    }

    @Override
    public Quality getQuality(Solution<TPhenotype> solution) {
        return getQuality((GeneticSolution<TGenotype, TPhenotype>) solution);
    }

    private Quality getQuality(GeneticSolution<TGenotype, TPhenotype> solution) {
        // first, decode the genotype
        solution.decodeGenotype2PhenotypeUsing(genomeDecoder);
        // then, evaluate the phenotype
        return super.getQuality(solution);
    }
}
