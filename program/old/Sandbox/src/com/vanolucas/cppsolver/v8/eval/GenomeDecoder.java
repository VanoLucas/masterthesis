package com.vanolucas.cppsolver.old.v8.eval;

import com.vanolucas.cppsolver.old.v8.util.Converter;

public interface GenomeDecoder<TGenotype, TDecoded> extends Converter<TGenotype, TDecoded> {
    @Override
    default TDecoded convert(TGenotype genome) {
        return decode(genome);
    }

    TDecoded decode(TGenotype genome);
}
