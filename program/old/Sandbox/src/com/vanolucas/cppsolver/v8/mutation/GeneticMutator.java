package com.vanolucas.cppsolver.old.v8.mutation;

import com.vanolucas.cppsolver.old.v8.solution.GeneticSolution;
import com.vanolucas.cppsolver.old.v8.solution.Solution;

public class GeneticMutator<TGenotype, TPhenotype> extends Mutator<TPhenotype> {
    private Mutation<TGenotype> genotypeMutationOperator;

    public GeneticMutator() {
        this(null);
    }

    public GeneticMutator(Mutation<TGenotype> genotypeMutationOperator) {
        super();
        this.genotypeMutationOperator = genotypeMutationOperator;
    }

    @Override
    public void mutate(Solution<TPhenotype> solution) {
        mutate((GeneticSolution<TGenotype, TPhenotype>) solution);
    }

    public void mutate(GeneticSolution<TGenotype, TPhenotype> solution) {
        if (genotypeMutationOperator != null) {
            solution.mutateGenotypeUsing(genotypeMutationOperator);
        } else {
            super.mutate(solution);
        }
    }
}
