package com.vanolucas.cppsolver.old.v8.mutation;

import java.util.function.Consumer;

public interface Mutation<T> extends Consumer<T> {
    @Override
    default void accept(T toMutate) {
        mutate(toMutate);
    }

    void mutate(T toMutate);
}
