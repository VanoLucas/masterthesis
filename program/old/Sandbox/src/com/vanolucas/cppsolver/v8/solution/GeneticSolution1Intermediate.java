package com.vanolucas.cppsolver.old.v8.solution;

import com.vanolucas.cppsolver.old.v8.mutation.Mutation;

import java.util.function.Function;

/**
 * Genetic solution with an intermediate representation between the genotype and phenotype.
 */
public class GeneticSolution1Intermediate<TGenotype, TIntermediate, TPhenotype> extends GeneticSolution<TGenotype, TPhenotype> {
    private TIntermediate intermediate;

    public GeneticSolution1Intermediate(TGenotype genotype) {
        super(genotype);
    }

    public TIntermediate getIntermediate() {
        return intermediate;
    }

    public TIntermediate decodeGenotype2IntermediateUsing(Function<TGenotype, TIntermediate> genomeDecoder) {
        intermediate = genomeDecoder.apply(genotype);
        return intermediate;
    }

    public TPhenotype convertIntermediate2PhenotypeUsing(Function<TIntermediate, TPhenotype> converter) {
        phenotype = converter.apply(intermediate);
        return phenotype;
    }

    public void mutateIntermediateUsing(Mutation<TIntermediate> mutationOperator) {
        mutationOperator.mutate(intermediate);
    }

    @Override
    public String toString() {
        return String.format("Genotype:%s%nIntermediate:%s%nPhenotype:%s",
                genotype, intermediate, phenotype
        );
    }
}
