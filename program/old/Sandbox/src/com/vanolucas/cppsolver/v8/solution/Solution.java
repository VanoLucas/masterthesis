package com.vanolucas.cppsolver.old.v8.solution;

import com.vanolucas.cppsolver.old.v8.mutation.Mutation;
import com.vanolucas.cppsolver.old.v8.eval.Evaluator;

public class Solution<TPhenotype> {
    protected TPhenotype phenotype;

    public Solution(TPhenotype phenotype) {
        this.phenotype = phenotype;
    }

    public TPhenotype getPhenotype() {
        return phenotype;
    }

    public EvaluatedSolution evaluateUsing(Evaluator<TPhenotype> evaluator) {
        return evaluator.evaluate(this);
    }

    public void mutatePhenotypeUsing(Mutation<TPhenotype> mutationOperator) {
        mutationOperator.mutate(phenotype);
    }

    @Override
    public String toString() {
        return phenotype.toString();
    }
}
