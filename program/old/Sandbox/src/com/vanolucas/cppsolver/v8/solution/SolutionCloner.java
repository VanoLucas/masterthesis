package com.vanolucas.cppsolver.old.v8.solution;

import com.vanolucas.cppsolver.old.v8.util.Cloner;

public interface SolutionCloner<TPhenotype> extends Cloner<Solution<TPhenotype>> {
    @Override
    default Solution<TPhenotype> clone(Solution<TPhenotype> solution) {
        return new Solution<>(clonePhenotype(solution.phenotype));
    }

    TPhenotype clonePhenotype(TPhenotype phenotype);
}
