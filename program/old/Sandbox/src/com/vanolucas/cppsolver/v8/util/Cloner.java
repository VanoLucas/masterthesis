package com.vanolucas.cppsolver.old.v8.util;

import java.util.function.Function;

public interface Cloner<T> extends Function<T, T> {
    @Override
    default T apply(T toClone) {
        return clone(toClone);
    }

    T clone(T toClone);
}
