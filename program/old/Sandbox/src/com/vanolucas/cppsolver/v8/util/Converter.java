package com.vanolucas.cppsolver.old.v8.util;

import java.util.function.Function;

public interface Converter<TSrc, TDst> extends Function<TSrc, TDst> {
    @Override
    default TDst apply(TSrc src) {
        return convert(src);
    }

    TDst convert(TSrc src);
}
