package com.vanolucas.cppsolver.old.v9.algo;

import com.vanolucas.cppsolver.old.v9.eval.Evaluator;
import com.vanolucas.cppsolver.old.v9.quality.Quality;
import com.vanolucas.cppsolver.old.v9.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v9.solution.Solution;
import com.vanolucas.cppsolver.old.v9.util.Observable;
import com.vanolucas.cppsolver.old.v9.util.Pool;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

/**
 * Iterative optimization algorithm with multithreading capability.
 */
class Algo implements Observable<AlgoListener> {
    private long iteration = 0L;
    private final AlgoStep step;
    private Quality best;
    private final Pool<Evaluator> evaluators;
    private final ForkJoinPool executor;
    private final AtomicBoolean pauseBeforeNextIteration = new AtomicBoolean(false);
    private final List<AlgoListener> listeners = new ArrayList<>(4);

    Algo(AlgoStep step, Pool<Evaluator> evaluators) {
        this(step, evaluators, Runtime.getRuntime().availableProcessors());
    }

    Algo(AlgoStep step, Pool<Evaluator> evaluators, int maxThreads) {
        this(step, evaluators, new ForkJoinPool(maxThreads));
    }

    Algo(AlgoStep step, Pool<Evaluator> evaluators, ForkJoinPool executor) {
        this.step = step;
        this.evaluators = evaluators;
        this.executor = executor;
    }

    public long getIteration() {
        return iteration;
    }

    /**
     * Run the iterative opti algo loop.
     */
    public void run() throws ExecutionException, InterruptedException {
        pauseBeforeNextIteration.set(false);

        // loop until asked to pause
        while (!pauseBeforeNextIteration.get()) {
            // start new iteration
            iteration++;
            // notify listeners that we start a new iteration
            listeners.forEach(l -> l.onNewIteration(iteration));
            // run the iteration
            step.runStep(this);
        }

        // notify listeners that we pause
        listeners.forEach(l -> l.onPause(iteration));
    }

    EvaluatedSolution evaluate(Solution solution) throws ExecutionException, InterruptedException {
        return evaluate(Collections.singletonList(solution)).get(0);
    }

    List<EvaluatedSolution> evaluate(List<Solution> solutions) throws ExecutionException, InterruptedException {
        // make sure the thread pool is not currently used before submitting our evaluation tasks
        synchronized (executor) {
            // evaluate all solutions in parallel using the thread pool
            return executor.submit(() -> solutions.parallelStream()
                    .map(solution -> {
                        // borrow an evaluator and use it to evaluate this solution
                        final Evaluator evaluator = evaluators.fetch();
                        final EvaluatedSolution evaluatedSolution = evaluator.evaluate(solution);
                        evaluators.returnToPool(evaluator);
                        // notify listeners that this solution got evaluated
                        listeners.forEach(l -> {
                            synchronized (l) {
                                l.onEvaluated(evaluatedSolution, iteration);
                            }
                        });
                        return evaluatedSolution;
                    })
                    // wait for all solutions to be evaluated and collect them
                    .collect(Collectors.toList())
            ).get();
        }
    }

    enum Verdict {
        BETTER, EQUIVALENT, WORSE
    }

    Verdict proposeNewBest(EvaluatedSolution candidate) {
        // if candidate is better than the currently known best
        if (best == null || candidate.isBetterThan(best)) {
            // record new best quality
            best = candidate.getQuality();
            // notify listeners that we have a new better solution
            listeners.forEach(l -> l.onBetterSolution(candidate, iteration));
            listeners.forEach(l -> l.onBetterOrEquivalentSolution(candidate, iteration));
            return Verdict.BETTER;
        }
        // else if candidate is equivalent to the currently known best
        else if (candidate.isEquivalentTo(best)) {
            // notify listeners that we have a new equivalent solution
            listeners.forEach(l -> l.onBetterOrEquivalentSolution(candidate, iteration));
            return Verdict.EQUIVALENT;
        }
        // else, the candidate is worse than the currently known best
        else {
            return Verdict.WORSE;
        }
    }

    public void pauseBeforeNextIteration() {
        pauseBeforeNextIteration.set(true);
    }

    @Override
    public void addListener(AlgoListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeListener(AlgoListener listener) {
        listeners.remove(listener);
    }
}
