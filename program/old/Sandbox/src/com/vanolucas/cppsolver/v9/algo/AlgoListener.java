package com.vanolucas.cppsolver.old.v9.algo;

import com.vanolucas.cppsolver.old.v9.solution.EvaluatedSolution;

interface AlgoListener {
    default void onNewIteration(long iteration) {
    }

    default void onPause(long finishedIteration) {
    }

    default void onEvaluated(EvaluatedSolution solution, long iteration) {
    }

    default void onBetterSolution(EvaluatedSolution solution, long iteration) {
    }

    default void onBetterOrEquivalentSolution(EvaluatedSolution solution, long iteration) {
    }
}
