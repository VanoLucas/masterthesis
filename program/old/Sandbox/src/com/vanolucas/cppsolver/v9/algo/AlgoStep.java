package com.vanolucas.cppsolver.old.v9.algo;

import java.util.concurrent.ExecutionException;

abstract class AlgoStep {
    abstract void runStep(Algo algo) throws ExecutionException, InterruptedException;
}
