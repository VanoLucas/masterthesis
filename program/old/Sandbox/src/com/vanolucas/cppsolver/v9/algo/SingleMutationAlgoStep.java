package com.vanolucas.cppsolver.old.v9.algo;

import com.vanolucas.cppsolver.old.v9.mutation.Mutator;
import com.vanolucas.cppsolver.old.v9.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v9.solution.Solution;

import java.util.concurrent.ExecutionException;

class SingleMutationAlgoStep extends AlgoStep {
    private final Solution currentSolution;
    private final Mutator mutator;

    SingleMutationAlgoStep(Solution initialSolution, Mutator mutator) {
        this.currentSolution = initialSolution;
        this.mutator = mutator;
    }

    @Override
    void runStep(Algo algo) throws ExecutionException, InterruptedException {
        // mutate solution
        mutator.mutate(currentSolution);
        // evaluate it
        final EvaluatedSolution evaluatedSolution = algo.evaluate(currentSolution);
        // propose it
        algo.proposeNewBest(evaluatedSolution);
    }
}
