package com.vanolucas.cppsolver.old.v9.cpp.eval;

import com.vanolucas.cppsolver.old.v9.cpp.instance.CppInstance;
import com.vanolucas.cppsolver.old.v9.cpp.solution.CppSolution;
import com.vanolucas.cppsolver.old.v9.cpp.solution.Plate;
import com.vanolucas.cppsolver.old.v9.util.Converter;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Slots2CppSolution implements Converter<Slots, CppSolution> {
    private final int nbSlotsPerPlate;

    public Slots2CppSolution(CppInstance cppInstance) {
        this(cppInstance.getNbSlotsPerPlate());
    }

    public Slots2CppSolution(int nbSlotsPerPlate) {
        this.nbSlotsPerPlate = nbSlotsPerPlate;
    }

    @Override
    public CppSolution convert(Slots slots) {
        // sort slots by number of printings so that we regroup on the same offset plate slots that have a similar nb of printings to reduce waste
        final List<Slot> sortedSlots = slots.stream()
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());

        // build collection of offset plates based on our allocated slots
        final int nbSlots = sortedSlots.size();
        final List<Plate> plates = new ArrayList<>(nbSlots / nbSlotsPerPlate);
        for (int indexFirstSlotOfPlate = 0; indexFirstSlotOfPlate < nbSlots; indexFirstSlotOfPlate += nbSlotsPerPlate) {
            final int ixFirstSlotOfPlate = indexFirstSlotOfPlate;

            final List<Slot> slotsForThisPlate = IntStream.range(0, nbSlotsPerPlate)
                    .mapToObj(i -> sortedSlots.get(ixFirstSlotOfPlate + i))
                    .collect(Collectors.toList());

            plates.add(new Plate(slotsForThisPlate));
        }

        // construct CPP solution with our offset plates
        return new CppSolution(plates);
    }
}
