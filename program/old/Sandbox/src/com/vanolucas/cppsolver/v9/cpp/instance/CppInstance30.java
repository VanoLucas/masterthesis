package com.vanolucas.cppsolver.old.v9.cpp.instance;

import java.util.Arrays;
import java.util.List;

/**
 * 30 book covers reference instance of the Cover Printing Problem.
 */
public class CppInstance30 extends CppInstance {
    private static final List<Integer> COVERS = Arrays.asList(
            30000,
            28000,
            27000,
            26000,
            26000,
            23000,
            22000,
            22000,
            20000,
            20000,
            19000,
            18000,
            17000,
            16000,
            15000,
            15000,
            14000,
            13500,
            13000,
            11000,
            10500,
            10000,
            9000,
            9000,
            7500,
            6000,
            5000,
            2500,
            1500,
            1000
    );

    private static final int DEFAULT_NB_PLATES = 10;

    public CppInstance30() {
        this(DEFAULT_NB_PLATES);
    }

    public CppInstance30(int nbPlates) {
        super(COVERS, nbPlates);
    }

    public CppInstance30(int nbPlates, int nbSlotsPerPlate) {
        super(COVERS, nbPlates, nbSlotsPerPlate);
    }

    public CppInstance30(int nbPlates, int nbSlotsPerPlate, double costPlate, double costPrinting) {
        super(COVERS, nbPlates, nbSlotsPerPlate, costPlate, costPrinting);
    }
}
