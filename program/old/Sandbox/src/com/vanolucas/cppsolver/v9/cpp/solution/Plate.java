package com.vanolucas.cppsolver.old.v9.cpp.solution;

import com.vanolucas.cppsolver.old.v9.cpp.eval.Slot;

import java.util.List;
import java.util.stream.Collectors;

public class Plate implements Comparable<Plate> {
    private final List<Integer> covers;
    private final int nbPrintings;

    public Plate(List<Slot> slots) {
        this(
                slots.stream()
                        .map(Slot::getCoverIndex)
                        .collect(Collectors.toList()),
                slots.stream()
                        .mapToInt(Slot::getNbPrintings)
                        .max().orElseThrow(IllegalStateException::new)
        );
    }

    public Plate(List<Integer> covers, int nbPrintings) {
        this.covers = covers;
        this.nbPrintings = nbPrintings;
    }

    int getNbPrintings() {
        return nbPrintings;
    }

    @Override
    public int compareTo(Plate other) {
        return Integer.compare(nbPrintings, other.nbPrintings);
    }

    @Override
    public String toString() {
        final String strCovers = covers.stream()
                .sorted()
                .map(cover -> String.format("%3d", cover))
                .collect(Collectors.joining("|", "|", "|"));
        return String.format("%s\t%6d printings",
                strCovers,
                nbPrintings
        );
    }
}
