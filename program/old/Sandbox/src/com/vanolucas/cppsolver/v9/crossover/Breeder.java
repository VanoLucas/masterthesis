package com.vanolucas.cppsolver.old.v9.crossover;

import com.vanolucas.cppsolver.old.v9.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v9.solution.Solution;

import java.util.List;
import java.util.function.Function;

/**
 * Performs reproduction (crossover and/or mutation) on a group of parents and outputs the corresponding children (offspring).
 */
public interface Breeder extends Function<List<EvaluatedSolution>, List<Solution>> {
    @Override
    default List<Solution> apply(List<EvaluatedSolution> parents) {
        return breed(parents);
    }

    List<Solution> breed(List<EvaluatedSolution> parents);
}
