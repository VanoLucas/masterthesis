package com.vanolucas.cppsolver.old.v9.eval;

import com.vanolucas.cppsolver.old.v9.quality.Quality;

import java.util.function.Function;

public interface EvaluationFunction<TPhenotype, TQuality extends Quality> extends Function<TPhenotype, TQuality> {
    @Override
    default TQuality apply(TPhenotype phenotype) {
        return evaluatePhenotype(phenotype);
    }

    default TQuality evaluate(Object phenotype) {
        //noinspection unchecked
        return evaluatePhenotype((TPhenotype) phenotype);
    }

    TQuality evaluatePhenotype(TPhenotype phenotype);
}
