package com.vanolucas.cppsolver.old.v9.eval;

import com.vanolucas.cppsolver.old.v9.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v9.solution.Solution;

public class Evaluator {
    private final EvaluationFunction phenotypeEvalFn;

    public Evaluator(EvaluationFunction phenotypeEvalFn) {
        this.phenotypeEvalFn = phenotypeEvalFn;
    }

    public EvaluatedSolution evaluate(Solution solution) {
        return new EvaluatedSolution(
                solution,
                solution.evaluatePhenotype(phenotypeEvalFn)
        );
    }
}
