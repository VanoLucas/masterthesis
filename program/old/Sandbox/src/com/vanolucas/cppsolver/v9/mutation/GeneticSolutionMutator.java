package com.vanolucas.cppsolver.old.v9.mutation;

import com.vanolucas.cppsolver.old.v9.solution.GeneticSolution;
import com.vanolucas.cppsolver.old.v9.solution.Solution;

public class GeneticSolutionMutator extends SolutionMutator {
    private final Mutation genotypeMutation;

    public GeneticSolutionMutator(Mutation genotypeMutation) {
        this(genotypeMutation, null);
    }

    public GeneticSolutionMutator(Mutation genotypeMutation, Mutation phenotypeMutation) {
        super(phenotypeMutation);
        this.genotypeMutation = genotypeMutation;
    }

    @Override
    public Solution mutate(Solution solution) {
        final GeneticSolution geneticSolution = (GeneticSolution) solution;
        // mutate genotype
        if (genotypeMutation != null) {
            geneticSolution.mutateGenotype(genotypeMutation);
        }
        // then mutate phenotype if needed
        return super.mutate(geneticSolution);
    }
}
