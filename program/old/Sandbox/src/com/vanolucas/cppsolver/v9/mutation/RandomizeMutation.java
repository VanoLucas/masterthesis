package com.vanolucas.cppsolver.old.v9.mutation;

import com.vanolucas.cppsolver.old.v9.util.Rand;
import com.vanolucas.cppsolver.old.v9.util.Randomizable;

public class RandomizeMutation implements Mutation<Randomizable> {
    private final Rand rand;

    public RandomizeMutation() {
        this(new Rand());
    }

    public RandomizeMutation(Rand rand) {
        this.rand = rand;
    }

    @Override
    public void mutate(Randomizable toRandomize) {
        toRandomize.randomize(rand);
    }
}
