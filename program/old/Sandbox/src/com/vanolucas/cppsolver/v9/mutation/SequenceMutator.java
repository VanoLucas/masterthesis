package com.vanolucas.cppsolver.old.v9.mutation;

import com.vanolucas.cppsolver.old.v9.solution.Solution;

import java.util.Arrays;
import java.util.List;

public class SequenceMutator implements Mutator {
    private final List<Mutator> mutators;

    public SequenceMutator(Mutator mutator1, Mutator mutator2) {
        this(Arrays.asList(mutator1, mutator2));
    }

    public SequenceMutator(List<Mutator> mutators) {
        this.mutators = mutators;
    }

    @Override
    public Solution mutate(Solution solution) {
        mutators.forEach(mutator ->
                mutator.mutate(solution)
        );
        return solution;
    }
}
