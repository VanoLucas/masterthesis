package com.vanolucas.cppsolver.old.v9.mutation;

import com.vanolucas.cppsolver.old.v9.solution.Solution;

public class SolutionMutator implements Mutator {
    private final Mutation phenotypeMutation;

    public SolutionMutator(Mutation phenotypeMutation) {
        this.phenotypeMutation = phenotypeMutation;
    }

    @Override
    public Solution mutate(Solution solution) {
        if (phenotypeMutation != null) {
            solution.mutatePhenotype(phenotypeMutation);
        }
        return solution;
    }
}
