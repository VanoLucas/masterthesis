package com.vanolucas.cppsolver.old.v9.mutation.rkv;

import com.vanolucas.cppsolver.old.v9.genome.RandomKeyVector;
import com.vanolucas.cppsolver.old.v9.mutation.Mutation;
import com.vanolucas.cppsolver.old.v9.mutation.rkv.key.KeyMutation;
import com.vanolucas.cppsolver.old.v9.util.Rand;

public class ProbaRKVMutation implements Mutation<RandomKeyVector> {
    private final KeyMutation keyMutation;
    private final double probaKeyMutation;
    private final Rand rand;

    public ProbaRKVMutation(KeyMutation keyMutation, double probaKeyMutation, Rand rand) {
        this.keyMutation = keyMutation;
        this.probaKeyMutation = probaKeyMutation;
        this.rand = rand;
    }

    @Override
    public void mutate(RandomKeyVector rkv) {
        final int length = rkv.length();
        for (int k = 0; k < length; k++) {
            if (rand.double01() < probaKeyMutation) {
                keyMutation.mutateKey(k, rkv);
            }
        }
    }
}
