package com.vanolucas.cppsolver.old.v9.mutation.rkv.key;

import com.vanolucas.cppsolver.old.v9.genome.RandomKeyVector;

public interface KeyMutation {
    default void mutateKey(int index, RandomKeyVector rkv) {
        rkv.mutateKey(index, this);
    }

    Double newKeyValue(int index, RandomKeyVector rkv);
}
