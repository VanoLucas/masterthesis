package com.vanolucas.cppsolver.old.v9.solution;

import com.vanolucas.cppsolver.old.v9.util.Cloner;

public class GeneticSolutionCloner extends SolutionCloner {
    private final Cloner genotypeCloner;

    public GeneticSolutionCloner(Cloner genotypeCloner) {
        this(genotypeCloner, null);
    }

    public GeneticSolutionCloner(Cloner genotypeCloner, Cloner phenotypeCloner) {
        super(phenotypeCloner);
        this.genotypeCloner = genotypeCloner;
    }

    @Override
    public GeneticSolution clone(Solution solution) {
        final GeneticSolution geneticSolution = (GeneticSolution) solution;
        return clone(geneticSolution);
    }

    public GeneticSolution clone(GeneticSolution geneticSolution) {
        return geneticSolution.clone(genotypeCloner, phenotypeCloner);
    }
}
