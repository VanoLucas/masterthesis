package com.vanolucas.cppsolver.old.v9.util;

import java.util.function.Function;

public interface Cloner<T> extends Function<T, T> {
    @Override
    default T apply(T toClone) {
        return clone(toClone);
    }

    default T cloneObj(Object obj) {
        //noinspection unchecked
        return clone((T) obj);
    }

    T clone(T toClone);
}
