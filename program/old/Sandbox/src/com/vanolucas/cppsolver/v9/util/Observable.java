package com.vanolucas.cppsolver.old.v9.util;

public interface Observable<TListener> {
    void addListener(TListener listener);

    void removeListener(TListener listener);
}
