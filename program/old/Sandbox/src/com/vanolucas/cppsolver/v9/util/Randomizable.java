package com.vanolucas.cppsolver.old.v9.util;

public interface Randomizable {
    default void randomize() {
        randomize(new Rand());
    }

    void randomize(Rand rand);
}
