package com.vanolucas.cppsolver.old.v9.util.rangeguard;

public interface DoubleRangeGuard {
    double bringBackToRange(double value, double minInclusive, double maxExclusive);
}
