package com.vanolucas.cppsolver.old.v1.cpp.problem;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class StandardCppInstanceTest {

    @Test
    void testToString() {
        final List<Integer> copies = Arrays.asList(50, 30, 100, 10);
        StandardCppInstance cppInstance = StandardCppInstance.newInstance(copies, 3, 4);
        System.out.println(cppInstance);
    }
}
