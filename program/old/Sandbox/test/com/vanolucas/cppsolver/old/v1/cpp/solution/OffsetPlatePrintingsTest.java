package com.vanolucas.cppsolver.old.v1.cpp.solution;

import org.junit.jupiter.api.Test;

class OffsetPlatePrintingsTest {

    @Test
    void testToString() {
        OffsetPlate offsetPlate = OffsetPlate.withNbSlots(4);
        OffsetPlatePrintings offsetPlatePrintings = new OffsetPlatePrintings(offsetPlate, 10);
        System.out.println(offsetPlatePrintings);
    }
}