package com.vanolucas.cppsolver.old.v1.cpp.solution;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertThrows;

class OffsetPlateTest {

    @Test
    void testToString() {
        OffsetPlate offsetPlate = OffsetPlate.withNbSlots(4);
        System.out.println(offsetPlate);

        offsetPlate = OffsetPlate.withBookCovers(Arrays.asList(1, 2, 3, 4));
        System.out.println(offsetPlate);
    }

    @Test
    void addBookCover() {
        OffsetPlate offsetPlate = OffsetPlate.withNbSlots(2);

        offsetPlate.addBookCover(1);
        System.out.println(offsetPlate);

        offsetPlate.addBookCover(2);
        System.out.println(offsetPlate);

        assertThrows(IndexOutOfBoundsException.class, () ->
                offsetPlate.addBookCover(3)
        );
        System.out.println(offsetPlate);
    }
}