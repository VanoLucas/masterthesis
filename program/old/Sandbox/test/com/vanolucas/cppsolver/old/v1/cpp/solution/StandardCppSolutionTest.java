package com.vanolucas.cppsolver.old.v1.cpp.solution;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

class StandardCppSolutionTest {

    @Test
    void testToString() {
        OffsetPlate offsetPlate = OffsetPlate.withNbSlots(4);
        OffsetPlatePrintings offsetPlatePrintings1 = new OffsetPlatePrintings(offsetPlate, 100);
        OffsetPlatePrintings offsetPlatePrintings2 = new OffsetPlatePrintings(offsetPlate, 200);
        StandardCppSolution solution = StandardCppSolution.withOffsetPlatePrintings(
                Arrays.asList(
                        offsetPlatePrintings1,
                        offsetPlatePrintings2
                )
        );
        System.out.println(solution);
    }
}