package com.vanolucas.cppsolver.old.v1.genetic;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Double01GeneTest {

    @Test
    void testClone() throws CloneNotSupportedException {
        Double01Gene gene = Double01Gene.newInit(0.123d);
        Double01Gene cloned = gene.clone();
        assertEquals(gene.getValue(), cloned.getValue());
    }
}