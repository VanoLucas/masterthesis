package com.vanolucas.cppsolver.old.v1.opti.algo;

import com.vanolucas.cppsolver.old.v1.cpp.eval.EvaluatorCP;
import com.vanolucas.cppsolver.old.v1.cpp.eval.EvaluatorSFP;
import com.vanolucas.cppsolver.old.v1.cpp.indiv.IndividualCP;
import com.vanolucas.cppsolver.old.v1.cpp.indiv.IndividualSFP;
import com.vanolucas.cppsolver.old.v1.cpp.problem.StandardCppInstance;
import com.vanolucas.cppsolver.old.v1.cpp.problem.StandardCppInstance30;
import com.vanolucas.cppsolver.old.v1.genetic.Genome;
import com.vanolucas.cppsolver.old.v1.opti.clone.GenotypeOnlyGeneticIndividualCloner;
import com.vanolucas.cppsolver.old.v1.opti.clone.IndividualCloner;
import com.vanolucas.cppsolver.old.v1.opti.evaluation.IndividualEvaluator;
import com.vanolucas.cppsolver.old.v1.opti.indiv.GeneticIndividual;
import com.vanolucas.cppsolver.old.v1.opti.indiv.Individual;
import com.vanolucas.cppsolver.old.v1.opti.mutation.*;
import com.vanolucas.cppsolver.old.v1.opti.mutation.gene.AddRandomValueDouble01GeneMutator;
import com.vanolucas.cppsolver.old.v1.opti.mutation.gene.RandomizeGeneMutator;
import com.vanolucas.cppsolver.old.v1.opti.mutation.genome.OneRandomGeneGenomeMutator;
import com.vanolucas.cppsolver.old.v1.opti.mutation.genome.ProbaToMutateGeneGenomeMutator;
import com.vanolucas.cppsolver.old.v1.opti.mutation.genome.RandomizeGenomeMutator;
import com.vanolucas.cppsolver.old.v1.opti.pause.PauseAfterIteration;
import com.vanolucas.cppsolver.old.v1.opti.quality.Quality;
import com.vanolucas.cppsolver.old.v1.opti.randomizer.IndividualRandomizer;
import com.vanolucas.cppsolver.old.v1.opti.randomizer.RandomizableGenotypeRandomizer;
import com.vanolucas.cppsolver.old.v1.sumproblem.SumGeneticIndividual;
import com.vanolucas.cppsolver.old.v1.sumproblem.SumGeneticIndividualEvaluator;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

class FirstBetterNeighborHillClimbingAlgoTest {

    @Test
    void runSumProblem() throws CloneNotSupportedException {
        final int target = 1000;

        // generate a random start individual
        final Individual startIndividual = new SumGeneticIndividual();
        new RandomizableGenotypeRandomizer().randomize(startIndividual);

        // solution randomizer
        IndividualRandomizer randomizer = new RandomizableGenotypeRandomizer();

        // individual cloner that only needs to clone the genotype
        final IndividualCloner cloner = new GenotypeOnlyGeneticIndividualCloner();

        // mutation operators
        final int nbMutators = 3;
        final IndividualMutator mutator = new GenotypeOnlyGeneticIndividualMutator(
                new OneRandomGeneGenomeMutator(new RandomizeGeneMutator())
        );
        final List<IndividualMutator> mutators = new ArrayList<>(nbMutators);
        for (int i = 0; i < nbMutators; i++) {
            mutators.add(mutator);
        }

        // evaluation function
        final IndividualEvaluator evaluator = new SumGeneticIndividualEvaluator(target);

        // hill climbing opti algo
        FirstBetterNeighborHillClimbingAlgo algo = new FirstBetterNeighborHillClimbingAlgo(
                startIndividual, randomizer, cloner, mutators, evaluator
        );

        algo.addListener(new OptiAlgoListener() {
            @Override
            public void onNewBetter(long iteration, Individual newBetterIndiv) {
                System.out.println(String.format("\nNew better at iteration %d:\n%s", iteration, newBetterIndiv));
            }
        });

        algo.addPauseCriterion(new PauseAfterIteration(1000));

        algo.run();
    }

    @Test
    void runStandardCpp() throws CloneNotSupportedException {
        // reference standard cpp 30 book covers instance
        StandardCppInstance problem = new StandardCppInstance30();

        // generate a random start solution
//        Individual startIndividual = new IndividualSFP(problem);
        Individual startIndividual = new IndividualCP(problem);
        new RandomizableGenotypeRandomizer().randomize(startIndividual);

        // solution randomizer
        IndividualRandomizer randomizer = new RandomizableGenotypeRandomizer();

        // individual cloner that only needs to clone the genotype
        final IndividualCloner cloner = new GenotypeOnlyGeneticIndividualCloner();

        // mutation operators

        // 1st set of mutators
        // 1 random gene mutation
        final int nb1RandomGeneMutators = 3;
        final IndividualMutator mutator = new GenotypeOnlyGeneticIndividualMutator(
                new OneRandomGeneGenomeMutator(new RandomizeGeneMutator())
        );
        final List<IndividualMutator> mutators = new ArrayList<>();
        for (int i = 0; i < nb1RandomGeneMutators; i++) {
            mutators.add(mutator);
        }
        // proba to randomize each gene mutation
        final int genomeLength = ((Genome) ((GeneticIndividual) startIndividual).getGenotype()).length();
        mutators.add(new GenotypeOnlyGeneticIndividualMutator(
                new ProbaToMutateGeneGenomeMutator(2d / genomeLength,
                        new RandomizeGeneMutator()
                )
        ));
        mutators.add(new GenotypeOnlyGeneticIndividualMutator(
                new ProbaToMutateGeneGenomeMutator(3d / genomeLength,
                        new RandomizeGeneMutator()
                )
        ));
        mutators.add(new GenotypeOnlyGeneticIndividualMutator(
                new ProbaToMutateGeneGenomeMutator(4d / genomeLength,
                        new RandomizeGeneMutator()
                )
        ));
        mutators.add(new GenotypeOnlyGeneticIndividualMutator(
                new ProbaToMutateGeneGenomeMutator(1d / 4d,
                        new RandomizeGeneMutator()
                )
        ));
        mutators.add(new GenotypeOnlyGeneticIndividualMutator(
                new ProbaToMutateGeneGenomeMutator(1d / 3d,
                        new RandomizeGeneMutator()
                )
        ));
        mutators.add(new GenotypeOnlyGeneticIndividualMutator(
                new ProbaToMutateGeneGenomeMutator(1d / 2d,
                        new RandomizeGeneMutator()
                )
        ));
        // add random value in range to random genes mutation
        mutators.add(new GenotypeOnlyGeneticIndividualMutator(
                new ProbaToMutateGeneGenomeMutator(2d / genomeLength,
                        new AddRandomValueDouble01GeneMutator(-0.1d, +0.1d)
                )
        ));
        mutators.add(new GenotypeOnlyGeneticIndividualMutator(
                new ProbaToMutateGeneGenomeMutator(2d / genomeLength,
                        new AddRandomValueDouble01GeneMutator(-0.01d, +0.01d)
                )
        ));
        mutators.add(new GenotypeOnlyGeneticIndividualMutator(
                new ProbaToMutateGeneGenomeMutator(3d / genomeLength,
                        new AddRandomValueDouble01GeneMutator(-0.1d, +0.1d)
                )
        ));
        mutators.add(new GenotypeOnlyGeneticIndividualMutator(
                new ProbaToMutateGeneGenomeMutator(3d / genomeLength,
                        new AddRandomValueDouble01GeneMutator(-0.01d, +0.01d)
                )
        ));
        // add same random value in range to all genes mutation
        mutators.add(new GenotypeOnlyGeneticIndividualMutator(
                new AddSameRandomValueToAllKeys(-0.1d, +0.1d)
        ));
        mutators.add(new GenotypeOnlyGeneticIndividualMutator(
                new AddSameRandomValueToAllKeys(-0.01d, +0.01d)
        ));
        mutators.add(new GenotypeOnlyGeneticIndividualMutator(
                new AddSameRandomValueToAllKeys(-0.001d, +0.001d)
        ));
        // randomize whole genome mutation
        mutators.add(new GenotypeOnlyGeneticIndividualMutator(
                new RandomizeGenomeMutator()
        ));

        // 2nd set of mutators
        mutators.clear();
        mutators.addAll(DoubleStream.iterate(0.01d, p -> p + 0.0025d).limit(10L)
                .mapToObj(proba -> new GenotypeOnlyGeneticIndividualMutator(
                        new ProbaToMutateGeneGenomeMutator(proba, new RandomizeGeneMutator())
                ))
                .collect(Collectors.toList())
        );
        mutators.addAll(DoubleStream.iterate(0.05d, p -> p + 0.0025d).limit(10L)
                .mapToObj(proba -> new GenotypeOnlyGeneticIndividualMutator(
                        new ProbaToMutateGeneGenomeMutator(proba, new RandomizeGeneMutator())
                ))
                .collect(Collectors.toList())
        );
        mutators.addAll(DoubleStream.iterate(0.90d, p -> p + 0.01d).limit(3L)
                .mapToObj(proba -> new GenotypeOnlyGeneticIndividualMutator(
                        new ProbaToMutateGeneGenomeMutator(proba, new RandomizeGeneMutator())
                ))
                .collect(Collectors.toList())
        );
        mutators.add(new GenotypeOnlyGeneticIndividualMutator(
                new ProbaToMutateGeneGenomeMutator(2d / genomeLength,
                        new AddRandomValueDouble01GeneMutator(-0.1d, +0.1d)
                )
        ));
        mutators.add(new GenotypeOnlyGeneticIndividualMutator(
                new ProbaToMutateGeneGenomeMutator(2d / genomeLength,
                        new AddRandomValueDouble01GeneMutator(-0.01d, +0.01d)
                )
        ));
        mutators.add(new GenotypeOnlyGeneticIndividualMutator(
                new ProbaToMutateGeneGenomeMutator(3d / genomeLength,
                        new AddRandomValueDouble01GeneMutator(-0.1d, +0.1d)
                )
        ));
        mutators.add(new GenotypeOnlyGeneticIndividualMutator(
                new ProbaToMutateGeneGenomeMutator(3d / genomeLength,
                        new AddRandomValueDouble01GeneMutator(-0.01d, +0.01d)
                )
        ));

        // evaluation function
//        IndividualEvaluator evaluator = new EvaluatorSFP(problem);
        IndividualEvaluator evaluator = new EvaluatorCP(problem);

        // hill climbing opti algo
        FirstBetterNeighborHillClimbingAlgo algo = new FirstBetterNeighborHillClimbingAlgo(
                startIndividual, randomizer, cloner, mutators, evaluator
        );

        // random restart condition
        final long secondsStuckAtLocalOptimum = 3L;
        RandomRestartHillClimbingAfterDurationAtLocalOptimum.applyTo(algo, Duration.ofSeconds(secondsStuckAtLocalOptimum));

        // listener to print logs
        algo.addListener(new HillClimbingAlgoListener() {
            @Override
            public void onNewBetter(long iteration, Individual newBetterIndiv) {
                System.out.println(String.format("\n[%s]\nNew better at iteration %d:\n%s",
                        Instant.now(),
                        iteration,
                        newBetterIndiv
                ));
            }

            @Override
            public void onRandomRestart(long iteration, Quality qualityBeforeRestart) {
                System.out.println(String.format("Random restart at iteration %d after being stuck for %d seconds at cost %s.",
                        iteration,
                        secondsStuckAtLocalOptimum,
                        qualityBeforeRestart
                ));
            }
        });

        // end criterion
        algo.addPauseCriterion(new PauseAfterIteration(3000000));

        // run our hill climbing
        algo.run();
    }
}

/*

New better at iteration 762632:
Genotype:
GenomeSFP
0.971201945352786;0.9592499949306521;0.22506173463409218;0.7772811165752854;0.5810381921835654;0.5167013017868499;0.8716220022819359;0.513829449935682;0.9066831096501027;0.4946668816567422;0.5430621827701898;0.9834790147827397;0.7757317125859525;0.9184821714660893;0.38371445312635577;0.3236998351780458;0.658561608769482;0.38195570385617394;0.13693260622314307;0.11088969542733618;0.3882185379570907;0.2618823786535093;0.27937287780390563;0.5301039585308346;0.38248978899980735;0.6002198238121939;0.5568781525394881;0.618703387267533;0.4992429502206114;0.15610676423295;0.7796294223846197;0.07918709245200517;0.8240346089146555;0.7066363182511776;0.5283643789432517;0.9385677655462882;0.14174552951802843;0.05381624070317825;0.8801055899475193;0.9741360956118831;0.9315320078347117;0.7853267242363888;0.5106240550952931;0.5786953346841682;0.18249479432132265;0.9784922225638534;0.04303752594196952;0.19035400187691687;0.5226031965308768;0.9042116393664306;0.3418928548811341;0.6167664613116267;0.29888605183333083;0.5505373463175952;0.9194911417385212;0.40402645242245616;0.28148897770512304;0.8139986102923191;0.2694935107386386;0.613524870958656;0.08661971864311202;0.27862350854197326;0.04950516214935252;0.15528829003694156;0.7969898812211653;0.7676537306018317;0.06090166116386797;0.522592076764635;0.6166804393829359;0.566407430077485
(70 genes)
Phenotype:
Slots: 29 (1000 required) | 5 (1152 required) | 11 (1295 required) | 28 (1500 required) | 27 (2500 required) | 24 (2859 required) | 0 (3000 required) | 8 (3000 required) | 24 (4641 required) | 21 (5000 required) | 21 (5000 required) | 26 (5000 required) | 25 (6000 required) | 1 (6197 required) | 18 (6500 required) | 18 (6500 required) | 10 (8584 required) | 9 (9000 required) | 22 (9000 required) | 23 (9000 required) | 10 (10416 required) | 20 (10500 required) | 9 (11000 required) | 19 (11000 required) | 17 (13500 required) | 16 (14000 required) | 14 (15000 required) | 15 (15000 required) | 13 (16000 required) | 11 (16705 required) | 8 (17000 required) | 12 (17000 required) | 1 (21803 required) | 5 (21848 required) | 6 (22000 required) | 7 (22000 required) | 3 (26000 required) | 4 (26000 required) | 0 (27000 required) | 2 (27000 required)
Solution:
Offset plates:
  3|  4|  0|  2: 27000 printings
  1|  5|  6|  7: 22000 printings
 13| 11|  8| 12: 17000 printings
 17| 16| 14| 15: 15000 printings
 10| 20|  9| 19: 11000 printings
 10|  9| 22| 23: 9000 printings
 25|  1| 18| 18: 6500 printings
 24| 21| 21| 26: 5000 printings
 27| 24|  0|  8: 3000 printings
 29|  5| 11| 28: 1500 printings
Cost = 117000

[2019-04-09T09:17:53.652Z]
New better at iteration 280916:
Genotype:
GenomeSFP
0.541907964517537;0.949852422019517;0.6808012229409715;0.3192035413456289;0.8436757974326574;0.7496783649213906;0.3510587138748308;0.8742500984958345;0.6523727523156787;0.6451614664821952;0.5840178120651307;0.627182500672794;0.09661372923726666;0.18596131959011022;0.7015976913550107;0.6026952342054079;0.879428054598384;0.09354473381213238;0.22250932559300857;0.29559662473416737;0.32298392145224586;0.24279300863906264;0.3611676039118663;0.023026438868073318;0.9022321877292812;0.2373041921461384;0.7381584135211057;0.4865943044391048;0.4524269783136064;0.7395483282473058;0.8819955835797298;0.7847465969370714;0.19729200175990128;0.3934341387143304;0.3071340494825619;0.10049987557517626;0.3361533275809345;0.2348483662609441;0.41124515799127614;0.18608656575738405;0.4948777400328789;0.8237071156744823;0.2723838300473436;0.5984799070622416;0.9253408378596008;0.9463378296981192;0.8707883656740558;0.7391538233901888;0.9644542746116083;0.7373950481173178;0.6471154387496922;0.5000336808738055;0.17029028332514717;0.2061637345506787;0.4645176601607044;0.4362610409635811;0.49677147963828816;0.40682218509281437;0.3877776221734669;0.024690391482170386;0.6744619500569883;0.905484171457662;0.5079970251246537;0.28852649123361707;0.03567434027261059;0.048472272266064695;0.48382248689959045;0.0540926253602145;0.8260764289746975;0.08726275500934857
(70 genes)
Phenotype:
Slots: 17 (921 required) | 11 (1000 required) | 25 (1000 required) | 29 (1000 required) | 28 (1500 required) | 27 (2500 required) | 24 (2699 required) | 4 (2706 required) | 24 (4801 required) | 16 (5000 required) | 25 (5000 required) | 26 (5000 required) | 2 (7553 required) | 16 (9000 required) | 22 (9000 required) | 23 (9000 required) | 21 (10000 required) | 20 (10500 required) | 3 (11000 required) | 19 (11000 required) | 17 (12579 required) | 0 (13000 required) | 1 (13000 required) | 18 (13000 required) | 1 (15000 required) | 3 (15000 required) | 14 (15000 required) | 15 (15000 required) | 13 (16000 required) | 0 (17000 required) | 11 (17000 required) | 12 (17000 required) | 10 (19000 required) | 2 (19447 required) | 8 (20000 required) | 9 (20000 required) | 6 (22000 required) | 7 (22000 required) | 5 (23000 required) | 4 (23294 required)
Solution:
Offset plates:
  6|  7|  5|  4: 23294 printings
 10|  2|  8|  9: 20000 printings
 13|  0| 11| 12: 17000 printings
  1|  3| 14| 15: 15000 printings
 17|  0|  1| 18: 13000 printings
 21| 20|  3| 19: 11000 printings
  2| 16| 22| 23: 9000 printings
 24| 16| 25| 26: 5000 printings
 28| 27| 24|  4: 2706 printings
 17| 11| 25| 29: 1000 printings
Cost = 117000

[2019-04-09T20:42:45.694Z]
New better at iteration 520733:
Genotype:
GenomeCP
0.0;0.06669816162053475;0.5904892329419212;0.33823919490652704;0.9030198034943346;0.6378816955078747;0.4531399649138419;0.31252048441015423;0.12454750659150772;0.15386090555952833;0.8244199790381874;0.50478512995186;0.4012270169492975;0.8823875542934712;0.8570918572435396;0.8334239286163916;0.18816079373947583;0.043483642531919886;0.6068718744353266;0.3077093567240309
(20 genes)
Phenotype:
Slots: 27 (906 required) | 25 (1000 required) | 29 (1000 required) | 5 (1000 required) | 28 (1500 required) | 27 (1594 required) | 12 (2000 required) | 0 (2000 required) | 24 (3715 required) | 24 (3785 required) | 3 (4000 required) | 18 (4000 required) | 17 (4566 required) | 26 (5000 required) | 13 (5000 required) | 25 (5000 required) | 17 (8934 required) | 18 (9000 required) | 22 (9000 required) | 23 (9000 required) | 21 (10000 required) | 20 (10500 required) | 13 (11000 required) | 19 (11000 required) | 16 (14000 required) | 14 (15000 required) | 15 (15000 required) | 12 (15000 required) | 11 (18000 required) | 10 (19000 required) | 8 (20000 required) | 9 (20000 required) | 3 (22000 required) | 5 (22000 required) | 6 (22000 required) | 7 (22000 required) | 4 (26000 required) | 2 (27000 required) | 0 (28000 required) | 1 (28000 required)
Solution:
Offset plates:
  4|  2|  0|  1: 28000 printings
  3|  5|  6|  7: 22000 printings
 11| 10|  8|  9: 20000 printings
 16| 14| 15| 12: 15000 printings
 21| 20| 13| 19: 11000 printings
 17| 18| 22| 23: 9000 printings
 17| 26| 13| 25: 5000 printings
 24| 24|  3| 18: 4000 printings
 28| 27| 12|  0: 2000 printings
 27| 25| 29|  5: 1000 printings
Cost = 117000

[2019-04-09T20:53:33.038Z]
New better at iteration 68626:
Genotype:
GenomeCP
0.46132578962529025;0.06254361344127003;0.6275101585793755;0.1538609967307068;0.41384471270430795;0.3529696642525072;0.01313399219285151;0.8456105833145722;0.9011795808852874;0.7427002140932472;0.18721991569570753;0.04010571530253024;0.03917165274653196;0.07142959373199533;0.717300527160372;0.4590324135008576;0.5876138251337326;0.33739683458632963;0.07179334586627882;0.8209820474672193
(20 genes)
Phenotype:
Slots: 27 (644 required) | 5 (922 required) | 29 (1000 required) | 13 (1000 required) | 28 (1500 required) | 27 (1856 required) | 18 (2000 required) | 1 (2000 required) | 17 (4554 required) | 21 (4590 required) | 0 (4632 required) | 2 (4834 required) | 26 (5000 required) | 21 (5410 required) | 25 (6000 required) | 12 (6000 required) | 24 (7500 required) | 17 (8946 required) | 22 (9000 required) | 23 (9000 required) | 20 (10500 required) | 12 (11000 required) | 18 (11000 required) | 19 (11000 required) | 16 (14000 required) | 13 (15000 required) | 14 (15000 required) | 15 (15000 required) | 11 (18000 required) | 10 (19000 required) | 8 (20000 required) | 9 (20000 required) | 6 (22000 required) | 7 (22000 required) | 5 (22078 required) | 2 (22166 required) | 0 (25368 required) | 1 (26000 required) | 3 (26000 required) | 4 (26000 required)
Solution:
Offset plates:
  0|  1|  3|  4: 26000 printings
  2|  5|  6|  7: 22166 printings
  8|  9| 10| 11: 20000 printings
 13| 14| 15| 16: 15000 printings
 12| 18| 19| 20: 11000 printings
 17| 22| 23| 24: 9000 printings
 12| 21| 25| 26: 6000 printings
  0|  2| 17| 21: 4834 printings
  1| 18| 27| 28: 2000 printings
  5| 13| 27| 29: 1000 printings
Cost = 117000

[2019-04-09T21:29:22.146Z]
New better at iteration 44765:
Genotype:
GenomeCP
0.44829470350984774;0.3125098714154346;0.0406464680136118;0.07143113684488143;0.35945870364702787;0.9473960239792236;0.9272178493988459;0.8002610603278228;0.622404895623365;0.3846763595613286;0.009239964537001266;0.2930629235868902;0.5673515832441589;0.3622059384800188;0.18154933437312304;0.34786638222384775;0.08599069978604293;0.037053888886417065;0.2683237975720714;0.10003976492212673
(20 genes)
Phenotype:
Slots: 27 (500 required) | 10 (1000 required) | 29 (1000 required) | 2 (1000 required) | 28 (1500 required) | 1 (2000 required) | 27 (2000 required) | 8 (2000 required) | 17 (4889 required) | 26 (5000 required) | 13 (5000 required) | 18 (5000 required) | 25 (6000 required) | 24 (7500 required) | 18 (8000 required) | 5 (8000 required) | 17 (8611 required) | 0 (8791 required) | 22 (9000 required) | 23 (9000 required) | 21 (10000 required) | 20 (10500 required) | 13 (11000 required) | 19 (11000 required) | 16 (14000 required) | 5 (15000 required) | 14 (15000 required) | 15 (15000 required) | 12 (17000 required) | 8 (18000 required) | 11 (18000 required) | 10 (18000 required) | 9 (20000 required) | 0 (21209 required) | 6 (22000 required) | 7 (22000 required) | 1 (26000 required) | 2 (26000 required) | 3 (26000 required) | 4 (26000 required)
Solution:
Offset plates:
  1|  2|  3|  4: 26000 printings
  0|  6|  7|  9: 22000 printings
  8| 10| 11| 12: 18000 printings
  5| 14| 15| 16: 15000 printings
 13| 19| 20| 21: 11000 printings
  0| 17| 22| 23: 9000 printings
  5| 18| 24| 25: 8000 printings
 13| 17| 18| 26: 5000 printings
  1|  8| 27| 28: 2000 printings
  2| 10| 27| 29: 1000 printings
Cost = 117000

[2019-04-09T21:29:51.995Z]
New better at iteration 185903:
Genotype:
GenomeCP
0.3480995435079941;0.5791327444748239;0.6260201070765687;0.38490511126154026;0.8565889142745792;0.1668238032434758;0.24499315827629928;0.9025781300628115;0.16835098642428425;0.347710802868699;0.0;0.33333596691601375;0.5682482455895183;0.814873928428257;0.2112593764140749;0.7726170638040579;0.03557377031443912;0.9642864816422589;0.3897263402845109;0.9444538511724547
(20 genes)
Phenotype:
Slots: 1 (1000 required) | 11 (1000 required) | 29 (1000 required) | 25 (1000 required) | 28 (1500 required) | 7 (2144 required) | 17 (2500 required) | 27 (2500 required) | 25 (5000 required) | 26 (5000 required) | 6 (5003 required) | 18 (5003 required) | 24 (7500 required) | 10 (7997 required) | 18 (7997 required) | 5 (7997 required) | 22 (9000 required) | 23 (9000 required) | 21 (10000 required) | 0 (10000 required) | 20 (10500 required) | 19 (11000 required) | 17 (11000 required) | 10 (11003 required) | 16 (14000 required) | 14 (15000 required) | 15 (15000 required) | 5 (15003 required) | 13 (16000 required) | 6 (16997 required) | 12 (17000 required) | 11 (17000 required) | 7 (19856 required) | 0 (20000 required) | 8 (20000 required) | 9 (20000 required) | 3 (26000 required) | 4 (26000 required) | 2 (27000 required) | 1 (27000 required)
Solution:
Offset plates:
  1|  2|  3|  4: 27000 printings
  0|  7|  8|  9: 20000 printings
  6| 11| 12| 13: 17000 printings
  5| 14| 15| 16: 15003 printings
 10| 17| 19| 20: 11003 printings
  0| 21| 22| 23: 10000 printings
  5| 10| 18| 24: 7997 printings
  6| 18| 25| 26: 5003 printings
  7| 17| 27| 28: 2500 printings
  1| 11| 25| 29: 1000 printings
Cost = 116506
 */