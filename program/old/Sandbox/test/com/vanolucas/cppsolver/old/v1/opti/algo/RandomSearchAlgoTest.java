package com.vanolucas.cppsolver.old.v1.opti.algo;

import com.vanolucas.cppsolver.old.v1.cpp.eval.EvaluatorCP;
import com.vanolucas.cppsolver.old.v1.cpp.eval.EvaluatorSFP;
import com.vanolucas.cppsolver.old.v1.cpp.indiv.IndividualCP;
import com.vanolucas.cppsolver.old.v1.cpp.indiv.IndividualSFP;
import com.vanolucas.cppsolver.old.v1.cpp.problem.StandardCppInstance;
import com.vanolucas.cppsolver.old.v1.cpp.problem.StandardCppInstance30;
import com.vanolucas.cppsolver.old.v1.opti.evaluation.IndividualEvaluator;
import com.vanolucas.cppsolver.old.v1.opti.indiv.Individual;
import com.vanolucas.cppsolver.old.v1.opti.pause.PauseAfterIteration;
import com.vanolucas.cppsolver.old.v1.opti.pause.PauseQualityReached;
import com.vanolucas.cppsolver.old.v1.opti.quality.Cost;
import com.vanolucas.cppsolver.old.v1.opti.randomizer.IndividualRandomizer;
import com.vanolucas.cppsolver.old.v1.opti.randomizer.RandomizableGenotypeRandomizer;
import com.vanolucas.cppsolver.old.v1.sumproblem.SumGeneticIndividual;
import com.vanolucas.cppsolver.old.v1.sumproblem.SumGeneticIndividualEvaluator;
import org.junit.jupiter.api.Test;

class RandomSearchAlgoTest {

    @Test
    void runSumProblem() throws CloneNotSupportedException {
        final int target = 300;

        Individual individual = new SumGeneticIndividual();
        IndividualRandomizer randomizer = new RandomizableGenotypeRandomizer();
        IndividualEvaluator evaluator = new SumGeneticIndividualEvaluator(target);

        RandomSearchAlgo algo = new RandomSearchAlgo(individual, randomizer, evaluator);

        algo.addListener(new OptiAlgoListener() {
            @Override
            public void onNewBetter(long iteration, Individual newBetterIndiv) {
                System.out.println(String.format("\nNew better at iteration %d:\n%s", iteration, newBetterIndiv));
            }
        });

        algo.addPauseCriterion(new PauseAfterIteration(100));

        algo.run();
    }

    @Test
    void runStandardCpp() throws CloneNotSupportedException {
        StandardCppInstance problem = new StandardCppInstance30();

//        Individual individual = new IndividualSFP(problem);
        Individual individual = new IndividualCP(problem);
        IndividualRandomizer randomizer = new RandomizableGenotypeRandomizer();
//        IndividualEvaluator evaluator = new EvaluatorSFP(problem);
        IndividualEvaluator evaluator = new EvaluatorCP(problem);

        RandomSearchAlgo algo = new RandomSearchAlgo(individual, randomizer, evaluator);

        algo.addListener(new OptiAlgoListener() {
            @Override
            public void onNewBetter(long iteration, Individual newBetterIndiv) {
                System.out.println(String.format("\nNew better at iteration %d:\n%s", iteration, newBetterIndiv));
            }
        });

        algo.addPauseCriterion(new PauseQualityReached(new Cost(120500)));

        algo.run();
    }
}
