package com.vanolucas.cppsolver.old.v1.opti.quality;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CostTest {

    @Test
    void isBetterThan() {
        Cost best = new Cost(1);
        Cost worst = new Cost(9);
        assertTrue(best.isBetterThan(worst));
        assertFalse(worst.isBetterThan(best));
    }

    @Test
    void testEquals() {
        Cost a = new Cost(10);
        Cost b = new Cost(10);
        assertEquals(a, b);
        assertEquals(b, a);
    }

    @Test
    void compare() {
        Cost a = new Cost(1);
        Cost b = new Cost(3);
        Cost c = new Cost(2);

        List<Cost> sorted = Arrays.asList(a, b, c);
        // sort worst to best
        sorted.sort(Comparator.naturalOrder());

        for (int i = 1; i < sorted.size(); i++) {
            final Cost prev = sorted.get(i - 1);
            final Cost curr = sorted.get(i);
            assertTrue(curr.isBetterThan(prev));
        }
    }
}
