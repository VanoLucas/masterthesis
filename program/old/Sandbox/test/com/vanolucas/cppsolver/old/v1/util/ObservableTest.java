package com.vanolucas.cppsolver.old.v1.util;

import org.junit.jupiter.api.Test;

class ObservableTest {

    @Test
    void notifyListeners() {
        class Listener {
            public void onEvent() {
                System.out.println("event occured");
            }
        }

        class SpecifiedListener extends Listener {
            public void onEventWithParam(int param) {
                System.out.println("specific event: " + param);
            }
        }

        Observable<Listener> observable = new Observable<>();
        observable.addListener(new Listener());
        observable.addListener(new SpecifiedListener());
        observable.notifyListeners(Listener.class, Listener::onEvent);
        observable.notifyListeners(SpecifiedListener.class, listener -> listener.onEventWithParam(123));
    }
}