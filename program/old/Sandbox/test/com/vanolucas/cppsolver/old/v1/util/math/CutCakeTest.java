package com.vanolucas.cppsolver.old.v1.util.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CutCakeTest {

    @Test
    void getShares() {
        int[] shares = CutCake.getShares(10, new double[]{0.123d, 0.123d});
        assertEquals(2, shares.length);
        assertEquals(shares[0], 5);
        assertEquals(shares[1], 5);

        shares = CutCake.getShares(10, new double[]{0.5d, 0.5d});
        assertEquals(2, shares.length);
        assertEquals(shares[0], 5);
        assertEquals(shares[1], 5);

        shares = CutCake.getShares(12, new double[]{0.333d, 0.333d, 0.333d});
        assertEquals(3, shares.length);
        assertEquals(shares[0], 4);
        assertEquals(shares[1], 4);
        assertEquals(shares[2], 4);

        shares = CutCake.getShares(100, new double[]{0.1d, 0.2d, 0.1d});
        assertEquals(3, shares.length);
        assertEquals(shares[0], 25);
        assertEquals(shares[1], 50);
        assertEquals(shares[2], 25);
    }
}