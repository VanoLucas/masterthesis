package com.vanolucas.cppsolver.old.v10.clone;

import com.vanolucas.cppsolver.old.v10.genome.Genome;
import com.vanolucas.cppsolver.old.v10.random.Rand;
import com.vanolucas.cppsolver.old.v10.solution.GeneticSolution;
import com.vanolucas.cppsolver.old.v10.solution.Solution;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class GeneticSolutionClonerTest {

    @Test
    void cloneGenotype() {
        class MyGenome implements Genome {
            private int value;

            private MyGenome(int value) {
                this.value = value;
            }

            @Override
            public String toString() {
                return String.valueOf(value);
            }

            @Override
            public void randomize(Rand rand) {

            }

            @Override
            public MyGenome newClone() {
                return new MyGenome(value);
            }
        }

        final GeneticSolutionCloner solutionCloner = new GeneticSolutionCloner(true, null, null);

        final Genome genotype = new MyGenome(111);
        final Solution solution = new GeneticSolution(genotype);

        System.out.println(solution);

        final Solution clonedSolution = solution.cloneUsing(solutionCloner);

        System.out.println(clonedSolution);
    }

    @Test
    void cloneAll() {
        class MyGenome implements Genome {
            private int value;

            private MyGenome(int value) {
                this.value = value;
            }

            @Override
            public String toString() {
                return String.valueOf(value);
            }

            @Override
            public void randomize(Rand rand) {

            }

            @Override
            public MyGenome newClone() {
                return new MyGenome(value);
            }
        }

        class MyIntermediate {
            private int value;

            private MyIntermediate(int value) {
                this.value = value;
            }

            @Override
            public String toString() {
                return String.valueOf(value);
            }
        }

        class MyPhenotype {
            private int value;

            private MyPhenotype(int value) {
                this.value = value;
            }

            @Override
            public String toString() {
                return String.valueOf(value);
            }
        }

        final Cloner<MyIntermediate> intermediateCloner1 = myIntermediate -> new MyIntermediate(myIntermediate.value);
        final Cloner<MyIntermediate> intermediateCloner2 = myIntermediate -> new MyIntermediate(myIntermediate.value * 2);
        final List<Cloner> intermediateCloners = Arrays.asList(intermediateCloner1, intermediateCloner2);
        final Cloner<MyPhenotype> phenotypeCloner = phenotype -> new MyPhenotype(phenotype.value);

        final GeneticSolutionCloner solutionCloner = new GeneticSolutionCloner(true, intermediateCloners, phenotypeCloner);

        final Genome genotype = new MyGenome(111);
        final List<Object> intermediates = Arrays.asList(
                new MyIntermediate(222),
                new MyIntermediate(333)
        );
        final MyPhenotype phenotype = new MyPhenotype(444);
        final Solution solution = new GeneticSolution(genotype, intermediates, phenotype);

        System.out.println(solution);

        final Solution clonedSolution = solution.cloneUsing(solutionCloner);

        System.out.println(clonedSolution);
    }
}