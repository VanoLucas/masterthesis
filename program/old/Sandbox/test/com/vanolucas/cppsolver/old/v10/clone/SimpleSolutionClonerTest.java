package com.vanolucas.cppsolver.old.v10.clone;

import com.vanolucas.cppsolver.old.v10.solution.SimpleSolution;
import com.vanolucas.cppsolver.old.v10.solution.Solution;
import org.junit.jupiter.api.Test;

class SimpleSolutionClonerTest {

    @Test
    void clone1() {
        class MyActualSolution {
            private int value;

            private MyActualSolution(int value) {
                this.value = value;
            }

            @Override
            public String toString() {
                return String.valueOf(value);
            }
        }
        final Cloner<MyActualSolution> actualSolutionCloner = actualSolution -> new MyActualSolution(actualSolution.value);

        final SimpleSolutionCloner cloner = new SimpleSolutionCloner(actualSolutionCloner);

        final MyActualSolution actualSolution = new MyActualSolution(111);
        final Solution solution = new SimpleSolution(actualSolution);

        System.out.println(solution);

        final Solution clonedSolution = cloner.clone(solution);

        System.out.println(clonedSolution);
    }
}