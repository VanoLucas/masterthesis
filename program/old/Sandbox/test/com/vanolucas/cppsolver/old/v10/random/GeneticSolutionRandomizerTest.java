package com.vanolucas.cppsolver.old.v10.random;

import com.vanolucas.cppsolver.old.v10.genome.Genome;
import com.vanolucas.cppsolver.old.v10.solution.GeneticSolution;
import com.vanolucas.cppsolver.old.v10.solution.Solution;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class GeneticSolutionRandomizerTest {

    @Test
    void randomizeGenotypeOnly() {
        class MyGenotype implements Genome {
            private int value;

            private MyGenotype(int value) {
                this.value = value;
            }

            @Override
            public void randomize(Rand rand) {
                value = 222;
            }

            @Override
            public Genome newClone() {
                return null;
            }

            @Override
            public String toString() {
                return String.valueOf(value);
            }
        }

        final Rand rand = new Rand();

        final SolutionRandomizer solutionRandomizer = new GeneticSolutionRandomizer(rand);

        final Genome genotype = new MyGenotype(111);

        final Solution solution = new GeneticSolution(genotype);

        System.out.println(solution);
        solution.randomizeUsing(solutionRandomizer);
        System.out.println(solution);
    }

    @Test
    void randomizeAll() {
        class MyGenotype implements Genome {
            private int value;

            private MyGenotype(int value) {
                this.value = value;
            }

            @Override
            public void randomize(Rand rand) {
                value = 555;
            }

            @Override
            public Genome newClone() {
                return null;
            }

            @Override
            public String toString() {
                return String.valueOf(value);
            }
        }

        class MyIntermediate {
            private int value;

            private MyIntermediate(int value) {
                this.value = value;
            }

            @Override
            public String toString() {
                return String.valueOf(value);
            }
        }

        class MyPhenotype {
            private int value;

            private MyPhenotype(int value) {
                this.value = value;
            }

            @Override
            public String toString() {
                return String.valueOf(value);
            }
        }

        final Rand rand = new Rand();

        final Randomizer<MyIntermediate> intermediateRandomizer1 = intermediate -> intermediate.value = 666;
        final Randomizer<MyIntermediate> intermediateRandomizer2 = intermediate -> intermediate.value = 777;
        final List<Randomizer> intermediateRandomizers = Arrays.asList(
                intermediateRandomizer1,
                intermediateRandomizer2
        );

        final Randomizer<MyPhenotype> phenotypeRandomizer = phenotype -> phenotype.value = 888;

        final SolutionRandomizer solutionRandomizer = new GeneticSolutionRandomizer(rand);

        final Genome genotype = new MyGenotype(111);
        final List<Object> intermediates = Arrays.asList(
                new MyIntermediate(222),
                new MyIntermediate(333)
        );
        final Object phenotype = new MyPhenotype(444);

        final Solution solution = new GeneticSolution(genotype, intermediates, phenotype);

        System.out.println(solution);
        solution.randomizeUsing(solutionRandomizer);
        System.out.println(solution);
    }
}