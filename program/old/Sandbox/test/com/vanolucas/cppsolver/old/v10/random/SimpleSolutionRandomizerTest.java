package com.vanolucas.cppsolver.old.v10.random;

import com.vanolucas.cppsolver.old.v10.solution.SimpleSolution;
import com.vanolucas.cppsolver.old.v10.solution.Solution;
import org.junit.jupiter.api.Test;

class SimpleSolutionRandomizerTest {

    @Test
    void randomize() {
        class MyActualSolution {
            private int value;

            private MyActualSolution(int value) {
                this.value = value;
            }

            @Override
            public String toString() {
                return String.valueOf(value);
            }
        }

        final Randomizer<MyActualSolution> actualSolutionRandomizer = actualSolution -> actualSolution.value = 333;

        final SolutionRandomizer solutionRandomizer = new SimpleSolutionRandomizer(actualSolutionRandomizer);

        final MyActualSolution actualSolution = new MyActualSolution(111);
        final Solution solution = new SimpleSolution(actualSolution);

        System.out.println(solution);
        solution.randomizeUsing(solutionRandomizer);
        System.out.println(solution);
    }
}