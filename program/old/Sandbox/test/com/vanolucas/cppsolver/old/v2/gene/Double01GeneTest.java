package com.vanolucas.cppsolver.old.v2.gene;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class Double01GeneTest {

    @Test
    void validValues() {
        double[] validValues = new double[]{0d, Double.MIN_VALUE, Math.nextDown(1d)};

        for (double validValue : validValues) {
            Double01Gene gene = new Double01Gene(validValue);
            assertEquals(validValue, gene.getValue());

            gene = new Double01Gene(0.1d);
            gene.setValue(validValue);
            assertEquals(validValue, gene.getValue());
        }
    }

    @Test
    void invalidValues() {
        double[] invalidValues = new double[]{1d, -Double.MIN_VALUE, Double.MAX_VALUE};

        Double01Gene gene = new Double01Gene(0d);

        for (double invalidValue : invalidValues) {
            // constructor
            assertThrows(IllegalArgumentException.class, () ->
                    new Double01Gene(invalidValue)
            );
            // setter
            assertThrows(IllegalArgumentException.class, () ->
                    gene.setValue(invalidValue)
            );
        }
    }
}