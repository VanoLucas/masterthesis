package com.vanolucas.cppsolver.old.v4.cpp.hyperopti;

import com.vanolucas.cppsolver.old.v4.opti.algo.Algo;
import com.vanolucas.cppsolver.old.v4.opti.algo.AlgoListener;
import com.vanolucas.cppsolver.old.v4.opti.mutation.RandomizeRandomizableMutation;
import com.vanolucas.cppsolver.old.v4.opti.mutation.mutator.IndividualGenotypeMutator;
import com.vanolucas.cppsolver.old.v4.opti.mutation.mutator.Mutator;
import com.vanolucas.cppsolver.old.v4.opti.mutation.mutator.RepeatedMutator;
import com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector.AllKeysMutation;
import com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector.OneRandomKeyMutation;
import com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector.RandomConsecutiveKeysMutation;
import com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector.key.AddRandomValueKeyMutation;
import com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector.key.RandomizeKeyMutation;
import com.vanolucas.cppsolver.old.v4.opti.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v4.util.rand.Rand;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

class CppHyperparamsProblemV1Test {

    @Test
    void hyperparamsOpti() throws ExecutionException, InterruptedException {
        final int nbRunsPerEval = 7;
        final int maxEvalPerRun = 10_000;

        CppHyperparamsProblemV1 problem = new CppHyperparamsProblemV1(nbRunsPerEval, maxEvalPerRun);

        List<Mutator> mutators = new ArrayList<>();

        // full randomization of the genome
        mutators.addAll(Collections.nCopies(5,
                new IndividualGenotypeMutator(new RandomizeRandomizableMutation(new Rand()))
        ));
        // add rand value to all keys
        mutators.addAll(Collections.nCopies(5,
                new IndividualGenotypeMutator(new AllKeysMutation(new AddRandomValueKeyMutation(-0.01d, +0.01d, new Rand())))
        ));
        mutators.addAll(Collections.nCopies(5,
                new IndividualGenotypeMutator(new AllKeysMutation(new AddRandomValueKeyMutation(-0.001d, +0.001d, new Rand())))
        ));
        mutators.addAll(Collections.nCopies(5,
                new IndividualGenotypeMutator(new AllKeysMutation(new AddRandomValueKeyMutation(-0.0001d, +0.0001d, new Rand())))
        ));
        // randomize 1 random key
        mutators.addAll(Collections.nCopies(10,
                new IndividualGenotypeMutator(new OneRandomKeyMutation(new RandomizeKeyMutation(new Rand())))
        ));
        // add rand value to 1 random key
        mutators.addAll(Collections.nCopies(5,
                new IndividualGenotypeMutator(new OneRandomKeyMutation(new AddRandomValueKeyMutation(-0.01d, +0.01d, new Rand())))
        ));
        mutators.addAll(Collections.nCopies(5,
                new IndividualGenotypeMutator(new OneRandomKeyMutation(new AddRandomValueKeyMutation(-0.001d, +0.001d, new Rand())))
        ));
        mutators.addAll(Collections.nCopies(5,
                new IndividualGenotypeMutator(new OneRandomKeyMutation(new AddRandomValueKeyMutation(-0.0001d, +0.0001d, new Rand())))
        ));
        // randomize 2 random consecutive keys
        mutators.addAll(Collections.nCopies(10,
                new IndividualGenotypeMutator(new RandomConsecutiveKeysMutation(2, new RandomizeKeyMutation(new Rand()), new Rand()))
        ));
        // add rand value to 2 random consecutive keys
        mutators.addAll(Collections.nCopies(5,
                new IndividualGenotypeMutator(new RandomConsecutiveKeysMutation(2, new AddRandomValueKeyMutation(-0.01d, +0.01d, new Rand())))
        ));
        mutators.addAll(Collections.nCopies(5,
                new IndividualGenotypeMutator(new RandomConsecutiveKeysMutation(2, new AddRandomValueKeyMutation(-0.001d, +0.001d, new Rand())))
        ));
        mutators.addAll(Collections.nCopies(5,
                new IndividualGenotypeMutator(new RandomConsecutiveKeysMutation(2, new AddRandomValueKeyMutation(-0.0001d, +0.0001d, new Rand())))
        ));
        // add rand value to 2 random keys
        mutators.addAll(Collections.nCopies(5,
                new RepeatedMutator<>(2,
                        new IndividualGenotypeMutator(new OneRandomKeyMutation(new AddRandomValueKeyMutation(-0.01d, +0.01d, new Rand())))
                )
        ));
        mutators.addAll(Collections.nCopies(5,
                new RepeatedMutator<>(2,
                        new IndividualGenotypeMutator(new OneRandomKeyMutation(new AddRandomValueKeyMutation(-0.001d, +0.001d, new Rand())))
                )
        ));
        mutators.addAll(Collections.nCopies(5,
                new RepeatedMutator<>(2,
                        new IndividualGenotypeMutator(new OneRandomKeyMutation(new AddRandomValueKeyMutation(-0.0001d, +0.0001d, new Rand())))
                )
        ));

        problem
//                .newHillClimbingAlgo(mutators)
                .newHillClimbingAlgo(
                        problem.newHillClimbingStep(
                                problem.newSteepestAscentHillClimbingStrategy(mutators)
                        )
                )
                .withListener(new AlgoListener() {
                    private Algo algo;

                    @Override
                    public void onListeningTo(Algo algo) {
                        this.algo = algo;
                    }

                    @Override
                    public void onIterationStart(long iteration) {
                        System.out.println(String.format("Iteration %d...", iteration));
                    }

                    @Override
                    public void onBetterSolution(EvaluatedSolution betterSolution, long iteration) {
                        // log best solutions
                        System.out.println(String.format("\n[%s]\n" +
                                        "New better solution at iteration %d:\n" +
                                        "%s",
                                Instant.now(),
                                iteration,
                                betterSolution.getSolution()
                        ));
                    }
                })
                .run();
    }
}

/*

[2019-04-21T08:01:01.535Z]
New better solution at iteration 6:
Genotype (RandomKeyVector):
0.7191448386773307;0.029544629625129604;0.7340446224536127;0.13274571812466263;0.6094391855668169;0.47708853452240696;0.03549089398911687;0.03204215438810079;0.5193208289907097;0.4287580785095091;0.31163509234973197;0.23765681451194354;0.45232159761854873;0.5609835687815086;0.8150508408328628
(15 genes)
Phenotype (ArrayList):
[1.7191448386773307, 0.5908925925025921, 14.680892449072253, 2.6549143624932525, 12.188783711336338, 9.54177069044814, 0.7098178797823373, 0.6408430877620158, 10.386416579814195, 8.575161570190183, 6.232701846994639, 4.753136290238871, 9.046431952370975, 11.21967137563017, 16.301016816657256]
Solution (CppHyperparamsSolution):
Hyperparams: [1, 0, 14, 2, 12, 9, 0, 0, 10, 8, 6, 4, 9, 11, 16]
Best costs: [117508, 117528, 117883, 118000, 118007, 118838, 119102]
Avg best cost: 118123
Median best cost: 118000

*/