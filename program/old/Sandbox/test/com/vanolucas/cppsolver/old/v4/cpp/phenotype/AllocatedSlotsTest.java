package com.vanolucas.cppsolver.old.v4.cpp.phenotype;

import com.vanolucas.cppsolver.old.v4.cpp.instance.CppInstance30BookCovers;
import com.vanolucas.cppsolver.old.v4.cpp.problem.CppGeneticProblemCP;
import com.vanolucas.cppsolver.old.v4.opti.algo.Algo;
import com.vanolucas.cppsolver.old.v4.opti.algo.AlgoListener;
import com.vanolucas.cppsolver.old.v4.opti.mutation.RandomizeRandomizableMutation;
import com.vanolucas.cppsolver.old.v4.opti.mutation.mutator.IndividualGenotypeMutator;
import com.vanolucas.cppsolver.old.v4.opti.mutation.mutator.MultipleMutator;
import com.vanolucas.cppsolver.old.v4.opti.mutation.mutator.Mutator;
import com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector.OneRandomKeyMutation;
import com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector.key.RandomizeKeyMutation;
import com.vanolucas.cppsolver.old.v4.opti.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v4.util.rand.Rand;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

class AllocatedSlotsTest {

    @Test
    void perfTest() throws ExecutionException, InterruptedException {
        final List<Mutator> mutators = new ArrayList<>();
        // randomize whole genome
        mutators.addAll(Collections.nCopies(40,
                new IndividualGenotypeMutator(new RandomizeRandomizableMutation())
        ));
        // randomize 1 random key of the genome
        mutators.addAll(Collections.nCopies(500,
                new IndividualGenotypeMutator(
                        new OneRandomKeyMutation(new RandomizeKeyMutation(new Rand()))
                )
        ));
        // randomize 1 random key then 1 again
        mutators.addAll(Collections.nCopies(500,
                new MultipleMutator<>(Arrays.asList(
                        new IndividualGenotypeMutator(
                                new OneRandomKeyMutation(new RandomizeKeyMutation(new Rand()))
                        ),
                        new IndividualGenotypeMutator(
                                new OneRandomKeyMutation(new RandomizeKeyMutation(new Rand()))
                        )
                ))
        ));

        new CppGeneticProblemCP(new CppInstance30BookCovers())
                .newHillClimbingAlgo(mutators)
                .withListener(new AlgoListener() {
                    private Algo algo;

                    @Override
                    public void onListeningTo(Algo algo) {
                        this.algo = algo;
                    }

                    @Override
                    public void onBetterSolution(EvaluatedSolution betterSolution, long iteration) {
                        // log best solutions
                        System.out.println(String.format("\n[%s]\n" +
                                        "New better solution at iteration %d:\n" +
                                        "%s",
                                Instant.now(),
                                iteration,
                                betterSolution.getSolution()
                        ));
                    }
                })
                .run();
    }
}