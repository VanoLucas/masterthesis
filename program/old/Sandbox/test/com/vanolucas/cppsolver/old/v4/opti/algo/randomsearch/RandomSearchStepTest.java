package com.vanolucas.cppsolver.old.v4.opti.algo.randomsearch;

import com.vanolucas.cppsolver.old.v4.cpp.instance.CppInstance30BookCovers;
import com.vanolucas.cppsolver.old.v4.cpp.problem.CppGeneticProblemCP;
import com.vanolucas.cppsolver.old.v4.opti.algo.Algo;
import com.vanolucas.cppsolver.old.v4.opti.algo.AlgoListener;
import com.vanolucas.cppsolver.old.v4.opti.problem.SumProblem;
import com.vanolucas.cppsolver.old.v4.opti.quality.IntCost;
import com.vanolucas.cppsolver.old.v4.opti.solution.EvaluatedSolution;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutionException;

class RandomSearchStepTest {

    @Test
    void sampleSumProblem() throws ExecutionException, InterruptedException {
        new SumProblem(1000)
                .newRandomSearchAlgo()
                .withListener(new AlgoListener() {
                    private Algo algo;

                    @Override
                    public void onListeningTo(Algo algo) {
                        this.algo = algo;
                    }

                    @Override
                    public void onBetterSolution(EvaluatedSolution betterSolution, long iteration) {
                        // log best solutions
                        System.out.println(String.format("\nNew better solution at iteration %d:\n" +
                                        "%s\n" +
                                        "Cost = %s",
                                iteration,
                                betterSolution.getSolution(),
                                betterSolution.getQuality()
                        ));
                        // stop at optimal solution
                        if (((IntCost) betterSolution.getQuality()).getValue() == 0) {
                            algo.pauseBeforeNextIteration();
                        }
                    }
                })
                .run();
    }

    @Test
    void cpp30() throws ExecutionException, InterruptedException {
        new CppGeneticProblemCP(new CppInstance30BookCovers())
                .newRandomSearchAlgo()
                .withListener(new AlgoListener() {
                    private Algo algo;

                    @Override
                    public void onListeningTo(Algo algo) {
                        this.algo = algo;
                    }

                    @Override
                    public void onBetterSolution(EvaluatedSolution betterSolution, long iteration) {
                        // log best solutions
                        System.out.println(String.format("\nNew better solution at iteration %d:\n" +
                                        "%s",
                                iteration,
                                betterSolution.getSolution()
                        ));
                        // stop at target quality
                        if (((IntCost) betterSolution.getQuality()).getValue() <= 121000) {
                            algo.pauseBeforeNextIteration();
                        }
                    }
                })
                .run();
    }
}