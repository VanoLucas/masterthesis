package com.vanolucas.cppsolver.old.v4.opti.mutation.mutator;

import org.junit.jupiter.api.Test;

class MutatorTest {

    @Test
    void allRandomKeyVectorMutators() {
        /*
        Mutator
            IndividualGenotypeMutator
            RepeatedMutator
            MultipleMutator
        RandomKeyVector Mutation
            Vector mutation
                RandomizeRandomizableMutation
                AllKeysMutation
                OneRandomKeyMutation
            Single key mutation
                RandomizeKeyMutation
                AddValueKeyMutation
                AddRandomValueKeyMutation
         */

        /*
        Interesting mutations for the RandomKeyVector:
        - IndividualGenotypeMutator
            - RandomizeRandomizableMutation -> randomize the whole genome.
            - AllKeysMutation
                - AddValueKeyMutation(value) -> add a fixed value to all keys
                - AddRandomValueKeyMutation(max) -> add a random value within 0d..max to all keys.
            - OneRandomKeyMutation
                - RandomizeKeyMutation -> randomize one random key (chainable)
                - AddValueKeyMutation(value) -> add fixed value to one random key (chainable)
                - AddRandomValueKeyMutation(max) -> add a random value within 0d..max to one random key (chainable).
            - Single key mutation
                - RandomizeKeyMutation(keyIndex) -> randomize one fixed key
                - AddValueKeyMutation(keyIndex, value) -> add fixed value to one fixed key (chainable)
                - AddRandomValueKeyMutation(keyIndex, max) -> add a random value within 0d..max to one fixed key (chainable).
         */
    }
}