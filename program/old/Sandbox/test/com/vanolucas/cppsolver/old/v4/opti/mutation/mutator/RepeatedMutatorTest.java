package com.vanolucas.cppsolver.old.v4.opti.mutation.mutator;

import com.vanolucas.cppsolver.old.v4.opti.algo.Algo;
import com.vanolucas.cppsolver.old.v4.opti.algo.AlgoListener;
import com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector.OneRandomKeyMutation;
import com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector.key.RandomizeKeyMutation;
import com.vanolucas.cppsolver.old.v4.opti.problem.SumProblem;
import com.vanolucas.cppsolver.old.v4.opti.quality.IntCost;
import com.vanolucas.cppsolver.old.v4.opti.solution.EvaluatedSolution;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

class RepeatedMutatorTest {

    @Test
    void sumProblem() throws ExecutionException, InterruptedException {
        final List<Mutator> mutators = new ArrayList<>();

        mutators.add(new RepeatedMutator(2,
                new IndividualGenotypeMutator(new OneRandomKeyMutation(
                        new RandomizeKeyMutation()
                ))
        ));

        new SumProblem(1000)
                .newHillClimbingAlgo(mutators)
                .withListener(new AlgoListener() {
                    private Algo algo;

                    @Override
                    public void onListeningTo(Algo algo) {
                        this.algo = algo;
                    }

                    @Override
                    public void onBetterSolution(EvaluatedSolution betterSolution, long iteration) {
                        // log best solutions
                        System.out.println(String.format("\n[%s]\n" +
                                        "New better solution at iteration %d:\n" +
                                        "%s",
                                Instant.now(),
                                iteration,
                                betterSolution.getSolution()
                        ));
                        // stop at optimal solution
                        if (((IntCost) betterSolution.getQuality()).getValue() == 0) {
                            algo.pauseBeforeNextIteration();
                        }
                    }
                })
                .run();
    }

}