package com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector;

import com.vanolucas.cppsolver.old.v4.opti.algo.Algo;
import com.vanolucas.cppsolver.old.v4.opti.algo.AlgoListener;
import com.vanolucas.cppsolver.old.v4.opti.mutation.mutator.IndividualGenotypeMutator;
import com.vanolucas.cppsolver.old.v4.opti.mutation.mutator.Mutator;
import com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector.key.AddRandomValueKeyMutation;
import com.vanolucas.cppsolver.old.v4.opti.problem.SumProblem;
import com.vanolucas.cppsolver.old.v4.opti.quality.IntCost;
import com.vanolucas.cppsolver.old.v4.opti.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v4.util.rand.Rand;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

class AddRandomValueKeyMutationTest {

    @Test
    void sumProblem() throws ExecutionException, InterruptedException {
        final List<Mutator> mutators = new ArrayList<>();
        mutators.addAll(Collections.nCopies(2,
                new IndividualGenotypeMutator(new OneRandomKeyMutation(
                        new AddRandomValueKeyMutation(-0.1d, 0.1d, new Rand())
                ))
        ));

        new SumProblem(1000)
                .newHillClimbingAlgo(mutators)
                .withListener(new AlgoListener() {
                    private Algo algo;

                    @Override
                    public void onListeningTo(Algo algo) {
                        this.algo = algo;
                    }

                    @Override
                    public void onBetterSolution(EvaluatedSolution betterSolution, long iteration) {
                        // log best solutions
                        System.out.println(String.format("\n[%s]\n" +
                                        "New better solution at iteration %d:\n" +
                                        "%s",
                                Instant.now(),
                                iteration,
                                betterSolution.getSolution()
                        ));
                        // stop at optimal solution
                        if (((IntCost) betterSolution.getQuality()).getValue() == 0) {
                            algo.pauseBeforeNextIteration();
                        }
                    }
                })
                .run();
    }
}