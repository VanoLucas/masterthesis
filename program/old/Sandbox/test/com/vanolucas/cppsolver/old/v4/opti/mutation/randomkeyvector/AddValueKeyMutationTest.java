package com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector;

import com.vanolucas.cppsolver.old.v4.opti.genotype.RandomKeyVector;
import com.vanolucas.cppsolver.old.v4.opti.mutation.randomkeyvector.key.AddValueKeyMutation;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AddValueKeyMutationTest {

    @Test
    void addValueToKey() {
        RandomKeyVector genome = new RandomKeyVector(3, 0.0d);
        AddValueKeyMutation addValueKeyMutation = new AddValueKeyMutation(0.1d);

        assertEquals(0.0d, genome.getKey(1));
        addValueKeyMutation.mutateKey(1, genome);
        assertEquals(0.1d, genome.getKey(1));

        addValueKeyMutation = new AddValueKeyMutation(0.9d);
        addValueKeyMutation.mutateKey(1, genome);
        assertEquals(Math.nextDown(1d), genome.getKey(1));
    }
}