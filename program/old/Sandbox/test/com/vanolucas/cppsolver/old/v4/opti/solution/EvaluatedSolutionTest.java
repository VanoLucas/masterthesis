package com.vanolucas.cppsolver.old.v4.opti.solution;

import com.vanolucas.cppsolver.old.v4.opti.quality.IntCost;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

class EvaluatedSolutionTest {

    @Test
    void compareTo() {
        List<EvaluatedSolution> sorted = Arrays.asList(
                new EvaluatedSolution(null, new IntCost(2)),
                new EvaluatedSolution(null, new IntCost(3)),
                new EvaluatedSolution(null, new IntCost(1))
        );
        Collections.sort(sorted);
        System.out.println(sorted.stream()
                .map(s -> s.getQuality().toString())
                .collect(Collectors.joining("\n"))
        );
    }
}