package com.vanolucas.cppsolver.old.v8.algo;

import com.vanolucas.cppsolver.old.v8.eval.EvaluationFunction;
import com.vanolucas.cppsolver.old.v8.eval.Evaluator;
import com.vanolucas.cppsolver.old.v8.judge.Judge;
import com.vanolucas.cppsolver.old.v8.judge.SingleBestJudge;
import com.vanolucas.cppsolver.old.v8.quality.IntCost;
import com.vanolucas.cppsolver.old.v8.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v8.solution.Solution;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class AlgoTest {

    @Test
    void run() throws ExecutionException, InterruptedException {
        final Strategy strategy = new Strategy() {
            private boolean newIteration;
            private final List<Solution> solutions = IntStream.range(0, 3)
                    .mapToObj(i -> new Solution<Integer>(i) {
                        @Override
                        public String toString() {
                            return String.valueOf(i);
                        }
                    })
                    .collect(Collectors.toList());

            @Override
            void onNewIteration(long iteration) {
                super.onNewIteration(iteration);
                System.out.println(String.format("%nIteration %d", iteration));
                this.newIteration = true;
            }

            @Override
            List<Solution> nextSolutionsToEvaluate() {
                return newIteration ? solutions : null;
            }

            @Override
            List<EvaluatedSolution> nextProposedSolutions() {
                return newIteration ? this.lastEvaluatedSolutions : null;
            }

            @Override
            void onVerdicts(List<Judge.Verdict> verdicts) {
                this.newIteration = false;
            }
        };
        final EvaluationFunction<Integer> evalFn = solution -> {
            System.out.println(String.format("Evaluating %d", solution));
            try {
                TimeUnit.SECONDS.sleep(1L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(String.format("Evaluated %d", solution));
            return new IntCost(111);
        };
        final Supplier<Evaluator> evaluatorsSupplier = () -> new Evaluator<>(evalFn);
        final Judge judge = new SingleBestJudge();
        final int maxThreads = 2;

        final Algo algo = new Algo(
                strategy, evaluatorsSupplier, judge, maxThreads
        );

        algo.addListener(new AlgoListener() {
            @Override
            public void onNewIteration(long iteration) {
                if (iteration >= 2) {
                    algo.pauseBeforeNextIteration();
                }
            }
        });

        algo.run();
    }
}