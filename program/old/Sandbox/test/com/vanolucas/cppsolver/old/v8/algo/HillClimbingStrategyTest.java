package com.vanolucas.cppsolver.old.v8.algo;

import com.vanolucas.cppsolver.old.v8.eval.EvaluationFunction;
import com.vanolucas.cppsolver.old.v8.eval.Evaluator;
import com.vanolucas.cppsolver.old.v8.judge.Judge;
import com.vanolucas.cppsolver.old.v8.judge.SingleBestJudge;
import com.vanolucas.cppsolver.old.v8.mutation.Mutator;
import com.vanolucas.cppsolver.old.v8.quality.IntCost;
import com.vanolucas.cppsolver.old.v8.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v8.solution.Solution;
import com.vanolucas.cppsolver.old.v8.solution.SolutionCloner;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

class HillClimbingStrategyTest {

    @Test
    void targetNumberProblem() throws ExecutionException, InterruptedException {
        final int target = 10;

        class Phenotype extends AtomicInteger implements Cloneable {
            private Phenotype(int i) {
                super(i);
            }

            private void increment() {
                incrementAndGet();
            }

            private void decrement() {
                decrementAndGet();
            }

            @Override
            protected Phenotype clone() throws CloneNotSupportedException {
                return (Phenotype) super.clone();
            }
        }

        final Solution<Phenotype> initialSolution = new Solution<>(new Phenotype(0));
        final SolutionCloner<Phenotype> cloner = phenotype -> {
            try {
                return phenotype.clone();
            } catch (CloneNotSupportedException e) {
                throw new UnsupportedOperationException("Clone operation not supported for this Phenotype.");
            }
        };
        final List<Mutator<Phenotype>> mutators = Arrays.asList(
                new Mutator<>(Phenotype::increment),
                new Mutator<>(Phenotype::decrement)
        );

        final Strategy strategy = new HillClimbingStrategy<>(initialSolution, cloner, mutators);

        final EvaluationFunction<AtomicInteger> evalFn = solution -> {
            final int distance = Math.abs(target - solution.get());
            return new IntCost(distance);
        };

        final Supplier<Evaluator> evaluatorsSupplier = () -> new Evaluator<>(evalFn);

        final Judge judge = new SingleBestJudge();
        final int maxThreads = 2;

        final Algo algo = new Algo(
                strategy, evaluatorsSupplier, judge, maxThreads
        );

        algo.addListener(new AlgoListener() {
            @Override
            public void onEvaluated(EvaluatedSolution evaluatedSolution, long iteration) {
                System.out.println("Evaluated: " + evaluatedSolution.toString());
            }

            @Override
            public void onBetterSolutionFound(EvaluatedSolution solution, long iteration) {
                System.out.println(String.format(
                        "Better solution at iteration %d: %s",
                        iteration,
                        solution
                ));
                if (solution.getQuality().isEquivalentTo(new IntCost(0))) {
                    algo.pauseBeforeNextIteration();
                }
            }
        });

        algo.run();
    }
}