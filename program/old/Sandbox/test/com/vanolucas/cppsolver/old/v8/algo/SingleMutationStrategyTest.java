package com.vanolucas.cppsolver.old.v8.algo;

import com.vanolucas.cppsolver.old.v8.eval.*;
import com.vanolucas.cppsolver.old.v8.genome.RandomKeyVector;
import com.vanolucas.cppsolver.old.v8.judge.Judge;
import com.vanolucas.cppsolver.old.v8.judge.SingleBestJudge;
import com.vanolucas.cppsolver.old.v8.mutation.GeneticMutator;
import com.vanolucas.cppsolver.old.v8.mutation.Mutation;
import com.vanolucas.cppsolver.old.v8.mutation.Mutator;
import com.vanolucas.cppsolver.old.v8.quality.IntCost;
import com.vanolucas.cppsolver.old.v8.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v8.solution.GeneticSolution;
import com.vanolucas.cppsolver.old.v8.solution.GeneticSolution1Intermediate;
import com.vanolucas.cppsolver.old.v8.solution.Solution;
import com.vanolucas.cppsolver.old.v8.util.Converter;
import com.vanolucas.cppsolver.old.v8.util.rand.Rand;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;
import java.util.stream.Collectors;

class SingleMutationStrategyTest {

    @Test
    void targetNumberProblem() throws ExecutionException, InterruptedException {
        final int target = 10;

        final Solution<AtomicInteger> initialSolution = new Solution<>(
                new AtomicInteger(0)
        );

        final Mutation<AtomicInteger> mutation = AtomicInteger::incrementAndGet;
        final Mutator<AtomicInteger> mutator = new Mutator<>(mutation);

        final Strategy strategy = new SingleMutationStrategy<>(initialSolution, mutator);

        final EvaluationFunction<AtomicInteger> evalFn = solution -> {
            final int distance = Math.abs(target - solution.get());
            return new IntCost(distance);
        };

        final Supplier<Evaluator> evaluatorsSupplier = () -> new Evaluator<>(evalFn);
        final Judge judge = new SingleBestJudge();
        final int maxThreads = 2;

        final Algo algo = new Algo(
                strategy, evaluatorsSupplier, judge, maxThreads
        );

        algo.addListener(new AlgoListener() {
            @Override
            public void onBetterSolutionFound(EvaluatedSolution solution, long iteration) {
                System.out.println(String.format(
                        "Iteration %d\tBetter solution: %d",
                        iteration,
                        ((AtomicInteger) solution.getSolution().getPhenotype()).get()
                ));
                if (solution.getQuality().isEquivalentTo(new IntCost(0))) {
                    algo.pauseBeforeNextIteration();
                }
            }
        });

        algo.run();
    }

    @Test
    void targetNumberGeneticProblem() throws ExecutionException, InterruptedException {
        class Genome extends RandomKeyVector {
            private Genome(int length) {
                super(length);
            }
        }
        class Phenotype extends AtomicInteger {
            private Phenotype(int i) {
                super(i);
            }
        }

        final int target = 100;

        final Genome initialGenome = new Genome(2);
        final GeneticSolution<Genome, Phenotype> initialSolution = new GeneticSolution<>(initialGenome);

        final Mutation<Genome> mutation = new Mutation<Genome>() {
            private final Rand rand = new Rand();

            @Override
            public void mutate(Genome genome) {
                genome.randomize(rand);
            }
        };
        final GeneticMutator<Genome, Phenotype> mutator = new GeneticMutator<>(mutation);

        final Strategy strategy = new SingleMutationStrategy<>(initialSolution, mutator);

        final GenomeDecoder<Genome, Phenotype> genomeDecoder = genome -> new Phenotype(
                genome.doubleStream()
                        .mapToInt(k -> (int) (k * 100.0))
                        .sum()
        );
        final EvaluationFunction<Phenotype> phenotypeEvalFn = phenotype -> {
            final int distance = Math.abs(phenotype.get() - target);
            return new IntCost(distance);
        };
        final Supplier<Evaluator> evaluatorsSupplier = () -> new GeneticEvaluator<Genome, Phenotype>(genomeDecoder, phenotypeEvalFn);

        final Judge judge = new SingleBestJudge();
        final int maxThreads = 2;

        final Algo algo = new Algo(
                strategy, evaluatorsSupplier, judge, maxThreads
        );

        algo.addListener(new AlgoListener() {
            @Override
            public void onBetterSolutionFound(EvaluatedSolution solution, long iteration) {
                System.out.println(String.format(
                        "Iteration %d\tBetter solution: %s",
                        iteration,
                        solution
                ));
                if (solution.getQuality().isEquivalentTo(new IntCost(0))) {
                    algo.pauseBeforeNextIteration();
                }
            }
        });

        algo.run();
    }

    @Test
    void targetNumberGenetic1IntermediateProblem() throws ExecutionException, InterruptedException {
        class Genome extends RandomKeyVector {
            private Genome(int length) {
                super(length);
            }
        }
        class Intermediate {
            private List<Integer> values;

            private Intermediate(List<Integer> values) {
                this.values = values;
            }

            private int sum() {
                return values.stream().mapToInt(__ -> __).sum();
            }

            @Override
            public String toString() {
                return values.toString();
            }
        }
        class Phenotype extends AtomicInteger {
            private Phenotype(int i) {
                super(i);
            }
        }

        final int target = 100;

        final Genome initialGenome = new Genome(2);
        final GeneticSolution1Intermediate<Genome, Intermediate, Phenotype> initialSolution = new GeneticSolution1Intermediate<>(initialGenome);

        final Mutation<Genome> mutation = new Mutation<Genome>() {
            private final Rand rand = new Rand();

            @Override
            public void mutate(Genome genome) {
                genome.randomize(rand);
            }
        };
        final GeneticMutator<Genome, Phenotype> mutator = new GeneticMutator<>(mutation);

        final Strategy strategy = new SingleMutationStrategy<>(initialSolution, mutator);

        final GenomeDecoder<Genome, Intermediate> genomeDecoder = genome -> new Intermediate(
                genome.doubleStream()
                        .mapToObj(k -> (int) (k * 100.0))
                        .collect(Collectors.toList())
        );
        final Converter<Intermediate, Phenotype> intermediateDecoder = intermediate -> new Phenotype(intermediate.sum());
        final EvaluationFunction<Phenotype> phenotypeEvalFn = phenotype -> {
            final int distance = Math.abs(phenotype.get() - target);
            return new IntCost(distance);
        };
        final Supplier<Evaluator> evaluatorsSupplier = () -> new GeneticEvaluator1Intermediate<Genome, Intermediate, Phenotype>(
                genomeDecoder,
                intermediateDecoder,
                phenotypeEvalFn
        );

        final Judge judge = new SingleBestJudge();
        final int maxThreads = 2;

        final Algo algo = new Algo(
                strategy, evaluatorsSupplier, judge, maxThreads
        );

        algo.addListener(new AlgoListener() {
            @Override
            public void onBetterSolutionFound(EvaluatedSolution solution, long iteration) {
                System.out.println(String.format(
                        "Iteration %d\tBetter solution: %s",
                        iteration,
                        solution
                ));
                if (solution.getQuality().isEquivalentTo(new IntCost(0))) {
                    algo.pauseBeforeNextIteration();
                }
            }
        });

        algo.run();
    }
}