package com.vanolucas.cppsolver.old.v9.algo;

import com.vanolucas.cppsolver.old.v9.eval.EvaluationFunction;
import com.vanolucas.cppsolver.old.v9.eval.Evaluator;
import com.vanolucas.cppsolver.old.v9.mutation.Mutation;
import com.vanolucas.cppsolver.old.v9.mutation.Mutator;
import com.vanolucas.cppsolver.old.v9.mutation.SolutionMutator;
import com.vanolucas.cppsolver.old.v9.quality.IntCost;
import com.vanolucas.cppsolver.old.v9.solution.Solution;
import com.vanolucas.cppsolver.old.v9.solution.SolutionCloner;
import com.vanolucas.cppsolver.old.v9.util.Cloner;
import com.vanolucas.cppsolver.old.v9.util.Pool;
import com.vanolucas.cppsolver.old.v9.util.Rand;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

class AlgoTest {

    @Test
    void parallelEvaluation() throws ExecutionException, InterruptedException {
        class Phenotype {
            private int value;

            private Phenotype(int value) {
                this.value = value;
            }

            private void setValue(int value) {
                this.value = value;
            }

            @Override
            public String toString() {
                return String.valueOf(value);
            }
        }

        final Solution initialSolution = new Solution(new Phenotype(0));

        final Cloner<Phenotype> phenotypeCloner = phenotype -> new Phenotype(phenotype.value);
        final SolutionCloner cloner = new SolutionCloner(phenotypeCloner);

        final Rand rand = new Rand();
        final Mutation<Phenotype> phenotypeMutation = phenotype -> phenotype.setValue(rand.int0To(100));
        final Mutator mutator = new SolutionMutator(phenotypeMutation);
        final List<Mutator> mutators = Collections.nCopies(3, mutator);

        final AlgoStep step = new SteepestAscentHillClimbingAlgoStep(initialSolution, cloner, mutators);

        final EvaluationFunction<Phenotype, IntCost> evalFn = phenotype -> {
            System.out.println(Instant.now() + " Evaluation " + phenotype + " [start]");
            try {
                TimeUnit.SECONDS.sleep(1L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Instant.now() + " Evaluation " + phenotype + " [end]");
            return new IntCost(phenotype.value);
        };
        final Supplier<Evaluator> evaluatorFactory = () -> new Evaluator(evalFn);
        final Pool<Evaluator> evaluators = new Pool<>(evaluatorFactory);

        final Algo algo = new Algo(step, evaluators, 2);

        algo.addListener(new AlgoListener() {
            @Override
            public void onNewIteration(long iteration) {
                System.out.println("Iteration " + iteration);
                if (iteration >= 3) {
                    algo.pauseBeforeNextIteration();
                }
            }
        });

        algo.run();
    }
}