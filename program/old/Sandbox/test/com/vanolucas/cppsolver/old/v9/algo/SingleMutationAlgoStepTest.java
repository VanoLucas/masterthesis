package com.vanolucas.cppsolver.old.v9.algo;

import com.vanolucas.cppsolver.old.v9.eval.EvaluationFunction;
import com.vanolucas.cppsolver.old.v9.eval.Evaluator;
import com.vanolucas.cppsolver.old.v9.eval.GeneticEvaluator;
import com.vanolucas.cppsolver.old.v9.genome.RandomKeyVector;
import com.vanolucas.cppsolver.old.v9.mutation.GeneticSolutionMutator;
import com.vanolucas.cppsolver.old.v9.mutation.Mutation;
import com.vanolucas.cppsolver.old.v9.mutation.Mutator;
import com.vanolucas.cppsolver.old.v9.mutation.SolutionMutator;
import com.vanolucas.cppsolver.old.v9.quality.IntCost;
import com.vanolucas.cppsolver.old.v9.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v9.solution.GeneticSolution;
import com.vanolucas.cppsolver.old.v9.solution.Solution;
import com.vanolucas.cppsolver.old.v9.util.Converter;
import com.vanolucas.cppsolver.old.v9.util.Pool;
import com.vanolucas.cppsolver.old.v9.util.Rand;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;
import java.util.stream.Collectors;

class SingleMutationAlgoStepTest {

    @Test
    void runSimpleTargetNumberProblem() throws ExecutionException, InterruptedException {
        final int target = 10;

        class Phenotype {
            private int value;

            private Phenotype(int value) {
                this.value = value;
            }

            private void increment() {
                value++;
            }

            @Override
            public String toString() {
                return String.valueOf(value);
            }
        }

        class Distance extends IntCost {
            private Distance(Integer value) {
                super(value);
            }
        }

        final Solution initialSolution = new Solution(new Phenotype(0));

        final Mutation<Phenotype> mutation = Phenotype::increment;
        final Mutator mutator = new SolutionMutator(mutation);

        final AlgoStep step = new SingleMutationAlgoStep(initialSolution, mutator);

        final EvaluationFunction<Phenotype, Distance> evalFn = phenotype -> new Distance(
                Math.abs(target - phenotype.value)
        );
        final Supplier<Evaluator> evaluatorSupplier = () -> new Evaluator(evalFn);
        final Pool<Evaluator> evaluators = new Pool<>(evaluatorSupplier);

        final int maxThreads = 2;

        final Algo algo = new Algo(
                step, evaluators, maxThreads
        );

        algo.addListener(new AlgoListener() {
            @Override
            public void onBetterSolution(EvaluatedSolution solution, long iteration) {
                System.out.println(String.format("Iteration %d, better solution: %s",
                        iteration,
                        solution
                ));
                // reached target
                if (solution.isEquivalentTo(new Distance(0))) {
                    algo.pauseBeforeNextIteration();
                }
            }
        });

        algo.run();
    }

    @Test
    void runGeneticTargetNumberProblem() throws ExecutionException, InterruptedException {
        final int target = 10;

        class Phenotype {
            private int value;

            private Phenotype(int value) {
                this.value = value;
            }

            @Override
            public String toString() {
                return String.valueOf(value);
            }
        }

        class Genotype extends RandomKeyVector {
            private Genotype() {
                super(2);
            }
        }

        class Distance extends IntCost {
            private Distance(Integer value) {
                super(value);
            }
        }

        final Solution initialSolution = new GeneticSolution(new Genotype());

        final Rand rand = new Rand();
        final Mutation<Genotype> mutation = genotype -> genotype.randomize(rand);
        final Mutator mutator = new GeneticSolutionMutator(mutation);

        final AlgoStep step = new SingleMutationAlgoStep(initialSolution, mutator);

        final Converter<Genotype, Phenotype> genomeDecoder = genotype -> new Phenotype(
                genotype.doubleStream()
                        .mapToInt(k -> (int) (k * 100))
                        .sum()
        );
        final EvaluationFunction<Phenotype, Distance> phenotypeEvalFn = phenotype -> new Distance(
                Math.abs(target - phenotype.value)
        );
        final Supplier<Evaluator> evaluatorSupplier = () -> new GeneticEvaluator(genomeDecoder, phenotypeEvalFn);
        final Pool<Evaluator> evaluators = new Pool<>(evaluatorSupplier);

        final int maxThreads = 2;

        final Algo algo = new Algo(
                step, evaluators, maxThreads
        );

        algo.addListener(new AlgoListener() {
            @Override
            public void onBetterSolution(EvaluatedSolution solution, long iteration) {
                System.out.println(String.format("Iteration %d, better solution: %s",
                        iteration,
                        solution
                ));
                // reached target
                if (solution.isEquivalentTo(new Distance(0))) {
                    algo.pauseBeforeNextIteration();
                }
            }
        });

        algo.run();
    }

    @Test
    void runGenetic1IntermediateTargetNumberProblem() throws ExecutionException, InterruptedException {
        final int target = 10;

        class Phenotype {
            private int value;

            private Phenotype(int value) {
                this.value = value;
            }

            @Override
            public String toString() {
                return String.valueOf(value);
            }
        }

        class Intermediate {
            private List<Integer> values;

            private Intermediate(List<Integer> values) {
                this.values = values;
            }

            private int sum() {
                return values.stream()
                        .mapToInt(__ -> __)
                        .sum();
            }

            @Override
            public String toString() {
                return values.toString();
            }
        }

        class Genotype extends RandomKeyVector {
            private Genotype() {
                super(2);
            }
        }

        class Distance extends IntCost {
            private Distance(Integer value) {
                super(value);
            }
        }

        final Solution initialSolution = new GeneticSolution(new Genotype(), 1);

        final Rand rand = new Rand();
        final Mutation<Genotype> mutation = genotype -> genotype.randomize(rand);
        final Mutator mutator = new GeneticSolutionMutator(mutation);

        final AlgoStep step = new SingleMutationAlgoStep(initialSolution, mutator);

        final Converter<Genotype, Intermediate> genotypeDecoder = genotype -> new Intermediate(
                genotype.doubleStream()
                        .mapToObj(k -> (int) (k * 100))
                        .collect(Collectors.toList())
        );
        final Converter<Intermediate, Phenotype> intermediateConverter = intermediate -> new Phenotype(
                intermediate.sum()
        );
        final EvaluationFunction<Phenotype, Distance> phenotypeEvalFn = phenotype -> new Distance(
                Math.abs(target - phenotype.value)
        );
        final Supplier<Evaluator> evaluatorSupplier = () -> new GeneticEvaluator(genotypeDecoder, intermediateConverter, phenotypeEvalFn);
        final Pool<Evaluator> evaluators = new Pool<>(evaluatorSupplier);

        final int maxThreads = 2;

        final Algo algo = new Algo(
                step, evaluators, maxThreads
        );

        algo.addListener(new AlgoListener() {
            @Override
            public void onBetterSolution(EvaluatedSolution solution, long iteration) {
                System.out.println(String.format("Iteration %d, better solution: %s",
                        iteration,
                        solution
                ));
                // reached target
                if (solution.isEquivalentTo(new Distance(0))) {
                    algo.pauseBeforeNextIteration();
                }
            }
        });

        algo.run();
    }

    @Test
    void runGenetic2IntermediatesTargetNumberProblem() throws ExecutionException, InterruptedException {
        final int target = 10;

        class Phenotype {
            private int value;

            private Phenotype(int value) {
                this.value = value;
            }

            @Override
            public String toString() {
                return String.valueOf(value);
            }
        }

        class Intermediate1 {
            private List<Integer> values;

            private Intermediate1(List<Integer> values) {
                this.values = values;
            }

            private int sum() {
                return values.stream()
                        .mapToInt(__ -> __)
                        .sum();
            }

            @Override
            public String toString() {
                return values.toString();
            }
        }

        class Intermediate2 {
            private int sum;

            private Intermediate2(int sum) {
                this.sum = sum;
            }

            @Override
            public String toString() {
                return String.format("Sum: %d", sum);
            }
        }

        class Genotype extends RandomKeyVector {
            private Genotype() {
                super(2);
            }
        }

        class Distance extends IntCost {
            private Distance(Integer value) {
                super(value);
            }
        }

        final Solution initialSolution = new GeneticSolution(new Genotype(), 2);

        final Rand rand = new Rand();
        final Mutation<Genotype> mutation = genotype -> genotype.randomize(rand);
        final Mutator mutator = new GeneticSolutionMutator(mutation);

        final AlgoStep step = new SingleMutationAlgoStep(initialSolution, mutator);

        final Converter<Genotype, Intermediate1> genotypeDecoder = genotype -> new Intermediate1(
                genotype.doubleStream()
                        .mapToObj(k -> (int) (k * 100))
                        .collect(Collectors.toList())
        );
        final Converter<Intermediate1, Intermediate2> intermediateConverter1 = intermediate1 -> new Intermediate2(
                intermediate1.sum()
        );
        final Converter<Intermediate2, Phenotype> intermediateConverter2 = intermediate2 -> new Phenotype(
                intermediate2.sum
        );
        final List<Converter> intermediateConverters = Arrays.asList(intermediateConverter1, intermediateConverter2);
        final EvaluationFunction<Phenotype, Distance> phenotypeEvalFn = phenotype -> new Distance(
                Math.abs(target - phenotype.value)
        );
        final Supplier<Evaluator> evaluatorSupplier = () -> new GeneticEvaluator(genotypeDecoder, intermediateConverters, phenotypeEvalFn);
        final Pool<Evaluator> evaluators = new Pool<>(evaluatorSupplier);

        final int maxThreads = 2;

        final Algo algo = new Algo(
                step, evaluators, maxThreads
        );

        algo.addListener(new AlgoListener() {
            @Override
            public void onBetterSolution(EvaluatedSolution solution, long iteration) {
                System.out.println(String.format("Iteration %d, better solution: %s",
                        iteration,
                        solution
                ));
                // reached target
                if (solution.isEquivalentTo(new Distance(0))) {
                    algo.pauseBeforeNextIteration();
                }
            }
        });

        algo.run();
    }
}