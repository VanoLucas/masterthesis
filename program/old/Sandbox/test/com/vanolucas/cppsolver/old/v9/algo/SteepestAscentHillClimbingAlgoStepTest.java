package com.vanolucas.cppsolver.old.v9.algo;

import com.vanolucas.cppsolver.old.v9.cpp.eval.*;
import com.vanolucas.cppsolver.old.v9.cpp.genome.GenomeCP;
import com.vanolucas.cppsolver.old.v9.cpp.instance.CppInstance;
import com.vanolucas.cppsolver.old.v9.cpp.instance.CppInstance30;
import com.vanolucas.cppsolver.old.v9.cpp.solution.CppSolution;
import com.vanolucas.cppsolver.old.v9.eval.EvaluationFunction;
import com.vanolucas.cppsolver.old.v9.eval.Evaluator;
import com.vanolucas.cppsolver.old.v9.eval.GeneticEvaluator;
import com.vanolucas.cppsolver.old.v9.genome.RandomKeyVector;
import com.vanolucas.cppsolver.old.v9.mutation.*;
import com.vanolucas.cppsolver.old.v9.mutation.rkv.ConsecutiveKeysRKVMutation;
import com.vanolucas.cppsolver.old.v9.mutation.rkv.ProbaRKVMutation;
import com.vanolucas.cppsolver.old.v9.mutation.rkv.key.AddRandValueKeyMutation;
import com.vanolucas.cppsolver.old.v9.mutation.rkv.key.KeyMutation;
import com.vanolucas.cppsolver.old.v9.mutation.rkv.key.RandomizeKeyMutation;
import com.vanolucas.cppsolver.old.v9.quality.IntCost;
import com.vanolucas.cppsolver.old.v9.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.old.v9.solution.GeneticSolution;
import com.vanolucas.cppsolver.old.v9.solution.GeneticSolutionCloner;
import com.vanolucas.cppsolver.old.v9.solution.Solution;
import com.vanolucas.cppsolver.old.v9.util.Cloner;
import com.vanolucas.cppsolver.old.v9.util.Converter;
import com.vanolucas.cppsolver.old.v9.util.Pool;
import com.vanolucas.cppsolver.old.v9.util.Rand;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;
import java.util.stream.Collectors;

class SteepestAscentHillClimbingAlgoStepTest {

    @Test
    void runGenetic2IntermediatesTargetNumberProblem() throws ExecutionException, InterruptedException {
        final int target = 10;

        class Phenotype {
            private int value;

            private Phenotype(int value) {
                this.value = value;
            }

            @Override
            public String toString() {
                return String.valueOf(value);
            }
        }

        class Intermediate1 {
            private List<Integer> values;

            private Intermediate1(List<Integer> values) {
                this.values = values;
            }

            private int sum() {
                return values.stream()
                        .mapToInt(__ -> __)
                        .sum();
            }

            @Override
            public String toString() {
                return values.toString();
            }
        }

        class Intermediate2 {
            private int sum;

            private Intermediate2(int sum) {
                this.sum = sum;
            }

            @Override
            public String toString() {
                return String.format("Sum: %d", sum);
            }
        }

        class Genotype extends RandomKeyVector {
            private Genotype() {
                super(2);
            }

            @Override
            public Genotype clone() throws CloneNotSupportedException {
                return (Genotype) super.clone();
            }
        }

        class Distance extends IntCost {
            private Distance(Integer value) {
                super(value);
            }
        }

        final Solution initialSolution = new GeneticSolution(new Genotype(), 2);

        final Cloner<Genotype> genotypeCloner = genotype -> {
            try {
                return genotype.clone();
            } catch (CloneNotSupportedException e) {
                throw new UnsupportedOperationException("Failed to clone genotype.");
            }
        };
        final GeneticSolutionCloner cloner = new GeneticSolutionCloner(genotypeCloner);

        final Rand rand = new Rand();
        final Mutation<Genotype> mutation = genotype -> genotype.randomize(rand);
        final SolutionMutator mutator = new GeneticSolutionMutator(mutation);
        final List<Mutator> mutators = Arrays.asList(mutator, mutator);

        final AlgoStep step = new SteepestAscentHillClimbingAlgoStep(initialSolution, cloner, mutators);

        final Converter<Genotype, Intermediate1> genotypeDecoder = genotype -> new Intermediate1(
                genotype.doubleStream()
                        .mapToObj(k -> (int) (k * 100))
                        .collect(Collectors.toList())
        );
        final Converter<Intermediate1, Intermediate2> intermediateConverter1 = intermediate1 -> new Intermediate2(
                intermediate1.sum()
        );
        final Converter<Intermediate2, Phenotype> intermediateConverter2 = intermediate2 -> new Phenotype(
                intermediate2.sum
        );
        final List<Converter> intermediateConverters = Arrays.asList(intermediateConverter1, intermediateConverter2);
        final EvaluationFunction<Phenotype, Distance> phenotypeEvalFn = phenotype -> new Distance(
                Math.abs(target - phenotype.value)
        );
        final Supplier<Evaluator> evaluatorSupplier = () -> new GeneticEvaluator(genotypeDecoder, intermediateConverters, phenotypeEvalFn);
        final Pool<Evaluator> evaluators = new Pool<>(evaluatorSupplier);

        final int maxThreads = 2;

        final Algo algo = new Algo(
                step, evaluators, maxThreads
        );

        algo.addListener(new AlgoListener() {
//            @Override
//            public void onEvaluated(EvaluatedSolution solution, long iteration) {
//                System.out.println(String.format("%nEvaluated at iteration %d: %s",
//                        iteration,
//                        solution
//                ));
//            }

            @Override
            public void onBetterSolution(EvaluatedSolution solution, long iteration) {
                System.out.println(String.format("%nIteration %d, better solution: %s",
                        iteration,
                        solution
                ));
                // reached target
                if (solution.isEquivalentTo(new Distance(0))) {
                    algo.pauseBeforeNextIteration();
                }
            }
        });

        algo.run();
    }

    @Test
    void runCppDraft() throws ExecutionException, InterruptedException {
        final CppInstance cppInstance = new CppInstance30();

        final Solution initialSolution = new GeneticSolution(new GenomeCP(cppInstance), 1);

        final Cloner<GenomeCP> genotypeCloner = genotype -> {
            try {
                return genotype.clone();
            } catch (CloneNotSupportedException e) {
                throw new UnsupportedOperationException(String.format("Failed to clone %s.", genotype.getClass().getSimpleName()));
            }
        };
        final GeneticSolutionCloner cloner = new GeneticSolutionCloner(genotypeCloner);

        final List<Mutator> mutators = getCppMutatorsTest1(cppInstance);
//        final List<Mutator> mutators = getCppMutatorsTest2(cppInstance);

        final AlgoStep step = new SteepestAscentHillClimbingAlgoStep(initialSolution, cloner, mutators);

        final Converter genotypeDecoder = new GenomeCPDecoder(cppInstance);
        final Converter intermediateConverter = new Slots2CppSolution(cppInstance);
        final EvaluationFunction<CppSolution, IntCost> phenotypeEvalFn = phenotype -> new IntCost(phenotype.getTotalPrintings());

        final Supplier<Evaluator> evaluatorSupplier = () -> new GeneticEvaluator(genotypeDecoder, intermediateConverter, phenotypeEvalFn);
        final Pool<Evaluator> evaluators = new Pool<>(evaluatorSupplier);

        final int maxThreads = 1;
//        final int maxThreads = 2;
//        final int maxThreads = 8;

        final Algo algo = new Algo(
                step, evaluators, maxThreads
        );

        algo.addListener(new AlgoListener() {
            private final List<Integer> lastCosts = new ArrayList<>();

            @Override
            public void onNewIteration(long iteration) {
                if (iteration % 1000 == 0) {
                    System.out.println(String.format("Avg cost: %f",
                            lastCosts.stream()
                                    .mapToInt(__ -> __)
                                    .average()
                                    .orElse(0d)
                    ));
                    lastCosts.clear();
                }
            }

            @Override
            public void onEvaluated(EvaluatedSolution solution, long iteration) {
                final int cost = ((IntCost) solution.getQuality()).getValue();
                lastCosts.add(cost);
//                System.out.println(String.format("%nEvaluated at iteration %d: %s",
//                        iteration,
//                        solution
//                ));
            }

            @Override
            public void onBetterSolution(EvaluatedSolution solution, long iteration) {
                // print better solution
//                if (solution.isBetterThan(new IntCost(117001))) {
                    System.out.println(String.format("%nBetter solution at iteration %d:%n" +
                                    "%s",
                            iteration,
                            solution
                    ));
//                }
                // stop when target reached
                if (solution.isBetterThan(new IntCost(115000))) {
                    algo.pauseBeforeNextIteration();
                }
            }
        });

        algo.run();
    }

    private List<Mutator> getCppMutatorsTest1(CppInstance cppInstance) {
        final Rand rand = new Rand();

        final KeyMutation randomizeKeyMutation = new RandomizeKeyMutation(rand);
        final KeyMutation addRandValueKeyMutation = new AddRandValueKeyMutation(rand, -0.01d, +0.01d);

        final Mutation rand2ConsecutiveKeysRkvMutation = new ConsecutiveKeysRKVMutation(2, randomizeKeyMutation, rand);
        final Mutation addValue2ConsecutiveKeysRkvMutation = new ConsecutiveKeysRKVMutation(2, addRandValueKeyMutation, rand);
        final Mutation randProbaRkvMutation = new ProbaRKVMutation(randomizeKeyMutation, 2d / GenomeCP.lengthForCppInstance(cppInstance), rand);
        final Mutation addValueProbaRkvMutation = new ProbaRKVMutation(addRandValueKeyMutation, 2d / GenomeCP.lengthForCppInstance(cppInstance), rand);

        final List<Mutator> mutators = new ArrayList<>();
        mutators.addAll(Collections.nCopies(15, new GeneticSolutionMutator(rand2ConsecutiveKeysRkvMutation)));
        mutators.addAll(Collections.nCopies(15, new GeneticSolutionMutator(addValue2ConsecutiveKeysRkvMutation)));
        mutators.addAll(Collections.nCopies(15, new GeneticSolutionMutator(randProbaRkvMutation)));
        mutators.addAll(Collections.nCopies(15, new GeneticSolutionMutator(addValueProbaRkvMutation)));
        return mutators;
    }

    private List<Mutator> getCppMutatorsTest2(CppInstance cppInstance) {
        final Rand rand = new Rand();

        final List<Mutator> mutators = new ArrayList<>();
        // randomize the whole genome
        mutators.addAll(Collections.nCopies(40, new GeneticSolutionMutator(
                new RandomizeMutation(rand)
        )));
        // randomize 1 random key of the genome
        mutators.addAll(Collections.nCopies(500, new GeneticSolutionMutator(
                new ConsecutiveKeysRKVMutation(1,
                        new RandomizeKeyMutation(rand), rand
                )
        )));
        // randomize 1 random key then 1 again
        mutators.addAll(Collections.nCopies(500, new SequenceMutator(
                new GeneticSolutionMutator(
                        new ConsecutiveKeysRKVMutation(1,
                                new RandomizeKeyMutation(rand), rand
                        )
                ),
                new GeneticSolutionMutator(
                        new ConsecutiveKeysRKVMutation(1,
                                new RandomizeKeyMutation(rand), rand
                        )
                )
        )));
        return mutators;
    }
}