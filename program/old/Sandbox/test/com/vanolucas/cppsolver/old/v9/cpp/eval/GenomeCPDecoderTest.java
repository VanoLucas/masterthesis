package com.vanolucas.cppsolver.old.v9.cpp.eval;

import com.vanolucas.cppsolver.old.v9.cpp.genome.GenomeCP;
import com.vanolucas.cppsolver.old.v9.cpp.instance.CppInstance;
import com.vanolucas.cppsolver.old.v9.cpp.instance.CppInstance2;
import com.vanolucas.cppsolver.old.v9.util.Rand;
import org.junit.jupiter.api.Test;

class GenomeCPDecoderTest {

    @Test
    void decode() {
        final CppInstance cppInstance = new CppInstance2();

        final GenomeCP genome = new GenomeCP(cppInstance);
        genome.randomizeKey(0, new Rand());
        genome.randomizeKey(1, new Rand());

        final GenomeCPDecoder decoder = new GenomeCPDecoder(cppInstance);

        final Slots slots = decoder.decode(genome);

        System.out.println(cppInstance);
        System.out.println(genome);
        System.out.println(slots);
    }
}