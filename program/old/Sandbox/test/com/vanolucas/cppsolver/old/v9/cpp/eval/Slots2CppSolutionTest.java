package com.vanolucas.cppsolver.old.v9.cpp.eval;

import com.vanolucas.cppsolver.old.v9.cpp.instance.CppInstance;
import com.vanolucas.cppsolver.old.v9.cpp.instance.CppInstance2;
import com.vanolucas.cppsolver.old.v9.cpp.solution.CppSolution;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

class Slots2CppSolutionTest {

    @Test
    void convert() {
        final CppInstance cppInstance = new CppInstance2();

        final Slots2CppSolution slots2CppSolution = new Slots2CppSolution(cppInstance);

        final Slots slots = new Slots(Arrays.asList(
                new Slot(1, 100),
                new Slot(1, 400),
                new Slot(2, 200),
                new Slot(2, 300)
        ));

        final CppSolution cppSolution = slots2CppSolution.convert(slots);

        System.out.println(cppSolution);
    }
}