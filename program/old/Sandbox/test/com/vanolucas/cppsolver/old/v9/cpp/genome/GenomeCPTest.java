package com.vanolucas.cppsolver.old.v9.cpp.genome;

import com.vanolucas.cppsolver.old.v9.cpp.instance.CppInstance;
import com.vanolucas.cppsolver.old.v9.cpp.instance.CppInstance2;
import org.junit.jupiter.api.Test;

class GenomeCPTest {
    @Test
    void calculateLength() {
        final CppInstance cppInstance = new CppInstance2();
        final GenomeCP genomeCP = new GenomeCP(cppInstance);
        System.out.println(cppInstance);
        System.out.println(GenomeCP.lengthForCppInstance(cppInstance));
        System.out.println(genomeCP);
    }
}