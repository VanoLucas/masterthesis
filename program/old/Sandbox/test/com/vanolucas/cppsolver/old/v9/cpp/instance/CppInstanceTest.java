package com.vanolucas.cppsolver.old.v9.cpp.instance;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class CppInstanceTest {

    @Test
    void toString1() {
        final List<Integer> covers = Arrays.asList(1000, 3000, 2000);
        final CppInstance cppInstance = new CppInstance(covers, 2);
        System.out.println(cppInstance);
    }
}