package com.vanolucas.cppsolver.old.v9.cpp.solution;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class CppSolutionTest {

    @Test
    void toString1() {
        final List<Plate> plates = Arrays.asList(
                new Plate(Arrays.asList(1, 2, 3, 4), 222),
                new Plate(Arrays.asList(2, 3, 4, 5), 111)
        );
        final CppSolution cppSolution = new CppSolution(plates);
        System.out.println(cppSolution);
    }
}