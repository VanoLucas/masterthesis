package com.vanolucas.cppsolver.old.v9.cpp.solution;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class PlateTest {

    @Test
    void toString1() {
        final List<Integer> covers = Arrays.asList(4, 2, 3, 1);
        final Plate plate = new Plate(covers, 123);
        System.out.println(plate);
    }
}