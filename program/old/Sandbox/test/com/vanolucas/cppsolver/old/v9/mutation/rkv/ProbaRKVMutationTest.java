package com.vanolucas.cppsolver.old.v9.mutation.rkv;

import com.vanolucas.cppsolver.old.v9.genome.RandomKeyVector;
import com.vanolucas.cppsolver.old.v9.mutation.rkv.key.KeyMutation;
import com.vanolucas.cppsolver.old.v9.mutation.rkv.key.RandomizeKeyMutation;
import com.vanolucas.cppsolver.old.v9.util.Rand;
import org.junit.jupiter.api.Test;

class ProbaRKVMutationTest {

    @Test
    void mutate() {
        final Rand rand = new Rand();

        final KeyMutation keyMutation = new RandomizeKeyMutation(rand);
        final ProbaRKVMutation rkvMutation = new ProbaRKVMutation(keyMutation, 0.5d, rand);

        final RandomKeyVector rkv = new RandomKeyVector(10, 0d);

        System.out.println(rkv);
        rkvMutation.mutate(rkv);
        System.out.println(rkv);
    }
}