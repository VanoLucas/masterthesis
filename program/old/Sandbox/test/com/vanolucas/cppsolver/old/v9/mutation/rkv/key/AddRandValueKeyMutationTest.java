package com.vanolucas.cppsolver.old.v9.mutation.rkv.key;

import com.vanolucas.cppsolver.old.v9.genome.RandomKeyVector;
import com.vanolucas.cppsolver.old.v9.mutation.rkv.AllKeysRKVMutation;
import com.vanolucas.cppsolver.old.v9.util.Rand;
import org.junit.jupiter.api.Test;

class AddRandValueKeyMutationTest {

    @Test
    void mutation() {
        final Rand rand = new Rand();
        final AddRandValueKeyMutation keyMutation = new AddRandValueKeyMutation(rand, -0.1d, 0.1d);
        final AllKeysRKVMutation rkvMutation = new AllKeysRKVMutation(keyMutation, rand);

        final RandomKeyVector rkv = new RandomKeyVector(3, 0d);

        System.out.println(rkv);
        rkvMutation.mutate(rkv);
        System.out.println(rkv);
    }
}