package com.vanolucas.cppsolver.v12.algo;

import org.junit.jupiter.api.Test;

class OptiTest {

    @Test
    void run() {
        final Opti opti = new Opti();

        final OptiStep step = new OptiStep(opti) {
            @Override
            void runStep(long iteration) {
                System.out.println("Step " + iteration);
                if (iteration >= 3) {
                    opti.pauseBeforeNextIteration();
                }
            }
        };

        opti.run();
    }

    @Test
    void addListener() {
        final Opti opti = new Opti();

        final OptiStep step = new OptiStep(opti) {
            @Override
            void runStep(long iteration) {
                System.out.println("Step " + iteration);
            }
        };

        opti.addOnNewIterationListener(iteration -> {
            if (iteration >= 3) {
                opti.pauseBeforeNextIteration();
            }
        });

        opti.addOnPauseListener(iteration -> {
            System.out.println("Paused after iteration " + iteration);
        });

        opti.run();
    }
}