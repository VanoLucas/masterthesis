package com.vanolucas.cppsolver.v12.algo;

import com.vanolucas.cppsolver.v12.eval.Evaluator;
import com.vanolucas.cppsolver.v12.eval.ParallelEvaluator;
import com.vanolucas.cppsolver.v12.eval.SolutionEvaluationFunction;
import com.vanolucas.cppsolver.v12.mutation.GeneticSolutionMutator;
import com.vanolucas.cppsolver.v12.mutation.GenomeMutator;
import com.vanolucas.cppsolver.v12.mutation.RandomizeGenomeMutator;
import com.vanolucas.cppsolver.v12.mutation.SolutionMutator;
import com.vanolucas.cppsolver.v12.problem.targetnb.TargetNumberProblem;
import com.vanolucas.cppsolver.v12.processor.ParallelSolutionProcessor;
import com.vanolucas.cppsolver.v12.processor.SolutionProcessor;
import com.vanolucas.cppsolver.v12.quality.IntCost;
import com.vanolucas.cppsolver.v12.rand.Rand;
import com.vanolucas.cppsolver.v12.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.v12.solution.Solution;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

class ParallelSingleMutationOptiStepTest {

    @Test
    void runStep() {
        final int nbParallelSingleMutationOpti = 10;

        // create target number problem instance
        final TargetNumberProblem problem = new TargetNumberProblem(100, 2, 100);

        // create opti algo
        final Opti opti = new Opti();

        // evaluation function
        final SolutionEvaluationFunction solutionEvalFn = problem.newSolutionEvaluationFunction();
        final Evaluator evaluator = new ParallelEvaluator(
                solutionEvalFn,
                8
        );

        // initial solutions
        final List<Solution> initialSolutions = IntStream.range(0, nbParallelSingleMutationOpti)
                .mapToObj(i -> problem.newSolution())
                .collect(Collectors.toList());

        // mutators
        final Rand rand = new Rand();
        final GenomeMutator genomeMutator = RandomizeGenomeMutator.getDefault(rand);
        final SolutionMutator solutionMutator = new GeneticSolutionMutator(genomeMutator);
        final List<SolutionMutator> mutators = Collections.nCopies(nbParallelSingleMutationOpti, solutionMutator);

        // processor
        final SolutionProcessor<EvaluatedSolution> stepProcessor = new ParallelSolutionProcessor<>(8);

        // create parallel single mutation algo
        final OptiStep step = new ParallelSingleMutationOptiStep(
                opti,
                evaluator,
                initialSolutions,
                mutators,
                stepProcessor
        );

        final List<Long> endIteration = new ArrayList<>();

        // set listeners
        opti.addOnEvaluatedListener((solution, iteration) -> {
            // print evaluated solution
//            System.out.println(String.format("%nEvaluated at iteration %d:%n" +
//                            "%s",
//                    iteration,
//                    solution
//            ));

            // stop when target reached
            if (solution.isBetterOrEquivalentTo(IntCost.zero())) {
                opti.pauseBeforeNextIteration();
                // log final iteration
                endIteration.add(iteration);
            }
        });

        // run opti multiple times
        while (endIteration.size() < 1000) {
            opti.resetIterationNumber();
            opti.run();
            // print average iterations to reach target
            if (endIteration.size() % 100 == 0) {
                System.out.println(String.format("Average iterations to reach target: %f",
                        endIteration.stream()
                                .mapToDouble(__ -> __)
                                .average()
                                .orElse(0d)
                ));
            }
        }
    }
}