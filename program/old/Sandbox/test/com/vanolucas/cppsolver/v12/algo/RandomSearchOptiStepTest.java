package com.vanolucas.cppsolver.v12.algo;

import com.vanolucas.cppsolver.v12.eval.ComplexGeneticSolutionEvaluationFunction;
import com.vanolucas.cppsolver.v12.eval.EvaluationFunction;
import com.vanolucas.cppsolver.v12.eval.SimpleSolutionEvaluationFunction;
import com.vanolucas.cppsolver.v12.eval.SolutionEvaluationFunction;
import com.vanolucas.cppsolver.v12.genome.Genome;
import com.vanolucas.cppsolver.v12.problem.cpp.eval.CppSolutionEvaluationFunction;
import com.vanolucas.cppsolver.v12.problem.cpp.eval.GenomeCfDecoder;
import com.vanolucas.cppsolver.v12.problem.cpp.eval.SlotsDecoder;
import com.vanolucas.cppsolver.v12.problem.cpp.genome.GenomeCf;
import com.vanolucas.cppsolver.v12.problem.cpp.instance.CppInstance;
import com.vanolucas.cppsolver.v12.quality.IntCost;
import com.vanolucas.cppsolver.v12.rand.*;
import com.vanolucas.cppsolver.v12.solution.ComplexGeneticSolution;
import com.vanolucas.cppsolver.v12.solution.SimpleSolution;
import com.vanolucas.cppsolver.v12.solution.Solution;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

class RandomSearchOptiStepTest {

    @Test
    void runStep() {
        final int target = 0;

        // actual solution type
        class ActualSolution implements Randomizable {
            private int value;

            private ActualSolution(int value) {
                this.value = value;
            }

            @Override
            public void randomize(Rand rand) {
                this.value = rand.int0To(100);
            }

            @Override
            public String toString() {
                return String.valueOf(value);
            }
        }

        // opti algo
        final Opti opti = new Opti();

        // evaluation function
        final EvaluationFunction<ActualSolution, IntCost> actualSolutionEvalFn = solution -> new IntCost(Math.abs(solution.value - target));
        final SolutionEvaluationFunction solutionEvalFn = new SimpleSolutionEvaluationFunction(actualSolutionEvalFn);

        // initial solution
        final Solution initialSolution = new SimpleSolution(new ActualSolution(111));

        // randomizer
        final Rand rand = new Rand();
        final Randomizer actualSolutionRandomizer = Randomizer.getDefault(rand);
        final SolutionRandomizer solutionRandomizer = new SimpleSolutionRandomizer(actualSolutionRandomizer);

        // random search step
        final OptiStep step = new RandomSearchOptiStep(opti, solutionEvalFn, initialSolution, solutionRandomizer);

        // listeners
        opti.addOnEvaluatedListener((solution, iteration) -> {
            System.out.println(String.format("Evaluated at iteration %d:%n%s",
                    iteration,
                    solution
            ));
            if (solution.isBetterOrEquivalentTo(IntCost.zero())) {
                opti.pauseBeforeNextIteration();
            }
        });

        // run opti
        opti.run();
    }

    @Test
    void cpp() {
        // CPP instance
        final List<Integer> covers = Arrays.asList(100, 200);
        final CppInstance cppInstance = new CppInstance(covers, 2);

        // opti algo
        final Opti opti = new Opti();

        // initial solution
        final Genome genome = new GenomeCf(cppInstance);
        final Solution initialSolution = new ComplexGeneticSolution(genome, 1);

        // evaluation function
        final Function genomeDecoder = new GenomeCfDecoder(cppInstance);
        final Function intermediateDecoder = new SlotsDecoder(cppInstance);
        final EvaluationFunction evalFn = new CppSolutionEvaluationFunction(cppInstance);
        final SolutionEvaluationFunction solutionEvalFn = new ComplexGeneticSolutionEvaluationFunction(
                genomeDecoder,
                Collections.singletonList(intermediateDecoder),
                evalFn
        );

        // randomizer
        final Rand rand = new Rand();
        final GenomeRandomizer genomeRandomizer = GenomeRandomizer.getDefault(rand);
        final SolutionRandomizer solutionRandomizer = new GeneticSolutionRandomizer(genomeRandomizer);

        // random search algo
        final OptiStep step = new RandomSearchOptiStep(
                opti,
                solutionEvalFn,
                initialSolution,
                solutionRandomizer
        );

        // listener
        opti.addOnEvaluatedListener((solution, iteration) -> {
            System.out.println(String.format("Evaluated at iteration %d:%n%s",
                    iteration,
                    solution
            ));
        });

        // run opti
        opti.run();
    }
}