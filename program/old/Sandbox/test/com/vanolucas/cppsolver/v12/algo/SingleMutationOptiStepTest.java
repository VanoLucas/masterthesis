package com.vanolucas.cppsolver.v12.algo;

import com.vanolucas.cppsolver.v12.eval.EvaluationFunction;
import com.vanolucas.cppsolver.v12.eval.SimpleSolutionEvaluationFunction;
import com.vanolucas.cppsolver.v12.eval.SolutionEvaluationFunction;
import com.vanolucas.cppsolver.v12.mutation.Mutator;
import com.vanolucas.cppsolver.v12.mutation.SimpleSolutionMutator;
import com.vanolucas.cppsolver.v12.mutation.SolutionMutator;
import com.vanolucas.cppsolver.v12.quality.IntCost;
import com.vanolucas.cppsolver.v12.rand.Rand;
import com.vanolucas.cppsolver.v12.rand.Randomizable;
import com.vanolucas.cppsolver.v12.solution.SimpleSolution;
import com.vanolucas.cppsolver.v12.solution.Solution;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SingleMutationOptiStepTest {

    @Test
    void runStep() {
        final int target = 0;

        // actual solution type
        class ActualSolution implements Randomizable {
            private int value;

            private ActualSolution(int value) {
                this.value = value;
            }

            @Override
            public void randomize(Rand rand) {
                this.value = rand.int0To(100);
            }

            @Override
            public String toString() {
                return String.valueOf(value);
            }
        }

        // opti algo
        final Opti opti = new Opti();

        // evaluation function
        final EvaluationFunction<ActualSolution, IntCost> evalFn = solution -> new IntCost(Math.abs(solution.value - target));
        final SolutionEvaluationFunction solutionEvalFn = new SimpleSolutionEvaluationFunction(evalFn);

        // initial solution
        final Solution initialSolution = new SimpleSolution(new ActualSolution(111));

        // mutator
        final Rand rand = new Rand();
        final Mutator<ActualSolution> mutator = solution -> solution.randomize(rand);
        final SolutionMutator solutionMutator = new SimpleSolutionMutator(mutator);

        // single mutation opti step
        final OptiStep step = new SingleMutationOptiStep(opti, solutionEvalFn, initialSolution, solutionMutator);

        // listeners
        opti.addOnEvaluatedListener((solution, iteration) -> {
            System.out.println(String.format("Evaluated at iteration %d:%n%s",
                    iteration,
                    solution
            ));
            if (solution.isBetterOrEquivalentTo(IntCost.zero())) {
                opti.pauseBeforeNextIteration();
            }
        });

        // run opti
        opti.run();
    }
}