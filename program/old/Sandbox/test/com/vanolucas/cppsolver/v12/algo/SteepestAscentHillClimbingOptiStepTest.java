package com.vanolucas.cppsolver.v12.algo;

import com.vanolucas.cppsolver.v12.clone.ComplexGeneticSolutionCloner;
import com.vanolucas.cppsolver.v12.clone.GenomeCloner;
import com.vanolucas.cppsolver.v12.clone.SolutionCloner;
import com.vanolucas.cppsolver.v12.eval.*;
import com.vanolucas.cppsolver.v12.genome.RandomKeyVector;
import com.vanolucas.cppsolver.v12.mutation.CompositeSolutionMutator;
import com.vanolucas.cppsolver.v12.mutation.GeneticSolutionMutator;
import com.vanolucas.cppsolver.v12.mutation.RandomizeGenomeMutator;
import com.vanolucas.cppsolver.v12.mutation.SolutionMutator;
import com.vanolucas.cppsolver.v12.mutation.rkv.AddRandValueKeyRkvMutator;
import com.vanolucas.cppsolver.v12.mutation.rkv.ConsecutiveKeysRkvMutator;
import com.vanolucas.cppsolver.v12.mutation.rkv.ProbaKeysRkvMutator;
import com.vanolucas.cppsolver.v12.mutation.rkv.RandomizeKeyRkvMutator;
import com.vanolucas.cppsolver.v12.problem.cpp.eval.*;
import com.vanolucas.cppsolver.v12.problem.cpp.genome.GenomeCf;
import com.vanolucas.cppsolver.v12.problem.cpp.genome.GenomePpc;
import com.vanolucas.cppsolver.v12.problem.cpp.genome.GenomeSp;
import com.vanolucas.cppsolver.v12.problem.cpp.instance.CppInstance;
import com.vanolucas.cppsolver.v12.problem.cpp.instance.CppInstanceI013;
import com.vanolucas.cppsolver.v12.problem.targetnb.TargetNumberProblem;
import com.vanolucas.cppsolver.v12.quality.IntCost;
import com.vanolucas.cppsolver.v12.rand.GenomeRandomizer;
import com.vanolucas.cppsolver.v12.rand.Rand;
import com.vanolucas.cppsolver.v12.solution.ComplexGeneticSolution;
import com.vanolucas.cppsolver.v12.solution.Solution;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;

class SteepestAscentHillClimbingOptiStepTest {

    @Test
    void runStep() {
        final int nbRuns = 1; // 100_000
        final int nbThreads = 2;

        final TargetNumberProblem problem = new TargetNumberProblem(100, 2, 100);

        final Opti opti = new Opti();

        final SolutionEvaluationFunction solutionEvalFn = problem.newSolutionEvaluationFunction();
        final Evaluator evaluator = new ParallelEvaluator(solutionEvalFn, nbThreads);

        final Solution initialSolution = problem.newSolution();

        final SolutionCloner cloner = problem.newSolutionCloner();

        final Rand rand = new Rand();
        final List<SolutionMutator> mutators = new ArrayList<>();
//        mutators.addAll(Collections.nCopies(1,
//                new GeneticSolutionMutator(RandomizeGenomeMutator.getDefault(rand))
//        ));
//        mutators.addAll(Collections.nCopies(1,
//                new GeneticSolutionMutator(new RandomizeKeyRkvMutator(0, rand))
//        ));
//        mutators.addAll(Collections.nCopies(1,
//                new GeneticSolutionMutator(new RandomizeKeyRkvMutator(1, rand))
//        ));
        mutators.addAll(Collections.nCopies(1,
                new GeneticSolutionMutator(new AddRandValueKeyRkvMutator(0, -0.1, +0.1, rand))
        ));
        mutators.addAll(Collections.nCopies(1,
                new GeneticSolutionMutator(new AddRandValueKeyRkvMutator(1, -0.1, +0.1, rand))
        ));

        final ExecutorService executor = Executors.newFixedThreadPool(nbThreads);

        final OptiStep optiStep = new SteepestAscentHillClimbingOptiStep(
                opti,
                evaluator,
                initialSolution,
                cloner,
                mutators,
                executor
        );

        final List<Long> endIteration = new ArrayList<>();

        // set listeners
        opti.addOnEvaluatedListener((solution, iteration) -> {
            // print evaluated solution
            System.out.println(String.format("%nEvaluated at iteration %d:%n" +
                            "%s",
                    iteration,
                    solution
            ));

            // stop when target reached
            if (solution.isBetterOrEquivalentTo(IntCost.zero())) {
                opti.pauseBeforeNextIteration();
                // log final iteration
                synchronized (endIteration) {
                    endIteration.add(iteration);
                }
            }
        });

        // run opti multiple times
        while (endIteration.size() < nbRuns) {
            opti.resetIterationNumber();
            opti.run();
            // print average iterations to reach target
            if (endIteration.size() % 10_000 == 0) {
                System.out.println(String.format("Average iterations to reach target: %f",
                        endIteration.stream()
                                .mapToDouble(__ -> __)
                                .average()
                                .orElse(0d)
                ));
            }
        }
    }

    @Test
    void cpp() throws IOException {
        // CPP instance
//        final CppInstance cppInstance = new CppInstanceI013();
        final CppInstance cppInstance = new CppInstance("I013", 10, 4, 18676d, 13.44d);

        // opti algo
        final Opti opti = new Opti();

        // initial solution
//        final GenomeCf genome = new GenomeCf(cppInstance);
        final GenomeSp genome = new GenomeSp(cppInstance);
//        final GenomePpc genome = new GenomePpc(cppInstance);
//        GenomeCf.parse("(20 genes) 0.06149308675043892 0.07188913742022388 | 0.621256334459859 0.6709382412155973 | 0.0 0.8597391005808943 | 0.5965840579329555 0.6694165105156483 | 0.0715115712314843 0.18473207048559437 | 0.43773315853318684 0.6855684675766428 | 0.18344167533304578 0.05400685636362078 | 0.837084157170689 0.7515815991225744 | 0.41538474519293506 0.12906743727521586 | 0.8328953908453587 0.338295912382585")
//                .copyKeyValuesTo(genome);
        final Solution initialSolution = new ComplexGeneticSolution(genome, 1);

        // evaluation
        final int nbThreadsEvaluator = 1;
//        final Function genomeDecoder = new GenomeCfDecoder(cppInstance);
        final Function genomeDecoder = new GenomeSpDecoder(cppInstance);
//        final Function genomeDecoder = new GenomePpcDecoder(cppInstance);
        final Function intermediateDecoder = new SlotsDecoder(cppInstance);
        final EvaluationFunction evalFn = new CppSolutionEvaluationFunction(cppInstance);
        final SolutionEvaluationFunction solutionEvalFn = new ComplexGeneticSolutionEvaluationFunction(
                genomeDecoder,
                Collections.singletonList(intermediateDecoder),
                evalFn
        );
        final Evaluator evaluator = new ParallelEvaluator(solutionEvalFn, nbThreadsEvaluator);

        // cloner
        final GenomeCloner genomeCloner = GenomeCloner.getDefault();
        final SolutionCloner solutionCloner = new ComplexGeneticSolutionCloner(genomeCloner);

        // mutators
        final Rand rand = new Rand();
        final List<SolutionMutator> solutionMutators = getMutators03(genome, rand);

        // hill climbing algo
        final int nbThreadsHillClimbing = 8;
        final OptiStep step = new SteepestAscentHillClimbingOptiStep(
                opti,
                evaluator,
                initialSolution,
                solutionCloner,
                solutionMutators,
                nbThreadsHillClimbing
        );

        // print new better solutions
        opti.enablePrintNewBetterSolutions();

        // run opti
        opti.run();
    }

    private List<SolutionMutator> getMutators00(RandomKeyVector genome, Rand rand) {
        final List<SolutionMutator> solutionMutators = new ArrayList<>();
        solutionMutators.addAll(Collections.nCopies(1, new GeneticSolutionMutator(
                new RandomizeGenomeMutator(GenomeRandomizer.getDefault(rand))
        )));
        return solutionMutators;
    }

    private List<SolutionMutator> getMutators01(RandomKeyVector genome, Rand rand) {
        final List<SolutionMutator> solutionMutators = new ArrayList<>();
        solutionMutators.addAll(Collections.nCopies(2, new GeneticSolutionMutator(
                new RandomizeGenomeMutator(GenomeRandomizer.getDefault(rand))
        )));
        solutionMutators.addAll(Collections.nCopies(3, new GeneticSolutionMutator(
                new ProbaKeysRkvMutator(2d / genome.length(),
                        new AddRandValueKeyRkvMutator(-0.05, +0.05, rand),
                        rand)
        )));
        solutionMutators.addAll(Collections.nCopies(3, new GeneticSolutionMutator(
                new ConsecutiveKeysRkvMutator(2,
                        new AddRandValueKeyRkvMutator(-0.05, +0.05, rand),
                        rand)
        )));
        return solutionMutators;
    }

    private List<SolutionMutator> getMutators02(RandomKeyVector genome, Rand rand) {
        final List<SolutionMutator> solutionMutators = new ArrayList<>();
        // randomize the whole genome
        solutionMutators.addAll(Collections.nCopies(40, new GeneticSolutionMutator(
                RandomizeGenomeMutator.getDefault(rand)
        )));
        // randomize 1 random key of the genome
        solutionMutators.addAll(Collections.nCopies(500, new GeneticSolutionMutator(
                new ConsecutiveKeysRkvMutator(1,
                        new RandomizeKeyRkvMutator(rand),
                        rand)
        )));
        // randomize 1 random key then 1 again
        solutionMutators.addAll(Collections.nCopies(500, new CompositeSolutionMutator(
                new GeneticSolutionMutator(new ConsecutiveKeysRkvMutator(1,
                        new RandomizeKeyRkvMutator(rand),
                        rand)),
                new GeneticSolutionMutator(new ConsecutiveKeysRkvMutator(1,
                        new RandomizeKeyRkvMutator(rand),
                        rand))
        )));
        return solutionMutators;
    }

    private List<SolutionMutator> getMutators03(RandomKeyVector genome, Rand rand) {
        final List<SolutionMutator> solutionMutators = new ArrayList<>();
        // randomize whole genome
        solutionMutators.addAll(Collections.nCopies(2, new GeneticSolutionMutator(
                new RandomizeGenomeMutator(GenomeRandomizer.getDefault(rand))
        )));
        // randomize 1 random key then 1 again
        solutionMutators.addAll(Collections.nCopies(3, new CompositeSolutionMutator(
                new GeneticSolutionMutator(new ConsecutiveKeysRkvMutator(1,
                        new RandomizeKeyRkvMutator(rand),
                        rand)),
                new GeneticSolutionMutator(new ConsecutiveKeysRkvMutator(1,
                        new RandomizeKeyRkvMutator(rand),
                        rand))
        )));
        // proba to add rand value to each key
        solutionMutators.addAll(Collections.nCopies(3, new GeneticSolutionMutator(
                new ProbaKeysRkvMutator(2d / genome.length(),
                        new AddRandValueKeyRkvMutator(-0.05, +0.05, rand),
                        rand)
        )));
        // add rand value to 2 consecutive keys
        solutionMutators.addAll(Collections.nCopies(3, new GeneticSolutionMutator(
                new ConsecutiveKeysRkvMutator(2,
                        new AddRandValueKeyRkvMutator(-0.05, +0.05, rand),
                        rand)
        )));
        return solutionMutators;

        /*
        New better solution at iteration 1379411:
        Genome:
        (20 genes) 0.6214798703247518 0.6888289325889448 | 0.821373970641556 0.7934141182161765 | 0.44828155756261556 0.32087572242301643 | 0.1771225689415295 0.043156027354643245 | 0.4326696280403719 0.35301607038011157 | 0.7160230574145419 0.12916866059165122 | 0.036172682362696426 0.21316470810082042 | 0.0 0.8662047717131821 | 0.9180222624268176 0.37467952356408435 | 0.0963839626714672 0.0363848131712992
        Intermediate:
        (40 slots) 2:26018 | 3:26000 | 4:26000 | 0:25986 | 1:22032 | 5:22008 | 6:22000 | 7:22000 | 8:20000 | 9:20000 | 10:19000 | 11:18000 | 14:15000 | 15:15000 | 16:14000 | 17:13500 | 19:11000 | 12:10999 | 13:10866 | 20:10500 | 22:9000 | 23:9000 | 18:8954 | 21:8709 | 12:6001 | 25:6000 | 1:5968 | 24:5950 | 13:5134 | 26:5000 | 18:4046 | 0:4014 | 27:1564 | 24:1550 | 28:1500 | 21:1291 | 29:1000 | 5:992 | 2:982 | 27:936
        Phenotype:
        Offset plates:
        |  0|  2|  3|  4|	 26018 printings
        |  1|  5|  6|  7|	 22032 printings
        |  8|  9| 10| 11|	 20000 printings
        | 14| 15| 16| 17|	 15000 printings
        | 12| 13| 19| 20|	 11000 printings
        | 18| 21| 22| 23|	  9000 printings
        |  1| 12| 24| 25|	  6001 printings
        |  0| 13| 18| 26|	  5134 printings
        | 21| 24| 27| 28|	  1564 printings
        |  2|  5| 27| 29|	  1000 printings
        Total printings: 116749
        Quality: 116749.0

        New better solution at iteration 395018:
        Genome:
        (20 genes) 0.19274935578055002 0.9578636503583133 | 0.06597292274149684 0.7813210934656618 | 0.42895641853913163 0.6456170137473577 | 0.0 0.841175306553649 | 0.5679710215719425 0.6617929474896898 | 0.6188656741360848 0.3317193297658409 | 0.9230382707582401 0.41520773077135364 | 0.44804200417749696 0.09110425474323991 | 0.8168381807809785 0.8035985290472433 | 0.08296667562262397 0.9610375032245168
        Intermediate:
        (40 slots) 3:26000 | 4:26000 | 2:25948 | 0:25235 | 5:22030 | 6:22000 | 7:22000 | 1:21876 | 8:20000 | 9:20000 | 10:19000 | 11:18000 | 14:15000 | 15:15000 | 13:14543 | 16:14000 | 19:11000 | 12:10975 | 20:10500 | 21:10000 | 22:9000 | 23:9000 | 17:8934 | 18:8688 | 1:6124 | 24:6026 | 12:6025 | 25:6000 | 26:5000 | 0:4765 | 17:4566 | 18:4312 | 28:1500 | 24:1474 | 27:1462 | 13:1457 | 2:1052 | 27:1038 | 29:1000 | 5:970
        Phenotype:
        Offset plates:
        |  0|  2|  3|  4|	 26000 printings
        |  1|  5|  6|  7|	 22030 printings
        |  8|  9| 10| 11|	 20000 printings
        | 13| 14| 15| 16|	 15000 printings
        | 12| 19| 20| 21|	 11000 printings
        | 17| 18| 22| 23|	  9000 printings
        |  1| 12| 24| 25|	  6124 printings
        |  0| 17| 18| 26|	  5000 printings
        | 13| 24| 27| 28|	  1500 printings
        |  2|  5| 27| 29|	  1052 printings
        Total printings: 116706
        Quality: 116706.0

        New better solution at iteration 1447839:
        Genome:
        (20 genes) 0.41433528684697596 0.6436455849317662 | 0.4421618911343212 0.07689896148933836 | 0.6271111894383818 0.35373063433696494 | 0.5721374704877261 0.3511473434833339 | 0.03327727713835339 0.1635214282634861 | 0.09963630782746115 0.9619367909791953 | 0.8172813313422995 0.7988754019865165 | 0.929948721118713 0.4031047735657824 | 0.18392734835783225 0.04546689558383021 | 0.0429945689292196 0.7887987806827499
        Intermediate:
        (40 slots) 3:26000 | 4:26000 | 2:25972 | 0:25095 | 1:22086 | 6:22000 | 7:22000 | 5:21955 | 8:20000 | 9:20000 | 10:19000 | 11:18000 | 14:15000 | 15:15000 | 13:14770 | 16:14000 | 19:11000 | 12:10941 | 20:10500 | 21:10000 | 22:9000 | 23:9000 | 17:8760 | 18:8402 | 12:6059 | 25:6000 | 24:5991 | 1:5914 | 26:5000 | 0:4905 | 17:4740 | 18:4598 | 24:1509 | 28:1500 | 27:1493 | 13:1230 | 5:1045 | 2:1028 | 27:1007 | 29:1000
        Phenotype:
        Offset plates:
        |  0|  2|  3|  4|	 26000 printings
        |  1|  5|  6|  7|	 22086 printings
        |  8|  9| 10| 11|	 20000 printings
        | 13| 14| 15| 16|	 15000 printings
        | 12| 19| 20| 21|	 11000 printings
        | 17| 18| 22| 23|	  9000 printings
        |  1| 12| 24| 25|	  6059 printings
        |  0| 17| 18| 26|	  5000 printings
        | 13| 24| 27| 28|	  1509 printings
        |  2|  5| 27| 29|	  1045 printings
        Total printings: 116699
        Quality: 116699.0

        New better solution at iteration 1165901:
        Genome:
        (20 genes) 0.08746828305936624 0.9626164745825332 | 0.02414471598301815 0.13768769442533071 | 0.45715281796418195 0.9309947174566751 | 0.1851991933622602 0.9532729178856791 | 0.8260253877480876 0.802954756197475 | 0.9154376144258032 0.426861049403893 | 0.6274763539697211 0.3894048532675445 | 0.5916440808235184 0.6640572900526879 | 0.03997567335160772 0.21545883258163254 | 0.4198349639993136 0.35488480038546405
        Intermediate:
        (40 slots) 3:26000 | 4:26000 | 2:25990 | 0:25870 | 6:22000 | 7:22000 | 1:21968 | 5:21925 | 8:20000 | 9:20000 | 10:19000 | 11:18000 | 14:15000 | 15:15000 | 13:14895 | 16:14000 | 19:11000 | 12:10967 | 20:10500 | 21:10000 | 22:9000 | 23:9000 | 17:8964 | 18:7938 | 12:6033 | 1:6032 | 24:6022 | 25:6000 | 18:5062 | 26:5000 | 17:4536 | 0:4130 | 28:1500 | 24:1478 | 27:1433 | 13:1105 | 5:1075 | 27:1067 | 2:1010 | 29:1000
        Phenotype:
        Offset plates:
        |  0|  2|  3|  4|	 26000 printings
        |  1|  5|  6|  7|	 22000 printings
        |  8|  9| 10| 11|	 20000 printings
        | 13| 14| 15| 16|	 15000 printings
        | 12| 19| 20| 21|	 11000 printings
        | 17| 18| 22| 23|	  9000 printings
        |  1| 12| 24| 25|	  6033 printings
        |  0| 17| 18| 26|	  5062 printings
        | 13| 24| 27| 28|	  1500 printings
        |  2|  5| 27| 29|	  1075 printings
        Total printings: 116670
        Quality: 116670.0

        New better solution at iteration 238129:
        Genome:
        (20 genes) 0.8119223096805934 0.7968772716967366 | 0.5748519246094772 0.35244810029595686 | 0.9124957496571998 0.47229531976555783 | 0.45838347417921155 0.06252647150057403 | 0.543717965156256 0.6439152640947443 | 0.061231800094165345 0.21213631067385638 | 0.08821800179895652 0.9644742622525502 | 0.19095340741086358 0.04219244131046751 | 0.012470936117471021 0.1333818341883804 | 0.4273352150840316 0.3512915652827778
        Intermediate:
        (40 slots) 2:26040 | 3:26000 | 4:26000 | 0:25999 | 1:22061 | 5:22030 | 6:22000 | 7:22000 | 8:20000 | 9:20000 | 10:19000 | 11:18000 | 13:15000 | 14:15000 | 15:15000 | 18:13000 | 12:11029 | 19:11000 | 20:10500 | 21:10000 | 16:9014 | 22:9000 | 23:9000 | 17:8742 | 25:6000 | 24:5976 | 12:5971 | 1:5939 | 26:5000 | 16:4986 | 17:4758 | 0:4001 | 24:1524 | 28:1500 | 27:1320 | 27:1180 | 29:1000 | 13:1000 | 5:970 | 2:960
        Phenotype:
        Offset plates:
        |  0|  2|  3|  4|	 26040 printings
        |  1|  5|  6|  7|	 22061 printings
        |  8|  9| 10| 11|	 20000 printings
        | 13| 14| 15| 18|	 15000 printings
        | 12| 19| 20| 21|	 11029 printings
        | 16| 17| 22| 23|	  9014 printings
        |  1| 12| 24| 25|	  6000 printings
        |  0| 16| 17| 26|	  5000 printings
        | 24| 27| 27| 28|	  1524 printings
        |  2|  5| 13| 29|	  1000 printings
        Total printings: 116668
        Quality: 116668.0

        New better solution at iteration 380661:
        Genome:
        (20 genes) 0.08008227820171626 0.9600082706954156 | 0.9310842005527211 0.4059753381354475 | 0.05696413255904995 0.7852475993151662 | 0.5850531812955109 0.6349860734591818 | 0.1724024314810324 0.05886722479958775 | 0.6066400984506115 0.3613363367276051 | 0.8169154005589672 0.19038533477214836 | 0.0 0.15179000031325293 | 0.4172519843421011 0.35230192443034264 | 0.43365727228773465 0.0648125175537901
        Intermediate:
        (40 slots) 3:26000 | 4:26000 | 2:25920 | 0:25447 | 6:22000 | 7:22000 | 1:21986 | 5:21647 | 8:20000 | 9:20000 | 10:19000 | 11:18000 | 14:15000 | 15:15000 | 13:14963 | 16:14000 | 12:11011 | 19:11000 | 20:10500 | 21:10000 | 22:9000 | 23:9000 | 17:8572 | 18:8303 | 24:6073 | 1:6014 | 25:6000 | 12:5989 | 26:5000 | 17:4928 | 18:4697 | 0:4553 | 28:1500 | 27:1486 | 24:1427 | 5:1353 | 2:1080 | 13:1037 | 27:1014 | 29:1000
        Phenotype:
        Offset plates:
        |  0|  2|  3|  4|	 26000 printings
        |  1|  5|  6|  7|	 22000 printings
        |  8|  9| 10| 11|	 20000 printings
        | 13| 14| 15| 16|	 15000 printings
        | 12| 19| 20| 21|	 11011 printings
        | 17| 18| 22| 23|	  9000 printings
        |  1| 12| 24| 25|	  6073 printings
        |  0| 17| 18| 26|	  5000 printings
        |  5| 24| 27| 28|	  1500 printings
        |  2| 13| 27| 29|	  1080 printings
        Total printings: 116664
        Quality: 116664.0

        New better solution at iteration 3344845:
        Genome:
        (20 genes) 0.07302809580566302 0.036194126165955416 | 0.5933620007947359 0.34084551022552045 | 0.8325248919050126 0.18614806939559791 | 0.0 0.844196697979414 | 0.923741595175909 0.40710025852751525 | 0.41740981126450355 0.6449086586959838 | 0.6291893760647067 0.36926877357484894 | 0.04833104256397616 0.7819261978858563 | 0.4551104120417461 0.9337106647786584 | 0.17798467988892547 0.9557693909302974
        Intermediate:
        (40 slots) 2:26023 | 3:26000 | 4:26000 | 0:25325 | 6:22000 | 7:22000 | 5:21982 | 1:21893 | 8:20000 | 9:20000 | 10:19000 | 11:18000 | 14:15000 | 15:15000 | 13:14939 | 16:14000 | 19:11000 | 12:10963 | 20:10500 | 21:10000 | 22:9000 | 23:9000 | 17:8899 | 18:8200 | 1:6107 | 24:6104 | 12:6037 | 25:6000 | 26:5000 | 18:4800 | 0:4675 | 17:4601 | 28:1500 | 27:1483 | 24:1396 | 13:1061 | 5:1018 | 27:1017 | 29:1000 | 2:977
        Phenotype:
        Offset plates:
        |  0|  2|  3|  4|	 26023 printings
        |  1|  5|  6|  7|	 22000 printings
        |  8|  9| 10| 11|	 20000 printings
        | 13| 14| 15| 16|	 15000 printings
        | 12| 19| 20| 21|	 11000 printings
        | 17| 18| 22| 23|	  9000 printings
        |  1| 12| 24| 25|	  6107 printings
        |  0| 17| 18| 26|	  5000 printings
        | 13| 24| 27| 28|	  1500 printings
        |  2|  5| 27| 29|	  1018 printings
        Total printings: 116648
        Quality: 116648.0

        New better solution at iteration 216142:
        Genome:
        (20 genes) 0.09635180988995211 0.036179899222266276 | 0.6034964107992506 0.3793476309525191 | 0.5969505747304825 0.35183049522584253 | 0.041363755630589805 0.2137182674938444 | 0.4489612063042245 0.06423789888417661 | 0.8169698791079683 0.7984698810371044 | 0.912311198408419 0.39402009397009957 | 0.40438467396112465 0.3488704245986387 | 0.19289855509109596 0.043891136904007116 | 0.0 0.8620662823241282
        Intermediate:
        (40 slots) 2:26024 | 3:26000 | 4:26000 | 0:25861 | 1:22016 | 6:22000 | 7:22000 | 5:21991 | 8:20000 | 9:20000 | 10:19000 | 11:18000 | 14:15000 | 15:15000 | 13:14973 | 16:14000 | 12:11070 | 19:11000 | 20:10500 | 21:10000 | 22:9000 | 23:9000 | 17:8751 | 18:8069 | 25:6000 | 24:5988 | 1:5984 | 12:5930 | 26:5000 | 18:4931 | 17:4749 | 0:4139 | 27:1515 | 24:1512 | 28:1500 | 13:1027 | 5:1009 | 29:1000 | 27:985 | 2:976
        Phenotype:
        Offset plates:
        |  0|  2|  3|  4|	 26024 printings
        |  1|  5|  6|  7|	 22016 printings
        |  8|  9| 10| 11|	 20000 printings
        | 13| 14| 15| 16|	 15000 printings
        | 12| 19| 20| 21|	 11070 printings
        | 17| 18| 22| 23|	  9000 printings
        |  1| 12| 24| 25|	  6000 printings
        |  0| 17| 18| 26|	  5000 printings
        | 13| 24| 27| 28|	  1515 printings
        |  2|  5| 27| 29|	  1009 printings
        Total printings: 116634
        Quality: 116634.0

        New better solution at iteration 684709:
        Genome:
        (20 genes) 0.6208500963994911 0.3210525275887218 | 0.4616595867155857 0.07913420192628656 | 0.4155519780125023 0.6460277634060185 | 0.587898333120255 0.3426580656224655 | 0.9284811190847928 0.383888363569016 | 0.05082765824930734 0.21463218645645069 | 0.19981826002969172 0.04535444296326876 | 0.02709419542803227 0.8524463211387734 | 0.8231111473246199 0.2031004893505926 | 0.07526193074245803 0.960350056243921
        Intermediate:
        (40 slots) 3:26000 | 4:26000 | 2:25929 | 0:25573 | 6:22000 | 7:22000 | 1:21991 | 5:21957 | 8:20000 | 9:20000 | 10:19000 | 11:18000 | 14:15000 | 15:15000 | 13:14734 | 16:14000 | 19:11000 | 12:10982 | 20:10500 | 21:10000 | 22:9000 | 23:9000 | 17:8875 | 18:8827 | 12:6018 | 1:6009 | 25:6000 | 24:5977 | 26:5000 | 17:4625 | 0:4427 | 18:4173 | 27:1541 | 24:1523 | 28:1500 | 13:1266 | 2:1071 | 5:1043 | 29:1000 | 27:959
        Phenotype:
        Offset plates:
        |  0|  2|  3|  4|	 26000 printings
        |  1|  5|  6|  7|	 22000 printings
        |  8|  9| 10| 11|	 20000 printings
        | 13| 14| 15| 16|	 15000 printings
        | 12| 19| 20| 21|	 11000 printings
        | 17| 18| 22| 23|	  9000 printings
        |  1| 12| 24| 25|	  6018 printings
        |  0| 17| 18| 26|	  5000 printings
        | 13| 24| 27| 28|	  1541 printings
        |  2|  5| 27| 29|	  1071 printings
        Total printings: 116630
        Quality: 116630.0

        New better solution at iteration 2150534:
        Genome:
        (20 genes) 0.06149308675043892 0.07188913742022388 | 0.621256334459859 0.6709382412155973 | 0.0 0.8597391005808943 | 0.5965840579329555 0.6694165105156483 | 0.0715115712314843 0.18473207048559437 | 0.43773315853318684 0.6855684675766428 | 0.18344167533304578 0.05400685636362078 | 0.837084157170689 0.7515815991225744 | 0.41538474519293506 0.12906743727521586 | 0.8328953908453587 0.338295912382585
        Intermediate:
        (40 slots) 3:26000 | 4:26000 | 1:25988 | 0:25792 | 2:22013 | 6:22000 | 7:22000 | 5:21758 | 8:20000 | 9:20000 | 10:19000 | 11:18000 | 14:15000 | 15:15000 | 12:14806 | 16:14000 | 19:11000 | 13:10969 | 20:10500 | 21:10000 | 17:9037 | 22:9000 | 23:9000 | 18:8722 | 13:5031 | 26:5000 | 2:4987 | 24:4963 | 25:4509 | 17:4463 | 18:4278 | 0:4208 | 24:2537 | 27:2500 | 12:2194 | 1:2012 | 28:1500 | 25:1491 | 5:1242 | 29:1000
        Phenotype:
        Offset plates:
        |  0|  1|  3|  4|	 26000 printings
        |  2|  5|  6|  7|	 22013 printings
        |  8|  9| 10| 11|	 20000 printings
        | 12| 14| 15| 16|	 15000 printings
        | 13| 19| 20| 21|	 11000 printings
        | 17| 18| 22| 23|	  9037 printings
        |  2| 13| 24| 26|	  5031 printings
        |  0| 17| 18| 25|	  4509 printings
        |  1| 12| 24| 27|	  2537 printings
        |  5| 25| 28| 29|	  1500 printings
        Total printings: 116627
        Quality: 116627.0

        New better solution at iteration 8:
        Genome:
        (20 genes) 0.4523998414248147 0.9389578712598525 | 0.058884200716198 0.7846955487486523 | 0.4188556661431178 0.6424821958237921 | 0.0 0.16050740944735326 | 0.18446285931050171 0.956651829129133 | 0.09780997257001331 0.054760201890117106 | 0.6216186137083168 0.6586572962679074 | 0.8317255982746368 0.802506710634693 | 0.5841757464304131 0.34702540142473337 | 0.9279981235325093 0.5971174490595882
        Intermediate:
        (40 slots) 3:26000 | 4:26000 | 2:25522 | 0:25185 | 5:22002 | 6:22000 | 7:22000 | 1:21971 | 8:20000 | 9:20000 | 10:19000 | 11:18000 | 13:15023 | 14:15000 | 15:15000 | 16:14000 | 19:11000 | 12:10922 | 20:10500 | 21:10000 | 22:9000 | 23:9000 | 17:8816 | 18:8562 | 12:6078 | 1:6029 | 24:6018 | 25:6000 | 26:5000 | 0:4815 | 17:4684 | 18:4438 | 28:1500 | 27:1492 | 24:1482 | 2:1478 | 27:1008 | 29:1000 | 5:998 | 13:977
        Phenotype:
        Offset plates:
        |  0|  2|  3|  4|	 26000 printings
        |  1|  5|  6|  7|	 22002 printings
        |  8|  9| 10| 11|	 20000 printings
        | 13| 14| 15| 16|	 15023 printings
        | 12| 19| 20| 21|	 11000 printings
        | 17| 18| 22| 23|	  9000 printings
        |  1| 12| 24| 25|	  6078 printings
        |  0| 17| 18| 26|	  5000 printings
        |  2| 24| 27| 28|	  1500 printings
        |  5| 13| 27| 29|	  1008 printings
        Total printings: 116611
        Quality: 116611.0

        New better solution at iteration 3:
        Genome:
        (20 genes) 0.4523998414248147 0.9389578712598525 | 0.058884200716198 0.7846955487486523 | 0.4144828798613099 0.6487966882282851 | 0.0107431072873009 0.13741277694994622 | 0.17602064804025905 0.956651829129133 | 0.09780997257001331 0.054760201890117106 | 0.6094506215332589 0.6327844864105917 | 0.8317255982746368 0.802506710634693 | 0.5732071046079863 0.35118882112506056 | 0.9169999180002679 0.5971174490595882
        Intermediate:
        (40 slots) 3:26000 | 4:26000 | 0:25878 | 2:25522 | 5:22002 | 6:22000 | 7:22000 | 1:21971 | 8:20000 | 9:20000 | 10:19000 | 11:18000 | 13:15023 | 14:15000 | 15:15000 | 16:14000 | 12:11029 | 19:11000 | 20:10500 | 21:10000 | 22:9000 | 23:9000 | 17:8759 | 18:8226 | 1:6029 | 24:6018 | 25:6000 | 12:5971 | 26:5000 | 18:4774 | 17:4741 | 0:4122 | 28:1500 | 27:1492 | 24:1482 | 2:1478 | 27:1008 | 29:1000 | 5:998 | 13:977
        Phenotype:
        Offset plates:
        |  0|  2|  3|  4|	 26000 printings
        |  1|  5|  6|  7|	 22002 printings
        |  8|  9| 10| 11|	 20000 printings
        | 13| 14| 15| 16|	 15023 printings
        | 12| 19| 20| 21|	 11029 printings
        | 17| 18| 22| 23|	  9000 printings
        |  1| 12| 24| 25|	  6029 printings
        |  0| 17| 18| 26|	  5000 printings
        |  2| 24| 27| 28|	  1500 printings
        |  5| 13| 27| 29|	  1008 printings
        Total printings: 116591
        Quality: 116591.0

        New better solution at iteration 19:
        Genome:
        (20 genes) 0.4523998414248147 0.9376077986500537 | 0.036281108297296596 0.7846955487486523 | 0.4031339473948262 0.6446603823361275 | 0.0 0.15931665212045312 | 0.17771210920933486 0.956651829129133 | 0.09828921905944038 0.046630757822145714 | 0.6094506215332589 0.6497000621615632 | 0.817582187642525 0.802506710634693 | 0.5864104608991463 0.3613497705860728 | 0.9319884757287605 0.5918871197410966
        Intermediate:
        (40 slots) 3:26000 | 4:26000 | 2:25741 | 0:25221 | 5:22002 | 6:22000 | 7:22000 | 1:21971 | 8:20000 | 9:20000 | 10:19000 | 11:18000 | 13:15001 | 14:15000 | 15:15000 | 16:14000 | 19:11000 | 12:10959 | 20:10500 | 21:10000 | 22:9000 | 23:9000 | 17:8622 | 18:8446 | 12:6041 | 1:6029 | 24:6018 | 25:6000 | 26:5000 | 17:4878 | 0:4779 | 18:4554 | 28:1500 | 24:1482 | 27:1479 | 2:1259 | 27:1021 | 29:1000 | 13:999 | 5:998
        Phenotype:
        Offset plates:
        |  0|  2|  3|  4|	 26000 printings
        |  1|  5|  6|  7|	 22002 printings
        |  8|  9| 10| 11|	 20000 printings
        | 13| 14| 15| 16|	 15001 printings
        | 12| 19| 20| 21|	 11000 printings
        | 17| 18| 22| 23|	  9000 printings
        |  1| 12| 24| 25|	  6041 printings
        |  0| 17| 18| 26|	  5000 printings
        |  2| 24| 27| 28|	  1500 printings
        |  5| 13| 27| 29|	  1021 printings
        Total printings: 116565
        Quality: 116565.0

        New better solution at iteration 1:
        Genome:
        (20 genes) 0.4523998414248147 0.9376077986500537 | 0.036281108297296596 0.7846955487486523 | 0.4031339473948262 0.6446603823361275 | 0.0 0.15931665212045312 | 0.17771210920933486 0.956651829129133 | 0.09828921905944038 0.046630757822145714 | 0.6094506215332589 0.6497000621615632 | 0.817582187642525 0.802506710634693 | 0.5864104608991463 0.3613497705860728 | 0.9085166015967897 0.6035127514832047
        Intermediate:
        (40 slots) 3:26000 | 4:26000 | 2:25741 | 0:25221 | 5:22002 | 6:22000 | 7:22000 | 1:21971 | 8:20000 | 9:20000 | 10:19000 | 11:18000 | 13:15001 | 14:15000 | 15:15000 | 16:14000 | 19:11000 | 12:10959 | 20:10500 | 21:10000 | 22:9000 | 23:9000 | 17:8622 | 18:8446 | 12:6041 | 1:6029 | 24:6018 | 25:6000 | 26:5000 | 17:4878 | 0:4779 | 18:4554 | 27:1508 | 28:1500 | 24:1482 | 2:1259 | 29:1000 | 13:999 | 5:998 | 27:992
        Phenotype:
        Offset plates:
        |  0|  2|  3|  4|	 26000 printings
        |  1|  5|  6|  7|	 22002 printings
        |  8|  9| 10| 11|	 20000 printings
        | 13| 14| 15| 16|	 15001 printings
        | 12| 19| 20| 21|	 11000 printings
        | 17| 18| 22| 23|	  9000 printings
        |  1| 12| 24| 25|	  6041 printings
        |  0| 17| 18| 26|	  5000 printings
        |  2| 24| 27| 28|	  1508 printings
        |  5| 13| 27| 29|	  1000 printings
        Total printings: 116552
        Quality: 116552.0
         */
    }
}