package com.vanolucas.cppsolver.v12.algo.listener;

import com.vanolucas.cppsolver.v12.algo.Opti;
import com.vanolucas.cppsolver.v12.algo.OptiStep;
import com.vanolucas.cppsolver.v12.algo.SteepestAscentHillClimbingOptiStep;
import com.vanolucas.cppsolver.v12.clone.ComplexGeneticSolutionCloner;
import com.vanolucas.cppsolver.v12.clone.GenomeCloner;
import com.vanolucas.cppsolver.v12.clone.SolutionCloner;
import com.vanolucas.cppsolver.v12.eval.*;
import com.vanolucas.cppsolver.v12.genome.RandomKeyVector;
import com.vanolucas.cppsolver.v12.mutation.CompositeSolutionMutator;
import com.vanolucas.cppsolver.v12.mutation.GeneticSolutionMutator;
import com.vanolucas.cppsolver.v12.mutation.RandomizeGenomeMutator;
import com.vanolucas.cppsolver.v12.mutation.SolutionMutator;
import com.vanolucas.cppsolver.v12.mutation.rkv.AddRandValueKeyRkvMutator;
import com.vanolucas.cppsolver.v12.mutation.rkv.ConsecutiveKeysRkvMutator;
import com.vanolucas.cppsolver.v12.mutation.rkv.ProbaKeysRkvMutator;
import com.vanolucas.cppsolver.v12.mutation.rkv.RandomizeKeyRkvMutator;
import com.vanolucas.cppsolver.v12.problem.cpp.eval.CppSolutionEvaluationFunction;
import com.vanolucas.cppsolver.v12.problem.cpp.eval.GenomeCfDecoder;
import com.vanolucas.cppsolver.v12.problem.cpp.eval.SlotsDecoder;
import com.vanolucas.cppsolver.v12.problem.cpp.genome.GenomeCf;
import com.vanolucas.cppsolver.v12.problem.cpp.instance.CppInstance;
import com.vanolucas.cppsolver.v12.problem.cpp.instance.CppInstanceI013;
import com.vanolucas.cppsolver.v12.quality.DoubleCost;
import com.vanolucas.cppsolver.v12.rand.GenomeRandomizer;
import com.vanolucas.cppsolver.v12.rand.Rand;
import com.vanolucas.cppsolver.v12.solution.ComplexGeneticSolution;
import com.vanolucas.cppsolver.v12.solution.Solution;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

class BetterSolutionPrinterTest {

    @Test
    void cpp() throws IOException {
        // CPP instance
        final CppInstance cppInstance = new CppInstanceI013();

        // opti algo
        final Opti opti = new Opti();

        // initial solution
        final GenomeCf genome = new GenomeCf(cppInstance);
        final Solution initialSolution = new ComplexGeneticSolution(genome, 1);

        // evaluation
        final int nbThreadsEvaluator = 1;
        final Function genomeDecoder = new GenomeCfDecoder(cppInstance);
        final Function intermediateDecoder = new SlotsDecoder(cppInstance);
        final EvaluationFunction evalFn = new CppSolutionEvaluationFunction(cppInstance);
        final SolutionEvaluationFunction solutionEvalFn = new ComplexGeneticSolutionEvaluationFunction(
                genomeDecoder,
                Collections.singletonList(intermediateDecoder),
                evalFn
        );
        final Evaluator evaluator = new ParallelEvaluator(solutionEvalFn, nbThreadsEvaluator);

        // cloner
        final GenomeCloner genomeCloner = GenomeCloner.getDefault();
        final SolutionCloner solutionCloner = new ComplexGeneticSolutionCloner(genomeCloner);

        // mutators
        final Rand rand = new Rand();
        final List<SolutionMutator> solutionMutators = getMutators03(genome, rand);

        // hill climbing algo
        final int nbThreadsHillClimbing = 8;
        final OptiStep step = new SteepestAscentHillClimbingOptiStep(
                opti,
                evaluator,
                initialSolution,
                solutionCloner,
                solutionMutators,
                nbThreadsHillClimbing
        );

        // stop criterion
        final MinDurationPauser pauser = new MinDurationPauser(opti, Duration.ofSeconds(3L));

        // print better solutions
        new BetterSolutionPrinter(opti, new DoubleCost(Math.nextDown(118000d)));

        // run opti
        opti.run();
    }

    private List<SolutionMutator> getMutators03(RandomKeyVector genome, Rand rand) {
        final List<SolutionMutator> solutionMutators = new ArrayList<>();
        // randomize whole genome
        solutionMutators.addAll(Collections.nCopies(1, new GeneticSolutionMutator(
                new RandomizeGenomeMutator(GenomeRandomizer.getDefault(rand))
        )));
        // randomize 1 random key then 1 again
        solutionMutators.addAll(Collections.nCopies(3, new CompositeSolutionMutator(
                new GeneticSolutionMutator(new ConsecutiveKeysRkvMutator(1,
                        new RandomizeKeyRkvMutator(rand),
                        rand)),
                new GeneticSolutionMutator(new ConsecutiveKeysRkvMutator(1,
                        new RandomizeKeyRkvMutator(rand),
                        rand))
        )));
        // proba to add rand value to each key
        solutionMutators.addAll(Collections.nCopies(3, new GeneticSolutionMutator(
                new ProbaKeysRkvMutator(2d / genome.length(),
                        new AddRandValueKeyRkvMutator(-0.05, +0.05, rand),
                        rand)
        )));
        // add rand value to 2 consecutive keys
        solutionMutators.addAll(Collections.nCopies(3, new GeneticSolutionMutator(
                new ConsecutiveKeysRkvMutator(2,
                        new AddRandValueKeyRkvMutator(-0.05, +0.05, rand),
                        rand)
        )));
        return solutionMutators;
    }
}