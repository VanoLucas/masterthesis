package com.vanolucas.cppsolver.v12.draft;

import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public class FutureTest {

    @Test
    void future() {
        class Task implements Supplier<String> {
            private int id;

            private Task(int id) {
                this.id = id;
            }

            @Override
            public String get() {
                log("task started " + id);
                try {
                    TimeUnit.SECONDS.sleep(1L);
                } catch (InterruptedException e) {
                    throw new IllegalStateException();
                }
                return "result " + id;
            }
        }

        ExecutorService executor = Executors.newFixedThreadPool(2);

        log("create future 1");
        CompletableFuture<Void> future1 = CompletableFuture
                .supplyAsync(new Task(1), executor)
                .thenAccept(this::log);
        log("future created 1");

        log("create future 2");
        CompletableFuture<Void> future2 = CompletableFuture
                .supplyAsync(new Task(2), executor)
                .thenAccept(this::log);
        log("future created 2");

        log("create future 3");
        CompletableFuture<Void> future3 = CompletableFuture
                .supplyAsync(new Task(3), executor)
                .thenAccept(this::log);
        log("future created 3");

        CompletableFuture<Void> allDone = CompletableFuture
                .allOf(future1, future2, future3)
                .thenRun(() -> log("all done"));

        allDone.join();

        try {
            TimeUnit.SECONDS.sleep(5L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    
    private void log(String msg) {
        System.out.println(Instant.now().toString() + " " + msg);
    }
}
