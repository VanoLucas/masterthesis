package com.vanolucas.cppsolver.v12.eval;

import com.vanolucas.cppsolver.v12.problem.targetnb.TargetNumberProblem;
import com.vanolucas.cppsolver.v12.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.v12.solution.Solution;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ParallelEvaluatorTest {

    @Test
    void parallelEvaluation() {
        final TargetNumberProblem problem = new TargetNumberProblem();

        final ParallelEvaluator evaluator = new ParallelEvaluator(problem.newSolutionEvaluationFunction(), 2);

        final List<Solution> solutions = Collections.nCopies(3, problem.newSolution());
        final List<EvaluatedSolution> evaluatedSolutions = evaluator.evaluate(solutions);
    }
}