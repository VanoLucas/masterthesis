package com.vanolucas.cppsolver.v12.mutation.rkv;

import com.vanolucas.cppsolver.v12.genome.RandomKeyVector;
import com.vanolucas.cppsolver.v12.rand.Rand;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ProbaKeysRkvMutatorTest {

    @Test
    void mutate() {
        final int nbRuns = 10000;

        final Rand rand = new Rand();
        final KeyRkvMutator keyMutator = new RandomizeKeyRkvMutator(rand);
        final ProbaKeysRkvMutator rkvMutator = new ProbaKeysRkvMutator(0.5d, keyMutator, rand);

        final List<Integer> countModifiedGenes = new ArrayList<>(nbRuns);
        for (int i = 0; i < nbRuns; i++) {
            final RandomKeyVector rkv = new RandomKeyVector(10, 0d);
//        System.out.println(rkv);
            rkv.mutateUsing(rkvMutator);
//        System.out.println(rkv);
            countModifiedGenes.add((int) rkv.doubleStream()
                    .filter(k -> k != 0d)
                    .count()
            );
        }

        System.out.println(String.format("Average modified genes: %f",
                countModifiedGenes.stream()
                        .mapToDouble(__ -> __)
                        .average()
                        .orElse(0d)
        ));
    }
}