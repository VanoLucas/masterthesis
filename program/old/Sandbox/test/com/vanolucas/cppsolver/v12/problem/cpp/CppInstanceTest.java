package com.vanolucas.cppsolver.v12.problem.cpp;

import com.vanolucas.cppsolver.v12.problem.cpp.instance.CppInstance;
import org.junit.jupiter.api.Test;

import java.io.IOException;

class CppInstanceTest {

    @Test
    void loadCoversFromInstance() throws IOException {
        CppInstance cppInstance = new CppInstance("I013", 10);
        System.out.println(cppInstance);

        cppInstance = new CppInstance("data10-1", 10);
        System.out.println(cppInstance);
    }
}