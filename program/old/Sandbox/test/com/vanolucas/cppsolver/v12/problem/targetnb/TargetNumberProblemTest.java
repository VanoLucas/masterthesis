package com.vanolucas.cppsolver.v12.problem.targetnb;

import com.vanolucas.cppsolver.v12.algo.Opti;
import com.vanolucas.cppsolver.v12.algo.OptiStep;
import com.vanolucas.cppsolver.v12.algo.SingleMutationOptiStep;
import com.vanolucas.cppsolver.v12.eval.SolutionEvaluationFunction;
import com.vanolucas.cppsolver.v12.mutation.GeneticSolutionMutator;
import com.vanolucas.cppsolver.v12.mutation.GenomeMutator;
import com.vanolucas.cppsolver.v12.mutation.RandomizeGenomeMutator;
import com.vanolucas.cppsolver.v12.mutation.SolutionMutator;
import com.vanolucas.cppsolver.v12.quality.IntCost;
import com.vanolucas.cppsolver.v12.rand.Rand;
import com.vanolucas.cppsolver.v12.solution.Solution;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TargetNumberProblemTest {

    @Test
    void singleMutationSearch() {
        // target number problem
        final TargetNumberProblem problem = new TargetNumberProblem(
                100,
                2,
                100
        );

        // create opti algo
        final Opti opti = new Opti();

        // evaluation function
        final SolutionEvaluationFunction solutionEvalFn = problem.newSolutionEvaluationFunction();

        // initial solution
        final Solution initialSolution = problem.newSolution();

        // mutation operator
        final Rand rand = new Rand();
        final GenomeMutator genomeMutator = RandomizeGenomeMutator.getDefault(rand);
        final SolutionMutator solutionMutator = new GeneticSolutionMutator(genomeMutator);

        // single mutation search algo
        final OptiStep step = new SingleMutationOptiStep(
                opti,
                solutionEvalFn,
                initialSolution,
                solutionMutator
        );

        // set listeners
        opti.addOnEvaluatedListener((solution, iteration) -> {
            // print evaluated solution
            System.out.println(String.format("%nEvaluated at iteration %d:%n" +
                            "%s",
                    iteration,
                    solution
            ));

            // stop when target reached
            if (solution.isBetterOrEquivalentTo(IntCost.zero())) {
                opti.pauseBeforeNextIteration();
            }
        });

        // run opti
        opti.run();
    }
}