package com.vanolucas.cppsolver.clone;

public interface Cloneable extends java.lang.Cloneable {
    default Cloneable newClone() {
        try {
            return clone();
        } catch (CloneNotSupportedException e) {
            throw new UnsupportedOperationException(String.format("Clone not supported in %s.",
                    getClass().getSimpleName()
            ));
        }
    }

    Cloneable clone() throws CloneNotSupportedException;
}
