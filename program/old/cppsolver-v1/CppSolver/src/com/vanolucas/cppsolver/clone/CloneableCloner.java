package com.vanolucas.cppsolver.clone;

public class CloneableCloner implements Cloner<Cloneable> {
    @Override
    public Cloneable clone(Cloneable cloneable) {
        return cloneable.newClone();
    }
}
