package com.vanolucas.cppsolver.clone;

public interface Cloner<T> {
    T clone(T toClone);
}
