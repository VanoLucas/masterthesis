package com.vanolucas.cppsolver.clone;

import com.vanolucas.cppsolver.solution.EncodedSolution;
import com.vanolucas.cppsolver.solution.Solution;

import java.util.Collections;
import java.util.List;

public class EncodedSolutionCloner implements SolutionCloner {
    private final List<Cloner> encodedRepresentationCloners;
    private final Cloner phenotypeCloner;

    public EncodedSolutionCloner(Cloner firstEncodedRepresentationCloner) {
        this(Collections.singletonList(firstEncodedRepresentationCloner));
    }

    public EncodedSolutionCloner(List<Cloner> encodedRepresentationCloners) {
        this(encodedRepresentationCloners, null);
    }

    public EncodedSolutionCloner(List<Cloner> encodedRepresentationCloners, Cloner phenotypeCloner) {
        this.encodedRepresentationCloners = encodedRepresentationCloners;
        this.phenotypeCloner = phenotypeCloner;
    }

    @Override
    public Solution clone(Solution solution) {
        return clone((EncodedSolution) solution);
    }

    public EncodedSolution clone(EncodedSolution solution) {
        if (encodedRepresentationCloners == null || encodedRepresentationCloners.isEmpty()) {
            return solution.clone(phenotypeCloner);
        } else if (phenotypeCloner == null) {
            return solution.clone(encodedRepresentationCloners);
        } else {
            return solution.clone(
                    encodedRepresentationCloners,
                    phenotypeCloner
            );
        }
    }
}
