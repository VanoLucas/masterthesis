package com.vanolucas.cppsolver.clone;

import com.vanolucas.cppsolver.solution.Solution;

public class PhenotypeOnlySolutionCloner implements SolutionCloner {
    private final Cloner phenotypeCloner;

    public PhenotypeOnlySolutionCloner(Cloner phenotypeCloner) {
        this.phenotypeCloner = phenotypeCloner;
    }

    @Override
    public Solution clone(Solution solution) {
        return solution.clone(phenotypeCloner);
    }
}
