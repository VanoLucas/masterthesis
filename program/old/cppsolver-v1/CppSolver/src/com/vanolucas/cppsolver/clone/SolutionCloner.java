package com.vanolucas.cppsolver.clone;

import com.vanolucas.cppsolver.solution.Solution;

/**
 * Produces a clone of a solution.
 */
public interface SolutionCloner {
    Solution clone(Solution solution);
}
