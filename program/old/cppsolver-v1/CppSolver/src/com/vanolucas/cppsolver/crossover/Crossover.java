package com.vanolucas.cppsolver.crossover;

import com.vanolucas.cppsolver.solution.EvaluatedPopulation;
import com.vanolucas.cppsolver.solution.Population;

public interface Crossover {
    Population makeChildren(EvaluatedPopulation parents);
}
