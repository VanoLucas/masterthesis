package com.vanolucas.cppsolver.crossover;

import com.vanolucas.cppsolver.rand.Rand;
import com.vanolucas.cppsolver.rkv.RandomKeyVector;
import com.vanolucas.cppsolver.solution.EncodedSolution;
import com.vanolucas.cppsolver.solution.Solution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RkvUniformCrossover extends UniformCrossover {
    public RkvUniformCrossover() {
        super();
    }

    public RkvUniformCrossover(double mixingRatio) {
        super(mixingRatio);
    }

    public RkvUniformCrossover(double mixingRatio, Rand rand) {
        super(mixingRatio, rand);
    }

    @Override
    public List<Solution> uniformCrossover(Solution parent1, Solution parent2) {
        EncodedSolution encodedParent1 = (EncodedSolution) parent1;
        EncodedSolution encodedParent2 = (EncodedSolution) parent2;
        List<RandomKeyVector> childrenRkv = uniformCrossover(
                (RandomKeyVector) encodedParent1.getEncodedRepresentation(0),
                (RandomKeyVector) encodedParent2.getEncodedRepresentation(0)
        );
        return Arrays.asList(
                new EncodedSolution(encodedParent1.getEncodedRepresentationsSize(), childrenRkv.get(0)),
                new EncodedSolution(encodedParent2.getEncodedRepresentationsSize(), childrenRkv.get(1))
        );
    }

    public List<RandomKeyVector> uniformCrossover(RandomKeyVector parent1, RandomKeyVector parent2) {
        if (parent1.size() != parent2.size()) {
            throw new IllegalArgumentException("Cannot perform uniform crossover on two random key vectors that are not of the same length.");
        }

        final int genomeLen = parent1.size();

        final List<Double> decision = rand.doubles01(genomeLen);

        List<Double> keysChild1 = new ArrayList<>(genomeLen);
        List<Double> keysChild2 = new ArrayList<>(genomeLen);

        for (int k = 0; k < genomeLen; k++) {
            if (decision.get(k) < mixingRatio) {
                keysChild1.add(parent2.getKey(k));
                keysChild2.add(parent1.getKey(k));
            } else {
                keysChild1.add(parent1.getKey(k));
                keysChild2.add(parent2.getKey(k));
            }
        }

        RandomKeyVector child1 = (RandomKeyVector) parent1.newClone();
        RandomKeyVector child2 = (RandomKeyVector) parent2.newClone();

        child1.setKeyValues(keysChild1);
        child2.setKeyValues(keysChild2);

        return Arrays.asList(
                child1, child2
        );
    }
}
