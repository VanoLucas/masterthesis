package com.vanolucas.cppsolver.crossover;

import com.vanolucas.cppsolver.rand.Rand;
import com.vanolucas.cppsolver.solution.EvaluatedPopulation;
import com.vanolucas.cppsolver.solution.Population;
import com.vanolucas.cppsolver.solution.Solution;

import java.util.List;

public abstract class UniformCrossover implements Crossover {
    protected final double mixingRatio;
    protected final Rand rand;

    public UniformCrossover() {
        this(0.5d);
    }

    public UniformCrossover(double mixingRatio) {
        this(mixingRatio, new Rand());
    }

    public UniformCrossover(double mixingRatio, Rand rand) {
        this.mixingRatio = mixingRatio;
        this.rand = rand;
    }

    @Override
    public Population makeChildren(EvaluatedPopulation parents) {
        return uniformCrossover(parents);
    }

    public Population uniformCrossover(EvaluatedPopulation parents) {
        return new Population(
                uniformCrossover(
                        parents.get(0).getSolution(),
                        parents.get(1).getSolution()
                )
        );
    }

    public abstract List<Solution> uniformCrossover(Solution parent1, Solution parent2);
}
