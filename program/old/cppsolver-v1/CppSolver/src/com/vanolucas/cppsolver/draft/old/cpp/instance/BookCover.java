package com.vanolucas.cppsolver.draft.old.cpp.instance;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Collectors;

public class BookCover implements Comparable<BookCover> {
    private final int id;
    private final int demand;

    public BookCover(int id, int demand) {
        // set id
        this.id = id;

        // set demand
        if (demand < 0) {
            throw new IllegalArgumentException("The demand for a book cover cannot be negative.");
        }
        this.demand = demand;
    }

    public int getId() {
        return id;
    }

    public static List<BookCover> loadFromFile(File file) throws IOException {
        // parse each line of the file
        return Files.lines(file.toPath())
                .map(line -> {
                    String[] fields = line.split(" ");
                    int id = Integer.parseInt(fields[0].trim());
                    int demand = Integer.parseInt(fields[1].trim());
                    // detect if the book cover id and demand columns are inverted
                    if (id > 150) {
                        // swap id with demand
                        int tmp = id;
                        id = demand;
                        demand = tmp;
                    }
                    return new BookCover(id, demand);
                })
                .collect(Collectors.toList());
    }

    @Override
    public int compareTo(BookCover other) {
        return Integer.compare(id, other.id);
    }

    @Override
    public String toString() {
        return String.format("%3d: %6d copies",
                id,
                demand
        );
    }
}
