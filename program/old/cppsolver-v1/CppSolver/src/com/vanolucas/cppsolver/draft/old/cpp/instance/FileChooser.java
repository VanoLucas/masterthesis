package com.vanolucas.cppsolver.draft.old.cpp.instance;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Paths;

public class FileChooser {
    private static final String LUCAS_HOSTNAME = "lucas-pc";
    private static final String LUCAS_CPP_INSTANCES_PATH = "/lucas/Lucas/Travail/UMons/Master_2/Memoire/git/instances/";

    public static File chooseFile() {
        final JFileChooser jfc = new JFileChooser(getInitialDir());

        final int returnValue = jfc.showOpenDialog(null);

        if (returnValue == JFileChooser.APPROVE_OPTION) {
            return jfc.getSelectedFile();
        } else {
            return null;
        }
    }

    private static File getInitialDir() {
        // detect hostname
        String hostname;
        try {
            hostname = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            hostname = "";
        }

        // if lucas host, the initial dir is the known path to cpp instances folder
        if (hostname.equals(LUCAS_HOSTNAME)) {
            return Paths.get(LUCAS_CPP_INSTANCES_PATH).toFile();
        } else {
            return FileSystemView.getFileSystemView().getHomeDirectory();
        }
    }
}
