package com.vanolucas.cppsolver.draft.old.cpp.instance;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GeneralCpp {
    private static final int DEFAULT_SLOTS_PER_PLATE = 4;

    private final List<BookCover> demand;
    private final int slotsPerPlate;
    private final double plateCost;
    private final double printingCost;

    public GeneralCpp(List<BookCover> demand, double plateCost, double printingCost) {
        this(demand, DEFAULT_SLOTS_PER_PLATE, plateCost, printingCost);
    }

    public GeneralCpp(List<BookCover> demand, int slotsPerPlate, double plateCost, double printingCost) {
        // set book cover demand
        if (demand == null || demand.isEmpty()) {
            throw new IllegalArgumentException("The demand cannot be empty. We must at least have a request for 1 book cover.");
        }
        this.demand = new ArrayList<>(demand);

        // set number of slots per offset plate
        if (slotsPerPlate < 2) {
            throw new IllegalArgumentException("The number of slots per offset plate must be at least 2 in a cover printing problem instance.");
        }
        this.slotsPerPlate = slotsPerPlate;

        // set costs
        this.plateCost = plateCost;
        this.printingCost = printingCost;
    }

    public int getBookCoverCount() {
        return demand.size();
    }

    @Override
    public String toString() {
        return String.format("Demand for m=%d book covers:%n" +
                        "%s%n" +
                        "S=%d slots per offset plate%n" +
                        "Cost of one offset plate (C1): %f%n" +
                        "Cost of one printing (C2): %f",
                getBookCoverCount(),
                demand.stream()
                        .sorted()
                        .map(Object::toString)
                        .collect(Collectors.joining(System.lineSeparator())),
                slotsPerPlate,
                plateCost,
                printingCost
        );
    }
}
