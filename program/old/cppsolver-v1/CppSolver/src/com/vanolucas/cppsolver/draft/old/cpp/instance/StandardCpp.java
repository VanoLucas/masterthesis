package com.vanolucas.cppsolver.draft.old.cpp.instance;

import java.util.List;

public class StandardCpp extends GeneralCpp {
    private static final double DEFAULT_PLATE_COST = 0d;
    private static final double DEFAULT_PRINTING_COST = 1d;

    private final int plateCount;

    public StandardCpp(List<BookCover> demand, int plateCount, int slotsPerPlate) {
        this(demand, plateCount, slotsPerPlate, DEFAULT_PLATE_COST);
    }

    public StandardCpp(List<BookCover> demand, int plateCount, int slotsPerPlate, double plateCost) {
        this(demand, plateCount, slotsPerPlate, plateCost, DEFAULT_PRINTING_COST);
    }

    public StandardCpp(List<BookCover> demand, int plateCount, int slotsPerPlate, double plateCost, double printingCost) {
        // set book cover demand and costs
        super(demand, slotsPerPlate, plateCost, printingCost);

        // set number of offset plates
        if (plateCount < 1) {
            throw new IllegalArgumentException("There must be at least one offset plate in a standard cover printing problem instance.");
        }
        this.plateCount = plateCount;
    }

    @Override
    public String toString() {
        return String.format("%s%n" +
                        "Number of offset plates n=%d",
                super.toString(),
                plateCount
        );
    }
}
