package com.vanolucas.cppsolver.draft.old.cpp.solution;

import com.vanolucas.cppsolver.draft.old.cpp.instance.BookCover;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Plate {
    private final List<BookCover> bookCovers;

    public Plate(List<BookCover> bookCovers) {
        this.bookCovers = new ArrayList<>(bookCovers);
    }

    @Override
    public String toString() {
        return bookCovers.stream()
                .map(bookCover -> String.valueOf(bookCover.getId()))
                .collect(Collectors.joining(" | ", "| ", " |"));
    }
}
