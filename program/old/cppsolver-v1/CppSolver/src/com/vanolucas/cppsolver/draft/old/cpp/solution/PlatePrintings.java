package com.vanolucas.cppsolver.draft.old.cpp.solution;

import com.vanolucas.cppsolver.draft.old.cpp.instance.BookCover;

import java.util.List;

public class PlatePrintings extends Plate {
    private final int printingCount;

    public PlatePrintings(List<BookCover> bookCovers, int printingCount) {
        super(bookCovers);
        this.printingCount = printingCount;
    }

    @Override
    public String toString() {
        return String.format("%s: %6d printings",
                super.toString(),
                printingCount
        );
    }
}
