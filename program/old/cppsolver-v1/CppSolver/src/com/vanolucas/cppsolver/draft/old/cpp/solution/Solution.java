package com.vanolucas.cppsolver.draft.old.cpp.solution;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Solution {
    private final List<PlatePrintings> plates;

    public Solution(List<PlatePrintings> plates) {
        this.plates = new ArrayList<>(plates);
    }

    @Override
    public String toString() {
        return plates.stream()
                .map(Object::toString)
                .collect(Collectors.joining(System.lineSeparator()));
    }
}
