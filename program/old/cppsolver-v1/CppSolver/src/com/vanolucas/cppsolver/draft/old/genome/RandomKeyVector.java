package com.vanolucas.cppsolver.draft.old.genome;

import com.vanolucas.cppsolver.draft.old.math.Scale;
import com.vanolucas.cppsolver.draft.old.math.range.Range;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class RandomKeyVector {
    private final List<Double> keys;

    public RandomKeyVector(List<Double> keys) {
        if (keys.stream()
                .anyMatch(key -> key < 0d || key >= 1d)) {
            throw new IllegalArgumentException("All keys of a random key vector must be in the range [0;1[.");
        }
        this.keys = new ArrayList<>(keys);
    }

    public double getScaledKey(int index, Range targetRange) {
        return Scale.scaleFrom01(keys.get(index), targetRange);
    }

    public List<Double> getScaledKeys(List<Range> targetRanges) {
        return Scale.scaleFrom01(keys, targetRanges);
    }

    @Override
    public String toString() {
        return keys.stream()
                .map(Object::toString)
                .collect(Collectors.joining(";"));
    }
}
