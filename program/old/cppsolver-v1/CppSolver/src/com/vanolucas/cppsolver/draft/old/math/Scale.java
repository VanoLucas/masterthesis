package com.vanolucas.cppsolver.draft.old.math;

import com.vanolucas.cppsolver.draft.old.math.range.Range;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Scale {
    /**
     * Scale linearly a value from source range to destination range.
     *
     * @param value Value in src range to convert to dst range.
     * @return The value translated to the target range.
     */
    public static double scale(final double value, final double srcMin, final double srcMax, final double dstMin, final double dstMax) {
        return ((dstMax - dstMin) * (value - srcMin) / (srcMax - srcMin)) + dstMin;
    }

    public static double scaleFrom01(final double value, final double dstMin, final double dstMax) {
        return (dstMax - dstMin) * value + dstMin;
    }

    public static double scaleFrom01(final double value, final Range targetRange) {
        return scaleFrom01(value, targetRange.getMin(), targetRange.getMax());
    }

    public static List<Double> scaleFrom01(final List<Double> values, final List<Range> targetRanges) {
        final int size = values.size();
        if (size != targetRanges.size()) {
            throw new IllegalArgumentException("There must be as many target ranges as values provided for the multiple scale operation to work.");
        }
        return IntStream.range(0, size)
                .mapToObj(i -> scaleFrom01(values.get(i), targetRanges.get(i)))
                .collect(Collectors.toList());
    }
}
