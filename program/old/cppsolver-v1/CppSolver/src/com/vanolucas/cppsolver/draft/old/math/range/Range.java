package com.vanolucas.cppsolver.draft.old.math.range;

public class Range {
    /**
     * Min value, inclusive.
     */
    private final double min;
    /**
     * Max value, exclusive.
     */
    private final double max;

    public Range(double min, double max) {
        if (!(min < max)) {
            throw new IllegalArgumentException("The max of a range must be strictly greater than its min.");
        }
        this.min = min;
        this.max = max;
    }

    public double getMin() {
        return min;
    }

    public double getMax() {
        return max;
    }
}
