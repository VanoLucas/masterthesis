package com.vanolucas.cppsolver.draft.old.math.range;

public interface RangeGuard {
    double bringBackToRange(double value, Range range);
}
