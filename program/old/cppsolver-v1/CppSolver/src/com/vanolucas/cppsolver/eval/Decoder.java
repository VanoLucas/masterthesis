package com.vanolucas.cppsolver.eval;

public interface Decoder<TToDecode, TDecoded> {
    TDecoded decode(TToDecode toDecode);
}
