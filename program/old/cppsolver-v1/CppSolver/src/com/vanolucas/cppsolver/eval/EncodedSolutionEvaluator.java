package com.vanolucas.cppsolver.eval;

import com.vanolucas.cppsolver.quality.Quality;
import com.vanolucas.cppsolver.solution.EncodedSolution;
import com.vanolucas.cppsolver.solution.Solution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class EncodedSolutionEvaluator extends SolutionEvaluator {
    private final List<Decoder> decoders;

    public EncodedSolutionEvaluator(Decoder decoder, Evaluator phenotypeEvaluator) {
        this(Collections.singletonList(decoder), phenotypeEvaluator);
    }

    public EncodedSolutionEvaluator(Decoder decoder1, Decoder decoder2, Evaluator phenotypeEvaluator) {
        this(Arrays.asList(decoder1, decoder2), phenotypeEvaluator);
    }

    public EncodedSolutionEvaluator(List<Decoder> decoders, Evaluator phenotypeEvaluator) {
        super(phenotypeEvaluator);
        this.decoders = new ArrayList<>(decoders);
    }

    @Override
    public Quality evaluate(Solution solution) {
        return evaluate((EncodedSolution) solution);
    }

    public Quality evaluate(EncodedSolution solution) {
        // first successively decode the encoded representations of the solution
        decodeRepresentations(solution);
        // then evaluate the decoded phenotype
        return evaluatePhenotype(solution);
    }

    public void decodeRepresentations(EncodedSolution solution) {
        solution.decodeRepresentations(decoders);
    }
}
