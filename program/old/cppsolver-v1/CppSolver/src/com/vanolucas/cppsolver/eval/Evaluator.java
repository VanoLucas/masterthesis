package com.vanolucas.cppsolver.eval;

import com.vanolucas.cppsolver.quality.Quality;

public interface Evaluator<TToEval, TQuality extends Quality> {
    TQuality evaluate(TToEval toEval);
}
