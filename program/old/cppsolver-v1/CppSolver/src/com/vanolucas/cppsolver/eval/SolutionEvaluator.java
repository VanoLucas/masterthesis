package com.vanolucas.cppsolver.eval;

import com.vanolucas.cppsolver.quality.Quality;
import com.vanolucas.cppsolver.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.solution.Solution;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

/**
 * Executes the evaluation function of a solution.
 */
public class SolutionEvaluator {
    private final Evaluator phenotypeEvaluator;

    public SolutionEvaluator(Evaluator phenotypeEvaluator) {
        this.phenotypeEvaluator = phenotypeEvaluator;
    }

    public EvaluatedSolution getEvaluated(Solution solution) {
        return new EvaluatedSolution(solution, evaluate(solution));
    }

    public Quality evaluate(Solution solution) {
        return evaluatePhenotype(solution);
    }

    public Quality evaluatePhenotype(Solution solution) {
        return solution.evaluatePhenotype(phenotypeEvaluator);
    }

    public CompletableFuture<EvaluatedSolution> getEvaluatedAsync(Solution solution, Executor executor) {
        return CompletableFuture.supplyAsync(
                () -> getEvaluated(solution),
                executor
        );
    }

    public CompletableFuture<Quality> evaluateAsync(Solution solution, Executor executor) {
        return CompletableFuture.supplyAsync(
                () -> evaluate(solution),
                executor
        );
    }
}
