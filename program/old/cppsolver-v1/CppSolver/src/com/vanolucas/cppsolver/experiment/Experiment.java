package com.vanolucas.cppsolver.experiment;

import com.vanolucas.cppsolver.opti.Opti;
import com.vanolucas.cppsolver.quality.Quality;
import com.vanolucas.cppsolver.quality.SingleNumberQuality;
import com.vanolucas.cppsolver.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.solution.keeper.SingleEarliestBestSolutionKeeper;
import com.vanolucas.cppsolver.util.PrettyDuration;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.google.common.math.Quantiles.median;

public class Experiment {
    private final String name;
    private final Supplier<Opti> optiSupplier;
    private final int runsCount;
    private final Duration durationPerRun;

    private final List<Quality> bestQualities;
    private final SingleEarliestBestSolutionKeeper overallBestKeeper;

    public Experiment(Supplier<Opti> optiSupplier, int runsCount, Duration durationPerRun, boolean printWhenNewBest) {
        this(null, optiSupplier, runsCount, durationPerRun, printWhenNewBest);
    }

    public Experiment(String name, Supplier<Opti> optiSupplier, int runsCount, Duration durationPerRun, boolean printWhenNewBest) {
        this.name = name;
        this.optiSupplier = optiSupplier;
        this.runsCount = runsCount;
        this.durationPerRun = durationPerRun;

        this.bestQualities = new ArrayList<>(this.runsCount);
        this.overallBestKeeper = new SingleEarliestBestSolutionKeeper(printWhenNewBest);
    }

    public void run() {
        if (runsCount < 1) {
            throw new IllegalArgumentException("The number of runs in an experiment must be at least 1.");
        }

        // do multiple runs sequentially
        for (int currentRun = 1; currentRun <= runsCount; currentRun++) {
            System.out.println(String.format("Experiment '%s', run %3d [start]", name, currentRun));
            // get the opti algo to run
            Opti opti = optiSupplier.get();
            // create a keeper to track the best solution from this opti
            SingleEarliestBestSolutionKeeper bestKeeper = new SingleEarliestBestSolutionKeeper(false);
            opti.addOnSolutionEvaluatedListener(bestKeeper::submit);
            // run the opti algo
            opti.runForAtMost(durationPerRun);
            // get the best solution
            EvaluatedSolution bestSolution = bestKeeper.getBest();
            Quality bestQuality = bestSolution.getQuality();
            // record the best quality for this run
            bestQualities.add(bestQuality);
            // update overall best solution
            overallBestKeeper.submit(bestSolution);
        }
    }

    public double getAvgQuality() {
        return streamBestQualities()
                .mapToDouble(this::getQualityAsDouble)
                .average()
                .orElseThrow(() -> {
                    throw new IllegalStateException("Could not calculate the average quality of the experiment.");
                });
    }

    public double getMedianQuality() {
        List<Double> bestQualities = streamBestQualities()
                .mapToDouble(this::getQualityAsDouble)
                .boxed()
                .collect(Collectors.toList());
        return median().compute(bestQualities);
    }

    private double getQualityAsDouble(Quality quality) {
        if (!(quality instanceof SingleNumberQuality)) {
            throw new UnsupportedOperationException("Cannot convert to a double value the quality objects that are not expressed as a single number.");
        }
        return ((SingleNumberQuality) quality).getValue().doubleValue();
    }

    public Stream<Quality> streamBestQualities() {
        return bestQualities.stream();
    }

    public String getBestQualitiesStr() {
        return streamBestQualities()
                .sorted(Comparator.reverseOrder())
                .map(Object::toString)
                .collect(Collectors.joining(" "));
    }

    public EvaluatedSolution getBestSolution() {
        return overallBestKeeper.getBest();
    }

    public String getStatsStr() {
        return String.format("Experiment: %s%n" +
                        "%d runs of duration %s%n" +
                        "Overall best:%n" +
                        "%s%n" +
                        "Best qualities:%n" +
                        "%s%n" +
                        "Average quality: %f%n" +
                        "Median quality: %f",
                name != null ? name : "",
                runsCount,
                PrettyDuration.prettify(durationPerRun),
                overallBestKeeper.getBest(),
                getBestQualitiesStr(),
                getAvgQuality(),
                getMedianQuality()
        );
    }

    public void printStats() {
        System.out.println(getStatsStr());
    }

    @Override
    public String toString() {
        return name;
    }
}
