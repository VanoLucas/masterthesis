package com.vanolucas.cppsolver.mutation;

import com.vanolucas.cppsolver.solution.EncodedSolution;
import com.vanolucas.cppsolver.solution.Solution;

public interface EncodedSolutionMutator extends SolutionMutator {
    @Override
    default void mutate(Solution solution) {
        mutate((EncodedSolution) solution);
    }

    void mutate(EncodedSolution solution);
}
