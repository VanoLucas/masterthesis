package com.vanolucas.cppsolver.mutation;

import com.vanolucas.cppsolver.solution.EncodedSolution;

public class FirstEncodedRepresentationSolutionMutator implements EncodedSolutionMutator {
    private final Mutator encodedRepresentationMutator;

    public FirstEncodedRepresentationSolutionMutator(Mutator encodedRepresentationMutator) {
        this.encodedRepresentationMutator = encodedRepresentationMutator;
    }

    @Override
    public void mutate(EncodedSolution solution) {
        mutateFirstEncodedRepresentation(solution);
    }

    public void mutateFirstEncodedRepresentation(EncodedSolution solution) {
        solution.mutateEncodedRepresentation(0, encodedRepresentationMutator);
    }
}
