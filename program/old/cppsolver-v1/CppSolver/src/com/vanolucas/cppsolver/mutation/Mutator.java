package com.vanolucas.cppsolver.mutation;

public interface Mutator<T> {
    void mutate(T toMutate);
}
