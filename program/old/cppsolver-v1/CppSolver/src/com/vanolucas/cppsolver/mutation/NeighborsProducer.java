package com.vanolucas.cppsolver.mutation;

import com.vanolucas.cppsolver.clone.SolutionCloner;
import com.vanolucas.cppsolver.solution.Solution;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.stream.Stream;

/**
 * List of mutation operators that define a neighborhood.
 * It can produce neighbors out of the provided neighboring mutation operators.
 */
public class NeighborsProducer {
    private final List<SolutionMutator> mutators;
    private final SolutionCloner cloner;

    public NeighborsProducer(List<SolutionMutator> mutators, SolutionCloner cloner) {
        this.mutators = new ArrayList<>(mutators);

        if (cloner == null) {
            throw new IllegalArgumentException("The neighbors producer needs a solution cloner to be able to produce new solutions before mutating them.");
        }
        this.cloner = cloner;
    }

    public Stream<Solution> produceNeighbors(Solution solution) {
        // each neighboring operator produces one neighbor
        return mutators.stream()
                .map(mutator -> produceNeighbor(solution, mutator));
    }

    public Stream<CompletableFuture<Solution>> produceNeighborsParallel(Solution solution, Executor executor) {
        // each neighboring operator produces one neighbor, executed in parallel
        return mutators.stream()
                .map(mutator -> produceNeighborAsync(solution, mutator, executor));
    }

    private Solution produceNeighbor(Solution solution, SolutionMutator mutator) {
        // create a neighbor as clone of the solution
        final Solution neighbor = cloner.clone(solution);
        // mutate the neighbor
        mutator.mutate(neighbor);
        return neighbor;
    }

    private CompletableFuture<Solution> produceNeighborAsync(Solution solution, SolutionMutator mutator, Executor executor) {
        // produce neighbor on another thread
        return CompletableFuture.supplyAsync(
                () -> produceNeighbor(solution, mutator),
                executor
        );
    }
}
