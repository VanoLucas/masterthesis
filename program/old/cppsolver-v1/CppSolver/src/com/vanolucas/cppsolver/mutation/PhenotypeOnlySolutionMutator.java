package com.vanolucas.cppsolver.mutation;

import com.vanolucas.cppsolver.solution.Solution;

public class PhenotypeOnlySolutionMutator implements SolutionMutator {
    private final Mutator phenotypeMutator;

    public PhenotypeOnlySolutionMutator(Mutator phenotypeMutator) {
        this.phenotypeMutator = phenotypeMutator;
    }

    @Override
    public void mutate(Solution solution) {
        mutatePhenotype(solution);
    }

    public void mutatePhenotype(Solution solution) {
        solution.mutatePhenotype(phenotypeMutator);
    }
}
