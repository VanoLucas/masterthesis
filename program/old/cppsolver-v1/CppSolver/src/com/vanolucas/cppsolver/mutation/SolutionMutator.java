package com.vanolucas.cppsolver.mutation;

import com.vanolucas.cppsolver.solution.Solution;

public interface SolutionMutator {
    void mutate(Solution solution);
}
