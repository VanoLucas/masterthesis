package com.vanolucas.cppsolver.opti;

import com.vanolucas.cppsolver.eval.SolutionEvaluator;
import com.vanolucas.cppsolver.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.solution.Solution;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;

/**
 * Base class for an iterative optimization algorithm.
 */
public abstract class Opti {
    private AtomicLong iteration = new AtomicLong(0L);
    private final SolutionEvaluator evaluator;

    private final List<Consumer<Long>> onIterationEndListeners;
    private final List<Consumer<EvaluatedSolution>> onSolutionEvaluatedListeners;

    private Duration runDuration = Duration.ZERO;

    @SuppressWarnings("WeakerAccess")
    public Opti(SolutionEvaluator evaluator) {
        if (evaluator == null) {
            throw new IllegalStateException("A solution evaluator is needed for the optimization algorithm to evaluate a solution.");
        }
        this.evaluator = evaluator;

        this.onIterationEndListeners = new ArrayList<>(2);
        this.onSolutionEvaluatedListeners = new ArrayList<>(2);
    }

    public long getIteration() {
        return iteration.get();
    }

    @SuppressWarnings("WeakerAccess")
    public void run() {
        while (true) {
            nextIteration();
        }
    }

    public void runFor(long iterationCount) {
        if (iterationCount < 0) {
            throw new IllegalArgumentException("The number of iterations to run for must be positive.");
        }

        for (long i = 0; i < iterationCount; i++) {
            nextIteration();
        }
    }

    public void runForAtMost(Duration duration) {
        // calculate when we must have finished
        final Instant mustStopBefore = Instant.now().plus(duration);

        // run iterations as long as we think we still have time for it by projecting based on the duration of the longest iteration
        Duration longestIterDuration = Duration.ZERO;
        do {
            Instant started = Instant.now();
            nextIteration();
            Duration iterDuration = Duration.between(started, Instant.now());
            if (iterDuration.compareTo(longestIterDuration) > 0) {
                longestIterDuration = iterDuration;
//                System.out.println("Longest iteration: " + longestIterDuration);
            }
        } while (Instant.now().plus(longestIterDuration).isBefore(mustStopBefore));
    }

    public void nextIteration() {
        Instant instantRunStart = Instant.now();
        iteration.incrementAndGet();
        runStep();
        runDuration = runDuration.plus(Duration.between(instantRunStart, Instant.now()));
    }

    protected abstract void runStep();

    @SuppressWarnings("WeakerAccess")
    protected EvaluatedSolution getEvaluated(Solution solution) {
        EvaluatedSolution evaluatedSolution = evaluator.getEvaluated(solution);
        onSolutionEvaluatedListeners.forEach(l -> l.accept(evaluatedSolution));
        return evaluatedSolution;
    }

    protected CompletableFuture<EvaluatedSolution> getEvaluatedAsync(Solution solution, Executor executor) {
        return evaluator.getEvaluatedAsync(solution, executor)
                .thenApplyAsync(evaluatedSolution -> {
                    onSolutionEvaluatedListeners.forEach(l -> l.accept(evaluatedSolution));
                    return evaluatedSolution;
                }, executor);
    }

    public void addOnIterationEndListener(Consumer<Long> listener) {
        onIterationEndListeners.add(listener);
    }

    public void addOnSolutionEvaluatedListener(Consumer<EvaluatedSolution> listener) {
        onSolutionEvaluatedListeners.add(listener);
    }

    public String getRunStatsStr() {
        return String.format("Iteration: %d%n" +
                        "Total run duration: %s",
                iteration.get(), runDuration
        );
    }

    public void printRunStats() {
        System.out.println(getRunStatsStr());
    }

    @Override
    public String toString() {
        return getRunStatsStr();
    }
}
