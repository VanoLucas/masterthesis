package com.vanolucas.cppsolver.opti;

import com.vanolucas.cppsolver.eval.SolutionEvaluator;
import com.vanolucas.cppsolver.mutation.NeighborsProducer;
import com.vanolucas.cppsolver.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.solution.Solution;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.stream.Collectors;

public class ParallelSteepestAscentHillClimbing extends SteepestAscentHillClimbing {
    protected final Executor executor;

    public ParallelSteepestAscentHillClimbing(Solution initialSolution,
                                              NeighborsProducer neighborsProducer,
                                              SolutionEvaluator evaluator,
                                              Executor executor) {
        super(initialSolution, neighborsProducer, evaluator);
        this.executor = executor;
    }

    @Override
    protected void runStep() {
        // start async neighbors creation and evaluation
        List<CompletableFuture<EvaluatedSolution>> evaluatedNeighborFutures =
                // produce neighbors using the neighboring operators
                neighborsProducer.produceNeighborsParallel(currentSolution, executor)
                        // evaluate neighbors
                        .map(neighborFuture -> neighborFuture
                                .thenApplyAsync(this::getEvaluated, executor)
                        )
                        .collect(Collectors.toList());

        // wait for all neighbors and pick the best
        currentSolution = evaluatedNeighborFutures.stream()
                // wait for all neighbors to be created and evaluated
                .map(CompletableFuture::join)
                // find the best neighbor
                .max(Comparator.naturalOrder())
                .orElseThrow(() -> new IllegalStateException("Could not find the best neighbor of the parallel steepest ascent hill climbing step."))
                // move to the best neighbor
                .getSolution();
    }
}
