package com.vanolucas.cppsolver.opti;

import com.vanolucas.cppsolver.eval.SolutionEvaluator;
import com.vanolucas.cppsolver.mutation.SolutionMutator;
import com.vanolucas.cppsolver.solution.Solution;

public class RandomSearch extends SingleMutation {
    public RandomSearch(Solution initialSolution, SolutionMutator randomizer, SolutionEvaluator evaluator) {
        super(initialSolution, randomizer, evaluator);
    }
}
