package com.vanolucas.cppsolver.opti;

import com.vanolucas.cppsolver.eval.SolutionEvaluator;
import com.vanolucas.cppsolver.mutation.NeighborsProducer;
import com.vanolucas.cppsolver.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.solution.Solution;
import com.vanolucas.cppsolver.solution.keeper.SingleEarliestBestSolutionKeeper;
import com.vanolucas.cppsolver.util.PrettyDuration;

import java.time.Duration;
import java.time.Instant;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class RestartParallelSteepestAscentHillClimbing extends ParallelSteepestAscentHillClimbing {
    private final Supplier<Solution> initialSolutionsSupplier;
    private final Duration durationWithoutImprovement;
    private final SingleEarliestBestSolutionKeeper bestKeeper;
    private Instant instantLastImprovement = null;
    private boolean randomRestartNeeded = false;

    public RestartParallelSteepestAscentHillClimbing(Supplier<Solution> initialSolutionsSupplier,
                                                     NeighborsProducer neighborsProducer,
                                                     SolutionEvaluator evaluator,
                                                     Executor executor,
                                                     Duration restartAfterDurationWithoutImprovement) {
        super(initialSolutionsSupplier.get(), neighborsProducer, evaluator, executor);
        this.initialSolutionsSupplier = initialSolutionsSupplier;
        if (restartAfterDurationWithoutImprovement == null) {
            throw new IllegalArgumentException("A max duration without improvement must be provided to the random restart hill climbing algo.");
        }
        this.durationWithoutImprovement = restartAfterDurationWithoutImprovement;
        this.bestKeeper = new SingleEarliestBestSolutionKeeper(false);
    }

    @Override
    protected void runStep() {
        // if need to perform a random restart
        if (randomRestartNeeded) {
            randomRestart();
        }

        // start async neighbors creation and evaluation
        List<CompletableFuture<EvaluatedSolution>> evaluatedNeighborFutures =
                // produce neighbors using the neighboring operators
                neighborsProducer.produceNeighborsParallel(currentSolution, executor)
                        // evaluate neighbors
                        .map(neighborFuture -> neighborFuture
                                .thenApplyAsync(this::getEvaluated, executor)
                        )
                        .collect(Collectors.toList());

        // wait for all neighbors and pick the best
        EvaluatedSolution bestNeighbor = evaluatedNeighborFutures.stream()
                // wait for all neighbors to be created and evaluated
                .map(CompletableFuture::join)
                // find the best neighbor
                .max(Comparator.naturalOrder())
                .orElseThrow(() -> new IllegalStateException("Could not find the best neighbor of the parallel steepest ascent hill climbing step."));

        // move to the best neighbor
        currentSolution = bestNeighbor.getSolution();

        // track improvements for random restart condition
        switch (bestKeeper.submit(bestNeighbor)) {
            // if we improved, keep track of when
            case KEPT:
                instantLastImprovement = Instant.now();
                break;
            // else if no improvement
            default:
                // if no improvement for too long, we need to random restart
                if (instantLastImprovement.plus(durationWithoutImprovement).isBefore(Instant.now())) {
                    randomRestartNeeded = true;
                }
                break;
        }
    }

    protected void randomRestart() {
//        System.out.println("Random restart");
        currentSolution = initialSolutionsSupplier.get();
        instantLastImprovement = Instant.now();
        bestKeeper.reset();
        randomRestartNeeded = false;
    }

    @Override
    public String getRunStatsStr() {
        return String.format("%s%n" +
                        "Random restart hill climbing when stuck for: %s",
                super.getRunStatsStr(),
                PrettyDuration.prettify(durationWithoutImprovement)
        );
    }
}
