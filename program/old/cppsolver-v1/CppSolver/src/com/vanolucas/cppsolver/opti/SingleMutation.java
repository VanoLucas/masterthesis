package com.vanolucas.cppsolver.opti;

import com.vanolucas.cppsolver.eval.SolutionEvaluator;
import com.vanolucas.cppsolver.mutation.SolutionMutator;
import com.vanolucas.cppsolver.solution.Solution;

public class SingleMutation extends Opti {
    private final Solution currentSolution;
    private final SolutionMutator mutator;

    public SingleMutation(Solution initialSolution, SolutionMutator mutator, SolutionEvaluator evaluator) {
        super(evaluator);

        if (initialSolution == null) {
            throw new IllegalArgumentException("The single mutation optimization algorithm needs an initial solution.");
        }
        this.currentSolution = initialSolution;

        if (mutator == null) {
            throw new IllegalArgumentException("The single mutation optimization algorithm needs a solution mutator.");
        }
        this.mutator = mutator;
    }

    @Override
    protected void runStep() {
        // mutate current solution
        mutator.mutate(currentSolution);

        // evaluate current solution
        getEvaluated(currentSolution);
    }
}
