package com.vanolucas.cppsolver.opti;

import com.vanolucas.cppsolver.eval.SolutionEvaluator;
import com.vanolucas.cppsolver.mutation.NeighborsProducer;
import com.vanolucas.cppsolver.solution.Solution;

import java.util.Comparator;

/**
 * Steepest ascent hill climbing optimization algorithm.
 */
public class SteepestAscentHillClimbing extends Opti {
    @SuppressWarnings("WeakerAccess")
    protected Solution currentSolution;
    @SuppressWarnings("WeakerAccess")
    protected final NeighborsProducer neighborsProducer;

    @SuppressWarnings("WeakerAccess")
    public SteepestAscentHillClimbing(Solution initialSolution, NeighborsProducer neighborsProducer, SolutionEvaluator evaluator) {
        super(evaluator);
        this.currentSolution = initialSolution;
        this.neighborsProducer = neighborsProducer;
    }

    @Override
    protected void runStep() {
        currentSolution = neighborsProducer
                // produce neighbors using the neighboring operators
                .produceNeighbors(currentSolution)
                // evaluate neighbors
                .map(this::getEvaluated)
                // find the best neighbor
                .max(Comparator.naturalOrder())
                .orElseThrow(() -> new IllegalStateException("Could not find the best neighbor of the steepest ascent hill climbing step."))
                // move to the best neighbor
                .getSolution();
    }
}
