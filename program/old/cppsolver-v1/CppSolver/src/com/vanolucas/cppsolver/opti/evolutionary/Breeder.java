package com.vanolucas.cppsolver.opti.evolutionary;

import com.vanolucas.cppsolver.solution.EvaluatedPopulation;
import com.vanolucas.cppsolver.solution.Population;

public interface Breeder {
    Population breed(EvaluatedPopulation parents);
}
