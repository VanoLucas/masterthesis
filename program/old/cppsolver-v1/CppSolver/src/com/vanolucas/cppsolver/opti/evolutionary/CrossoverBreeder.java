package com.vanolucas.cppsolver.opti.evolutionary;

import com.vanolucas.cppsolver.crossover.Crossover;
import com.vanolucas.cppsolver.solution.EvaluatedPopulation;
import com.vanolucas.cppsolver.solution.Population;

public class CrossoverBreeder implements Breeder {
    private final Crossover crossover;

    public CrossoverBreeder(Crossover crossover) {
        this.crossover = crossover;
    }

    @Override
    public Population breed(EvaluatedPopulation parents) {
        return crossover.makeChildren(parents);
    }
}
