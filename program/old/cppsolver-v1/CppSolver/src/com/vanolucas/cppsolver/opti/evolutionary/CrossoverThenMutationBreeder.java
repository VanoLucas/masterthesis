package com.vanolucas.cppsolver.opti.evolutionary;

import com.vanolucas.cppsolver.crossover.Crossover;
import com.vanolucas.cppsolver.mutation.SolutionMutator;
import com.vanolucas.cppsolver.rand.Rand;
import com.vanolucas.cppsolver.solution.EvaluatedPopulation;
import com.vanolucas.cppsolver.solution.Population;

public class CrossoverThenMutationBreeder extends CrossoverBreeder {
    private final SolutionMutator mutator;
    private final double mutationProba;
    private final Rand rand;

    public CrossoverThenMutationBreeder(Crossover crossover, SolutionMutator mutator, double mutationProba) {
        this(crossover, mutator, mutationProba, new Rand());
    }

    public CrossoverThenMutationBreeder(Crossover crossover, SolutionMutator mutator, double mutationProba, Rand rand) {
        super(crossover);
        this.mutator = mutator;
        this.mutationProba = mutationProba;
        this.rand = rand;
    }

    @Override
    public Population breed(EvaluatedPopulation parents) {
        Population children = super.breed(parents);
        children.forEach(child -> {
            if (rand.double01() < mutationProba) {
                mutator.mutate(child);
            }
        });
        return children;
    }
}
