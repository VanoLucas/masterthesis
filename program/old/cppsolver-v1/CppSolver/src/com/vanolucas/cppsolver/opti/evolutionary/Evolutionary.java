package com.vanolucas.cppsolver.opti.evolutionary;

import com.vanolucas.cppsolver.eval.SolutionEvaluator;
import com.vanolucas.cppsolver.opti.Opti;
import com.vanolucas.cppsolver.solution.EvaluatedPopulation;
import com.vanolucas.cppsolver.solution.EvaluatedSolutionGroups;
import com.vanolucas.cppsolver.solution.Population;

import java.util.stream.Collectors;

public class Evolutionary extends Opti {
    /**
     * The current population of this evolutionary algorithm.
     */
    private EvaluatedPopulation population;
    /**
     * Selects solutions for reproduction.
     */
    private final GroupsMaker matesSelector;
    /**
     * Produces children solutions out of a group of parents.
     */
    private final Breeder breeder;
    /**
     * Merges the main population with the new offspring population.
     */
    private final Merger merger;

    public Evolutionary(Population initialPopulation, SolutionEvaluator evaluator, GroupsMaker matesSelector, Breeder breeder, Merger merger) {
        super(evaluator);

        this.matesSelector = matesSelector;
        this.breeder = breeder;
        this.merger = merger;

        // set and evaluate initial population
        population = evaluate(initialPopulation);
    }

    @Override
    protected void runStep() {
        // select individuals for reproduction (make mating groups)
        EvaluatedSolutionGroups matingGroups = matesSelector.makeGroups(population);
        // breed to give birth to offspring
        Population offspring = matingGroups.breedUsing(breeder);
        // evaluate children
        EvaluatedPopulation evaluatedOffspring = evaluate(offspring);
        // update population
        population = merger.merge(population, evaluatedOffspring);
    }

    protected EvaluatedPopulation evaluate(Population pop) {
        return new EvaluatedPopulation(
                pop.stream()
                        .map(this::getEvaluated)
                        .collect(Collectors.toList())
        );
    }
}
