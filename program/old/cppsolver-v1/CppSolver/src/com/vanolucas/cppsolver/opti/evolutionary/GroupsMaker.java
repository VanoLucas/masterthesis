package com.vanolucas.cppsolver.opti.evolutionary;

import com.vanolucas.cppsolver.solution.EvaluatedPopulation;
import com.vanolucas.cppsolver.solution.EvaluatedSolutionGroups;

public interface GroupsMaker {
    EvaluatedSolutionGroups makeGroups(EvaluatedPopulation population);
}
