package com.vanolucas.cppsolver.opti.evolutionary;

import com.vanolucas.cppsolver.solution.EvaluatedPopulation;

public class KeepBestMerger implements Merger {
    @Override
    public EvaluatedPopulation merge(EvaluatedPopulation p1, EvaluatedPopulation p2) {
        EvaluatedPopulation mergedPopulation = p1.concat(p2);
        mergedPopulation.keepBest(p1.size());
        return mergedPopulation;
    }
}
