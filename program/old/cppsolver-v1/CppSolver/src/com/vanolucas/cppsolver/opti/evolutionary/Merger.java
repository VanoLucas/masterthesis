package com.vanolucas.cppsolver.opti.evolutionary;

import com.vanolucas.cppsolver.solution.EvaluatedPopulation;

public interface Merger {
    EvaluatedPopulation merge(EvaluatedPopulation p1, EvaluatedPopulation p2);
}
