package com.vanolucas.cppsolver.opti.evolutionary;

import com.vanolucas.cppsolver.rand.Rand;
import com.vanolucas.cppsolver.solution.EvaluatedPopulation;
import com.vanolucas.cppsolver.solution.EvaluatedSolutionGroups;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class RandomGroupsMaker implements GroupsMaker {
    private final int groupSize;
    private final Rand rand;

    public RandomGroupsMaker(int groupSize) {
        this(groupSize, new Rand());
    }

    public RandomGroupsMaker(int groupSize, Rand rand) {
        this.groupSize = groupSize;
        this.rand = rand;
    }

    @Override
    public EvaluatedSolutionGroups makeGroups(EvaluatedPopulation population) {
        final int groupsCount = population.size() / 2;
        return new EvaluatedSolutionGroups(
                IntStream.range(0, groupsCount)
                        .mapToObj(i -> population.pickRand(groupSize, rand))
                        .collect(Collectors.toList())
        );
    }
}
