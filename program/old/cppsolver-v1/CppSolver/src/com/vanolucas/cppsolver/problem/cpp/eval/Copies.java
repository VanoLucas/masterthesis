package com.vanolucas.cppsolver.problem.cpp.eval;

import com.vanolucas.cppsolver.problem.cpp.instance.BookCover;

/**
 * A number of copies of a given book cover.
 */
public class Copies implements Comparable<Copies> {
    private final BookCover bookCover;
    private int copiesCount;

    public Copies(BookCover bookCover, int copiesCount) {
        this.bookCover = bookCover;
        this.copiesCount = copiesCount;
    }

    public BookCover getBookCover() {
        return bookCover;
    }

    public int getCopiesCount() {
        return copiesCount;
    }

    public Copies splitInTwo(double fractionToExtract) {
        final int otherCopiesCount = (int) (fractionToExtract * copiesCount);
        copiesCount -= otherCopiesCount;
        return new Copies(bookCover, otherCopiesCount);
    }

    /**
     * Compare based on the number of copies.
     */
    @Override
    public int compareTo(Copies other) {
        return Integer.compare(copiesCount, other.copiesCount);
    }

    @Override
    public String toString() {
        return String.format("%6d copies / %6d of book cover %3d",
                copiesCount, bookCover.getDemand(), bookCover.getId()
        );
    }
}
