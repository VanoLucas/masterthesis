package com.vanolucas.cppsolver.problem.cpp.eval;

import com.vanolucas.cppsolver.eval.Evaluator;
import com.vanolucas.cppsolver.problem.cpp.instance.CppInstance;
import com.vanolucas.cppsolver.problem.cpp.solution.CppSolution;
import com.vanolucas.cppsolver.quality.DoubleCost;

public class CppSolutionEvaluator implements Evaluator<CppSolution, DoubleCost> {
    private final double costPlate;
    private final double costPrinting;

    public CppSolutionEvaluator(CppInstance cppInstance) {
        this(cppInstance.getCostPlate(), cppInstance.getCostPrinting());
    }

    public CppSolutionEvaluator(double costPlate, double costPrinting) {
        this.costPlate = costPlate;
        this.costPrinting = costPrinting;
    }

    @Override
    public DoubleCost evaluate(CppSolution cppSolution) {
//        System.out.println(String.format("Eval:%n%s", cppSolution));
        return new DoubleCost(
                cppSolution.getCost(costPlate, costPrinting)
        );
    }
}
