package com.vanolucas.cppsolver.problem.cpp.eval;

import com.vanolucas.cppsolver.problem.cpp.instance.Demand;
import com.vanolucas.cppsolver.problem.cpp.solution.CppSolution;

/**
 * Allocates the input slots to some offset plates.
 */
public abstract class PlatesAssigner {
    protected final Demand demand;

    public PlatesAssigner(Demand demand) {
        this.demand = demand;
    }

    public abstract CppSolution assign(Slots slots, int slotsPerPlateCount);
}
