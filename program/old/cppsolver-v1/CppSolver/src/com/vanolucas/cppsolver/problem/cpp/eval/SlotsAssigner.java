package com.vanolucas.cppsolver.problem.cpp.eval;

import com.vanolucas.cppsolver.problem.cpp.instance.Demand;

/**
 * Allocates the demand for book cover copies over a number of slots.
 */
public interface SlotsAssigner {
    Slots assign(Demand demand, int slotsCount);
}
