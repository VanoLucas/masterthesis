package com.vanolucas.cppsolver.problem.cpp.eval;

import com.vanolucas.cppsolver.eval.Decoder;
import com.vanolucas.cppsolver.problem.cpp.instance.CppInstance;
import com.vanolucas.cppsolver.problem.cpp.instance.Demand;
import com.vanolucas.cppsolver.problem.cpp.solution.CppSolution;

public class SlotsDecoder implements Decoder<Slots, CppSolution> {
    private final PlatesAssigner platesAssigner;
    private final int slotsPerPlateCount;

    public SlotsDecoder(CppInstance cppInstance) {
        this(new SortByCopiesPlatesAssigner(cppInstance.getDemand()), cppInstance);
    }

    public SlotsDecoder(Demand demand, int slotsPerPlateCount) {
        this(new SortByCopiesPlatesAssigner(demand), slotsPerPlateCount);
    }

    public SlotsDecoder(PlatesAssigner platesAssigner, CppInstance cppInstance) {
        this(platesAssigner, cppInstance.getSlotsPerPlateCount());
    }

    public SlotsDecoder(PlatesAssigner platesAssigner, int slotsPerPlateCount) {
        this.platesAssigner = platesAssigner;
        this.slotsPerPlateCount = slotsPerPlateCount;
    }

    @Override
    public CppSolution decode(Slots slots) {
        return platesAssigner.assign(slots, slotsPerPlateCount);
    }
}
