package com.vanolucas.cppsolver.problem.cpp.eval;

import com.vanolucas.cppsolver.problem.cpp.instance.Demand;
import com.vanolucas.cppsolver.problem.cpp.solution.CppSolution;
import com.vanolucas.cppsolver.problem.cpp.solution.Plate;

import java.util.List;
import java.util.stream.Collectors;

public class SortByCopiesPlatesAssigner extends PlatesAssigner {
    public SortByCopiesPlatesAssigner(Demand demand) {
        super(demand);
    }

    @Override
    public CppSolution assign(Slots slots, int slotsPerPlateCount) {
        // check params
        if (slots == null) {
            throw new IllegalArgumentException("Some slots must be provided for the plates assigner to work.");
        }
        if (slotsPerPlateCount < 1) {
            throw new IllegalArgumentException("An offset plate must contain at least 1 slot.");
        }

        // sort slots by number of copies descending
        slots.sortByCopiesCountDescending();

        // allocate slots to offset plates
        List<Plate> plates = slots.splitInGroupsOf(slotsPerPlateCount)
                .map(Plate::new)
                .collect(Collectors.toList());

        return new CppSolution(demand, plates);
    }
}
