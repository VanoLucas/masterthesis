package com.vanolucas.cppsolver.problem.cpp.genome;

import com.vanolucas.cppsolver.problem.cpp.instance.StandardCppInstance;
import com.vanolucas.cppsolver.rkv.RandomKeyVector;

/**
 * S-P Genome for the standard cover printing problem.
 * Slots-Printings random key vector genome representation of a standard cover printing problem solution.
 *
 * Part 1: keys to determine the number of extra slots for each book cover -> 1 key per book cover.
 * Part 2: keys to determine the number of printings to allocate each slot -> 1 key per slot.
 */
public class GenomeSp extends RandomKeyVector {
    public GenomeSp(StandardCppInstance cppInstance) {
        this(calculateLengthForCppInstance(cppInstance));
    }

    public GenomeSp(int length) {
        super(length);
    }

    public static int calculateLengthForCppInstance(StandardCppInstance cppInstance) {
        return calculateLength(
                cppInstance.getBookCoversCount(),
                cppInstance.getSlotsCount()
        );
    }

    public static int calculateLength(int bookCoversCount, int slotsCount) {
        return bookCoversCount + slotsCount;
    }
}
