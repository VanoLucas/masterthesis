package com.vanolucas.cppsolver.problem.cpp.genome;

public enum GenomeType {
    CF, P_PC, S_P
}
