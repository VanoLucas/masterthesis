package com.vanolucas.cppsolver.problem.cpp.genome.decode;

import com.vanolucas.cppsolver.eval.Decoder;
import com.vanolucas.cppsolver.math.CutCake;
import com.vanolucas.cppsolver.problem.cpp.eval.Copies;
import com.vanolucas.cppsolver.problem.cpp.eval.Slots;
import com.vanolucas.cppsolver.problem.cpp.genome.GenomeSp;
import com.vanolucas.cppsolver.problem.cpp.instance.Demand;
import com.vanolucas.cppsolver.problem.cpp.instance.StandardCppInstance;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

// todo test
public class GenomeSpDecoder implements Decoder<GenomeSp, Slots> {
    private final Demand demand;
    private final int bookCoversCount;
    private final int slotsCount;

    public GenomeSpDecoder(StandardCppInstance cppInstance) {
        this(cppInstance.getDemand(), cppInstance.getBookCoversCount(), cppInstance.getSlotsCount());
    }

    public GenomeSpDecoder(Demand demand, int bookCoversCount, int slotsCount) {
        this.demand = demand;
        this.bookCoversCount = bookCoversCount;
        this.slotsCount = slotsCount;
    }

    @Override
    public Slots decode(GenomeSp genome) {
        final Slots slots = new Slots(slotsCount);

        // determine the number of slots to allocate to each book cover
        final int[] nbSlotsPerCover = getNbSlotsPerCover(genome);

        // get the keys that determine the number of copies to allocate on the slots of each book cover
        final List<List<Double>> keysPrintingsForBookCover = getKeysPrintingsForEachBookCover(genome, nbSlotsPerCover);
        // calculate the number of copies to require for each slot of each book cover
        final List<List<Integer>> requiredCopies = getRequiredPrintings(keysPrintingsForBookCover);

        // allocate the slots
        IntStream.range(0, bookCoversCount)
                // for each book cover
                .forEach(bookCoverIndex -> {
                    // get the number of copies to allocate to each of its slots
                    final List<Integer> copiesBySlot = requiredCopies.get(bookCoverIndex);
                    // construct the copies object for each slot
                    copiesBySlot.stream()
                            .map(copiesCount ->
                                    new Copies(demand.getBookCover(bookCoverIndex), copiesCount)
                            )
                            .forEach(slots::add);
                });

        return slots;
    }

    private int[] getNbSlotsPerCover(GenomeSp genome) {
        // get keys of the genome that define the number of slots for each book cover
        double[] keysNbSlots = genome.doubleStream()
                .limit(bookCoversCount)
                .toArray();
        // determine how many extra slots to allocate to each book cover based on the keys
        int[] nbExtraSlotsPerCover = CutCake.getShares(slotsCount - bookCoversCount, keysNbSlots);
        // calculate total nb of slots for each book cover
        return Arrays.stream(nbExtraSlotsPerCover)
                // each book cover must be allocated to at least 1 slot
                .map(nbExtraSlots -> nbExtraSlots + 1)
                .toArray();
    }

    private List<List<Double>> getKeysPrintingsForEachBookCover(GenomeSp genome, int[] nbSlotsPerCover) {
        // get keys of the genome that define the number of printings for each slot
        double[] keysPrintings = genome.doubleStream()
                .skip(bookCoversCount)
                .toArray();

        // to store the keys that define how to spread the printings for each book cover
        List<List<Double>> keysPrintingsForBookCover = IntStream.range(0, bookCoversCount)
                .mapToObj(cover -> new ArrayList<Double>(slotsCount - bookCoversCount))
                .collect(Collectors.toList());

        // assign each key to a book cover based on the number of slots per book cover
        int remainingSlotsForBook = 0;
        int cover = -1;
        for (int keyIndex = 0; keyIndex < slotsCount; keyIndex++) {
            if (remainingSlotsForBook == 0) {
                cover++;
                remainingSlotsForBook = nbSlotsPerCover[cover];
            }
            keysPrintingsForBookCover.get(cover)
                    .add(keysPrintings[keyIndex]);
            remainingSlotsForBook--;
        }

        return keysPrintingsForBookCover;
    }

    private List<List<Integer>> getRequiredPrintings(List<List<Double>> keysPrintingsForBookCover) {
        // to store the required number of printings for each slot of each book cover
        List<List<Integer>> requiredPrintings = IntStream.range(0, bookCoversCount)
                .mapToObj(cover -> new ArrayList<Integer>(keysPrintingsForBookCover.get(cover).size()))
                .collect(Collectors.toList());

        // for each book cover, determine the required number of printings for each of its slots based on the keys
        for (int cover = 0; cover < bookCoversCount; cover++) {
            // get demand for this book cover
            final int coverDemand = demand.getDemand(cover);
            // get keys that determine the nb of printings of each slot for this book cover
            double[] keys = keysPrintingsForBookCover.get(cover)
                    .stream()
                    .mapToDouble(__ -> __)
                    .toArray();
            // spread the number of printings over the slots of this book cover
            int[] nbPrintingsPerSlot = CutCake.getShares(coverDemand, keys);
            requiredPrintings.get(cover)
                    .addAll(Arrays.stream(nbPrintingsPerSlot)
                            .boxed()
                            .collect(Collectors.toList())
                    );
        }

        return requiredPrintings;
    }
}
