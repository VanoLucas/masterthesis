package com.vanolucas.cppsolver.problem.cpp.instance;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Demand {
    private final List<BookCover> bookCovers;

    public Demand(List<BookCover> bookCovers) {
        this.bookCovers = new ArrayList<>(bookCovers);
    }

    public BookCover getBookCover(int index) {
        return bookCovers.get(index);
    }

    public int getDemand(int index) {
        return bookCovers.get(index).getDemand();
    }

    public int getTotalCopies() {
        return bookCovers.stream()
                .mapToInt(BookCover::getDemand)
                .sum();
    }

    public int size() {
        return bookCovers.size();
    }

    public Stream<BookCover> stream() {
        return bookCovers.stream();
    }

    public static Demand loadFromFile(Path path) throws IOException {
        return new Demand(
                // parse each line of the file
                Files.lines(path)
                        // load each line to a book cover object
                        .map(line -> {
                            // split fields
                            String[] fields = line.split(" ");
                            int id = Integer.parseInt(fields[0].trim());
                            int demand = Integer.parseInt(fields[1].trim());
                            // detect if the id and demand columns are inverted
                            if (id > 150) {
                                // swap id with demand
                                int tmp = id;
                                id = demand;
                                demand = tmp;
                            }
                            return new BookCover(id, demand);
                        })
                        .collect(Collectors.toList())
        );
    }

    @Override
    public String toString() {
        return bookCovers.stream()
                .sorted()
                .map(Object::toString)
                .collect(Collectors.joining(System.lineSeparator()));
    }
}
