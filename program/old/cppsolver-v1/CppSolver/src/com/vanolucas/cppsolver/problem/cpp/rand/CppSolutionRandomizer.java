package com.vanolucas.cppsolver.problem.cpp.rand;

import com.vanolucas.cppsolver.problem.cpp.solution.CppSolution;
import com.vanolucas.cppsolver.rand.Rand;
import com.vanolucas.cppsolver.rand.Randomizer;

public class CppSolutionRandomizer implements Randomizer<CppSolution> {
    private final RandomCppSolutionFactory randomCppSolutionFactory;

    public CppSolutionRandomizer(RandomCppSolutionFactory randomCppSolutionFactory) {
        this.randomCppSolutionFactory = randomCppSolutionFactory;
    }

    @Override
    public void randomize(CppSolution cppSolution, Rand rand) {
        cppSolution.randomize(randomCppSolutionFactory);
    }
}
