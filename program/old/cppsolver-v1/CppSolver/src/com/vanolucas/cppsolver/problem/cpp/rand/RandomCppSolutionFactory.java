package com.vanolucas.cppsolver.problem.cpp.rand;

import com.vanolucas.cppsolver.problem.cpp.eval.PlatesAssigner;
import com.vanolucas.cppsolver.problem.cpp.eval.Slots;
import com.vanolucas.cppsolver.problem.cpp.eval.SlotsAssigner;
import com.vanolucas.cppsolver.problem.cpp.eval.SortByCopiesPlatesAssigner;
import com.vanolucas.cppsolver.problem.cpp.instance.Demand;
import com.vanolucas.cppsolver.problem.cpp.instance.StandardCppInstance;
import com.vanolucas.cppsolver.problem.cpp.solution.CppSolution;
import com.vanolucas.cppsolver.rand.Rand;

public class RandomCppSolutionFactory {
    private final StandardCppInstance cppInstance;
    private final SlotsAssigner slotsAssigner;
    private final PlatesAssigner platesAssigner;

    public RandomCppSolutionFactory(StandardCppInstance cppInstance) {
        this(cppInstance, new Rand());
    }

    public RandomCppSolutionFactory(StandardCppInstance cppInstance, Rand rand) {
        this.cppInstance = cppInstance;
        this.slotsAssigner = new RandomSlotsAssigner(rand);
        this.platesAssigner = new SortByCopiesPlatesAssigner(cppInstance.getDemand());
    }

    public CppSolution newCppSolution() {
        final Demand demand = cppInstance.getDemand();
        final int slotsCount = cppInstance.getSlotsCount();

        Slots slots = slotsAssigner.assign(
                demand, slotsCount
        );
        CppSolution cppSolution = platesAssigner.assign(
                slots, cppInstance.getSlotsPerPlateCount()
        );

        return cppSolution;
    }
}
