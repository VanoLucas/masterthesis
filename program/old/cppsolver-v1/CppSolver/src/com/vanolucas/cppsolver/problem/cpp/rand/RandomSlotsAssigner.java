package com.vanolucas.cppsolver.problem.cpp.rand;

import com.vanolucas.cppsolver.problem.cpp.eval.Copies;
import com.vanolucas.cppsolver.problem.cpp.eval.Slots;
import com.vanolucas.cppsolver.problem.cpp.eval.SlotsAssigner;
import com.vanolucas.cppsolver.problem.cpp.instance.Demand;
import com.vanolucas.cppsolver.rand.Rand;

public class RandomSlotsAssigner implements SlotsAssigner {
    private final Rand rand;

    public RandomSlotsAssigner() {
        this(new Rand());
    }

    public RandomSlotsAssigner(Rand rand) {
        this.rand = rand;
    }

    @Override
    public Slots assign(Demand demand, int slotsCount) {
        final Slots slots = new Slots(slotsCount);

        // allot each book cover to at least one slot
        demand.stream()
                .map(bookCover -> new Copies(bookCover, bookCover.getDemand()))
                .forEach(slots::add);

        // allot each extra slot randomly to one of the book covers
        while (slots.size() < slotsCount) {
            final int slotToSplitIndex = rand.int0To(slots.size());
            final double fractionToExtract = rand.double01();
            slots.splitInTwo(
                    slotToSplitIndex,
                    fractionToExtract
            );
        }

        return slots;
    }
}
