package com.vanolucas.cppsolver.problem.cpp.solution;

import com.vanolucas.cppsolver.problem.cpp.eval.Copies;
import com.vanolucas.cppsolver.problem.cpp.instance.BookCover;
import com.vanolucas.cppsolver.problem.cpp.instance.CppInstance;
import com.vanolucas.cppsolver.problem.cpp.instance.Demand;
import com.vanolucas.cppsolver.problem.cpp.rand.RandomCppSolutionFactory;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CppSolution {
    private final Demand demand;
    private final List<Plate> plates;

    public CppSolution(Demand demand, List<Plate> plates) {
        this.demand = demand;
        this.plates = new ArrayList<>(plates);
    }

    public double getCost(CppInstance cppInstance) {
        return getCost(
                cppInstance.getCostPlate(),
                cppInstance.getCostPrinting()
        );
    }

    public double getCost(double costPlate, double costPrinting) {
        return getPlatesCount() * costPlate + getPrintingsCount() * costPrinting;
    }

    public int getPlatesCount() {
        return plates.size();
    }

    public int getPrintingsCount() {
        return plates.stream()
                .mapToInt(Plate::getPrintings)
                .sum();
    }

    public Stream<Copies> getRealizedCopies() {
        return demand.stream()
                // for each book cover, calculate its actual number of copies realized
                .map(bookCover -> {
                    // calculate the number of copies of this book cover based on its appearance on plate slots and number of printings of each plate
                    final int copiesCount = getCopiesCount(bookCover);
                    // create the copies object
                    return new Copies(bookCover, copiesCount);
                });
    }

    /**
     * @return Total number of copies of the given book cover produced by this solution.
     */
    public int getCopiesCount(BookCover bookCover) {
        return plates.stream()
                .mapToInt(plate -> plate.countSlotsWithBookCover(bookCover) * plate.getPrintings())
                .sum();
    }

    public int getTotalBookCoverCopiesProduced() {
        return plates.stream()
                .mapToInt(plate -> plate.getPrintings() * plate.getSlotsCount())
                .sum();
    }

    public int getTotalWastedCopies() {
        // for each book cover, calculate its wasted copies
        return demand.stream()
                .mapToInt(bookCover -> {
                    // calculate the actual number of copies of this book cover
                    final int copiesCount = getCopiesCount(bookCover);
                    // calculate the wasted copies of this book cover based on its demand
                    return copiesCount - bookCover.getDemand();
                })
                .sum();
    }

    public double getTotalWasteComparedToDemandPercent() {
        return (double) (getTotalBookCoverCopiesProduced() - demand.getTotalCopies()) / demand.getTotalCopies() * 100d;
    }

    /**
     * The correct waste formula (used in TUY14 and CAN19).
     */
    public double getTotalWastePercent() {
//        return (double) getTotalWastedCopies() / getTotalBookCoverCopiesProduced() * 100d;
        return (double) (getTotalBookCoverCopiesProduced() - demand.getTotalCopies()) / getTotalBookCoverCopiesProduced() * 100d;
    }

    public Stream<String> getBookCoverCopiesStr() {
        // for each book cover, calculate its stats
        return demand.stream()
                .sorted()
                .map(bookCover -> {
                    // calculate the actual number of copies of this book cover
                    final int copiesCount = getCopiesCount(bookCover);
                    // calculate the wasted copies of this book cover
                    final int wastedCopies = copiesCount - bookCover.getDemand();
                    // put all these stats in a pretty string
                    return String.format("%3d: %6d / %6d (%5d extra)",
                            bookCover.getId(), copiesCount, bookCover.getDemand(), wastedCopies
                    );
                });
    }

    public boolean isFeasible() {
        final List<Copies> realizedCopies = getRealizedCopies()
                .collect(Collectors.toList());
        // verify for each book cover that the number of copies realized is sufficient to satisfy its demand
        return demand.stream()
                .allMatch(bookCover -> {
                    final int realizedCopiesCount = realizedCopies.stream()
                            .filter(copies -> copies.getBookCover().equals(bookCover))
                            .findFirst()
                            .orElse(new Copies(bookCover, 0))
                            .getCopiesCount();
                    return realizedCopiesCount >= bookCover.getDemand();
                });
    }

    public void forEachPlate(Consumer<? super Plate> consumer) {
        plates.forEach(consumer);
    }

    public void randomize(RandomCppSolutionFactory randomCppSolutionFactory) {
        final CppSolution newRandSolution = randomCppSolutionFactory.newCppSolution();
        plates.clear();
        plates.addAll(newRandSolution.plates);
    }

    public void mergePlatesByTwo() {
        List<Plate> oldPlates = new ArrayList<>(plates);
        plates.clear();
        for (int i = 0; i < oldPlates.size(); i += 2) {
            plates.add(
                    oldPlates.get(i).mergeWith(oldPlates.get(i + 1))
            );
        }
    }

    @Override
    public String toString() {
        final String strPlates = plates.stream()
                .sorted(Comparator.reverseOrder())
                .map(Object::toString)
                .collect(Collectors.joining(System.lineSeparator()));

        final String strBookCoverCopies = getBookCoverCopiesStr()
                .collect(Collectors.joining(System.lineSeparator()));

        return String.format("Demand fulfillment:%n" +
                        "%s%n" +
                        "Plates configuration (%d offset plates):%n" +
                        "%s%n" +
                        "Feasible solution: %s%n" +
                        "Total printings: %d%n" +
                        "Total copies: %d (%d requested)%n" +
                        "Total waste: %.4f %%",
                strBookCoverCopies,
                plates.size(),
                strPlates,
                isFeasible(),
                getPrintingsCount(),
                getTotalBookCoverCopiesProduced(), demand.getTotalCopies(),
                getTotalWastePercent()
        );
    }
}
