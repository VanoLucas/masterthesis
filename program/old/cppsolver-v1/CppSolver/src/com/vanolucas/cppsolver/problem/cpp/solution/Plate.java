package com.vanolucas.cppsolver.problem.cpp.solution;

import com.vanolucas.cppsolver.problem.cpp.eval.Slots;
import com.vanolucas.cppsolver.problem.cpp.instance.BookCover;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Plate implements Comparable<Plate> {
    private final List<BookCover> bookCovers;
    private int printings;

    public Plate(Slots slots) {
        this(
                slots.streamBookCovers().collect(Collectors.toList()),
                slots.getMaxCopiesCount()
        );
    }

    public Plate(List<BookCover> bookCovers, int printings) {
        this.bookCovers = new ArrayList<>(bookCovers);

        if (printings < 0) {
            throw new IllegalArgumentException("The number of printings of an offset plate must be positive.");
        }
        this.printings = printings;
    }

    public Stream<BookCover> streamBookCovers() {
        return bookCovers.stream();
    }

    public int getSlotsCount() {
        return bookCovers.size();
    }

    public int countSlotsWithBookCover(BookCover bookCover) {
        return (int) bookCovers.stream()
                .filter(bc -> bc.equals(bookCover))
                .count();
    }

    public int getPrintings() {
        return printings;
    }

    @Override
    public int compareTo(Plate other) {
        return Integer.compare(printings, other.printings);
    }

    @Override
    public String toString() {
        return String.format("%s : %6d printings",
                bookCovers.stream()
                        .sorted()
                        .map(bookCover -> String.format("%3d", bookCover.getId()))
                        .collect(Collectors.joining(" | ", "| ", " |")),
                printings
        );
    }

    public Plate mergeWith(Plate other) {
        return new Plate(
                new ArrayList<BookCover>(bookCovers.size() * 2) {{
                    addAll(bookCovers);
                    addAll(other.bookCovers);
                }},
                Math.max(printings, other.printings)
        );
    }

    public Plate mergeWith(List<Plate> others) {
        return new Plate(
                others.stream()
                        .flatMap(Plate::streamBookCovers)
                        .collect(Collectors.toList()),
                Math.max(
                        printings,
                        others.stream()
                                .mapToInt(Plate::getPrintings)
                                .max()
                                .orElseThrow(() -> new IllegalStateException(
                                        "Other plates need to be provided in order for the merge operation to be performed."
                                ))
                )
        );
    }
}
