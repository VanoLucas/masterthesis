package com.vanolucas.cppsolver.problem.cpp.solver;

import java.time.Duration;

class InputParams {
    private final int slotsPerPlateCount;
    private final int platesCount;

    private final Duration restartAfterDurationWithoutImprovement;
    private final Duration runDuration;

    static InputParams fromArgs(String[] args) {
        if (args.length < 2) {
            throw new IllegalArgumentException(
                    "The number of slots per plate (S) and the number of offset plates (n) must be provided as args like this: " +
                            "java -jar CppSolver.jar 4 10"
            );
        } else if (args.length == 2) {
            return new InputParams(
                    Integer.parseInt(args[0]),
                    Integer.parseInt(args[1])
            );
        } else if (args.length == 3) {
            return new InputParams(
                    Integer.parseInt(args[0]),
                    Integer.parseInt(args[1]),
                    Duration.ofSeconds(Long.parseLong(args[2]))
            );
        } else {
            return new InputParams(
                    Integer.parseInt(args[0]),
                    Integer.parseInt(args[1]),
                    Duration.ofSeconds(Long.parseLong(args[2])),
                    Duration.ofSeconds(Long.parseLong(args[3]))
            );
        }
    }

    private InputParams(int slotsPerPlateCount, int platesCount) {
        this(slotsPerPlateCount, platesCount, Duration.ofSeconds(20L));
    }

    private InputParams(int slotsPerPlateCount, int platesCount, Duration restartAfterDurationWithoutImprovement) {
        this(slotsPerPlateCount, platesCount, restartAfterDurationWithoutImprovement, null);
    }

    private InputParams(int slotsPerPlateCount, int platesCount, Duration restartAfterDurationWithoutImprovement, Duration runDuration) {
        this.slotsPerPlateCount = slotsPerPlateCount;
        this.platesCount = platesCount;
        this.restartAfterDurationWithoutImprovement = restartAfterDurationWithoutImprovement;
        this.runDuration = runDuration;
    }

    int getSlotsPerPlateCount() {
        return slotsPerPlateCount;
    }

    int getPlatesCount() {
        return platesCount;
    }

    Duration getRestartAfterDurationWithoutImprovement() {
        return restartAfterDurationWithoutImprovement;
    }

    Duration getRunDuration() {
        return runDuration;
    }

    static void printUsage() {
        System.out.println(String.format(
                "#################%n" +
                        "### CppSolver ###%n" +
                        "#################%n"
        ));
        System.out.println(
                "Usage: java -jar CppSolver.jar " +
                        "<nb of slots per plate> " +
                        "<number of plates> " +
                        "<random-restart hill climbing when no improvement for (seconds)> " +
                        "<max run duration (seconds)>"
        );
    }
}
