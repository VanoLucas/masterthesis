package com.vanolucas.cppsolver.problem.cpp.solver;

import com.vanolucas.cppsolver.clone.CloneableCloner;
import com.vanolucas.cppsolver.clone.EncodedSolutionCloner;
import com.vanolucas.cppsolver.clone.SolutionCloner;
import com.vanolucas.cppsolver.eval.EncodedSolutionEvaluator;
import com.vanolucas.cppsolver.eval.SolutionEvaluator;
import com.vanolucas.cppsolver.mutation.FirstEncodedRepresentationSolutionMutator;
import com.vanolucas.cppsolver.mutation.NeighborsProducer;
import com.vanolucas.cppsolver.mutation.SolutionMutator;
import com.vanolucas.cppsolver.opti.Opti;
import com.vanolucas.cppsolver.opti.RestartParallelSteepestAscentHillClimbing;
import com.vanolucas.cppsolver.problem.cpp.eval.CppSolutionEvaluator;
import com.vanolucas.cppsolver.problem.cpp.eval.SlotsDecoder;
import com.vanolucas.cppsolver.problem.cpp.genome.GenomeCf;
import com.vanolucas.cppsolver.problem.cpp.genome.decode.GenomeCfDecoder;
import com.vanolucas.cppsolver.problem.cpp.instance.Demand;
import com.vanolucas.cppsolver.problem.cpp.instance.DemandFileChooser;
import com.vanolucas.cppsolver.problem.cpp.instance.StandardCppInstance;
import com.vanolucas.cppsolver.rand.Rand;
import com.vanolucas.cppsolver.rand.RandomizableRandomizer;
import com.vanolucas.cppsolver.rkv.RandomKeyVector;
import com.vanolucas.cppsolver.rkv.RandomizeOneKeyRkvMutator;
import com.vanolucas.cppsolver.solution.EncodedSolution;
import com.vanolucas.cppsolver.solution.Solution;
import com.vanolucas.cppsolver.solution.keeper.SingleEarliestBestSolutionKeeper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Solver {
    public static void main(String[] args) throws IOException {
        // print usage
        InputParams.printUsage();

        // read params from cmd line args
        InputParams inputParams = InputParams.fromArgs(args);

        final int platesCount = inputParams.getPlatesCount();
        final int slotsPerPlateCount = inputParams.getSlotsPerPlateCount();

        // select CPP instance file and load demand
        final Demand demand = DemandFileChooser.chooseFileAndGetDemand();
        StandardCppInstance cppInstance = new StandardCppInstance(
                demand, platesCount, slotsPerPlateCount
        );
        System.out.println(cppInstance);

        // random genome supplier
        Supplier<RandomKeyVector> genomeSupplier = () -> new GenomeCf(cppInstance);

        Rand rand = new Rand();

        Supplier<RandomKeyVector> randomGenomeSupplier = () -> {
            RandomKeyVector genome = genomeSupplier.get();
            genome.randomize(rand);
            return genome;
        };

        // random solution supplier
        Supplier<Solution> initialSolutionsSupplier = () -> new EncodedSolution(
                2, randomGenomeSupplier.get()
        );

        // neighboring operators
        final int genomeLength = GenomeCf.calculateLengthForCppInstance(cppInstance);
        List<SolutionMutator> solutionMutators = new ArrayList<>();
        if (slotsPerPlateCount > 4) {
            // seems good to have full randomization operators for instances with high slotsPerPlate count
            solutionMutators.addAll(Arrays.asList(
                    new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer()),
                    new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer())
            ));
        }
        solutionMutators.addAll(IntStream.range(0, genomeLength)
                .mapToObj(keyIndex -> new FirstEncodedRepresentationSolutionMutator(new RandomizeOneKeyRkvMutator(keyIndex)))
                .collect(Collectors.toList())
        );

        SolutionCloner solutionCloner = new EncodedSolutionCloner(
                new CloneableCloner()
        );

        NeighborsProducer neighborsProducer = new NeighborsProducer(
                solutionMutators, solutionCloner
        );

        // evaluation function
        SolutionEvaluator solutionEvaluator = new EncodedSolutionEvaluator(
                new GenomeCfDecoder(cppInstance),
                new SlotsDecoder(cppInstance),
                new CppSolutionEvaluator(cppInstance)
        );

        // thread pool
        ExecutorService executor = Executors.newFixedThreadPool(
                Runtime.getRuntime().availableProcessors()
        );

        // create the random-restart hill climbing algo
        Opti opti = new RestartParallelSteepestAscentHillClimbing(
                initialSolutionsSupplier, neighborsProducer, solutionEvaluator, executor, inputParams.getRestartAfterDurationWithoutImprovement()
        );

        // best solution logger
        SingleEarliestBestSolutionKeeper keeper = new SingleEarliestBestSolutionKeeper(true);
        opti.addOnSolutionEvaluatedListener(keeper::submit);

        // run optimization
        if (inputParams.getRunDuration() != null) {
            opti.runForAtMost(
                    inputParams.getRunDuration()
            );
        } else {
            opti.run();
        }

        // stop thread pool
        executor.shutdown();

        // print stats
        System.out.println("\n########################################\n");
        System.out.println("CPP instance:");
        System.out.println(cppInstance);

        keeper.printBest("\nBest solution found:");

        System.out.println("\nCppSolver run stats:");
        opti.printRunStats();
    }
}
