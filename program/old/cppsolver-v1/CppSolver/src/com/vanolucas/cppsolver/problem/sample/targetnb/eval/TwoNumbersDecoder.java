package com.vanolucas.cppsolver.problem.sample.targetnb.eval;

import com.vanolucas.cppsolver.eval.Decoder;
import com.vanolucas.cppsolver.problem.sample.targetnb.genome.TwoNumbers;
import com.vanolucas.cppsolver.problem.sample.targetnb.solution.Value;

public class TwoNumbersDecoder implements Decoder<TwoNumbers, Value> {
    @Override
    public Value decode(TwoNumbers twoNumbers) {
//        System.out.println(twoNumbers);
        return new Value(twoNumbers.getN1() + twoNumbers.getN2());
    }
}
