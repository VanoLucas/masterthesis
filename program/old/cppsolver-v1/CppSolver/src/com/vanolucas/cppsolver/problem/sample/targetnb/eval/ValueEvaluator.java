package com.vanolucas.cppsolver.problem.sample.targetnb.eval;

import com.vanolucas.cppsolver.eval.Evaluator;
import com.vanolucas.cppsolver.problem.sample.targetnb.instance.TnpInstance;
import com.vanolucas.cppsolver.problem.sample.targetnb.solution.Value;
import com.vanolucas.cppsolver.quality.IntCost;

import java.util.concurrent.TimeUnit;

public class ValueEvaluator implements Evaluator<Value, IntCost> {
    private final int target;

    public ValueEvaluator(TnpInstance tnpInstance) {
        this(tnpInstance.getTarget());
    }

    public ValueEvaluator(int target) {
        this.target = target;
    }

    @Override
    public IntCost evaluate(Value value) {
        System.out.println(String.format("Eval: %s [start]", value));
        try {
            TimeUnit.MILLISECONDS.sleep(10L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(String.format("Eval: %s [end]", value));
        return new IntCost(
                value.getDistanceTo(target)
        );
    }
}
