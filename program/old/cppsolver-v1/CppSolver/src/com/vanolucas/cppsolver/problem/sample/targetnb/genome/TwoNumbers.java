package com.vanolucas.cppsolver.problem.sample.targetnb.genome;

import com.vanolucas.cppsolver.clone.Cloneable;

public class TwoNumbers implements Cloneable {
    private int n1;
    private int n2;

    public TwoNumbers(int n1, int n2) {
        this.n1 = n1;
        this.n2 = n2;
    }

    public int getN1() {
        return n1;
    }

    public int getN2() {
        return n2;
    }

    public void addToN1(int toAdd) {
        n1 += toAdd;
    }

    public void addToN2(int toAdd) {
        n2 += toAdd;
    }

    @Override
    public TwoNumbers clone() throws CloneNotSupportedException {
        return (TwoNumbers) super.clone();
    }

    @Override
    public String toString() {
        return String.format("%d;%d", n1, n2);
    }
}
