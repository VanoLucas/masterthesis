package com.vanolucas.cppsolver.problem.sample.targetnb.instance;

import com.vanolucas.cppsolver.problem.sample.targetnb.genome.TwoNumbers;
import com.vanolucas.cppsolver.problem.sample.targetnb.solution.Value;
import com.vanolucas.cppsolver.solution.EncodedSolution;
import com.vanolucas.cppsolver.solution.Solution;

public class TnpInstance {
    private final int target;

    public TnpInstance(int target) {
        this.target = target;
    }

    public int getTarget() {
        return target;
    }

    public Solution newSolution() {
        return new Solution(new Value(0));
    }

    public EncodedSolution newEncodedSolution() {
        return new EncodedSolution(
                new TwoNumbers(0, 0)
        );
    }
}
