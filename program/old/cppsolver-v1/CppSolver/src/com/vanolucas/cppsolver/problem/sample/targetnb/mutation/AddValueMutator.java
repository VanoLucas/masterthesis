package com.vanolucas.cppsolver.problem.sample.targetnb.mutation;

import com.vanolucas.cppsolver.mutation.Mutator;
import com.vanolucas.cppsolver.problem.sample.targetnb.solution.Value;

import java.util.concurrent.TimeUnit;

public class AddValueMutator implements Mutator<Value> {
    private final int valueToAdd;

    public AddValueMutator(int valueToAdd) {
        this.valueToAdd = valueToAdd;
    }

    @Override
    public void mutate(Value value) {
        System.out.println(String.format("Mutate: %s [start]", value));
        try {
            TimeUnit.MILLISECONDS.sleep(10L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        value.addToValue(valueToAdd);
        System.out.println(String.format("Mutate: %s [end]", value));
    }
}
