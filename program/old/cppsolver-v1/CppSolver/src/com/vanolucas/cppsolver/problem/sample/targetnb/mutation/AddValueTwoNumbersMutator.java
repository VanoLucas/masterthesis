package com.vanolucas.cppsolver.problem.sample.targetnb.mutation;

import com.vanolucas.cppsolver.mutation.Mutator;
import com.vanolucas.cppsolver.problem.sample.targetnb.genome.TwoNumbers;

public class AddValueTwoNumbersMutator implements Mutator<TwoNumbers> {
    private final int valueToAdd1;
    private final int valueToAdd2;

    public AddValueTwoNumbersMutator(int valueToAdd1, int valueToAdd2) {
        this.valueToAdd1 = valueToAdd1;
        this.valueToAdd2 = valueToAdd2;
    }

    @Override
    public void mutate(TwoNumbers twoNumbers) {
        System.out.println(String.format("Mutate: %s [start]", twoNumbers));
        twoNumbers.addToN1(valueToAdd1);
        twoNumbers.addToN2(valueToAdd2);
        System.out.println(String.format("Mutate: %s [end]", twoNumbers));
    }
}
