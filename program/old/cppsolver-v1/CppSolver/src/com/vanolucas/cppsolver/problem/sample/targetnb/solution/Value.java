package com.vanolucas.cppsolver.problem.sample.targetnb.solution;

import com.vanolucas.cppsolver.clone.Cloneable;

public class Value implements Cloneable {
    private int value;

    public Value(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public int getDistanceTo(int target) {
        return Math.abs(value - target);
    }

    public void addToValue(int toAdd) {
        value += toAdd;
    }

    @Override
    public Value clone() throws CloneNotSupportedException {
        return (Value) super.clone();
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
