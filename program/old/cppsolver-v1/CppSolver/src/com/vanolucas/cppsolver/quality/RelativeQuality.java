package com.vanolucas.cppsolver.quality;

public enum RelativeQuality {
    BETTER, EQUIVALENT, WORSE
}
