package com.vanolucas.cppsolver.rand;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

/**
 * Conveniently provides random features based on a regular Java Random numbers generator.
 */
public class Rand {

    /**
     * Random numbers generator.
     */
    private final Random random;

    public Rand() {
        this(new Random());
    }

    public Rand(long seed) {
        this(new Random(seed));
    }

    public Rand(Random random) {
        this.random = random;
    }

    public int int0To(int maxExclusive) {
        return random.nextInt(maxExclusive);
    }

    public double doubleBetween(double minInclusive, double maxExclusive) {
        return minInclusive + (maxExclusive - minInclusive) * random.nextDouble();
    }

    /**
     * @return Random double in the range 0.0d (inclusive) to 1.0d (exclusive).
     */
    public double double01() {
        return random.nextDouble();
    }

    public DoubleStream streamDoubles01(long count) {
        return random.doubles(count);
    }

    public List<Double> doubles01(long count) {
        return streamDoubles01(count)
                .boxed()
                .collect(Collectors.toList());
    }

    /**
     * @return One item at random from the provided list.
     */
    public <T> T uniformPick(List<T> items) {
        return items.get(
                int0To(items.size())
        );
    }

    /**
     * @return itemsCount items at random from the provided list. The same item may be picked multiple times.
     */
    public <T> List<T> uniformPick(List<T> items, int itemsCount) {
        final int size = items.size();
        return IntStream.range(0, itemsCount)
                .mapToObj(i -> items.get(
                        int0To(size)
                ))
                .collect(Collectors.toList());
    }

    /**
     * @param keepProba Probability that an item is kept.
     * @return List of items that contains a subset of the initial item list.
     */
    public <T> List<T> keepWithProba(List<T> items, double keepProba) {
        return items.stream()
                .filter(item -> double01() < keepProba)
                .collect(Collectors.toList());
    }
}

