package com.vanolucas.cppsolver.rand;

public interface Randomizable {
    default void randomize() {
        randomize(new Rand());
    }

    void randomize(Rand rand);
}
