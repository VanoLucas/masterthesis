package com.vanolucas.cppsolver.rand;

public class RandomizableRandomizer implements Randomizer<Randomizable> {
    private final Rand rand;

    public RandomizableRandomizer() {
        this(new Rand());
    }

    public RandomizableRandomizer(Rand rand) {
        this.rand = rand;
    }

    @Override
    public void randomize(Randomizable toRandomize) {
        randomize(toRandomize, rand);
    }

    @Override
    public void randomize(Randomizable toRandomize, Rand rand) {
        toRandomize.randomize(rand);
    }
}
