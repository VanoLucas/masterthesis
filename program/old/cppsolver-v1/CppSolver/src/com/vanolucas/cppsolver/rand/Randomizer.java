package com.vanolucas.cppsolver.rand;

import com.vanolucas.cppsolver.mutation.Mutator;

public interface Randomizer<T> extends Mutator<T> {
    @Override
    default void mutate(T toMutate) {
        randomize(toMutate);
    }

    default void randomize(T toRandomize) {
        randomize(toRandomize, new Rand());
    }

    void randomize(T toRandomize, Rand rand);
}
