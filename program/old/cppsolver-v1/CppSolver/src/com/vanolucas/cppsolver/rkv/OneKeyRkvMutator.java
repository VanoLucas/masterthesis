package com.vanolucas.cppsolver.rkv;

public abstract class OneKeyRkvMutator implements RkvMutator {
    private final Integer keyIndex;

    public OneKeyRkvMutator() {
        this(null);
    }

    public OneKeyRkvMutator(Integer keyIndex) {
        this.keyIndex = keyIndex;
    }

    @Override
    public void mutate(RandomKeyVector rkv) {
        if (keyIndex == null) {
            throw new IllegalStateException("Cannot mutate one random key vector key if the index of the key to mutate is not provided.");
        }
        mutateKey(keyIndex, rkv);
    }

    public abstract void mutateKey(int keyIndex, RandomKeyVector rkv);
}
