package com.vanolucas.cppsolver.rkv;

import com.vanolucas.cppsolver.rand.Rand;

public class OneRandomKeyRkvMutator implements RkvMutator {
    private final OneKeyRkvMutator keyMutator;
    private final Rand rand;

    public OneRandomKeyRkvMutator(OneKeyRkvMutator keyMutator) {
        this(keyMutator, new Rand());
    }

    public OneRandomKeyRkvMutator(OneKeyRkvMutator keyMutator, Rand rand) {
        this.keyMutator = keyMutator;
        this.rand = rand;
    }

    @Override
    public void mutate(RandomKeyVector rkv) {
        mutateOneRandomKey(rkv, rand);
    }

    public void mutateOneRandomKey(RandomKeyVector rkv, Rand rand) {
        // choose one random key to mutate
        final int keyIndex = rand.int0To(rkv.size());
        // mutate the chosen key
        keyMutator.mutateKey(keyIndex, rkv);
    }
}
