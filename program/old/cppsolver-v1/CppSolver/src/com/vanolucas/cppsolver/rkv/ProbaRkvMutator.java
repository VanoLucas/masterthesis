package com.vanolucas.cppsolver.rkv;

import com.vanolucas.cppsolver.rand.Rand;

import java.util.List;

public class ProbaRkvMutator implements RkvMutator {
    private final OneKeyRkvMutator keyMutator;
    private final double proba;
    private final Rand rand;

    public ProbaRkvMutator(OneKeyRkvMutator keyMutator, double proba) {
        this(keyMutator, proba, new Rand());
    }

    public ProbaRkvMutator(OneKeyRkvMutator keyMutator, double proba, Rand rand) {
        this.keyMutator = keyMutator;
        this.proba = proba;
        this.rand = rand;
    }

    @Override
    public void mutate(RandomKeyVector rkv) {
        mutateKeysWithProba(rkv, rand);
    }

    public void mutateKeysWithProba(RandomKeyVector rkv, Rand rand) {
        final int size = rkv.size();
        final List<Double> decisions = rand.doubles01(size);
        for (int keyIndex = 0; keyIndex < size; keyIndex++) {
            if (decisions.get(keyIndex) < proba) {
                keyMutator.mutateKey(keyIndex, rkv);
            }
        }
    }
}
