package com.vanolucas.cppsolver.rkv;

import com.vanolucas.cppsolver.clone.Cloneable;
import com.vanolucas.cppsolver.rand.Rand;
import com.vanolucas.cppsolver.rand.Randomizable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

public class RandomKeyVector implements Randomizable, Cloneable {
    private List<Double> keys;

    public RandomKeyVector(int length) {
        this(Collections.nCopies(length, 0d));
    }

    public RandomKeyVector(List<Double> keys) {
        this.keys = new ArrayList<>(keys);
    }

    public double getKey(int index) {
        return keys.get(index);
    }

    public void setKey(int index, Double value) {
        keys.set(index, value);
    }

    public void setKeyValues(List<Double> keyValues) {
        keys.clear();
        keys.addAll(keyValues);
    }

    public int size() {
        return keys.size();
    }

    public DoubleStream doubleStream() {
        return keys.stream().mapToDouble(__ -> __);
    }

    public String getKeysStr() {
        return keys.stream()
                .map(Object::toString)
                .collect(Collectors.joining(" ; "));
    }

    @Override
    public void randomize(Rand rand) {
        keys = rand.doubles01(keys.size());
    }

    public void randomizeKey(int index, Rand rand) {
        keys.set(index, rand.double01());
    }

    @Override
    public RandomKeyVector clone() throws CloneNotSupportedException {
        RandomKeyVector cloned = (RandomKeyVector) super.clone();
        cloned.keys = new ArrayList<>(keys);
        return cloned;
    }

    @Override
    public String toString() {
        return String.format("(%d keys) %s", keys.size(), getKeysStr());
    }
}
