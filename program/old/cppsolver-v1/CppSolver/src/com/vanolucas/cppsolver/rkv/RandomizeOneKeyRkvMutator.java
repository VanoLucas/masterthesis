package com.vanolucas.cppsolver.rkv;

import com.vanolucas.cppsolver.rand.Rand;

public class RandomizeOneKeyRkvMutator extends OneKeyRkvMutator {
    private final Rand rand;

    public RandomizeOneKeyRkvMutator() {
        this(new Rand());
    }

    public RandomizeOneKeyRkvMutator(Rand rand) {
        this.rand = rand;
    }

    public RandomizeOneKeyRkvMutator(Integer keyIndex) {
        this(keyIndex, new Rand());
    }

    public RandomizeOneKeyRkvMutator(Integer keyIndex, Rand rand) {
        super(keyIndex);
        this.rand = rand;
    }

    @Override
    public void mutateKey(int keyIndex, RandomKeyVector rkv) {
        randomizeKey(keyIndex, rkv, rand);
    }

    public void randomizeKey(int keyIndex, RandomKeyVector rkv, Rand rand) {
        rkv.randomizeKey(keyIndex, rand);
    }
}
