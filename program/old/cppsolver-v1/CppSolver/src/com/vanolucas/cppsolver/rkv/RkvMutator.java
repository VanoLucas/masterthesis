package com.vanolucas.cppsolver.rkv;

import com.vanolucas.cppsolver.mutation.Mutator;

public interface RkvMutator extends Mutator<RandomKeyVector> {
}
