package com.vanolucas.cppsolver.solution;

import com.vanolucas.cppsolver.clone.Cloner;
import com.vanolucas.cppsolver.eval.Decoder;
import com.vanolucas.cppsolver.mutation.Mutator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Optimization solution that is described using one or more encoded representations.
 * Each representation is an encoded version of the next one, all the way to the phenotype.
 */
public class EncodedSolution extends Solution {
    private final List<Object> encodedRepresentations;

    public EncodedSolution(int encodedRepresentationsCount) {
        this(Collections.nCopies(encodedRepresentationsCount, null), null);
    }

    public EncodedSolution(int encodedRepresentationsCount, Object firstEncodedRepresentation) {
        this(Collections.nCopies(encodedRepresentationsCount, null));
        encodedRepresentations.set(0, firstEncodedRepresentation);
    }

    public EncodedSolution(Object firstEncodedRepresentation) {
        this(Collections.singletonList(firstEncodedRepresentation));
    }

    public EncodedSolution(List<Object> encodedRepresentations) {
        this(encodedRepresentations, null);
    }

    public EncodedSolution(List<Object> encodedRepresentations, Object phenotype) {
        super(phenotype);
        this.encodedRepresentations = new ArrayList<>(encodedRepresentations);
    }

    public Object getEncodedRepresentation(int index) {
        return encodedRepresentations.get(index);
    }

    public int getEncodedRepresentationsSize() {
        return encodedRepresentations.size();
    }

    private boolean isLastRepresentationBeforePhenotype(int representationIndex) {
        return representationIndex == encodedRepresentations.size() - 1;
    }

    public void mutateEncodedRepresentation(int representationIndex, Mutator mutator) {
        mutator.mutate(encodedRepresentations.get(representationIndex));
    }

    public void decodeRepresentations(List<Decoder> decoders) {
        int i = 0;
        for (; i < decoders.size(); i++) {
            decodeRepresentation(i, decoders.get(i));
        }
    }

    public void decodeRepresentation(int representationIndex, Decoder decoder) {
        Object toDecode = encodedRepresentations.get(representationIndex);
        Object decoded = decoder.decode(toDecode);

        if (isLastRepresentationBeforePhenotype(representationIndex)) {
            phenotype = decoded;
        } else {
            encodedRepresentations.set(representationIndex + 1, decoded);
        }
    }

    /**
     * Clone this solution including only its phenotype.
     */
    @Override
    public EncodedSolution clone(Cloner phenotypeCloner) {
        return new EncodedSolution(
                Collections.nCopies(encodedRepresentations.size(), null),
                clonePhenotype(phenotypeCloner)
        );
    }

    /**
     * Clone this solution including only its encoded representations.
     */
    public EncodedSolution clone(List<Cloner> encodedRepresentationCloners) {
        return new EncodedSolution(
                cloneEncodedRepresentations(encodedRepresentationCloners),
                null
        );
    }

    /**
     * Clone this solution including its encoded representations and phenotype.
     */
    public EncodedSolution clone(List<Cloner> encodedRepresentationCloners, Cloner phenotypeCloner) {
        return new EncodedSolution(
                cloneEncodedRepresentations(encodedRepresentationCloners),
                clonePhenotype(phenotypeCloner)
        );
    }

    public List<Object> cloneEncodedRepresentations(List<Cloner> encodedRepresentationCloners) {
        // clone each encoded representation using the corresponding cloner
        return IntStream.range(0, encodedRepresentations.size())
                .mapToObj(i -> {
                    // if we have a cloner for this representation, clone it, otherwise put null
                    if (i < encodedRepresentationCloners.size()) {
                        final Cloner cloner = encodedRepresentationCloners.get(i);
                        return cloneEncodedRepresentation(i, cloner);
                    }
                    // else if we have no cloner for this representation, put null
                    else {
                        return null;
                    }
                })
                .collect(Collectors.toList());
    }

    public Object cloneEncodedRepresentation(int encodedRepresentationIndex, Cloner cloner) {
        final Object representationToClone = encodedRepresentations.get(encodedRepresentationIndex);
        if (cloner == null || representationToClone == null) {
            return null;
        } else {
            return cloner.clone(representationToClone);
        }
    }

    @Override
    public String toString() {
        final String strEncodedRepresentations = IntStream.range(0, encodedRepresentations.size())
                .mapToObj(i -> String.format("Encoded representation %d (%s):%n%s",
                        i + 1,
                        encodedRepresentations.get(i).getClass().getSimpleName(),
                        encodedRepresentations.get(i).toString()
                ))
                .collect(Collectors.joining(System.lineSeparator()));
        final String strPhenotypeClass;
        Class<?> phenotypeClass = getPhenotypeClass();
        if (phenotypeClass == null) {
            strPhenotypeClass = "null";
        } else {
            strPhenotypeClass = phenotypeClass.getSimpleName();
        }
        return String.format("%s%n" +
                        "Phenotype (%s):%n" +
                        "%s",
                strEncodedRepresentations,
                strPhenotypeClass,
                phenotypeStr()
        );
    }
}
