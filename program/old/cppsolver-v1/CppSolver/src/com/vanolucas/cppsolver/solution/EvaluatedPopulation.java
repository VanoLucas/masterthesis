package com.vanolucas.cppsolver.solution;

import com.vanolucas.cppsolver.rand.Rand;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Stores a list of evaluated solutions.
 */
public class EvaluatedPopulation {
    private List<EvaluatedSolution> solutions;

    public EvaluatedPopulation(List<EvaluatedSolution> solutions) {
        this.solutions = new ArrayList<>(solutions);
    }

    public EvaluatedSolution get(int index) {
        return solutions.get(index);
    }

    public int size() {
        return solutions.size();
    }

    public EvaluatedPopulation pickRand(int count) {
        return pickRand(count, new Rand());
    }

    public EvaluatedPopulation pickRand(int count, Rand rand) {
        return new EvaluatedPopulation(
                rand.uniformPick(solutions, count)
        );
    }

    public EvaluatedPopulation concat(EvaluatedPopulation other) {
        return new EvaluatedPopulation(
                new ArrayList<EvaluatedSolution>(solutions.size() + other.solutions.size()) {{
                    addAll(solutions);
                    addAll(other.solutions);
                }}
        );
    }

    public void keepBest(int count) {
        solutions = solutions.stream()
                .sorted(Comparator.reverseOrder())
                .limit(count)
                .collect(Collectors.toList());
    }
}
