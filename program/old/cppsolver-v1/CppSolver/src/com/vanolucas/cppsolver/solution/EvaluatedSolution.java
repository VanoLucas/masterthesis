package com.vanolucas.cppsolver.solution;

import com.vanolucas.cppsolver.quality.Quality;

/**
 * A solution and its quality.
 */
public class EvaluatedSolution implements Comparable<EvaluatedSolution> {
    private final Solution solution;
    private final Quality quality;

    public EvaluatedSolution(Solution solution, Quality quality) {
        this.solution = solution;
        this.quality = quality;
    }

    public Solution getSolution() {
        return solution;
    }

    public Quality getQuality() {
        return quality;
    }

    public boolean isBetterThan(EvaluatedSolution other) {
        if (other == null) {
            return true;
        }
        return quality.isBetterThan(other.quality);
    }

    public boolean isBetterOrEquivalentTo(EvaluatedSolution other) {
        if (other == null) {
            return true;
        }
        return quality.isBetterOrEquivalentTo(other.quality);
    }

    @Override
    public int compareTo(EvaluatedSolution other) {
        return quality.compareTo(other.quality);
    }

    @Override
    public String toString() {
        return String.format("%s%n" +
                        "%s",
                solution,
                quality
        );
    }
}
