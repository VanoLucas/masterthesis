package com.vanolucas.cppsolver.solution;

import com.vanolucas.cppsolver.opti.evolutionary.Breeder;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EvaluatedSolutionGroups {
    private final List<EvaluatedPopulation> groups;

    public EvaluatedSolutionGroups(List<EvaluatedPopulation> groups) {
        this.groups = new ArrayList<>(groups);
    }

    public Population breedUsing(Breeder breeder) {
        return new Population(groups.stream()
                // breed each group using the provided breeder
                .flatMap(parents -> breeder.breed(parents).stream())
                // collect all children
                .collect(Collectors.toList())
        );
    }
}
