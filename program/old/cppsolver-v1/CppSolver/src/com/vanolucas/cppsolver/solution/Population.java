package com.vanolucas.cppsolver.solution;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Stores a list of solutions.
 */
public class Population {
    private final List<Solution> solutions;

    public Population(List<Solution> solutions) {
        this.solutions = new ArrayList<>(solutions);
    }

    public Solution get(int index) {
        return solutions.get(index);
    }

    public Stream<Solution> stream() {
        return solutions.stream();
    }

    public void forEach(Consumer<? super Solution> consumer) {
        solutions.forEach(consumer);
    }

    @Override
    public String toString() {
        return IntStream.range(0, solutions.size())
                .mapToObj(i -> String.format("Individual %4d:%n%s",
                        i + 1,
                        solutions.get(i).toString()
                ))
                .collect(Collectors.joining(System.lineSeparator()));
    }

    public void print() {
        System.out.println(toString());
    }
}
