package com.vanolucas.cppsolver.solution;

import com.vanolucas.cppsolver.clone.Cloner;
import com.vanolucas.cppsolver.eval.Evaluator;
import com.vanolucas.cppsolver.mutation.Mutator;
import com.vanolucas.cppsolver.quality.Quality;

/**
 * A solution in optimization.
 * It contains at least its phenotype (final) representation.
 */
public class Solution {
    protected Object phenotype;

    public Solution(Object phenotype) {
        this.phenotype = phenotype;
    }

    public void mutatePhenotype(Mutator mutator) {
        mutator.mutate(phenotype);
    }

    public Quality evaluatePhenotype(Evaluator evaluator) {
        return evaluator.evaluate(phenotype);
    }

    public Solution clone(Cloner phenotypeCloner) {
        return new Solution(
                clonePhenotype(phenotypeCloner)
        );
    }

    public Object clonePhenotype(Cloner cloner) {
        return cloner.clone(phenotype);
    }

    public Class<?> getPhenotypeClass() {
        if (phenotype == null) {
            return null;
        }
        return phenotype.getClass();
    }

    public String phenotypeStr() {
        if (phenotype == null) {
            return "null";
        }
        return phenotype.toString();
    }

    @Override
    public String toString() {
        return phenotypeStr();
    }
}
