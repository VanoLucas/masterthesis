package com.vanolucas.cppsolver.solution.keeper;

import com.vanolucas.cppsolver.solution.EvaluatedSolution;

public interface Keeper {
    enum Decision {
        KEPT, REJECTED
    }

    Decision submit(EvaluatedSolution candidate);
}
