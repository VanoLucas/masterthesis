package com.vanolucas.cppsolver.solution.keeper;

import com.vanolucas.cppsolver.solution.EvaluatedSolution;

import java.time.Instant;

public abstract class SingleBestSolutionKeeper implements Keeper {
    private EvaluatedSolution best;
    private Instant instantNewBest;
    private final Object lockBest = new Object();
    private final boolean printWhenNewBest;

    public SingleBestSolutionKeeper() {
        this(false);
    }

    public SingleBestSolutionKeeper(boolean printWhenNewBest) {
        this.printWhenNewBest = printWhenNewBest;
    }

    @Override
    public Decision submit(EvaluatedSolution candidate) {
        synchronized (lockBest) {
            if (shouldReplaceBestWith(candidate)) {
                best = candidate;
                instantNewBest = Instant.now();
                if (printWhenNewBest) {
                    printBest("New best:");
                }
                return Decision.KEPT;
            }
        }
        return Decision.REJECTED;
    }

    protected abstract boolean shouldReplaceBestWith(EvaluatedSolution candidate);

    public EvaluatedSolution getBest() {
        synchronized (lockBest) {
            return best;
        }
    }

    public void reset() {
        synchronized (lockBest) {
            best = null;
            instantNewBest = null;
        }
    }

    public void printBest() {
        synchronized (lockBest) {
            System.out.println(best);
        }
    }

    public void printBest(String prefixMsg) {
        synchronized (lockBest) {
            System.out.println(String.format(
                    "%s%n" +
                            "%s%n" +
                            "Time: %s",
                    prefixMsg + (prefixMsg.length() > 0 ? " " : ""),
                    best,
                    instantNewBest
            ));
        }
    }
}
