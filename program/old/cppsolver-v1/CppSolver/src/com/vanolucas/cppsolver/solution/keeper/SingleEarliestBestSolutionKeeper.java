package com.vanolucas.cppsolver.solution.keeper;

import com.vanolucas.cppsolver.solution.EvaluatedSolution;

public class SingleEarliestBestSolutionKeeper extends SingleBestSolutionKeeper {
    public SingleEarliestBestSolutionKeeper() {
    }

    public SingleEarliestBestSolutionKeeper(boolean printWhenNewBest) {
        super(printWhenNewBest);
    }

    @Override
    protected boolean shouldReplaceBestWith(EvaluatedSolution candidate) {
        return candidate.isBetterThan(getBest());
    }
}
