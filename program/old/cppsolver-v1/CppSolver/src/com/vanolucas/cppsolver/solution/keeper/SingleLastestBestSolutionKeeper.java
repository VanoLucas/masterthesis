package com.vanolucas.cppsolver.solution.keeper;

import com.vanolucas.cppsolver.solution.EvaluatedSolution;

public class SingleLastestBestSolutionKeeper extends SingleBestSolutionKeeper {
    public SingleLastestBestSolutionKeeper() {
    }

    public SingleLastestBestSolutionKeeper(boolean printWhenNewBest) {
        super(printWhenNewBest);
    }

    @Override
    protected boolean shouldReplaceBestWith(EvaluatedSolution candidate) {
        return candidate.isBetterOrEquivalentTo(getBest());
    }
}
