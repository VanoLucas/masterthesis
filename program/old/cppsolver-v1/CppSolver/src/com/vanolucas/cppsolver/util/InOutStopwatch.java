package com.vanolucas.cppsolver.util;

import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

public class InOutStopwatch {
    private final String name;

    private Duration durationIn;
    private Duration durationOut;
    private Instant inSince;
    private Instant outSince;

    private Long printEveryCalls;
    private long calls = 0L;

    public InOutStopwatch() {
        this(null, null);
    }

    public InOutStopwatch(Long printEveryCalls) {
        this(null, printEveryCalls);
    }

    public InOutStopwatch(String name) {
        this(name, null);
    }

    public InOutStopwatch(String name, Long printEveryCalls) {
        this.name = name;
        this.printEveryCalls = printEveryCalls;
    }

    public void in() {
        if (outSince != null) {
            if (durationOut == null) {
                durationOut = Duration.ZERO;
            }
            durationOut = durationOut.plus(Duration.between(outSince, Instant.now()));
            outSince = null;
        }
        if (inSince == null) {
            inSince = Instant.now();
        }
    }

    public void out() {
        if (inSince != null) {
            if (durationIn == null) {
                durationIn = Duration.ZERO;
            }
            durationIn = durationIn.plus(Duration.between(inSince, Instant.now()));
            inSince = null;
        }
        if (outSince == null) {
            calls++;
            if (printEveryCalls != null) {
                printEveryCalls(printEveryCalls);
            }
            outSince = Instant.now();
        }
    }

    public void reset() {
        durationIn = null;
        durationOut = null;
        inSince = null;
        outSince = null;
        calls = 0L;
    }

    public void printEveryCalls(long nbCalls) {
        if (calls % nbCalls == 0L) {
            print();
        }
    }

    public void print() {
        System.out.println(this);
    }

    @Override
    public String toString() {
        final double percentIn;
        if (durationIn != null && durationOut != null) {
            percentIn = (double) durationIn.toMillis() / (durationIn.plus(durationOut).toMillis()) * 100d;
        } else {
            percentIn = 0d;
        }
        return String.format("Stopwatch '%30s' %.2f%% of the duration in (in: %s out: %s)",
                name, percentIn, durationIn, durationOut
        );
    }

    private static Map<String, InOutStopwatch> STOPWATCHES;

    public static void in(String stopwatchId) {
        final InOutStopwatch stopwatch = get(stopwatchId);
        stopwatch.in();
    }

    public static void out(String stopwatchId, Long printEveryCalls) {
        final InOutStopwatch stopwatch = get(stopwatchId);
        stopwatch.out();
        stopwatch.printEveryCalls(printEveryCalls);
    }

    public static InOutStopwatch get(String stopwatchId) {
        if (STOPWATCHES == null) {
            STOPWATCHES = new HashMap<>(4);
        }
        final InOutStopwatch stopwatch = STOPWATCHES.get(stopwatchId);
        if (stopwatch == null) {
            STOPWATCHES.put(stopwatchId,
                    new InOutStopwatch(stopwatchId)
            );
            return get(stopwatchId);
        }
        return stopwatch;
    }
}
