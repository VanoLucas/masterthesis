package com.vanolucas.cppsolver.util;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Supplier;

public class Pool<T> {
    private final ConcurrentLinkedQueue<T> pool = new ConcurrentLinkedQueue<>();
    private final Supplier<T> itemSupplier;

    public Pool(Supplier<T> itemSupplier) {
        this.itemSupplier = itemSupplier;
    }

    public int size() {
        return pool.size();
    }

    public T get() {
        T item = pool.poll();
        while (item == null) {
            pool.offer(itemSupplier.get());
            item = pool.poll();
        }
        return item;
    }

    public void release(T item) {
        pool.offer(item);
    }
}
