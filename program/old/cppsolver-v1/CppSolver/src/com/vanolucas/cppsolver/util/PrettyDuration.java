package com.vanolucas.cppsolver.util;

import java.time.Duration;

public class PrettyDuration {
    public static String prettify(Duration duration) {
        return duration
                .toString()
                .substring(2)
                .replaceAll("(\\d[HMS])(?!$)", "$1 ")
                .toLowerCase();
    }
}
