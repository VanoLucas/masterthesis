package com.vanolucas.cppsolver.crossover;

import com.vanolucas.cppsolver.rkv.RandomKeyVector;
import com.vanolucas.cppsolver.solution.EncodedSolution;
import com.vanolucas.cppsolver.solution.EvaluatedPopulation;
import com.vanolucas.cppsolver.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.solution.Population;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

class RkvUniformCrossoverTest {

    @Test
    void makeChildren() {
        RkvUniformCrossover rkvUniformCrossover = new RkvUniformCrossover(0.5d);

        RandomKeyVector rkvParent1 = new RandomKeyVector(
                Collections.nCopies(10, 0.1d)
        );
        RandomKeyVector rkvParent2 = new RandomKeyVector(
                Collections.nCopies(10, 0.9d)
        );

        EvaluatedPopulation popParents = new EvaluatedPopulation(Arrays.asList(
                new EvaluatedSolution(new EncodedSolution(rkvParent1), null),
                new EvaluatedSolution(new EncodedSolution(rkvParent2), null)
        ));

        Population popChildren = rkvUniformCrossover.uniformCrossover(popParents);

        popChildren.print();
    }
}