package com.vanolucas.cppsolver.draft.old.cpp.instance;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

class StandardCppTest {

    @Test
    void toString1() throws IOException {
        File cppInstanceFile = FileChooser.chooseFile();

        if (cppInstanceFile != null) {
            final StandardCpp cppInstance = new StandardCpp(
                    BookCover.loadFromFile(cppInstanceFile),
                    2,
                    4
            );
            System.out.println(cppInstance);
        }
    }
}