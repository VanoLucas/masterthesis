package com.vanolucas.cppsolver.draft.old.cpp.solution;

import com.vanolucas.cppsolver.draft.old.cpp.instance.BookCover;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class SolutionTest {

    @Test
    void toString1() {
        BookCover[] bookCovers = {
                new BookCover(1, 111),
                new BookCover(2, 222)
        };
        PlatePrintings plate1 = new PlatePrintings(
                Arrays.asList(bookCovers[0], bookCovers[0], bookCovers[1], bookCovers[1]),
                111
        );
        PlatePrintings plate2 = new PlatePrintings(
                Arrays.asList(bookCovers[1], bookCovers[1], bookCovers[1], bookCovers[1]),
                222
        );
        List<PlatePrintings> plates = Arrays.asList(
                plate1, plate2
        );
        Solution solution = new Solution(plates);

        System.out.println(solution);
    }
}