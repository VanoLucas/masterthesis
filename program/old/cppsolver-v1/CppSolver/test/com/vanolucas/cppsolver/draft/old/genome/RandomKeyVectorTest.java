package com.vanolucas.cppsolver.draft.old.genome;

import com.vanolucas.cppsolver.draft.old.math.range.Range;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RandomKeyVectorTest {

    @Test
    void toString1() {
        RandomKeyVector rkv = new RandomKeyVector(Arrays.asList(
                0d, 0.5d, 0.9d
        ));
        System.out.println(rkv);
    }

    @Test
    void scale() {
        RandomKeyVector rkv = new RandomKeyVector(Arrays.asList(
                0.0d, 0.5d, 0.9d
        ));

        List<Range> targetRanges = Arrays.asList(
                new Range(0d, 100d),
                new Range(0d, 100d),
                new Range(0d, 200d)
        );

        List<Double> result = rkv.getScaledKeys(targetRanges);

        System.out.println(rkv);
        System.out.println(result);

        assertEquals(0d, result.get(0));
        assertEquals(50d, result.get(1));
        assertEquals(180d, result.get(2));
    }

    @Test
    void draft() {
    }
}