package com.vanolucas.cppsolver.experiment;

import com.google.common.collect.Lists;
import com.vanolucas.cppsolver.clone.CloneableCloner;
import com.vanolucas.cppsolver.clone.EncodedSolutionCloner;
import com.vanolucas.cppsolver.clone.SolutionCloner;
import com.vanolucas.cppsolver.eval.EncodedSolutionEvaluator;
import com.vanolucas.cppsolver.eval.SolutionEvaluator;
import com.vanolucas.cppsolver.mutation.FirstEncodedRepresentationSolutionMutator;
import com.vanolucas.cppsolver.mutation.NeighborsProducer;
import com.vanolucas.cppsolver.mutation.SolutionMutator;
import com.vanolucas.cppsolver.opti.Opti;
import com.vanolucas.cppsolver.opti.ParallelSteepestAscentHillClimbing;
import com.vanolucas.cppsolver.problem.cpp.eval.CppSolutionEvaluator;
import com.vanolucas.cppsolver.problem.cpp.eval.SlotsDecoder;
import com.vanolucas.cppsolver.problem.cpp.genome.GenomeCf;
import com.vanolucas.cppsolver.problem.cpp.genome.decode.GenomeCfDecoder;
import com.vanolucas.cppsolver.problem.cpp.instance.Demand;
import com.vanolucas.cppsolver.problem.cpp.instance.StandardCppInstance;
import com.vanolucas.cppsolver.rand.Rand;
import com.vanolucas.cppsolver.rand.RandomizableRandomizer;
import com.vanolucas.cppsolver.rkv.RandomizeOneKeyRkvMutator;
import com.vanolucas.cppsolver.solution.EncodedSolution;
import com.vanolucas.cppsolver.solution.Solution;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

class ExperimentTest {

    @Test
    void run() throws IOException {
        /*
        experiment params
         */

        final int runsCount = 12;
        final Duration durationPerRun = Duration.ofSeconds(200L);

        /*
        load a standard cpp instance
         */

        final String instanceName = "data10-1";
        final int platesCount = 3;
//        Demand demand = Demand.loadFromFile(Paths.get("/lucas/Lucas/Travail/UMons/Master_2/Memoire/git/instances/I013.dat"));
//        Demand demand = Demand.loadFromFile(Paths.get("/lucas/Lucas/Travail/UMons/Master_2/Memoire/git/instances/I014.dat"));
        Demand demand = Demand.loadFromFile(Paths.get(String.format("/lucas/Lucas/Travail/UMons/Master_2/Memoire/git/instances/%s.dat", instanceName)));
//        Demand demand = Demand.loadFromFile(Paths.get("/lucas/Lucas/Travail/UMons/Master_2/Memoire/git/instances/PLPP18.dat"));
        StandardCppInstance cppInstance = new StandardCppInstance(
                demand, platesCount, 10
//                ,0d, 1d
        );
        System.out.println(cppInstance);

        /*
        initial solution
         */

        Solution initialSolution = new EncodedSolution(
                2, new GenomeCf(cppInstance)
//                2, new GenomePpc(cppInstance)
//                2, new GenomeSp(cppInstance)
        );

        /*
        neighbors producer
         */

        // mutation: full randomization of the genome
        List<SolutionMutator> solutionMutators = Lists.newArrayList(
                new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer()),
                new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer()),
                new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer())
        );

        // mutation: randomization of one key in the genome
        Rand rand = new Rand();
        final int genomeLength = GenomeCf.calculateLengthForCppInstance(cppInstance);
//        final int genomeLength = GenomePpc.calculateLengthForCppInstance(cppInstance);
//        final int genomeLength = GenomeSp.calculateLengthForCppInstance(cppInstance);
        for (int keyIndex = 0; keyIndex < genomeLength; keyIndex++) {
            solutionMutators.add(
                    new FirstEncodedRepresentationSolutionMutator(
                            new RandomizeOneKeyRkvMutator(keyIndex, rand)
                    )
            );
        }

        SolutionCloner solutionCloner = new EncodedSolutionCloner(
                new CloneableCloner()
        );

        NeighborsProducer neighborsProducer = new NeighborsProducer(
                solutionMutators, solutionCloner
        );

        /*
        evaluator
         */

        SolutionEvaluator solutionEvaluator = new EncodedSolutionEvaluator(
                new GenomeCfDecoder(cppInstance),
//                new GenomePpcDecoder(cppInstance),
//                new GenomeSpDecoder(cppInstance),
                new SlotsDecoder(cppInstance),
                new CppSolutionEvaluator(cppInstance)
        );

        /*
        create experiment
         */

        ExecutorService executor = Executors.newFixedThreadPool(4);

        // opti algo supplier
        Supplier<Opti> optiSupplier = () -> new ParallelSteepestAscentHillClimbing(
                initialSolution, neighborsProducer, solutionEvaluator, executor
        );

        // experiment
        Experiment experiment = new Experiment(
                String.format("%s with %d plates", instanceName, platesCount), optiSupplier, runsCount, durationPerRun, true
        );

        /*
        run experiment
         */

        experiment.run();
        executor.shutdown();

        System.out.println(System.lineSeparator() + "#######################################################" + System.lineSeparator());
        experiment.printStats();
    }
}