package com.vanolucas.cppsolver.opti;

import com.vanolucas.cppsolver.eval.EncodedSolutionEvaluator;
import com.vanolucas.cppsolver.eval.SolutionEvaluator;
import com.vanolucas.cppsolver.mutation.FirstEncodedRepresentationSolutionMutator;
import com.vanolucas.cppsolver.mutation.PhenotypeOnlySolutionMutator;
import com.vanolucas.cppsolver.mutation.SolutionMutator;
import com.vanolucas.cppsolver.problem.cpp.eval.CppSolutionEvaluator;
import com.vanolucas.cppsolver.problem.cpp.eval.SlotsDecoder;
import com.vanolucas.cppsolver.problem.cpp.genome.GenomeCf;
import com.vanolucas.cppsolver.problem.cpp.genome.decode.GenomeCfDecoder;
import com.vanolucas.cppsolver.problem.cpp.instance.Demand;
import com.vanolucas.cppsolver.problem.cpp.instance.DemandFileChooser;
import com.vanolucas.cppsolver.problem.cpp.instance.StandardCppInstance;
import com.vanolucas.cppsolver.problem.cpp.rand.CppSolutionRandomizer;
import com.vanolucas.cppsolver.problem.cpp.rand.RandomCppSolutionFactory;
import com.vanolucas.cppsolver.rand.RandomizableRandomizer;
import com.vanolucas.cppsolver.solution.EncodedSolution;
import com.vanolucas.cppsolver.solution.Solution;
import com.vanolucas.cppsolver.solution.keeper.Keeper;
import com.vanolucas.cppsolver.solution.keeper.SingleEarliestBestSolutionKeeper;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;

class RandomSearchTest {

    @Test
    void runStandardCpp() throws IOException {
        /*
        load a standard cpp instance
         */

        Demand demand = DemandFileChooser.chooseFileAndGetDemand();

        StandardCppInstance cppInstance = new StandardCppInstance(
                demand, 10, 4
        );

        System.out.println(String.format("Standard CPP instance:%n%s", cppInstance));

        /*
        initial solution
         */

        RandomCppSolutionFactory cppSolutionFactory = new RandomCppSolutionFactory(cppInstance);
        Solution initialSolution = new Solution(
                cppSolutionFactory.newCppSolution()
        );

        /*
        randomizer
         */

        SolutionMutator solutionRandomizer = new PhenotypeOnlySolutionMutator(
                new CppSolutionRandomizer(cppSolutionFactory)
        );

        /*
        evaluator
         */

        SolutionEvaluator solutionEvaluator = new SolutionEvaluator(
                new CppSolutionEvaluator(cppInstance)
        );

        /*
        create random search opti algo
         */

        Opti opti = new RandomSearch(
                initialSolution, solutionRandomizer, solutionEvaluator
        );

        /*
        best solution keeper
         */

        Keeper keeper = new SingleEarliestBestSolutionKeeper(true);
        opti.addOnSolutionEvaluatedListener(keeper::submit);

        /*
        run optimization
         */

        Instant instantRunStart = Instant.now();
        opti.runForAtMost(Duration.ofSeconds(15L));
        System.out.println(String.format("Run duration: %s",
                Duration.between(instantRunStart, Instant.now())
        ));
    }

    @Test
    void runGeneticStandardCpp() throws IOException {
        /*
        load a standard cpp instance
         */

        Demand demand = DemandFileChooser.chooseFileAndGetDemand();

        StandardCppInstance cppInstance = new StandardCppInstance(
                demand, 10, 4
        );

        System.out.println(String.format("Standard CPP instance:%n%s", cppInstance));

        /*
        initial solution
         */

        Solution initialSolution = new EncodedSolution(
                2, new GenomeCf(cppInstance)
        );

        /*
        randomizer
         */

        SolutionMutator solutionRandomizer = new FirstEncodedRepresentationSolutionMutator(
                new RandomizableRandomizer()
        );

        /*
        evaluator
         */

        SolutionEvaluator solutionEvaluator = new EncodedSolutionEvaluator(
                new GenomeCfDecoder(cppInstance),
                new SlotsDecoder(cppInstance),
                new CppSolutionEvaluator(cppInstance)
        );

        /*
        create random search opti algo
         */

        Opti opti = new RandomSearch(
                initialSolution, solutionRandomizer, solutionEvaluator
        );

        /*
        best solution keeper
         */

        Keeper keeper = new SingleEarliestBestSolutionKeeper(true);
        opti.addOnSolutionEvaluatedListener(solution -> {
            Keeper.Decision decision = keeper.submit(solution);
            if (decision.equals(Keeper.Decision.KEPT)) {
                System.out.println(String.format("At iteration: %d", opti.getIteration()));
            }
        });

        /*
        run optimization
         */

        Instant instantRunStart = Instant.now();
        opti.runForAtMost(Duration.ofSeconds(15L));
        System.out.println(String.format("Run duration: %s",
                Duration.between(instantRunStart, Instant.now())
        ));
    }
}