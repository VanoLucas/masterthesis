package com.vanolucas.cppsolver.opti;

import com.vanolucas.cppsolver.clone.CloneableCloner;
import com.vanolucas.cppsolver.clone.EncodedSolutionCloner;
import com.vanolucas.cppsolver.clone.SolutionCloner;
import com.vanolucas.cppsolver.eval.EncodedSolutionEvaluator;
import com.vanolucas.cppsolver.eval.SolutionEvaluator;
import com.vanolucas.cppsolver.mutation.FirstEncodedRepresentationSolutionMutator;
import com.vanolucas.cppsolver.mutation.NeighborsProducer;
import com.vanolucas.cppsolver.mutation.SolutionMutator;
import com.vanolucas.cppsolver.problem.cpp.eval.CppSolutionEvaluator;
import com.vanolucas.cppsolver.problem.cpp.eval.SlotsDecoder;
import com.vanolucas.cppsolver.problem.cpp.genome.GenomeCf;
import com.vanolucas.cppsolver.problem.cpp.genome.decode.GenomeCfDecoder;
import com.vanolucas.cppsolver.problem.cpp.instance.Demand;
import com.vanolucas.cppsolver.problem.cpp.instance.StandardCppInstance;
import com.vanolucas.cppsolver.rand.RandomizableRandomizer;
import com.vanolucas.cppsolver.solution.EncodedSolution;
import com.vanolucas.cppsolver.solution.Solution;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SequentialVsParallelSteepestAscentHillClimbingTest {

    @Test
    void runGeneticStandardCpp() throws IOException {
        /*
        load a standard cpp instance
         */

        Demand demand = Demand.loadFromFile(Paths.get("/lucas/Lucas/Travail/UMons/Master_2/Memoire/git/instances/I013.dat"));
        StandardCppInstance cppInstance = new StandardCppInstance(
                demand, 10, 4
        );

//        System.out.println(String.format("Standard CPP instance:%n%s", cppInstance));

        /*
        initial solution
         */

        Solution initialSolution = new EncodedSolution(
                2, new GenomeCf(cppInstance)
        );

        /*
        neighbors producer
         */

        List<SolutionMutator> solutionMutators = Arrays.asList(
                new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer()),
                new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer()),
                new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer()),
                new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer()),
                new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer()),
                new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer()),
                new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer()),
                new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer()),
                new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer()),
                new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer()),
                new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer()),
                new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer()),
                new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer()),
                new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer()),
                new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer()),
                new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer())
        );

        SolutionCloner solutionCloner = new EncodedSolutionCloner(
                new CloneableCloner()
        );

        NeighborsProducer neighborsProducer = new NeighborsProducer(
                solutionMutators, solutionCloner
        );

        /*
        evaluator
         */

        SolutionEvaluator solutionEvaluator = new EncodedSolutionEvaluator(
                new GenomeCfDecoder(cppInstance),
                new SlotsDecoder(cppInstance),
                new CppSolutionEvaluator(cppInstance)
        );

        /*
        create steepest ascent hill climbing algo
         */

        ExecutorService executor = Executors.newFixedThreadPool(4);

        Opti sequentialOpti = new SteepestAscentHillClimbing(initialSolution, neighborsProducer, solutionEvaluator);
        Opti parallelOpti = new ParallelSteepestAscentHillClimbing(initialSolution, neighborsProducer, solutionEvaluator, executor);

        /*
        run optimization
         */

        sequentialOpti.runFor(10_000L);
        parallelOpti.runFor(10_000L);

        executor.shutdown();

        /*
        print stats
         */

        System.out.println("SEQUENTIAL RUN:");
        sequentialOpti.printRunStats();

        System.out.println("PARALLEL RUN:");
        parallelOpti.printRunStats();
    }
}
