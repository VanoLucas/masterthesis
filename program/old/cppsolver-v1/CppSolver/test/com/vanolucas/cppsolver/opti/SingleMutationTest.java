package com.vanolucas.cppsolver.opti;

import com.vanolucas.cppsolver.eval.EncodedSolutionEvaluator;
import com.vanolucas.cppsolver.eval.SolutionEvaluator;
import com.vanolucas.cppsolver.mutation.FirstEncodedRepresentationSolutionMutator;
import com.vanolucas.cppsolver.mutation.PhenotypeOnlySolutionMutator;
import com.vanolucas.cppsolver.mutation.SolutionMutator;
import com.vanolucas.cppsolver.problem.sample.targetnb.eval.TwoNumbersDecoder;
import com.vanolucas.cppsolver.problem.sample.targetnb.eval.ValueEvaluator;
import com.vanolucas.cppsolver.problem.sample.targetnb.instance.TnpInstance;
import com.vanolucas.cppsolver.problem.sample.targetnb.mutation.AddValueMutator;
import com.vanolucas.cppsolver.problem.sample.targetnb.mutation.AddValueTwoNumbersMutator;
import com.vanolucas.cppsolver.problem.sample.targetnb.solution.Value;
import com.vanolucas.cppsolver.solution.Solution;
import org.junit.jupiter.api.Test;

class SingleMutationTest {

    @Test
    void runWithPhenotypeOnlySolution() {
        TnpInstance tnpInstance = new TnpInstance(1000);

        Solution initialSolution = new Solution(
                new Value(0)
        );

        SolutionMutator solutionMutator = new PhenotypeOnlySolutionMutator(
                new AddValueMutator(1)
        );

        SolutionEvaluator solutionEvaluator = new SolutionEvaluator(
                new ValueEvaluator(tnpInstance)
        );

        Opti opti = new SingleMutation(
                initialSolution, solutionMutator, solutionEvaluator
        );

        opti.runFor(1000);
//        opti.runForAtMost(Duration.ofSeconds(5L));
    }

    @Test
    void runWithEncodedSolution() {
        TnpInstance tnpInstance = new TnpInstance(1000);

        Solution initialSolution = tnpInstance.newEncodedSolution();

        SolutionMutator solutionMutator = new FirstEncodedRepresentationSolutionMutator(
                new AddValueTwoNumbersMutator(1, 1)
        );

        SolutionEvaluator solutionEvaluator = new EncodedSolutionEvaluator(
                new TwoNumbersDecoder(),
                new ValueEvaluator(tnpInstance)
        );

        Opti opti = new SingleMutation(
                initialSolution, solutionMutator, solutionEvaluator
        );

        opti.runFor(500);
    }
}