package com.vanolucas.cppsolver.opti;

import com.vanolucas.cppsolver.clone.CloneableCloner;
import com.vanolucas.cppsolver.clone.EncodedSolutionCloner;
import com.vanolucas.cppsolver.clone.PhenotypeOnlySolutionCloner;
import com.vanolucas.cppsolver.clone.SolutionCloner;
import com.vanolucas.cppsolver.eval.EncodedSolutionEvaluator;
import com.vanolucas.cppsolver.eval.SolutionEvaluator;
import com.vanolucas.cppsolver.mutation.FirstEncodedRepresentationSolutionMutator;
import com.vanolucas.cppsolver.mutation.NeighborsProducer;
import com.vanolucas.cppsolver.mutation.PhenotypeOnlySolutionMutator;
import com.vanolucas.cppsolver.mutation.SolutionMutator;
import com.vanolucas.cppsolver.problem.cpp.eval.CppSolutionEvaluator;
import com.vanolucas.cppsolver.problem.cpp.eval.SlotsDecoder;
import com.vanolucas.cppsolver.problem.cpp.genome.GenomeCf;
import com.vanolucas.cppsolver.problem.cpp.genome.decode.GenomeCfDecoder;
import com.vanolucas.cppsolver.problem.cpp.instance.Demand;
import com.vanolucas.cppsolver.problem.cpp.instance.DemandFileChooser;
import com.vanolucas.cppsolver.problem.cpp.instance.StandardCppInstance;
import com.vanolucas.cppsolver.problem.sample.targetnb.eval.TwoNumbersDecoder;
import com.vanolucas.cppsolver.problem.sample.targetnb.eval.ValueEvaluator;
import com.vanolucas.cppsolver.problem.sample.targetnb.genome.TwoNumbers;
import com.vanolucas.cppsolver.problem.sample.targetnb.instance.TnpInstance;
import com.vanolucas.cppsolver.problem.sample.targetnb.mutation.AddValueMutator;
import com.vanolucas.cppsolver.problem.sample.targetnb.mutation.AddValueTwoNumbersMutator;
import com.vanolucas.cppsolver.problem.sample.targetnb.solution.Value;
import com.vanolucas.cppsolver.rand.RandomizableRandomizer;
import com.vanolucas.cppsolver.solution.EncodedSolution;
import com.vanolucas.cppsolver.solution.Solution;
import com.vanolucas.cppsolver.solution.keeper.Keeper;
import com.vanolucas.cppsolver.solution.keeper.SingleEarliestBestSolutionKeeper;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;

class SteepestAscentHillClimbingTest {

    @Test
    void runWithPhenotypeOnlySolution() {
        TnpInstance tnpInstance = new TnpInstance(999);

        Solution initialSolution = new Solution(
                new Value(0)
        );

        List<SolutionMutator> solutionMutators = Arrays.asList(
                new PhenotypeOnlySolutionMutator(new AddValueMutator(+1)),
                new PhenotypeOnlySolutionMutator(new AddValueMutator(-1)),
                new PhenotypeOnlySolutionMutator(new AddValueMutator(+10)),
                new PhenotypeOnlySolutionMutator(new AddValueMutator(-10))
        );

        SolutionCloner solutionCloner = new PhenotypeOnlySolutionCloner(
                new CloneableCloner()
        );

        NeighborsProducer neighborsProducer = new NeighborsProducer(
                solutionMutators, solutionCloner
        );

        SolutionEvaluator solutionEvaluator = new SolutionEvaluator(
                new ValueEvaluator(tnpInstance)
        );

        Opti opti = new SteepestAscentHillClimbing(
                initialSolution, neighborsProducer, solutionEvaluator
        );

        opti.runFor(101);
    }

    @Test
    void runWithEncodedSolution() {
        TnpInstance tnpInstance = new TnpInstance(999);

        Solution initialSolution = new EncodedSolution(
                new TwoNumbers(0, 0)
        );

        List<SolutionMutator> solutionMutators = Arrays.asList(
                new FirstEncodedRepresentationSolutionMutator(new AddValueTwoNumbersMutator(1, 1)),
                new FirstEncodedRepresentationSolutionMutator(new AddValueTwoNumbersMutator(-1, -1)),
                new FirstEncodedRepresentationSolutionMutator(new AddValueTwoNumbersMutator(5, 5))
        );

        SolutionCloner solutionCloner = new EncodedSolutionCloner(
                new CloneableCloner()
        );

        NeighborsProducer neighborsProducer = new NeighborsProducer(
                solutionMutators, solutionCloner
        );

        SolutionEvaluator solutionEvaluator = new EncodedSolutionEvaluator(
                new TwoNumbersDecoder(),
                new ValueEvaluator(tnpInstance)
        );

        Opti opti = new SteepestAscentHillClimbing(
                initialSolution, neighborsProducer, solutionEvaluator
        );

        opti.runFor(101);
    }

    @Test
    void runGeneticStandardCpp() throws IOException {
        /*
        load a standard cpp instance
         */

        Demand demand = DemandFileChooser.chooseFileAndGetDemand();

        StandardCppInstance cppInstance = new StandardCppInstance(
                demand, 10, 4
        );

        System.out.println(String.format("Standard CPP instance:%n%s", cppInstance));

        /*
        initial solution
         */

        Solution initialSolution = new EncodedSolution(
                2, new GenomeCf(cppInstance)
        );

        /*
        neighbors producer
         */

        // todo more sophisticated mutation operators for rkv
        List<SolutionMutator> solutionMutators = Arrays.asList(
                new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer()),
                new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer())
        );

        SolutionCloner solutionCloner = new EncodedSolutionCloner(
                new CloneableCloner()
        );

        NeighborsProducer neighborsProducer = new NeighborsProducer(
                solutionMutators, solutionCloner
        );

        /*
        evaluator
         */

        SolutionEvaluator solutionEvaluator = new EncodedSolutionEvaluator(
                new GenomeCfDecoder(cppInstance),
                new SlotsDecoder(cppInstance),
                new CppSolutionEvaluator(cppInstance)
        );

        /*
        create steepest ascent hill climbing algo
         */

        Opti opti = new SteepestAscentHillClimbing(
                initialSolution, neighborsProducer, solutionEvaluator
        );

        /*
        best solution keeper
         */

        Keeper keeper = new SingleEarliestBestSolutionKeeper(true);
        opti.addOnSolutionEvaluatedListener(solution -> {
            Keeper.Decision decision = keeper.submit(solution);
            if (decision.equals(Keeper.Decision.KEPT)) {
                System.out.println(String.format("At iteration: %d", opti.getIteration()));
            }
        });

        /*
        run optimization
         */

        Instant instantRunStart = Instant.now();
        opti.runForAtMost(Duration.ofSeconds(15L));

        System.out.println(String.format("Total iterations: %d%n" +
                        "Run duration: %s",
                opti.getIteration(),
                Duration.between(instantRunStart, Instant.now())
        ));

        opti.printRunStats();
    }
}