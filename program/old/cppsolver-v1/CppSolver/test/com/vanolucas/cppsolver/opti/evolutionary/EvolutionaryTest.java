package com.vanolucas.cppsolver.opti.evolutionary;

import com.vanolucas.cppsolver.crossover.Crossover;
import com.vanolucas.cppsolver.crossover.RkvUniformCrossover;
import com.vanolucas.cppsolver.eval.EncodedSolutionEvaluator;
import com.vanolucas.cppsolver.eval.SolutionEvaluator;
import com.vanolucas.cppsolver.experiment.Experiment;
import com.vanolucas.cppsolver.mutation.FirstEncodedRepresentationSolutionMutator;
import com.vanolucas.cppsolver.mutation.SolutionMutator;
import com.vanolucas.cppsolver.opti.Opti;
import com.vanolucas.cppsolver.problem.cpp.eval.CppSolutionEvaluator;
import com.vanolucas.cppsolver.problem.cpp.eval.SlotsDecoder;
import com.vanolucas.cppsolver.problem.cpp.genome.GenomeCf;
import com.vanolucas.cppsolver.problem.cpp.genome.decode.GenomeCfDecoder;
import com.vanolucas.cppsolver.problem.cpp.instance.Demand;
import com.vanolucas.cppsolver.problem.cpp.instance.StandardCppInstance;
import com.vanolucas.cppsolver.rand.Rand;
import com.vanolucas.cppsolver.rkv.ProbaRkvMutator;
import com.vanolucas.cppsolver.rkv.RandomKeyVector;
import com.vanolucas.cppsolver.rkv.RandomizeOneKeyRkvMutator;
import com.vanolucas.cppsolver.solution.EncodedSolution;
import com.vanolucas.cppsolver.solution.Population;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class EvolutionaryTest {

    @Test
    void firstCppTest() throws IOException {
        // experiment params
        final int runsCount = 4;
        final Duration durationPerRun = Duration.ofSeconds(15L);
        final int populationSize = 6;
        final double crossoverMixingRatio = 0.1d;

        // standard cpp instance
        final Demand demand = Demand.loadFromFile(Paths.get(
                "/lucas/Lucas/Travail/UMons/Master_2/Memoire/git/instances/I013.dat"
        ));
        final StandardCppInstance cppInstance = new StandardCppInstance(
                demand, 10, 4
        );

        // rand generator
        final Rand rand = new Rand();

        // create random genomes for the initial population
        final List<RandomKeyVector> initialGenomes = IntStream.range(0, populationSize)
                .mapToObj(i -> {
                    final GenomeCf genome = new GenomeCf(cppInstance);
                    genome.randomize(rand);
                    return genome;
                })
                .collect(Collectors.toList());

        // create initial population
        final Population initialPopulation = new Population(
                initialGenomes.stream()
                        .map(genome -> new EncodedSolution(2, genome))
                        .collect(Collectors.toList())
        );

        // evaluator
        final SolutionEvaluator solutionEvaluator = new EncodedSolutionEvaluator(
                new GenomeCfDecoder(cppInstance),
                new SlotsDecoder(cppInstance),
                new CppSolutionEvaluator(cppInstance)
        );

        // mates selector
        final GroupsMaker matesSelector = new RandomGroupsMaker(2, rand);

        // crossover
        final Crossover crossover = new RkvUniformCrossover(crossoverMixingRatio, rand);

        // mutation
        final SolutionMutator solutionMutator = new FirstEncodedRepresentationSolutionMutator(
                new ProbaRkvMutator(
                        new RandomizeOneKeyRkvMutator(rand),
                        2d / GenomeCf.calculateLengthForCppInstance(cppInstance)
                )
        );

        // breeder (crossover + mutation)
        final Breeder breeder = new CrossoverThenMutationBreeder(
                crossover,
                solutionMutator, 0.5d, rand
        );

        // merger
        final Merger merger = new KeepBestMerger();

        // create evolutionary algo
        final Supplier<Opti> optiSupplier = () -> new Evolutionary(
                initialPopulation, solutionEvaluator, matesSelector, breeder, merger
        );

        // create experiment
        final Experiment experiment = new Experiment(
                "First Evolutionary Algo Test",
                optiSupplier, runsCount, durationPerRun, true
        );

        // run experiment
        experiment.run();

        // print stats
        experiment.printStats();
    }
}