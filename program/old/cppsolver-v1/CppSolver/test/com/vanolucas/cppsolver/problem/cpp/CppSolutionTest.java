package com.vanolucas.cppsolver.problem.cpp;

import com.vanolucas.cppsolver.problem.cpp.instance.BookCover;
import com.vanolucas.cppsolver.problem.cpp.instance.CppInstance;
import com.vanolucas.cppsolver.problem.cpp.instance.Demand;
import com.vanolucas.cppsolver.problem.cpp.solution.CppSolution;
import com.vanolucas.cppsolver.problem.cpp.solution.Plate;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CppSolutionTest {

    @Test
    void toString1() {
        BookCover[] bookCovers = new BookCover[]{
                new BookCover(1, 1000),
                new BookCover(2, 2000),
                new BookCover(3, 3000)
        };

        Plate plate1 = new Plate(Arrays.asList(
                bookCovers[0], bookCovers[0], bookCovers[0], bookCovers[0]
        ), 111);
        Plate plate2 = new Plate(Arrays.asList(
                bookCovers[1], bookCovers[2], bookCovers[1], bookCovers[2]
        ), 222);

        List<Plate> plates = Arrays.asList(
                plate1, plate2
        );

        CppSolution cppSolution = new CppSolution(new Demand(Collections.emptyList()), plates);

        System.out.println(cppSolution);
        System.out.println("Cost: " + cppSolution.getCost(1000d, 1d));
    }

    @Test
    void isFeasible() {
        BookCover[] bookCovers = new BookCover[]{
                new BookCover(1, 1000),
                new BookCover(2, 1000),
                new BookCover(3, 1000),
                new BookCover(4, 1000)
        };

        Demand demand = new Demand(Stream.of(bookCovers).collect(Collectors.toList()));

        CppInstance cppInstance = new CppInstance(
                demand, 4
        );

        Plate plate1 = new Plate(
                Arrays.asList(
                        bookCovers[0], bookCovers[1], bookCovers[2], bookCovers[1]
                ),
                500
        );
        Plate plate2 = new Plate(
                Arrays.asList(
                        bookCovers[0], bookCovers[0], bookCovers[0], bookCovers[0]
                ),
                125
        );

        CppSolution cppSolution = new CppSolution(cppInstance.getDemand(), Arrays.asList(
                plate1, plate2
        ));

        System.out.println(cppSolution);
        System.out.println(String.format("Realized copies: %s", cppSolution.getRealizedCopies().collect(Collectors.toList())));
        System.out.println(String.format("Feasible: %s", cppSolution.isFeasible()));
        assertFalse(cppSolution.isFeasible());

        plate1 = new Plate(
                Arrays.asList(
                        bookCovers[0], bookCovers[0], bookCovers[0], bookCovers[1]
                ),
                1000
        );
        plate2 = new Plate(
                Arrays.asList(
                        bookCovers[2], bookCovers[2], bookCovers[3], bookCovers[3]
                ),
                500
        );

        cppSolution = new CppSolution(cppInstance.getDemand(), Arrays.asList(
                plate1, plate2
        ));

        System.out.println(cppSolution);
        System.out.println(String.format("Realized copies: %s", cppSolution.getRealizedCopies().collect(Collectors.toList())));
        System.out.println(String.format("Feasible: %s", cppSolution.isFeasible()));
        assertTrue(cppSolution.isFeasible());
    }
}