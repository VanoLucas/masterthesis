package com.vanolucas.cppsolver.problem.cpp;

import com.vanolucas.cppsolver.problem.cpp.instance.Demand;
import com.vanolucas.cppsolver.problem.cpp.instance.DemandFileChooser;
import com.vanolucas.cppsolver.problem.cpp.instance.StandardCppInstance;
import org.junit.jupiter.api.Test;

import java.io.IOException;

class DemandFileChooserTest {

    @Test
    void chooseFileAndGetDemand() throws IOException {
        Demand demand = DemandFileChooser.chooseFileAndGetDemand();
        System.out.println(new StandardCppInstance(
                demand, 10, 10
        ));
    }
}