package com.vanolucas.cppsolver.problem.cpp;

import com.vanolucas.cppsolver.problem.cpp.eval.Copies;
import com.vanolucas.cppsolver.problem.cpp.eval.PlatesAssigner;
import com.vanolucas.cppsolver.problem.cpp.eval.Slots;
import com.vanolucas.cppsolver.problem.cpp.eval.SortByCopiesPlatesAssigner;
import com.vanolucas.cppsolver.problem.cpp.instance.BookCover;
import com.vanolucas.cppsolver.problem.cpp.instance.Demand;
import com.vanolucas.cppsolver.problem.cpp.solution.CppSolution;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

class SortByCopiesPlatesAssignerTest {

    @Test
    void allocate() {
        BookCover[] bookCovers = new BookCover[]{
                new BookCover(1, 1000),
                new BookCover(2, 2000),
                new BookCover(3, 3000),
                new BookCover(4, 4000)
        };

        Slots slots = new Slots(Arrays.asList(
                new Copies(bookCovers[0], 500),
                new Copies(bookCovers[0], 500),
                new Copies(bookCovers[1], 2000),
                new Copies(bookCovers[2], 3000),
                new Copies(bookCovers[3], 1000),
                new Copies(bookCovers[3], 2000),
                new Copies(bookCovers[3], 1000)
        ));

        PlatesAssigner platesAssigner = new SortByCopiesPlatesAssigner(new Demand(Collections.emptyList()));

        CppSolution cppSolution = platesAssigner.assign(
                slots, 4
        );

        cppSolution.forEachPlate(System.out::println);
    }
}