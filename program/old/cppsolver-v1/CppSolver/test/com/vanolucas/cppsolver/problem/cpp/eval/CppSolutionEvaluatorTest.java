package com.vanolucas.cppsolver.problem.cpp.eval;

import com.vanolucas.cppsolver.eval.SolutionEvaluator;
import com.vanolucas.cppsolver.problem.cpp.instance.BookCover;
import com.vanolucas.cppsolver.problem.cpp.instance.Demand;
import com.vanolucas.cppsolver.problem.cpp.instance.StandardCppInstance;
import com.vanolucas.cppsolver.problem.cpp.solution.CppSolution;
import com.vanolucas.cppsolver.problem.cpp.solution.Plate;
import com.vanolucas.cppsolver.solution.EvaluatedSolution;
import com.vanolucas.cppsolver.solution.Solution;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

class CppSolutionEvaluatorTest {

    @Test
    void evaluate() throws IOException {
        // load CPP instance LPP18
        final Demand demand = Demand.loadFromFile(Paths.get("/lucas/Lucas/Travail/UMons/Master_2/Memoire/git/instances/PLPP18.dat"));
        final int platesCount = 3;
        final int slotsPerPlateCount = 14;

        StandardCppInstance cppInstance = new StandardCppInstance(
                demand, platesCount, slotsPerPlateCount
        );
        System.out.println(cppInstance);

        // construct best solution with 3 plates from [TUY14]
        List<BookCover> bookCoversPlate1 = Arrays.asList(
                demand.getBookCover(12), demand.getBookCover(12), demand.getBookCover(12),
                demand.getBookCover(13),
                demand.getBookCover(14), demand.getBookCover(14),
                demand.getBookCover(15),
                demand.getBookCover(17), demand.getBookCover(17), demand.getBookCover(17), demand.getBookCover(17),
                demand.getBookCover(8),
                demand.getBookCover(9),
                demand.getBookCover(16)
        );
        List<BookCover> bookCoversPlate2 = Arrays.asList(
                demand.getBookCover(1),
                demand.getBookCover(3),
                demand.getBookCover(4), demand.getBookCover(4),
                demand.getBookCover(9), demand.getBookCover(9), demand.getBookCover(9), demand.getBookCover(9), demand.getBookCover(9),
                demand.getBookCover(10), demand.getBookCover(10),
                demand.getBookCover(11), demand.getBookCover(11), demand.getBookCover(11)
        );
        List<BookCover> bookCoversPlate3 = Arrays.asList(
                demand.getBookCover(0), demand.getBookCover(0), demand.getBookCover(0), demand.getBookCover(0),
                demand.getBookCover(2),
                demand.getBookCover(5),
                demand.getBookCover(6),
                demand.getBookCover(7),
                demand.getBookCover(8), demand.getBookCover(8),
                demand.getBookCover(11),
                demand.getBookCover(16), demand.getBookCover(16), demand.getBookCover(16)
        );
        List<Plate> plates = Arrays.asList(
                new Plate(bookCoversPlate1, 1400),
                new Plate(bookCoversPlate2, 210),
                new Plate(bookCoversPlate3, 550)
        );
        CppSolution cppSolution = new CppSolution(
                demand, plates
        );
        Solution solution = new Solution(cppSolution);

        // evaluate solution
        SolutionEvaluator solutionEvaluator = new SolutionEvaluator(
                new CppSolutionEvaluator(cppInstance)
        );
        EvaluatedSolution evaluatedSolution = solutionEvaluator.getEvaluated(solution);

        System.out.println(evaluatedSolution);
    }
}