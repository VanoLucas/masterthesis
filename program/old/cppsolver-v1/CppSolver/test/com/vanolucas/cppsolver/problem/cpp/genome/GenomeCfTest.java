package com.vanolucas.cppsolver.problem.cpp.genome;

import com.vanolucas.cppsolver.problem.cpp.instance.Demand;
import com.vanolucas.cppsolver.problem.cpp.instance.StandardCppInstance;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GenomeCfTest {

    @Test
    void size() {
        GenomeCf genome = new GenomeCf(100);
        assertEquals(100, genome.size());
    }

    @Test
    void sizeFromStandardCppInstance() throws IOException {
        Demand demand = Demand.loadFromFile(Paths.get("/lucas/Lucas/Travail/UMons/Master_2/Memoire/git/instances/I013.dat"));
        StandardCppInstance cppInstance = new StandardCppInstance(
                demand, 10, 4
        );

        GenomeCf genome = new GenomeCf(cppInstance);
        System.out.println(genome);

        final int slotsCount = 10 * 4;
        final int bookCoversCount = 30;
        assertEquals(2 * (slotsCount - bookCoversCount), genome.size());
    }

    @Test
    void randomize() throws IOException {
        Demand demand = Demand.loadFromFile(Paths.get("/lucas/Lucas/Travail/UMons/Master_2/Memoire/git/instances/I013.dat"));
        StandardCppInstance cppInstance = new StandardCppInstance(
                demand, 10, 4
        );

        GenomeCf genome = new GenomeCf(cppInstance);
        genome.randomize();
        assertEquals(20, genome.size());
    }
}