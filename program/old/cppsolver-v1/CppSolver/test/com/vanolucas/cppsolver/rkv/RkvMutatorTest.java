package com.vanolucas.cppsolver.rkv;

import org.junit.jupiter.api.Test;

class RkvMutatorTest {

    @Test
    void randomizeOneRandomKey() {
        RandomKeyVector rkv = new RandomKeyVector(10);

        RkvMutator mutator = new OneRandomKeyRkvMutator(
                new RandomizeOneKeyRkvMutator()
        );

        System.out.println(rkv);
        mutator.mutate(rkv);
        System.out.println(rkv);
        mutator.mutate(rkv);
        System.out.println(rkv);
    }

    @Test
    void randomizeKeysWithProba() {
        RandomKeyVector rkv = new RandomKeyVector(10);

        RkvMutator mutator = new ProbaRkvMutator(
                new RandomizeOneKeyRkvMutator(), 0.1d
        );

        System.out.println(rkv);
        mutator.mutate(rkv);
        System.out.println(rkv);
        mutator.mutate(rkv);
        System.out.println(rkv);
    }
}