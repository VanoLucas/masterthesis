package com.vanolucas.cppsolver.study;

import com.google.common.collect.Lists;
import com.vanolucas.cppsolver.clone.CloneableCloner;
import com.vanolucas.cppsolver.clone.EncodedSolutionCloner;
import com.vanolucas.cppsolver.clone.SolutionCloner;
import com.vanolucas.cppsolver.eval.EncodedSolutionEvaluator;
import com.vanolucas.cppsolver.eval.SolutionEvaluator;
import com.vanolucas.cppsolver.experiment.Experiment;
import com.vanolucas.cppsolver.mutation.FirstEncodedRepresentationSolutionMutator;
import com.vanolucas.cppsolver.mutation.NeighborsProducer;
import com.vanolucas.cppsolver.mutation.SolutionMutator;
import com.vanolucas.cppsolver.opti.Opti;
import com.vanolucas.cppsolver.opti.ParallelSteepestAscentHillClimbing;
import com.vanolucas.cppsolver.problem.cpp.eval.CppSolutionEvaluator;
import com.vanolucas.cppsolver.problem.cpp.eval.SlotsDecoder;
import com.vanolucas.cppsolver.problem.cpp.genome.GenomeCf;
import com.vanolucas.cppsolver.problem.cpp.genome.decode.GenomeCfDecoder;
import com.vanolucas.cppsolver.problem.cpp.instance.Demand;
import com.vanolucas.cppsolver.problem.cpp.instance.StandardCppInstance;
import com.vanolucas.cppsolver.rand.Rand;
import com.vanolucas.cppsolver.rand.RandomizableRandomizer;
import com.vanolucas.cppsolver.rkv.RandomizeOneKeyRkvMutator;
import com.vanolucas.cppsolver.solution.EncodedSolution;
import com.vanolucas.cppsolver.solution.Solution;

import java.io.IOException;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class Study01 {
    public static void main(String[] args) throws IOException {
        /*
        experiment params
         */

        final int runsCount = 10;
        final Duration durationPerRun = Duration.ofSeconds(15L);

        /*
        load a standard cpp instance
         */

        Demand demand = Demand.loadFromFile(Paths.get("/lucas/Lucas/Travail/UMons/Master_2/Memoire/git/instances/I013.dat"));
        StandardCppInstance cppInstance = new StandardCppInstance(
                demand, 10, 4
        );

        /*
        initial solution
         */

        Solution initialSolution = new EncodedSolution(
                2, new GenomeCf(cppInstance)
        );

        /*
        neighbors producer
         */

        // full randomization of the genome
        List<SolutionMutator> solutionMutators = Lists.newArrayList(
                new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer()),
                new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer()),
                new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer())
        );

        // randomization of one key in the genome
        Rand rand = new Rand();
        final int genomeLength = GenomeCf.calculateLengthForCppInstance(cppInstance);
        for (int keyIndex = 0; keyIndex < genomeLength; keyIndex++) {
            solutionMutators.add(
                    new FirstEncodedRepresentationSolutionMutator(
                            new RandomizeOneKeyRkvMutator(keyIndex, rand)
                    )
            );
        }

        SolutionCloner solutionCloner = new EncodedSolutionCloner(
                new CloneableCloner()
        );

        NeighborsProducer neighborsProducer = new NeighborsProducer(
                solutionMutators, solutionCloner
        );

        /*
        evaluator
         */

        SolutionEvaluator solutionEvaluator = new EncodedSolutionEvaluator(
                new GenomeCfDecoder(cppInstance),
                new SlotsDecoder(cppInstance),
                new CppSolutionEvaluator(cppInstance)
        );

        /*
        create experiment
         */

        ExecutorService executor = Executors.newFixedThreadPool(4);

        // opti algo supplier
        Supplier<Opti> optiSupplier = () -> new ParallelSteepestAscentHillClimbing(
                initialSolution, neighborsProducer, solutionEvaluator, executor
        );

        // experiment
        Experiment experiment = new Experiment(
                optiSupplier, runsCount, durationPerRun, true
        );

        /*
        run experiment
         */

        experiment.run();

        executor.shutdown();

        String bestQualitiesStr = experiment.streamBestQualities()
                .sorted(Comparator.reverseOrder())
                .map(Object::toString)
                .collect(Collectors.joining(";"));
        System.out.println(bestQualitiesStr);

        /*
        Best qualities:
        116581.0;116633.0;116636.0;116694.0;116706.0;116793.0;116811.0;116885.0;116916.0;117048.0
         */
    }
}
