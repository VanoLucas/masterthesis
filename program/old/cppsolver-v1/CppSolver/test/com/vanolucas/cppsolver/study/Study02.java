package com.vanolucas.cppsolver.study;

import com.google.common.collect.Lists;
import com.vanolucas.cppsolver.clone.CloneableCloner;
import com.vanolucas.cppsolver.clone.EncodedSolutionCloner;
import com.vanolucas.cppsolver.clone.SolutionCloner;
import com.vanolucas.cppsolver.eval.EncodedSolutionEvaluator;
import com.vanolucas.cppsolver.eval.SolutionEvaluator;
import com.vanolucas.cppsolver.experiment.Experiment;
import com.vanolucas.cppsolver.mutation.FirstEncodedRepresentationSolutionMutator;
import com.vanolucas.cppsolver.mutation.NeighborsProducer;
import com.vanolucas.cppsolver.mutation.SolutionMutator;
import com.vanolucas.cppsolver.opti.Opti;
import com.vanolucas.cppsolver.opti.ParallelSteepestAscentHillClimbing;
import com.vanolucas.cppsolver.problem.cpp.eval.CppSolutionEvaluator;
import com.vanolucas.cppsolver.problem.cpp.eval.SlotsDecoder;
import com.vanolucas.cppsolver.problem.cpp.genome.GenomeCf;
import com.vanolucas.cppsolver.problem.cpp.genome.decode.GenomeCfDecoder;
import com.vanolucas.cppsolver.problem.cpp.instance.Demand;
import com.vanolucas.cppsolver.problem.cpp.instance.StandardCppInstance;
import com.vanolucas.cppsolver.rand.Rand;
import com.vanolucas.cppsolver.rand.RandomizableRandomizer;
import com.vanolucas.cppsolver.rkv.RandomizeOneKeyRkvMutator;
import com.vanolucas.cppsolver.solution.EncodedSolution;
import com.vanolucas.cppsolver.solution.Solution;

import java.io.IOException;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class Study02 {
    public static void main(String[] args) throws IOException {
        /*
        experiment params
         */

        final int runsCount = 20 * 4;
        final Duration durationPerRun = Duration.ofSeconds(15L);

        /*
        load a standard cpp instance
         */

        Demand demand = Demand.loadFromFile(Paths.get("/lucas/Lucas/Travail/UMons/Master_2/Memoire/git/instances/I013.dat"));
        StandardCppInstance cppInstance = new StandardCppInstance(
                demand, 10, 4
        );

        /*
        initial solution
         */

        Solution initialSolution = new EncodedSolution(
                2, new GenomeCf(cppInstance)
        );

        /*
        neighbors producer
         */

        // mutation: full randomization of the genome
        List<SolutionMutator> solutionMutators = Lists.newArrayList(
                new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer()),
                new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer()),
                new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer())
        );

        // mutation: randomization of one key in the genome
        Rand rand = new Rand();
        final int genomeLength = GenomeCf.calculateLengthForCppInstance(cppInstance);
        for (int keyIndex = 0; keyIndex < genomeLength; keyIndex++) {
            solutionMutators.add(
                    new FirstEncodedRepresentationSolutionMutator(
                            new RandomizeOneKeyRkvMutator(keyIndex, rand)
                    )
            );
        }

        SolutionCloner solutionCloner = new EncodedSolutionCloner(
                new CloneableCloner()
        );

        NeighborsProducer neighborsProducer = new NeighborsProducer(
                solutionMutators, solutionCloner
        );

        /*
        evaluator
         */

        SolutionEvaluator solutionEvaluator = new EncodedSolutionEvaluator(
                new GenomeCfDecoder(cppInstance),
                new SlotsDecoder(cppInstance),
                new CppSolutionEvaluator(cppInstance)
        );

        /*
        create experiment
         */

        ExecutorService executor = Executors.newFixedThreadPool(4);

        // opti algo supplier
        Supplier<Opti> optiSupplier = () -> new ParallelSteepestAscentHillClimbing(
                initialSolution, neighborsProducer, solutionEvaluator, executor
        );

        // experiment
        Experiment experiment = new Experiment(
                optiSupplier, runsCount, durationPerRun, true
        );

        /*
        run experiment
         */

        experiment.run();

        executor.shutdown();

        /*
        print stats
         */

        String bestQualitiesStr = experiment.streamBestQualities()
                .sorted(Comparator.reverseOrder())
                .map(Object::toString)
                .collect(Collectors.joining(";"));
        System.out.println(String.format("Best qualities:%n%s", bestQualitiesStr));

        System.out.println(String.format("Avg best quality: %f", experiment.getAvgQuality()));
        System.out.println(String.format("Median best quality: %f", experiment.getMedianQuality()));
    }
}

/*
New best:
|   2 |   6 |   7 |   8 | :  22030 printings
|   1 |   9 |  10 |  11 | :  20000 printings
|   4 |   5 |  12 |  13 | :  17021 printings
|  14 |  15 |  16 |  17 | :  15000 printings
|   3 |   3 |  18 |  19 | :  13517 printings
|   1 |  20 |  21 |  22 | :  11000 printings
|   4 |   5 |  23 |  24 | :   9000 printings
|   2 |  25 |  26 |  27 | :   6072 printings
|   6 |  25 |  28 |  29 | :   1500 printings
|  12 |  14 |  28 |  30 | :   1131 printings
116271.0
Best qualities:
116271.0;116541.0;116548.0;116553.0;116553.0;116556.0;116563.0;116564.0;116572.0;116575.0;116576.0;116576.0;116580.0;116584.0;116584.0;116585.0;116586.0;116587.0;116591.0;116593.0;116594.0;116594.0;116601.0;116603.0;116604.0;116607.0;116609.0;116612.0;116614.0;116614.0;116615.0;116616.0;116620.0;116621.0;116624.0;116625.0;116626.0;116632.0;116636.0;116642.0;116646.0;116649.0;116663.0;116686.0;116705.0;116717.0;116727.0;116742.0;116745.0;116752.0;116783.0;116827.0;116853.0;116860.0;116863.0;116870.0;116873.0;116920.0;116923.0;116948.0;116963.0;117025.0;117033.0;117036.0;117045.0;117547.0;119000.0;119000.0;119000.0;119000.0;119000.0;119000.0;119000.0;119000.0;119000.0;119000.0;119000.0;119000.0;119500.0;119500.0
Avg best quality: 117110.600000
Median best quality: 116644.000000
 */