package com.vanolucas.cppsolver.study;

import com.google.common.collect.Lists;
import com.vanolucas.cppsolver.clone.CloneableCloner;
import com.vanolucas.cppsolver.clone.EncodedSolutionCloner;
import com.vanolucas.cppsolver.clone.SolutionCloner;
import com.vanolucas.cppsolver.eval.EncodedSolutionEvaluator;
import com.vanolucas.cppsolver.eval.SolutionEvaluator;
import com.vanolucas.cppsolver.experiment.Experiment;
import com.vanolucas.cppsolver.mutation.FirstEncodedRepresentationSolutionMutator;
import com.vanolucas.cppsolver.mutation.NeighborsProducer;
import com.vanolucas.cppsolver.mutation.SolutionMutator;
import com.vanolucas.cppsolver.opti.Opti;
import com.vanolucas.cppsolver.opti.ParallelSteepestAscentHillClimbing;
import com.vanolucas.cppsolver.problem.cpp.eval.CppSolutionEvaluator;
import com.vanolucas.cppsolver.problem.cpp.eval.SlotsDecoder;
import com.vanolucas.cppsolver.problem.cpp.genome.GenomeCf;
import com.vanolucas.cppsolver.problem.cpp.genome.GenomePpc;
import com.vanolucas.cppsolver.problem.cpp.genome.GenomeSp;
import com.vanolucas.cppsolver.problem.cpp.genome.decode.GenomeCfDecoder;
import com.vanolucas.cppsolver.problem.cpp.genome.decode.GenomePpcDecoder;
import com.vanolucas.cppsolver.problem.cpp.genome.decode.GenomeSpDecoder;
import com.vanolucas.cppsolver.problem.cpp.instance.Demand;
import com.vanolucas.cppsolver.problem.cpp.instance.StandardCppInstance;
import com.vanolucas.cppsolver.rand.Rand;
import com.vanolucas.cppsolver.rand.RandomizableRandomizer;
import com.vanolucas.cppsolver.rkv.RandomizeOneKeyRkvMutator;
import com.vanolucas.cppsolver.solution.EncodedSolution;
import com.vanolucas.cppsolver.solution.Solution;

import java.io.IOException;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * CF vs. P-PC vs. S-P genome.
 */
public class Study03 {
    public static void main(String[] args) throws IOException {
        /*
        experiments params
         */

        final int runsCount = 10;
        final Duration durationPerRun = Duration.ofSeconds(15L);

        final String[] experimentNames = new String[]{
                "CF genome", "P-PC genome", "S-P genome"
        };

        /*
        load a standard cpp instance
         */

        Demand demand = Demand.loadFromFile(Paths.get("/lucas/Lucas/Travail/UMons/Master_2/Memoire/git/instances/I013.dat"));
        StandardCppInstance cppInstance = new StandardCppInstance(
                demand, 10, 4
        );

        /*
        initial solution
         */

        Solution[] initialSolutions = new Solution[]{
                new EncodedSolution(2, new GenomeCf(cppInstance)),
                new EncodedSolution(2, new GenomePpc(cppInstance)),
                new EncodedSolution(2, new GenomeSp(cppInstance))
        };

        /*
        neighbors producer
         */

        // mutation: full randomization of the genome
        List<SolutionMutator> solutionMutators = Lists.newArrayList(
                new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer()),
                new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer()),
                new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer())
        );

        // mutation: randomization of one key in the genome
        Rand rand = new Rand();
        final int genomeLength = GenomeCf.calculateLengthForCppInstance(cppInstance);
        for (int keyIndex = 0; keyIndex < genomeLength; keyIndex++) {
            solutionMutators.add(
                    new FirstEncodedRepresentationSolutionMutator(
                            new RandomizeOneKeyRkvMutator(keyIndex, rand)
                    )
            );
        }

        SolutionCloner solutionCloner = new EncodedSolutionCloner(
                new CloneableCloner()
        );

        NeighborsProducer neighborsProducer = new NeighborsProducer(
                solutionMutators, solutionCloner
        );

        /*
        evaluator
         */

        SolutionEvaluator[] solutionEvaluators = new SolutionEvaluator[]{
                new EncodedSolutionEvaluator(
                        new GenomeCfDecoder(cppInstance),
                        new SlotsDecoder(cppInstance),
                        new CppSolutionEvaluator(cppInstance)
                ),
                new EncodedSolutionEvaluator(
                        new GenomePpcDecoder(cppInstance),
                        new SlotsDecoder(cppInstance),
                        new CppSolutionEvaluator(cppInstance)
                ),
                new EncodedSolutionEvaluator(
                        new GenomeSpDecoder(cppInstance),
                        new SlotsDecoder(cppInstance),
                        new CppSolutionEvaluator(cppInstance)
                )
        };

        /*
        create experiments
         */

        ExecutorService executor = Executors.newFixedThreadPool(4);

        // opti algo supplier
        List<Supplier<Opti>> optiSuppliers = IntStream.range(0, initialSolutions.length)
                .mapToObj(i -> (Supplier<Opti>) (() -> new ParallelSteepestAscentHillClimbing(
                        initialSolutions[i], neighborsProducer, solutionEvaluators[i], executor
                )))
                .collect(Collectors.toList());

        // experiments
        List<Experiment> experiments = IntStream.range(0, initialSolutions.length)
                .mapToObj(i -> new Experiment(
                        experimentNames[i], optiSuppliers.get(i), runsCount, durationPerRun, true
                ))
                .collect(Collectors.toList());

        /*
        run experiments
         */

        for (Experiment experiment : experiments) {
            experiment.run();
        }

        executor.shutdown();

        for (Experiment experiment : experiments) {
            System.out.println();
            experiment.printStats();
        }
    }
}

/*
Experiment: CF genome
10 runs of duration 15s
Overall best:
Encoded representation 1 (GenomeCf):
(20 genes) 0.16454826404220946 ; 0.6536073394318371 ; 0.1317433090498068 ; 0.23105637073702145 ; 0.015495373845669569 ; 0.27657335978524433 ; 0.17020414748347568 ; 0.9559065537166282 ; 0.07341584090173625 ; 0.5000783721633059 ; 0.4494194112017401 ; 0.914089429200852 ; 0.05779344257276009 ; 0.3927910131958089 ; 0.921961232513194 ; 0.6598566619167192 ; 0.3768216998470225 ; 0.05577338905724116 ; 0.8179064168955904 ; 0.7790076785381989
Encoded representation 2 (Slots):
 22000 copies /  22000 of book cover   7
 22000 copies /  22000 of book cover   8
 21985 copies /  23000 of book cover   6
 21703 copies /  30000 of book cover   1
 20000 copies /  20000 of book cover   9
 20000 copies /  20000 of book cover  10
 19993 copies /  26000 of book cover   4
 19000 copies /  19000 of book cover  11
 17002 copies /  28000 of book cover   2
 17000 copies /  17000 of book cover  13
 16997 copies /  18000 of book cover  12
 16993 copies /  26000 of book cover   5
 15000 copies /  15000 of book cover  15
 15000 copies /  15000 of book cover  16
 14625 copies /  16000 of book cover  14
 14000 copies /  14000 of book cover  17
 13502 copies /  27000 of book cover   3
 13500 copies /  13500 of book cover  18
 13498 copies /  27000 of book cover   3
 13000 copies /  13000 of book cover  19
 11000 copies /  11000 of book cover  20
 10998 copies /  28000 of book cover   2
 10500 copies /  10500 of book cover  21
 10000 copies /  10000 of book cover  22
  9007 copies /  26000 of book cover   5
  9000 copies /   9000 of book cover  23
  9000 copies /   9000 of book cover  24
  8297 copies /  30000 of book cover   1
  6007 copies /  26000 of book cover   4
  6000 copies /   6000 of book cover  26
  5842 copies /   7500 of book cover  25
  5000 copies /   5000 of book cover  27
  1658 copies /   7500 of book cover  25
  1649 copies /   2500 of book cover  28
  1500 copies /   1500 of book cover  29
  1375 copies /  16000 of book cover  14
  1015 copies /  23000 of book cover   6
  1003 copies /  18000 of book cover  12
  1000 copies /   1000 of book cover  30
   851 copies /   2500 of book cover  28
Phenotype (CppSolution):
|   1 |   6 |   7 |   8 | :  22000 printings
|   4 |   9 |  10 |  11 | :  20000 printings
|   2 |   5 |  12 |  13 | :  17002 printings
|  14 |  15 |  16 |  17 | :  15000 printings
|   3 |   3 |  18 |  19 | :  13502 printings
|   2 |  20 |  21 |  22 | :  11000 printings
|   1 |   5 |  23 |  24 | :   9007 printings
|   4 |  25 |  26 |  27 | :   6007 printings
|  14 |  25 |  28 |  29 | :   1658 printings
|   6 |  12 |  28 |  30 | :   1015 printings
116191.0
Best qualities:
116191.0 116581.0 116589.0 116668.0 116669.0 116724.0 116822.0 116850.0 118500.0 119000.0
Average quality: 117059.400000
Median quality: 116696.500000

Experiment: P-PC genome
10 runs of duration 15s
Overall best:
Encoded representation 1 (GenomePpc):
(50 genes) 0.5879549850190682 ; 0.16565354355111084 ; 0.3756824575769554 ; 0.202193444486099 ; 0.17431029066498516 ; 0.24314877352906628 ; 0.8574457568175703 ; 0.5751274141317283 ; 0.9529025427476877 ; 0.2168956979447899 ; 0.7790032615361493 ; 0.9793845469650482 ; 0.29205927028745604 ; 0.9685156651104927 ; 0.38045036924847764 ; 0.057168649565846996 ; 0.03846028336396845 ; 0.08966317809437752 ; 0.7264734603729203 ; 0.07384633573056043 ; 0.8153107704304347 ; 0.7455679043101001 ; 0.2699592413647789 ; 0.5180209499068077 ; 0.43371524422721275 ; 0.34625561730958454 ; 0.8777320183160005 ; 0.36340800389954064 ; 0.08947545965603321 ; 0.17558314081947357 ; 0.19328810749017233 ; 0.19432439015822567 ; 0.05747485188918755 ; 0.018036303050800728 ; 0.5811853603021598 ; 0.4405500842646498 ; 0.5351370587692761 ; 0.41330045881016375 ; 0.4952803804393938 ; 0.16751498072052873 ; 0.4283636163270136 ; 0.6177222733484083 ; 0.711640577948095 ; 0.22421029910229828 ; 0.47243737569798006 ; 0.24937751976142075 ; 0.3718399228525898 ; 0.06391787045673303 ; 0.41338596940463257 ; 0.05928045008503269
Encoded representation 2 (Slots):
 27329 copies /  30000 of book cover   1
 27000 copies /  27000 of book cover   3
 26000 copies /  26000 of book cover   4
 26000 copies /  26000 of book cover   5
 20000 copies /  20000 of book cover   9
 20000 copies /  20000 of book cover  10
 19000 copies /  19000 of book cover  11
 18000 copies /  18000 of book cover  12
 15000 copies /  15000 of book cover  15
 15000 copies /  15000 of book cover  16
 14000 copies /  14000 of book cover  17
 13500 copies /  13500 of book cover  18
 12226 copies /  23000 of book cover   6
 12173 copies /  28000 of book cover   2
 12078 copies /  22000 of book cover   8
 12022 copies /  22000 of book cover   7
 11000 copies /  11000 of book cover  20
 10998 copies /  17000 of book cover  13
 10949 copies /  28000 of book cover   2
 10500 copies /  10500 of book cover  21
 10000 copies /  16000 of book cover  14
 10000 copies /  10000 of book cover  22
  9978 copies /  22000 of book cover   7
  9922 copies /  22000 of book cover   8
  9000 copies /   9000 of book cover  23
  9000 copies /   9000 of book cover  24
  8178 copies /  13000 of book cover  19
  7500 copies /   7500 of book cover  25
  6002 copies /  23000 of book cover   6
  6002 copies /  17000 of book cover  13
  6000 copies /  16000 of book cover  14
  6000 copies /   6000 of book cover  26
  5000 copies /   5000 of book cover  27
  4878 copies /  28000 of book cover   2
  4822 copies /  13000 of book cover  19
  4772 copies /  23000 of book cover   6
  2671 copies /  30000 of book cover   1
  2500 copies /   2500 of book cover  28
  1500 copies /   1500 of book cover  29
  1000 copies /   1000 of book cover  30
Phenotype (CppSolution):
|   1 |   3 |   4 |   5 | :  27329 printings
|   9 |  10 |  11 |  12 | :  20000 printings
|  15 |  16 |  17 |  18 | :  15000 printings
|   2 |   6 |   7 |   8 | :  12226 printings
|   2 |  13 |  20 |  21 | :  11000 printings
|   7 |   8 |  14 |  22 | :  10000 printings
|  19 |  23 |  24 |  25 | :   9000 printings
|   6 |  13 |  14 |  26 | :   6002 printings
|   2 |   6 |  19 |  27 | :   5000 printings
|   1 |  28 |  29 |  30 | :   2671 printings
118228.0
Best qualities:
118228.0 119025.0 119149.0 119253.0 119327.0 119423.0 119559.0 120355.0 120383.0 120500.0
Average quality: 119520.200000
Median quality: 119375.000000

Experiment: S-P genome
10 runs of duration 15s
Overall best:
Encoded representation 1 (GenomeSp):
(70 genes) 0.6520648495419932 ; 0.581436790235565 ; 0.18928094645749127 ; 0.12201959702731968 ; 0.4602296180356694 ; 0.9659742219449267 ; 0.9446003513723972 ; 0.05900283681807483 ; 0.6156948523506788 ; 0.9133248051140925 ; 0.5867961174836583 ; 0.9908760705845405 ; 0.9658950414012513 ; 0.1629764001347659 ; 0.17671250809546735 ; 0.02536556042807725 ; 0.6940802928218079 ; 0.7506297768394385 ; 0.9583819375690057 ; 0.21872359815351516 ; 0.6602084495793186 ; 0.2483929777076448 ; 0.37768556386141305 ; 0.10071491047970371 ; 0.38662587815287996 ; 0.15505818022201778 ; 0.07326130749217696 ; 0.3394005586538563 ; 0.3583129076624737 ; 0.29729632421674235 ; 0.3345747836580336 ; 0.03925703033265293 ; 0.6709400779231693 ; 0.5063306314862039 ; 0.11933557406222617 ; 0.22796641579241472 ; 0.8869068030852328 ; 0.2174383704077545 ; 0.1950356705720352 ; 0.6185256957540143 ; 0.5325352826136008 ; 0.6790427524665827 ; 0.05644712844503352 ; 0.9280232028564604 ; 0.32467347915829325 ; 0.03737034997585331 ; 0.22525411954067387 ; 0.5846910755369742 ; 0.15310528822255465 ; 0.6465581771113253 ; 0.3727680821045928 ; 0.9509500197498376 ; 0.18207892807732973 ; 0.8300708405799464 ; 0.1842130489814322 ; 0.8317636221819649 ; 0.3024304458465915 ; 0.9765150437181265 ; 0.3918583708698531 ; 0.9604719165461499 ; 0.5651910486511155 ; 0.29141514568390803 ; 0.9443140462776748 ; 0.3583744946885158 ; 0.15823329531623553 ; 0.7462399423075567 ; 0.8505934057525311 ; 0.9496856784077282 ; 0.9494233072426163 ; 0.7669477345273775
Encoded representation 2 (Slots):
 27000 copies /  27000 of book cover   3
 26850 copies /  30000 of book cover   1
 26000 copies /  26000 of book cover   4
 26000 copies /  26000 of book cover   5
 20312 copies /  22000 of book cover   8
 20000 copies /  20000 of book cover   9
 20000 copies /  20000 of book cover  10
 19000 copies /  19000 of book cover  11
 16000 copies /  16000 of book cover  14
 15958 copies /  28000 of book cover   2
 15000 copies /  15000 of book cover  15
 15000 copies /  15000 of book cover  16
 14000 copies /  14000 of book cover  17
 13745 copies /  17000 of book cover  13
 13500 copies /  13500 of book cover  18
 12994 copies /  18000 of book cover  12
 12125 copies /  23000 of book cover   6
 12042 copies /  28000 of book cover   2
 11822 copies /  22000 of book cover   7
 10875 copies /  23000 of book cover   6
 10500 copies /  10500 of book cover  21
 10178 copies /  22000 of book cover   7
 10000 copies /  10000 of book cover  22
  9534 copies /  13000 of book cover  19
  9000 copies /   9000 of book cover  23
  9000 copies /   9000 of book cover  24
  7850 copies /  11000 of book cover  20
  7500 copies /   7500 of book cover  25
  5006 copies /  18000 of book cover  12
  5000 copies /   5000 of book cover  27
  4950 copies /   6000 of book cover  26
  3466 copies /  13000 of book cover  19
  3255 copies /  17000 of book cover  13
  3150 copies /  30000 of book cover   1
  3150 copies /  11000 of book cover  20
  2500 copies /   2500 of book cover  28
  1688 copies /  22000 of book cover   8
  1500 copies /   1500 of book cover  29
  1050 copies /   6000 of book cover  26
  1000 copies /   1000 of book cover  30
Phenotype (CppSolution):
|   1 |   3 |   4 |   5 | :  27000 printings
|   8 |   9 |  10 |  11 | :  20312 printings
|   2 |  14 |  15 |  16 | :  16000 printings
|  12 |  13 |  17 |  18 | :  14000 printings
|   2 |   6 |   6 |   7 | :  12125 printings
|   7 |  19 |  21 |  22 | :  10500 printings
|  20 |  23 |  24 |  25 | :   9000 printings
|  12 |  19 |  26 |  27 | :   5006 printings
|   1 |  13 |  20 |  28 | :   3255 printings
|   8 |  26 |  29 |  30 | :   1688 printings
118886.0
Best qualities:
118886.0 118902.0 119070.0 119075.0 119090.0 119131.0 119307.0 119314.0 119390.0 119394.0
Average quality: 119155.900000
Median quality: 119110.500000

 */