package com.vanolucas.cppsolver.study;

import com.google.common.collect.Lists;
import com.vanolucas.cppsolver.clone.CloneableCloner;
import com.vanolucas.cppsolver.clone.EncodedSolutionCloner;
import com.vanolucas.cppsolver.clone.SolutionCloner;
import com.vanolucas.cppsolver.eval.EncodedSolutionEvaluator;
import com.vanolucas.cppsolver.eval.SolutionEvaluator;
import com.vanolucas.cppsolver.experiment.Experiment;
import com.vanolucas.cppsolver.mutation.FirstEncodedRepresentationSolutionMutator;
import com.vanolucas.cppsolver.mutation.NeighborsProducer;
import com.vanolucas.cppsolver.mutation.SolutionMutator;
import com.vanolucas.cppsolver.opti.Opti;
import com.vanolucas.cppsolver.opti.ParallelSteepestAscentHillClimbing;
import com.vanolucas.cppsolver.problem.cpp.eval.CppSolutionEvaluator;
import com.vanolucas.cppsolver.problem.cpp.eval.SlotsDecoder;
import com.vanolucas.cppsolver.problem.cpp.genome.GenomeCf;
import com.vanolucas.cppsolver.problem.cpp.genome.decode.GenomeCfDecoder;
import com.vanolucas.cppsolver.problem.cpp.instance.Demand;
import com.vanolucas.cppsolver.problem.cpp.instance.StandardCppInstance;
import com.vanolucas.cppsolver.rand.Rand;
import com.vanolucas.cppsolver.rand.RandomizableRandomizer;
import com.vanolucas.cppsolver.rkv.RandomizeOneKeyRkvMutator;
import com.vanolucas.cppsolver.solution.EncodedSolution;
import com.vanolucas.cppsolver.solution.Solution;

import java.io.IOException;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Steepest ascent hill climbing for 15s on multiple instances.
 */
public class Study04 {
    public static void main(String[] args) throws IOException {
        /*
        experiments params
         */

        final int runsCount = 10;
        final Duration durationPerRun = Duration.ofSeconds(35L);

        final String[] experimentNames = new String[]{
                "data25-5",
                "E015",
                "I001",
                "I003",
                "I013",
                "I014",
                "I015",
                "PLPP18",
                "PLPP22"
        };

        /*
        load standard cpp instances
         */

        final StandardCppInstance[] cppInstances = new StandardCppInstance[]{
                new StandardCppInstance(
                        Demand.loadFromFile(Paths.get("/lucas/Lucas/Travail/UMons/Master_2/Memoire/git/instances/data25-5.dat")), 10, 4
                ),
                new StandardCppInstance(
                        Demand.loadFromFile(Paths.get("/lucas/Lucas/Travail/UMons/Master_2/Memoire/git/instances/E015.dat")), 8, 4
                ),
                new StandardCppInstance(
                        Demand.loadFromFile(Paths.get("/lucas/Lucas/Travail/UMons/Master_2/Memoire/git/instances/I001.dat")), 4, 4
                ),
                new StandardCppInstance(
                        Demand.loadFromFile(Paths.get("/lucas/Lucas/Travail/UMons/Master_2/Memoire/git/instances/I003.dat")), 4, 4
                ),
                new StandardCppInstance(
                        Demand.loadFromFile(Paths.get("/lucas/Lucas/Travail/UMons/Master_2/Memoire/git/instances/I013.dat")), 10, 4
                ),
                new StandardCppInstance(
                        Demand.loadFromFile(Paths.get("/lucas/Lucas/Travail/UMons/Master_2/Memoire/git/instances/I014.dat")), 13, 4
                ),
                new StandardCppInstance(
                        Demand.loadFromFile(Paths.get("/lucas/Lucas/Travail/UMons/Master_2/Memoire/git/instances/I015.dat")), 19, 4
                ),
                new StandardCppInstance(
                        Demand.loadFromFile(Paths.get("/lucas/Lucas/Travail/UMons/Master_2/Memoire/git/instances/PLPP18.dat")), 5, 14
                ),
                new StandardCppInstance(
                        Demand.loadFromFile(Paths.get("/lucas/Lucas/Travail/UMons/Master_2/Memoire/git/instances/PLPP22.dat")), 5, 15
                ),
        };

        /*
        initial solutions
         */

        final List<Solution> initialSolutions = IntStream.range(0, experimentNames.length)
                .mapToObj(i -> new EncodedSolution(
                        2,
                        new GenomeCf(cppInstances[i])
                ))
                .collect(Collectors.toList());

        /*
        neighbors producers
         */

        final Rand rand = new Rand();

        final SolutionCloner solutionCloner = new EncodedSolutionCloner(
                new CloneableCloner()
        );

        final List<NeighborsProducer> neighborsProducers = IntStream.range(0, experimentNames.length)
                .mapToObj(i -> {
                    // mutation: full randomization of the genome
                    final List<SolutionMutator> solutionMutators = Lists.newArrayList(
                            new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer()),
                            new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer()),
                            new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer())
                    );
                    // mutation: randomization of one key in the genome
                    final int genomeLength = GenomeCf.calculateLengthForCppInstance(cppInstances[i]);
                    for (int keyIndex = 0; keyIndex < genomeLength; keyIndex++) {
                        solutionMutators.add(
                                new FirstEncodedRepresentationSolutionMutator(
                                        new RandomizeOneKeyRkvMutator(keyIndex, rand)
                                )
                        );
                    }
                    // construct neighbors producer
                    return new NeighborsProducer(solutionMutators, solutionCloner);
                })
                .collect(Collectors.toList());

        /*
        evaluators
         */

        final List<SolutionEvaluator> solutionEvaluators = IntStream.range(0, experimentNames.length)
                .mapToObj(i -> new EncodedSolutionEvaluator(
                        new GenomeCfDecoder(cppInstances[i]),
                        new SlotsDecoder(cppInstances[i]),
                        new CppSolutionEvaluator(cppInstances[i])
                ))
                .collect(Collectors.toList());

        /*
        create experiments
         */

        final ExecutorService executor = Executors.newFixedThreadPool(4);

        // opti algo suppliers
        final List<Supplier<Opti>> optiSuppliers = IntStream.range(0, experimentNames.length)
                .mapToObj(i -> (Supplier<Opti>) (() -> new ParallelSteepestAscentHillClimbing(
                        initialSolutions.get(i), neighborsProducers.get(i), solutionEvaluators.get(i), executor
                )))
                .collect(Collectors.toList());

        // experiments
        final List<Experiment> experiments = IntStream.range(0, experimentNames.length)
                .mapToObj(i -> new Experiment(
                        experimentNames[i], optiSuppliers.get(i), runsCount, durationPerRun, true
                ))
                .collect(Collectors.toList());

        /*
        run experiments
         */

        // run experiments
        for (Experiment experiment : experiments) {
            System.out.println(experiment);
            experiment.run();
        }

        // clean stop executor
        executor.shutdown();

        // print experiment results
        for (Experiment experiment : experiments) {
            System.out.println();
            experiment.printStats();
        }
    }
}
