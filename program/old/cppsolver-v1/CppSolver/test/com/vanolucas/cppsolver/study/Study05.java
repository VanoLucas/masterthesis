package com.vanolucas.cppsolver.study;

import com.vanolucas.cppsolver.clone.CloneableCloner;
import com.vanolucas.cppsolver.clone.EncodedSolutionCloner;
import com.vanolucas.cppsolver.clone.SolutionCloner;
import com.vanolucas.cppsolver.eval.Decoder;
import com.vanolucas.cppsolver.eval.EncodedSolutionEvaluator;
import com.vanolucas.cppsolver.eval.SolutionEvaluator;
import com.vanolucas.cppsolver.experiment.Experiment;
import com.vanolucas.cppsolver.mutation.FirstEncodedRepresentationSolutionMutator;
import com.vanolucas.cppsolver.mutation.NeighborsProducer;
import com.vanolucas.cppsolver.mutation.SolutionMutator;
import com.vanolucas.cppsolver.opti.Opti;
import com.vanolucas.cppsolver.opti.RestartParallelSteepestAscentHillClimbing;
import com.vanolucas.cppsolver.problem.cpp.eval.CppSolutionEvaluator;
import com.vanolucas.cppsolver.problem.cpp.eval.SlotsDecoder;
import com.vanolucas.cppsolver.problem.cpp.genome.GenomeCf;
import com.vanolucas.cppsolver.problem.cpp.genome.GenomePpc;
import com.vanolucas.cppsolver.problem.cpp.genome.GenomeSp;
import com.vanolucas.cppsolver.problem.cpp.genome.GenomeType;
import com.vanolucas.cppsolver.problem.cpp.genome.decode.GenomeCfDecoder;
import com.vanolucas.cppsolver.problem.cpp.genome.decode.GenomePpcDecoder;
import com.vanolucas.cppsolver.problem.cpp.genome.decode.GenomeSpDecoder;
import com.vanolucas.cppsolver.problem.cpp.instance.Demand;
import com.vanolucas.cppsolver.problem.cpp.instance.DemandFileChooser;
import com.vanolucas.cppsolver.problem.cpp.instance.StandardCppInstance;
import com.vanolucas.cppsolver.rand.Rand;
import com.vanolucas.cppsolver.rand.RandomizableRandomizer;
import com.vanolucas.cppsolver.rkv.RandomKeyVector;
import com.vanolucas.cppsolver.rkv.RandomizeOneKeyRkvMutator;
import com.vanolucas.cppsolver.solution.EncodedSolution;
import com.vanolucas.cppsolver.solution.Solution;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Compare 3 types of genome.
 */
public class Study05 {
    public static void main(String[] args) throws IOException {
        final Demand demand = DemandFileChooser.chooseFileAndGetDemand();
        final int platesCount = 5;
        final int slotsPerPlateCount = 15;

        final Duration restartAfterDurationWithoutImprovement = Duration.ofSeconds(15L);
        final Duration durationPerGenomeType = Duration.ofMinutes(2L);

        final GenomeType[] genomeTypes = new GenomeType[]{
                GenomeType.S_P, GenomeType.P_PC, GenomeType.CF
        };

        StandardCppInstance cppInstance = new StandardCppInstance(
                demand, platesCount, slotsPerPlateCount
        );
        System.out.println(cppInstance);

        for (GenomeType genomeType : genomeTypes) {
            System.out.println("\nStarting experiment with genome " + genomeType.name());
            final String experimentName = "Genome " + genomeType.name();

            Supplier<RandomKeyVector> genomeSupplier = () -> {
                switch (genomeType) {
                    case CF:
                        return new GenomeCf(cppInstance);
                    case P_PC:
                        return new GenomePpc(cppInstance);
                    case S_P:
                        return new GenomeSp(cppInstance);
                    default:
                        throw new IllegalArgumentException("Unknown genome type.");
                }
            };

            Rand rand = new Rand();

            Supplier<RandomKeyVector> randomGenomeSupplier = () -> {
                RandomKeyVector genome = genomeSupplier.get();
                genome.randomize(rand);
                return genome;
            };

            final int genomeLength;
            switch (genomeType) {
                case CF:
                    genomeLength = GenomeCf.calculateLengthForCppInstance(cppInstance);
                    break;
                case P_PC:
                    genomeLength = GenomePpc.calculateLengthForCppInstance(cppInstance);
                    break;
                case S_P:
                    genomeLength = GenomeSp.calculateLengthForCppInstance(cppInstance);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown genome type");
            }

            Supplier<Solution> initialSolutionsSupplier = () -> new EncodedSolution(
                    2, randomGenomeSupplier.get()
            );

            List<SolutionMutator> solutionMutators = new ArrayList<>();
            // seems good to have full randomization operators for instances with high slotsPerPlate count or when using P-PC genome
            if (slotsPerPlateCount > 4 || genomeType == GenomeType.P_PC) {
                solutionMutators.addAll(Arrays.asList(
                        new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer()),
                        new FirstEncodedRepresentationSolutionMutator(new RandomizableRandomizer())
                ));
            }
            solutionMutators.addAll(IntStream.range(0, genomeLength)
                    .mapToObj(keyIndex -> new FirstEncodedRepresentationSolutionMutator(new RandomizeOneKeyRkvMutator(keyIndex)))
                    .collect(Collectors.toList())
            );

            SolutionCloner solutionCloner = new EncodedSolutionCloner(
                    new CloneableCloner()
            );

            NeighborsProducer neighborsProducer = new NeighborsProducer(
                    solutionMutators, solutionCloner
            );

            Supplier<Decoder> genomeDecoderSupplier = () -> {
                switch (genomeType) {
                    case CF:
                        return new GenomeCfDecoder(cppInstance);
                    case P_PC:
                        return new GenomePpcDecoder(cppInstance);
                    case S_P:
                        return new GenomeSpDecoder(cppInstance);
                    default:
                        throw new IllegalArgumentException("Unknown genome type.");
                }
            };

            SolutionEvaluator solutionEvaluator = new EncodedSolutionEvaluator(
                    genomeDecoderSupplier.get(),
                    new SlotsDecoder(cppInstance),
                    new CppSolutionEvaluator(cppInstance)
            );

            ExecutorService executor = Executors.newFixedThreadPool(4);

            Supplier<Opti> optiSupplier = () -> new RestartParallelSteepestAscentHillClimbing(
                    initialSolutionsSupplier, neighborsProducer, solutionEvaluator, executor, restartAfterDurationWithoutImprovement
            );

            Experiment experiment = new Experiment(
                    experimentName, optiSupplier, 1, durationPerGenomeType, false
            );

            experiment.run();
            executor.shutdown();

            experiment.printStats();
        }
    }
}
